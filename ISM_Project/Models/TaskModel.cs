﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ISM_Project.Models
{
    public class TaskModel
    {
        public int TaskId { get; set; }
        public string TaskNumber { get; set; }
        public string TaskName { get; set; }
        public Nullable<int> TaskGroupId { get; set; }
        public string TaskGroupName { get; set; }
        public Nullable<int> RoleId { get; set; }
        public string RoleName { get; set; }
        public List<InputModel> Inputs { get; set; }
        public List<OutputModel> Outputs { get; set; }
        public List<ReasonModel> Records { get; set; }
        public string PrerequisiteTasks { get; set; }
        public Nullable<DateTime> EstimatedCompletionDate { get; set; }
    }
}