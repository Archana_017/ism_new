//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ISM_Project.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class InventoryUsageHistory
    {
        public int InventoryUseID { get; set; }
        public Nullable<int> InventoryID { get; set; }
        public string QunatityUsed { get; set; }
        public Nullable<int> InvestigationID { get; set; }
        public Nullable<int> NotificationID { get; set; }
        public Nullable<System.DateTime> UsageDate { get; set; }
        public string Remarks { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
    
        public virtual Inventory Inventory { get; set; }
    }
}
