//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ISM_Project.Models
{
    using System;
    
    public partial class USP_VIEW_USER_SKILL_Result
    {
        public int UserSkillID { get; set; }
        public int UserID { get; set; }
        public int LKSkillTypeID { get; set; }
        public int ExperienceInYears { get; set; }
        public int UserSkillRating { get; set; }
    }
}
