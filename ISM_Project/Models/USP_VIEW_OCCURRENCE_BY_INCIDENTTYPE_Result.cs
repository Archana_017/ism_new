//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ISM_Project.Models
{
    using System;
    
    public partial class USP_VIEW_OCCURRENCE_BY_INCIDENTTYPE_Result
    {
        public string OccurrenceYear { get; set; }
        public Nullable<int> Accident { get; set; }
        public Nullable<int> Serious_Incident { get; set; }
        public Nullable<int> Incident { get; set; }
        public Nullable<int> Others { get; set; }
    }
}
