﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using ISM_Project.Models;
using System.Security.Cryptography;
using System.Web.Helpers;
using PagedList;
using System.Text.RegularExpressions;

namespace ISM_Project.Controllers
{
    /// <summary>
    /// Default application generated  Home - Controll  application  login functionality , Global Search for all dashboard and application common theme settings.
    /// </summary> 
    /// 
    public class HomeController : Controller
    {
        gcaa_ismEntities dc = new gcaa_ismEntities();
        Common comm = new Common();

        /// <summary>
        /// Dashboard
        /// </summary> 
        /// 
        public ActionResult Dashboard()
        {
            return View();
        }
        /// <summary>
        /// Index page
        /// </summary> 
        /// 
        public ActionResult Index()
        {
            if (Session["username"] != null)
            {
                return RedirectToAction("Dashboard", "Home");
            }
            return View();
        }
        /// <summary>
        /// user login page
        /// </summary> 
        /// 
        /// <param name="user">Receive the user details as object.</param>     
        [HttpPost]
        public ActionResult Index(ISM_Project.Models.Login user)
        {
            LoginPassword pass = new LoginPassword();

            try
            {
                var pa = dc.Users.Where(a => a.EmailAddress == user.username).FirstOrDefault();

                if (pa != null)
                {
                    string salt = pa.PasswordSalt;

                    user.password = pass.Generatehash(user.password, salt.ToString());

                    /*Crypto.Hash(user.password);*/

                    using (gcaa_ismEntities gc = new gcaa_ismEntities())
                    {
                        var user_details = gc.Users.Where(a => a.EmailAddress == user.username && a.PasswordHash == user.password).FirstOrDefault();
                        var rolename = gc.Users.Where(a => a.EmailAddress == user.username && a.PasswordHash == user.password).Select(a => a.UserRoleID).SingleOrDefault();
                        if (user_details != null)
                        {
                            if (user_details.Status == 1)
                            {
                                Session["username"] = user.username;
                                Session["roleid"] = rolename;
                                user.password = Crypto.Hash(user.password);
                                Session["displayname"] = user_details.FirstName;
                                Session["UserId"] = user_details.UserID;
                                Session["FirstName"] = user_details.FirstName;
                                if (user.rememberme)
                                {
                                    HttpCookie cookies = new HttpCookie("LoginCookies");
                                    cookies["username_ck"] = user.username;
                                    cookies["password_ck"] = user.password;
                                    cookies.Expires = DateTime.Now.AddHours(1);
                                    Response.Cookies.Add(cookies);
                                }

                                var GetDefaultTheme = dc.Themes.Where(a => a.IsActive == 1).FirstOrDefault();

                                Session["theme_name"] = GetDefaultTheme.ClassName.ToString();

                                Session["Font_Size"] = GetDefaultTheme.FontSize.ToString();

                                if (user_details.ActAsDirector == true)
                                {                                   
                                    return RedirectToAction("UserRoleSelection", "Home");
                                }

                                return RedirectToAction("Dashboard", "Home");
                            }
                            else
                            {
                                ViewBag.errormessage = "User has been deactivated.Please contact admin.";

                                return View(user);
                            }
                        }
                        else
                        {
                            ViewBag.errormessage = "Invalid Email or Password.";

                            return View(user);
                        }
                    }
                }
                else
                {
                    ViewBag.errormessage = "Invalid Email";

                    return View(user);
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("Home", "Index", ex.Message.ToString() + " line No :" + ex.StackTrace.ToString() + "" + ex.InnerException.ToString(), Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }

            // return View(user);
        }
        /// <summary>
        /// User Role Selection view page
        /// </summary> 
        /// 
        public ActionResult UserRoleSelection()
        {
            var UserRoleList = dc.USP_VIEW_USERROLE().ToList();

            ViewBag.UserRole = UserRoleList;

            return View();
        }
        /// <summary>
        /// user role id set as default "3" in session because of Act As Director functionality
        /// </summary> 
        /// 
        /// <param name="usr">Receive the user details as object.</param>         
        [HttpPost]
        public ActionResult UserRoleSelection(User usr)
        {
            if (usr.UserRoleID == 3)
            {
                Session["roleid"] = usr.UserRoleID;
            }

            return RedirectToAction("Dashboard", "Home");
        }
        /// <summary>
        /// description of application
        /// </summary> 
        /// 
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
        /// <summary>
        /// Login API
        /// </summary> 
        /// 
        public ActionResult LoginAPI()
        {
            return View();
        }
        /// <summary>
        /// Contact
        /// </summary> 
        /// 
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        /// <summary>
        /// Here we implement the code for global search based on user role id
        /// </summary> 
        /// 
        /// <param name="page">Receive the global search page no.</param>     
        /// <param name="page1">Receive the global search page no.</param>     
        /// <param name="SearchText">Receive the global search text.</param>     
        [HttpGet]
        public ActionResult GlobalSearch(int? page, int? page1, string SearchText = "")
        {
            var _invSearch = new InvGlobalSearch();

            try
            {
                TempData["active_url_manuals_iic"] = "iic";

                TempData["investigationdetail_url"] = "iic";
                if (Session["act_dashboard"].ToString() == "di")
                {
                    if (page == null)
                    {
                        ViewBag.serial_nbr = 0;
                    }
                    else
                    {
                        ViewBag.serial_nbr = 10 * (page - 1);
                    }

                    _invSearch = new InvGlobalSearch()
                    {
                        InvDiDashboard = dc.USP_VIEW_OCCURRENCE_FOR_DI(Convert.ToInt32(Session["UserId"]),0, 0,0).

                        Where(x =>

                             string.Equals(x.NotificationId.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                            || x.NotificationId.ToLower().Contains(SearchText.ToLower())

                            || string.Equals(x.LKOccurrenceStatusTypeName.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                            || x.LKOccurrenceStatusTypeName.ToLower().Contains(SearchText.ToLower())

                            || string.Equals(x.LKIncidentTypeName.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                            || x.LKIncidentTypeName.ToLower().Contains(SearchText.ToLower())

                            || string.Equals(x.LKCountryName.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                            || x.LKCountryName.ToLower().Contains(SearchText.ToLower())

                            || string.Equals(x.LKCountryName.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                            || x.LKCountryName.ToLower().Contains(SearchText.ToLower())

                            || string.Equals(x.OccurrenceDescription.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                            || x.OccurrenceDescription.ToLower().Contains(SearchText.ToLower())

                            ).ToList()
                    };

                    ViewBag.page_count = page;

                    ViewBag.page_content = _invSearch.InvDiDashboard.ToList().Count();

                    TempData["SearchKey"] = SearchText.ToString();
                }
                else if (Session["act_dashboard"].ToString() == "teammember")
                {
                    if (page == null)
                    {
                        ViewBag.serial_nbr = 0;
                    }
                    else
                    {
                        ViewBag.serial_nbr = 10 * (page - 1);
                    }

                    _invSearch = new InvGlobalSearch()
                    {
                        InvTeamMemDashBoard = dc.USP_VIEW_INVESTIGATIONDETAILS_FOR_TEAMMEMBER_DASHBOARD(Convert.ToInt32(Session["UserId"])).

                        Where(x =>

                             string.Equals(x.InvestigationNumber.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                            || x.InvestigationNumber.ToLower().Contains(SearchText.ToLower())

                            || string.Equals(x.InvestigationTaskGroupEventName.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                            || x.InvestigationTaskGroupEventName.ToLower().Contains(SearchText.ToLower())

                            || string.Equals(x.NotificationActor.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                            || x.NotificationActor.ToLower().Contains(SearchText.ToLower())

                            || string.Equals(x.NotificationType.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                            || x.NotificationType.ToLower().Contains(SearchText.ToLower())

                            || string.Equals(x.NotificationID.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                            || x.NotificationID.ToLower().Contains(SearchText.ToLower())

                            || string.Equals(x.CountryName.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                            || x.CountryName.ToLower().Contains(SearchText.ToLower())

                            ).ToList()
                    };

                    ViewBag.page_count = page;

                    ViewBag.page_content = _invSearch.InvTeamMemDashBoard.ToList().Count();

                    TempData["SearchKey"] = SearchText.ToString();
                }
                else if (Session["act_dashboard"].ToString() == "IIC")
                {
                    if (page == null)
                    {
                        ViewBag.serial_nbr = 0;
                    }
                    else
                    {
                        ViewBag.serial_nbr = 10 * (page - 1);
                    }
                    if (page1 == null)
                    {
                        ViewBag.serial_nbr1 = 0;
                    }
                    else
                    {
                        ViewBag.serial_nbr1 = 10 * (page1 - 1);
                    }

                    TempData["active_dashboard"] = "IIC";

                    Session["act_dashboard"] = "IIC";

                    _invSearch = new InvGlobalSearch()
                    {
                        InvDashBoard = dc.USP_VIEW_INVESTIGATIONDETAILS_BY_OCCURRENCESTATUSTYPE_ID("3,4", Convert.ToInt32(Session["UserId"]), 0, 0).

                        Where(x =>

                             string.Equals(x.InvestigationNumber.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                             || x.InvestigationNumber.ToLower().Contains(SearchText.ToLower())

                             || string.Equals(x.PlaceOfIncident.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                             || x.PlaceOfIncident.ToLower().Contains(SearchText.ToLower())

                             || string.Equals(x.LKOccurrenceStatusTypeName.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                             || x.LKOccurrenceStatusTypeName.ToLower().Contains(SearchText.ToLower())

                             || string.Equals(x.NotificationID.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                             || x.NotificationID.ToLower().Contains(SearchText.ToLower())

                             || string.Equals(x.CountryName.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                             || x.CountryName.ToLower().Contains(SearchText.ToLower())

                             || string.Equals(x.LKIncidentTypeName.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                             || x.LKIncidentTypeName.ToLower().Contains(SearchText.ToLower())

                             || string.Equals(x.EventDescription.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                             || x.EventDescription.ToLower().Contains(SearchText.ToLower())

                             || string.Equals(x.OccurrenceDescription.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                             || x.OccurrenceDescription.ToLower().Contains(SearchText.ToLower())

                             || string.Equals(x.OccurrenceCategorization.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                             || x.OccurrenceCategorization.ToLower().Contains(SearchText.ToLower())



                             ).ToList(),

                        InvTeamMemDashBoard = dc.USP_VIEW_INVESTIGATIONDETAILS_FOR_TEAMMEMBER_DASHBOARD(Convert.ToInt32(Session["UserId"])).

                        Where(x =>

                        string.Equals(x.InvestigationNumber.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                             || x.InvestigationNumber.ToLower().Contains(SearchText.ToLower())

                             || string.Equals(x.PlaceOfIncident.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                             || x.PlaceOfIncident.ToLower().Contains(SearchText.ToLower())

                             || string.Equals(x.InvestigationTaskGroupEventName.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                             || x.InvestigationTaskGroupEventName.ToLower().Contains(SearchText.ToLower())

                             || string.Equals(x.NotificationID.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                             || x.NotificationID.ToLower().Contains(SearchText.ToLower())

                             || string.Equals(x.NotificationType.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                             || x.NotificationType.ToLower().Contains(SearchText.ToLower())

                             || string.Equals(x.NotificationActor.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                             || x.NotificationActor.ToLower().Contains(SearchText.ToLower())

                             || string.Equals(x.CountryName.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                             || x.CountryName.ToLower().Contains(SearchText.ToLower())

                        ).ToList()
                    };

                    ViewBag.page_no = page;

                    ViewBag.page_no1 = page1;


                    ViewBag.count_pagecontent = _invSearch.InvDashBoard.Count();

                    ViewBag.count_pagecontent1 = _invSearch.InvTeamMemDashBoard.Count();

                    TempData["SearchKey"] = SearchText.ToString();
                }
                else if (Session["act_dashboard"].ToString() == "director")
                {
                    if (page == null)                           //occurrences
                    {
                        ViewBag.serial_nbr = 0;
                    }
                    else
                    {
                        ViewBag.serial_nbr = 10 * (page - 1);
                    }
                    if (page1 == null)                         //Investigations
                    {
                        ViewBag.serial_nbr1 = 0;
                    }
                    else
                    {
                        ViewBag.serial_nbr1 = 10 * (page1 - 1);
                    }

                    TempData["active_dashboard"] = "director";

                    Session["act_dashboard"] = "director";

                    _invSearch = new InvGlobalSearch()
                    {
                        InvAllOccurance = dc.USP_VIEW_ALL_OCCURRENCE(0, 0, 0, 0).

                        Where(x =>

                             string.Equals(x.InvestigationNumber.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                             || x.InvestigationNumber.ToLower().Contains(SearchText.ToLower())

                             || string.Equals(x.IncidentType.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                             || x.IncidentType.ToLower().Contains(SearchText.ToLower())

                             || string.Equals(x.Location.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                             || x.Location.ToLower().Contains(SearchText.ToLower())

                             || string.Equals(x.OccuranceStatus.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                             || x.OccuranceStatus.ToLower().Contains(SearchText.ToLower())

                             || string.Equals(x.NotificationID.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                             || x.NotificationID.ToLower().Contains(SearchText.ToLower())

                              || string.Equals(x.OccurrenceDescription.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                             || x.OccurrenceDescription.ToLower().Contains(SearchText.ToLower())

                        ).ToList(),

                        InvAllInvestig = dc.USP_VIEW_INVESTIGATION(0, 0)

                        .Where(x =>

                        string.Equals(x.InvestigationNumber.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                             || x.InvestigationNumber.ToLower().Contains(SearchText.ToLower())

                             || string.Equals(x.CountryName.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                             || x.CountryName.ToLower().Contains(SearchText.ToLower())

                             || string.Equals(x.LKIncidentTypeName.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                             || x.LKIncidentTypeName.ToLower().Contains(SearchText.ToLower())

                             || string.Equals(x.LKOccurrenceStatusTypeName.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                             || x.LKOccurrenceStatusTypeName.ToLower().Contains(SearchText.ToLower())

                             || string.Equals(x.PlaceOfIncident.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                             || x.PlaceOfIncident.ToLower().Contains(SearchText.ToLower())

                             || string.Equals(x.NotificationID.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                             || x.NotificationID.ToLower().Contains(SearchText.ToLower())

                             || string.Equals(x.EventDescription.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                             || x.EventDescription.ToLower().Contains(SearchText.ToLower())

                        ).ToList()
                    };

                    ViewBag.page_no = page;

                    ViewBag.page_no1 = page1;

                    ViewBag.count_pagecontent = _invSearch.InvAllOccurance.Count();

                    ViewBag.count_pagecontent1 = _invSearch.InvAllInvestig.Count();

                    TempData["SearchKey"] = SearchText.ToString();
                }
                else if (Session["act_dashboard"].ToString() == "admin")
                {
                    if (page == null)
                    {
                        ViewBag.serial_nbr = 0;
                    }
                    else
                    {
                        ViewBag.serial_nbr = 10 * (page - 1);
                    }

                    TempData["active_dashboard"] = "admin";

                    _invSearch = new InvGlobalSearch()
                    {
                        invUser = dc.USP_VIEW_ALL_USERS().Where(x =>

                         string.Equals(x.FirstName.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                         || x.FirstName.ToLower().Contains(SearchText.ToLower())

                         || string.Equals(x.EmailAddress.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                         || x.EmailAddress.ToLower().Contains(SearchText.ToLower())

                         || string.Equals(x.LKUserRoleName.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                         || x.LKUserRoleName.ToLower().Contains(SearchText.ToLower())

                         || string.Equals(x.ContactNo.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                         || x.ContactNo.ToLower().Contains(SearchText.ToLower())

                         ).ToList()
                    };


                    ViewBag.page_no = page;

                    ViewBag.count_pagecontent = _invSearch.invUser.Count();

                    TempData["SearchKey"] = SearchText.ToString();
                }
                else if (Session["act_dashboard"].ToString() == "global_DL")
                {
                    //if (page == null)                          
                    //{
                    //    ViewBag.serial_nbr = 0;
                    //}
                    //else
                    //{
                    //    ViewBag.serial_nbr = 10 * (page - 1);
                    //}
                    //if (page1 == null)                       
                    //{
                    //    ViewBag.serial_nbr1 = 0;
                    //}
                    //else
                    //{
                    //    ViewBag.serial_nbr1 = 10 * (page1 - 1);
                    //}

                    //TempData["active_dashboard"] = "admin";                   

                    _invSearch = new InvGlobalSearch()
                    {
                        invGlobalDL = dc.USP_VIEW_GLOBAL_DISTRIBUTIONLIST().Where(x =>

                         string.Equals(x.EmailDistributionListName.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                         || x.EmailDistributionListName.ToLower().Contains(SearchText.ToLower())

                         || string.Equals(x.EmailDistributionListDescription.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                         || x.EmailDistributionListDescription.ToLower().Contains(SearchText.ToLower())

                          || string.Equals(x.EmailAddess.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                         || x.EmailAddess.ToLower().Contains(SearchText.ToLower())

                        ).ToList()
                    };

                    //ViewBag.page_no = page;

                    //ViewBag.page_no1 = page1;

                    //ViewBag.count_pagecontent = _invSearch.InvAllOccurance.Count();

                    //ViewBag.count_pagecontent1 = _invSearch.InvAllInvestig.Count();

                    TempData["SearchKey"] = SearchText.ToString();
                }
                else if (Session["act_dashboard"].ToString() == "SRM")
                {
                    if (page == null)
                    {
                        ViewBag.serial_nbr = 0;
                    }
                    else
                    {
                        ViewBag.serial_nbr = 10 * (page - 1);
                    }
                    if (page1 == null)
                    {
                        ViewBag.serial_nbr1 = 0;
                    }
                    else
                    {
                        ViewBag.serial_nbr1 = 10 * (page1 - 1);
                    }

                    TempData["active_dashboard"] = "SRM";

                    Session["act_dashboard"] = "SRM";

                    _invSearch = new InvGlobalSearch()
                    {
                        InvDashBoard = dc.USP_VIEW_INVESTIGATIONDETAILS_BY_OCCURRENCESTATUSTYPE_ID("3,4", Convert.ToInt32(Session["UserId"]), 0, 0).

                        Where(x =>

                             string.Equals(x.InvestigationNumber.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                             || x.InvestigationNumber.ToLower().Contains(SearchText.ToLower())

                             || string.Equals(x.PlaceOfIncident.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                             || x.PlaceOfIncident.ToLower().Contains(SearchText.ToLower())

                             || string.Equals(x.LKOccurrenceStatusTypeName.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                             || x.LKOccurrenceStatusTypeName.ToLower().Contains(SearchText.ToLower())

                             || string.Equals(x.NotificationID.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                             || x.NotificationID.ToLower().Contains(SearchText.ToLower())

                             || string.Equals(x.CountryName.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                             || x.CountryName.ToLower().Contains(SearchText.ToLower())

                             || string.Equals(x.LKIncidentTypeName.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                             || x.LKIncidentTypeName.ToLower().Contains(SearchText.ToLower())

                             || string.Equals(x.EventDescription.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                             || x.EventDescription.ToLower().Contains(SearchText.ToLower())

                             || string.Equals(x.OccurrenceDescription.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                             || x.OccurrenceDescription.ToLower().Contains(SearchText.ToLower())

                             ).ToList(),

                        InvTeamMemDashBoard = dc.USP_VIEW_INVESTIGATIONDETAILS_FOR_TEAMMEMBER_DASHBOARD(Convert.ToInt32(Session["UserId"])).

                        Where(x =>

                        string.Equals(x.InvestigationNumber.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                             || x.InvestigationNumber.ToLower().Contains(SearchText.ToLower())

                             || string.Equals(x.PlaceOfIncident.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                             || x.PlaceOfIncident.ToLower().Contains(SearchText.ToLower())

                             || string.Equals(x.InvestigationTaskGroupEventName.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                             || x.InvestigationTaskGroupEventName.ToLower().Contains(SearchText.ToLower())

                             || string.Equals(x.NotificationID.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                             || x.NotificationID.ToLower().Contains(SearchText.ToLower())

                             || string.Equals(x.NotificationType.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                             || x.NotificationType.ToLower().Contains(SearchText.ToLower())

                             || string.Equals(x.NotificationActor.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                             || x.NotificationActor.ToLower().Contains(SearchText.ToLower())

                             || string.Equals(x.CountryName.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                             || x.CountryName.ToLower().Contains(SearchText.ToLower())

                        ).ToList()
                    };

                    ViewBag.page_no = page;

                    ViewBag.page_no1 = page1;


                    ViewBag.count_pagecontent = _invSearch.InvDashBoard.Count();

                    ViewBag.count_pagecontent1 = _invSearch.InvTeamMemDashBoard.Count();

                    TempData["SearchKey"] = SearchText.ToString();
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("HomeController", "GlobalSearch(int? page, string SearchText =)", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }

            return View(_invSearch);
        }
        /// <summary>
        /// Application Theme setting page open , we can able to change the application theme and appearances 
        /// </summary> 
        /// 
        [HttpGet]
        public ActionResult ThemeSetting()
        {
            Theme Thm = new Theme();

            try
            {
                var ThemeInfo = dc.Themes.ToList().OrderByDescending(a => a.IsActive);

                SelectList Thm_List = new SelectList(ThemeInfo, "ClassName", "ThemeName");

                ViewBag.Theme_List = Thm_List;

                var GetDefaultTheme = dc.Themes.Where(a => a.IsActive == 1).FirstOrDefault();

                Thm.ThemeName = GetDefaultTheme.ThemeName;

                Thm.ClassName = GetDefaultTheme.ClassName;

                Thm.FontSize = GetDefaultTheme.FontSize;
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("", "", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("NotFound", "Error");
            }

            return View(Thm);
        }
        /// <summary>
        /// change the theme name and save the changes reflect immediate in whole application.
        /// </summary> 
        /// 
        /// <param name="Thm">Receive the application theme settings as object.</param>     
        [HttpPost]
        public ActionResult ThemeSetting(Theme Thm)
        {
            try
            {
                Thm.ThemeId = dc.Themes.Where(a => a.ClassName == Thm.ClassName).FirstOrDefault().ThemeId;

                if (Thm.FontSize == 0) { Thm.FontSize = 14; }

                var result = dc.USP_UPDT_THEME(Thm.ThemeId, Thm.FontSize);

                var ThemeInfo = dc.Themes.ToList().OrderByDescending(a => a.IsActive);

                SelectList Thm_List = new SelectList(ThemeInfo, "ClassName", "ThemeName");

                ViewBag.Theme_List = Thm_List;

                var GetDefaultTheme = dc.Themes.Where(a => a.IsActive == 1).FirstOrDefault();

                Session["theme_name"] = GetDefaultTheme.ClassName.ToString();

                Session["Font_Size"] = GetDefaultTheme.FontSize.ToString();
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("", "", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("NotFound", "Error");
            }

            return View(Thm);
        }
    }
}