//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ISM_Project.Models
{
    using System;
    
    public partial class USP_VIEW_POSSESSION_FORM_PDF_Result
    {
        public string LocationOfStorage { get; set; }
        public string EvidenceSerialNumber { get; set; }
        public string EvidencePartNumber { get; set; }
        public string EvidenceName { get; set; }
        public string PersonHandingOverEvidence { get; set; }
        public string OrganizationHandingOverEvidence { get; set; }
        public System.DateTime DateCreated { get; set; }
        public string EvidenceManufacturer { get; set; }
        public string EvidenceDescription { get; set; }
        public string InvestigationFileNumber { get; set; }
        public string InvestigatorInCharge { get; set; }
        public string IICAssigned { get; set; }
    }
}
