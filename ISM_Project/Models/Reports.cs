﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ISM_Project.Models
{
    public class Reports
    {
        public int InvestigationFileID { get; set; }
        public string FilePath { get; set; }
        public string FileVersion { get; set; }
        public string InvestigationFileNumber { get; set; }
        public HttpPostedFileBase[] ReportFiles { get; set; }

        public IEnumerable<USP_VIEW_REPORTS_Result> RPTs { get; set; }
        public IEnumerable<USP_VIEW_INVESTIGATION_FORM35_Result> RPTInv { get; set; }

        public int LKAircraftRegistrationID { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }

        public string SR_Number { get; set; }
        public int SR_ID { get; set; }
        public int LKAirportID { get; set; }
        public int LKOccurrenceCategorizationId { get; set; }

        public string IsSrReport { get; set; }

        public IEnumerable<USP_VIEW_SR_FOR_EXCEEXPORT_Result> ExcelInfo { get; set; }
        public IEnumerable<USP_VIEW_INVESTIGATIONDETAILS_FOR_EXCEEXPORT_Result> ExcelInvInfo { get; set; }
        public IEnumerable<USP_VIEW_OCCURRENCEDETAILS_FOR_EXCEEXPORT_Result> ExcelOccInfo { get; set; }

        public string NotificationNo { get; set;}
    }
}