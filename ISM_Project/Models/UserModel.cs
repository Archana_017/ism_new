﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ISM_Project.Models
{
    public class UserModel
    {
        public int user_id { get; set; }
        public string first_name { get; set; }
        public string middle_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public Nullable<int> emp_id { get; set; }
        public string phone { get; set; }
        public Nullable<System.DateTime> dob { get; set; }
        public Nullable<System.DateTime> doj { get; set; }
        public Nullable<int> dept_id { get; set; }
        public string address { get; set; }
        public string country { get; set; }
        public string state { get; set; }
        public Nullable<int> pin { get; set; }
        public Nullable<bool> is_active { get; set; }
        public Nullable<int> role_id { get; set; }
        public string role_name { get; set; }
        public int created_by { get; set; }
        public System.DateTime created_date { get; set; }
        public System.DateTime updated_date { get; set; }
        public string work_experience { get; set; }
        public string ResetPasswordCode { get; set; }
        public List<CertificateModel> Certificates { get; set; }
    }
}