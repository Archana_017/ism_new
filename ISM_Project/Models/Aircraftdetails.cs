﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ISM_Project.Models
{
    public class Aircraftdetails
    {
        public int[] LKAircraftMakeID { get; set; }

       
        public Nullable<int> LKAircraftModelID { get; set; }
        public Nullable<int> LKAircraftTypeID { get; set; }
      
        public Nullable<int> LKAircraftRegistrationID { get; set; }
        public string NameOfRegisteredOwner { get; set; }
        public Nullable<int> LKAirlineOperatorID { get; set; }
        public string QualificationOfPilot { get; set; }
        public string PilotCertificateNumber { get; set; }
        public Nullable<int> LKAirport { get; set; }
        public Nullable<int> LKAirport1 { get; set; }
        public Nullable<int> PersonsAboard { get; set; }
        public Nullable<int> PersonsKilled { get; set; }
        public Nullable<int> PersonsSeriouslyInjured { get; set; }
        public Nullable<int> PersonsWithMinorInjury { get; set; }
        public Nullable<int> LKCountryID { get; set; }

    }
}

 