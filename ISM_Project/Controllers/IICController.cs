﻿using ISM_Project.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using PagedList;
using PagedList.Mvc;
using System.Web.Routing;
using System.Web.Hosting;
using System.Data;

namespace ISM_Project.Controllers
{
    /// <summary>
    /// IIC - Controller  contains some functionality of investigation process such as Correspondences,Analysis,
    ///                   Task assignment and delete investigation. 
    /// </summary> 
    /// 

    public class IICController : Controller
    {
        gcaa_ismEntities gcaa = new gcaa_ismEntities();
        Common comm = new Common();
        static string[] SharepointfilePath = new string[100];
        static string[] SharepointAnonymousPath = new string[100];
        static int[] DocumentTypeId = new int[100];
        static int[] userId = new int[50];
        static int[] DLId = new int[50];
        static string[] RecepientType = new string[50];
        static string[] extMailids = new string[50];
        static string[] mailType = new string[50];
        MailConfiguration mail = new MailConfiguration();
        PdfGeneration pdf = new PdfGeneration();
        static int InvFileID = 0;
        // GET: Investigator

        /// <summary>
        /// Analysis Main page
        /// </summary> 
        /// 
        /// <param name="investfileid">Receive the investigation file id.</param>     
        /// <param name="tab">Receive the tab page index no.</param>     
        [HttpGet]
        public ActionResult AnalysisMainpage(/*int userid, int taskgroupid, int taskassignmentid, int eventuniqno,*/ int investfileid, int tab = 1)
        {
            try
            {
                ViewBag.tabno = tab;
                var results = gcaa.InvestigationEventAnalysis.AsQueryable().Where(a => a.InvestigationId == investfileid).ToList();
                if (results.Count() > 0)
                {
                    foreach (var result_status in results)
                    {
                        if (result_status.LKAnalysisId == 1)
                        {
                            ViewBag.jamesstatus = 1;
                        }

                        if (result_status.LKAnalysisId == 2)
                        {
                            ViewBag.shellstatus = 1;
                        }

                        if (result_status.LKAnalysisId == 3)
                        {
                            ViewBag.fishbonesstatus = 1;
                        }
                    }
                    if (ViewBag.jamesstatus == null)
                    {
                        ViewBag.jamesstatus = 0;

                    }
                    if (ViewBag.shellstatus == null)
                    {
                        ViewBag.shellstatus = 0;

                    }
                    if (ViewBag.fishbonesstatus == null)
                    {
                        ViewBag.fishbonesstatus = 0;
                    }
                }
                else
                {
                    ViewBag.jamesstatus = 0;
                    ViewBag.shellstatus = 0;
                    ViewBag.fishbonesstatus = 0;
                }



                ViewBag.investfileid = investfileid;
                var InvInfo = gcaa.USP_VIEW_INVESTIGATION_FORM35(investfileid).FirstOrDefault();
                TempData["PageHead"] = InvInfo.InvestigationNumber;
                TempData["InvId"] = investfileid;
                TempData["InvFileID"] = investfileid;
                TempData["BackPage"] = "yes";

                TempData["active_rootcause_iic"] = "iic";
                TempData["lf_menu"] = "1";


                if (ViewBag.shellstatus == 0)
                {
                    var Lsysproc = gcaa.LKLivewareSysProcedures.ToList().OrderBy(x => x.LKLivewareSysProceduresName);
                    SelectList Lsysproc_list = new SelectList(Lsysproc, "LKLivewareSysProceduresId", "LKLivewareSysProceduresName");
                    ViewBag.Lsysproc = Lsysproc_list;
                    var Lsystrng = gcaa.LKLivewareSysTrainings.ToList().OrderBy(x => x.LKLivewareSysTrainingName);
                    SelectList Lsystrng_list = new SelectList(Lsystrng, "LKLivewareSysTrainingId", "LKLivewareSysTrainingName");
                    ViewBag.Lsystrng = Lsystrng_list;


                    var Lhardhardware = gcaa.LKLivewareHardHardwares.ToList().OrderBy(x => x.LKLivewareHardHardwareName);
                    SelectList Lhardhardware_list = new SelectList(Lhardhardware, "LKLivewareHardHardwareId", "LKLivewareHardHardwareName");
                    ViewBag.Lhardhardware = Lhardhardware_list;
                    var Linfods = gcaa.LKLivewareHardInfoDatas.ToList().OrderBy(x => x.LKLivewareHardInfoDataName);
                    SelectList Linfods_list = new SelectList(Linfods, "LKLivewareHardInfoDataId", "LKLivewareHardInfoDataName");
                    ViewBag.Linfods = Linfods_list;
                    var Lsoftwarehard = gcaa.LKLivewareHardFirmwares.ToList().OrderBy(x => x.LKLivewareHardFirmwareName);
                    SelectList Lsoftwarehard_list = new SelectList(Lsoftwarehard, "LKLivewareHardFirmwareId", "LKLivewareHardFirmwareName");
                    ViewBag.Lsoftwarehard = Lsoftwarehard_list;
                    var Lotheroper = gcaa.LKLivewareHardOtherOperationals.ToList().OrderBy(x => x.LKLivewareHardOtherOperationalName);
                    SelectList Lotheroper_list = new SelectList(Lotheroper, "LKLivewareHardOtherOperationalId", "LKLivewareHardOtherOperationalName");
                    ViewBag.Lotheroper = Lotheroper_list;


                    var Lenvphyenv = gcaa.LKLivewareEnvPhysicals.ToList().OrderBy(x => x.LKLivewareEnvPhysicalName);
                    SelectList Lenvphyenv_list = new SelectList(Lenvphyenv, "LKLivewareEnvPhysicalId", "LKLivewareEnvPhysicalName");
                    ViewBag.Lenvphyenv = Lenvphyenv_list;
                    var Lenvpsycho = gcaa.LKLivewareEnvPsychosocials.ToList().OrderBy(x => x.LKLivewareEnvPsychosocialName);
                    SelectList Lenvpsycho_list = new SelectList(Lenvpsycho, "LKLivewareEnvPsychosocialId", "LKLivewareEnvPsychosocialName");
                    ViewBag.Lenvpsycho = Lenvpsycho_list;
                    var Lenvcompmgmt = gcaa.LKLivewareEnvComMgmts.ToList().OrderBy(x => x.LKLivewareEnvComMgmtName);
                    SelectList Lenvcompmgmt_list = new SelectList(Lenvcompmgmt, "LKLivewareEnvComMgmtId", "LKLivewareEnvComMgmtName");
                    ViewBag.Lenvcompmgmt = Lenvcompmgmt_list;
                    var Lopertsk = gcaa.LKLivewareEnvOperationaltsks.ToList().OrderBy(x => x.LKLivewareEnvOperationaltskName);
                    SelectList Lopertsk_list = new SelectList(Lopertsk, "LKLivewareEnvOperationaltskId", "LKLivewareEnvOperationaltskName");
                    ViewBag.Lopertsk = Lopertsk_list;


                    var Lintercomm = gcaa.LkLivewareInterfaceComms.ToList().OrderBy(x => x.LkLivewareInterfaceCommName);
                    SelectList Lintercomm_list = new SelectList(Lintercomm, "LkLivewareInterfaceCommId", "LkLivewareInterfaceCommName");
                    ViewBag.Lintercomm = Lintercomm_list;
                    var Linterskill = gcaa.LkLivewareInterfaceTmskls.ToList().OrderBy(x => x.LkLivewareInterfaceTmsklName);
                    SelectList Linterskill_list = new SelectList(Linterskill, "LkLivewareInterfaceTmsklId", "LkLivewareInterfaceTmsklName");
                    ViewBag.Linterskill = Linterskill_list;
                    var Lintersprvsn = gcaa.LkLivewareInterfaceSupvsns.ToList().OrderBy(x => x.LkLivewareInterfaceSupvsnName);
                    SelectList Lintersprvsn_list = new SelectList(Lintersprvsn, "LkLivewareInterfaceSupvsnId", "LkLivewareInterfaceSupvsnName");
                    ViewBag.Lintersprvsn = Lintersprvsn_list;
                    var Linterregact = gcaa.LkLivewareInterfaceRegacts.ToList().OrderBy(x => x.LkLivewareInterfaceRegactName);
                    SelectList Linterregact_list = new SelectList(Linterregact, "LkLivewareInterfaceRegactId", "LkLivewareInterfaceRegactName");
                    ViewBag.Linterregact = Linterregact_list;

                    var Lphysical = gcaa.LkLivewarePhysicals.ToList().OrderBy(x => x.LkLivewarePhysicalName);
                    SelectList Lphysical_list = new SelectList(Lphysical, "LkLivewarePhysicalId", "LkLivewarePhysicalName");
                    ViewBag.Lphysical = Lphysical_list;
                    var Lpsychological = gcaa.LkLivewarePsychologicals.ToList().OrderBy(x => x.LkLivewarePsychologicalName);
                    SelectList Lpsychological_list = new SelectList(Lpsychological, "LkLivewarePsychologicalId", "LkLivewarePsychologicalName");
                    ViewBag.Lpsychological = Lpsychological_list;
                    var wrkldmgmt = gcaa.LkLivewarewrkldmgmts.ToList().OrderBy(x => x.LkLivewarewrkldmgmtName);
                    SelectList wrkldmgmt_list = new SelectList(wrkldmgmt, "LkLivewarewrkldmgmtId", "LkLivewarewrkldmgmtName");
                    ViewBag.wrkldmgmt = wrkldmgmt_list;
                    var Lexpqual = gcaa.LkLivewareExpQuals.ToList().OrderBy(x => x.LkLivewareExpQualName);
                    SelectList Lexpqual_list = new SelectList(Lexpqual, "LkLivewareExpQualId", "LkLivewareExpQualName");
                    ViewBag.Lexpqual = Lexpqual_list;

                }
                else
                {
                    var Lsysproc = gcaa.LKLivewareSysProcedures.ToList().OrderBy(x => x.LKLivewareSysProceduresName);
                    SelectList Lsysproc_list = new SelectList(Lsysproc, "LKLivewareSysProceduresId", "LKLivewareSysProceduresName");
                    ViewBag.Lsysproc = Lsysproc_list;
                    var Lsystrng = gcaa.LKLivewareSysTrainings.ToList().OrderBy(x => x.LKLivewareSysTrainingName);
                    SelectList Lsystrng_list = new SelectList(Lsystrng, "LKLivewareSysTrainingId", "LKLivewareSysTrainingName");
                    ViewBag.Lsystrng = Lsystrng_list;


                    var Lhardhardware = gcaa.LKLivewareHardHardwares.ToList().OrderBy(x => x.LKLivewareHardHardwareName);
                    SelectList Lhardhardware_list = new SelectList(Lhardhardware, "LKLivewareHardHardwareId", "LKLivewareHardHardwareName");
                    ViewBag.Lhardhardware = Lhardhardware_list;
                    var Linfods = gcaa.LKLivewareHardInfoDatas.ToList().OrderBy(x => x.LKLivewareHardInfoDataName);
                    SelectList Linfods_list = new SelectList(Linfods, "LKLivewareHardInfoDataId", "LKLivewareHardInfoDataName");
                    ViewBag.Linfods = Linfods_list;
                    var Lsoftwarehard = gcaa.LKLivewareHardFirmwares.ToList().OrderBy(x => x.LKLivewareHardFirmwareName);
                    SelectList Lsoftwarehard_list = new SelectList(Lsoftwarehard, "LKLivewareHardFirmwareId", "LKLivewareHardFirmwareName");
                    ViewBag.Lsoftwarehard = Lsoftwarehard_list;
                    var Lotheroper = gcaa.LKLivewareHardOtherOperationals.ToList().OrderBy(x => x.LKLivewareHardOtherOperationalName);
                    SelectList Lotheroper_list = new SelectList(Lotheroper, "LKLivewareHardOtherOperationalId", "LKLivewareHardOtherOperationalName");
                    ViewBag.Lotheroper = Lotheroper_list;


                    var Lenvphyenv = gcaa.LKLivewareEnvPhysicals.ToList().OrderBy(x => x.LKLivewareEnvPhysicalName);
                    SelectList Lenvphyenv_list = new SelectList(Lenvphyenv, "LKLivewareEnvPhysicalId", "LKLivewareEnvPhysicalName");
                    ViewBag.Lenvphyenv = Lenvphyenv_list;
                    var Lenvpsycho = gcaa.LKLivewareEnvPsychosocials.ToList().OrderBy(x => x.LKLivewareEnvPsychosocialName);
                    SelectList Lenvpsycho_list = new SelectList(Lenvpsycho, "LKLivewareEnvPsychosocialId", "LKLivewareEnvPsychosocialName");
                    ViewBag.Lenvpsycho = Lenvpsycho_list;
                    var Lenvcompmgmt = gcaa.LKLivewareEnvComMgmts.ToList().OrderBy(x => x.LKLivewareEnvComMgmtName);
                    SelectList Lenvcompmgmt_list = new SelectList(Lenvcompmgmt, "LKLivewareEnvComMgmtId", "LKLivewareEnvComMgmtName");
                    ViewBag.Lenvcompmgmt = Lenvcompmgmt_list;
                    var Lopertsk = gcaa.LKLivewareEnvOperationaltsks.ToList().OrderBy(x => x.LKLivewareEnvOperationaltskName);
                    SelectList Lopertsk_list = new SelectList(Lopertsk, "LKLivewareEnvOperationaltskId", "LKLivewareEnvOperationaltskName");
                    ViewBag.Lopertsk = Lopertsk_list;


                    var Lintercomm = gcaa.LkLivewareInterfaceComms.ToList().OrderBy(x => x.LkLivewareInterfaceCommName);
                    SelectList Lintercomm_list = new SelectList(Lintercomm, "LkLivewareInterfaceCommId", "LkLivewareInterfaceCommName");
                    ViewBag.Lintercomm = Lintercomm_list;
                    var Linterskill = gcaa.LkLivewareInterfaceTmskls.ToList().OrderBy(x => x.LkLivewareInterfaceTmsklName);
                    SelectList Linterskill_list = new SelectList(Linterskill, "LkLivewareInterfaceTmsklId", "LkLivewareInterfaceTmsklName");
                    ViewBag.Linterskill = Linterskill_list;
                    var Lintersprvsn = gcaa.LkLivewareInterfaceSupvsns.ToList().OrderBy(x => x.LkLivewareInterfaceSupvsnName);
                    SelectList Lintersprvsn_list = new SelectList(Lintersprvsn, "LkLivewareInterfaceSupvsnId", "LkLivewareInterfaceSupvsnName");
                    ViewBag.Lintersprvsn = Lintersprvsn_list;
                    var Linterregact = gcaa.LkLivewareInterfaceRegacts.ToList().OrderBy(x => x.LkLivewareInterfaceRegactName);
                    SelectList Linterregact_list = new SelectList(Linterregact, "LkLivewareInterfaceRegactId", "LkLivewareInterfaceRegactName");
                    ViewBag.Linterregact = Linterregact_list;

                    var Lphysical = gcaa.LkLivewarePhysicals.ToList().OrderBy(x => x.LkLivewarePhysicalName);
                    SelectList Lphysical_list = new SelectList(Lphysical, "LkLivewarePhysicalId", "LkLivewarePhysicalName");
                    ViewBag.Lphysical = Lphysical_list;
                    var Lpsychological = gcaa.LkLivewarePsychologicals.ToList().OrderBy(x => x.LkLivewarePsychologicalName);
                    SelectList Lpsychological_list = new SelectList(Lpsychological, "LkLivewarePsychologicalId", "LkLivewarePsychologicalName");
                    ViewBag.Lpsychological = Lpsychological_list;
                    var wrkldmgmt = gcaa.LkLivewarewrkldmgmts.ToList().OrderBy(x => x.LkLivewarewrkldmgmtName);
                    SelectList wrkldmgmt_list = new SelectList(wrkldmgmt, "LkLivewarewrkldmgmtId", "LkLivewarewrkldmgmtName");
                    ViewBag.wrkldmgmt = wrkldmgmt_list;
                    var Lexpqual = gcaa.LkLivewareExpQuals.ToList().OrderBy(x => x.LkLivewareExpQualName);
                    SelectList Lexpqual_list = new SelectList(Lexpqual, "LkLivewareExpQualId", "LkLivewareExpQualName");
                    ViewBag.Lexpqual = Lexpqual_list;

                    var shell_SSI_values = gcaa.USP_VIEW_SHELL_SSI_VALUES(investfileid).ToList();
                    var shell_HSI_values = gcaa.USP_VIEW_SHELL_HSI_VALUES(investfileid).ToList();
                    var shell_EI_values = gcaa.USP_VIEW_SHELL_EI_VALUES(investfileid).ToList();
                    var shell_LI_values = gcaa.USP_VIEW_SHELL_LI_VALUES(investfileid).ToList();
                    var shell_LW_values = gcaa.USP_VIEW_SHELL_LW_VALUES(investfileid).ToList();
                    ViewBag.shell_SSI_values = shell_SSI_values;
                    ViewBag.shell_SSI_values_count = shell_SSI_values.Count();
                    ViewBag.shell_HSI_values = shell_HSI_values;
                    ViewBag.shell_HSI_values_count = shell_HSI_values.Count();
                    ViewBag.shell_EI_values = shell_EI_values;
                    ViewBag.shell_EI_values_count = shell_EI_values.Count();
                    ViewBag.shell_LI_values = shell_LI_values;
                    ViewBag.shell_LI_values_count = shell_LI_values.Count();
                    ViewBag.shell_LW_values = shell_LW_values;
                    ViewBag.shell_LW_values_count = shell_LW_values.Count();


                }
                if (ViewBag.jamesstatus == 0)
                {
                    gcaa_ismEntities gcaa = new gcaa_ismEntities();

                    var org_infl = gcaa.LKOrganizationalInfluences.ToList().OrderBy(x => x.LKOrganizationalInfluences);
                    SelectList org_infl_list = new SelectList(org_infl, "LKOrganizationalInfluencesId", "LKOrganizationalInfluences");
                    ViewBag.org_infl = org_infl_list;
                    var unsafe_svision = gcaa.LKUnsafeSupervisions.ToList().OrderBy(x => x.LKUnsafeSupervisionName);
                    SelectList unsafe_svision_list = new SelectList(unsafe_svision, "LKUnsafeSupervisionId", "LKUnsafeSupervisionName");
                    ViewBag.unsafe_svision_list = unsafe_svision_list;
                    var pre_cond = gcaa.LKPreConditions.ToList().OrderBy(x => x.LKPreConditionsName);
                    SelectList pre_cond_list = new SelectList(pre_cond, "LKPreConditionsId", "LKPreConditionsName");
                    ViewBag.pre_cond_list = pre_cond_list;
                    var unsafe_act = gcaa.LKUnsafeActs.ToList().OrderBy(x => x.LKUnsafeActName);
                    SelectList unsafe_act_list = new SelectList(unsafe_act, "LKUnsafeActId", "LKUnsafeActName");
                    ViewBag.unsafe_act_list = unsafe_act_list;

                    // var analysis = gcaa.LKAnalysis.ToList().OrderBy(x => x.LKAnalysisName).ToList();
                    //SelectList analysislist = new SelectList(analysis, "LKAnalysisId", "LKAnalysisName");
                    //ViewBag.analysis = analysislist;

                }
                else
                {
                    var Orgn_infl = gcaa.LKOrganizationalInfluences.ToList().OrderBy(x => x.LKOrganizationalInfluences);
                    SelectList Orgn_infl_id = new SelectList(Orgn_infl, "LKOrganizationalInfluencesId", "LKOrganizationalInfluences");
                    ViewBag.Orgn_infl_id = Orgn_infl_id;
                    var unsafe_supervision = gcaa.LKUnsafeSupervisions.ToList().OrderBy(x => x.LKUnsafeSupervisionName);
                    SelectList unsafe_supervision_id = new SelectList(unsafe_supervision, "LKUnsafeSupervisionId", "LKUnsafeSupervisionName");
                    ViewBag.unsafe_supervision_id = unsafe_supervision_id;
                    var pre_condition = gcaa.LKPreConditions.ToList().OrderBy(x => x.LKPreConditionsName);
                    SelectList pre_condition_id = new SelectList(pre_condition, "LKPreConditionsId", "LKPreConditionsName");
                    ViewBag.pre_condition_id = pre_condition_id;
                    var unsafe_act = gcaa.LKUnsafeActs.ToList().OrderBy(x => x.LKUnsafeActName);
                    SelectList unsafe_act_id = new SelectList(unsafe_act, "LKUnsafeActId", "LKUnsafeActName");
                    ViewBag.unsafe_act_id = unsafe_act_id;
                    //var eventanalysisid = (from eventanalysis in gcaa.InvestigationEventAnalysis
                    //                       where eventanalysis.InvestigationTaskGroupId == taskgroupid && eventanalysis.InvestigationTaskGroupEventUniqueNumber == eventuniq_no
                    //                       && eventanalysis.InvestigationId == investfileid
                    //                       select new
                    //                       {
                    //                           eventanalysis.InvestigationEventAnalysisId,
                    //                           eventanalysis.InvestigationId
                    //                       }).ToList().FirstOrDefault();

                    var eventanalysisid = (from eventanalysis in gcaa.InvestigationEventAnalysis
                                           where eventanalysis.InvestigationId == investfileid
                                           select new
                                           {
                                               eventanalysis.InvestigationEventAnalysisId,
                                               eventanalysis.InvestigationId
                                           }).ToList().FirstOrDefault();
                    var eventalaysisactive_values = gcaa.USP_VIEW_EVENT_ANALYSIS_ACTIVE_VALUES(eventanalysisid.InvestigationEventAnalysisId).ToList();
                    var eventalaysislatent_values = gcaa.USP_VIEW_EVENT_ANALYSIS_LATENT_VALUES(eventanalysisid.InvestigationEventAnalysisId).ToList();
                    ViewBag.inveventanalysis_id = eventanalysisid.InvestigationEventAnalysisId;
                    ViewBag.activedropdownvalues = eventalaysisactive_values;
                    ViewBag.activedropdownvaluesCount = eventalaysisactive_values.Count();
                    ViewBag.latentdropdownvalues = eventalaysislatent_values;
                    ViewBag.latentdropdownvaluesCount = eventalaysislatent_values.Count();

                    //   var eventalaysisactive_values = gcaa.USP_VIEW_EVENT_ANALYSIS_ACTIVE_VALUES(eventanalysisid.InvestigationEventAnalysisId).ToList();
                    //var eventalaysislatent_values = gcaa.USP_VIEW_EVENT_ANALYSIS_LATENT_VALUES(eventanalysisid.InvestigationEventAnalysisId).ToList();
                    //ViewBag.inveventanalysis_id = eventanalysisid.InvestigationEventAnalysisId;
                    //ViewBag.activedropdownvalues = eventalaysisactive_values;
                    //ViewBag.activedropdownvaluesCount = eventalaysisactive_values.Count();
                    //ViewBag.latentdropdownvalues = eventalaysislatent_values;
                    //ViewBag.latentdropdownvaluesCount = eventalaysislatent_values.Count();


                }
                if (ViewBag.fishbonesstatus != 0)
                {
                    var fishbn_result = gcaa.USP_VIEW_FISHBONE_VALUES(investfileid).ToList();
                    ViewBag.fishbn_result = fishbn_result;
                }

                return View();

            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("IICController", "AnalysisMainpage", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }

        }
        /// <summary>
        /// Analysis Main page - Save or update Root Cause Analysis values 
        /// </summary> 
        /// 
        /// <param name="root">Receive the Root Cause Analysis object.</param>     
        [HttpPost]
        public JsonResult AnalysisMainpage(RootCauseAnalysis root)
        {
            //if (Convert.ToInt32(Session["UserId"]) > 0)
            //{
            //    if (root.InvEventAnalysisId == 0)
            //    {
            //        System.Data.Entity.Core.Objects.ObjectParameter invevanalysisid = new System.Data.Entity.Core.Objects.ObjectParameter("invevanalysis_id", typeof(Int32));

            //        gcaa.USP_INST_INVESTIGATION_EVENT_ANALYSIS(root.InvEventAnalysisId, root.Invfileid, root.Selectanalysis, Convert.ToInt32(Session["UserId"]), invevanalysisid);

            //        for (int j = 0; j < root.unsafe_act_list.Count(); j++)
            //        {
            //            gcaa.USP_INST_INVESTIGATION_EVENT_ANALYSIS_DETAILS(0, Convert.ToInt32(invevanalysisid.Value), root.org_infl[j], root.unsafe_svision_list[j], root.pre_cond_list[j], root.unsafe_act_list[j], root.activelatentchecker[j], Convert.ToInt32(Session["UserId"]));
            //        }
            //        return Json(0);
            //    }
            //    else
            //    {
            //        gcaa.USP_DELETE_INVESTIGATION_EVENT_ANALYSIS_DETAILS(root.InvEventAnalysisddetailId);

            //        for (int j = 0; j < root.unsafe_act_list.Count(); j++)
            //        {
            //            gcaa.USP_UPDATE_INVESTIGATION_EVENT_ANALYSIS_DETAILS(root.InvEventAnalysisddetailId, root.org_infl[j], root.unsafe_svision_list[j], root.pre_cond_list[j], root.unsafe_act_list[j], root.activelatentchecker[j], Convert.ToInt32(Session["UserId"]));
            //        }
            //        return Json(1);
            //    }
            //    return Json(1);
            //}
            //return Json(2);

            //latest code 

            if (Convert.ToInt32(Session["UserId"]) > 0)
            {
                var event_tbl_st = gcaa.InvestigationEventAnalysis.AsQueryable().Where(a => a.InvestigationId == root.Invfileid && a.LKAnalysisId == 1).ToList();
                var jmaes_tbl_st = gcaa.InvestigationEventAnalysisDetaileds.AsQueryable().Where(a => a.InvestigationFileId == root.Invfileid).ToList();
                if (event_tbl_st.Count() > 0)
                {
                    if (jmaes_tbl_st.Count() > 0)
                    {
                        gcaa.USP_DELETE_INVESTIGATION_EVENT_ANALYSIS_DETAILS(root.Invfileid);
                    }

                    for (int j = 0; j < root.unsafe_act_list.Count(); j++)
                    {

                        gcaa.USP_INST_INVESTIGATION_EVENT_ANALYSIS_DETAILS(jmaes_tbl_st[0].InvestigationEventAnalysisId, root.Invfileid, root.org_infl[j], root.unsafe_svision_list[j], root.pre_cond_list[j], root.unsafe_act_list[j], root.activelatentchecker[j], Convert.ToInt32(Session["UserId"]), Convert.ToInt32(Session["UserId"]));

                    }



                    return Json(0);
                }
                else
                {
                    System.Data.Entity.Core.Objects.ObjectParameter invevanalysisid = new System.Data.Entity.Core.Objects.ObjectParameter("invevanalysis_id", typeof(Int32));
                    var result = gcaa.USP_INST_INVESTIGATION_EVENT_ANALYSIS(0, root.Invfileid, 1, Convert.ToInt32(Session["UserId"]), invevanalysisid);

                    for (int j = 0; j < root.unsafe_act_list.Count(); j++)
                    {
                        gcaa.USP_INST_INVESTIGATION_EVENT_ANALYSIS_DETAILS(Convert.ToInt32(invevanalysisid.Value), root.Invfileid, root.org_infl[j], root.unsafe_svision_list[j], root.pre_cond_list[j], root.unsafe_act_list[j], root.activelatentchecker[j], Convert.ToInt32(Session["UserId"]), Convert.ToInt32(Session["UserId"]));
                    }


                    return Json(0);
                }

            }
            return Json(0);






        }
        /// <summary>
        /// Save or update shell Analysis values 
        /// </summary> 
        /// 
        /// <param name="shell">Receive the shell Analysis object.</param>     
        [HttpPost]
        public JsonResult ShellAnalysis(ShellAnalysis shell)
        {

            if (Convert.ToInt32(Session["UserId"]) > 0)
            {
                var event_tbl_st = gcaa.InvestigationEventAnalysis.AsQueryable().Where(a => a.InvestigationId == shell.Invfileid && a.LKAnalysisId == 2).ToList();
                var shell_tbl_st = gcaa.InvestigationShellAnalysisdetails.AsQueryable().Where(a => a.InvestigationFileId == shell.Invfileid).ToList();
                if (event_tbl_st.Count() > 0)
                {
                    int i = 0;
                    if (shell_tbl_st.Count() > 0)
                    {
                        gcaa.USP_REMOVE_SHELL_DETAILS(shell.Invfileid);
                    }

                    int cntrl_count = shell.shellcheker.Count();
                    for (i = 0; i < shell.Livewaresysproc.Count(); i++)
                    {
                        gcaa.USP_INST_Shell_Analysis_Details(event_tbl_st[0].InvestigationEventAnalysisId, shell.Invfileid, shell.Livewaresysproc[i], shell.Livewaresystraining[i], 0, 0, 0, 0, 0,
                            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, Convert.ToInt32(Session["UserId"]), Convert.ToInt32(Session["UserId"]));
                    }
                    for (i = 0; i < shell.Livewarehardhardware.Count(); i++)
                    {
                        gcaa.USP_INST_Shell_Analysis_Details(event_tbl_st[0].InvestigationEventAnalysisId, shell.Invfileid, 0, 0, shell.Livewarehardhardware[i], shell.Livewareinfods[i], shell.Livewaresoftwarehard[i], shell.Livewareotheroper[i], 0,
                            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, Convert.ToInt32(Session["UserId"]), Convert.ToInt32(Session["UserId"]));
                    }
                    for (i = 0; i < shell.Livewareenvphyenv.Count(); i++)
                    {
                        gcaa.USP_INST_Shell_Analysis_Details(event_tbl_st[0].InvestigationEventAnalysisId, shell.Invfileid, 0, 0, 0, 0, 0, 0, shell.Livewareenvphyenv[i],
                            shell.LivewareenvPsycho[i], shell.Livewareenvcmpmgmt[i], shell.Livewareenvopertsk[i], 0, 0, 0, 0, 0, 0, 0, 0, 3, Convert.ToInt32(Session["UserId"]), Convert.ToInt32(Session["UserId"]));
                    }
                    for (i = 0; i < shell.Livewareintercom.Count(); i++)
                    {
                        gcaa.USP_INST_Shell_Analysis_Details(event_tbl_st[0].InvestigationEventAnalysisId, shell.Invfileid, 0, 0, 0, 0, 0, 0, 0,
                            0, 0, 0, shell.Livewareintercom[i], shell.Livewareinterskill[i], shell.Livewareintersprvsn[i], shell.Livewareinterregact[i], 0, 0, 0, 0, 4, Convert.ToInt32(Session["UserId"]), Convert.ToInt32(Session["UserId"]));
                    }
                    for (i = 0; i < shell.Livewarephysical.Count(); i++)
                    {
                        gcaa.USP_INST_Shell_Analysis_Details(event_tbl_st[0].InvestigationEventAnalysisId, shell.Invfileid, 0, 0, 0, 0, 0, 0, 0,
                            0, 0, 0, 0, 0, 0, 0, shell.Livewarephysical[i], shell.Livewarepsychological[i], shell.Livewarewrkldmgmt[i], shell.Livewareexpqual[i], 5, Convert.ToInt32(Session["UserId"]), Convert.ToInt32(Session["UserId"]));
                    }
                    return Json(0);
                }
                else
                {
                    System.Data.Entity.Core.Objects.ObjectParameter invevanalysisid = new System.Data.Entity.Core.Objects.ObjectParameter("invevanalysis_id", typeof(Int32));
                    var result = gcaa.USP_INST_INVESTIGATION_EVENT_ANALYSIS(0, shell.Invfileid, 2, Convert.ToInt32(Session["UserId"]), invevanalysisid);

                    int i = 0;
                    for (i = 0; i < shell.Livewaresysproc.Count(); i++)
                    {
                        gcaa.USP_INST_Shell_Analysis_Details(Convert.ToInt32(invevanalysisid.Value), shell.Invfileid, shell.Livewaresysproc[i], shell.Livewaresystraining[i], 0, 0, 0, 0, 0,
                            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, Convert.ToInt32(Session["UserId"]), Convert.ToInt32(Session["UserId"]));
                    }
                    for (i = 0; i < shell.Livewarehardhardware.Count(); i++)
                    {
                        gcaa.USP_INST_Shell_Analysis_Details(Convert.ToInt32(invevanalysisid.Value), shell.Invfileid, 0, 0, shell.Livewarehardhardware[i], shell.Livewareinfods[i], shell.Livewaresoftwarehard[i], shell.Livewareotheroper[i], 0,
                            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, Convert.ToInt32(Session["UserId"]), Convert.ToInt32(Session["UserId"]));
                    }
                    for (i = 0; i < shell.Livewareenvphyenv.Count(); i++)
                    {
                        gcaa.USP_INST_Shell_Analysis_Details(Convert.ToInt32(invevanalysisid.Value), shell.Invfileid, 0, 0, 0, 0, 0, 0, shell.Livewareenvphyenv[i],
                            shell.LivewareenvPsycho[i], shell.Livewareenvcmpmgmt[i], shell.Livewareenvopertsk[i], 0, 0, 0, 0, 0, 0, 0, 0, 3, Convert.ToInt32(Session["UserId"]), Convert.ToInt32(Session["UserId"]));
                    }
                    for (i = 0; i < shell.Livewareintercom.Count(); i++)
                    {
                        gcaa.USP_INST_Shell_Analysis_Details(Convert.ToInt32(invevanalysisid.Value), shell.Invfileid, 0, 0, 0, 0, 0, 0, 0,
                            0, 0, 0, shell.Livewareintercom[i], shell.Livewareinterskill[i], shell.Livewareintersprvsn[i], shell.Livewareinterregact[i], 0, 0, 0, 0, 4, Convert.ToInt32(Session["UserId"]), Convert.ToInt32(Session["UserId"]));
                    }
                    for (i = 0; i < shell.Livewarephysical.Count(); i++)
                    {
                        gcaa.USP_INST_Shell_Analysis_Details(Convert.ToInt32(invevanalysisid.Value), shell.Invfileid, 0, 0, 0, 0, 0, 0, 0,
                            0, 0, 0, 0, 0, 0, 0, shell.Livewarephysical[i], shell.Livewarepsychological[i], shell.Livewarewrkldmgmt[i], shell.Livewareexpqual[i], 5, Convert.ToInt32(Session["UserId"]), Convert.ToInt32(Session["UserId"]));
                    }
                    return Json(0);
                }

            }
            return Json(0);

        }
        /// <summary>
        /// Save or update fishbone Analysis values 
        /// </summary> 
        /// 
        /// <param name="fishbn">Receive the Fish Bone Analysis object.</param>     
        public JsonResult FishboneAnalysis(FishBone fishbn)
        {

            if (Convert.ToInt32(Session["UserId"]) > 0)
            {
                var event_tbl_st = gcaa.InvestigationEventAnalysis.AsQueryable().Where(a => a.InvestigationId == fishbn.Invfileid && a.LKAnalysisId == 3).ToList();
                var fishbone_tbl_st = gcaa.InvestigationFishBoneAnalysisdetails.AsQueryable().Where(a => a.InvestigationFileId == fishbn.Invfileid).ToList();
                if (event_tbl_st.Count() > 0)
                {
                    if (fishbone_tbl_st.Count() > 0)
                    {
                        gcaa.USP_REMOVE_FISHBONE_DETAILS(fishbn.Invfileid);
                    }

                    gcaa.USP_INST_Fishbone_Analysis_Details(fishbone_tbl_st[0].InvestigationEventAnalysisId, fishbn.Invfileid, fishbn.mainfbupleft, fishbn.mainfbupmid,
                        fishbn.mainfbupright, fishbn.subfbup1, fishbn.subfbup2, fishbn.subfbup3, fishbn.subfbupmid1, fishbn.subfbupmid2, fishbn.subfbupmid3,
                        fishbn.subfbupright1, fishbn.subfbupright2, fishbn.subfbupright3, fishbn.mainfbmiddle, fishbn.mainfbdownleft, fishbn.mainfbdownmid, fishbn.mainfbdownright,
                        fishbn.subfbdownleft1, fishbn.subfbdownleft2, fishbn.subfbdownleft3, fishbn.subfbdownmid1, fishbn.subfbdownmid2, fishbn.subfbdownmid3,
                        fishbn.subfbdownright1, fishbn.subfbdownright2, fishbn.subfbdownright3, Convert.ToInt32(Session["UserId"]), Convert.ToInt32(Session["UserId"]));

                    return Json(0);
                }
                else
                {
                    System.Data.Entity.Core.Objects.ObjectParameter invevanalysisid = new System.Data.Entity.Core.Objects.ObjectParameter("invevanalysis_id", typeof(Int32));
                    var result = gcaa.USP_INST_INVESTIGATION_EVENT_ANALYSIS(0, fishbn.Invfileid, 3, Convert.ToInt32(Session["UserId"]), invevanalysisid);
                    gcaa.USP_INST_Fishbone_Analysis_Details(Convert.ToInt32(invevanalysisid.Value), fishbn.Invfileid, fishbn.mainfbupleft, fishbn.mainfbupmid,
                      fishbn.mainfbupright, fishbn.subfbup1, fishbn.subfbup2, fishbn.subfbup3, fishbn.subfbupmid1, fishbn.subfbupmid2, fishbn.subfbupmid3,
                      fishbn.subfbupright1, fishbn.subfbupright2, fishbn.subfbupright3, fishbn.mainfbmiddle, fishbn.mainfbdownleft, fishbn.mainfbdownmid, fishbn.mainfbdownright,
                      fishbn.subfbdownleft1, fishbn.subfbdownleft2, fishbn.subfbdownleft3, fishbn.subfbdownmid1, fishbn.subfbdownmid2, fishbn.subfbdownmid3,
                      fishbn.subfbdownright1, fishbn.subfbdownright2, fishbn.subfbdownright3, Convert.ToInt32(Session["UserId"]), Convert.ToInt32(Session["UserId"]));


                    return Json(0);
                }

            }
            return Json(0);

        }
        /// <summary>
        /// Save or update shell Analysis Liveware-System Support Interface
        /// </summary>         
        /// <param name="addopt">Receive the Analysis Options as object.</param>     
        [HttpPost]
        public JsonResult AddAnalysisopt(AnalysisOptionsAdd addopt)
        {
            try
            {

                if (Convert.ToInt32(Session["UserId"]) > 0)
                {
                    int add_org_influences_flag = 0;
                    int unsafe_supervision_flag = 0;
                    int precondition_flag = 0;
                    int unsafe_act_flag = 0;
                    if (addopt.add_org_influences == null || addopt.add_org_influences == string.Empty)
                    {
                        add_org_influences_flag = 0;
                    }
                    else
                    {
                        var addoptnexistornot_orginfl = gcaa.LKOrganizationalInfluences.Where(a => a.LKOrganizationalInfluences == addopt.add_org_influences).ToList();

                        if (addoptnexistornot_orginfl == null || addoptnexistornot_orginfl.Count() == 0)
                        {
                            add_org_influences_flag = 1;
                        }
                        else
                        {
                            add_org_influences_flag = 0;
                            return Json(2);
                        }
                    }
                    if (addopt.unsafe_supervision == null || addopt.unsafe_supervision == string.Empty)
                    {
                        unsafe_supervision_flag = 0;
                    }
                    else
                    {
                        var addoptnexistornot_unsafe_supervision = gcaa.LKUnsafeSupervisions.Where(a => a.LKUnsafeSupervisionName == addopt.unsafe_supervision).ToList();
                        if (addoptnexistornot_unsafe_supervision == null || addoptnexistornot_unsafe_supervision.Count() == 0)
                        {
                            unsafe_supervision_flag = 1;
                        }
                        else
                        {
                            unsafe_supervision_flag = 0;
                            return Json(2);
                        }
                    }
                    if (addopt.add_precondition == null || addopt.add_precondition == string.Empty)
                    {
                        precondition_flag = 0;
                    }
                    else
                    {
                        var addoptnexistornot_pre_condition = gcaa.LKPreConditions.Where(a => a.LKPreConditionsName == addopt.add_precondition).ToList();

                        if (addoptnexistornot_pre_condition == null || addoptnexistornot_pre_condition.Count() == 0)
                        {
                            precondition_flag = 1;
                        }
                        else
                        {
                            precondition_flag = 0;
                            return Json(2);
                        }
                    }

                    if (addopt.unsafe_act == null || addopt.unsafe_act == string.Empty)
                    {
                        unsafe_act_flag = 0;
                    }
                    else
                    {
                        var addoptnexistornot_unsafe_act = gcaa.LKUnsafeActs.Where(a => a.LKUnsafeActName == addopt.unsafe_act).ToList();

                        if (addoptnexistornot_unsafe_act == null || addoptnexistornot_unsafe_act.Count() == 0)
                        {
                            unsafe_act_flag = 1;
                        }
                        else
                        {
                            unsafe_act_flag = 0;
                            return Json(2);
                        }
                    }

                    gcaa.USP_ADD_ANALYSIS_OPTION(addopt.add_org_influences, addopt.unsafe_supervision, addopt.add_precondition, addopt.unsafe_act, add_org_influences_flag, unsafe_supervision_flag, precondition_flag, unsafe_act_flag);

                    return Json(0);
                }
                return Json(0);
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("IICController", "AddAnalysisopt", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
                return Json(1);
            }
        }
        /// <summary>
        ///  Save or update shell Analysis Liveware-System Support Interface
        /// </summary> 
        /// 
        /// <param name="ssiopt">Receive the Shell ssi Options as object.</param>     
        public JsonResult Addshellssiopt(ShellssiOption ssiopt)
        {
            try
            {

                if (Convert.ToInt32(Session["UserId"]) > 0)
                {
                    int add_procedures_flag = 0;
                    int add_training_flag = 0;

                    if (ssiopt.add_procedures == null || ssiopt.add_procedures == string.Empty)
                    {
                        add_procedures_flag = 0;
                    }
                    else
                    {
                        var addoptnexistornot_add_procedures = gcaa.LKLivewareSysProcedures.Where(a => a.LKLivewareSysProceduresName == ssiopt.add_procedures).ToList();

                        if (addoptnexistornot_add_procedures == null || addoptnexistornot_add_procedures.Count() == 0)
                        {
                            add_procedures_flag = 1;
                        }
                        else
                        {
                            add_procedures_flag = 0;
                            return Json(2);
                        }
                    }
                    if (ssiopt.add_training == null || ssiopt.add_training == string.Empty)
                    {
                        add_training_flag = 0;
                    }
                    else
                    {
                        var addoptnexistornot_add_training = gcaa.LKLivewareSysTrainings.Where(a => a.LKLivewareSysTrainingName == ssiopt.add_training).ToList();
                        if (addoptnexistornot_add_training == null || addoptnexistornot_add_training.Count() == 0)
                        {
                            add_training_flag = 1;
                        }
                        else
                        {
                            add_training_flag = 0;
                            return Json(2);
                        }
                    }

                    gcaa.USP_ADD_SHELL_SSI_OPTION(ssiopt.add_procedures, ssiopt.add_training, add_procedures_flag, add_training_flag);

                    return Json(0);
                }
                return Json(0);
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("IICController", "AddAnalysisopt", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
                return Json(1);
            }
        }
        /// <summary>
        /// Save or update shell Analysis Liveware-Hardware/Software Interface
        /// </summary> 
        /// 
        /// <param name="hsiopt">Receive the Shell hsi Options as object.</param>     
        public JsonResult Addshellhsiopt(ShellhsiOption hsiopt)
        {
            try
            {

                if (Convert.ToInt32(Session["UserId"]) > 0)
                {
                    int add_hardware_flag = 0;
                    int add_infodatasource_flag = 0;
                    int add_softfirmware_flag = 0;
                    int add_otheropermet_flag = 0;

                    if (hsiopt.add_hardware == null || hsiopt.add_hardware == string.Empty)
                    {
                        add_hardware_flag = 0;
                    }
                    else
                    {
                        var addoptnexistornot_add_hardware = gcaa.LKLivewareHardHardwares.Where(a => a.LKLivewareHardHardwareName == hsiopt.add_hardware).ToList();

                        if (addoptnexistornot_add_hardware == null || addoptnexistornot_add_hardware.Count() == 0)
                        {
                            add_hardware_flag = 1;
                        }
                        else
                        {
                            add_hardware_flag = 0;
                            return Json(2);
                        }
                    }
                    if (hsiopt.add_infodatasource == null || hsiopt.add_infodatasource == string.Empty)
                    {
                        add_infodatasource_flag = 0;
                    }
                    else
                    {
                        var addoptnexistornot_add_infodatasource = gcaa.LKLivewareHardInfoDatas.Where(a => a.LKLivewareHardInfoDataName == hsiopt.add_infodatasource).ToList();
                        if (addoptnexistornot_add_infodatasource == null || addoptnexistornot_add_infodatasource.Count() == 0)
                        {
                            add_infodatasource_flag = 1;
                        }
                        else
                        {
                            add_infodatasource_flag = 0;
                            return Json(2);
                        }
                    }
                    if (hsiopt.add_softfirmware == null || hsiopt.add_softfirmware == string.Empty)
                    {
                        add_softfirmware_flag = 0;
                    }
                    else
                    {
                        var addoptnexistornot_add_softfirmware = gcaa.LKLivewareHardFirmwares.Where(a => a.LKLivewareHardFirmwareName == hsiopt.add_softfirmware).ToList();
                        if (addoptnexistornot_add_softfirmware == null || addoptnexistornot_add_softfirmware.Count() == 0)
                        {
                            add_softfirmware_flag = 1;
                        }
                        else
                        {
                            add_softfirmware_flag = 0;
                            return Json(2);
                        }
                    }
                    if (hsiopt.add_otheropermet == null || hsiopt.add_otheropermet == string.Empty)
                    {
                        add_otheropermet_flag = 0;
                    }
                    else
                    {
                        var addoptnexistornot_add_otheropermet = gcaa.LKLivewareHardOtherOperationals.Where(a => a.LKLivewareHardOtherOperationalName == hsiopt.add_otheropermet).ToList();
                        if (addoptnexistornot_add_otheropermet == null || addoptnexistornot_add_otheropermet.Count() == 0)
                        {
                            add_otheropermet_flag = 1;
                        }
                        else
                        {
                            add_otheropermet_flag = 0;
                            return Json(2);
                        }
                    }

                    gcaa.USP_ADD_SHELL_HSI_OPTION(hsiopt.add_hardware, hsiopt.add_infodatasource, hsiopt.add_softfirmware, hsiopt.add_otheropermet, add_hardware_flag, add_infodatasource_flag, add_softfirmware_flag, add_otheropermet_flag);

                    return Json(0);
                }
                return Json(0);
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("IICController", "AddAnalysisopt", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
                return Json(1);
            }
        }
        /// <summary>
        /// Save or update shell Analysis Liveware-Environment Interface
        /// </summary> 
        /// 
        /// <param name="eiopt">Receive the Shell ei Options as object.</param>     
        public JsonResult Addshelleiopt(ShelleiOption eiopt)
        {
            try
            {

                if (Convert.ToInt32(Session["UserId"]) > 0)
                {
                    int add_phyenvrnmnt_flag = 0;
                    int add_Psychosocial_flag = 0;
                    int add_compmgmt_flag = 0;
                    int add_opertskdmnd_flag = 0;

                    if (eiopt.add_phyenvrnmnt == null || eiopt.add_phyenvrnmnt == string.Empty)
                    {
                        add_phyenvrnmnt_flag = 0;
                    }
                    else
                    {
                        var addoptnexistornot_add_phyenvrnmnt = gcaa.LKLivewareEnvPhysicals.Where(a => a.LKLivewareEnvPhysicalName == eiopt.add_phyenvrnmnt).ToList();

                        if (addoptnexistornot_add_phyenvrnmnt == null || addoptnexistornot_add_phyenvrnmnt.Count() == 0)
                        {
                            add_phyenvrnmnt_flag = 1;
                        }
                        else
                        {
                            add_phyenvrnmnt_flag = 0;
                            return Json(2);
                        }
                    }
                    if (eiopt.add_Psychosocial == null || eiopt.add_Psychosocial == string.Empty)
                    {
                        add_Psychosocial_flag = 0;
                    }
                    else
                    {
                        var addoptnexistornot_add_Psychosocial = gcaa.LKLivewareEnvPsychosocials.Where(a => a.LKLivewareEnvPsychosocialName == eiopt.add_Psychosocial).ToList();
                        if (addoptnexistornot_add_Psychosocial == null || addoptnexistornot_add_Psychosocial.Count() == 0)
                        {
                            add_Psychosocial_flag = 1;
                        }
                        else
                        {
                            add_Psychosocial_flag = 0;
                            return Json(2);
                        }
                    }
                    if (eiopt.add_compmgmt == null || eiopt.add_compmgmt == string.Empty)
                    {
                        add_compmgmt_flag = 0;
                    }
                    else
                    {
                        var addoptnexistornot_add_compmgmt = gcaa.LKLivewareEnvComMgmts.Where(a => a.LKLivewareEnvComMgmtName == eiopt.add_compmgmt).ToList();
                        if (addoptnexistornot_add_compmgmt == null || addoptnexistornot_add_compmgmt.Count() == 0)
                        {
                            add_compmgmt_flag = 1;
                        }
                        else
                        {
                            add_compmgmt_flag = 0;
                            return Json(2);
                        }
                    }
                    if (eiopt.add_opertskdmnd == null || eiopt.add_opertskdmnd == string.Empty)
                    {
                        add_opertskdmnd_flag = 0;
                    }
                    else
                    {
                        var addoptnexistornot_add_opertskdmnd = gcaa.LKLivewareEnvOperationaltsks.Where(a => a.LKLivewareEnvOperationaltskName == eiopt.add_opertskdmnd).ToList();
                        if (addoptnexistornot_add_opertskdmnd == null || addoptnexistornot_add_opertskdmnd.Count() == 0)
                        {
                            add_opertskdmnd_flag = 1;
                        }
                        else
                        {
                            add_opertskdmnd_flag = 0;
                            return Json(2);
                        }
                    }

                    gcaa.USP_ADD_SHELL_EI_OPTION(eiopt.add_phyenvrnmnt, eiopt.add_Psychosocial, eiopt.add_compmgmt, eiopt.add_opertskdmnd, add_phyenvrnmnt_flag, add_Psychosocial_flag, add_compmgmt_flag, add_opertskdmnd_flag);

                    return Json(0);
                }
                return Json(0);
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("IICController", "AddAnalysisopt", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
                return Json(1);
            }
        }
        /// <summary>
        /// Save or update shell Analysis Liveware-Liveware Interface
        /// </summary> 
        /// 
        /// <param name="liopt">Receive the Shell li Options as object.</param>     
        public JsonResult Addshellliopt(ShellliOption liopt)
        {
            try
            {

                if (Convert.ToInt32(Session["UserId"]) > 0)
                {
                    int add_communications_flag = 0;
                    int add_add_inteamSkills_flag = 0;
                    int add_supervision_flag = 0;
                    int add_RegulatoryActivities_flag = 0;

                    if (liopt.add_communications == null || liopt.add_communications == string.Empty)
                    {
                        add_communications_flag = 0;
                    }
                    else
                    {
                        var addoptnexistornot_add_communications = gcaa.LkLivewareInterfaceComms.Where(a => a.LkLivewareInterfaceCommName == liopt.add_communications).ToList();

                        if (addoptnexistornot_add_communications == null || addoptnexistornot_add_communications.Count() == 0)
                        {
                            add_communications_flag = 1;
                        }
                        else
                        {
                            add_communications_flag = 0;
                            return Json(2);
                        }
                    }
                    if (liopt.add_inteamSkills == null || liopt.add_inteamSkills == string.Empty)
                    {
                        add_add_inteamSkills_flag = 0;
                    }
                    else
                    {
                        var addoptnexistornot_add_inteamSkills = gcaa.LkLivewareInterfaceTmskls.Where(a => a.LkLivewareInterfaceTmsklName == liopt.add_inteamSkills).ToList();
                        if (addoptnexistornot_add_inteamSkills == null || addoptnexistornot_add_inteamSkills.Count() == 0)
                        {
                            add_add_inteamSkills_flag = 1;
                        }
                        else
                        {
                            add_add_inteamSkills_flag = 0;
                            return Json(2);
                        }
                    }
                    if (liopt.add_supervision == null || liopt.add_supervision == string.Empty)
                    {
                        add_supervision_flag = 0;
                    }
                    else
                    {
                        var addoptnexistornot_add_supervision = gcaa.LkLivewareInterfaceSupvsns.Where(a => a.LkLivewareInterfaceSupvsnName == liopt.add_supervision).ToList();
                        if (addoptnexistornot_add_supervision == null || addoptnexistornot_add_supervision.Count() == 0)
                        {
                            add_supervision_flag = 1;
                        }
                        else
                        {
                            add_supervision_flag = 0;
                            return Json(2);
                        }
                    }
                    if (liopt.add_RegulatoryActivities == null || liopt.add_RegulatoryActivities == string.Empty)
                    {
                        add_RegulatoryActivities_flag = 0;
                    }
                    else
                    {
                        var addoptnexistornot_add_RegulatoryActivities = gcaa.LkLivewareInterfaceRegacts.Where(a => a.LkLivewareInterfaceRegactName == liopt.add_RegulatoryActivities).ToList();
                        if (addoptnexistornot_add_RegulatoryActivities == null || addoptnexistornot_add_RegulatoryActivities.Count() == 0)
                        {
                            add_RegulatoryActivities_flag = 1;
                        }
                        else
                        {
                            add_RegulatoryActivities_flag = 0;
                            return Json(2);
                        }
                    }

                    gcaa.USP_ADD_SHELL_LI_OPTION(liopt.add_communications, liopt.add_inteamSkills, liopt.add_supervision, liopt.add_RegulatoryActivities, add_communications_flag, add_add_inteamSkills_flag, add_supervision_flag, add_RegulatoryActivities_flag);

                    return Json(0);
                }
                return Json(0);
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("IICController", "AddAnalysisopt", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
                return Json(1);
            }
        }
        /// <summary>
        /// Save or update shell Analysis Liveware 
        /// </summary> 
        /// 
        /// <param name="lwopt">Receive the Shell lw Options as object.</param>     
        public JsonResult Addshelllwopt(ShelllwOption lwopt)
        {
            try
            {

                if (Convert.ToInt32(Session["UserId"]) > 0)
                {
                    int add_physical_flag = 0;
                    int add_Psychological_flag = 0;
                    int add_wrkldmgmt_flag = 0;
                    int add_expqual_flag = 0;

                    if (lwopt.add_physical == null || lwopt.add_physical == string.Empty)
                    {
                        add_physical_flag = 0;
                    }
                    else
                    {
                        var addoptnexistornot_add_physical = gcaa.LkLivewarePhysicals.Where(a => a.LkLivewarePhysicalName == lwopt.add_physical).ToList();

                        if (addoptnexistornot_add_physical == null || addoptnexistornot_add_physical.Count() == 0)
                        {
                            add_physical_flag = 1;
                        }
                        else
                        {
                            add_physical_flag = 0;
                            return Json(2);
                        }
                    }
                    if (lwopt.add_Psychological == null || lwopt.add_Psychological == string.Empty)
                    {
                        add_Psychological_flag = 0;
                    }
                    else
                    {
                        var addoptnexistornot_add_Psychological = gcaa.LkLivewarePsychologicals.Where(a => a.LkLivewarePsychologicalName == lwopt.add_Psychological).ToList();
                        if (addoptnexistornot_add_Psychological == null || addoptnexistornot_add_Psychological.Count() == 0)
                        {
                            add_Psychological_flag = 1;
                        }
                        else
                        {
                            add_Psychological_flag = 0;
                            return Json(2);
                        }
                    }
                    if (lwopt.add_wrkldmgmt == null || lwopt.add_wrkldmgmt == string.Empty)
                    {
                        add_wrkldmgmt_flag = 0;
                    }
                    else
                    {
                        var addoptnexistornot_add_wrkldmgmt = gcaa.LkLivewarewrkldmgmts.Where(a => a.LkLivewarewrkldmgmtName == lwopt.add_wrkldmgmt).ToList();
                        if (addoptnexistornot_add_wrkldmgmt == null || addoptnexistornot_add_wrkldmgmt.Count() == 0)
                        {
                            add_wrkldmgmt_flag = 1;
                        }
                        else
                        {
                            add_wrkldmgmt_flag = 0;
                            return Json(2);
                        }
                    }
                    if (lwopt.add_expqual == null || lwopt.add_expqual == string.Empty)
                    {
                        add_expqual_flag = 0;
                    }
                    else
                    {
                        var addoptnexistornot_add_expqual = gcaa.LkLivewareExpQuals.Where(a => a.LkLivewareExpQualName == lwopt.add_expqual).ToList();
                        if (addoptnexistornot_add_expqual == null || addoptnexistornot_add_expqual.Count() == 0)
                        {
                            add_expqual_flag = 1;
                        }
                        else
                        {
                            add_expqual_flag = 0;
                            return Json(2);
                        }
                    }

                    gcaa.USP_ADD_SHELL_LW_OPTION(lwopt.add_physical, lwopt.add_Psychological, lwopt.add_wrkldmgmt, lwopt.add_expqual, add_physical_flag, add_Psychological_flag, add_wrkldmgmt_flag, add_expqual_flag);

                    return Json(0);
                }
                return Json(0);
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("IICController", "AddAnalysisopt", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
                return Json(1);
            }
        }
        /// <summary>
        ///  Auto fill shell analysis dropdown values
        /// </summary> 
        /// 
        public ActionResult getAnalysisdropdownvalues()
        {
            gcaa_ismEntities db = new gcaa_ismEntities();

            string[] result;

            using (gcaa_ismEntities gc = new gcaa_ismEntities())
            {

                var organizationinflnames = (gc.LKOrganizationalInfluences.ToList().OrderBy(x => x.LKOrganizationalInfluences).Select(a => new
                {
                    LKOrganizationalInfluencesId = a.LKOrganizationalInfluencesId,
                    LKOrganizationalInfluences = a.LKOrganizationalInfluences
                })).ToList();

                var unsafesupervision = (gc.LKUnsafeSupervisions.ToList().OrderBy(x => x.LKUnsafeSupervisionName).Select(a => new
                {
                    LKUnsafeSupervisionId = a.LKUnsafeSupervisionId,
                    LKUnsafeSupervisionName = a.LKUnsafeSupervisionName
                })).ToList();
                var precondition = (gc.LKPreConditions.ToList().OrderBy(x => x.LKPreConditionsName).Select(a => new
                {
                    LKPreConditionsId = a.LKPreConditionsId,
                    LKPreConditionsName = a.LKPreConditionsName
                })).ToList();
                var unsafeact = (gc.LKUnsafeActs.ToList().OrderBy(x => x.LKUnsafeActName).Select(a => new
                {
                    LKUnsafeActId = a.LKUnsafeActId,
                    LKUnsafeActName = a.LKUnsafeActName
                })).ToList();


                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                result = new string[] { javaScriptSerializer.Serialize(organizationinflnames), javaScriptSerializer.Serialize(unsafesupervision), javaScriptSerializer.Serialize(precondition), javaScriptSerializer.Serialize(unsafeact) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        ///  Auto fill shell analysis dropdown values
        /// </summary> 
        /// 
        public ActionResult getshellssidropdownvalues()
        {
            gcaa_ismEntities db = new gcaa_ismEntities();

            string[] result;

            using (gcaa_ismEntities gc = new gcaa_ismEntities())
            {

                var livewareprocnames = (gc.LKLivewareSysProcedures.ToList().OrderBy(x => x.LKLivewareSysProceduresName).Select(a => new
                {
                    LKLivewareSysProceduresId = a.LKLivewareSysProceduresId,
                    LKLivewareSysProceduresName = a.LKLivewareSysProceduresName
                })).ToList();

                var livewaretrainnames = (gc.LKLivewareSysTrainings.ToList().OrderBy(x => x.LKLivewareSysTrainingName).Select(a => new
                {
                    LKLivewareSysTrainingId = a.LKLivewareSysTrainingId,
                    LKLivewareSysTrainingName = a.LKLivewareSysTrainingName
                })).ToList();



                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                result = new string[] { javaScriptSerializer.Serialize(livewareprocnames), javaScriptSerializer.Serialize(livewaretrainnames) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        ///  Auto fill shell analysis dropdown values
        /// </summary> 
        /// 
        public ActionResult getshellshsidropdownvalues()
        {
            gcaa_ismEntities db = new gcaa_ismEntities();

            string[] result;

            using (gcaa_ismEntities gc = new gcaa_ismEntities())
            {

                var livewarehardhardware = (gc.LKLivewareHardHardwares.ToList().OrderBy(x => x.LKLivewareHardHardwareName).Select(a => new
                {
                    LKLivewareHardHardwareId = a.LKLivewareHardHardwareId,
                    LKLivewareHardHardwareName = a.LKLivewareHardHardwareName
                })).ToList();

                var livewarehardinfo = (gc.LKLivewareHardInfoDatas.ToList().OrderBy(x => x.LKLivewareHardInfoDataName).Select(a => new
                {
                    LKLivewareHardInfoDataId = a.LKLivewareHardInfoDataId,
                    LKLivewareHardInfoDataName = a.LKLivewareHardInfoDataName
                })).ToList();
                var livewarehardfirm = (gc.LKLivewareHardFirmwares.ToList().OrderBy(x => x.LKLivewareHardFirmwareName).Select(a => new
                {
                    LKLivewareHardFirmwareId = a.LKLivewareHardFirmwareId,
                    LKLivewareHardFirmwareName = a.LKLivewareHardFirmwareName
                })).ToList();
                var livewarehardoper = (gc.LKLivewareHardOtherOperationals.ToList().OrderBy(x => x.LKLivewareHardOtherOperationalName).Select(a => new
                {
                    LKLivewareHardOtherOperationalId = a.LKLivewareHardOtherOperationalId,
                    LKLivewareHardOtherOperationalName = a.LKLivewareHardOtherOperationalName
                })).ToList();



                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                result = new string[] { javaScriptSerializer.Serialize(livewarehardhardware), javaScriptSerializer.Serialize(livewarehardinfo), javaScriptSerializer.Serialize(livewarehardfirm), javaScriptSerializer.Serialize(livewarehardoper) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        ///  Auto fill shell analysis dropdown values
        /// </summary> 
        /// 
        public ActionResult getshelleidropdownvalues()
        {
            gcaa_ismEntities db = new gcaa_ismEntities();

            string[] result;

            using (gcaa_ismEntities gc = new gcaa_ismEntities())
            {

                var livewareenvphys = (gc.LKLivewareEnvPhysicals.ToList().OrderBy(x => x.LKLivewareEnvPhysicalName).Select(a => new
                {
                    LKLivewareEnvPhysicalId = a.LKLivewareEnvPhysicalId,
                    LKLivewareEnvPhysicalName = a.LKLivewareEnvPhysicalName
                })).ToList();

                var livewareenvpsycho = (gc.LKLivewareEnvPsychosocials.ToList().OrderBy(x => x.LKLivewareEnvPsychosocialName).Select(a => new
                {
                    LKLivewareEnvPsychosocialId = a.LKLivewareEnvPsychosocialId,
                    LKLivewareEnvPsychosocialName = a.LKLivewareEnvPsychosocialName
                })).ToList();
                var livewwareenvmgmts = (gc.LKLivewareEnvComMgmts.ToList().OrderBy(x => x.LKLivewareEnvComMgmtName).Select(a => new
                {
                    LKLivewareEnvComMgmtId = a.LKLivewareEnvComMgmtId,
                    LKLivewareEnvComMgmtName = a.LKLivewareEnvComMgmtName
                })).ToList();
                var livewareenvopertsk = (gc.LKLivewareEnvOperationaltsks.ToList().OrderBy(x => x.LKLivewareEnvOperationaltskName).Select(a => new
                {
                    LKLivewareEnvOperationaltskId = a.LKLivewareEnvOperationaltskId,
                    LKLivewareEnvOperationaltskName = a.LKLivewareEnvOperationaltskName
                })).ToList();



                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                result = new string[] { javaScriptSerializer.Serialize(livewareenvphys), javaScriptSerializer.Serialize(livewareenvpsycho), javaScriptSerializer.Serialize(livewwareenvmgmts), javaScriptSerializer.Serialize(livewareenvopertsk) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        ///  Auto fill shell analysis dropdown values
        /// </summary> 
        /// 
        public ActionResult getshell_llidropdownvalues()
        {
            gcaa_ismEntities db = new gcaa_ismEntities();

            string[] result;

            using (gcaa_ismEntities gc = new gcaa_ismEntities())
            {

                var livewateintercomm = (gc.LkLivewareInterfaceComms.ToList().OrderBy(x => x.LkLivewareInterfaceCommName).Select(a => new
                {
                    LkLivewareInterfaceCommId = a.LkLivewareInterfaceCommId,
                    LkLivewareInterfaceCommName = a.LkLivewareInterfaceCommName
                })).ToList();

                var livewareintertmskil = (gc.LkLivewareInterfaceTmskls.ToList().OrderBy(x => x.LkLivewareInterfaceTmsklName).Select(a => new
                {
                    LkLivewareInterfaceTmsklId = a.LkLivewareInterfaceTmsklId,
                    LkLivewareInterfaceTmsklName = a.LkLivewareInterfaceTmsklName
                })).ToList();
                var livewareintersprvsn = (gc.LkLivewareInterfaceSupvsns.ToList().OrderBy(x => x.LkLivewareInterfaceSupvsnName).Select(a => new
                {
                    LkLivewareInterfaceSupvsnId = a.LkLivewareInterfaceSupvsnId,
                    LkLivewareInterfaceSupvsnName = a.LkLivewareInterfaceSupvsnName
                })).ToList();
                var livewareinteropertsk = (gc.LkLivewareInterfaceRegacts.ToList().OrderBy(x => x.LkLivewareInterfaceRegactName).Select(a => new
                {
                    LkLivewareInterfaceRegactId = a.LkLivewareInterfaceRegactId,
                    LkLivewareInterfaceRegactName = a.LkLivewareInterfaceRegactName
                })).ToList();



                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                result = new string[] { javaScriptSerializer.Serialize(livewateintercomm), javaScriptSerializer.Serialize(livewareintertmskil), javaScriptSerializer.Serialize(livewareintersprvsn), javaScriptSerializer.Serialize(livewareinteropertsk) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Auto fill shell dropdown values
        /// </summary> 
        /// 
        public ActionResult getshell_lwdropdownvalues()
        {
            gcaa_ismEntities db = new gcaa_ismEntities();

            string[] result;

            using (gcaa_ismEntities gc = new gcaa_ismEntities())
            {

                var livewarephysical = (gc.LkLivewarePhysicals.ToList().OrderBy(x => x.LkLivewarePhysicalName).Select(a => new
                {
                    LkLivewarePhysicalId = a.LkLivewarePhysicalId,
                    LkLivewarePhysicalName = a.LkLivewarePhysicalName
                })).ToList();

                var livewarepsycho = (gc.LkLivewarePsychologicals.ToList().OrderBy(x => x.LkLivewarePsychologicalName).Select(a => new
                {
                    LkLivewarePsychologicalId = a.LkLivewarePsychologicalId,
                    LkLivewarePsychologicalName = a.LkLivewarePsychologicalName
                })).ToList();
                var livewarewrlldmfmt = (gc.LkLivewarewrkldmgmts.ToList().OrderBy(x => x.LkLivewarewrkldmgmtName).Select(a => new
                {
                    LkLivewarewrkldmgmtId = a.LkLivewarewrkldmgmtId,
                    LkLivewarewrkldmgmtName = a.LkLivewarewrkldmgmtName
                })).ToList();
                var livewareexpquals = (gc.LkLivewareExpQuals.ToList().OrderBy(x => x.LkLivewareExpQualName).Select(a => new
                {
                    LkLivewareExpQualId = a.LkLivewareExpQualId,
                    LkLivewareExpQualName = a.LkLivewareExpQualName
                })).ToList();



                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                result = new string[] { javaScriptSerializer.Serialize(livewarephysical), javaScriptSerializer.Serialize(livewarepsycho), javaScriptSerializer.Serialize(livewarewrlldmfmt), javaScriptSerializer.Serialize(livewareexpquals) };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Auto fill default values of drop down control
        /// </summary> 
        /// 
        public ActionResult jamesreason()
        {
            var org_infl = gcaa.LKOrganizationalInfluences.ToList().OrderBy(x => x.LKOrganizationalInfluences);
            SelectList org_infl_list = new SelectList(org_infl, "LKOrganizationalInfluencesId", "LKOrganizationalInfluences");
            ViewBag.org_infl = org_infl_list;
            var unsafe_svision = gcaa.LKUnsafeSupervisions.ToList().OrderBy(x => x.LKUnsafeSupervisionName);
            SelectList unsafe_svision_list = new SelectList(unsafe_svision, "LKUnsafeSupervisionId", "LKUnsafeSupervisionName");
            ViewBag.unsafe_svision_list = unsafe_svision_list;
            var pre_cond = gcaa.LKPreConditions.ToList().OrderBy(x => x.LKPreConditionsName);
            SelectList pre_cond_list = new SelectList(pre_cond, "LKPreConditionsId", "LKPreConditionsName");
            ViewBag.pre_cond_list = pre_cond_list;
            var unsafe_act = gcaa.LKUnsafeActs.ToList().OrderBy(x => x.LKUnsafeActName);
            SelectList unsafe_act_list = new SelectList(unsafe_act, "LKUnsafeActId", "LKUnsafeActName");
            ViewBag.unsafe_act_list = unsafe_act_list;
            return View();
        }
        /// <summary>
        /// Fish bone
        /// </summary> 
        /// 
        public ActionResult Fishbone()
        {
            return View();
        }
        /// <summary>
        /// Check if Root Analysis already exist or not 
        /// </summary> 
        /// 
        public JsonResult RootAnalysisCheck(/*int taskgroupid, int taskassignmentid, int eventuniqno,*/ int investfileid)
        {
            //string eventunique = "E" + Convert.ToString(eventuniqno);

            //var results = gcaa.InvestigationEventAnalysis.AsQueryable().Where(a => a.InvestigationId == investfileid && a.InvestigationTaskGroupId == taskgroupid && a.InvestigationTaskGroupEventUniqueNumber == eventunique).FirstOrDefault();

            var results = gcaa.InvestigationEventAnalysis.AsQueryable().Where(a => a.InvestigationId == investfileid).FirstOrDefault();

            if (results == null)
            {
                return Json(0);
            }
            else
            {
                return Json(1);
            }
        }
        /// <summary>
        /// Defn_Inv_Scope
        /// </summary> 
        /// 
        public ActionResult Defn_Inv_Scope()
        {
            try
            {
                var Inv_Scope = new IIC()
                {
                    //Inv = gcaa.(1).ToList(),
                    Grp = gcaa.USP_VIEW_TASKGROUPS().ToList()
                };

                return View(Inv_Scope);
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("IICController", "Defn_Inv_Scope", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }
            //return View();
        }
        /// <summary>
        /// Redirect IIC Tasks view page 
        /// </summary> 
        /// 
        /// <param name="id">Receive the investigation file id.</param>     
        public ActionResult IICTasks(int id = 0)
        {
            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 3 || Convert.ToInt32(Session["RoleId"]) == 6 || Convert.ToInt32(Session["RoleId"]) == 7)
                {
                        var InvClosureStatusInfo = gcaa.InvestigationFiles.Where(a => a.InvestigationFileID == id).FirstOrDefault();

                        var InvestigationClosed = gcaa.InvestigationClosureDetails.Where(a => a.InvestigationFileID == id &&

                                                   a.InvestigationClosedBy == 0).FirstOrDefault();

                        int InvestigationClosedBy = 1; string ClosureReason = string.Empty;

                        if (InvestigationClosed != null)
                        {
                            InvestigationClosedBy = InvestigationClosed.InvestigationClosedBy; // for hide close investigation save button

                            ClosureReason = Convert.ToString(InvestigationClosed.InvestigationClosureReason);
                        }

                        var Inv_Scope = new IIC()
                        {
                            Inv = gcaa.USP_VIEW_INVESTIGATION_FORM35(id).ToList(),
                            Inv_AircraftDet = gcaa.USP_VIEW_INVESTIGATION_AIRCRAFT_DETAIL_FORM35(id).ToList(),
                            Attachment = gcaa.USP_VIEW_INVESTIGATION_ATTACHMENTFILE(id).ToList(),
                            IIC_Tasks = gcaa.USP_VIEW_IIC_TASKS(id).ToList(),
                            INV_TM = gcaa.USP_VIEW_TEAMMEMBER(id).ToList(),
                            InvestigationFileID = id,
                            InvestigationClosureStatusId = InvClosureStatusInfo.LKInvestigationStatusTypeID,
                            InvestigationClosedBy = InvestigationClosedBy,
                            InvestigationClosureReason = ClosureReason,
                        };

                        var invInfo = gcaa.USP_VIEW_INVESTIGATION_FORM35(id).FirstOrDefault();
                        TempData["INV_STATUS"] = invInfo.LKInvestigationStatusTypeID;
                        var Notify_ID = gcaa.USP_VIEW_INVESTIGATION_FORM35(id).FirstOrDefault();
                        TempData["PageHead"] = Notify_ID.InvestigationNumber;
                        TempData["InvFileID"] = Notify_ID.InvestigationFileID;
                        Session["InvFileID"] = Notify_ID.InvestigationFileID;
                        TempData["lf_menu"] = "1";
                        TempData["active_general_info_iic"] = "iic";
                        Inv_Scope.InvestigationFileNo = Notify_ID.InvestigationNumber;

                        var acc_class = gcaa.USP_GET_MASTERVALUES(1).ToList();
                        ViewBag.AccID_ClassList = acc_class;
                        var Air_Class = gcaa.USP_GET_MASTERVALUES(2);
                        ViewBag.Air_ClassList = Air_Class.ToList();
                        var USE = gcaa.USP_GET_MASTERVALUES(3).ToList();
                        ViewBag.USEList = USE;
                        var INVOLVE = gcaa.USP_GET_MASTERVALUES(4).ToList();
                        ViewBag.INVOLVEList = INVOLVE;
                        var operINVOLVE = gcaa.USP_GET_MASTERVALUES(22).ToList();
                        ViewBag.oper_INVOLVEList = operINVOLVE;
                        var YesNo = gcaa.USP_GET_MASTERVALUES(17).ToList();
                        ViewBag.YesNoList = YesNo;
                        var DI_Sugg = gcaa.USP_GET_MASTERVALUES(5);
                        ViewBag.DI_SuggList = DI_Sugg.ToList();
                        var DI_Fur_Sugg = gcaa.USP_GET_MASTERVALUES(6);
                        ViewBag.DI_Fur_SuggList = DI_Fur_Sugg.ToList();
                        var Incident_Type = gcaa.USP_GET_INCIDENTTYPE().ToList();
                        ViewBag.Incident_TypeList = Incident_Type;
                        var Investigation_Type = gcaa.USP_GET_MASTERVALUES(7);
                        SelectList InvestigationType = new SelectList(Investigation_Type, "ID", "LKVALUE");
                        ViewBag.Investigation_Type = Investigation_Type;
                        if (Inv_Scope.INV_TM.Count() > 0)
                        {
                            TempData["invscope_enable/disable"] = "enable";
                        }
                        else
                        {
                            TempData["invscope_enable/disable"] = "disable";
                        }
                        return View(Inv_Scope);
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("IICController", "IICTasks", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }
            //return View();
        }
        /// <summary>
        /// redirect to iic task
        /// </summary> 
        /// 
        /// <param name="id">Receive the investigation file id.</param>     
        public ActionResult View_Inv_Prg(int id = 0)
        {
            var Inv_Prg = new IIC();
            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 3 || Convert.ToInt32(Session["RoleId"]) == 6 || Convert.ToInt32(Session["RoleId"]) == 7)
                {
                    var CheckInvPermission = gcaa.InvestigationIICAssignments.AsEnumerable().Where(a => a.InvestigationFileID == id).FirstOrDefault();

                    if ((CheckInvPermission.IICAssigned == Convert.ToInt32(Session["UserId"]) &&

                                           CheckInvPermission.InvestigationFileID == id) || Convert.ToInt32(Session["RoleId"]) == 3 || Convert.ToInt32(Session["RoleId"]) == 6 || Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 7)
                    {
                        if (id > 0)
                        {
                            int OverallProgress = 0;

                            var InvOverallProgress = gcaa.InvestigationTaskDetails.AsEnumerable().Where(a => a.InvestigationFileID == id).ToList();

                            if (InvOverallProgress != null)
                            {
                                decimal OverallProgresssum = Convert.ToInt32((InvOverallProgress.Sum(a => a.ProgressInPercentage)));

                                decimal OverallProgressCount = InvOverallProgress.Count() * 100;

                                if (OverallProgresssum > 0)
                                {
                                    string perc = Convert.ToString((OverallProgresssum / OverallProgressCount) * 100);

                                    OverallProgress = Convert.ToInt32(Math.Round(Convert.ToDecimal(perc)));
                                }
                            }

                            Inv_Prg = new IIC()
                            {
                                Inv = gcaa.USP_VIEW_INVESTIGATION_FORM35(id).ToList(),
                                Inv_AircraftDet = gcaa.USP_VIEW_INVESTIGATION_AIRCRAFT_DETAIL_FORM35(id).ToList(),
                                Attachment = gcaa.USP_VIEW_INVESTIGATION_ATTACHMENTFILE(id).ToList(),
                                IIC_Tasks = gcaa.USP_VIEW_IIC_TASKS(id).ToList(),
                                Inv_Group_Scope = gcaa.USP_VIEW_INVESTIGATION_SCOPE_DEFINITION_INVESTIGATIONID(id).ToList(),
                                inv_taskDetails = gcaa.USP_VIEW_INVESTIGATION_TASKDETAILS_INVESTIGATIONID(id).ToList(),
                                OverallProgressPercentage = OverallProgress
                            };
                            var InvInfo = gcaa.USP_VIEW_INVESTIGATION_FORM35(id).FirstOrDefault();
                            TempData["PageHead"] = InvInfo.InvestigationNumber;
                            TempData["InvId"] = InvInfo.InvestigationFileID;
                            TempData["InvFileID"] = InvInfo.InvestigationFileID;
                            TempData["BackPage"] = "yes";
                            TempData["active_url_View_Inv_Prg"] = "tm";
                            TempData["active_url_View_Inv_Prg_iic"] = "iic";
                            TempData["lf_menu"] = "1";
                        }
                    }
                    else
                    {
                        return RedirectToAction("Error");
                    }
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("IICController", "View_Inv_Prg", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }
            return View(Inv_Prg);
        }
        /// <summary>
        /// Redirect to Correspondences view page
        /// </summary> 
        /// 
        /// <param name="search">Receive the search key value.</param>     
        /// <param name="page">Receive the correspondence view page no.</param>     
        /// <param name="id">Receive the investigation file id.</param>     
        public ActionResult Correspondences(string search, int? page, int id = 0)
        {
            var Corres = new IIC();
            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 3 || Convert.ToInt32(Session["RoleId"]) == 6 || Convert.ToInt32(Session["RoleId"]) == 7)
                {
                    if (page == null)
                    {
                        ViewBag.serial_nbr = 0;
                    }
                    else
                    {
                        ViewBag.serial_nbr = 10 * (page - 1);
                    }
                    if (id > 0)
                    {
                        Corres = new IIC()
                        {
                            Inv = gcaa.USP_VIEW_INVESTIGATION_FORM35(id).ToList(),
                            Corres = gcaa.USP_VIEW_ALL_CORRESPONDENCES(id).ToList()
                        };
                        var InvInfo = gcaa.USP_VIEW_INVESTIGATION_FORM35(id).FirstOrDefault();
                        var Corre_Type = gcaa.LKCorrespondenceTypes.ToList();
                        SelectList Corre_Typelist = new SelectList(Corre_Type, "LKCorrespondenceTypeID", "LKCorrespondenceTypeName");
                        ViewBag.corresp_Lsit = Corre_Typelist;
                        TempData["PageHead"] = InvInfo.InvestigationNumber;
                        Session["InvNum"] = InvInfo.InvestigationNumber;
                        TempData["InvId"] = InvInfo.InvestigationFileID;
                        TempData["InvFileID"] = InvInfo.InvestigationFileID;
                        TempData["BackPage"] = "yes";
                        TempData["active_url_CorrespondenceManagement"] = "tm";
                        TempData["active_url_CorrespondenceManagement_iic"] = "iic";
                        TempData["lf_menu"] = "1";
                        var MailType = new List<SelectListItem>();
                        MailType.Add(new SelectListItem() { Text = "All Correspondences", Value = "N/A" });
                        MailType.Add(new SelectListItem() { Text = "Sent", Value = "Sent" });
                        MailType.Add(new SelectListItem() { Text = "Received", Value = "Received" });
                        var DAAIApproval = new List<SelectListItem>();
                        DAAIApproval.Add(new SelectListItem() { Text = "Select", Value = "N/A" });
                        DAAIApproval.Add(new SelectListItem() { Text = "Yes", Value = "true" });
                        DAAIApproval.Add(new SelectListItem() { Text = "No", Value = "false" });
                        SelectList DAAIApproval_List = new SelectList(DAAIApproval, "VALUE", "Text");
                        ViewBag.DAAIApprovalList = DAAIApproval_List;
                        SelectList MailType_List = new SelectList(MailType, "VALUE", "Text");
                        ViewBag.MailTypeList = MailType_List;
                        TempData["INV_STATUS"] = InvInfo.LKInvestigationStatusTypeID;
                        //Check the userDetails
                        Corres.InvestigationFileID = InvInfo.InvestigationFileID;
                        int checkInvPermission = gcaa.InvestigationIICAssignments.AsEnumerable().Where(a => a.InvestigationFileID == InvInfo.InvestigationFileID).FirstOrDefault().InvestigationFileID;
                        List<InvestigationTeamMember> teammembers = gcaa.InvestigationTeamMembers.Where(x => x.InvestigationFileID == checkInvPermission).ToList();
                        foreach (var team in teammembers)
                        {
                            if (team.TeamMemberID == Convert.ToInt32(Session["UserId"]))
                            {
                                Corres.invAssigned = team.TeamMemberID;
                            }
                        }

                    }
                    ViewBag.page_no = page;
                    ViewBag.count_pagecontent = Corres.Corres.Count();
                    ViewBag.Corres = Corres.Corres;
                    //Session["Corres"] = Corres.Corres;
                    Session["Inv_Corres"] = Corres.Corres.ToList();
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("IICController", "Correspondences", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }
            return View(Corres);
        }
        /// <summary>
        /// Save correspondence details to database table
        /// </summary> 
        /// 
        /// <param name="corres">Receive the Correspondences details as object.</param>     
        /// <param name="page">Receive the correspondence view page no.</param>     
        /// <param name="id">Receive the investigation file id.</param>   
        [HttpPost]
        public ActionResult Correspondences(IIC corres, int? page, int id)
        {
            var Corres = new IIC();

            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 3 || Convert.ToInt32(Session["RoleId"]) == 6 || Convert.ToInt32(Session["RoleId"]) == 7)
                {
                    string searchText = corres.CorrespondenceControlNumber;

                    //bool searchDAAIApproval = false;

                    //if (searchText != string.Empty && searchText != null)
                    //{
                    //    if (searchText.ToLower() == "No")
                    //        searchDAAIApproval = false;
                    //    if (searchText.ToLower() == "yes")
                    //        searchDAAIApproval = true;
                    //}
                    if (page == null)
                    {
                        ViewBag.serial_nbr = 0;
                    }
                    else
                    {
                        ViewBag.serial_nbr = 10 * (page - 1);
                    }
                    if (id > 0)
                    {
                        Corres = new IIC()
                        {
                            Inv = gcaa.USP_VIEW_INVESTIGATION_FORM35(id).ToList(),
                        };
                        var InvInfo = gcaa.USP_VIEW_INVESTIGATION_FORM35(id).FirstOrDefault();
                        var Corre_Type = gcaa.LKCorrespondenceTypes.ToList();
                        SelectList Corre_Typelist = new SelectList(Corre_Type, "LKCorrespondenceTypeID", "LKCorrespondenceTypeName");
                        ViewBag.corresp_Lsit = Corre_Typelist;
                        TempData["PageHead"] = InvInfo.InvestigationNumber;
                        TempData["InvId"] = InvInfo.InvestigationFileID;
                        TempData["InvFileID"] = InvInfo.InvestigationFileID;
                        TempData["BackPage"] = "yes";
                        TempData["active_url_CorrespondenceManagement"] = "tm";
                        TempData["lf_menu"] = "1";
                        ViewBag.page_no = page;
                        var MailType = new List<SelectListItem>();
                        MailType.Add(new SelectListItem() { Text = "All Correspondences", Value = "N/A" });
                        MailType.Add(new SelectListItem() { Text = "Sent", Value = "Sent" });
                        MailType.Add(new SelectListItem() { Text = "Received", Value = "Received" });
                        var DAAIApproval = new List<SelectListItem>();
                        DAAIApproval.Add(new SelectListItem() { Text = "Select", Value = "N/A" });
                        DAAIApproval.Add(new SelectListItem() { Text = "Yes", Value = "true" });
                        DAAIApproval.Add(new SelectListItem() { Text = "No", Value = "false" });
                        SelectList DAAIApproval_List = new SelectList(DAAIApproval, "VALUE", "Text");
                        ViewBag.DAAIApprovalList = DAAIApproval_List;
                        SelectList MailType_List = new SelectList(MailType, "VALUE", "Text");
                        ViewBag.MailTypeList = MailType_List;
                        Corres.Corres = (List<USP_VIEW_ALL_CORRESPONDENCES_Result>)Session["Inv_Corres"];
                        if (corres.LKCorrespondenceMessageTypeID != null)
                            Corres.Corres = Corres.Corres.Where(x => x.LKCorrespondenceMessageTypeID == corres.LKCorrespondenceMessageTypeID).ToList();
                        if (Convert.ToString(corres.DAAIApproved) != "N/A" && corres.DAAIApproved != null)
                            Corres.Corres = Corres.Corres.Where(x => x.DAAIApprovalRequired == Convert.ToBoolean(corres.DAAIApproved)).ToList();
                        if (corres.CorrespondenceControlNumber != null && corres.CorrespondenceControlNumber != string.Empty)
                        {
                            Corres.Corres = Corres.Corres.Where(x => string.Equals(x.CorrespondenceControlNumber, searchText, StringComparison.CurrentCultureIgnoreCase) || x.CorrespondenceControlNumber.ToLower().Contains(searchText.ToLower()) || string.Equals(x.Subject, searchText, StringComparison.CurrentCultureIgnoreCase) || x.Subject.ToLower().Contains(searchText.ToLower()) || string.Equals(x.CorrespondenceContent, searchText, StringComparison.CurrentCultureIgnoreCase) || x.CorrespondenceContent.ToLower().Contains(searchText.ToLower())).ToList();
                        }
                        //commented since search filter  is not working while paging
                        //  ViewBag.count_pagecontent = Corres.Corres.Count();
                        ViewBag.count_pagecontent = 1;
                        TempData["INV_STATUS"] = InvInfo.LKInvestigationStatusTypeID;
                        TempData["SearchKey"] = searchText;
                    }
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("IICController", "Correspondences::POST", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }
            return View(Corres);
        }
        /// <summary>
        /// Redirect to add new correspondence based on the new investigation
        /// </summary> 
        /// 
        /// <param name="invid">Receive the investigation file id.</param>   
        public ActionResult AddCorrespondence(int invid = 0)
        {
            var Corres = new IIC();
            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 3 || Convert.ToInt32(Session["RoleId"]) == 6 || Convert.ToInt32(Session["RoleId"]) == 7)

                {
                    if (invid > 0)
                    {
                        Corres = new IIC()
                        {
                            Inv = gcaa.USP_VIEW_INVESTIGATION_FORM35(invid).ToList(),
                            Corres = gcaa.USP_VIEW_ALL_CORRESPONDENCES(invid).ToList()
                        };
                    }

                    InvFileID = invid;

                    int userID = Convert.ToInt32(Session["UserId"]);
                    var userDet = gcaa.Users.Where(a => a.UserID == userID).FirstOrDefault();
                    ViewBag.IICmailid = userDet.EmailAddress;
                    var InvInfo = gcaa.USP_VIEW_INVESTIGATION_FORM35(invid).FirstOrDefault();
                    string filenum = InvInfo.InvestigationNumber.Split('/')[1];
                    var CONTROL_NUMER = gcaa.USP_GEN_CORRESPONDENCE_CONTROL_NUMBER(invid, Convert.ToInt32(filenum)).FirstOrDefault();
                    TempData["CONTROL_NUMER"] = CONTROL_NUMER.CONTROL_NUM;
                    ViewBag.CORRES_MESS_ID = CONTROL_NUMER.INV_FILE_NUMBER;
                    var Corre_Type = gcaa.LKCorrespondenceTypes.ToList();
                    SelectList Corre_Typelist = new SelectList(Corre_Type, "LKCorrespondenceTypeID", "LKCorrespondenceTypeName");
                    ViewBag.corresp_Lsit = Corre_Typelist;
                    var Roles = gcaa.LKCorrespondencePersonRoles.ToList();
                    SelectList Roles_List = new SelectList(Roles, "LKCorrespondencePersonRoleID", "LKCorrespondencePersonRoleName");
                    ViewBag.RolesList = Roles_List;
                    var DAAIApproval = new List<SelectListItem>();
                    DAAIApproval.Add(new SelectListItem() { Text = "Select", Value = "N/A" });
                    DAAIApproval.Add(new SelectListItem() { Text = "Yes", Value = "true" });
                    DAAIApproval.Add(new SelectListItem() { Text = "No", Value = "false" });
                    SelectList DAAIApproval_List = new SelectList(DAAIApproval, "VALUE", "Text");
                    ViewBag.DAAIApprovalList = DAAIApproval_List;
                    var DueDate = new List<SelectListItem>();
                    DueDate.Add(new SelectListItem() { Text = "Select", Value = "N/A" });
                    DueDate.Add(new SelectListItem() { Text = "30", Value = "30" });
                    DueDate.Add(new SelectListItem() { Text = "60", Value = "60" });
                    DueDate.Add(new SelectListItem() { Text = "90", Value = "90" });
                    DueDate.Add(new SelectListItem() { Text = "120", Value = "120" });
                    SelectList DueDate_List = new SelectList(DueDate, "VALUE", "Text");
                    ViewBag.DueDateList = DueDate_List;
                    var MailType = new List<SelectListItem>();
                    MailType.Add(new SelectListItem() { Text = "Select", Value = "N/A" });
                    MailType.Add(new SelectListItem() { Text = "Sent", Value = "Sent" });
                    MailType.Add(new SelectListItem() { Text = "Received", Value = "Received" });
                    SelectList MailType_List = new SelectList(MailType, "VALUE", "Text");
                    ViewBag.MailTypeList = MailType_List;
                    TempData["PageHead"] = InvInfo.InvestigationNumber;
                    TempData["InvId"] = InvInfo.InvestigationFileID;
                    TempData["InvFileID"] = InvInfo.InvestigationFileID;
                    TempData["BackPage"] = "corr_mg";
                    TempData["active_url_CorrespondenceManagement"] = "tm";
                    TempData["active_url_CorrespondenceManagement_iic"] = "iic";

                    TempData["lf_menu"] = "1";
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("IICController", "AddCorrespondence", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }
            return View(Corres);
        }
        /// <summary>
        /// Save the correspondence based on the new investigation
        /// </summary> 
        /// 
        /// <param name="corres">Receive the Correspondences details as object.</param>     
        [HttpPost]
        public ActionResult AddCorrespondence(IIC corres)
        {
            bool mailSent = false;
            string siteURL = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
            try
            {
                saveFilestoSharePoint(Request.Files, corres.CorrespondenceControlNumber);
                if (Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 3 || Convert.ToInt32(Session["RoleId"]) == 7)
                {
                    if (Convert.ToString(corres.DAAIApprovalRequired).ToLower() == "false")
                        corres.DAAIApproved = null;
                    else
                        corres.DAAIApproved = "Pending";
                    if (Convert.ToInt32(Session["UserId"]) > 0)
                    {
                        var msg_ID = gcaa.USP_ADD_CORRESPONDENCE(corres.CorrespondenceControlNumber, corres.InvestigationFileID, corres.CorrespondenceInitiatedDate, corres.Subject, corres.CorrespondenceTags, Convert.ToInt32(Session["UserId"]), Convert.ToInt32(Session["UserId"]),
                                              corres.CorrespondenceID, corres.CorrespondenceInitiatedDate, corres.CorrespondenceMessageFrom, corres.LKCorrespondenceMessageFromRoleID, corres.CorrespondenceMessageOtherRoleName, corres.CorrespondenceDueDate, corres.Corresrespnsdays,
                                              corres.LKCorrespondenceMessageTypeID, corres.LKCorrespondenceMessageOtherTypeName, corres.DAAIApprovalRequired, corres.DAAIApproved, corres.CorrespondenceContent,
                                              null, null, null, corres.Corr_Msg_Type, corres.SR_Number,corres.SR_ID).FirstOrDefault();
                        for (int i = 0; i < SharepointfilePath.Length; i++)
                        {
                            if (SharepointfilePath[i] != null)
                                gcaa.USP_CORRES_ATTACHMENTS("insert", Convert.ToInt32(msg_ID.CorrespondenceMessageID), DocumentTypeId[i], SharepointfilePath[i], SharepointAnonymousPath[i], Convert.ToInt32(Session["UserId"]));
                            else
                                break;
                        }
                        for (int i = 0; i < RecepientType.Length; i++)
                        {
                            if (RecepientType[i] != null)
                                gcaa.USP_CORRES_RECIPIENTS("insert", Convert.ToInt32(msg_ID.CorrespondenceID), userId[i], DLId[i], extMailids[i], mailType[i], Convert.ToInt32(Session["UserId"]), Convert.ToInt32(Session["UserId"])).ToList();
                            else
                                break;
                        }
                        gcaa.SaveChanges();
                        if (corres.DAAIApprovalRequired == false)
                        {
                            var Recepients = gcaa.USP_CORRES_RECIPIENTS("viewuser", msg_ID.CorrespondenceID, null, null, null, null, null, Convert.ToInt32(Session["UserId"])).ToList();
                            List<string> DListUsers = new List<string>();
                            Dictionary<string, List<string>> DListUsersEmail = new Dictionary<string, List<string>>();
                            Dictionary<string, string> DListRandomLink = new Dictionary<string, string>();
                            foreach (var emailrec in Recepients)
                            {
                                List<string> lst = new List<string>();
                                if (DListUsersEmail.ContainsKey(emailrec.RECTYPE))
                                    DListUsersEmail[emailrec.RECTYPE].Add(emailrec.RECIPIENT);
                                else
                                    DListUsersEmail.Add(emailrec.RECTYPE, lst);
                                lst.Add(emailrec.RECIPIENT);
                                DListUsers.Add(emailrec.FirstName);
                            }

                            foreach (var ToUsers in DListUsersEmail.ToList())
                            {
                                if (ToUsers.Key.ToLower() == "to")
                                {
                                    gcaa_ismEntities dbcontext = new gcaa_ismEntities();

                                    //    foreach (var usermail in ToUsers.Value.ToList())
                                    //    {
                                    //        string randomCode = Convert.ToString(Guid.NewGuid());
                                    //        var verifyurl = "/Anonymous/CorrespondenceResponse/" + randomCode;
                                    //        string randomURL = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, verifyurl);
                                    //        if (!DListRandomLink.Keys.Contains(usermail))
                                    //            DListRandomLink.Add(usermail, randomURL);
                                    //        var ResponsedUsers = gcaa.CorrespondenceRecipients.Where(a => a.CorrespondenceID == msg_ID.CorrespondenceID && a.RecipientEmailID == usermail).FirstOrDefault();
                                    //        if (ResponsedUsers == null)
                                    //            dbcontext.USP_ADD_CORRES_RESPONSE_DETAILS(msg_ID.CorrespondenceID, randomCode, false, usermail, Convert.ToInt32(Session["UserId"]), Convert.ToInt32(Session["UserId"]));
                                    //    }
                                    //    dbcontext.SaveChanges();
                                    //}
                                    foreach (var usermail in ToUsers.Value.ToList())
                                    {
                                        string randomCode = Convert.ToString(Guid.NewGuid());
                                        string verifyurl = string.Empty;
                                        //for safety recommendations need a separate random url
                                        //if (corres.LKCorrespondenceMessageTypeID == 6)
                                        //    verifyurl = "/Anonymous/SRExternalResponse/" + randomCode;
                                        //  else
                                        verifyurl = "/Anonymous/CorrespondenceResponse/" + randomCode;
                                        string randomURL = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, verifyurl);
                                        if (!DListRandomLink.Keys.Contains(usermail))
                                            DListRandomLink.Add(usermail, randomURL);
                                        var ResponsedUsers = gcaa.CorrespondenceRecipients.Where(a => a.CorrespondenceID == msg_ID.CorrespondenceID && a.RecipientEmailID == usermail).FirstOrDefault();
                                        if (ResponsedUsers == null)
                                            dbcontext.USP_ADD_CORRES_RESPONSE_DETAILS(msg_ID.CorrespondenceID, randomCode, 0, usermail, Convert.ToInt32(Session["UserId"]), Convert.ToInt32(Session["UserId"]));
                                        if (corres.LKCorrespondenceMessageTypeID == 6)
                                            dbcontext.USP_ADD_SR_STAKEHOLDER_RESPONSE_DETAILS(corres.SR_ID, randomCode, false, usermail, Convert.ToInt32(Session["UserId"]), Convert.ToInt32(Session["UserId"]));
                                    }
                                }
                            }
                            var Attachments = gcaa.USP_CORRES_ATTACHMENTS("view", Convert.ToInt32(msg_ID.CorrespondenceMessageID), null, null, null, null).ToList();
                            int filesCount = 0;
                            string[] filesAttached = new string[50];
                            foreach (var attachment in Attachments)
                            {
                                if (!string.IsNullOrEmpty(attachment.AttachmentPublicUrl))
                                {
                                    filesAttached[filesCount] = comm.getEventFilesOrOtherFilesOnPrem(Convert.ToString(attachment.AttachmentPublicUrl));
                                    filesCount++;
                                }
                            }
                            if (corres.LKCorrespondenceMessageTypeID == 6)
                            {
                                var SRDetails = gcaa.USP_VIEW_SR(corres.SR_ID).FirstOrDefault();

                                var SRattachments = gcaa.USP_SR_ATTACHMENTS("view", corres.SR_ID, 0, null, null, null, null).ToList();

                                if (SRattachments.Count > 0)
                                {
                                    foreach (var SRattachment in SRattachments)
                                    {
                                        if (!string.IsNullOrEmpty(SRattachment.AttachmentPublicUrl))
                                        {
                                            filesAttached[filesCount] = comm.getEventFilesOrOtherFilesOnPrem(Convert.ToString(SRattachment.AttachmentPublicUrl));
                                            filesCount++;
                                        }
                                    }
                                }
                                filesAttached[filesCount] = pdf.generatePdf(SRDetails, null, null, "CorresSR", "");
                            }
                            mailSent = mail.SendMailNotificationwithAttach(7, Convert.ToString(Session["FirstName"]), Convert.ToInt32(Session["roleid"]), siteURL, DListUsers, DListUsersEmail, DListRandomLink, Convert.ToInt32(Session["InvFileID"]), filesAttached, corres.CorrespondenceControlNumber, Convert.ToString(Session["InvNum"]), corres.Subject, null, corres.CorrespondenceContent, corres.CorrespondenceMessageFrom);
                        }
                    }
                    if (corres.DAAIApprovalRequired == true)
                    {
                        var DList = gcaa.Users.AsQueryable().Where(d => d.UserRoleID == 3).Select(x => new { x.EmailAddress, x.FirstName }).FirstOrDefault();
                        List<string> DListUsers = new List<string>();
                        List<string> DListUsersEmail = new List<string>();
                        DListUsersEmail.Add(DList.EmailAddress);
                        DListUsers.Add(DList.FirstName);
                        mailSent = mail.SendMailNotification(6, Convert.ToString(Session["FirstName"]), Convert.ToInt32(Session["roleid"]), siteURL, DListUsers, DListUsersEmail, Convert.ToInt32(Session["InvFileID"]), null, corres.CorrespondenceControlNumber, Convert.ToString(Session["InvNum"]), null, corres.CorrespondenceMessageFrom);
                    }
                    Array.Clear(SharepointfilePath, 0, SharepointfilePath.Length);
                    Array.Clear(DocumentTypeId, 0, DocumentTypeId.Length);
                    Array.Clear(DLId, 0, DLId.Length);
                    Array.Clear(userId, 0, userId.Length);
                    Array.Clear(RecepientType, 0, RecepientType.Length);
                    Array.Clear(extMailids, 0, extMailids.Length);
                    Array.Clear(mailType, 0, extMailids.Length);
                    Array.Clear(SharepointAnonymousPath, 0, SharepointAnonymousPath.Length);
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("IICController", "AddCorrespondence::POST", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }
            return RedirectToAction("Correspondences", "IIC", new { id = corres.InvestigationFileID });
        }
        /// <summary>
        /// View the correspondence details based on investigation file id and correspondence id
        /// </summary> 
        /// 
        /// <param name="id">Receive the investigation file id.</param>   
        /// <param name="Corr_id">Receive the Correspondences details as object.</param>     
        public ActionResult ViewCorrespondence(int id = 0, int Corr_id = 0)
        {
            var Corres = new IIC();
            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 3 || Convert.ToInt32(Session["RoleId"]) == 6 || Convert.ToInt32(Session["RoleId"]) == 7)
                {
                    int invid = id;
                    if (invid > 0)
                    {
                        Corres = new IIC()
                        {
                            Inv = gcaa.USP_VIEW_INVESTIGATION_FORM35(invid).ToList(),
                            Corres = gcaa.USP_VIEW_ALL_CORRESPONDENCES(invid).ToList(),
                            corres_Det = gcaa.USP_VIEW_CORRESPONDENCE(Corr_id).ToList(),
                            Corres_Attach = gcaa.USP_VIEW_CORRES_ATTACHMENTS(Corr_id).ToList(),
                            Corres_Recepients = gcaa.USP_CORRES_RECIPIENTS("View", Corr_id, null, null, null, null, null, Convert.ToInt32(Session["UserId"])).ToList()
                        };
                    }
                    int userID = Convert.ToInt32(Session["UserId"]);
                    var userDet = gcaa.Users.Where(a => a.UserID == userID).FirstOrDefault();
                    ViewBag.IICmailid = userDet.EmailAddress;
                    var InvInfo = gcaa.USP_VIEW_INVESTIGATION_FORM35(invid).FirstOrDefault();
                    string filenum = InvInfo.InvestigationNumber.Split('/')[1];
                    var CONTROL_NUMER = gcaa.USP_VIEW_CORRESPONDENCE(Corr_id).FirstOrDefault();
                    TempData["CONTROL_NUMER"] = CONTROL_NUMER.CorrespondenceControlNumber;
                    //ViewBag.CORRES_MESS_ID = CONTROL_NUMER.INV_FILE_NUMBER;

                    var Corre_Type = gcaa.LKCorrespondenceTypes.ToList();
                    SelectList Corre_Typelist = new SelectList(Corre_Type, "LKCorrespondenceTypeID", "LKCorrespondenceTypeName");
                    ViewBag.corresp_Lsit = Corre_Typelist;
                    var Roles = gcaa.LKCorrespondencePersonRoles.ToList();

                    SelectList Roles_List = new SelectList(Roles, "LKCorrespondencePersonRoleID", "LKCorrespondencePersonRoleName");
                    ViewBag.RolesList = Roles_List;

                    var DAAIApproval = new List<SelectListItem>();
                    DAAIApproval.Add(new SelectListItem() { Text = "Select", Value = "N/A" });
                    DAAIApproval.Add(new SelectListItem() { Text = "Yes", Value = "true" });
                    DAAIApproval.Add(new SelectListItem() { Text = "No", Value = "false" });

                    var DueDate = new List<SelectListItem>();
                    DueDate.Add(new SelectListItem() { Text = "Select", Value = "N/A" });
                    DueDate.Add(new SelectListItem() { Text = "30", Value = "30" });
                    DueDate.Add(new SelectListItem() { Text = "60", Value = "60" });
                    DueDate.Add(new SelectListItem() { Text = "90", Value = "90" });
                    DueDate.Add(new SelectListItem() { Text = "120", Value = "120" });

                    SelectList DueDate_List = new SelectList(DueDate, "VALUE", "Text");
                    ViewBag.DueDateList = DueDate_List;
                    SelectList DAAIApproval_List = new SelectList(DAAIApproval, "VALUE", "Text");
                    ViewBag.DAAIApprovalList = DAAIApproval_List;
                    TempData["PageHead"] = InvInfo.InvestigationNumber;
                    TempData["InvId"] = InvInfo.InvestigationFileID;
                    TempData["InvFileID"] = InvInfo.InvestigationFileID;

                    TempData["BackPage"] = "corr_mg";
                    TempData["active_url_CorrespondenceManagement"] = "tm";
                    TempData["active_url_CorrespondenceManagement_iic"] = "iic";

                    TempData["lf_menu"] = "1";
                    TempData["INV_STATUS"] = InvInfo.LKInvestigationStatusTypeID;

                    Correspondence correspondence = (from c in gcaa.Correspondences where c.CorrespondenceID == Corr_id select c).SingleOrDefault();
                    correspondence.IsViewed = true;
                    gcaa.SaveChanges();
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("IICController", "ViewCorrespondence", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }
            return View(Corres);
        }
        /// <summary>
        /// save correspondence attachment files to sharepoint 
        /// </summary> 
        /// 
        /// <param name="files">Receive the attachment files details as object.</param>   
        /// <param name="CONTROL_NUMER">Receive the investigation file id.</param>     
        private void saveFilestoSharePoint(HttpFileCollectionBase files, string CONTROL_NUMER)
        {
            try
            {
                if (files.Count > 0)
                {
                    if (string.IsNullOrEmpty(CONTROL_NUMER))
                        CONTROL_NUMER = "0";
                    // comm.CreateIfMissing(Server.MapPath("~/") + "CorrespondenceFiles");
                    int filecount = 0;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];

                        if (file.ContentLength > 0 && !string.IsNullOrEmpty(file.FileName))
                        {

                            int fileSize = file.ContentLength;

                            //string fileName = file.FileName;
                            string fileName = file.FileName.Substring(0, file.FileName.LastIndexOf('.')) + "_" + DateTime.Now.ToFileTime() + file.FileName.Substring(file.FileName.LastIndexOf('.'), ((file.FileName.Length) - (file.FileName.LastIndexOf('.'))));

                            string mimeType = file.ContentType.Split('/')[1].ToString();

                            DocumentTypeId[i] = Convert.ToInt32(gcaa.LKDocumentTypes.AsQueryable().Where(d => d.LKDocumentTypeName == mimeType).Select(d => d.LKDocumentTypeID).FirstOrDefault());

                            Stream fileContent = file.InputStream;

                            //   file.SaveAs(Server.MapPath("~/") + "CorrespondenceFiles\\" + fileName);

                            //string[] attachURLs = comm.savetoSharePointDocs(fileContent, Server.MapPath("~/") + "CorrespondenceFiles\\" + fileName, CONTROL_NUMER, "Corres_Documents");

                            string[] attachURLs = comm.savetoSharePointOtherDocsOnPrem(fileContent, fileName, CONTROL_NUMER, "Corres_Documents");

                            SharepointfilePath[filecount] = attachURLs[0];

                            SharepointAnonymousPath[filecount] = attachURLs[1];

                            //   var result = gcaa.USP_INST_INVESTIGATION_DOCUMENTASATTACHMENT(0, InvestigationFileID, 1, DocumentTypeId, SharepointfilePath);

                            //   gcaa.SaveChanges();
                            filecount++;
                        }
                    }
                    //   comm.DeleteDirectory(Server.MapPath("~/") + "CorrespondenceFiles");

                    var fullpathoffile = Server.MapPath("~/App_Data/Corres_Files/" + InvFileID);

                    if (System.IO.Directory.Exists(fullpathoffile))
                    {
                        comm.DeleteDirectory(fullpathoffile);

                        System.IO.Directory.Delete(fullpathoffile);
                    }
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("IICController", "saveFilestoSharePoint", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }
        }
        /// <summary>
        /// Correspondence success message display by message type 
        /// </summary> 
        /// 
        /// <param name="message">Receive the success message info.</param>   
        /// <param name="type">Receive the type of messgage.</param>     
        public ActionResult SuccessMessage(string message, string type)
        {
            ViewBag.Message = message;
            ViewBag.Type = type;
            return View();
        }
        /// <summary>
        /// Update DAAI Status by correspondence id and correspondence message id
        /// </summary> 
        /// 
        /// <param name="Corr_msg_id">Receive the corresspondence meaasge approval status id.</param>   
        /// <param name="corr_id">Receive the corresspondence id.</param>     
        /// <param name="control_num">Receive the investigation file id.</param>   
        /// <param name="Status">Receive corresspondence status.</param>     
        /// <param name="respnseduedate">Receive the date and time of correspondence due .</param>   

        public ActionResult UpdateDAAIStatus(int Corr_msg_id, int corr_id, string control_num, string Status, DateTime respnseduedate)
        {
            try
            {
                DateTime start = DateTime.Today;
                TimeSpan diff = respnseduedate - start;
                string siteURL = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                int days = diff.Days;
                if (Convert.ToInt32(Session["RoleId"]) == 3)
                {
                    if (Convert.ToInt32(Session["UserId"]) > 0)
                    {
                        gcaa.USP_UPDT_CORRES_APPRSTATUS(Corr_msg_id, Status, corr_id, days, Convert.ToInt32(Session["UserId"]));

                        gcaa.SaveChanges();
                    }
                    if (Status.ToLower() == "approved")
                    {
                        var Recepients = gcaa.USP_CORRES_RECIPIENTS("viewuser", corr_id, null, null, null, null, null, Convert.ToInt32(Session["UserId"]));
                        List<string> DListUsers = new List<string>();
                        Dictionary<string, List<string>> DListUsersEmail = new Dictionary<string, List<string>>();
                        Dictionary<string, string> DListRandomLink = new Dictionary<string, string>();
                        foreach (var emailrec in Recepients)
                        {
                            List<string> lst = new List<string>();
                            if (DListUsersEmail.ContainsKey(emailrec.RECTYPE))
                                DListUsersEmail[emailrec.RECTYPE].Add(emailrec.RECIPIENT);
                            else
                                DListUsersEmail.Add(emailrec.RECTYPE, lst);
                            lst.Add(emailrec.RECIPIENT);
                            DListUsers.Add(emailrec.FirstName);
                        }
                        foreach (var ToUsers in DListUsersEmail.ToList())
                        {
                            if (ToUsers.Key.ToLower() == "to")
                            {
                                gcaa_ismEntities dbcontext = new gcaa_ismEntities();

                                foreach (var usermail in ToUsers.Value.ToList())
                                {
                                    List<string> lst = new List<string>();
                                    string randomCode = Convert.ToString(Guid.NewGuid());
                                    var verifyurl = "/Anonymous/CorrespondenceResponse/" + randomCode;
                                    string randomURL = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, verifyurl);
                                    if (!DListRandomLink.Keys.Contains(usermail))
                                        DListRandomLink.Add(usermail, randomURL);
                                    //var ResponseUsers = gcaa.USP_ADD_CORRES_RESPONSE_DETAILS(33, "1c3ffae0-c84d-44c1-b8e6-20eab62d8728", false, "isminvestigator1@gmail.com", Convert.ToInt32(Session["UserId"]), Convert.ToInt32(Session["UserId"]));
                                    var ResponsedUsers = gcaa.CorrespondenceRecipients.Where(a => a.CorrespondenceID == corr_id && a.RecipientEmailID == usermail).FirstOrDefault();
                                    if (ResponsedUsers == null)
                                        dbcontext.USP_ADD_CORRES_RESPONSE_DETAILS(corr_id, randomCode, 0, usermail, Convert.ToInt32(Session["UserId"]), Convert.ToInt32(Session["UserId"]));
                                }
                                dbcontext.SaveChanges();
                            }
                        }
                        var Corres = gcaa.Correspondences.Where(x => x.CorrespondenceID == corr_id).FirstOrDefault();
                        var Corres_Det = gcaa.CorrespondenceMessages.Where(x => x.CorrespondenceMessageID == Corr_msg_id).FirstOrDefault();
                        bool mailSent = false;
                        var Attachments = gcaa.USP_CORRES_ATTACHMENTS("view", Convert.ToInt32(Corr_msg_id), null, null, null, null).ToList();
                        int filesCount = 0;
                        string[] filesAttached = new string[50];
                        var corresUser = gcaa.Users.Where(a => a.UserID == Corres.CreatedBy).FirstOrDefault();
                        foreach (var attachment in Attachments)
                        {
                            filesAttached[filesCount] = comm.getEventFilesOrOtherFilesOnPrem(Convert.ToString(attachment.AttachmentPublicUrl));
                            filesCount++;
                        }
                        if (Corres_Det.LKCorrespondenceMessageTypeID == 6)
                        {
                            var SRValue = gcaa.InvestigationSafetyRecommendations.Where(ar => ar.SafetyRecommendationNumber == Corres_Det.SR_Number                            
                                                  && ar.InvestigationSafetyRecommendationID == Corres_Det.InvestigationSafetyRecommendationID).FirstOrDefault();
                            var SRDetails = gcaa.USP_VIEW_SR(SRValue.InvestigationSafetyRecommendationID).FirstOrDefault();
                            var SRattachments = gcaa.USP_SR_ATTACHMENTS("view", SRValue.InvestigationSafetyRecommendationID, 0, null, null, null, null).ToList();
                            if (SRattachments.Count > 0)
                            {
                                foreach (var SRattachment in SRattachments)
                                {
                                    if (!string.IsNullOrEmpty(SRattachment.AttachmentPublicUrl))
                                    {
                                        filesAttached[filesCount] = comm.getEventFilesOrOtherFilesOnPrem(Convert.ToString(SRattachment.AttachmentPublicUrl));
                                        filesCount++;
                                    }
                                }
                            }
                            filesAttached[filesCount] = pdf.generatePdf(SRDetails, null, null, "CorresSR", "");
                        }
                        mailSent = mail.SendMailNotificationwithAttach(7, corresUser.FirstName, corresUser.UserRoleID, siteURL, DListUsers, DListUsersEmail, DListRandomLink, Convert.ToInt32(Session["InvFileID"]), filesAttached, control_num, Convert.ToString(Session["InvNum"]), Corres.Subject, null, Corres_Det.CorrespondenceContent, Corres_Det.CorrespondenceMessageFrom);
                    }
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("IICController", "UpdateDAAIStatus", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }
            return RedirectToAction("Dashboard", "Home");
        }
        /// <summary>
        /// Save correspondence document to database table 
        /// </summary> 
        /// 
        /// <param name="control">Receive the investigation file id.</param>
        [HttpPost]
        public void SaveFiles(string control)
        {
            try
            {
                string CONTROL_NUMER = control;
                if (CONTROL_NUMER != null)
                {
                    if (Request.Files.Count > 0)
                    {
                        // comm.CreateIfMissing(Server.MapPath("~/") + "CorrespondenceFiles");
                        int filecount = 0;
                        for (int i = 0; i < Request.Files.Count; i++)
                        {
                            HttpPostedFileBase file = Request.Files[i];

                            int fileSize = file.ContentLength;

                            //string fileName = file.FileName;
                            string fileName = file.FileName.Substring(0, file.FileName.LastIndexOf('.')) + "_" + DateTime.Now.ToFileTime() + file.FileName.Substring(file.FileName.LastIndexOf('.'), ((file.FileName.Length) - (file.FileName.LastIndexOf('.'))));

                            string mimeType = file.ContentType.Split('/')[1].ToString();

                            DocumentTypeId[i] = Convert.ToInt32(gcaa.LKDocumentTypes.AsQueryable().Where(d => d.LKDocumentTypeName == mimeType).Select(d => d.LKDocumentTypeID).FirstOrDefault());

                            Stream fileContent = file.InputStream;

                            //   file.SaveAs(Server.MapPath("~/") + "CorrespondenceFiles\\" + fileName);

                            //string[] attachURLs = comm.savetoSharePointDocs(fileContent, Server.MapPath("~/") + "CorrespondenceFiles\\" + fileName, CONTROL_NUMER, "Corres_Documents");

                            string[] attachURLs = comm.savetoSharePointOtherDocsOnPrem(fileContent, fileName, CONTROL_NUMER, "Corres_Documents");

                            SharepointfilePath[filecount] = attachURLs[0];

                            SharepointAnonymousPath[filecount] = attachURLs[1];

                            //   var result = gcaa.USP_INST_INVESTIGATION_DOCUMENTASATTACHMENT(0, InvestigationFileID, 1, DocumentTypeId, SharepointfilePath);

                            //   gcaa.SaveChanges();
                            filecount++;
                        }
                        //   comm.DeleteDirectory(Server.MapPath("~/") + "CorrespondenceFiles");


                    }
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("IICController", "SaveFiles", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                RedirectToAction("PageNotFound", "Error");
            }
        }
        /// <summary>
        /// Save the recipients name and email address to database
        /// </summary> 
        /// 
        /// <param name="recipient">Receive the recipient name.</param>
        [HttpPost]
        public void saveRecipients(string recipient)
        {
            int count = 0;
            try
            {
                if (Request.Form.Count > 0)
                {
                    if (Request.Form["UName"].Contains(','))
                        count = Request.Form["UName"].Split(',').Count();
                    else
                        count = 1;
                }
                string username; string mailid;
                for (int i = 0; i < count; i++)
                {
                    username = mailid = string.Empty;
                    if (Request.Form["mailType"].Contains(','))
                        mailType[i] = Request.Form["mailType"].Split(',')[i];
                    else
                        mailType[i] = Request.Form["mailType"];
                    if (Request.Form["UName"].Split(',')[i].Contains('-'))
                    {
                        username = Request.Form["UName"].Split(',')[i].Split('-')[0].Trim();
                        mailid = Request.Form["UName"].Split(',')[i].Split('-')[1].Trim();
                    }
                    else
                    {
                        if (Request.Form["UName"].Split(',')[i].Contains('@'))
                        {
                            extMailids[i] = Request.Form["UName"].Split(',')[i].Trim();
                            RecepientType[i] = "ext";
                        }
                        else
                        {
                            username = Request.Form["UName"].Split(',')[i].Trim();
                            mailid = "";
                        }
                    }
                    if (username != null)
                    {
                        if (mailid == "")
                            mailid = null;
                        var users = gcaa.USP_GET_USER_ID(username, mailid).FirstOrDefault();
                        if (users != null)
                        {
                            if (users.Rec_Type.ToLower() == "user")
                            {
                                userId[i] = users.ID;
                                RecepientType[i] = users.Rec_Type;
                            }
                            if (users.Rec_Type.ToLower() == "dl")
                            {
                                DLId[i] = users.ID;
                                RecepientType[i] = users.Rec_Type;
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("IICController", "saveRecipients", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                RedirectToAction("PageNotFound", "Error");
            }
        }
        /// <summary>
        /// Search the user name and recipients name by input keyword
        /// </summary> 
        /// 
        /// <param name="keyword">Receive the search keyword.</param>
        public ActionResult searchUsers(string keyword)
        {
            string result = string.Empty;
            try
            {
                var item = gcaa.USP_GET_USERS_EMAIL(keyword, Convert.ToInt32(Session["InvFileID"])).ToList();

                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                result = javaScriptSerializer.Serialize(item);
            }
            catch (System.Exception ex)
            {
                result = string.Empty;
                comm.Exception_Log("IICController", "searchUsers", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Get all the safety recommendations by investigation id
        /// </summary> 
        /// 
        /// <param name="InvId">Receive the investigation file id</param>
        public ActionResult searchSRs(int InvId)
        {
            string result = string.Empty;
            try
            {
                var item = gcaa.USP_GET_SR_INVESTIGATION(InvId).ToList();

                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                result = javaScriptSerializer.Serialize(item);
            }
            catch (System.Exception ex)
            {
                result = string.Empty;
                comm.Exception_Log("IICController", "searchUsers", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #region Reassigning IIC
        /// <summary>
        /// Redirect reassign IIC view page
        /// </summary> 
        /// 
        /// <param name="id">Receive the investigation file id</param>
        public ActionResult ReassignIIC(int id = 0)
        {
            var ReassignIIC = new IIC();
            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 3)
                {
                    int invid = id;
                    if (invid > 0)
                    {
                        ReassignIIC = new IIC()
                        {
                            Inv = gcaa.USP_VIEW_INVESTIGATION_FORM35(invid).ToList(),
                            Assign_IIC = gcaa.USP_VIEW_ASSIGNED_IIC(invid).ToList(),
                            list_iic_incharge = gcaa.USP_LIST_IIC_INCHARGE_WITH_CASE().ToList(),
                            reassignhistory = gcaa.USP_VIEW_REASSIGNED_HISTORY_IIC(invid).ToList()
                        };
                        var InvDet = gcaa.USP_VIEW_INVESTIGATION_FORM35(invid).FirstOrDefault();
                        TempData["PageHead"] = InvDet.InvestigationNumber;
                        TempData["BackPage"] = "yes";
                        TempData["InvId"] = InvDet.InvestigationFileID;
                        TempData["InvFileID"] = InvDet.InvestigationFileID;
                        TempData["active_url_reassign_iic"] = "director";
                        TempData["lf_menu"] = "1";
                        var assignedIIC = gcaa.USP_VIEW_ASSIGNED_IIC(invid).FirstOrDefault();
                        ViewBag.ASSIGNED_IIC = assignedIIC.IICAssigned;
                    }
                }
                else
                {
                    return RedirectToAction("PageNotFound", "Error");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("IICController", "ReassignIIC", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }
            return View(ReassignIIC);
        }
        /// <summary>
        /// Update reassign IIC view page
        /// </summary> 
        /// 
        /// <param name="Reassign">Receive the iic details as object</param>
        [HttpPost]
        public ActionResult ReassignIIC(IIC Reassign)
        {
            bool mailSent = false;
            try
            {
                string siteURL = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));

                var InvInfo = gcaa.InvestigationIICAssignments.Where(a => a.InvestigationFileID == Reassign.InvestigationFileID).FirstOrDefault();

                var invUserInfo = gcaa.Users.Where(a => a.UserID == InvInfo.IICAssigned).FirstOrDefault();

                if (Convert.ToInt32(Session["UserId"]) > 0)
                {
                    gcaa.USP_ASSIGN_IIC_INVESTIGATION(Reassign.InvestigationFileID, Reassign.IIC_ASSIGNED, 0, Reassign.IIC_REASSIGNED, Convert.ToInt32(Session["UserID"]), Convert.ToInt32(Session["UserID"]), Reassign.IIC_REASONFORREMOVE, Convert.ToInt32(Session["UserID"]), Convert.ToInt32(Session["UserID"]), "Reassign", Reassign.InvestigationFileNo);
                    gcaa.SaveChanges();
                }
                var Assigned_IIC = gcaa.USP_VIEW_ASSIGNED_IIC(Reassign.InvestigationFileID).ToList();
                List<string> DListUsers = new List<string>();
                List<string> DListUsersEmail = new List<string>();
                foreach (var users in Assigned_IIC)
                {
                    DListUsersEmail.Add(users.EmailAddress);
                    DListUsers.Add(users.FirstName);
                }
                dynamic Inv = gcaa.USP_VIEW_INVESTIGATION_FORM35_PDF(Convert.ToInt32(Session["InvFileID"])).FirstOrDefault();
                dynamic FILES = gcaa.USP_VIEW_INVESTIGATION_ATTACHMENTFILE(Convert.ToInt32(Session["InvFileID"])).ToList();
                dynamic invAircraft = gcaa.USP_VIEW_INVESTIGATION_AIRCRAFT_DETAIL_FORM35_PDF(Convert.ToInt32(Session["InvFileID"])).ToList();
                string sourceFile = pdf.generatePdf(Inv, invAircraft, FILES, "Form39", "");
                mailSent = mail.SendMailNotification(3, Convert.ToString(Session["FirstName"]), Convert.ToInt32(Session["roleid"]), siteURL, DListUsers, DListUsersEmail, Convert.ToInt32(Session["InvFileID"]), sourceFile, Inv.NotificationID, null, null, null);
                //already assigned IICs mail
                DListUsers = new List<string>();
                DListUsersEmail = new List<string>();
                DListUsersEmail.Add(invUserInfo.EmailAddress);
                DListUsers.Add(invUserInfo.FirstName);
                mailSent = mail.SendMailNotification(14, Convert.ToString(invUserInfo.FirstName), Convert.ToInt32(invUserInfo.UserRoleID), siteURL, DListUsers, DListUsersEmail, Convert.ToInt32(Session["InvFileID"]), null, Inv.NotificationID, null, null, null);
            }
            catch (System.Exception ex)
            {
                mailSent = false;
                comm.Exception_Log("IICController", "ReassignIIC::POST", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
                return RedirectToAction("PageNotFound", "Error");
            }
            return RedirectToAction("IICTasks", "IIC", new { id = Reassign.InvestigationFileID });
        }
        #endregion
        /// <summary>
        /// Download Manuals
        /// </summary> 
        /// 
        public ActionResult DownloadManuals()
        {
            try
            {


                TempData["active_url_manuals_iic"] = "iic";

                TempData["investigationdetail_url"] = "iic";
                TempData["lf_menu"] = "1";
                //  var InvInfo = gcaa.USP_VIEW_INVESTIGATION_INVESTIGATIONID(invid).FirstOrDefault();
                // TempData["PageHead"] = InvInfo.InvestigationFileNumber;

                // Session["InvFileID"] = InvInfo.InvestigationFileID;

                //  TempData["InvId"] = InvInfo.InvestigationFileID;

                //  TempData["InvFileID"] = InvInfo.InvestigationFileID;

                if (Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 5 || Convert.ToInt32(Session["RoleId"]) == 7)
                {
                    return View();
                }
                else { return RedirectToAction("Dashboard", "Home"); }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("IICController", "DownloadManuals", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }
            // return View();
        }
        /// <summary>
        /// Event Action iic
        /// </summary> 
        /// 
        /// <param name="ev_iic_action">Receive the iic event details</param>
        public JsonResult Event_Action_iic(event_iic_action ev_iic_action)
        {
            if (Convert.ToInt32(Session["UserId"]) > 0)
            {

                int retval = 0;

                if (ev_iic_action.event_action_status == 0)
                {
                    gcaa.USP_INST_EVENT_ACTION(ev_iic_action.inv_task_assignment_id, ev_iic_action.invest_file_id, ev_iic_action.task_grp_id, ev_iic_action.comment);
                    var InvEventList = gcaa.InvestigationTaskGroupEvents.Where(a => a.InvestigationTaskGroupEventUniqueNumber == "E" + ev_iic_action.event_uniq_no).ToList();

                    var CheckTaskStatus = gcaa.InvestigationTaskDetails.AsQueryable().Where(a => a.InvestigationTaskAssignmentID == ev_iic_action.inv_task_assignment_id && a.InvestigationTaskStatus == 0).FirstOrDefault();

                    if (CheckTaskStatus != null)
                    {
                        foreach (var itm in InvEventList)
                        {
                            retval = gcaa.USP_UPDT_INVESTIGATION_SCOPEDEFINITION_PROGRESS(ev_iic_action.invest_file_id, itm.InvestigationTaskGroupID);
                        }
                    }
                    gcaa.USP_UPDT_TASKDETAILS_EVENT_ACTION(ev_iic_action.event_action_status, ev_iic_action.inv_task_assignment_id);
                    return Json(0);
                }
                else if (ev_iic_action.event_action_status == 1)
                {
                    gcaa.USP_INST_EVENT_ACTION(ev_iic_action.inv_task_assignment_id, ev_iic_action.invest_file_id, ev_iic_action.task_grp_id, ev_iic_action.comment);
                    gcaa.USP_UPDT_TASKDETAILS_EVENT_SUBMIT_STATUS(ev_iic_action.event_action_status, ev_iic_action.inv_task_assignment_id);

                    var event_tm_info = gcaa.USP_GET_EVENT_MEMBERS_EMAIL_DETAILS(ev_iic_action.inv_task_assignment_id).ToList();

                    var inv_file_number = gcaa.USP_GET_INVESTIGATION_FILE_NUMBER(ev_iic_action.invest_file_id).FirstOrDefault();

                    List<string> DListUsersEmail = new List<string>();

                    List<string> DListUsers = new List<string>();
                    foreach (var users in event_tm_info)
                    {
                        DListUsersEmail.Add(users.EmailAddress);
                        DListUsers.Add(users.EmailAddress);
                    }
                    bool IsSend = mail.SendMailNotification(17, null, 0, null, DListUsers, DListUsersEmail, ev_iic_action.event_uniq_no, ev_iic_action.comment, inv_file_number.InvestigationFileNumber, null, null, ev_iic_action.comment);
                    return Json(1);
                }
            }
            return Json(2);
        }
        /// <summary>
        /// Upload Corres File to database table
        /// </summary> 
        /// 
        [HttpPost]
        public JsonResult UploadCorresFile()
        {
            try
            {
                comm.CreateIfMissing(Server.MapPath("~/") + "App_Data/Corres_Files/" + InvFileID);

                foreach (string file in Request.Files)
                {
                    var fileContent = Request.Files[file];

                    if (fileContent != null && fileContent.ContentLength > 0)
                    {
                        var stream = fileContent.InputStream;

                        var fileName = Path.GetFileName(file);

                        var path = Path.Combine(Server.MapPath("~/App_Data/Corres_Files/" + InvFileID), fileContent.FileName);

                        using (var fileStream = System.IO.File.Create(path))
                        {
                            stream.CopyTo(fileStream);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("DIController", "UploadCorresFile", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return Json("Upload failed");
            }

            return Json("File uploaded successfully");
        }
        /// <summary>
        /// Remove Correspondance Attached File
        /// </summary> 
        /// 
        /// <param name="filename">Receive the attachment file name</param>
        [HttpPost]
        public JsonResult RemoveCorresAttachedFile(string filename)
        {
            string result = "failed";

            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 7)
                {
                    if (Convert.ToInt32(Session["UserId"]) > 0)
                    {
                        string[] fName = filename.Split('\\');

                        if (fName.Count() >= 2)
                        {
                            var fullpathoffile = Server.MapPath("~/App_Data/Corres_Files/" + InvFileID + "/" + fName[2]);

                            if (System.IO.File.Exists(fullpathoffile))
                            {
                                System.IO.File.Delete(fullpathoffile);
                            }

                            result = "success";
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("IIC", "RemoveCorresAttachedFile", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }

            return Json(result);
        }
        /// <summary>
        /// Mark Correspondence Message As Read
        /// </summary>
        /// <param name="correspondenceMessageId"></param> 
        /// 
        [HttpPost]
        public JsonResult MarkCorrespondenceAsRead(int correspondenceMessageId)
        {
            string result = "failed";

            try
            {
                CorrespondenceMessage correspondenceMessage = (from c in gcaa.CorrespondenceMessages where c.CorrespondenceMessageID == correspondenceMessageId select c).SingleOrDefault();
                correspondenceMessage.IsViewed = true;
                gcaa.SaveChanges();
                result = "success";
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("IIC", "MarkCorrespondenceAsRead", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }

            return Json(result);
        }
        /// <summary>
        /// upload Large files
        /// </summary> 
        /// 
        /// <param name="investfileid">Receive the investigation file id</param>
        public ActionResult Largefiles(int investfileid)
        {
            ViewBag.investfileid = investfileid;
            var largefile_list = gcaa.InvLargefiles.AsQueryable().Where(a => a.InvestigationFileId == investfileid).ToList();
            ViewBag.largefiles = largefile_list.ToList();
            TempData["lf_menu"] = "1";
            TempData["active_LargeFileUpload"] = "iic";
            TempData["InvFileID"] = investfileid;
            TempData["BackPage"] = "yes";
            TempData["InvId"] = investfileid;
            return View();
        }
        /// <summary>
        /// upload Large files save as post method
        /// </summary> 
        /// 
        /// <param name="form">Receive the form collection default object</param>
        /// <param name="invfileid">Receive the investigation file id</param>
        [HttpPost]
        public ActionResult Largefilespost(FormCollection form, int invfileid)
        {
            try
            {
                HttpFileCollectionBase files = Request.Files;
                for (int filecount = 0; filecount < files.Count; filecount++)
                {
                    HttpPostedFileBase uploadedfile = files[filecount];
                    if (uploadedfile.ContentLength > 0)
                    {
                        string _FileName = uploadedfile.FileName.Substring(0, uploadedfile.FileName.LastIndexOf('.')) + "_" + DateTime.Now.ToFileTime() + uploadedfile.FileName.Substring(uploadedfile.FileName.LastIndexOf('.'), ((uploadedfile.FileName.Length) - (uploadedfile.FileName.LastIndexOf('.'))));

                        string _path = "D:\\largefiles\\" + _FileName;
                        uploadedfile.SaveAs(_path);
                        gcaa.USP_INST_LARGEFILES(invfileid, _path, null, "queued", Convert.ToInt32(Session["UserId"]), Convert.ToInt32(Session["UserId"]));
                    }
                }
            }

            catch (System.Exception ex)
            {

                comm.Exception_Log("IICController", "Largefilespost", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

            }
            //   HttpFileCollectionBase file = Request.Files;            

            //  HostingEnvironment.QueueBackgroundWorkItem(cancellationToken => new Backgroundworker().startprocess(Request.Files));
            // return RedirectToAction("Largefiles");
            return RedirectToAction("Largefiles", "iic", new
            {
                investfileid = invfileid,
            });
        }
        /// <summary>
        /// delete large files uploaded in system based on investigation
        /// </summary> 
        /// 
        /// <param name="file_url">Receive the URL of file name</param>
        /// <param name="investfileid">Receive the investigation file id</param>
        [HttpPost]
        public JsonResult deletelargefiles(int investfileid, string file_url)
        {
            try
            {
                gcaa.USP_DEL_LARGEFILES(investfileid, file_url);
                return Json(0);
            }
            catch (System.Exception ex)
            {

                comm.Exception_Log("iic", "deletelargefiles", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
                return Json(1);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CorrId"></param>
        /// <param name="SrNo"></param>
        /// <returns></returns>
        /// 
        public ActionResult ResendEmail(int CorrId = 0)
        {
            CorrespondenceMessage corr = new CorrespondenceMessage();

            try
            {
                corr.CorrespondenceID = CorrId;

                var srinfo = gcaa.CorrespondenceMessages.Where(a => a.CorrespondenceID == CorrId).FirstOrDefault().SR_Number;

                if (srinfo != null)
                {
                    corr.SR_Number = srinfo;
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("DIController", " AddAirOperatorName", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }

            return View(corr);
        }
        /// <summary>
        /// Resend Email Updates
        /// </summary> 
        /// 
        /// <param name="SrId">Receive the aircraft incident type id.</param>
        /// <param name="CorrId">Receive the investigation file id.</param>
        /// <param name="EmailId"></param>
        [HttpPost]
        public JsonResult ResendEmailUpdates(string SrId, int CorrId, string EmailId)
        {
            if (SrId != string.Empty)
            {
                var srinfo = gcaa.InvestigationSafetyRecommendations.Where(a => a.SafetyRecommendationNumber == SrId).FirstOrDefault();

                var invstakinfo = gcaa.InvestigationSafetyRecommendationStakeholders.Where(a => a.SRID == srinfo.InvestigationSafetyRecommendationID).FirstOrDefault();

                var corrmsginfo = gcaa.CorrespondenceMessages.Where(a => a.SR_Number == SrId).FirstOrDefault();

                if (corrmsginfo != null)
                {
                    var corrinfo = gcaa.Correspondences.Where(a => a.CorrespondenceID == corrmsginfo.CorrespondenceID).FirstOrDefault();

                    string siteURL = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));

                    var SRDetails = gcaa.USP_VIEW_SR(srinfo.InvestigationSafetyRecommendationID).FirstOrDefault();

                    List<string> DListUsers = new List<string>();
                    Dictionary<string, List<string>> DListUsersEmail = new Dictionary<string, List<string>>();
                    Dictionary<string, string> DListRandomLink = new Dictionary<string, string>();
                    List<string> lst = new List<string>();

                    int filesCount = 0;
                    string[] filesAttached = new string[50];

                    filesAttached[filesCount] = pdf.generatePdf(SRDetails, null, null, "CorresSR", "");

                    string randomCode = Convert.ToString(Guid.NewGuid());
                    string verifyurl = string.Empty;

                    verifyurl = "/Anonymous/CorrespondenceResponse/" + randomCode;
                    string randomURL = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, verifyurl);
                    if (!DListRandomLink.Keys.Contains(EmailId))
                        DListRandomLink.Add(EmailId, randomURL);
                    lst.Add(EmailId);
                    DListUsersEmail.Add("To", lst);

                    // Email notification

                    bool mailSent = mail.SendMailNotificationwithAttach(7, Convert.ToString(Session["FirstName"]), Convert.ToInt32(Session["roleid"]),

                         siteURL, DListUsers, DListUsersEmail, DListRandomLink, Convert.ToInt32(Session["InvFileID"]),

                         filesAttached, corrinfo.CorrespondenceControlNumber, Convert.ToString(Session["InvNum"]),

                         corrinfo.Subject, null, corrmsginfo.CorrespondenceContent, corrmsginfo.CorrespondenceMessageFrom);

                    // Update Email id to database

                    var result = gcaa.USP_UPDT_SR_STAKEHOLDER_RESPONSE_DETAILS(srinfo.InvestigationSafetyRecommendationID, randomCode, EmailId,

                                 Convert.ToInt32(Session["UserId"]), corrinfo.CorrespondenceID);
                }

                return Json(1);
            }

            return Json(0);
        }

        /// <summary>
        /// search SR By SR No
        /// </summary>
        /// <param name="srno"></param>
        /// <returns></returns>
        /// 

        public ActionResult searchSRBySRNo(string srno)
        {
            string result = string.Empty;

            try
            {
                var item = from srinfo in gcaa.InvestigationSafetyRecommendations

                           where srinfo.SafetyRecommendationNumber.Contains(srno)

                           select new { srinfo.SafetyRecommendationNumber, srinfo.InvestigationSafetyRecommendationID };

                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();

                result = javaScriptSerializer.Serialize(item);
            }
            catch (System.Exception ex)
            {
                result = string.Empty;

                comm.Exception_Log("IICController", "searchUsers", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Delete Investigation
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult DeleteInvestigation(int id)
        {
            try
            {
                var invinfo = gcaa.InvestigationFiles.Where(a => a.InvestigationFileID == id).FirstOrDefault();

                var result = gcaa.USP_DELETE_INVESTIGATION(id, Convert.ToInt32(Session["UserId"]), invinfo.NotificationID, invinfo.InvestigationFileNumber);
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("IICController", " DeleteInvestigation", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }

            return RedirectToAction("InvestigatorDashboard", "Dashboard");
        }

        /// <summary>
        /// Update Investigation Number
        /// </summary>
        /// <param name="InvId"></param>
        /// <param name="InvNo"></param>
        /// <returns></returns> 
        public JsonResult UpdateInvNo(int InvId, string InvNo)
        {
            try
            {
                string[] InvNoSplit = (InvNo.Split('/'));

                if(InvNoSplit[0] !="AIFN")
                {
                    return Json("0");
                }
                
                if (InvNoSplit[1].Length != 4)
                {
                    return Json("0");
                }                

                if (InvNoSplit[2].Length != 4)
                {
                    return Json("0");
                }

                var AlredyExist = gcaa.InvestigationFiles.Where(a => a.InvestigationFileNumber == InvNo && a.InvestigationFileID != InvId).FirstOrDefault();

                if (AlredyExist != null)
                {
                    return Json("2");
                }
                else
                {
                    var result = gcaa.USP_UPDATE_INVESTIGATION_FILENO(InvId, InvNo);
                }
               
                return Json("1");
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("IICController", " DeleteInvestigation", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return Json("0");
            }
        }
        
    }
}