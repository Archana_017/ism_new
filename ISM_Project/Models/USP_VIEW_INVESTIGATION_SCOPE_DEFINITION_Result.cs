//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ISM_Project.Models
{
    using System;
    
    public partial class USP_VIEW_INVESTIGATION_SCOPE_DEFINITION_Result
    {
        public int InvestigationFileID { get; set; }
        public int InvestigationScopeDefinitionID { get; set; }
        public int InvestigationTaskGroupID { get; set; }
        public string InvestigationTaskGroupName { get; set; }
        public string InvestigationTaskGroupDescription { get; set; }
        public int InvestigationTaskGroupInScope { get; set; }
        public int InvestigationTaskGroupProgress { get; set; }
        public int InvestigationTaskGroupStatus { get; set; }
        public int NumberOfCompletedEvents { get; set; }
        public int NumberOfInProgressEvents { get; set; }
        public int NumberOfSubEvents { get; set; }
        public int NumberOfUnassignedEvents { get; set; }
        public string EXT_STK_NAME { get; set; }
    }
}
