﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ISM_Project.Models
{
    public class IIC
    {
        public IEnumerable<USP_VIEW_INVESTIGATION_FORM35_Result> Inv { get; set; }
        public IEnumerable<USP_VIEW_INVESTIGATION_AIRCRAFT_DETAIL_FORM35_Result> Inv_AircraftDet { get; set; }
        public IEnumerable<USP_VIEW_TASKGROUPS_Result> Grp { get; set; }
        public IEnumerable<USP_VIEW_INVESTIGATION_ATTACHMENTFILE_Result> Attachment { get; set; }
        public IEnumerable<USP_VIEW_IIC_TASKS_Result> IIC_Tasks { get; set; }
        public IEnumerable<USP_VIEW_INVESTIGATION_SCOPE_DEFINITION_INVESTIGATIONID_Result> Inv_Group_Scope { get; set; }
        public IEnumerable<USP_VIEW_ALL_CORRESPONDENCES_Result> Corres { get; set; }
        public IEnumerable<USP_VIEW_CORRESPONDENCE_Result> corres_Det { get; set; }
        public IEnumerable<USP_VIEW_CORRES_ATTACHMENTS_Result> Corres_Attach { get; set; }
        public IEnumerable<USP_CORRES_RECIPIENTS_Result> Corres_Recepients { get; set; }
        public IEnumerable<USP_VIEW_TEAMMEMBER_Result> INV_TM { get; set; }
        public IEnumerable<USP_VIEW_ASSIGNED_IIC_Result> Assign_IIC { get; set; }
        public IEnumerable<USP_VIEW_REASSIGNED_HISTORY_IIC_Result> reassignhistory { get; set; }
        public IEnumerable<USP_LIST_IIC_INCHARGE_WITH_CASE_Result> list_iic_incharge { get; set; }
        public IEnumerable<USP_VIEW_INVESTIGATION_TASKDETAILS_INVESTIGATIONID_Result> inv_taskDetails { get; set; }
        public IEnumerable<InvestigationSafetyRecommendationCommunicationHIstory> ISRCHistory { get; set; }
        public HttpPostedFileBase[] CorresFiles { get; set; }

        public int CorrespondenceID { get; set; }
        public string CorrespondenceControlNumber { get; set; }
        public Nullable<int> InvestigationFileID { get; set; }
        public System.DateTime CorrespondenceInitiatedDate { get; set; }
        public System.DateTime LastCorrespondenceDate { get; set; }
        public string Subject { get; set; }
        public string CorrespondenceTags { get; set; }
        public System.DateTime DateCreated { get; set; }
        public System.DateTime DateUpdated { get; set; }
        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }
        public string Corr_Msg_Type { get; set; }
        public int responseStatus { get; set; }
        public System.DateTime CorrespondenceMessageInitiatedDate { get; set; }
        public string CorrespondenceMessageFrom { get; set; }
        public Nullable<int> LKCorrespondenceMessageFromRoleID { get; set; }
        public string CorrespondenceMessageOtherRoleName { get; set; }
        public Nullable<int> LKCorrespondenceMessageTypeID { get; set; }
        public string LKCorrespondenceMessageOtherTypeName { get; set; }
        public DateTime CorrespondenceDueDate { get; set; }
        public string Corresrespnsdays { get; set; } 
        public bool DAAIApprovalRequired { get; set; }
        public string DAAIApproved { get; set; }
        public string CorrespondenceContent { get; set; }
        public string SR_Number { get; set; }
        public int SR_ID { get; set; }
        public int SR_Response_ID { get; set; }
        public int LKInitialCorrespondenceMessageTypeID { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "Enter valid Email address")]
        public string CorrespondenceMessageResponseFrom { get; set; }

        public int LKDocumentTypeID { get; set; }
        public string AttachmentPublicUrl { get; set; }

        //public Nullable<int> CorrespondenceMessageRecipientUserID { get; set; }
        //public Nullable<int> CorrespondenceMessageEmailDistributionListID { get; set; }
        //public string CorrespondenceMessageRecipientEmailAddress { get; set; }

        public string InvestigationClosureReason { get; set; }
        public int InvestigationClosureStatusId { get; set; }
        public int InvestigationClosedBy { get; set; }


        public int IIC_ASSIGNED { get; set; }
        public int IIC_REASSIGNED { get; set; }
        public int IIC_ASSIGNED_HISTORY_ID { get; set; }
        public int IIC_ASSIGNED_BY { get; set; }
        public int IIC_REMOVED_BY { get; set; }
        public string IIC_REASONFORREMOVE { get; set; }

        public int OverallProgressPercentage { get; set; }    // For investigation overall percentage -- added On 28-July-2018

        public string InvestigationFileNo { get; set; }

        public string InvestigationReopenReason { get; set; }
        
        public int invAssigned { get; set; }
    }
}