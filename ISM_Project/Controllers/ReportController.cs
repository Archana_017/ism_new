﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace ISM_Project.Controllers
{
    /// <summary>
    /// Report - Controller contains document file version 
    /// </summary> 
    /// 
    public class ReportController : Controller
    {
        Common comm = new Common();
        // GET: Reports
        /// <summary>
        /// Gen Reports
        /// </summary> 
        /// 
        public ActionResult GenReports()
        {
            ViewBag.status = "";
            return View();
        }
        /// <summary>
        /// Get Version of particular File and return by JSON result
        /// </summary> 
        /// 
        public JsonResult GetVersionFiles()
        {
            Dictionary<string, string> files = new Dictionary<string, string>();
            try
            {
                files = comm.getFilesVersioning();
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("DIController", "NotifyACCID", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }
            return Json(files);
        }
    }
}