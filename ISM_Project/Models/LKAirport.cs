//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ISM_Project.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class LKAirport
    {
        public int LKAirportID { get; set; }
        public int LKAirportCountryId { get; set; }
        public string LKAirportName { get; set; }
        public System.DateTime DateCreated { get; set; }
    
        public virtual LKCountry LKCountry { get; set; }
    }
}
