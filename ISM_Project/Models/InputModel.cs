﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ISM_Project.Models
{
    public class InputModel
    {
        public int InputId { get; set; }
        public string InputName { get; set; }
    }
}