﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ISM_Project.Models
{
    public class RootCauseAnalysis
    {
        public int Selectanalysis { get; set; }
        public int InvEventAnalysisId { get; set; }
        public int InvEventAnalysisddetailId { get; set; }
        public int InvtaskgroupId { get; set; }
        public int InvtaskassignmentId { get; set; }
        public string InvEventuniqno { get; set; }
        public int Invfileid { get; set; }
        public int[] org_infl { get; set; }
        public int[] unsafe_svision_list { get; set; }
        public int[] pre_cond_list { get; set; }
        public int[] unsafe_act_list { get; set; }
        public int[] activelatentchecker { get; set; }
       // public int[] latent_org_infl { get; set; }
       // public int[] latent_unsafe_svision_list { get; set; }
       // public int[] latent_pre_cond_list { get; set; }
       //   public int[] latent_unsafe_act_list { get; set; }

    }
}