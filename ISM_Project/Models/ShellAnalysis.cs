﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ISM_Project.Models
{
    public class ShellAnalysis
    {
        public int InvestigationEventAnalysisId { get; set; }
        public int Invfileid { get; set; }
        public int[] Livewaresysproc { get; set; }
        public int[] Livewaresystraining{ get; set; }
        public int[] Livewarehardhardware { get; set; }
        public int[] Livewareinfods { get; set; }
        public int[] Livewaresoftwarehard { get; set; }
        public int[] Livewareotheroper { get; set; }
        public int[] Livewareenvphyenv { get; set; }
        public int[] LivewareenvPsycho { get; set; }
        public int[] Livewareenvcmpmgmt { get; set; }
        public int[] Livewareenvopertsk { get; set; }
        public int[] Livewareintercom { get; set; }
        public int[] Livewareinterskill { get; set; }
        public int[] Livewareintersprvsn { get; set; }
        public int[] Livewareinterregact { get; set; }
        public int[] Livewarephysical { get; set; }
        public int[] Livewarepsychological { get; set; }
        public int[] Livewarewrkldmgmt { get; set; }
        public int[] Livewareexpqual { get; set; }
        public int[] shellcheker { get; set; }
    }
}