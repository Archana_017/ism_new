﻿
function changeDateFormat(inpdate) {
    var chDT = inpdate.split(' ')[0];
    var conDate = chDT.split('/')[1] + "/" + chDT.split('/')[0] + "/" + chDT.split('/')[2];
    var convDT = new Date(conDate + " " + inpdate.split(' ')[1]);
    var c_date = convDT.getFullYear() + "/" + (convDT.getMonth() + 1) + "/" + (convDT.getDate()) + " " + convDT.getHours() + ":" + convDT.getMinutes();
    return c_date;
}

function changeDateOnlyFormat(inpdate) {
    var chDT = inpdate;
    var conDate = chDT.split('/')[1] + "/" + chDT.split('/')[0] + "/" + chDT.split('/')[2];
    var convDT = new Date(conDate);
    var c_date = convDT.getFullYear() + "/" + (convDT.getMonth() + 1) + "/" + (convDT.getDate()) ;
    return c_date;
}

function getSkillNames(element_id) {
    $.ajax
        ({
            url: '/Notifications/getSkillNames',
            type: 'GET',
            datatype: 'application/json',
            contentType: 'application/json',
            success: function (result) {
                $("#Skillname_" + element_id).html();
                $("#Skillname_" + element_id).html($('<option></option>').val(0).html('Select'));
                console.log(result);
                $.each(JSON.parse(result), function (key, item) {
                    $("#Skillname_" + element_id).append
                        ($('<option></option>').val(item.LKskillID).html(item.LKskillName));
                });

            },

            error: function () {
                alert("Something went wrong..")
            },
        });
}

$(document).ready(function () {


    //$("#dob").attr('readOnly', 'true');
    //$("#dob").keypress(function (event) { alert(event.keyCode);  event.preventDefault(); });
    $(document.body).on('keypress keydown', '#dob,.certidate,.renewdate,.noFuturedate,#DateOfReceipt,#DateOfMovement,#CorrespondenceInitiatedDate,#DateChanged', function (event) {
        event.preventDefault();

    });
    $(document.body).on('click', '.remove_certicate', function () {

        var certificate_id = $(this).attr('id').split('_');
        if (certificate_id[1]) {
            var certid = certificate_id[1]
            $("#Certificate_details_id_" + certid).remove();
            var old_count = $("#NoOfcerticate").val();
            var new_count = parseInt(old_count) - parseInt(1);
            $("#NoOfcerticate").val(new_count);
            //  $("#aircrafts_inv_no").html(new_count);
        }
    });

    $(document.body).on('click', '.remove_skill', function () {
        var skill_id = $(this).attr('id').split('_');
        if (skill_id[1]) {
            var skillid = skill_id[1]
            $("#skill_details_id_" + skillid).remove();
            var old_count = $("#NoOfskill").val();
            var new_count = parseInt(old_count) - parseInt(1);
            $("#NoOfskill").val(new_count);
            //  $("#aircrafts_inv_no").html(new_count);
        }
    });


    //$(document.body).on('dp.change', $('.renewdate').datetimepicker({ format: 'DD/MM/YYYY' }),function () {
    //    alert('oi');

    //});

    //$('.renewdate').datetimepicker({ format: 'DD/MM/YYYY' }).on('dp.change', function () {
    //    alert(this);
    //    alert($('.renewdate').val());
    //});


    $("#add_certificate").click(function (e) {

        var certificate_count = $("#NoOfcerticate").val();
        var cert_count = parseInt(certificate_count) + parseInt(1);

        var cert_info = '<div class="add-certificate-details" id="Certificate_details_id_' + cert_count + '">' +
            '<div class="form-group">' +
            '<div class="row">' +
            '<div class="col-md-3">' +
            '<label>Certification Name</label>' +
            '<input class="form-control" id="Certificates_' + cert_count + '" name="CertificateName[]" placeholder="Enter Certificate Name" required="required" type="text" value="">' +
            '</div>' +
            '<div class="col-md-3">' +
            '<label>Certified Date</label>' +
            '<div class="input-group date_only_nofuture" id="dtUTCOcc">' +
            '<input type="text" id="certifieddate_' + cert_count + '" class="form-control certidate" name="certifieddate[]" value=""/>' +
            '<span class="input-group-addon">' +
            '<span class="glyphicon glyphicon-calendar"></span>' +
            '</span>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-3">' +
            '<label>Date of Renewal</label>' +
            '<div class="input-group date_only" id="dtUTCOcc">' +
            '<input type="text" id="reneweddate_' + cert_count + '" class="form-control renewdate" name="reneweddate[]"  value=""/>' +
            '<span class="input-group-addon">' +
            ' <span class="glyphicon glyphicon-calendar"></span>' +
            '</span>' +
            '</div>' +
            '</div>' +
            //'<div class="col-md-2">' +
            //'<small>validity period</small>' +
            //'<input class="form-control" data-val="true" data-val-number="The field ValidityDays must be a number." data-val-required="The ValidityDays field is required." id="validityperiod_' + cert_count + '" name="validityperiod[]" required="required" type="text" value="" readonly="">' +
            //'</div>' +
            '<div class="col-md-1">' +
            '<label></label>' +
            '<button style="margin-top:33px;" type= "button" class="btn btn-danger btn-number remove_certicate"  id="rmcertificate_' + cert_count + '"data- type="minus"> ' +
            '<span class="glyphicon glyphicon-minus">' +
            '</span>' +
            '</button>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';
        $("#Dynamiccertificate").append(cert_info);
        $("#NoOfcerticate").val(cert_count);
    });

    $("#AddSkillTextBox").click(function (e) {


        var skill_count = $("#NoOfskill").val();
        var skillset_count = parseInt(skill_count) + parseInt(1);

        var skill_info = '<div class="add-skill-details" id="skill_details_id_' + skillset_count + '">' +
            '<div class="form-group">' +
            '<div class="row">' +
            '<div class="col-md-3">' +
            '<label>Skill Name</label>' +
            '<select name="Skillname[]" class="form-control" id="Skillname_' + skillset_count + '">' +
            '<option value="">Select</option>' +
            '</select>' +
            '</div>' +
            '<div class="col-md-3">' +
            '<label>Experience (Years)</label>' +
            '<input type="text" id="Experiance_' + skillset_count + '" name="Experiance[]" class="form-control" onkeydown="return ValidateNumber(event)" maxlength="2">' +
            '</div>' +
            '<div class="col-md-3">' +
            '<label>Rating</label>' +
            '<select name="Rating[]" class="form-control" id="Rating_' + skillset_count + '">' +
            '<option value="0">Select</option>' +
            '<option value="1">1</option>' +
            '<option value="2">2</option>' +
            '<option value="3">3</option>' +
            '<option value="4">4</option>' +
            '<option value="5">5</option>' +
            '<option value="6">6</option>' +
            '<option value="7">7</option>' +
            '<option value="8">8</option>' +
            '<option value="9">9</option>' +
            '<option value="10">10</option>' +
            '</select>' +
            '</div>' +
            '<div class="col-md-1"><button type="button" style="margin-top:33px;" class="btn btn-danger btn-number remove_skill"  id="rmskill_' + skillset_count + '" data-type="minus">' +
            '<span class="glyphicon glyphicon-minus">' +
            '</span>' +
            '</button>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';
        $("#DynamicSkillArea").append(skill_info);
        $("#NoOfskill").val(skillset_count);
        getSkillNames(skillset_count);
    });


    $(document.body).on('click', '.AircraftReg', function () {

        var k = $(this).attr('id').split('_');

        var reg_id = k[1];

        var RegistrationID = $("#LKAircraftRegistrationID_" + reg_id).val();

        $("#Add_AircraftRegistration").modal('show');

        $('#Aircraft_Registration').load('/DI/AddAircraftRegistration', { AircraftRegistrationID: parseInt(RegistrationID) },

            function (RegistrationID) {

                $("#aircid").val(reg_id);

            });
    });

    $(document.body).on('click', '.airOperName', function () {
        $("#Add_AirlineOperator").modal('show');
        $('#Airline_Operator').load('/DI/AddAirOperatorName', function () { });
    });

    $(document.body).on('click', '.AirportReg', function () {

        var k = $(this).attr('id').split('_');

        var airport_id = k[1];

        $("#Airport_Registration").modal('show');

        $('#Airport_Reg').load('/Notifications/getAirportDetailsByAirportId', { AirportId: parseInt(airport_id) }, function () { $("#aircid").val(airport_id); });
    });

    $(document.body).on('click', '.OccurCateg', function () {

        var Ocurcategory_id = $("#OccurrenceCategorizationId").val();

        if (Ocurcategory_id === '') {
            Ocurcategory_id = 0;
        }

        $("#OccurCateg_Registration").modal('show');

        $('#OccurCateg_Reg').load('/Notifications/getOccurCategoryById', { OccurCatId: parseInt(Ocurcategory_id) }, function () { $("#aircid").val(0); });
    });

    $(document.body).on('click', '.UpdateInvNo', function () {

        var Inv_No = $("#InvestigationFileNo").val();
        var Inv_Id = $("#InvestigationFileID").val();

        if (Inv_No === '') {
            $('.alert-danger').html('Investigation File No cannot be blank');
            $('.alert-danger').show();
            setTimeout(function () { $(".alert").hide(); }, 2000);
            return false;
        }
        else {
            $.ajax({
                url: "/IIC/UpdateInvNo",
                type: "POST",
                dataType: 'json',
                data: { InvId: parseInt(Inv_Id), InvNo: (Inv_No) },
                success: function (data) {                    
                    if (data == 0) {
                        $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i> Invalid Investigation File No Format </div>');
                        $('.alert-danger').show();
                        setTimeout(function () { $(".alert").hide(); }, 2000);
                    }
                    else if (data == 1) {
                        $('.alert-success').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"> Investigation File No Updated Successfully </i> </div>');
                        $('.alert-success').show();
                        setTimeout(function () { $(".alert").hide(); }, 2000);
                        window.location.href = '/IIC/IICTasks/' + parseInt(Inv_Id);
                    }
                    else if (data == 2) {
                        $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i> Investigation File No Already Exist </div>');
                        $('.alert-danger').show();
                        setTimeout(function () { $(".alert").hide(); }, 2000);
                    }
                }
            });
        }
    });

    $(document.body).on('click', '.ChangeIncident', function () {

        var inc_id = $("#Incident_TypeList:checked").val();

        var inv_id = $("#InvestigationFileID").val();

        $("#Change_IncType").modal('show');

        $('#Change_IncidentType').load('/DI/ChangeIncidentType', { Incidentid: parseInt(inc_id), InvId: parseInt(inv_id) },

            function (inc_id) {
                $("#Inciden_id").val(inc_id);
            }
        );
    });

    $(document.body).on('click', '.ResendCorrEmail', function () {
       
        var corr_id = $("#CorresID").val();
        
        $("#ResendEmail").modal('show');

        $('#Resend_Email').load('/IIC/ResendEmail', { CorrId: parseInt(corr_id) },

            function (inc_id) {
                $("#Inciden_id").val(inc_id);
            }
        );
    });

    $(document.body).on('click', '.user_Desig', function () {

        var desg_ID = $("#DesingationID").val();

        $("#Add_UserDesignation").modal('show');
        $('#User_Designation').load('/Admin/AddDesignation', { designationID: parseInt(desg_ID) },

            function (desg_ID) {
                //$("#Inciden_id").val(desg_ID);
            }
        );
    });



    $(document.body).on('click', '.assigned_members', function () {

        var TaskEvenId = $(this).attr('id');

        var invId = $("#InvFileId").val();

        $.ajax({
            url: "/Task/GetInvestigationTeamMemberList",
            type: "POST",
            dataType: 'json',
            data: { TaskEventId: parseInt(TaskEvenId), InvestigationId: parseInt(invId) },
            success: function (data) {
                $('#assg_member').html('');
                $("#show_assigned").modal('show');
                $.each(data, function (key, item) {
                    var html = '<li><span>' + item.FirstName + ' ' + item.LastName + '</span></li>';
                    $('#assg_member').append(html);
                });
            }
        });
    });

    $(document.body).on('click', '#event_status', function () {

        if ($('input[name=event_action_status]:checked').val()==1) {

            if ($("#comment").val() == "")
            {
                $('.popup_danger').html('Comments cannot be blank');
                $(".popup_danger").css({ display: "block" });
                setTimeout(function () { $('.popup_danger').hide(); }, 2000);
                return false;
            }
        }
        $('.popup_info').html('Please Wait...Processing Request');
       $(".popup_info").css({ display: "block" });

        var forminfo = {

             "invest_file_id" : $("#inv_file").val(),
             "inv_task_assignment_id" : $("#inv_task_assignment_id").val(),
               "task_grp_id" : $("#task_grp_id").val(),
               "event_uniq_no": $("#event_uniq_no").val(),
               "comment": $("#comment").val(),
           "event_action_status" : $('input[name=event_action_status]:checked').val()
        };
        

        $("#event_status").hide();
        $("#event_close").hide();

        $.ajax({
            url: "/iic/Event_Action_iic",
            type: "POST",
            data: JSON.stringify(forminfo),
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                $('.popup_info').hide();
                if (data == 0) {
                    $('.popup_success').html('Event Accepted Successfully');
                    $(".popup_success").css({ display: "block" });
                    setTimeout(function () { $('.popup_success').hide(); $("#take_action_by_iic").modal('hide'); }, 2000);
                    window.location.href = '/Task/IICeventview/'+ parseInt($("#inv_task_assignment_id").val())+'?tskgrp_id=' + parseInt($("#task_grp_id").val());
                }
                if (data == 1) {
                
                    $('.popup_success').html('Successfully Send for clarification');
                    $(".popup_success").css({ display: "block" });
                    setTimeout(function () { $('.popup_success').hide(); $("#take_action_by_iic").modal('hide'); }, 2000);
                    window.location.href = '/Task/IICeventview/' + parseInt($("#inv_task_assignment_id").val()) + '?tskgrp_id=' + parseInt($("#task_grp_id").val());
                }
                if (data == 2) {

                    $('.popup_danger').html('Session Expired');
                    $(".popup_danger").css({ display: "block" });
                    setTimeout(function () { $('.popup_danger').hide(); }, 2000);

                }
               
            }
        });
    });

    $(document.body).on('click', '#event_close', function () {

        $("#take_action_by_iic").modal('hide');

    });


    $("#Admin_form").on("submit", function (e) {
        e.preventDefault();
        $("#add_user").hide();
        $('.alert-info').html('<div class="msg-icon"><i class="fas fa-info-circle"> Processing Please Wait...</i> </div>');
        $('.alert-info').show();

        var userid = $("#user_id").val();

        var certificatename = [];

        certificatename = $("input[name='CertificateName[]'")
            .map(function () { return $(this).val(); }).get();
        var certified_date = [];
        certified_date = $("input[name='certifieddate[]'")
            .map(function () { return changeDateOnlyFormat($(this).val()); }).get();
        var Renewal_date = [];
        Renewal_date = $("input[name='reneweddate[]'")
            .map(function () { return changeDateOnlyFormat($(this).val()); }).get();

        var Skill_Name = [];
        Skill_Name = $("select[name='Skillname[]'")
            .map(function () {
                if ($(this).val() == '')
                    return 0;
                else
                    return $(this).val();
            }).get();
        var Experiance = [];
        Experiance = $("input[name='Experiance[]'")
            .map(function () { return $(this).val(); }).get();
        var Rating_ = [];
        Rating_ = $("select[name='Rating[]'")
            .map(function () {
                if ($(this).val() == '')
                    return 0;
                else
                    return $(this).val();
            }).get();
        var person = {
            "first_name": $("#first_name").val(),
            "middle_name": $("#middle_name").val(),
            "last_name": $("#last_name").val(),
            "Staff_id": $("#Staff_id").val(),
            //"dob": changeDateOnlyFormat($("#dob").val()),
            "email": $("#email").val(),
            "password": $("#password").val(),
            "phone": $("#phone").val(),
            "CerticateName": certificatename,
            "Certified_dt": certified_date,
            "Renewal_dt": Renewal_date,
            "Skillname": Skill_Name,
            "work_experience": Experiance,
            "Rating": Rating_,
            "roleid": $("#roleid").val(),
            "DesingationID": $("#DesingationID").val(),
            "is_active": $("#is_active").val(),
            "No_of_certificate": $("#NoOfcerticate").val(),
            "No_of_Skill": $("#NoOfskill").val(),
            "user_id": $("#user_id").val(),
            "ActAsDirector": $("#ActAsDirector:checked").val()
        };
        console.log(person);
        $.ajax({

            url: "/Admin/SaveAdminform",
            type: "POST",
            data: JSON.stringify(person),
            dataType: "json",
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                $("#add_user").show();
                $('.alert').hide();
                if (data == 0) {
                    $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i> Email Id already exists</div>');
                    $('.alert-danger').show();
                    setTimeout(function () { $(".alert").hide(); }, 2000);
                }
                else if (data == 1) {
                    $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"> Staff ID already exists</i> </div>');
                    $('.alert-danger').show();
                    setTimeout(function () { $(".alert").hide(); }, 2000);
                }
                else if (data == 2) {
                    $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"> Director already exists</i> </div>');
                    $('.alert-danger').show();
                    setTimeout(function () { $(".alert").hide(); }, 2000);
                }

                else if (data == 3) {
                    $('.alert-success').html('<div class="msg-icon"><i class="fas fa-info-circle"> Saved Successfully</i> </div>');
                    $('.alert-success').show();
                    setTimeout(function () { window.location.href = "/Home/Dashboard/" + userid }, 2000);

                }
                else if (data == 4) {
                    $('.alert-success').html('<div class="msg-icon"><i class="fas fa-info-circle"> User Created Successfully</i> </div>');
                    $('.alert-success').show();
                    setTimeout(function () { window.location.href = "/Home/Dashboard/" }, 2000);
                }
                else if (data == 5) {
                    $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"> Director is already active</i> </div>');
                    $('.alert-danger').show();
                    setTimeout(function () { $(".alert").hide(); }, 2000);
                }
                else if (data == 6) {
                    $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"> Atleast one admin should be active </i> </div>');
                    $('.alert-danger').show();
                    setTimeout(function () { $(".alert").hide(); }, 2000);
                }
                else if (data == 7) {
                    $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"> At a time only one user can act as DAAI</i> </div>');
                    $('.alert-danger').show();
                    setTimeout(function () { $(".alert").hide(); }, 2000);
                }
                else if (data == 8) {
                    $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"> User role Should be Investigator In-charge </i> </div>');
                    $('.alert-danger').show();
                    setTimeout(function () { $(".alert").hide(); }, 2000);
                }
                else if (data == 9) {
                    $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"> Only one user allow as DI </i> </div>');
                    $('.alert-danger').show();
                    setTimeout(function () { $(".alert").hide(); }, 2000);
                }
                else if (data == 10) {
                    $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"> Atleast one DI should be active </i> </div>');
                    $('.alert-danger').show();
                    setTimeout(function () { $(".alert").hide(); }, 2000);
                }
                else if (data == 11) {
                    $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"> ADG is already active </i> </div>');
                    $('.alert-danger').show();
                    setTimeout(function () { $(".alert").hide(); }, 2000);
                }
                else if (data == 12) {
                    $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"> Only one user allow as ADG </i> </div>');
                    $('.alert-danger').show();
                    setTimeout(function () { $(".alert").hide(); }, 2000);
                }
            },
            error: function (msg) { alert(msg); }
        });
    });

});

function clicknew() {
    sessionStorage.setItem("mode", "new");
    window.location.href = "/SR/New_SR";
}

function viewclick() {
    sessionStorage.setItem("mode", "view");
    window.location.href = "/SR/New_SR";
}

function assignInvestigator() {
    $("#btnAssign").hide();    
    var IIC = $(".ac-tm-iic:checked").val();
    var invFileID = $("#InvestigationFileID").val();
    //var tm_id = $('[name="TeammemberList[]"]:checked').val();
    var tm_id = [];
    tm_id = $("input[name='TeammemberList[]']:checked")
        .map(function () {
            return $(this).val();
        }).get();

    var invFileNO = $("#InvestigationNumber").val();
    if (invFileNO == '') {
        $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>Please Enter Investigation File Number.</div>');
        $('.alert-danger').show();
        $("#btnAssign").show();
        setTimeout(function () { $(".alert").hide(); }, 2000);
        return false;
    }

    if (tm_id == null || tm_id ==0) {
        $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>Please Select Team Member.</div>');
        $('.alert-danger').show();
        $("#btnAssign").show();
        setTimeout(function () { $(".alert").hide(); }, 2000);
        return false;
    }
    $('.alert-info').html('<div class="msg-icon"><i class="fas fa-info-circle"></i>Please Wait...Processing Request</div>');
    $('.alert-info').show();
    $.ajax({
        url: "/Director/Assign_IIC",
        type: "POST", 
        data: { IICAssigned: IIC, TeammemberList: tm_id, InvestigationNumber: invFileNO},
        success: function (data) {
            $('.alert').hide();           
            if (tm_id == null) {
                $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>Please Select Team Member.</div>');
                $('.alert-danger').show();
                $("#btnAssign").show();
                setTimeout(function () { $(".alert").hide(); }, 2000);
                return false;
            }
            else if (data == true) {
                $('.alert-success').html('<div class="msg-icon"><i class="fas fa-check-circle"></i>Assign IIC and Team Member notification email has been sent successfully</div>');
                $('.alert-success').show();
                setTimeout(function () { window.location.href = "/Director/IIC_Assigned/" + invFileID; }, 5000);
            }
            else if (data == 3) {
                $("#btnAssign").show();
                $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-check-circle"></i>Please Enter Valid Investigation File No</div>');
                $('.alert-danger').show();
                setTimeout(function () { $(".alert").hide(); }, 5000);
                return false;
            }
            else if (data == 2) {
                $("#btnAssign").show();
                $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-check-circle"></i>Investigation File Number Already Exist</div>');
                $('.alert-danger').show();
                setTimeout(function () { $(".alert").hide(); }, 5000);
                return false;
            }
            else {
                $("#btnAssign").show();
                $('.alert').hide();
                $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>There has been an issue in sending mail.</div>');
                $('.alert-danger').show();
                setTimeout(function () { $(".alert").hide(); }, 5000);
            }
        }
    });
}

//$("#btnAssign").click(function (e) {
//    e.preventDefault();
//    var tm_id = $('[name="TeammemberList[]"]:checked').val();
//    if (tm_id == null) {
//        $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>Please Select Team Member.</div>');
//        $('.alert-danger').show();
//        //setTimeout(function () { $(".alert").hide(); }, 3000);
//        return false;
//    }
//    else {
//        $("#formAssign_IIC").submit();        
//    }

//});

function notifyACCID() {
    var Inv_ID = $("#InvestigationFileID").val();
    $('.alert-info').html('<div class="msg-icon"><i class="fas fa-info-circle"></i>Processing Request</div>');
    $('.alert-info').show();
    $("#btnnotifyaccid").hide();

    $.ajax({
        url: "/DI/NotifyACCID",
        type: "POST",
        data: { Inv_Id: Inv_ID },

        success: function (data) {
            $('.alert').hide();
            if (data == true) {
                $('.alert-success').html('<div class="msg-icon"><i class="fas fa-check-circle"></i>Form 035 notification email has been sent successfully</div>');
                $('.alert-success').show();
                setTimeout(function () { window.location.href = "/Home/Dashboard"; }, 5000);
            }
            else {
                $("#btnnotifyaccid").show();
                $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>There has been an issue in sending mail.</div>');
                $('.alert-danger').show();
                setTimeout(function () { $(".alert").hide(); }, 5000);

            }
        },
        failure: function (data) {
            $("#btnnotifyaccid").show();
            $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>There has been an issue in sending mail.</div>');
            $('.alert-danger').show();
            setTimeout(function () { $(".alert").hide(); }, 5000);
        }
    });
}

function sendNotificationDL(OCC_ID) {
    $.ajax({
        url: "/Notifications/NotifyDL",
        type: "POST",
        data: { occur_Id: OCC_ID },
        success: function (data) {
            $('.alert').hide();
            if (data == true) {
                $('.alert-success').html('<div class="msg-icon"><i class="fas fa-check-circle"></i>Occurrence notification email sent successfully</div>');
                $('.alert-success').show();
                setTimeout(function () { window.location.href = "/Home/Dashboard"; }, 5000);
            }
            else {
                $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>There has been an issue in sending mail.</div>');
                $('.alert-danger').show();
                setTimeout(function () { $(".alert").hide(); }, 5000);
                $(".btn-save-init,.btn-notify").show();
            }
        }, failure: function (data) {
            $('.alert').hide();
            $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>There has been an issue in sending mail.</div>');
            $('.alert-danger').show();
            setTimeout(function () { $(".alert").hide(); }, 5000);
            $(".btn-save-init,.btn-notify").show();



        }
    });
}

//$(".interim_report").click(function () {
//    $("#interim_report_popup").modal('show');

//});

//$(".prelim_report").click(function () {
//    $("#prelim_report_popup").modal('show');

//});

$("#interimsave").click(function () {
    if ($("#Investigation_Process").val() == '') {
        $('.popup_danger').html('Please enter Investigation Process');
        $('.popup_danger').show();
        setTimeout(function () { $('.popup_danger').hide(); }, 2000);
        return false;

    }
    else if ($("#Interim_statement").val() == '') {
        $('.popup_danger').html('Please enter Interim Statement');
        $('.popup_danger').show();
        setTimeout(function () { $('.popup_danger').hide(); }, 2000);
        return false;
    }
    else {
        $('#new_interim').submit();
        $("#interim_report_popup").modal('hide');
    }
});

$("#btnPrelimActivity").click(function () {
    if ($("#Ongoing_Investigation_Activitiea").val() == '') {
        $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>Please enter Investigation Process and Interim Statement</div>');
        $('.alert-danger').show();
        setTimeout(function () { $(".alert").hide(); }, 2000);
        return false;
    }
    else {
        $('#new_prelim').submit();
        $("#prelim_report_popup").modal('hide');
    }
});

$(".teammember_profile").click(function () {
    $("#teammember_profile_assigned_popup").modal('show');
    var f = {};
    f['user_id'] = $(this).attr('data-iic');
    var usrid = parseInt($(this).attr('data-iic'));

    $("#profile_data").load('/Director/IIC_View_Profile', { userid: usrid }, function (usrid) { });
});

$(document.body).on('change', '#Selectanalysis', function () {


    var selectedID = $(this).val();
    if (selectedID == 0) {
        $(".james_xpanel1").hide();

    }

    if (selectedID == 1) {

        $.get('/iic/jamesreason/', function (data) {
            $('#analysis').html(data);
        });
    }
    if (selectedID == 3) {
        $.get('/iic/Fishbone/', function (data) {
            $('#analysis').html(data);
        });
    }
})

$(".root_analysis").click(function () {
    $("#rootanalysis_popup").modal('show');
    var task_grp_id = parseInt($(this).attr('data-taskgroupid'));
    var task_assignment_id = parseInt($(this).attr('data-taskassignment'));
    var ev_unique_nbr = parseInt($(this).attr('data-evuniquenumber'));
    var invfileid = parseInt($("#InvFileID").val());
        $.ajax({
        url: "/iic/RootAnalysisCheck",
        type: 'POST',
        datatype: 'application/json',
        data: {
            taskgroupid: task_grp_id,
            taskassignmentid: task_assignment_id,
            eventuniqno: ev_unique_nbr,
            investfileid: invfileid
        },
        success: function (data) {
            if (data == 1 || data == 0) {
                $("#analysis_data").load('/iic/AnalysisMainpage/?' + $.param({
                    userid: data,
                    taskgroupid: task_grp_id,
                    taskassignmentid: task_assignment_id,
                    eventuniqno: ev_unique_nbr,
                    investfileid: invfileid
                })
                );
            } else {
                alert('here');
            }
        }, failure: function (data) {
            alert('here2');
            console.log(data);
        }
    });
    // var f = {};
    // f['user_id'] = $(this).attr('data-iic');
    // var usrid = parseInt($(this).attr('data-iic'));
    //$("#analysis_data").load('/iic/JamesReason'/*,/* { userid: usrid }, function (usrid) { }*/);
});
$(".take_action_iic").click(function () {
    $("#take_action_by_iic").modal('show');
    $('#inv_file').val($("#InvFileID").val());
    $('#inv_task_assignment_id').val($(this).attr('data-taskassignment'));
    $('#task_grp_id').val($(this).attr('data-taskgroupid'));
    $('#event_uniq_no').val($(this).attr('data-evuniquenumber'));

    //var task_grp_id = parseInt($(this).attr('data-taskgroupid'));
    //var task_assignment_id = parseInt($(this).attr('data-taskassignment'));
    //var ev_unique_nbr = parseInt($(this).attr('data-evuniquenumber'));
    //var invfileid = parseInt($("#InvFileID").val());
    //$.ajax({
    //    url: "/iic/RootAnalysisCheck",
    //    type: 'POST',
    //    datatype: 'application/json',
    //    data: {
    //        taskgroupid: task_grp_id,
    //        taskassignmentid: task_assignment_id,
    //        eventuniqno: ev_unique_nbr,
    //        investfileid: invfileid
    //    },
    //    success: function (data) {
    //        if (data == 1 || data == 0) {
    //            $("#analysis_data").load('/iic/AnalysisMainpage/?' + $.param({
    //                userid: data,
    //                taskgroupid: task_grp_id,
    //                taskassignmentid: task_assignment_id,
    //                eventuniqno: ev_unique_nbr,
    //                investfileid: invfileid
    //            })
    //            );
    //        } else {
    //            alert('here');
    //        }
    //    }, failure: function (data) {
    //        alert('here2');
    //        console.log(data);
    //    }
    //});
    // var f = {};
    // f['user_id'] = $(this).attr('data-iic');
    // var usrid = parseInt($(this).attr('data-iic'));
    //$("#analysis_data").load('/iic/JamesReason'/*,/* { userid: usrid }, function (usrid) { }*/);
});
$(".iic_profile").click(function () {
    $("#iic_assigned_popup").modal('show');
    var f = {};
    f['user_id'] = $(this).attr('data-iic');
    var usrid = parseInt($(this).attr('data-iic'));
    $("#profile_data").load('/Director/IIC_View_Profile', { userid: usrid }, function (usrid) { });
});

$(".iicprofile").click(function () {
    $("#iic_assignedpopup").modal('show');
    var f = {};
    f['user_id'] = $(this).attr('data-iic');
    var usrid = parseInt($(this).attr('data-iic'));
    $("#profile_data").load('/Task/IIC_ViewProfile', { userid: usrid }, function (usrid) { });
});

$("#addmovements").click(function () {
    $("#Eve_Movements").modal('show');
    var f = {};
    f['evd_id'] = $(this).attr('data-iic');
    var evd_id = parseInt($(this).attr('data-iic'));
    $('#Movements_data').load('/Task/AddNewMovement', { EvMovId: parseInt(evd_id) }, function (evd_id) { });
});

//$(".AircraftReg").click(function () {
//    $("#Add_AircraftRegistration").modal('show');   
//    $('#Aircraft_Registration').load('/DI/AddAircraftRegistration', function () { });
//});

$(".EditMove").click(function () {
    $("#Eve_Movements").modal('show');
    var f = {};
    f['evd_id'] = $(this).attr('data-iic');
    var evd_id = parseInt($(this).attr('data-iic'));
    $('#Movements_data').load('/Task/AddNewMovement', { EvMovId: parseInt(evd_id) }, function (evd_id) { });
});

$("#global_search_main").click(function () {
    var Search_Text = $("#SearchText").val();
    if (Search_Text != '') {
        window.location.href = '/HOME/GlobalSearch/' + (null) + '?SearchText=' + (Search_Text) + '';
    }
});

$(".glob_search").keypress(function (e) {
    var keycode = (e.keyCode ? e.keyCode : e.which);
    if (keycode == '13') {
        var Search_Text = $("#SearchText").val();
        if (Search_Text != '') {
            window.location.href = '/HOME/GlobalSearch/' + (null) + '?SearchText=' + (Search_Text) + '';
        }
    }
});

$("#clear_search").click(function () {

    location.reload(true);

});

function ReassignIIC() {
    var ReasonforRemoval = $("#IIC_REASONFORREMOVE").val();
    var IIC = $('[name="IIC_REASSIGNED"]:checked').val();

    if (ReasonforRemoval == '') {
        $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>Please enter reason for removal</div>');
        $('.alert-danger').show();
        $("#IIC_REASONFORREMOVE").focus();
        setTimeout(function () { $(".alert").hide(); }, 2000);
        return false;
    }
    else if (IIC == null) {
        $('.alert-danger').html('<div class="msg-icon"><i class="fas fa-exclamation-triangle"></i>Please select IIC</div>');
        $('.alert-danger').show();
        setTimeout(function () { $(".alert").hide(); }, 2000);
        return false;
    }
    else {
        $('[name="IIC_REASSIGNED"]:checked').val();

        $("#formReAssignIIC").submit();

        $('.alert-info').html('<div class="msg-icon"><i class="fas fa-info-circle"></i>Processing Request</div>');
        $('.alert-info').show();
        $("#BtnReAssignIIC").hide();
    }
}
 