﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ISM_Project.Models
{
    public class InvScope
    {
        public string[] invScopeId { get; set; }
        public string[] invScopeStatus { get; set; }
        public bool[] invScopeStatusChk { get; set; }
        public string[] invTaskGroupId { get; set; }

        public Int32 InvestigationId { get; set; }
        public string LocalDateAndTimeOfOccurrence { get; set; }
        public string UTCDateAndTimeOfOccurrence { get; set; }

        public string NotifierName { get; set; }
        public string NotifierOrganization { get; set; }

        public string EventUniqueName { get; set; }
        public string EventUniqueNumber { get; set; }
        public string InvestigationTaskGroupName { get; set; }
        public string InvestigationTaskGroupEventName { get; set; }
        public string UniqueNumber { get; set; }

        public int InvestigationTaskGroupEventID { get; set; }
        public DateTime DateTimeofCompletion { get; set; }
        //  public string TeamMemberId { get; set; }
        public string[] TeamMemberId { get; set; }

        public string[] Team_member_assign_status { get; set; }

        public string assign { get; set; }
        public string InvestigationGroup { get; set; }

        public string ReasonForTeammemberRemoval { get; set; }
        public string TeammemberName { get; set; }
        public string TeammemberRole { get; set; }

        public string[] multipleTeammemberName {get;set;}

        public string[] multipleTeammemberRole { get; set; }

        public int[] multipleAssignedTeam_mbr_id { get; set; }

        public int[] multipleUnassignedTeam_mbr_id { get; set; }

        public string ControlFrom { get; set; }

        public string PreTaskEventStatus { get; set; }
    }
}