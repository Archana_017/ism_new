﻿using Microsoft.SharePoint.Client;
using Microsoft.SharePoint.Client.Sharing;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using ISM_Project.Models;
using System.Security;
using TheArtOfDev.HtmlRenderer.PdfSharp;

namespace ISM_Project
{
    /// <summary>
    /// Common class  share the frequenntly using some funtion especially  sharepoint save ,default folder creation , datetime conversion and Error log etc
    /// </summary> 
    /// 
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1001:TypesThatOwnDisposableFieldsShouldBeDisposable")]
    public class Common
    {
        gcaa_ismEntities dc = new gcaa_ismEntities();

        /// <summary>
        /// Datetime converted to string format
        /// </summary> 
        /// 
        ///<param name="dt">Receive date time value</param>
        public string convertDateTime(DateTime dt)
        {
            string conv_Time = string.Empty;
            if (dt != null)
                conv_Time = Convert.ToDateTime(dt).ToString("dd-MM-yyyy HH:mm:ss");
            //conv_Time = Convert.ToDateTime(dt).ToString("yyyy-MM-dd HH:mm:");
            return conv_Time;
        }

        /// <summary>
        /// Datetime converted to date in string format
        /// </summary> 
        /// 
        ///<param name="dt">Receive date time value</param>
        public string convertDate(DateTime dt)
        {
            string conv_Time = string.Empty;
            if (dt != null)
                conv_Time = Convert.ToDateTime(dt).ToString("dd-MM-yyyy");
            return conv_Time;
        }

        /// <summary>
        /// Password genrator method used to generate new password for newly create user through our application
        /// </summary> 
        /// 
        public string Passwordgenrator()
        {

            string allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789!@$?_-";
            char[] chars = new char[8];
            Random rd = new Random();

            for (int i = 0; i < 8; i++)
            {
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }
            string pwd = new string(chars);
            return pwd;
        }
        /// <summary>
        /// Exception Log are written to database table
        /// </summary> 
        /// 
        ///<param name="Controller">Receive name of the controller or class</param>
        ///<param name="Action">Receive action name </param>
        ///<param name="Exception">Receive Exception message</param>
        ///<param name="User_Id">Receive the current user id</param>
        public void Exception_Log(string Controller, string Action, string Exception, int User_Id)
        {
            dc.USP_INST_EXCEPTIONS(Controller, Action, Exception, User_Id);
            dc.SaveChanges();
        }
        /// <summary>
        /// Get SharePoint deafult  Values from database
        /// </summary> 
        /// 
        public string[] getSharePointValues()
        {
            string[] SharePointData = new string[5];
            var SPData = dc.USP_GET_SHAREPOINT_DATA(1).FirstOrDefault();
            SharePointData[0] = SPData.SharePointUrl;
            SharePointData[1] = SPData.UserName;
            SharePointData[2] = SPData.Password;
            return SharePointData;
        }
        /// <summary>
        /// Get the mail details from database like server name and credentials
        /// </summary> 
        /// 
        public string[] getMailValues()
        {
            string[] MailDetails = new string[10];
            var MailData = dc.USP_GET_MAIL_DETAILS(1).FirstOrDefault();
            MailDetails[0] = MailData.ServerName;
            MailDetails[1] = Convert.ToString(MailData.Port);
            MailDetails[2] = MailData.UserName;
            MailDetails[3] = MailData.Password;
            MailDetails[4] = MailData.FromEmail;
            return MailDetails;
        }
        /// <summary>
        /// File upload to sharepoint server - input file path and stream of data
        /// </summary> 
        /// <param name="filepath">Receive file path of attched file</param>
        /// <param name="Inv_ID">Receive investigation file id</param>
        /// <param name="ms">Receive memory stream object</param>
        public string[] savetoSharePoint(Stream ms, string filepath, int Inv_ID)
        {
            string[] uploadedUrl;
            try
            {
                // using (var clientContext = new ClientContext("http://sharepoint/sites/sharepoint"))
                using (var clientContext = new ClientContext("https://xmindsinfotech.sharepoint.com"))
                {
                    //NetworkCredential credentials = new NetworkCredential(@"ad.x-minds.info\xminds", "xminds123");
                    SecureString passWord = new SecureString();
                    foreach (char c in "xminds@123".ToCharArray()) passWord.AppendChar(c);
                    var credentials = new SharePointOnlineCredentials("admin@xmindsinfotech.onmicrosoft.com", passWord);
                    clientContext.Credentials = credentials;
                    var list = clientContext.Web.Lists.GetByTitle("Documents");
                    clientContext.Load(list.RootFolder);
                    clientContext.ExecuteQuery();
                    var fi = new FileInfo(filepath);
                    var fileUrl = String.Format("{0}/{1}", list.RootFolder.ServerRelativeUrl, fi.Name);
                    FileCreationInformation fileInfo = new FileCreationInformation();
                    //fileInfo.Content = System.IO.File.ReadAllBytes(filepath);
                    fileInfo.ContentStream = new FileStream(filepath, FileMode.Open);
                    fileInfo.Url = fileUrl;
                    fileInfo.Overwrite = true;
                    //Microsoft.SharePoint.Client.File.SaveBinaryDirect(clientContext, fileUrl, ms, true);
                    Microsoft.SharePoint.Client.File uploadedFile = list.RootFolder.Files.Add(fileInfo);
                    ListItem listItem = uploadedFile.ListItemAllFields;
                    listItem["Investigation_ID"] = Inv_ID;
                    listItem.Update();
                    clientContext.ExecuteQuery();
                    clientContext.Load(uploadedFile);
                    clientContext.Load(clientContext.Web);
                    clientContext.ExecuteQuery();
                    string absoluteFileUrl = clientContext.Web.Url + uploadedFile.ServerRelativeUrl;
                    clientContext.Load(uploadedFile, f => f.ListItemAllFields);
                    clientContext.ExecuteQuery();
                    //uploadedUrl = clientContext.Web.Url + uploadedFile.ListItemAllFields["FileRef"];
                    //for getting the anonymous access link
                    string Anonymouslink = clientContext.Web.CreateAnonymousLinkForDocument(absoluteFileUrl, ExternalSharingDocumentOption.View);
                    uploadedUrl = new string[] { absoluteFileUrl, Anonymouslink };
                }
            }
            catch (System.Exception ex)
            {
                uploadedUrl = new string[] { "" };
                Exception_Log("Common", "savetoSharePoint", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(HttpContext.Current.Session["UserId"]));
            }
            return uploadedUrl;
        }
        #region Upload Correspondence and Evidence Attachments
        /// <summary>
        /// File upload to sharepoint server - input file path and stream of data with document library
        /// </summary>
        /// <param name="ms">Receive memory stream data</param>
        /// <param name="filepath">Receive file path</param>
        /// <param name="Inv_ID">Receive investigation file id</param>       
        /// <param name="doc_llibrary">Receive investigation document name</param>
        /// <returns></returns>
        public string[] savetoSharePointDocs(Stream ms, string filepath, string Inv_ID, string doc_llibrary)
        {
            string[] uploadedUrl;
            try
            {
                // using (var clientContext = new ClientContext("http://sharepoint/sites/sharepoint"))
                using (var clientContext = new ClientContext("https://xmindsinfotech.sharepoint.com"))
                {
                    //NetworkCredential credentials = new NetworkCredential(@"ad.x-minds.info\xminds", "xminds123");
                    SecureString passWord = new SecureString();
                    foreach (char c in "xminds@123".ToCharArray()) passWord.AppendChar(c);
                    var credentials = new SharePointOnlineCredentials("admin@xmindsinfotech.onmicrosoft.com", passWord);
                    clientContext.Credentials = credentials;
                    var list = clientContext.Web.Lists.GetByTitle(doc_llibrary);
                    var folder = list.RootFolder;
                    clientContext.Load(folder);
                    clientContext.ExecuteQuery();
                    var fi = new FileInfo(filepath);
                    var fileUrl = String.Format("{0}/{1}", folder.ServerRelativeUrl, fi.Name);
                    FileCreationInformation fileInfo = new FileCreationInformation();
                    //fileInfo.Content = System.IO.File.ReadAllBytes(filepath);
                    fileInfo.ContentStream = new FileStream(filepath, FileMode.Open);
                    fileInfo.Url = fileUrl;
                    fileInfo.Overwrite = true;
                    //Microsoft.SharePoint.Client.File.SaveBinaryDirect(clientContext, fileUrl, ms, true);
                    Microsoft.SharePoint.Client.File uploadedFile = list.RootFolder.Files.Add(fileInfo);
                    ListItem listItem = uploadedFile.ListItemAllFields;
                    listItem["Control_Num"] = Inv_ID;
                    listItem.Update();
                    clientContext.ExecuteQuery();
                    clientContext.Load(uploadedFile);
                    clientContext.Load(clientContext.Web);
                    clientContext.ExecuteQuery();
                    string absoluteFileUrl = clientContext.Web.Url + uploadedFile.ServerRelativeUrl;

                    clientContext.Load(uploadedFile, f => f.ListItemAllFields);
                    clientContext.ExecuteQuery();
                    //for getting anonymous link in online
                    string Anonymouslink = clientContext.Web.CreateAnonymousLinkForDocument(absoluteFileUrl, ExternalSharingDocumentOption.View);
                    uploadedUrl = new string[] { absoluteFileUrl, Anonymouslink };
                }
            }
            catch (System.Exception ex)
            {
                uploadedUrl = new string[] { "" }; ;
                Exception_Log("Common", "savetoSharePointDocs", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(HttpContext.Current.Session["UserId"]));
            }
            return uploadedUrl;
        }
        #endregion
        #region Event Docs
        /// <summary>
        /// To upload the events attachments in the Events Document Library
        /// 
        /// </summary>
        /// <param name="ms">Receive memory stream data</param>
        /// <param name="filepath">Receive file path</param>
        /// <param name="Inv_ID">Receive investigation file id</param>
        /// <param name="folder1">folder Location</param>
        /// <param name="folder2">folder Location</param>
        /// <param name="doc_llibrary">Receive investigation document name</param>
        /// <returns></returns>
        public string[] SharePointEventDocs(Stream ms, string filepath, string Inv_ID, string folder1, string folder2, string doc_llibrary)
        {
            string[] uploadedUrl;
            try
            {
                // using (var clientContext = new ClientContext("http://sharepoint/sites/sharepoint"))
                using (var clientContext = new ClientContext("https://xmindsinfotech.sharepoint.com"))
                {
                    //NetworkCredential credentials = new NetworkCredential(@"ad.x-minds.info\xminds", "xminds123");
                    SecureString passWord = new SecureString();
                    foreach (char c in "xminds@123".ToCharArray()) passWord.AppendChar(c);
                    var credentials = new SharePointOnlineCredentials("admin@xmindsinfotech.onmicrosoft.com", passWord);
                    clientContext.Credentials = credentials;
                    var list = clientContext.Web.Lists.GetByTitle(doc_llibrary);
                    var folder = list.RootFolder;
                    clientContext.Load(folder);
                    clientContext.ExecuteQuery();
                    string folderUrl = string.Empty;
                    folder = folder.Folders.Add(folder1);
                    folder = folder.Folders.Add(folder2);
                    clientContext.ExecuteQuery();
                    clientContext.Load(folder);
                    clientContext.ExecuteQuery();
                    folderUrl = folder.ServerRelativeUrl;
                    var fi = new FileInfo(filepath);
                    var fileUrl = String.Format("{0}/{1}", folderUrl, fi.Name);
                    FileCreationInformation fileInfo = new FileCreationInformation();
                    //fileInfo.Content = System.IO.File.ReadAllBytes(filepath);
                    fileInfo.ContentStream = new FileStream(filepath, FileMode.Open);
                    fileInfo.Url = fileUrl;
                    fileInfo.Overwrite = true;
                    ////Microsoft.SharePoint.Client.File.SaveBinaryDirect(clientContext, fileUrl, ms, true);
                    Microsoft.SharePoint.Client.File uploadedFile = list.RootFolder.Files.Add(fileInfo);
                    ListItem listItem = uploadedFile.ListItemAllFields;
                    listItem["Investigation_ID"] = Inv_ID;
                    listItem.Update();
                    clientContext.ExecuteQuery();
                    clientContext.Load(uploadedFile);
                    clientContext.Load(clientContext.Web);
                    clientContext.ExecuteQuery();
                    string absoluteFileUrl = clientContext.Web.Url + uploadedFile.ServerRelativeUrl;

                    clientContext.Load(uploadedFile, f => f.ListItemAllFields);
                    clientContext.ExecuteQuery();
                    //Print List Item Id
                    //   uploadedUrl = clientContext.Web.Url + uploadedFile.ListItemAllFields["FileRef"];
                    string Anonymouslink = clientContext.Web.CreateAnonymousLinkForDocument(absoluteFileUrl, ExternalSharingDocumentOption.View);
                    uploadedUrl = new string[] { absoluteFileUrl, Anonymouslink };
                }
            }
            catch (System.Exception ex)
            {
                uploadedUrl = new string[] { "" };
                Exception_Log("Common", "SharePointEventDocs", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(HttpContext.Current.Session["UserId"]));
            }
            return uploadedUrl;
        }
        #endregion
        /// <summary>
        ///When we attach a file in application it will automatically create a folder ,if the folder name is missing in application hosted location create automatically
        /// </summary> 
        /// 
        /// <param name="path">Receive file path</param>
        public void CreateIfMissing(string path)
        {
            bool folderExists = Directory.Exists(path);
            if (!folderExists)
                Directory.CreateDirectory(path);
        }
        /// <summary>
        /// Delete the Directory in application hosted location after the file successfuly save in sharepoint and database
        /// </summary> 
        /// 
        /// <param name="path">Receive file path</param>
        public void DeleteDirectory(string path)
        {
            foreach (string filename in Directory.GetFiles(path))
            {
                System.IO.File.Delete(filename);
            }
            foreach (string subfolder in Directory.GetDirectories(path))
            {
                DeleteDirectory(subfolder);
            }
            // Directory.Delete(path);
        }
        #region Download files from SharePoint
        /// <summary>
        /// downloads file from sharepoint
        /// It is required in Correspondence mail notifiication for external users
        /// documents uploaded in correspondence needs to be sent as attachment in mail.
        /// created by Aishwarya on 14-05-2018        
        /// </summary>
        /// <param name="docLibName"></param>
        /// <param name="RefNo"></param>
        //For other attachemnts which are in root folders
        public string[] getFiles(string docLibName, string RefNo)
        {
            string[] files = new string[50];
            try
            {
                using (var clientContext = new ClientContext("https://xmindsinfotech.sharepoint.com"))
                {
                    SecureString passWord = new SecureString();
                    foreach (char c in "xminds@123".ToCharArray()) passWord.AppendChar(c);
                    var credentials = new SharePointOnlineCredentials("admin@xmindsinfotech.onmicrosoft.com", passWord);
                    clientContext.Credentials = credentials;
                    List list = clientContext.Web.Lists.GetByTitle(docLibName);
                    clientContext.Load(list);
                    clientContext.ExecuteQuery();
                    CamlQuery query = new CamlQuery();
                    //query.ViewXml = "<view/>";
                    query.ViewXml = "<View>" + "<Query>" + "<Where><Eq><FieldRef Name='Control_Num' /><Value Type='Text'>" + RefNo + "</Value></Eq></Where>" + "</Query>" + "</View>";
                    ListItemCollection licoll = list.GetItems(query);
                    clientContext.Load(licoll);
                    clientContext.ExecuteQuery();
                    int filecount = 0;
                    foreach (ListItem li in licoll)
                    {
                        Microsoft.SharePoint.Client.File file = li.File;
                        if (file != null)
                        {
                            clientContext.Load(file);
                            clientContext.ExecuteQuery();
                            FileInformation fileInfo = Microsoft.SharePoint.Client.File.OpenBinaryDirect(clientContext, file.ServerRelativeUrl);
                            CreateIfMissing(HttpContext.Current.Server.MapPath("~/") + "CorrespondenceAttachments");
                            var fileName = HttpContext.Current.Server.MapPath(Path.Combine("~/CorrespondenceAttachments", (string)li.File.Name));
                            using (var fileStream = System.IO.File.Create(fileName))
                            {
                                fileInfo.Stream.CopyTo(fileStream);
                            }
                            files[filecount] = fileName;
                            filecount++;
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                files[0] = string.Empty;
                Exception_Log("Common", "getFiles", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(HttpContext.Current.Session["UserId"]));
            }
            return files;
        }
        /// <summary>
        /// For Event attachments which are in subfolders
        /// </summary> 
        ///         
        public string getEventOrOtherFiles(string url)
        {
            string files = string.Empty;
            try
            {
                using (var clientContext = new ClientContext("https://xmindsinfotech.sharepoint.com"))
                {
                    SecureString passWord = new SecureString();
                    foreach (char c in "xminds@123".ToCharArray()) passWord.AppendChar(c);
                    var credentials = new SharePointOnlineCredentials("admin@xmindsinfotech.onmicrosoft.com", passWord);
                    clientContext.Credentials = credentials;
                    clientContext.Load(clientContext.Web);
                    clientContext.ExecuteQuery();
                    int filecount = 0;
                    Microsoft.SharePoint.Client.File file = clientContext.Web.GetFileByUrl(url);
                    clientContext.Load(file);
                    clientContext.ExecuteQuery();
                    if (file != null)
                    {
                        FileInformation fileInfo = Microsoft.SharePoint.Client.File.OpenBinaryDirect(clientContext, file.ServerRelativeUrl);
                        CreateIfMissing(HttpContext.Current.Server.MapPath("~/") + "EventsorCorresAttachment");
                        var fileName = HttpContext.Current.Server.MapPath(Path.Combine("~/EventsorCorresAttachment", (string)file.Name));
                        using (var fileStream = System.IO.File.Create(fileName))
                        {
                            fileInfo.Stream.CopyTo(fileStream);
                            files = fileName;
                            filecount++;
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                files = string.Empty;
                Exception_Log("Common", "getFiles", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(HttpContext.Current.Session["UserId"]));
            }
            return files;
        }
        /// <summary>
        /// get Files On Prem  used for save the attached files to sharepoint
        /// </summary> 
        /// 
        public string[] getFilesOnPrem(string docLibName, string RefNo)
        {
            string[] files = new string[50];
            try
            {
                string[] spdata = getSharePointValues();

                using (var clientContext = new ClientContext(spdata[0]))
                {
                    NetworkCredential credentials = new NetworkCredential(spdata[1], spdata[2]);
                    clientContext.Credentials = credentials;
                    List list = clientContext.Web.Lists.GetByTitle(docLibName);
                    clientContext.Load(list);
                    clientContext.ExecuteQuery();
                    CamlQuery query = new CamlQuery();
                    //query.ViewXml = "<view/>";
                    query.ViewXml = "<View>" + "<Query>" + "<Where><Eq><FieldRef Name='Control_Num' /><Value Type='Text'>" + RefNo + "</Value></Eq></Where>" + "</Query>" + "</View>";
                    ListItemCollection licoll = list.GetItems(query);
                    clientContext.Load(licoll);
                    clientContext.ExecuteQuery();
                    int filecount = 0;
                    foreach (ListItem li in licoll)
                    {
                        Microsoft.SharePoint.Client.File file = li.File;
                        if (file != null)
                        {
                            clientContext.Load(file);
                            clientContext.ExecuteQuery();
                            FileInformation fileInfo = Microsoft.SharePoint.Client.File.OpenBinaryDirect(clientContext, file.ServerRelativeUrl);
                            CreateIfMissing(HttpContext.Current.Server.MapPath("~/") + "CorrespondenceAttachments");
                            var fileName = HttpContext.Current.Server.MapPath(Path.Combine("~/CorrespondenceAttachments", (string)li.File.Name));
                            using (var fileStream = System.IO.File.Create(fileName))
                            {
                                fileInfo.Stream.CopyTo(fileStream);
                            }
                            files[filecount] = fileName;
                            filecount++;
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                files[0] = string.Empty;
                Exception_Log("Common", "getFilesOnPrem", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(HttpContext.Current.Session["UserId"]));
            }
            return files;
        }
        /// <summary>
        /// Get Event Files Or Other Files On Prem  used for save the attached files to sharepoint most commonly used for SR,Evidence management and correspondence
        /// </summary> 
        /// 
        public string getEventFilesOrOtherFilesOnPrem(string url)
        {
            string files = string.Empty;
            try
            {
                string[] spdata = getSharePointValues();
                using (var clientContext = new ClientContext(spdata[0]))
                {

                    NetworkCredential credentials = new NetworkCredential(spdata[1], spdata[2]);
                    clientContext.Credentials = credentials;
                    clientContext.Load(clientContext.Web);
                    clientContext.ExecuteQuery();
                    int filecount = 0;
                    Uri serverUrl = new Uri(url);
                    Microsoft.SharePoint.Client.File file = clientContext.Web.GetFileByServerRelativeUrl(serverUrl.AbsolutePath);
                    clientContext.Load(file);
                    clientContext.ExecuteQuery();
                    if (file != null)
                    {
                        FileInformation fileInfo = Microsoft.SharePoint.Client.File.OpenBinaryDirect(clientContext, file.ServerRelativeUrl);
                        CreateIfMissing(HttpContext.Current.Server.MapPath("~/") + "EventsAttachmentReports");
                        var fileName = HttpContext.Current.Server.MapPath(Path.Combine("~/EventsAttachmentReports", (string)file.Name));
                        using (var fileStream = System.IO.File.Create(fileName))
                        {
                            fileInfo.Stream.CopyTo(fileStream);
                            files = fileName;
                            filecount++;
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                files = string.Empty;
                Exception_Log("Common", "getEventFilesOrOtherFilesOnPrem", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(HttpContext.Current.Session["UserId"]));
            }
            return files;
        }
        #endregion
        #region save docs in SharePoint On Premise
        /// <summary>
        /// save to Share Point based on investigation file id 
        /// </summary> 
        /// 
        /// <param name="ms">Receive memory stream data</param>
       /// <param name="filename">Receive file path</param>
       /// <param name="Inv_ID">Receive investigation file id</param>
        public string[] savetoSharePointInvOnPrem(Stream ms, string filename, int Inv_ID)
        {
            string[] uploadedUrl;
            try
            {
                string[] spdata = getSharePointValues();
                using (ClientContext clientContext = new ClientContext(spdata[0]))
                {
                    NetworkCredential credentials = new NetworkCredential(spdata[1], spdata[2]);
                    clientContext.Credentials = credentials;
                    Web web = clientContext.Web;
                    clientContext.Load(web);
                    clientContext.ExecuteQuery();
                    var list = clientContext.Web.Lists.GetByTitle("Documents");
                    clientContext.Load(list.RootFolder);
                    clientContext.ExecuteQuery();
                    var fi = new FileInfo(filename);
                    var fileUrl = String.Format("{0}/{1}", list.RootFolder.ServerRelativeUrl, fi.Name);
                    FileCreationInformation fileInfo = new FileCreationInformation();
                    fileInfo.ContentStream = ms;
                    fileInfo.Url = fileUrl;
                    fileInfo.Overwrite = true;
                    Microsoft.SharePoint.Client.File uploadedFile = list.RootFolder.Files.Add(fileInfo);
                    ListItem listItem = uploadedFile.ListItemAllFields;
                    listItem["Investigation_ID"] = Inv_ID;
                    listItem.Update();
                    //uploadedFile.CheckOut();
                    uploadedFile.CheckIn("", CheckinType.MajorCheckIn);
                    clientContext.ExecuteQuery();
                    clientContext.Load(uploadedFile);
                    clientContext.Load(clientContext.Web);
                    clientContext.ExecuteQuery();
                    string absoluteFileUrl = clientContext.Web.Url + uploadedFile.ServerRelativeUrl;
                    clientContext.Load(uploadedFile, f => f.ListItemAllFields);
                    clientContext.ExecuteQuery();
                    uploadedUrl = new string[] { clientContext.Web.Url + uploadedFile.ListItemAllFields["FileRef"], clientContext.Web.Url + uploadedFile.ListItemAllFields["FileRef"] };
                }
            }
            catch (System.Exception ex)
            {
                uploadedUrl = new string[] { "" };
                Exception_Log("Common", "savetoSharePointInvOnPrem", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(HttpContext.Current.Session["UserId"]));
            }
            return uploadedUrl;
        }
        /// <summary>
        /// save to Share Point Other Docs On Prem used for save the attached files to sharepoint
        /// </summary> 
        /// 
        /// <param name="ms">Receive memory stream data</param>
        /// <param name="fileName">Receive file path</param>
        /// <param name="Inv_ID">Receive investigation file id</param>
        /// <param name="doc_llibrary">Receive investigation document name</param>
        public string[] savetoSharePointOtherDocsOnPrem(Stream ms, string fileName, string Inv_ID, string doc_llibrary)
        {
            string[] uploadedUrl;
            try
            {
                string[] spdata = getSharePointValues();
                using (var clientContext = new ClientContext(spdata[0]))
                {
                    //NetworkCredential credentials = new NetworkCredential(@"ad.x-minds.info\xminds", "xminds123");
                    NetworkCredential credentials = new NetworkCredential(spdata[1], spdata[2]);
                    clientContext.Credentials = credentials;
                    var list = clientContext.Web.Lists.GetByTitle(doc_llibrary);
                    var folder = list.RootFolder;
                    clientContext.Load(folder);
                    clientContext.ExecuteQuery();
                    var fi = new FileInfo(fileName);
                    var fileUrl = String.Format("{0}/{1}", folder.ServerRelativeUrl, fi.Name);
                    FileCreationInformation fileInfo = new FileCreationInformation();
                    fileInfo.ContentStream = ms;
                    fileInfo.Url = fileUrl;
                    fileInfo.Overwrite = true;
                    Microsoft.SharePoint.Client.File uploadedFile = list.RootFolder.Files.Add(fileInfo);
                    ListItem listItem = uploadedFile.ListItemAllFields;
                    listItem["Control_Num"] = Inv_ID;
                    listItem.Update();
                    uploadedFile.CheckIn("", CheckinType.MajorCheckIn);
                    clientContext.ExecuteQuery();
                    clientContext.Load(uploadedFile);
                    clientContext.Load(clientContext.Web);
                    clientContext.ExecuteQuery();
                    clientContext.Load(uploadedFile, f => f.ListItemAllFields);
                    clientContext.ExecuteQuery();
                    uploadedUrl = new string[] { clientContext.Web.Url + uploadedFile.ListItemAllFields["FileRef"], clientContext.Web.Url + uploadedFile.ListItemAllFields["FileRef"] };
                }
            }
            catch (System.Exception ex)
            {
                uploadedUrl = new string[] { "" }; ;
                Exception_Log("Common", "savetoSharePointOtherDocsOnPrem", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(HttpContext.Current.Session["UserId"]));
            }
            return uploadedUrl;
        }
        /// <summary>
        /// save to Share Point Other Docs On Prem used for save the attached files to sharepoint most commonly used for investigation report
        /// </summary> 
        /// 
        /// <param name="ms">Receive memory stream data</param>
        /// <param name="fileName">Receive file path</param>
        /// <param name="Inv_ID">Receive investigation file id</param>
        /// <param name="doc_llibrary">Receive investigation document name</param>
        public string[] savetoSharePointReportsOnPrem(Stream ms, string fileName, string Inv_ID, string doc_llibrary)
        {
            string[] uploadedUrl;
            try
            {
                string[] spdata = getSharePointValues();

                using (var clientContext = new ClientContext(spdata[0]))
                {
                    NetworkCredential credentials = new NetworkCredential(spdata[1], spdata[2]);
                    clientContext.Credentials = credentials;
                    var list = clientContext.Web.Lists.GetByTitle(doc_llibrary);
                    var folder = list.RootFolder;
                    clientContext.Load(folder);
                    clientContext.ExecuteQuery();
                    folder = folder.Folders.Add(Inv_ID);
                    clientContext.ExecuteQuery();
                    string folderUrl = string.Empty;
                    clientContext.Load(folder);
                    clientContext.ExecuteQuery();
                    folderUrl = folder.ServerRelativeUrl;
                    var fi = new FileInfo(fileName);
                    var fileUrl = String.Format("{0}/{1}", folderUrl, fi.Name);
                    FileCreationInformation fileInfo = new FileCreationInformation();
                    fileInfo.ContentStream = ms;
                    fileInfo.Url = fileUrl;
                    fileInfo.Overwrite = true;
                    Microsoft.SharePoint.Client.File uploadedFile = list.RootFolder.Files.Add(fileInfo);
                    uploadedFile.CheckIn("", CheckinType.MajorCheckIn);
                    clientContext.ExecuteQuery();
                    clientContext.Load(uploadedFile);
                    clientContext.Load(clientContext.Web);
                    clientContext.ExecuteQuery();
                    string absoluteFileUrl = clientContext.Web.Url + uploadedFile.ServerRelativeUrl;
                    clientContext.Load(uploadedFile, f => f.ListItemAllFields);
                    clientContext.ExecuteQuery();
                    uploadedUrl = new string[] { clientContext.Web.Url + uploadedFile.ListItemAllFields["FileRef"], clientContext.Web.Url + uploadedFile.ListItemAllFields["FileRef"] };
                }
            }
            catch (System.Exception ex)
            {
                uploadedUrl = new string[] { "" }; ;
                Exception_Log("Common", "savetoSharePointReportsOnPrem", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(HttpContext.Current.Session["UserId"]));
            }
            return uploadedUrl;
        }
        /// <summary>
        /// save to Share Point Event Docs On Prem used for save the attached files to sharepoint most commonly used for event attachment
        /// </summary> 
        /// 
        /// <param name="ms">Receive memory stream data</param>
        /// <param name="fileName">Receive file path</param>
        /// <param name="Inv_ID">Receive investigation file id</param>
        /// <param name="folder1">Receive investigation folder name</param>
        /// <param name="folder2">Receive investigation folder name</param>
        /// <param name="doc_llibrary">Receive investigation document name</param>
        public string[] savetoSharePointEventDocsOnPrem(Stream ms, string fileName, string Inv_ID, string folder1, string folder2, string doc_llibrary)
        {
            string[] uploadedUrl;
            try
            {
                string[] spdata = getSharePointValues();
                using (var clientContext = new ClientContext(spdata[0]))
                {
                    NetworkCredential credentials = new NetworkCredential(spdata[1], spdata[2]);
                    clientContext.Credentials = credentials;
                    var list = clientContext.Web.Lists.GetByTitle(doc_llibrary);
                    var folder = list.RootFolder;
                    clientContext.Load(folder);
                    clientContext.ExecuteQuery();
                    string folderUrl = string.Empty;
                    folder = folder.Folders.Add(folder1);
                    folder = folder.Folders.Add(folder2);
                    clientContext.ExecuteQuery();
                    clientContext.Load(folder);
                    clientContext.ExecuteQuery();
                    folderUrl = folder.ServerRelativeUrl;
                    var fi = new FileInfo(fileName);
                    var fileUrl = String.Format("{0}/{1}", folderUrl, fi.Name);
                    FileCreationInformation fileInfo = new FileCreationInformation();
                    fileInfo.ContentStream = ms;
                    fileInfo.Url = fileUrl;
                    fileInfo.Overwrite = true;
                    Microsoft.SharePoint.Client.File uploadedFile = list.RootFolder.Files.Add(fileInfo);
                    ListItem listItem = uploadedFile.ListItemAllFields;
                    listItem["Investigation_ID"] = Inv_ID;
                    listItem.Update();
                    uploadedFile.CheckIn("", CheckinType.MajorCheckIn);
                    clientContext.ExecuteQuery();
                    clientContext.Load(uploadedFile);
                    clientContext.Load(clientContext.Web);
                    clientContext.ExecuteQuery();
                    string absoluteFileUrl = clientContext.Web.Url + uploadedFile.ServerRelativeUrl;

                    clientContext.Load(uploadedFile, f => f.ListItemAllFields);
                    clientContext.ExecuteQuery();
                    uploadedUrl = new string[] { absoluteFileUrl, absoluteFileUrl };
                }
            }
            catch (System.Exception ex)
            {
                uploadedUrl = new string[] { "" };
                Exception_Log("Common", "SharePointEventDocsOnPrem", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(HttpContext.Current.Session["UserId"]));
            }
            return uploadedUrl;
        }
        /// <summary>
        /// delete the saved file in Share Point 
        /// </summary> 
        /// 
        /// <param name="fileUrl">Receive investigation file url</param>
        public bool deleteFiles(string fileUrl)
        {
            bool Isdeleted = false;
            try
            {
                string[] spdata = getSharePointValues();
                using (var clientContext = new ClientContext(spdata[0]))
                {
                    NetworkCredential credentials = new NetworkCredential(spdata[1], spdata[2]);
                    clientContext.Credentials = credentials;
                    Web web = clientContext.Web;
                    clientContext.Load(web);
                    clientContext.ExecuteQuery();
                    Uri serverUrl = new Uri(fileUrl);
                    Microsoft.SharePoint.Client.File f = web.GetFileByServerRelativeUrl(serverUrl.AbsolutePath);
                    f.DeleteObject();
                    clientContext.ExecuteQuery(); // Delete file here but throw Exception                
                    Isdeleted = true;
                }
            }
            catch (System.Exception ex)
            {
                Isdeleted = false;
                Exception_Log("Common", "deleteFiles", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(HttpContext.Current.Session["UserId"]));
            }
            return Isdeleted;
        }
        /// <summary>
        /// This method is used for investigation save Large Files upload section
        /// </summary> 
        /// 
        /// <param name="ms">Receive memory stream data</param>
        /// <param name="fileName">Receive file path</param>
        /// <param name="Inv_ID">Receive investigation file id</param>
        /// <param name="doc_llibrary">Receive investigation document name</param>
        public string[] savetoSharePointLargeFilessOnPrem(Stream ms, string fileName, int Inv_ID,string doc_llibrary)
        {
            string[] uploadedUrl;
            try
            {
                string[] spdata = getSharePointValues();
                using (var clientContext = new ClientContext(spdata[0]))
                {
                    NetworkCredential credentials = new NetworkCredential(spdata[1], spdata[2]);
                    clientContext.Credentials = credentials;
                    var list = clientContext.Web.Lists.GetByTitle(doc_llibrary);
                    var folder = list.RootFolder;
                    clientContext.Load(folder);
                    clientContext.ExecuteQuery();
                    string folderUrl = string.Empty;
                    //  if (existingFolder == 0)
                    // {
                    //folder = folder.Folders.Add(folder1);
                    //folder = folder.Folders.Add(folder2);
                    clientContext.ExecuteQuery();
                    clientContext.Load(folder);
                    clientContext.ExecuteQuery();
                    folderUrl = folder.ServerRelativeUrl;
                    //string filePath = fileName.Split('\\')[4];
                    var fi = new FileInfo(fileName);
                    var fileUrl = String.Format("{0}/{1}", folderUrl, fi.Name);
                    FileCreationInformation fileInfo = new FileCreationInformation();
                    fileInfo.ContentStream = ms;
                    fileInfo.Url = fileUrl;
                    fileInfo.Overwrite = true;

                    clientContext.RequestTimeout = 3600000;
                    //destStream = new MemoryStream(byteArr);
                    //}
                    Microsoft.SharePoint.Client.File.SaveBinaryDirect(clientContext,fileUrl, ms, true);
                    Exception_Log("Common", "savetoSharePointLargeFilessOnPrem", "MemoryStream::Size : " +ms.Length + "MEmoryStream:Position : " + ms.Position,4);
                    

                    //Microsoft.SharePoint.Client.File uploadedFile = list.RootFolder.Files.Add(fileInfo);
                    //  var uploadedFile = clientContext.Web.GetFileByServerRelativeUrl(fileUrl);
                    //ListItem listItem = uploadedFile.ListItemAllFields;
                    //listItem["Investigation_ID"] = Inv_ID;
                    //listItem.Update();
                    // uploadedFile.CheckIn("", CheckinType.MajorCheckIn);
                    //clientContext.Load(uploadedFile);
                    clientContext.Load(clientContext.Web);
                    clientContext.ExecuteQuery();
                    string absoluteFileUrl = clientContext.Web.Url ;
                    //clientContext.Load(uploadedFile, f => f.ListItemAllFields);
                    //clientContext.ExecuteQuery();
                    uploadedUrl = new string[] { absoluteFileUrl, absoluteFileUrl };
                }
            }
            catch (System.Exception ex)
            {
                uploadedUrl = new string[] { "" };
                Exception_Log("Common", "savetoSharePointLargeFilessOnPrem", "Message : " + ex.Message + "InnerException : " + ex.StackTrace + " Line No : " + ex.StackTrace, 4);
            }
            return uploadedUrl;
        }
        /// <summary>
        /// get files with versioning for save files to sharepoint before check already exist or not.
        /// </summary> 
        ///         
        public Dictionary<string, string> getFilesVersioning()
        {
            string[] files = new string[50];
            Dictionary<string, string> KeyVerFiles = new Dictionary<string, string>();
            try
            {
                string[] spdata = getSharePointValues();
                using (var clientContext = new ClientContext(spdata[0]))
                {
                    NetworkCredential credentials = new NetworkCredential(spdata[1], spdata[2]);
                    clientContext.Credentials = credentials;
                    List list = clientContext.Web.Lists.GetByTitle("Documents");
                    clientContext.Load(list);
                    clientContext.ExecuteQuery();


                    CamlQuery query = new CamlQuery();
                    //query.ViewXml = "<view/>";
                    query.ViewXml = "<View>" + "<Query>" + "<Where><Eq><FieldRef Name='Investigation_ID' /><Value Type='Text'>" + 19 + "</Value></Eq></Where>" + "</Query>" + "</View>";
                    ListItemCollection licoll = list.GetItems(query);
                    clientContext.Load(licoll);
                    clientContext.ExecuteQuery();
                    int filecount = 0;
                    foreach (ListItem li in licoll)
                    {
                        Microsoft.SharePoint.Client.File file = li.File;
                        if (file != null)
                        {
                            clientContext.Load(file);
                            clientContext.ExecuteQuery();
                            var versions = file.Versions;
                            clientContext.Load(versions);

                            var oldVersions = clientContext.LoadQuery(versions.Where(v => v != null));
                            clientContext.ExecuteQuery();

                            if (oldVersions != null)
                            {
                                foreach (Microsoft.SharePoint.Client.FileVersion _version in oldVersions)
                                {
                                    clientContext.Load(_version, item => item.VersionLabel);
                                    clientContext.ExecuteQuery();

                                    clientContext.Load(clientContext.Web);
                                    clientContext.ExecuteQuery();
                                    //wc.DownloadFile(clientContext.Web.Url + "/"+_version.Url, filePath);
                                    files[filecount] = clientContext.Web.Url + "/" + _version.Url;
                                    KeyVerFiles.Add(Path.GetFileNameWithoutExtension(file.Name) + "_" + _version.VersionLabel, clientContext.Web.Url + "/" + _version.Url);
                                    filecount++;
                                }
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                files[0] = string.Empty;
                Exception_Log("Common", "getFiles", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(HttpContext.Current.Session["UserId"]));
            }
            return KeyVerFiles;
        }
        /// <summary>
        /// Pdf Sharp file Convert using PdfGenerateConfig
        /// </summary>
        /// <param name="html"> Receive Html input</param>
        /// <returns> PDF file</returns>
        /// 
        public Byte[] PdfSharpConvert(String html)
        {
            PdfGenerateConfig config = new PdfGenerateConfig();
            config.PageOrientation = PdfSharp.PageOrientation.Portrait;
            //config.ManualPageSize = new PdfSharp.Drawing.XSize(1080, 828);
            config.PageSize = PdfSharp.PageSize.A4;
            Byte[] res = null;
            using (MemoryStream ms = new MemoryStream())
            {
                var pdf = TheArtOfDev.HtmlRenderer.PdfSharp.PdfGenerator.GeneratePdf(html, config);
                pdf.Save(ms);
                res = ms.ToArray();
            }
            return res;
        }
        #endregion
    }
}