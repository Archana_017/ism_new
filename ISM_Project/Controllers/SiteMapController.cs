﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ISM_Project.Controllers
{
    /// <summary>
    /// SiteMap - Controller
    /// </summary> 
    /// 
    public class SiteMapController : Controller
    {
        // GET: SiteMap
        /// <summary>
        /// Site_Map view page
        /// </summary> 
        /// 
        public ActionResult Site_Map()
        {
            return View();
        }
    }
}