using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ISM_Project.Models;
using System.Text;
using Newtonsoft.Json;

namespace ISM_Project.Controllers
{
    /// <summary>
    /// Notify - Controller contains functionality of occurrence notify used for API 
    /// </summary> 
    /// 
    public class NotifyController : ApiController
    {
        MailConfiguration mail = new MailConfiguration();
        /// <summary>
        /// default get method
        /// </summary> 
        /// 
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        /// <summary>
        /// Occurrence aircraft information and accident / incident details pass throught this api 
        /// </summary> 
        /// <param name="occur">Receive notification details as object</param>
        public HttpResponseMessage Post([FromBody] Occurrence_detail occur)
        {
            using (gcaa_ismEntities api = new gcaa_ismEntities())
                {
                DateTime UTCdatetimeofaccident = new DateTime();
                if (occur.LocalDateAndTimeOfOccurrence == null)
                {
                    occur.LocalDateAndTimeOfOccurrence = DateTime.Now;
                     UTCdatetimeofaccident = occur.LocalDateAndTimeOfOccurrence.Value.ToUniversalTime();
                }
                else
                {
                    UTCdatetimeofaccident = occur.LocalDateAndTimeOfOccurrence.Value.ToUniversalTime();
                }

                if (Convert.ToInt32(HttpContext.Current.Session["UserId"]) > 0)
                {                    

                    if (occur.OccurrenceDetailID == 0)
                    {
                        System.Data.Entity.Core.Objects.ObjectParameter OccId = new System.Data.Entity.Core.Objects.ObjectParameter("OccId", typeof(Int32));

                        api.USP_API_INST_OCCURENCE(occur.NoofAircraft, Convert.ToInt32(HttpContext.Current.Session["UserId"]), occur.LKOccurrenceStatusTypeId,
                            occur.LKIncidentTypeID, occur.LKOccurrenceInformerID, occur.PlaceofIncident,occur.DatetimeofCall, occur.LocalDateAndTimeOfOccurrence,
                            UTCdatetimeofaccident, occur.PrevailingWeatherConditions, occur.ExtendOfDamageToAircraft, occur.DamagesToGroundObjects, 
                            occur.DescriptionOfExplosivesAndDangerousArticles, occur.DescriptionOfOccurrence,Convert.ToInt32(HttpContext.Current.Session["UserId"]),
                            OccId, occur.Notifier,occur.Notifier_Contact,occur.DIsecurityNotify,occur.NotificationDescription,
                            occur.OccurrenceCategorizationId,occur.UpdatedInformation,occur.NotificationId);

                        for (int i = 0; i < occur.NoofAircraft; i++)
                        {
                            api.USP_API_INST_OCCURENCE_AIRCRAFT_DETAIL(Convert.ToInt32(OccId.Value), occur.LKAircraftModelID[i], occur.LKAircraftcategoryid[i], occur.LKDepartureAirportID[i], occur.LKDestinationAirportID[i],
                            Convert.ToString(occur.MaximumMass[i]), occur.LKAircraftRegistrationID[i], occur.EngineInfo[i], occur.StateofRegistry[i], occur.LongLat[i], occur.FlightNumber[i], occur.LKAirlineOperatorID[i]
                            , occur.stateofoperatorid[i], occur.OperatorContacts[i], (occur.PassengerOnboard[i]), (occur.CrewOnboard[i]), occur.PassengersKilled[i], occur.CrewsKilled[i], occur.OthersKilled[i],
                           occur.PassengersWithFatalInjury[i], occur.CrewsWithFatalInjury[i], occur.OthersWithFatalInjury[i], occur.PassengersWithSeriousInjury[i], occur.CrewsWithSeriousInjury[i], occur.OthersWithSeriousInjury[i],
                           occur.PassengersWithMinorInjury[i], occur.CrewsWithMinorInjury[i], occur.OthersWithMinorInjury[i], occur.msn[i], Convert.ToInt32(HttpContext.Current.Session["UserId"]),occur.CabinCrew[i]
                           ,occur.TotalCrewsInjuries[i],occur.TotalPassengersInjuries[i],occur.LKDivertedToAirportID[i]);                          
                        }                        

                        HttpResponseMessage message = Request.CreateResponse(HttpStatusCode.Created, Convert.ToInt32(OccId.Value));
                        message.Content = new StringContent(Convert.ToString(Convert.ToInt32(OccId.Value)), Encoding.Unicode);
                       
                        return message;
                    }
                    else
                    {
                        System.Data.Entity.Core.Objects.ObjectParameter OccId = new System.Data.Entity.Core.Objects.ObjectParameter("OccId", typeof(Int32));
                                               

                        api.USP_API_UPDT_OCCURENCE(occur.OccurrenceDetailID, occur.LKOccurrenceStatusTypeId, occur.PlaceofIncident, occur.LKIncidentTypeID,occur.DatetimeofCall, occur.LocalDateAndTimeOfOccurrence, UTCdatetimeofaccident, null,
                            null,null,null,occur.DescriptionOfOccurrence, occur.LKOccurrenceInformerID, Convert.ToInt32(HttpContext.Current.Session["UserId"]), OccId, occur.NoofAircraft, occur.Notifier,
                            occur.Notifier_Contact, occur.DIsecurityNotify,occur.NotificationDescription,occur.OccurrenceCategorizationId,occur.UpdatedInformation,occur.NotificationId);

                        api.USP_API_DEL_OCCURENCE_AIRCRAFTDETAIL(occur.OccurrenceDetailID);

                        for (int i = 0; i < occur.NoofAircraft; i++)
                        {
                            api.USP_API_INST_OCCURENCE_AIRCRAFT_DETAIL(Convert.ToInt32(OccId.Value), occur.LKAircraftModelID[i], occur.LKAircraftcategoryid[i], occur.LKDepartureAirportID[i], occur.LKDestinationAirportID[i],
                           Convert.ToString(occur.MaximumMass[i]), occur.LKAircraftRegistrationID[i], occur.EngineInfo[i], occur.StateofRegistry[i], occur.LongLat[i], occur.FlightNumber[i], occur.LKAirlineOperatorID[i]
                            , occur.stateofoperatorid[i], occur.OperatorContacts[i], (occur.PassengerOnboard[i]), (occur.CrewOnboard[i]), occur.PassengersKilled[i], occur.CrewsKilled[i], occur.OthersKilled[i],
                           occur.PassengersWithFatalInjury[i], occur.CrewsWithFatalInjury[i], occur.OthersWithFatalInjury[i], occur.PassengersWithSeriousInjury[i], occur.CrewsWithSeriousInjury[i], occur.OthersWithSeriousInjury[i],
                           occur.PassengersWithMinorInjury[i], occur.CrewsWithMinorInjury[i], occur.OthersWithMinorInjury[i], occur.msn[i],
                           Convert.ToInt32(HttpContext.Current.Session["UserId"]),occur.CabinCrew[i],occur.TotalCrewsInjuries[i],occur.TotalPassengersInjuries[i],occur.LKDivertedToAirportID[i]);
                        }

                        var message = Request.CreateResponse(HttpStatusCode.Created, Convert.ToInt32(OccId.Value));
                        int val = Convert.ToInt32(OccId.Value);
                      
                        return message;
                    }
                }
                else
                {
                    var message = Request.CreateResponse(HttpStatusCode.Created,"401");
                    return message;
                }
            }
        }

    }
}
