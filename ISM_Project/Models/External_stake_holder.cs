﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ISM_Project.Models
{
    public class ExternalStake
    {
 
        public IEnumerable<USP_GET_EXTERNAL_STAKE_HOLDERSNAME_Result> Ext_stk_holder { get; set; }
        public IEnumerable<USP_GET_EXTERNAL_STAKE_HOLDERSNAME_FOR_EMAIL_Result> Ext_stk_holder_email { get; set; }
              public string[] FirstName_ { get; set; }

        public string[] LastName_ { get; set; }

        public string[] Email_id { get; set; }

        public string[] OperatorName { get; set; }

    }

}