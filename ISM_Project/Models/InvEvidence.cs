﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ISM_Project.Models
{
    public class InvEvidence
    {
        public string InvestigationNumber { get; set; }
        public string LocalDateAndTimeOfOccurrence { get; set; }
        public string UTCDateAndTimeOfOccurrence { get; set; }
        public string NotifierName { get; set; }
        public string NotifierOrganization { get; set; }

        public int InvestigationEvidenceID { get; set; }
        public int InvestigationFileID { get; set; }
        public string EvidenceIdentificationNumber { get; set; }
        public string EvidencePartNumber { get; set; }
        public string EvidenceSerialNumber { get; set; }
        public int LKEvidenceCategoryID { get; set; }
        public int LKEvidenceTypeID { get; set; }
        public string EvidenceName { get; set; }
        public string EvidenceManufacturer { get; set; }
        public string EvidenceDescription { get; set; }
        public string LocationOfStorage { get; set; }
        public string EvidenceTags { get; set; }
        public Int32 EvidenceCurrentlyWith { get; set; }
        public System.DateTime? DateOfReceipt { get; set; }

        public int InvestigationEvidenceMovementID { get; set; }
        public int EvidenceMovementType { get; set; }
        public short EvidenceReturned { get; set; }
        public string PersonHandingOverEvidence { get; set; }
        public string MPersonHandingOverEvidence { get; set; }
        public string OrganizationHandingOverEvidence { get; set; }
        public string MOrganizationHandingOverEvidence { get; set; }        
        public System.DateTime? DateOfMovement { get; set; }
        public string PersonReceivingEvidence { get; set; }
        public string MPersonReceivingEvidence { get; set; }
        public string OrganizationReceivingEvidence { get; set; }
        public string CustodianOfEvidence { get; set; }
        public string MCustodianOfEvidence { get; set; }
        public int? LKShipmentModeID { get; set; }
        public string ShipmentDetails { get; set; }

        public string EvidenceTypeName { get; set; }

        public string SearchText { get; set; }


        public IEnumerable<USP_VIEW_INVESTIGATION_EVIDENCE_DETAILS_Result> InvEveden { get; set; }

        public HttpPostedFileBase[] EvidenceFiles { get; set; }

        public int evidenceAssigned { get; set; }

    }
}