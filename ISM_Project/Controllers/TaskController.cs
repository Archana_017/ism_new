﻿using DocumentFormat.OpenXml.Wordprocessing;
using ISM_Project.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using PagedList;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Xml.Linq;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using A1 = DocumentFormat.OpenXml.Drawing;
using DW = DocumentFormat.OpenXml.Drawing.Wordprocessing;
using PIC = DocumentFormat.OpenXml.Drawing.Pictures;
using System.Drawing;

namespace ISM_Project.Controllers
{
    /// <summary>
    /// Task Controller - Contains all functionality of Investigation Team management, Scope definition,Task assignment,Evidence Management,Close Investigation
    /// </summary>  
    public class TaskController : Controller
    {
        gcaa_ismEntities dc = new gcaa_ismEntities();
        Common comm = new Common();
        string[] attachments = new string[50];
        MailConfiguration mail = new MailConfiguration();
        static int investigation_fileid = 0; static int InvestigationEvidenceMovementId = 0; static int InvestigationEvidenceId = 0;
        static string[] SharepointfilePath = new string[100]; static string[] SharepointAnonymousPath = new string[100];

        /// <summary>
        /// Listing the investigation task groups based on the particular investigation
        /// </summary>  
        /// <param name="id">Receive investigation file id</param>
        /// 
        public ActionResult Defn_Inv_Scope(int id = 0)
        {
            InvScope scop = new InvScope();

            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 3 || Convert.ToInt32(Session["RoleId"]) == 6 || Convert.ToInt32(Session["RoleId"]) == 7)
                {
                    var CheckInvPermission = dc.InvestigationIICAssignments.AsEnumerable().Where(a => a.InvestigationFileID == id).FirstOrDefault();

                    if ((CheckInvPermission.IICAssigned == Convert.ToInt32(Session["UserId"]) &&

                                           CheckInvPermission.InvestigationFileID == id) || Convert.ToInt32(Session["RoleId"]) == 3 || Convert.ToInt32(Session["RoleId"]) == 6 || Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 7)
                    {
                        TempData["BackPage"] = "yes";
                        TempData["active_url_Defn_Inv_Scope"] = "tm";
                        TempData["active_url_Defn_Inv_Scope_iic"] = "iic";
                        TempData["lf_menu"] = "1";
                        TempData["active_url_manuals_iic"] = "not-iic";


                        var InvInfo = dc.USP_VIEW_INVESTIGATION_INVESTIGATIONID(id).FirstOrDefault();

                        if (InvInfo != null)
                        {
                            scop.InvestigationId = InvInfo.InvestigationFileID;

                            TempData["PageHead"] = InvInfo.InvestigationFileNumber;

                            Session["InvFileID"] = InvInfo.InvestigationFileID;

                            TempData["InvId"] = InvInfo.InvestigationFileID;
                            TempData["InvFileID"] = InvInfo.InvestigationFileID;

                            scop.LocalDateAndTimeOfOccurrence = InvInfo.LocalDateAndTimeOfOccurrence.ToString();

                            scop.UTCDateAndTimeOfOccurrence = InvInfo.UTCDateAndTimeOfOccurrence.ToString();

                            scop.NotifierName = InvInfo.NotifierName;

                            scop.NotifierOrganization = InvInfo.NotifierOrganization;

                            var Investigation_Scope = dc.USP_VIEW_INVESTIGATION_SCOPE_DEFINITION_INVESTIGATIONID(id).ToList();

                            if (Investigation_Scope.Count == 0 || Investigation_Scope == null)
                            {
                                var Inv_Scope = dc.USP_VIEW_INVESTIGATION_SCOPE_DEFINITION(id).ToList();

                                ViewBag.InvestigationScope = Inv_Scope;
                            }
                            else
                            {
                                ViewBag.InvestigationScope = Investigation_Scope;
                            }

                            TempData["id"] = id;

                            var invInfo = dc.USP_VIEW_INVESTIGATION_FORM35(id).FirstOrDefault();

                            TempData["INV_STATUS"] = invInfo.LKInvestigationStatusTypeID;
                        }
                        else
                        {
                            return RedirectToAction("PageNotFound", "Error");
                        }
                    }
                    else
                    {

                    }
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("TASKController", "Defn_Inv_Scope(int id = 0)", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }

            return View(scop);
        }
        /// <summary>
        /// Save the investigation task groups details based on the investigation
        /// </summary>        
        /// <param name="scope">Receive investigation scope identification details</param>
        [HttpPost]
        public ActionResult Defn_Inv_Scope(InvScope scope)
        {
            //var DbTransaction = dc.Database.BeginTransaction();

            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 3 || Convert.ToInt32(Session["RoleId"]) == 7)
                {
                    var result = 0;

                    int TaskGroupInScope = 1; string[] StaticEvents = { "E1", "E2", "E64", "E65", "E66" };

                    for (int i = 0; i < scope.invScopeId.Length; i++)
                    {
                        if (Convert.ToInt32(scope.invScopeStatus[i]) == 2)
                        {
                            TaskGroupInScope = 0;

                            //// Check If Scope Already Define Or Not and If The corresponding Event get Started.

                            //if (Convert.ToInt32(scope.invScopeId[i]) > 0)
                            //{
                            //    var ScopeInfo = dc.InvestigationScopeDefinitions.AsEnumerable().Where(a => a.InvestigationFileID == scope.InvestigationId

                            //                    && a.InvestigationScopeDefinitionID == Convert.ToInt32(scope.invScopeId[i])).FirstOrDefault();

                            //    if (ScopeInfo != null)
                            //    {
                            //        if (ScopeInfo.InvestigationTaskGroupInScope == 1)
                            //        {
                            //            if (ScopeInfo.NumberOfSubEvents != ScopeInfo.NumberOfUnassignedEvents)
                            //            {
                            //                TaskGroupInScope = 1;
                            //            }
                            //        }
                            //    }
                            //}                                                        

                            if (Convert.ToInt32(scope.invScopeId[i]) > 0)
                            {
                                var ScopeInfo = dc.InvestigationScopeDefinitions.AsEnumerable().Where(a => a.InvestigationFileID == scope.InvestigationId

                                                && a.InvestigationScopeDefinitionID == Convert.ToInt32(scope.invScopeId[i])).FirstOrDefault();

                                if (ScopeInfo != null)
                                {
                                    if (ScopeInfo.InvestigationTaskGroupInScope == 1)
                                    {
                                        if (ScopeInfo.NumberOfSubEvents != ScopeInfo.NumberOfUnassignedEvents)
                                        {
                                            var CheckTaskDetail = dc.InvestigationTaskDetails.AsEnumerable().Where(a => a.InvestigationFileID == scope.InvestigationId

                                                                  && a.InvestigationTaskGroupID != ScopeInfo.InvestigationTaskGroupID).FirstOrDefault();

                                            if (CheckTaskDetail != null) // Update Scope Definition columns value set as Zero & Update Event Group Id
                                            {
                                                var CheckTaskAssign = dc.InvestigationTaskAssignments.AsEnumerable().Where(a => a.InvestigationFileID == scope.InvestigationId

                                                                  && a.InvestigationTaskGroupID == CheckTaskDetail.InvestigationTaskGroupID).FirstOrDefault();

                                                var TaskDetail = dc.InvestigationTaskDetails.AsEnumerable().Where(a => a.InvestigationFileID == scope.InvestigationId

                                                                  && a.InvestigationTaskGroupID == Convert.ToInt32(scope.invTaskGroupId[i])).ToList();

                                                var AssignDetail = dc.InvestigationTaskAssignments.AsEnumerable().Where(a => a.InvestigationFileID == scope.InvestigationId

                                                                  && a.InvestigationTaskGroupID == Convert.ToInt32(scope.invTaskGroupId[i])).ToList();

                                                if (TaskDetail != null)
                                                {
                                                    for (int j = 0; j < TaskDetail.Count; j++)
                                                    {
                                                        string UniqueNo = Convert.ToString(TaskDetail[j].InvestigationTaskGroupEventUniqueNumber);

                                                        var TaskGroupEventInfoId = dc.InvestigationTaskGroupEvents.AsEnumerable().Where(a => a.InvestigationTaskGroupID == CheckTaskAssign.InvestigationTaskGroupID

                                                                                                         && a.InvestigationTaskGroupEventUniqueNumber == UniqueNo).FirstOrDefault();

                                                        int TaskGroupEventId = 0;

                                                        if (TaskGroupEventInfoId != null)
                                                        {
                                                            TaskGroupEventId = TaskGroupEventInfoId.InvestigationTaskGroupEventID;
                                                        }

                                                        if (StaticEvents.Contains(UniqueNo)) // update
                                                        {
                                                            if (TaskGroupEventId == 0)
                                                            {
                                                                //var Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(scope.InvestigationId,

                                                                //                                                             Convert.ToInt32(scope.invScopeId[i]),

                                                                //                                                             Convert.ToInt32(CheckTaskDetail.InvestigationTaskGroupID),

                                                                //                                                             TaskGroupEventId,

                                                                //                                                             Convert.ToInt32(TaskDetail[j].InvestigationTaskStatusID),

                                                                //                                                             Convert.ToInt32(AssignDetail[j].InvestigationTaskAssignmentID),

                                                                //                                                             Convert.ToInt32(Session["UserId"]),

                                                                //                                                             "DELETE");


                                                                var CheckIfStaticEvent = dc.InvestigationTaskDetails.AsEnumerable().Where(a => a.InvestigationFileID == scope.InvestigationId

                                                                               && a.InvestigationTaskGroupEventUniqueNumber == UniqueNo

                                                                               ).FirstOrDefault();
                                                                if (CheckIfStaticEvent != null)
                                                                {
                                                                    if (CheckIfStaticEvent.InvestigationTaskStatus == 1)
                                                                    {
                                                                        if (StaticEvents.Contains(UniqueNo))
                                                                        {
                                                                            if (UniqueNo == "E2")
                                                                            {
                                                                                int[] E2TaskGroupId = { 7, 9, 10, 11, 12, 13, 14 };

                                                                                var Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(scope.InvestigationId, 0, 0, 0, Convert.ToInt32(TaskDetail[j].InvestigationTaskStatusID), Convert.ToInt32(AssignDetail[j].InvestigationTaskAssignmentID), Convert.ToInt32(Session["UserId"]), "DELETECOMPLETED");

                                                                                foreach (var TaskGrpId in E2TaskGroupId)
                                                                                {
                                                                                    Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(scope.InvestigationId, 0, TaskGrpId, 0, Convert.ToInt32(TaskDetail[j].InvestigationTaskStatusID), Convert.ToInt32(AssignDetail[j].InvestigationTaskAssignmentID), Convert.ToInt32(Session["UserId"]), "UPDATECOMPLETED");
                                                                                }
                                                                            }
                                                                            else if (UniqueNo == "E1" || UniqueNo == "E64" || UniqueNo == "E65" || UniqueNo == "E66")
                                                                            {
                                                                                var Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(scope.InvestigationId, 0, Convert.ToInt32(CheckTaskDetail.InvestigationTaskGroupID), 0, Convert.ToInt32(TaskDetail[j].InvestigationTaskStatusID), Convert.ToInt32(AssignDetail[j].InvestigationTaskAssignmentID), Convert.ToInt32(Session["UserId"]), "DELETECOMPLETED");

                                                                                for (int k = 1; k <= 14; k++)
                                                                                {
                                                                                    Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(scope.InvestigationId, 0, k, 0, Convert.ToInt32(TaskDetail[j].InvestigationTaskStatusID), Convert.ToInt32(AssignDetail[j].InvestigationTaskAssignmentID), Convert.ToInt32(Session["UserId"]), "UPDATECOMPLETED");
                                                                                }
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            var Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(scope.InvestigationId, 0, Convert.ToInt32(CheckTaskDetail.InvestigationTaskGroupID), 0, Convert.ToInt32(TaskDetail[j].InvestigationTaskStatusID), Convert.ToInt32(AssignDetail[j].InvestigationTaskAssignmentID), Convert.ToInt32(Session["UserId"]), "DELETECOMPLETED");

                                                                            Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(scope.InvestigationId, 0, Convert.ToInt32(scope.invTaskGroupId[i]), 0, Convert.ToInt32(TaskDetail[j].InvestigationTaskStatusID), Convert.ToInt32(AssignDetail[j].InvestigationTaskAssignmentID), Convert.ToInt32(Session["UserId"]), "UPDATECOMPLETED");
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        if (StaticEvents.Contains(UniqueNo))
                                                                        {
                                                                            if (UniqueNo == "E2")
                                                                            {
                                                                                int[] E2TaskGroupId = { 7, 9, 10, 11, 12, 13, 14 };

                                                                                var Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(scope.InvestigationId, 0, 0, 0, Convert.ToInt32(TaskDetail[j].InvestigationTaskStatusID), Convert.ToInt32(AssignDetail[j].InvestigationTaskAssignmentID), Convert.ToInt32(Session["UserId"]), "DELETENOTCOMPLETED");

                                                                                foreach (var TaskGrpId in E2TaskGroupId)
                                                                                {
                                                                                    Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(scope.InvestigationId, 0, TaskGrpId, 0, Convert.ToInt32(TaskDetail[j].InvestigationTaskStatusID), Convert.ToInt32(AssignDetail[j].InvestigationTaskAssignmentID), Convert.ToInt32(Session["UserId"]), "UPDATENOTCOMPLETED");
                                                                                }
                                                                            }
                                                                            else if (UniqueNo == "E1" || UniqueNo == "E64" || UniqueNo == "E65" || UniqueNo == "E66")
                                                                            {
                                                                                var Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(scope.InvestigationId, 0, 0, 0, Convert.ToInt32(TaskDetail[j].InvestigationTaskStatusID), Convert.ToInt32(AssignDetail[j].InvestigationTaskAssignmentID), Convert.ToInt32(Session["UserId"]), "DELETENOTCOMPLETED");

                                                                                for (int k = 1; k <= 14; k++)
                                                                                {
                                                                                    Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(scope.InvestigationId, 0, k, 0, Convert.ToInt32(TaskDetail[j].InvestigationTaskStatusID), Convert.ToInt32(AssignDetail[j].InvestigationTaskAssignmentID), Convert.ToInt32(Session["UserId"]), "UPDATENOTCOMPLETED");
                                                                                }
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            var Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(scope.InvestigationId, 0, 0, 0, Convert.ToInt32(TaskDetail[j].InvestigationTaskStatusID), Convert.ToInt32(AssignDetail[j].InvestigationTaskAssignmentID), Convert.ToInt32(Session["UserId"]), "DELETENOTCOMPLETED");

                                                                            Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(scope.InvestigationId, 0, Convert.ToInt32(scope.invTaskGroupId[i]), 0, Convert.ToInt32(TaskDetail[j].InvestigationTaskStatusID), Convert.ToInt32(AssignDetail[j].InvestigationTaskAssignmentID), Convert.ToInt32(Session["UserId"]), "UPDATENOTCOMPLETED");
                                                                        }

                                                                    }
                                                                }

                                                            }
                                                            else
                                                            {
                                                                var Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(scope.InvestigationId,

                                                                                                                                    Convert.ToInt32(scope.invScopeId[i]),

                                                                                                                                    Convert.ToInt32(CheckTaskDetail.InvestigationTaskGroupID),

                                                                                                                                    (TaskGroupEventId),

                                                                                                                                    Convert.ToInt32(TaskDetail[j].InvestigationTaskStatusID),

                                                                                                                                    Convert.ToInt32(AssignDetail[j].InvestigationTaskAssignmentID),

                                                                                                                                    Convert.ToInt32(Session["UserId"]),

                                                                                                                                    "UPDATE");
                                                            }
                                                        }
                                                        else  //delete
                                                        {
                                                            //var Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(scope.InvestigationId,

                                                            //                                                                  Convert.ToInt32(scope.invScopeId[i]),

                                                            //                                                                  Convert.ToInt32(CheckTaskDetail.InvestigationTaskGroupID),

                                                            //                                                                  TaskGroupEventId,

                                                            //                                                                  Convert.ToInt32(TaskDetail[j].InvestigationTaskStatusID),

                                                            //                                                                  Convert.ToInt32(AssignDetail[j].InvestigationTaskAssignmentID),

                                                            //                                                                  Convert.ToInt32(Session["UserId"]),

                                                            //                                                                  "DELETE");

                                                            var CheckIfStaticEvent = dc.InvestigationTaskDetails.AsEnumerable().Where(a => a.InvestigationFileID == scope.InvestigationId

                                                                                 && a.InvestigationTaskGroupEventUniqueNumber == UniqueNo

                                                                                 ).FirstOrDefault();
                                                            if (CheckIfStaticEvent != null)
                                                            {
                                                                if (CheckIfStaticEvent.InvestigationTaskStatus == 1)
                                                                {
                                                                    if (StaticEvents.Contains(UniqueNo))
                                                                    {
                                                                        if (UniqueNo == "E2")
                                                                        {
                                                                            int[] E2TaskGroupId = { 7, 9, 10, 11, 12, 13, 14 };

                                                                            var Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(scope.InvestigationId, 0, 0, 0, Convert.ToInt32(TaskDetail[j].InvestigationTaskStatusID), Convert.ToInt32(AssignDetail[j].InvestigationTaskAssignmentID), Convert.ToInt32(Session["UserId"]), "DELETECOMPLETED");

                                                                            foreach (var TaskGrpId in E2TaskGroupId)
                                                                            {
                                                                                Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(scope.InvestigationId, 0, TaskGrpId, 0, Convert.ToInt32(TaskDetail[j].InvestigationTaskStatusID), Convert.ToInt32(AssignDetail[j].InvestigationTaskAssignmentID), Convert.ToInt32(Session["UserId"]), "UPDATECOMPLETED");
                                                                            }
                                                                        }
                                                                        else if (UniqueNo == "E1" || UniqueNo == "E64" || UniqueNo == "E65" || UniqueNo == "E66")
                                                                        {
                                                                            var Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(scope.InvestigationId, 0, Convert.ToInt32(CheckTaskDetail.InvestigationTaskGroupID), 0, Convert.ToInt32(TaskDetail[j].InvestigationTaskStatusID), Convert.ToInt32(AssignDetail[j].InvestigationTaskAssignmentID), Convert.ToInt32(Session["UserId"]), "DELETECOMPLETED");

                                                                            for (int k = 1; k <= 14; k++)
                                                                            {
                                                                                Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(scope.InvestigationId, 0, k, 0, Convert.ToInt32(TaskDetail[j].InvestigationTaskStatusID), Convert.ToInt32(AssignDetail[j].InvestigationTaskAssignmentID), Convert.ToInt32(Session["UserId"]), "UPDATECOMPLETED");
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        var Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(scope.InvestigationId, 0, Convert.ToInt32(CheckTaskDetail.InvestigationTaskGroupID), 0, Convert.ToInt32(TaskDetail[j].InvestigationTaskStatusID), Convert.ToInt32(AssignDetail[j].InvestigationTaskAssignmentID), Convert.ToInt32(Session["UserId"]), "DELETECOMPLETED");

                                                                        Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(scope.InvestigationId, 0, Convert.ToInt32(scope.invTaskGroupId[i]), 0, Convert.ToInt32(TaskDetail[j].InvestigationTaskStatusID), Convert.ToInt32(AssignDetail[j].InvestigationTaskAssignmentID), Convert.ToInt32(Session["UserId"]), "UPDATECOMPLETED");
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    if (StaticEvents.Contains(UniqueNo))
                                                                    {
                                                                        if (UniqueNo == "E2")
                                                                        {
                                                                            int[] E2TaskGroupId = { 7, 9, 10, 11, 12, 13, 14 };

                                                                            var Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(scope.InvestigationId, 0, 0, 0, Convert.ToInt32(TaskDetail[j].InvestigationTaskStatusID), Convert.ToInt32(AssignDetail[j].InvestigationTaskAssignmentID), Convert.ToInt32(Session["UserId"]), "DELETENOTCOMPLETED");

                                                                            foreach (var TaskGrpId in E2TaskGroupId)
                                                                            {
                                                                                Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(scope.InvestigationId, 0, TaskGrpId, 0, Convert.ToInt32(TaskDetail[j].InvestigationTaskStatusID), Convert.ToInt32(AssignDetail[j].InvestigationTaskAssignmentID), Convert.ToInt32(Session["UserId"]), "UPDATENOTCOMPLETED");
                                                                            }
                                                                        }
                                                                        else if (UniqueNo == "E1" || UniqueNo == "E64" || UniqueNo == "E65" || UniqueNo == "E66")
                                                                        {
                                                                            var Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(scope.InvestigationId, 0, 0, 0, Convert.ToInt32(TaskDetail[j].InvestigationTaskStatusID), Convert.ToInt32(AssignDetail[j].InvestigationTaskAssignmentID), Convert.ToInt32(Session["UserId"]), "DELETENOTCOMPLETED");

                                                                            for (int k = 1; k <= 14; k++)
                                                                            {
                                                                                Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(scope.InvestigationId, 0, k, 0, Convert.ToInt32(TaskDetail[j].InvestigationTaskStatusID), Convert.ToInt32(AssignDetail[j].InvestigationTaskAssignmentID), Convert.ToInt32(Session["UserId"]), "UPDATENOTCOMPLETED");
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        var Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(scope.InvestigationId, 0, 0, 0, Convert.ToInt32(TaskDetail[j].InvestigationTaskStatusID), Convert.ToInt32(AssignDetail[j].InvestigationTaskAssignmentID), Convert.ToInt32(Session["UserId"]), "DELETENOTCOMPLETED");

                                                                        Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(scope.InvestigationId, 0, Convert.ToInt32(scope.invTaskGroupId[i]), 0, Convert.ToInt32(TaskDetail[j].InvestigationTaskStatusID), Convert.ToInt32(AssignDetail[j].InvestigationTaskAssignmentID), Convert.ToInt32(Session["UserId"]), "UPDATENOTCOMPLETED");
                                                                    }

                                                                }
                                                            }

                                                        }
                                                    }
                                                }
                                            }
                                            else if (CheckTaskDetail == null) // Update Scope Definition columns value set as Zero  & delete row in Table TaskDetail , Task Assignment
                                            {
                                                var Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(scope.InvestigationId,

                                                                                                                              Convert.ToInt32(scope.invScopeId[i]),

                                                                                                                              0, 0, 0, 0,

                                                                                                                              Convert.ToInt32(Session["UserId"]),

                                                                                                                             "DELETEALL");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            TaskGroupInScope = 1;
                        }


                        if (Convert.ToInt32(Session["UserId"]) > 0)
                        {
                            if (scope.invScopeId[i].ToString() == "0")
                            {
                                result = dc.USP_INST_INVESTIGATION_SCOPE_DEFINITION(scope.InvestigationId, Convert.ToInt32(scope.invTaskGroupId[i]), Convert.ToInt16(TaskGroupInScope),

                                    Convert.ToInt16(TaskGroupInScope), 0, 0, 0, 0, 0, Convert.ToInt32(Session["UserId"]));
                            }
                            else
                            {
                                result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION(Convert.ToInt32(scope.invScopeId[i]), TaskGroupInScope, Convert.ToInt32(Session["UserId"]));
                            }
                        }
                    }

                    var CheckScopeStatus = dc.USP_VIEW_INVESTIGATION_SCOPE_DEFINITION_TASKGROUPSTATUS(scope.InvestigationId).FirstOrDefault();

                    if (CheckScopeStatus == null)
                    {
                        var Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(scope.InvestigationId, 0, 0, 0, 0, 0,

                                                                                                                          Convert.ToInt32(Session["UserId"]),

                                                                                                                         "DELETEALL");
                    }
                    else
                    {
                        var CheckUnAssignEvent = dc.USP_VIEW_INVESTIGATION_UNASSIGN_TASKDETAILS(scope.InvestigationId).ToList();

                        foreach (var taskdet in CheckUnAssignEvent)
                        {
                            if (taskdet.InvestigationTaskStatus == 1)
                            {
                                if (StaticEvents.Contains(taskdet.InvestigationTaskGroupEventUniqueNumber))
                                {
                                    if (taskdet.InvestigationTaskGroupEventUniqueNumber == "E2")
                                    {
                                        int[] E2TaskGroupId = { 7, 9, 10, 11, 12, 13, 14 };

                                        var Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(scope.InvestigationId, 0, 0, 0, Convert.ToInt32(taskdet.InvestigationTaskStatusID), Convert.ToInt32(taskdet.InvestigationTaskAssignmentID), Convert.ToInt32(Session["UserId"]), "DELETECOMPLETED");

                                        foreach (var TaskGrpId in E2TaskGroupId)
                                        {
                                            Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(scope.InvestigationId, 0, TaskGrpId, 0, Convert.ToInt32(taskdet.InvestigationTaskStatusID), Convert.ToInt32(taskdet.InvestigationTaskAssignmentID), Convert.ToInt32(Session["UserId"]), "UPDATECOMPLETED");
                                        }
                                    }
                                    else if (taskdet.InvestigationTaskGroupEventUniqueNumber == "E1" || taskdet.InvestigationTaskGroupEventUniqueNumber == "E64" ||

                                        taskdet.InvestigationTaskGroupEventUniqueNumber == "E65" || taskdet.InvestigationTaskGroupEventUniqueNumber == "E66")
                                    {
                                        var Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(scope.InvestigationId, 0, Convert.ToInt32(taskdet.InvestigationTaskGroupID), 0, Convert.ToInt32(taskdet.InvestigationTaskStatusID), Convert.ToInt32(taskdet.InvestigationTaskAssignmentID), Convert.ToInt32(Session["UserId"]), "DELETECOMPLETED");

                                        for (int k = 1; k <= 14; k++)
                                        {
                                            Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(scope.InvestigationId, 0, k, 0, Convert.ToInt32(taskdet.InvestigationTaskStatusID), Convert.ToInt32(taskdet.InvestigationTaskAssignmentID), Convert.ToInt32(Session["UserId"]), "UPDATECOMPLETED");
                                        }
                                    }
                                }
                                else
                                {
                                    var Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(scope.InvestigationId, 0, Convert.ToInt32(taskdet.InvestigationTaskGroupID), 0, Convert.ToInt32(taskdet.InvestigationTaskStatusID), Convert.ToInt32(taskdet.InvestigationTaskAssignmentID), Convert.ToInt32(Session["UserId"]), "DELETECOMPLETED");

                                    Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(scope.InvestigationId, 0, Convert.ToInt32(taskdet.InvestigationTaskGroupID), 0, Convert.ToInt32(taskdet.InvestigationTaskStatusID), Convert.ToInt32(taskdet.InvestigationTaskAssignmentID), Convert.ToInt32(Session["UserId"]), "UPDATECOMPLETED");
                                }
                            }
                            else
                            {
                                if (StaticEvents.Contains(taskdet.InvestigationTaskGroupEventUniqueNumber))
                                {
                                    if (taskdet.InvestigationTaskGroupEventUniqueNumber == "E2")
                                    {
                                        int[] E2TaskGroupId = { 7, 9, 10, 11, 12, 13, 14 };

                                        var Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(scope.InvestigationId, 0, 0, 0, Convert.ToInt32(taskdet.InvestigationTaskStatusID), Convert.ToInt32(taskdet.InvestigationTaskAssignmentID), Convert.ToInt32(Session["UserId"]), "DELETENOTCOMPLETED");

                                        foreach (var TaskGrpId in E2TaskGroupId)
                                        {
                                            Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(scope.InvestigationId, 0, TaskGrpId, 0, Convert.ToInt32(taskdet.InvestigationTaskStatusID), Convert.ToInt32(taskdet.InvestigationTaskAssignmentID), Convert.ToInt32(Session["UserId"]), "UPDATENOTCOMPLETED");
                                        }
                                    }
                                    else if (taskdet.InvestigationTaskGroupEventUniqueNumber == "E1" || taskdet.InvestigationTaskGroupEventUniqueNumber == "E64" ||

                                        taskdet.InvestigationTaskGroupEventUniqueNumber == "E65" || taskdet.InvestigationTaskGroupEventUniqueNumber == "E66")
                                    {
                                        var Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(scope.InvestigationId, 0, 0, 0, Convert.ToInt32(taskdet.InvestigationTaskStatusID), Convert.ToInt32(taskdet.InvestigationTaskAssignmentID), Convert.ToInt32(Session["UserId"]), "DELETENOTCOMPLETED");

                                        for (int k = 1; k <= 14; k++)
                                        {
                                            Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(scope.InvestigationId, 0, k, 0, Convert.ToInt32(taskdet.InvestigationTaskStatusID), Convert.ToInt32(taskdet.InvestigationTaskAssignmentID), Convert.ToInt32(Session["UserId"]), "UPDATENOTCOMPLETED");
                                        }
                                    }
                                }
                                else
                                {
                                    var Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(scope.InvestigationId, 0, 0, 0, Convert.ToInt32(taskdet.InvestigationTaskStatusID), Convert.ToInt32(taskdet.InvestigationTaskAssignmentID), Convert.ToInt32(Session["UserId"]), "DELETENOTCOMPLETED");

                                    Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(scope.InvestigationId, 0, Convert.ToInt32(taskdet.InvestigationTaskGroupID), 0, Convert.ToInt32(taskdet.InvestigationTaskStatusID), Convert.ToInt32(taskdet.InvestigationTaskAssignmentID), Convert.ToInt32(Session["UserId"]), "UPDATENOTCOMPLETED");
                                }

                            }
                        }
                    }

                    var InvInfo = dc.USP_VIEW_INVESTIGATION_INVESTIGATIONID(scope.InvestigationId).FirstOrDefault();

                    scope.InvestigationId = InvInfo.InvestigationFileID;

                    TempData["PageHead"] = InvInfo.InvestigationFileNumber;

                    TempData["BackPage"] = "yes";

                    Session["InvFileID"] = InvInfo.InvestigationFileID;

                    TempData["InvId"] = InvInfo.InvestigationFileID;
                    TempData["active_url_manuals_iic"] = "not-iic";



                    scope.LocalDateAndTimeOfOccurrence = InvInfo.LocalDateAndTimeOfOccurrence.ToString();

                    scope.UTCDateAndTimeOfOccurrence = InvInfo.UTCDateAndTimeOfOccurrence.ToString();

                    scope.NotifierName = InvInfo.NotifierName;

                    scope.NotifierOrganization = InvInfo.NotifierOrganization;

                    var Investigation_Scope = dc.USP_VIEW_INVESTIGATION_SCOPE_DEFINITION_INVESTIGATIONID(scope.InvestigationId).ToList();
                    ViewBag.InvestigationScope = Investigation_Scope;
                    return RedirectToAction("Defn_Inv_Scope", "task", new { id = InvInfo.InvestigationFileID });

                    //  DbTransaction.Commit();
                }
                else
                {
                    //DbTransaction.Rollback();

                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                //DbTransaction.Rollback();

                comm.Exception_Log("TASKController", "Defn_Inv_Scope(InvScope scope)", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }
            //return View(scope);
        }
        /// <summary>
        /// open the investigation file details from Form 35 by investigation file id
        /// </summary>  
        /// <param name="InvId">Receive investigation file id</param>
        /// 
        public ActionResult Gen_Forms(int InvId = 0)
        {
            InvestigationModel invModel = new InvestigationModel();

            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 3 || Convert.ToInt32(Session["RoleId"]) == 6 || Convert.ToInt32(Session["RoleId"]) == 7)
                {
                    var CheckInvPermission = dc.InvestigationIICAssignments.AsEnumerable().Where(a => a.InvestigationFileID == InvId).FirstOrDefault();

                    if ((CheckInvPermission.IICAssigned == Convert.ToInt32(Session["UserId"]) &&

                                           CheckInvPermission.InvestigationFileID == InvId) || Convert.ToInt32(Session["RoleId"]) == 3 || Convert.ToInt32(Session["RoleId"]) == 6 || Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 7)
                    {
                        if (InvId > 0)
                        {
                            var Inv = dc.USP_VIEW_INVESTIGATION_FORM35(InvId).FirstOrDefault();

                            if (Inv != null)
                            {
                                TempData["InvId"] = InvId;
                                TempData["InvFileID"] = InvId;

                                TempData["PageHead"] = Inv.InvestigationNumber;
                                TempData["BackPage"] = "yes";
                                TempData["active_url_Gen_Forms"] = "tm";
                                TempData["active_url_Gen_Forms_iic"] = "iic";
                                TempData["lf_menu"] = "1";

                                invModel.InvestigationFileID = Inv.InvestigationFileID;

                                invModel.LocalDateAndTimeOfOccurrence = Inv.LocalDateAndTimeOfOccurrence;

                                invModel.UTCDateAndTimeOfOccurrence = Inv.UTCDateAndTimeOfOccurrence;

                                invModel.NotifierName = Inv.NotifierName;

                                invModel.NotifierOrganization = Inv.NotifierOrganization;
                                var interim_det = dc.USP_VIEW_INTERIMREPORT_INVFILEDETAILS(InvId).ToList();
                                invModel.Interim_statement = interim_det[0].InvestigationProcess;
                                invModel.Investigation_Process = interim_det[0].InterimStatement;
                                var Prelim_det = dc.USP_VIEW_INVFILEDETAILS_REPORT_PRELIM(InvId).FirstOrDefault();
                                invModel.Ongoing_Investigation_Activitiea = Prelim_det.OngoingInvestigationActivities;
                            }
                            else
                            {
                                return RedirectToAction("PageNotFound", "Error");
                            }
                        }
                    }
                    else
                    {
                        return RedirectToAction("Error");
                    }
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("TASKController", "Gen_Forms(int InvId = 0)", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }
            return View(invModel);
        }
        /// <summary>
        /// download the Interim report as word dcument for particular investigation
        /// </summary> 
        /// <param name="Intreport">Receive investigation form generation details</param>
        [HttpPost]
        public ActionResult Gen_Forms(InvestigationModel Intreport)
        {
            string invprocess = Intreport.Investigation_Process;
            string interim_stmt = Intreport.Interim_statement;
            int? invfile_id = Intreport.InvestigationFileID;
            dc.USP_UPDT_INVDETAILS_INTERIM(invprocess, interim_stmt, invfile_id);

            try
            {
                string status = string.Empty;
                string sourceFile = Server.MapPath(System.IO.Path.Combine("~/App_data", "InterimAirAccidentInvestigationreport_1.dotx"));
                string country = string.Empty;
                Dictionary<string, string> keyValues = new Dictionary<string, string>();
                Dictionary<string, List<string>> keyJSONValues = new Dictionary<string, List<string>>();
                var PrelimReportvalues = dc.USP_VIEW_INVESTIGATION_REPORT_INTERIM(invfile_id).ToList();
                var PrelimReportAircraftvalues = dc.USP_VIEW_INVESTIGATION_AIRCRAFT_DETAIL_REPORT_PRELIM(invfile_id).ToList();
                DateTime start = DateTime.Parse(PrelimReportvalues[0].UTCDATEOCC);
                DateTime End = DateTime.Now;
                String Count = string.Empty;
                TimeSpan diff = End - start;


                if (Convert.ToInt32(diff.Days) <= 365)
                {
                    Count = "First";
                }
                else if (Convert.ToInt32(diff.Days) > 365 && Convert.ToInt32(diff.Days) <= 730)
                {
                    Count = "Second";
                }
                else if (Convert.ToInt32(diff.Days) > 730 && Convert.ToInt32(diff.Days) <= 1095)
                {
                    Count = "Third";
                }

                else if (Convert.ToInt32(diff.Days) > 1095 && Convert.ToInt32(diff.Days) <= 1460)
                {
                    Count = "Fourth";
                }
                else if (Convert.ToInt32(diff.Days) > 1460 && Convert.ToInt32(diff.Days) <= 1825)
                {
                    Count = "Fifth";
                }
                else if (Convert.ToInt32(diff.Days) > 1825 && Convert.ToInt32(diff.Days) <= 2190)
                {
                    Count = "Sixth";
                }
                else if (Convert.ToInt32(diff.Days) > 2190 && Convert.ToInt32(diff.Days) <= 2555)
                {
                    Count = "Seventh";
                }
                else if (Convert.ToInt32(diff.Days) > 2555 && Convert.ToInt32(diff.Days) <= 2920)
                {
                    Count = "Eight";
                }
                else if (Convert.ToInt32(diff.Days) > 2920 && Convert.ToInt32(diff.Days) <= 3285)
                {
                    Count = "Nineth";
                }
                else if (Convert.ToInt32(diff.Days) > 3285 && Convert.ToInt32(diff.Days) <= 3650)
                {
                    Count = "Tenth";
                }


                var PrelimEventsJSON = dc.USP_VIEW_EVENTSJSON_REPORTS(invfile_id, "e1,e3,e6,e7,e8,e11,e14,e15,e18,e21,e22,e23,e24,e27,e29,e42,e45,e46").ToList();
                foreach (USP_VIEW_INVESTIGATION_REPORT_INTERIM_Result PDFResult in PrelimReportvalues)
                {
                    Type type = PDFResult.GetType();
                    PropertyInfo[] props = type.GetProperties();
                    string[] str = new string[50];
                    //int i = 0;
                    foreach (var prop in props)
                    {
                        if (prop.PropertyType.FullName.Contains("DateTime"))
                        {
                            string convDateTime = "N/A";
                            if (prop.GetValue(PDFResult) != null)
                                convDateTime = comm.convertDateTime(Convert.ToDateTime(prop.GetValue(PDFResult)));
                            keyValues.Add(prop.Name, (!string.IsNullOrEmpty(Convert.ToString(convDateTime)) ? (Convert.ToString(convDateTime)) : " "));
                        }
                        else
                        {
                            keyValues.Add(prop.Name, (!string.IsNullOrEmpty(Convert.ToString(prop.GetValue(PDFResult))) ? (Convert.ToString(prop.GetValue(PDFResult))) : " "));
                        }
                    }
                    keyValues.Add("CURDATE", (DateTime.Today).ToString("dd-MMMM-yyyy"));
                    keyValues.Add("TIMES", Count);
                }
                string[] keys = new string[100];

                foreach (USP_VIEW_EVENTSJSON_REPORTS_Result PDFJSONResult in PrelimEventsJSON)
                {
                    Type type = PDFJSONResult.GetType();
                    PropertyInfo[] props = type.GetProperties();
                    //List<string> keys = new List<string>();
                    //int i = 0;
                    foreach (var prop in props)
                    {
                        if (prop.Name == "InvestigationTaskGroupEventUniqueNumber")
                        {
                            if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e1")
                            {
                                keys = new string[] { "reportflighthistory.text", "reportcrewmembers.text", "reportPassengers.text", "reportothers.text", "reportInvestigationProcess.text", "reportNameofowner.text", "reportOrganizationalandManagement.text", "reportAdditionalInformation.text", "reportreportpublishedlinktxtbox.text" };
                            }
                            else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e3")
                            {
                                keys = new string[] { "reportflightpath.text", "reportdateofmanufacturetxtbox.text", "reportManufacturertxtbox.text", "reportflighthourscyclestxtbox.text", "reportPilotinCommand.text", "reportcopilot.text", "reportcabincrew.text" };
                            }
                            else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e6")
                            {
                                keys = new string[] { "reportFlightRecordersdetails.text" };
                            }
                            else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e7")
                            {
                                keys = new string[] { "reportConclusion7.text" };
                            }
                            else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e8")
                            {
                                keys = new string[] { "reportairportpersonnel.text","reportAirtrafficcontrollers.text","reportAerodromeInformation.text","reportAerodromecode(ICAO/IATA)txtbox.text","reportAirportnametxtbox.text","reportAirportaddresstxtbox.text",
                                                      "reportAirportclasstxtbox.text","reportAirportauthoritytxtbox.text","reportAirportservicetxtbox.text","reportTypeoftrafficpermittedtxtbox.text","reportCoordinatestxtbox.text","reportElevationreferencetemperaturetxtbox.text",
                                                      "reportRunwaylengthtxtbox.text","reportRunwaywidthtxtbox.text","reportStopwaytxtbox.text","report Runwayendsafetyareatxtbox.text","reportAzimuthtxtbox.text","reportCategoryFirefightingtxtbox.text"};
                            }
                            else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e11")
                            {
                                keys = new string[] {"reportCertificateofAirworthinesstxtbox.text","reportValid toCertificateofAirworthinesstxtbox.text","reportCertificateofRegistrationtxtbox.text","reportValidtoCertificateofRegistrationtxtbox.text","reportTotalHoursSinceNewtxtbox.text",
                                                     "reportTotalCyclesSinceNewtxtbox.text","reportTotalHoursSinceOverhaultxtbox.text","reportTotalCyclesSinceOverhaultxtbox.text","reportTotalHoursSinceLastInspectiontxtbox.text","reportTotalCyclesSinceLastInspectiontxtbox.text"};
                            }
                            else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e14")
                            {
                                keys = new string[] {"reportPropellerTypetxtbox.text","reportPropellerManufacturertxtbox.text","reportManufacturertypetxtbox.text",
                                                     "reportPropellernumberone(Left)txtbox.text","reportleftSerialNumbertxtbox.text","reportleftTotalTimeSinceNewtxtbox.text","reportleftTotalTimeSinceOverhaultxtbox.text",
                                                     "reportPropellernumbertwo(Right)txtbox.text","reportRightSerialNumbertxtbox.text","reportrightTotalTimeSinceNewtxtbox.text","reportrightTotalTimeSinceOverhaultxtbox.text",
                                                     "reportEngineTypetxtbox.text","reportEngineManufacturertxtbox.text","reportEngineManufacturerTypetxtbox.text",
                                                     "reportEnginenumberone(Left)txtbox.text","reportEngineleftSerialNumbertxtbox.text","reportEngineleftTotalTimeSinceNewtxtbox.text","reportEngineleftTotalTimeSinceOverhaultxtbox.text",
                                                     "reportEnginenumberone(Right)txtbox.text","reportEngineRightSerialNumbertxtbox.text","reportEnginerightTotalTimeSinceNewtxtbox.text","reportEnginerightTotalTimeSinceOverhaultxtbox.text","reportConclusion14.text"};
                            }
                            else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e15")
                            {
                                keys = new string[] { "reportWreckageDistribution.text" };
                            }
                            else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e18")
                            {
                                keys = new string[] { "reportCrewmemberMedical.text" };
                            }
                            else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e21")
                            {
                                keys = new string[] { "reportConclusion21.text", "reportReviewWeatherDoc.text" };
                            }
                            else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e22")
                            {
                                keys = new string[] { "reportATCcommunication.text" };
                            }
                            else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e23")
                            {
                                keys = new string[] { "EvacuationProcess.text", "Passengerbehavior.text" };
                            }
                            else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e24")
                            {
                                keys = new string[] { "reportAircraftCabinData.text", "prelimCabinLayoutDiagram" };
                            }
                            else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e27")
                            {
                                keys = new string[] { "reportInflightFire.text", "reportGroundFire.text" };
                            }
                            else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e29")
                            {
                                keys = new string[] { "reportDamagetoAerodrome.text", "reportRunwaydamage.text", "reportDamagetonavigation.text", "reportDamagetoenvironment.text", "reportAircraftInitialimpact.text", "reportAircraftInitialimpactdiagram" };
                            }
                            else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e42")
                            {
                                keys = new string[] { "reportMaximumtakeoffweight.text", "prelimMaximumLandingweight.text", "reportMaximumzerofuelweight.text", "reportTakeoffweight.text", "reportLandingweight.text", "reportZeroweight.text" };
                            }
                            else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e45")
                            {
                                keys = new string[] { "reportConclusion45.text" };
                            }
                            else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e46")
                            {
                                keys = new string[] { "reportFireCrew.text", "reportARFFSresponse.text" };
                            }
                            else
                            {
                                keys = null;
                            }
                        }
                        if (prop.Name == "InvestigationTaskDataInJSON")
                        {
                            if (!string.IsNullOrEmpty(Convert.ToString(prop.GetValue(PDFJSONResult))))
                            {
                                keyValues = getOtherValuesfromEventJSON(keys, Convert.ToString(prop.GetValue(PDFJSONResult)), keyValues);
                            }
                            else
                            {
                                if (keys != null)
                                {
                                    if (keys.Length > 0)
                                    {
                                        for (int ii = 0; ii < keys.Length; ii++)
                                        {
                                            if (keys[ii].Contains("txtbox"))
                                                keyValues.Add(keys[ii], " ");
                                            if (!keys[ii].Contains("txtbox"))
                                                keyValues.Add(keys[ii], "This information will be added in future or in Final Report");
                                        }
                                    }
                                }


                            }
                        }

                    }
                }
                string fileName = "AAI Interim_" + Convert.ToString(DateTime.Now.ToFileTime()) + ".docx";
                string destinationFile = Server.MapPath(Path.Combine("~/App_data", fileName));
                Dictionary<string, List<string>> keyValuesAirDet = new Dictionary<string, List<string>>();
                if (PrelimReportAircraftvalues != null)
                {
                    foreach (USP_VIEW_INVESTIGATION_AIRCRAFT_DETAIL_REPORT_PRELIM_Result PDFAirResult in PrelimReportAircraftvalues)
                    {
                        Type type = PDFAirResult.GetType();
                        PropertyInfo[] props = type.GetProperties();
                        //string str = "{";
                        string[] str = new string[50];
                        //    int i = 0;
                        foreach (var prop in props)
                        {

                            List<string> list = new List<string>();
                            if (keyValuesAirDet.ContainsKey(prop.Name))
                                keyValuesAirDet[prop.Name].Add(!string.IsNullOrEmpty(Convert.ToString(prop.GetValue(PDFAirResult))) ? (Convert.ToString(prop.GetValue(PDFAirResult))) : "N/A");
                            else
                            {
                                keyValuesAirDet.Add(prop.Name, list);
                                list.Add(!string.IsNullOrEmpty(Convert.ToString(prop.GetValue(PDFAirResult))) ? (Convert.ToString(prop.GetValue(PDFAirResult))) : "N/A");
                            }
                        }
                    }
                }
                MemoryStream fileMem = getFile(keyValues, keyValuesAirDet, null, sourceFile, destinationFile, "Rep-PRE");

                fileMem.Seek(0, SeekOrigin.Begin);
                System.IO.File.Delete(destinationFile);
                return File(fileMem, "application/octet-stream", fileName);

            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("TASK", "gen_forms", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("NotFound", "Error");
            }
            //return View();
        }
        /// <summary>
        /// show invesigation task event based on task group id 
        /// </summary> 
        /// <param name="id">Receive investigation task group id </param>
        /// <param name="invid">Receive investigation file id</param>
        public ActionResult TaskGroup(int id = 0, int invid = 0)
        {
            InvScope scope = new InvScope();

            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 3 || Convert.ToInt32(Session["RoleId"]) == 6 || Convert.ToInt32(Session["RoleId"]) == 7)
                {
                    TempData["BackPage"] = "inv_scope";
                    TempData["active_url_Defn_Inv_Scope"] = "tm";
                    TempData["active_url_Defn_Inv_Scope_iic"] = "iic";
                    TempData["lf_menu"] = "1";

                    var InvInfo = dc.USP_VIEW_INVESTIGATION_INVESTIGATIONID(invid).FirstOrDefault();

                    TempData["PageHead"] = InvInfo.InvestigationFileNumber;

                    TempData["InvId"] = InvInfo.InvestigationFileID;
                    TempData["InvFileID"] = InvInfo.InvestigationFileID;

                    scope.InvestigationId = InvInfo.InvestigationFileID;

                    scope.LocalDateAndTimeOfOccurrence = InvInfo.LocalDateAndTimeOfOccurrence.ToString();

                    scope.UTCDateAndTimeOfOccurrence = InvInfo.UTCDateAndTimeOfOccurrence.ToString();

                    scope.NotifierName = InvInfo.NotifierName;

                    scope.NotifierOrganization = InvInfo.NotifierOrganization;

                    var InvTaskGroup = dc.InvestigationTaskGroups.AsQueryable().Where(a => a.InvestigationTaskGroupID == id).FirstOrDefault(); // Get Investigation task group Name

                    scope.InvestigationTaskGroupName = InvTaskGroup.InvestigationTaskGroupName;

                    //var result = dc.USP_VIEW_TASKGROUPEVENTS(invid, id).ToList();  // Add InvId on 03 Apr 2018
                    var result = dc.USP_VIEW_MULTIPLE_TASKGROUPEVENTS(invid, id).ToList();
                    if (result.Count() == 0)
                    {
                        var NewGroupEvnts = dc.USP_VIEW_TASKGROUPEVENTS_FOR_NEW_INVESTIGATIONGROUPEVENTS(id).ToList();

                        ViewBag.NewgroupEvents = NewGroupEvnts;
                    }
                    else
                    {
                        result.ForEach(obj => {
                            obj.InvestigationTaskGroupEventName = "Sub-Task "+ obj.InvestigationTaskGroupEventName.Remove(0,5);
                        });
                        ViewBag.NewgroupEvents = null;

                        ViewBag.groupEvents = result;
                    }

                    TempData["id"] = invid;

                    var invInfo = dc.USP_VIEW_INVESTIGATION_FORM35(invid).FirstOrDefault();

                    TempData["INV_STATUS"] = invInfo.LKInvestigationStatusTypeID;
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("TASKController", "TaskGroup(int id = 0, int invid = 0)", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }

            return View(scope);
        }
        /// <summary>
        /// Validate task group Event based on investigation
        /// </summary> 
        /// <param name="InvId">Receive investigation file id </param>
        /// <param name="TgEvtId">Receive investigation task group event id </param>
        public int ValidateTaskGroupEvent(int InvId, int TgEvtId)
        {
            int status = 0;

            try
            {
                // #region Check PreTaskEvent Status

                // var TaskGroupID = dc.USP_GET_INVESTIGATION_TaskGroupID_By_InvestigationTaskGroupEventID(TgEvtId).FirstOrDefault();

                // var uniqueNo = dc.InvestigationTaskGroupEvents.Where(a => a.InvestigationTaskGroupEventID == TgEvtId).FirstOrDefault();

                // //code comeented for jira ticket multiple team members 

                // //var ValidateTaskGroup = dc.USP_VIEW_VALIDATE_TASKGROUPEVENTS(InvId,

                // //    TaskGroupID.InvestigationTaskGroupID, Convert.ToInt32(uniqueNo.InvestigationTaskGroupEventUniqueNumber.Remove(0, 1))).FirstOrDefault();

                // var ValidateTaskGroup = dc.USP_VIEW_MULTIPLE_VALIDATE_TASKGROUPEVENTS(InvId,

                //TaskGroupID.InvestigationTaskGroupID, Convert.ToInt32(uniqueNo.InvestigationTaskGroupEventUniqueNumber.Remove(0, 1))).FirstOrDefault();

                // if (ValidateTaskGroup != null)
                // {
                //     if (ValidateTaskGroup.FirstName == null)
                //     {
                //         status = 0;
                //     }
                //     else
                //     {
                //         status = 1;
                //     }
                // }
                // else
                // {
                //     status = 1;
                // }
                // #endregion

                status = 1; // On 02-07-2018 - remove previous event assignment validation
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("TASKController", "TaskGroup(int id = 0, int invid = 0)", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }
            return status;
        }
        /// <summary>
        /// open assigning team members page for both investigation scope details and investigation progress page
        /// </summary> 
        /// <param name="id">Receive investigation task group id </param>
        /// <param name="CtrlFrom">Receive investigation task group event flow </param>
        /// <param name="invid">Receive investigation id </param>
        public ActionResult Assigning_team_members(int id = 0, int invid = 0, string CtrlFrom = "")
        {
            InvScope scope = new InvScope();

            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 4||Convert.ToInt32(Session["RoleId"]) == 4)
                {
                    TempData["BackPage"] = "task";
                    TempData["lf_menu"] = "1";
                    TempData["CtrlFrom"] = CtrlFrom;
                    //  TempData["active_url_Defn_Inv_Scope_iic"] = "iic";


                    if (CtrlFrom == "TG")
                    {
                        TempData["active_url_Defn_Inv_Scope_iic"] = "iic";
                    }
                    else if (CtrlFrom == "IF")
                    {
                        TempData["active_url_View_Inv_Prg_iic"] = "iic";
                    }

                    scope.ControlFrom = CtrlFrom;

                    var InvInfo = dc.USP_VIEW_INVESTIGATION_INVESTIGATIONID(invid).FirstOrDefault();

                    TempData["PageHead"] = InvInfo.InvestigationFileNumber;

                    TempData["InvId"] = InvInfo.InvestigationFileID;

                    TempData["InvFileID"] = InvInfo.InvestigationFileID;
                    scope.InvestigationId = InvInfo.InvestigationFileID;



                    scope.LocalDateAndTimeOfOccurrence = InvInfo.LocalDateAndTimeOfOccurrence.ToString();

                    scope.UTCDateAndTimeOfOccurrence = InvInfo.UTCDateAndTimeOfOccurrence.ToString();

                    scope.NotifierName = InvInfo.NotifierName;

                    scope.NotifierOrganization = InvInfo.NotifierOrganization;

                    var TeamMembers = dc.USP_VIEW_TOTALNOOF_CASE(invid);

                    ViewBag.TeamMembers = TeamMembers;

                    var operational_TaskReview = dc.USP_VIEW_INVESTIGATION_OPERATIONALTASKREVIEW(id);

                    var UniqueNumber = operational_TaskReview.FirstOrDefault();


                    var TaskGroupID = dc.USP_GET_INVESTIGATION_TaskGroupID_By_InvestigationTaskGroupEventID(id).FirstOrDefault();

                    var InvTaskGroupEventName = dc.InvestigationTaskGroupEvents.AsQueryable().Where

                                    (a => a.InvestigationTaskGroupEventID == id).FirstOrDefault(); // Get Investigation task group Event Name

                    scope.InvestigationTaskGroupEventName = InvTaskGroupEventName.InvestigationTaskGroupEventName;

                    scope.UniqueNumber = InvTaskGroupEventName.InvestigationTaskGroupEventUniqueNumber;

                    scope.InvestigationTaskGroupName = InvTaskGroupEventName.InvestigationTaskGroupEventDescription;

                    scope.InvestigationTaskGroupEventID = id;

                    TempData["id"] = TaskGroupID.InvestigationTaskGroupID;

                    var task_details = dc.USP_VIEW_INVESTIGATION_TASKDETAILS(InvTaskGroupEventName.InvestigationTaskGroupEventUniqueNumber);

                    ViewBag.taskdetails = task_details;

                    ViewBag.DateTimeof_Completion = null;

                    scope.InvestigationGroup = TaskGroupID.LKInvestigationTeamName;

                    #region PreTask Complition Code

                    var PreTask = dc.USP_VIEW_INVESTIGATION_PRETASKREVIEW(id).FirstOrDefault();

                    if (PreTask != null)
                    {
                        scope.EventUniqueName = PreTask.InvestigationTaskGroupEventName;

                        scope.EventUniqueNumber = PreTask.InvestigationTaskGroupEventUniqueNumber;

                        scope.PreTaskEventStatus = "NOT FINISHED";

                        var PreTaskEventStatus = dc.USP_VIEW_INVESTIGATION_PRE_TASKGROUPEVENT_STATUS(InvInfo.InvestigationFileID, scope.EventUniqueNumber).FirstOrDefault();

                        if (PreTaskEventStatus != null)
                        {
                            scope.PreTaskEventStatus = "FINISHED";
                        }
                    }
                    else
                    {
                        scope.EventUniqueName = null;

                        scope.EventUniqueNumber = null;

                        scope.PreTaskEventStatus = null;
                    }

                    #endregion

                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("TASKController", "Assigning_team_members(int id = 0, int invid = 0)", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }

            return View(scope);
        }
        /// <summary>
        /// Save or update assigned team members
        /// </summary>     
        /// <param name="scope">Receive investigation scope details</param>
        [HttpPost]
        public ActionResult Assigning_team_members(InvScope scope)
        {
            var result = 0; int TaskGroupId = 0; bool IssuccessorComplete = false;

            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 7)
                {
                    if (Convert.ToInt32(Session["UserId"]) > 0)
                    {
                        var CheckInvTaskGroupExist = dc.InvestigationScopeDefinitions.AsQueryable().Where(a => a.InvestigationFileID == scope.InvestigationId).FirstOrDefault();

                        if (CheckInvTaskGroupExist == null)
                        {
                            for (int i = 1; i <= 14; i++)
                            {
                                result = dc.USP_INST_INVESTIGATION_SCOPE_DEFINITION(scope.InvestigationId, Convert.ToInt32(i), Convert.ToInt16(0),

                                   Convert.ToInt16(0), 0, 0, 0, 0, 0, Convert.ToInt32(Session["UserId"]));
                            }
                        }

                        var InvTaskGroupId = dc.InvestigationTaskGroupEvents.AsQueryable().Where(a => a.InvestigationTaskGroupEventID == scope.InvestigationTaskGroupEventID).FirstOrDefault();

                        var invScopeId = dc.InvestigationScopeDefinitions.AsQueryable().Where(a => a.InvestigationTaskGroupID == InvTaskGroupId.InvestigationTaskGroupID &&

                                                a.InvestigationFileID == scope.InvestigationId).FirstOrDefault();

                        #region Check PretaskEvent Status                

                        string group_event_UniqueNo = InvTaskGroupId.InvestigationTaskGroupEventUniqueNumber;

                        var predecessorid = (from prdid in dc.InvestigationTaskGroupEvents

                                             where prdid.InvestigationTaskGroupEventUniqueNumber == group_event_UniqueNo

                                             select new
                                             {
                                                 prdid.InvestigationTaskGroupPreEventUniqueNumber

                                             }).ToList();

                        if (predecessorid.Count > 0)
                        {
                            string UniqueNO = predecessorid[0].InvestigationTaskGroupPreEventUniqueNumber;

                            var TaskEventStatus = dc.InvestigationTaskDetails.Where(a => a.InvestigationTaskGroupEventUniqueNumber == UniqueNO &&

                                                     a.InvestigationFileID == scope.InvestigationId).FirstOrDefault();

                            if (TaskEventStatus != null)
                            {
                                IssuccessorComplete = Convert.ToBoolean(TaskEventStatus.InvestigationTaskStatus);
                            }
                        }

                        #endregion

                        int TaskStatus = 0;

                        #region On-03-07-2018

                        //if (IssuccessorComplete)
                        //{
                        //    TaskStatus = 0;
                        //}
                        //else if (predecessorid.Count() == 0)
                        //{
                        //    TaskStatus = 0;
                        //}
                        //else if (predecessorid.Count() > 0)
                        //{
                        //    if (predecessorid[0].InvestigationTaskGroupPreEventUniqueNumber == "")
                        //    {
                        //        TaskStatus = 0;
                        //    }
                        //    else
                        //    {
                        //        TaskStatus = 2;
                        //    }
                        //}
                        //else
                        //{
                        //    TaskStatus = 0;
                        //}

                        #endregion

                        //var CheckTeammemberInfo = dc.InvestigationTaskAssignments.AsEnumerable().Where(a => a.InvestigationFileID == scope.InvestigationId &&

                        //        a.InvestigationTaskGroupEventUniqueNumber == InvTaskGroupId.InvestigationTaskGroupEventUniqueNumber).FirstOrDefault();

                        //var CheckTaskDetailInfo = dc.InvestigationTaskDetails.AsEnumerable().Where(a => a.InvestigationFileID == scope.InvestigationId &&

                        //        a.InvestigationTaskGroupEventUniqueNumber == InvTaskGroupId.InvestigationTaskGroupEventUniqueNumber).FirstOrDefault();

                        //if (CheckTeammemberInfo == null)
                        //{
                        System.Data.Entity.Core.Objects.ObjectParameter inv_task_assignment_id = new System.Data.Entity.Core.Objects.ObjectParameter("inv_task_assignment_id", typeof(Int32));
                        result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION(invScopeId.InvestigationScopeDefinitionID, 1, Convert.ToInt32(Session["UserId"]));


                        result = dc.USP_INST_INVESTIGATION_TASK_ASSIGNMENT_WITH_MULTIPLETEAMMEMBER(scope.InvestigationId, scope.InvestigationTaskGroupEventID,


                                                    /*Convert.ToInt32(scope.TeamMemberId)*/null, scope.DateTimeofCompletion, Convert.ToInt32(Session["userid"]),

                                                    InvTaskGroupId.InvestigationTaskGroupEventUniqueNumber, InvTaskGroupId.InvestigationTaskGroupID, inv_task_assignment_id);
                        result = dc.USP_INST_INVESTIGATION_TASKDETAILS(scope.InvestigationId, scope.InvestigationTaskGroupEventID, 0, null, TaskStatus, Convert.ToInt32(Session["UserId"]), InvTaskGroupId.InvestigationTaskGroupID, InvTaskGroupId.InvestigationTaskGroupEventUniqueNumber, Convert.ToInt32(inv_task_assignment_id.Value));
                        for (int k = 0; k < scope.TeamMemberId.Count(); k++) // newly added
                        {

                            result = dc.USP_INST_MULTIPLE_TEAMMEMBER_EVENT(Convert.ToInt32(inv_task_assignment_id.Value), Convert.ToInt32(scope.TeamMemberId[k]), Convert.ToInt32(Session["UserId"]), Convert.ToInt32(Session["UserId"]));
                        } // newly added
                        var InvEventInfo = dc.InvestigationTaskGroupEvents.Where(a => a.InvestigationTaskGroupEventUniqueNumber == scope.UniqueNumber).ToList();

                        foreach (var TaskId in InvEventInfo)
                        {
                            result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_TASKGROUPIDandINVESTIGATIONID(TaskId.InvestigationTaskGroupID, scope.InvestigationId, Convert.ToInt32(Session["userid"]));
                        }
                        //}
                        //else
                        //{
                        //    result = dc.USP_UPDT_INVESTIGATION_TASK_ASSIGNMENT(CheckTeammemberInfo.InvestigationTaskAssignmentID, CheckTaskDetailInfo.InvestigationTaskStatusID, scope.InvestigationTaskGroupEventID,

                        //           Convert.ToInt32(scope.TeamMemberId), scope.DateTimeofCompletion, Convert.ToInt32(Session["UserId"]), InvTaskGroupId.InvestigationTaskGroupID);
                        //}

                        TaskGroupId = dc.InvestigationTaskGroupEvents.AsQueryable().Where(a => a.InvestigationTaskGroupEventID == scope.InvestigationTaskGroupEventID).FirstOrDefault().InvestigationTaskGroupID;

                        List<string> ListUsers = new List<string>();

                        List<string> ListUsersEmail = new List<string>();

                        if (scope.TeamMemberId != null)
                        {
                            int UserId = Convert.ToInt32(Session["UserId"]);
                            for (int assigned = 0; assigned < scope.TeamMemberId.Count(); assigned++)
                            {
                                int TeamMemberId = Convert.ToInt32(scope.TeamMemberId[assigned]);

                                var TeammberInfo = dc.Users.AsQueryable().Where(a => a.UserID == TeamMemberId).FirstOrDefault();

                                ListUsersEmail.Add(TeammberInfo.EmailAddress.ToString());

                                //ListUsersEmail.Add("ismteamm1@gmail.com");

                                ListUsers.Add(TeammberInfo.FirstName.ToString());
                            }
                            var InvInfo = dc.InvestigationFiles.AsQueryable().Where(a => a.InvestigationFileID == scope.InvestigationId).FirstOrDefault();

                            var LoginUserInfo = dc.Users.AsQueryable().Where(a => a.UserID == UserId).FirstOrDefault();

                            string siteURL = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));

                            // change on 18 june 2019 - mail temp id 4 to 24

                            // bool IsSend = mail.SendMailNotification(4, LoginUserInfo.EmailAddress, LoginUserInfo.UserRoleID, siteURL, ListUsers, ListUsersEmail, InvInfo.InvestigationFileID,

                            //null, InvInfo.InvestigationFileNumber, null, "", "");

                            string eventname = Convert.ToString(dc.InvestigationTaskGroupEvents.Where(a => a.InvestigationTaskGroupEventUniqueNumber == scope.UniqueNumber)

                                                                                               .Select(b => b.InvestigationTaskGroupEventDescription).FirstOrDefault());

                            string taskName = Convert.ToString(dc.InvestigationTaskGroupEvents.Where(a => a.InvestigationTaskGroupEventUniqueNumber == scope.UniqueNumber)

                                                                                               .Select(b => b.InvestigationTaskGroup.InvestigationTaskGroupName).FirstOrDefault());

                            string subject = "Task  : " + taskName.Substring(0,taskName.Length - 6) + ", Sub-Task "+ scope.UniqueNumber.Remove(0, 1) + " : " + eventname + "";

                            bool IsSend = mail.SendMailNotification(24, LoginUserInfo.EmailAddress, LoginUserInfo.UserRoleID, siteURL, ListUsers, ListUsersEmail, InvInfo.InvestigationFileID,

                               subject, InvInfo.InvestigationFileNumber, scope.UniqueNumber, eventname, "");
                        }
                    }
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("TASKController", "Assigning_team_members(InvScope scope)", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }

            string ActionName = string.Empty;

            if (scope.ControlFrom == "IF") { ActionName = "InvestigationFlow"; } else { ActionName = "TaskGroup"; }

            return RedirectToAction(ActionName, new { id = TaskGroupId, invid = scope.InvestigationId });
        }
        /// <summary>
        ///  Open task completion date of the particular task group event for update completion date
        /// </summary> 
        /// <param name="id">Receive investigation task group id </param>
        /// <param name="CtrlFrom">Receive investigation task group event flow </param>
        /// <param name="invid">Receive investigation id </param>
        public ActionResult ChangeCompletionDate(int id = 0, int invid = 0, string CtrlFrom = "")
        {
            InvScope scope = new InvScope();

            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 7)
                {
                    TempData["BackPage"] = "task";
                    TempData["lf_menu"] = "1";
                    TempData["CtrlFrom"] = CtrlFrom;

                    scope.ControlFrom = CtrlFrom;
                    if (CtrlFrom == "TG")
                    {
                        TempData["active_url_Defn_Inv_Scope_iic"] = "iic";
                    }
                    else if (CtrlFrom == "IF")
                    {
                        TempData["active_url_View_Inv_Prg_iic"] = "iic";
                    }

                    var InvInfo = dc.USP_VIEW_INVESTIGATION_INVESTIGATIONID(invid).FirstOrDefault();

                    TempData["PageHead"] = InvInfo.InvestigationFileNumber;

                    TempData["InvId"] = InvInfo.InvestigationFileID;
                    TempData["InvFileID"] = InvInfo.InvestigationFileID;

                    scope.InvestigationId = InvInfo.InvestigationFileID;

                    scope.LocalDateAndTimeOfOccurrence = InvInfo.LocalDateAndTimeOfOccurrence.ToString();

                    scope.UTCDateAndTimeOfOccurrence = InvInfo.UTCDateAndTimeOfOccurrence.ToString();

                    scope.NotifierName = InvInfo.NotifierName;

                    scope.NotifierOrganization = InvInfo.NotifierOrganization;

                    var TeamMembers = dc.USP_VIEW_TOTALNOOF_CASE(invid);

                    ViewBag.TeamMembers = TeamMembers;

                    var TaskGroupID = dc.USP_GET_INVESTIGATION_TaskGroupID_By_InvestigationTaskGroupEventID(id).FirstOrDefault();

                    var InvTaskGroupEventName = dc.InvestigationTaskGroupEvents.AsQueryable().Where

                                    (a => a.InvestigationTaskGroupEventID == id).FirstOrDefault(); // Get Investigation task group Event Name

                    scope.InvestigationTaskGroupEventName = InvTaskGroupEventName.InvestigationTaskGroupEventName;

                    scope.UniqueNumber = InvTaskGroupEventName.InvestigationTaskGroupEventUniqueNumber;

                    scope.InvestigationTaskGroupName = InvTaskGroupEventName.InvestigationTaskGroupEventDescription;

                    scope.InvestigationTaskGroupEventID = id;

                    var AssignedTeammberInfo = dc.InvestigationTaskAssignments.AsQueryable().Where(a => a.InvestigationFileID == scope.InvestigationId &&

                                 a.InvestigationTaskGroupEventID == scope.InvestigationTaskGroupEventID).FirstOrDefault();


                    // var AssignedTeammberInfo = dc.InvestigationMultipleTaskAssignments.AsQueryable().Where(a => a.InvestigationFileID == scope.InvestigationId && //newly added code for multilpe teammber

                    //            a.InvestigationTaskGroupEventID == scope.InvestigationTaskGroupEventID).FirstOrDefault();//newly added code for multilpe teammber

                    scope.DateTimeofCompletion = (Convert.ToDateTime(AssignedTeammberInfo.ExpectedDateOfCompletion));

                    ViewBag.DateTimeofCompletion = comm.convertDateTime(Convert.ToDateTime(AssignedTeammberInfo.ExpectedDateOfCompletion));

                    TempData["id"] = TaskGroupID.InvestigationTaskGroupID;

                    var task_details = dc.USP_VIEW_INVESTIGATION_TASKDETAILS(InvTaskGroupEventName.InvestigationTaskGroupEventUniqueNumber);

                    ViewBag.taskdetails = task_details;

                    scope.InvestigationGroup = TaskGroupID.LKInvestigationTeamName;

                    var assigned_tm = dc.USP_VIEW_ASSIGNED_TEAM_MEMBERS_EVENT(invid, id).ToList();//newly added code for multilpe teammber

                    ViewBag.assigned_team = assigned_tm;
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("TASKController", "Assigning_team_members(int id = 0, int invid = 0)", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }
            return View(scope);
        }
        /// <summary>
        ///  Update completion date of the particular task group event under the particular investigation file id.
        /// </summary> 
        /// <param name="inv_Change">Receive investigation Completion Date</param>
        [HttpPost]
        public ActionResult ChangeCompletionDate(InvScope inv_Change)
        {
            int TaskGroupId = 0;

            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 7)
                {
                    var completion_Date = dc.USP_UPDT_COMPLETION_DATE_TASKS(inv_Change.InvestigationId, inv_Change.InvestigationTaskGroupEventID, inv_Change.DateTimeofCompletion);
                    dc.SaveChanges();
                    TaskGroupId = dc.InvestigationTaskGroupEvents.AsQueryable().Where(a => a.InvestigationTaskGroupEventID == inv_Change.InvestigationTaskGroupEventID).FirstOrDefault().InvestigationTaskGroupID;
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("TASKController", "Assigning_team_members(int id = 0, int invid = 0)", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }
            string ActionName = string.Empty;
            if (inv_Change.ControlFrom == "IF") { ActionName = "InvestigationFlow"; } else { ActionName = "TaskGroup"; }
            return RedirectToAction(ActionName, "Task", new { id = TaskGroupId, invid = inv_Change.InvestigationId });
        }
        /// <summary>
        ///  Remove task group sub events based on { "E1", "E2", "E64", "E65", "E66" };
        /// </summary> 
        /// 
        /// <param name="InvId">Receive investigation file id </param>
        /// <param name="TgEvtId">Receive investigation task group event id </param>
        public ActionResult RemoveTaskSubEvents(int InvId = 0, int TgEvtId = 0)
        {
            int TaskGroupId = 0;
            try
            {
                string[] StaticEvents = { "E1", "E2", "E64", "E65", "E66" };

                string UniqueNo = dc.InvestigationTaskGroupEvents.AsEnumerable().Where(a => a.InvestigationTaskGroupEventID == TgEvtId).

                    FirstOrDefault().InvestigationTaskGroupEventUniqueNumber;

                TaskGroupId = dc.InvestigationTaskGroupEvents.AsEnumerable().Where(a => a.InvestigationTaskGroupEventID == TgEvtId).

                    FirstOrDefault().InvestigationTaskGroupID;

                int ScopeDefId = dc.InvestigationScopeDefinitions.AsEnumerable().Where(a => a.InvestigationFileID == InvId && a.InvestigationTaskGroupID == TaskGroupId).

                    FirstOrDefault().InvestigationScopeDefinitionID;

                int TaskDetId = dc.InvestigationTaskDetails.AsEnumerable().Where(a => a.InvestigationFileID == InvId && a.InvestigationTaskGroupEventID == TgEvtId).

                    FirstOrDefault().InvestigationTaskStatusID;

                int TaskAssignId = dc.InvestigationTaskAssignments.AsEnumerable().Where(a => a.InvestigationFileID == InvId && a.InvestigationTaskGroupEventID == TgEvtId).

                    FirstOrDefault().InvestigationTaskAssignmentID;

                var CheckIfStaticEvent = dc.InvestigationTaskDetails.AsEnumerable().Where(a => a.InvestigationFileID == InvId

                                                                           && a.InvestigationTaskGroupEventUniqueNumber == UniqueNo

                                                                           ).FirstOrDefault();
                if (CheckIfStaticEvent != null)
                {
                    if (CheckIfStaticEvent.InvestigationTaskStatus == 1)
                    {
                        if (StaticEvents.Contains(UniqueNo))
                        {
                            if (UniqueNo == "E2")
                            {
                                int[] E2TaskGroupId = { 7, 9, 10, 11, 12, 13, 14 };

                                var Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(InvId, ScopeDefId, 0, TgEvtId, TaskDetId, TaskAssignId, Convert.ToInt32(Session["UserId"]), "DELETECOMPLETED");

                                foreach (var TaskGrpId in E2TaskGroupId)
                                {
                                    Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(InvId, ScopeDefId, TaskGrpId, TgEvtId, TaskDetId, TaskAssignId, Convert.ToInt32(Session["UserId"]), "UPDATECOMPLETED");
                                }
                            }
                            else if (UniqueNo == "E1" || UniqueNo == "E64" || UniqueNo == "E65" || UniqueNo == "E66")
                            {
                                var Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(InvId, ScopeDefId, 0, TgEvtId, TaskDetId, TaskAssignId, Convert.ToInt32(Session["UserId"]), "DELETECOMPLETED");

                                for (int k = 1; k <= 14; k++)
                                {
                                    Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(InvId, ScopeDefId, k, TgEvtId, TaskDetId, TaskAssignId, Convert.ToInt32(Session["UserId"]), "UPDATECOMPLETED");
                                }
                            }
                        }
                        else
                        {
                            var Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(InvId, ScopeDefId, 0, TgEvtId, TaskDetId, TaskAssignId, Convert.ToInt32(Session["UserId"]), "DELETECOMPLETED");

                            Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(InvId, ScopeDefId, TaskGroupId, TgEvtId, TaskDetId, TaskAssignId, Convert.ToInt32(Session["UserId"]), "UPDATECOMPLETED");
                        }
                    }
                    else
                    {
                        if (StaticEvents.Contains(UniqueNo))
                        {
                            if (UniqueNo == "E2")
                            {
                                int[] E2TaskGroupId = { 7, 9, 10, 11, 12, 13, 14 };

                                var Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(InvId, ScopeDefId, 0, TgEvtId, TaskDetId, TaskAssignId, Convert.ToInt32(Session["UserId"]), "DELETENOTCOMPLETED");

                                foreach (var TaskGrpId in E2TaskGroupId)
                                {
                                    Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(InvId, ScopeDefId, TaskGrpId, TgEvtId, TaskDetId, TaskAssignId, Convert.ToInt32(Session["UserId"]), "UPDATENOTCOMPLETED");
                                }
                            }
                            else if (UniqueNo == "E1" || UniqueNo == "E64" || UniqueNo == "E65" || UniqueNo == "E66")
                            {
                                var Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(InvId, ScopeDefId, 0, TgEvtId, TaskDetId, TaskAssignId, Convert.ToInt32(Session["UserId"]), "DELETENOTCOMPLETED");

                                for (int k = 1; k <= 14; k++)
                                {
                                    Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(InvId, ScopeDefId, k, TgEvtId, TaskDetId, TaskAssignId, Convert.ToInt32(Session["UserId"]), "UPDATENOTCOMPLETED");
                                }
                            }
                        }
                        else
                        {
                            var Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(InvId, ScopeDefId, TaskGroupId, TgEvtId, TaskDetId, TaskAssignId, Convert.ToInt32(Session["UserId"]), "DELETENOTCOMPLETED");

                            Result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_FOR_REMOVAL(InvId, ScopeDefId, TaskGroupId, TgEvtId, TaskDetId, TaskAssignId, Convert.ToInt32(Session["UserId"]), "UPDATENOTCOMPLETED");
                        }

                    }
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("TASKController", "Assigning_team_members(InvScope scope)", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }

            string ActionName = string.Empty;

            return Json(TaskGroupId);
        }
        /// <summary>
        /// View  IIC profile by userid
        /// </summary> 
        /// 
        /// <param name="userid">Receive user login id </param>
        public ActionResult IIC_ViewProfile(int userid = 0)
        {
            var model = new Form39();
            try
            {
                gcaa_ismEntities gcaa = new gcaa_ismEntities();

                model = new Form39
                {

                    iic_view_profile = gcaa.USP_IIC_VIEW_PROFILE(userid).ToList(),
                    iic_view_profile_2 = gcaa.USP_IIC_VIEW_PROFILE_2(userid).ToList(),
                    IIC_Cases = gcaa.USP_VIEW_IIC_CASES(userid).ToList()
                };

            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("TASKController", "IIC_ViewProfile(int userid = 0)", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }
            return View(model);
        }
        /// <summary>
        /// View Assigned team members for removal
        /// </summary> 
        /// 
        /// <param name="id">Receive investigation task group id </param>
        /// <param name="CtrlFrom">Receive investigation task group event flow </param>
        /// <param name="invid">Receive investigation file id </param>
        public ActionResult Assigning_team_members_Removal(int id = 0, int invid = 0, string CtrlFrom = "")
        {
            InvScope scope = new InvScope();
            scope.multipleTeammemberName = new string[50];
            scope.multipleTeammemberRole = new string[50];
            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 7)
                {
                    TempData["BackPage"] = "task";
                    TempData["lf_menu"] = "1";
                    TempData["CtrlFrom"] = CtrlFrom;
                    // TempData["active_url_Defn_Inv_Scope_iic"] = "iic";

                    if (CtrlFrom == "TG")
                    {
                        TempData["active_url_Defn_Inv_Scope_iic"] = "iic";
                    }
                    else if (CtrlFrom == "IF")
                    {
                        TempData["active_url_View_Inv_Prg_iic"] = "iic";
                    }

                    scope.ControlFrom = CtrlFrom;

                    var InvInfo = dc.USP_VIEW_INVESTIGATION_INVESTIGATIONID(invid).FirstOrDefault();

                    TempData["PageHead"] = InvInfo.InvestigationFileNumber;

                    TempData["InvId"] = InvInfo.InvestigationFileID;
                    TempData["InvFileID"] = InvInfo.InvestigationFileID;


                    scope.InvestigationId = InvInfo.InvestigationFileID;

                    scope.LocalDateAndTimeOfOccurrence = InvInfo.LocalDateAndTimeOfOccurrence.ToString();

                    scope.UTCDateAndTimeOfOccurrence = InvInfo.UTCDateAndTimeOfOccurrence.ToString();

                    scope.NotifierName = InvInfo.NotifierName;

                    scope.NotifierOrganization = InvInfo.NotifierOrganization;

                    var operational_TaskReview = dc.USP_VIEW_INVESTIGATION_OPERATIONALTASKREVIEW(id);

                    var UniqueNumber = operational_TaskReview.FirstOrDefault();

                    var TaskGroupID = dc.USP_GET_INVESTIGATION_TaskGroupID_By_InvestigationTaskGroupEventID(id).FirstOrDefault();

                    var InvTaskGroupEventName = dc.InvestigationTaskGroupEvents.AsQueryable().Where

                                    (a => a.InvestigationTaskGroupEventID == id).FirstOrDefault(); // Get Investigation task group Event Name

                    scope.InvestigationTaskGroupEventName = InvTaskGroupEventName.InvestigationTaskGroupEventName;

                    scope.InvestigationTaskGroupName = InvTaskGroupEventName.InvestigationTaskGroupEventDescription;

                    scope.InvestigationTaskGroupEventID = id;


                    TempData["id"] = TaskGroupID.InvestigationTaskGroupID;

                    var assigned_tm = dc.USP_VIEW_ASSIGNED_TEAM_MEMBERS_EVENT(invid, id).ToList();//newly added code for multilpe teammber
                    ViewBag.assigned_team = assigned_tm;                                          //newly added code for multilpe teammber
                    var unassigned_tm = dc.USP_VIEW_UNASSIGNED_TEAM_MEMBERS_EVENT(invid, id).ToList();//newly added code for multilpe teammber
                    ViewBag.unassigned_team = unassigned_tm;                                        //newly added code for multilpe teammber

                    var AssignedTeammberInfo = dc.InvestigationTaskAssignments.AsQueryable().Where(a => a.InvestigationFileID == scope.InvestigationId &&

                                    a.InvestigationTaskGroupEventID == scope.InvestigationTaskGroupEventID).FirstOrDefault();


                    // var AssignedTeammberInfo = dc.InvestigationMultipleTaskAssignments.AsQueryable().Where(a => a.InvestigationFileID == scope.InvestigationId && //newly added code for multilpe teammber

                    //            a.InvestigationTaskGroupEventID == scope.InvestigationTaskGroupEventID).FirstOrDefault();//newly added code for multilpe teammber

                    scope.DateTimeofCompletion = Convert.ToDateTime(AssignedTeammberInfo.ExpectedDateOfCompletion);

                    if (AssignedTeammberInfo != null)
                    {
                        int m = 0;
                        foreach (var assigned in AssignedTeammberInfo.InvestigationMultipleTaskAssignments)
                        {


                            var userInfo = dc.Users.AsQueryable().Where(a => a.UserID == assigned.TeamMemberID).FirstOrDefault();
                            var UserRoleInfo = dc.LKUserRoles.AsQueryable().Where(a => a.LKUserRoleID == userInfo.UserRoleID).FirstOrDefault();
                            scope.multipleTeammemberName[m] = userInfo.FirstName;

                            scope.multipleTeammemberRole[m] = UserRoleInfo.LKUserRoleName;
                            m++;

                        }

                        //var userInfo = dc.Users.AsQueryable().Where(a => a.UserID == AssignedTeammberInfo.TeamMemberID).FirstOrDefault();    

                        // var TeamMembers = dc.USP_VIEW_TOTALNOOF_CASE(invid).Where(a => a.UserID != userInfo.UserID).ToList(); // Except Currently assigned Member

                        //  ViewBag.TeamMembers = TeamMembers;
                    }
                    else
                    {
                        scope.TeammemberName = null;

                        scope.TeammemberRole = null;

                        var TeamMembers = dc.USP_VIEW_TOTALNOOF_CASE(invid);

                        ViewBag.TeamMembers = TeamMembers;
                    }

                    var PreTask = dc.USP_VIEW_INVESTIGATION_PRETASKREVIEW(id).FirstOrDefault();

                    if (PreTask != null)
                    {
                        scope.EventUniqueName = PreTask.InvestigationTaskGroupEventName;

                        scope.EventUniqueNumber = PreTask.InvestigationTaskGroupEventUniqueNumber;

                        scope.PreTaskEventStatus = "NOT FINISHED";

                        var PreTaskEventStatus = dc.USP_VIEW_INVESTIGATION_PRE_TASKGROUPEVENT_STATUS(InvInfo.InvestigationFileID, scope.EventUniqueNumber).FirstOrDefault();

                        if (PreTaskEventStatus != null)
                        {
                            scope.PreTaskEventStatus = "FINISHED";
                        }
                    }
                    else
                    {
                        scope.EventUniqueName = null;

                        scope.EventUniqueNumber = null;

                        scope.PreTaskEventStatus = null;
                    }
                    var task_details = dc.USP_VIEW_INVESTIGATION_TASKDETAILS(InvTaskGroupEventName.InvestigationTaskGroupEventUniqueNumber);

                    ViewBag.taskdetails = task_details;
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("TASKController", "Assigning_team_members(int id = 0, int invid = 0)", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }
            return View(scope);
        }
        /// <summary>
        /// Remove Assigned investigation team member from the investigation
        /// </summary> 
        /// 
        /// <param name="scope">Receive investigation scope details as object </param>
        [HttpPost]
        public ActionResult Assigning_team_members_Removal(InvScope scope)
        {
            var result = 0; int TaskGroupId = 0;

            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 7)
                {
                    if (Convert.ToInt32(Session["UserId"]) > 0)
                    {
                        //result = dc.USP_INST_INVESTIGATION_TASKEVENT_REASSIGN(scope.InvestigationId, Convert.ToInt32(scope.TeamMemberId), scope.InvestigationTaskGroupEventID,

                        //                   Convert.ToInt32(Session["UserId"]), scope.ReasonForTeammemberRemoval); code commented for jira ticket multiple teammember



                        if (scope.multipleUnassignedTeam_mbr_id != null)
                        {
                            for (int unassign_count = 0; unassign_count < scope.multipleUnassignedTeam_mbr_id.Count(); unassign_count++)
                            {
                                result = dc.USP_INST_MULTIPLE_INVESTIGATION_TASKEVENT_REASSIGN(scope.InvestigationId, scope.multipleUnassignedTeam_mbr_id[unassign_count], scope.InvestigationTaskGroupEventID, Convert.ToInt32(Session["UserId"]));
                            }
                        }
                        if (scope.multipleAssignedTeam_mbr_id != null)
                        {
                            for (int assigned_count = 0; assigned_count < scope.multipleAssignedTeam_mbr_id.Count(); assigned_count++)
                            {
                                result = dc.USP_REMOVE_MULTIPLE_INVESTIGATION_TASKEVENT_REASSIGN(scope.InvestigationId, scope.multipleAssignedTeam_mbr_id[assigned_count], scope.InvestigationTaskGroupEventID, Convert.ToInt32(Session["UserId"]), scope.ReasonForTeammemberRemoval);

                            }

                        }
                        dc.SaveChanges();

                        TaskGroupId = dc.InvestigationTaskGroupEvents.AsQueryable().Where(a => a.InvestigationTaskGroupEventID == scope.InvestigationTaskGroupEventID).FirstOrDefault().InvestigationTaskGroupID;

                        List<string> ListUsers = new List<string>();

                        List<string> ListUsersEmail = new List<string>();

                        int UserId = Convert.ToInt32(Session["UserId"]);

                        if (scope.multipleUnassignedTeam_mbr_id != null)
                        {
                            for (int j = 0; j < scope.multipleUnassignedTeam_mbr_id.Count(); j++)
                            {
                                int TeamMemberId = Convert.ToInt32(scope.multipleUnassignedTeam_mbr_id[j]);
                                var TeammberInfo = dc.Users.AsQueryable().Where(a => a.UserID == TeamMemberId).FirstOrDefault();

                                ListUsersEmail.Add(TeammberInfo.EmailAddress);

                                ListUsers.Add(TeammberInfo.FirstName.ToString());
                            }

                            var InvInfo = dc.InvestigationFiles.AsQueryable().Where(a => a.InvestigationFileID == scope.InvestigationId).FirstOrDefault();

                            var LoginUserInfo = dc.Users.AsQueryable().Where(a => a.UserID == UserId).FirstOrDefault();


                            //ListUsersEmail.Add("ismteamm1@gmail.com");

                            string siteURL = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));

                            //4 to 25

                            scope.EventUniqueNumber = dc.InvestigationTaskGroupEvents.Where(a => a.InvestigationTaskGroupEventID == scope.InvestigationTaskGroupEventID)

                                .Select(b => b.InvestigationTaskGroupEventUniqueNumber).FirstOrDefault();

                            bool IsSend = mail.SendMailNotification(25, LoginUserInfo.EmailAddress, LoginUserInfo.UserRoleID, siteURL, ListUsers, ListUsersEmail, InvInfo.InvestigationFileID,

                                null, InvInfo.InvestigationFileNumber, Convert.ToString(scope.EventUniqueNumber), "", "");
                        }
                    }
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("TASKController", "Assigning_team_members(InvScope scope)", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }

            string ActionName = string.Empty;

            if (scope.ControlFrom == "IF") { ActionName = "InvestigationFlow"; } else { ActionName = "TaskGroup"; }

            return RedirectToAction(ActionName, new { id = TaskGroupId, invid = scope.InvestigationId });
        }
        /// <summary>
        /// View Investigation event by investigation id
        /// </summary> 
        /// 
        /// <param name="id">Receive investigation task assignment id </param>
        /// <param name="task_validt_status">Receive investigation task validation status</param>
        public ActionResult InvestigationDetails(int id = 0, int task_validt_status = 0)
        {
            // var task_assignment = dc.USP_VIEW_TASK_ASSIGNMENT_FOR_TEAMMEMBER(id).ToList();
            ViewBag.task_validt_status = task_validt_status;
            var task_assignment = dc.USP_VIEW_TASK_ASSIGNMENT_MULTIPLE_FOR_TEAMMEMBER(id, Convert.ToInt32(Session["UserId"])).ToList();
            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 5 || Convert.ToInt32(Session["RoleId"]) == 7) // || Convert.ToInt32(Session["RoleId"]) == 3
                {
                    TempData["BackPage"] = "yes";
                    //TempData["active_url_Managedistributionlist"] = "tm";
                    TempData["investigationdetail_url"] = "iic";
                    TempData["active_url_manuals_iic"] = "iic";

                    TempData["lf_menu"] = "1";
                    TempData["InvFileID"] = task_assignment[0].InvestigationFileID;
                    string assigned_mult_teammember = string.Empty;
                    var currentsessionjson = dc.InvestigationTaskDetails.AsEnumerable().Where(a => a.InvestigationTaskAssignmentID == id).ToList();
                    string strcurrentsessionjson_ = currentsessionjson[0].InvestigationTaskDataInJSON;

                    Session["ResultJson"] = strcurrentsessionjson_;

                    string oldjsonFieldsProp = "fields";
                    string oldjsonValueProp = "value";
                    string oldjsonNameProp = "name";
                    string modifiedJsonold = string.Empty;
                    DynamicEventforms dy = new DynamicEventforms();

                    //var InvTemmberInfo = dc.InvestigationTaskAssignments.AsEnumerable().Where(a => a.InvestigationFileID == Convert.ToInt32(task_assignment[0].InvestigationFileID) &&

                    //                       a.InvestigationTaskAssignmentID == id).FirstOrDefault(); code commented for jira ticket multiple team member

                    var InvTemmberInfo = dc.InvestigationMultipleTaskAssignments.AsEnumerable().Where(a => a.InvestigationTaskAssignmentID == id).ToList();

                    if (InvTemmberInfo != null)
                    {
                        int teammember_byid = 0;                                                           // code added 
                        for (int membercnt = 0; membercnt < InvTemmberInfo.Count(); membercnt++)
                        {
                            if (InvTemmberInfo[membercnt].TeamMemberID == Convert.ToInt32(Session["UserId"]))
                            {
                                teammember_byid = InvTemmberInfo[membercnt].TeamMemberID;
                                break;

                            }

                        }                                                                                 //code added by jackson@xminds.in
                        if (teammember_byid == Convert.ToInt32(Session["UserId"]))
                        {
                            var InvInfo = dc.USP_VIEW_INVESTIGATION_INVESTIGATIONID(task_assignment[0].InvestigationFileID).FirstOrDefault();

                            TempData["PageHead"] = InvInfo.InvestigationFileNumber;

                            string GlobalTaskGroupEventUniqueNumber = task_assignment[0].InvestigationTaskGroupEventUniqueNumber;
                            int globalspliteventuniquenumber = Convert.ToInt32(GlobalTaskGroupEventUniqueNumber.Split('E')[1]);

                            var result_json = dc.USP_GET_TASKDETAILS_JSON(task_assignment[0].InvestigationFileID, task_assignment[0].InvestigationTaskGroupID).ToList();
                            var tskgrpid = task_assignment[0].InvestigationTaskGroupID;
                            var tskgrpname = (from tskgrpidname in dc.InvestigationTaskGroups
                                              where tskgrpidname.InvestigationTaskGroupID == tskgrpid
                                              select new
                                              {
                                                  tskgrpidname.InvestigationTaskGroupName,
                                                  tskgrpidname.InvestigationTaskGroupSortOrder,

                                              }).ToList();
                            TempData["taskgrpdetailsname"] = tskgrpname[0].InvestigationTaskGroupName;
                            var TaskGroupEventUniqueNumber = (from grpevents in dc.InvestigationTaskGroupEvents
                                                              where grpevents.InvestigationTaskGroupID == tskgrpid
                                                              select new
                                                              {
                                                                  grpevents.InvestigationTaskGroupEventUniqueNumber,
                                                                  grpevents.InvestigationTaskGroupEventName,
                                                                  grpevents.InvestigationTaskGroupEventDescription
                                                              }).ToList();

                            int varcnt = 1;
                            ViewBag.TaskGroupEventUniqueNumberCount = TaskGroupEventUniqueNumber.Count();

                            for (int i = 0; i < TaskGroupEventUniqueNumber.Count(); i++)
                            {
                                var EventUniqueNumber = TaskGroupEventUniqueNumber[i].InvestigationTaskGroupEventUniqueNumber;
                                var FileID = task_assignment[0].InvestigationFileID;
                                var globalcheck = (from invtaskassignment in dc.InvestigationTaskAssignments

                                                   where invtaskassignment.InvestigationTaskGroupEventUniqueNumber == EventUniqueNumber && invtaskassignment.InvestigationFileID == FileID

                                                   select new
                                                   {
                                                       invtaskassignment.InvestigationFileID,
                                                       invtaskassignment.InvestigationTaskAssignmentID,
                                                       invtaskassignment.InvestigationTaskGroupEventID
                                                   }).ToList();
                                string eventheader = "Sub-Task" + TaskGroupEventUniqueNumber[i].InvestigationTaskGroupEventName.Remove(0, 5) + ":" + TaskGroupEventUniqueNumber[i].InvestigationTaskGroupEventDescription;

                                int spliteventuniquenumber = Convert.ToInt32(EventUniqueNumber.Split('E')[1]);




                                if (globalcheck.Count >= 1)
                                {

                                    var taskdetailjson = (from invtaskdetail in dc.InvestigationTaskDetails

                                                          where invtaskdetail.InvestigationTaskGroupEventUniqueNumber == EventUniqueNumber && invtaskdetail.InvestigationFileID == FileID
                                                          //commented for common events retrieval
                                                          //&& invtaskdetail.InvestigationTaskGroupID == tskgrpid
                                                          select new
                                                          {
                                                              invtaskdetail.InvestigationTaskDataInJSON,
                                                              invtaskdetail.ProgressInPercentage,
                                                              invtaskdetail.InvestigationTaskGroupEventID,
                                                              invtaskdetail.InvestigationTaskStatus,
                                                              invtaskdetail.InvestigationTaskGroupEventUniqueNumber,
                                                              invtaskdetail.EventSubmitStatus
                                                          }).ToList();


                                    int taskevent_id = 0;

                                    if (taskdetailjson.Count() > 0)
                                    {
                                        taskevent_id = taskdetailjson[0].InvestigationTaskGroupEventID;
                                    }

                                    var pvttaskassignmentid = (from invtaskdetail in dc.InvestigationTaskAssignments

                                                               where invtaskdetail.InvestigationFileID == FileID && invtaskdetail.InvestigationTaskGroupEventID == taskevent_id
                                                               select new
                                                               {
                                                                   invtaskdetail.InvestigationTaskAssignmentID
                                                               }).ToList();


                                    if (taskdetailjson.Count() > 0)
                                    {
                                        var invtaskgrpid = taskdetailjson[0].InvestigationTaskGroupEventID;
                                        var assigned = (from tskassign in dc.InvestigationTaskAssignments

                                                        join multitaskassign in dc.InvestigationMultipleTaskAssignments on tskassign.InvestigationTaskAssignmentID equals multitaskassign.InvestigationTaskAssignmentID

                                                        where tskassign.InvestigationTaskGroupEventID == invtaskgrpid && tskassign.InvestigationFileID == FileID

                                                        select new
                                                        {
                                                            multitaskassign.TeamMemberID,
                                                            tskassign.ExpectedDateOfCompletion,
                                                            tskassign.InvestigationTaskAssignmentID
                                                        }).ToList();

                                        int assignedname = 0;
                                        for (int m = 0; m < assigned.Count(); m++)
                                        {
                                            if (assigned[m].TeamMemberID == Convert.ToInt32(Session["UserId"]))
                                            {
                                                assignedname = assigned[m].TeamMemberID;
                                                break;
                                            }
                                            else
                                            {
                                                assignedname = assigned[m].TeamMemberID;
                                            }
                                        }

                                        for (int n = 0; n < assigned.Count(); n++)

                                        {
                                            int multi_tm_id = assigned[n].TeamMemberID;

                                            var assigned_name = (from user in dc.Users

                                                                 where user.UserID == multi_tm_id

                                                                 select new
                                                                 {
                                                                     user.FirstName,
                                                                     user.MiddleName,
                                                                     user.LastName
                                                                 }).ToList();
                                            assigned_mult_teammember = assigned_mult_teammember + "," + " " + assigned_name[0].FirstName + " " + assigned_name[0].MiddleName + " " + assigned_name[0].LastName;
                                        }
                                        assigned_mult_teammember = assigned_mult_teammember.Remove(0, 2);
                                        var tm_member = (from tskassign in dc.InvestigationTaskAssignments
                                                         join invtaskdetail in dc.InvestigationTaskDetails on tskassign.InvestigationFileID equals invtaskdetail.InvestigationFileID
                                                         join multitask in dc.InvestigationMultipleTaskAssignments on tskassign.InvestigationTaskAssignmentID equals multitask.InvestigationTaskAssignmentID
                                                         where tskassign.InvestigationTaskGroupEventID == invtaskgrpid
                                                         select new
                                                         {
                                                             multitask.TeamMemberID
                                                         }).Distinct().ToList();
                                        int tm_mbr = 0;
                                        for (int count = 0; count < tm_member.Count(); count++)
                                        {
                                            if (tm_member[count].TeamMemberID == Convert.ToInt32(Session["UserId"]))
                                            {
                                                tm_mbr = tm_member[count].TeamMemberID;
                                                break;
                                            }
                                            else
                                            {
                                                tm_mbr = tm_member[count].TeamMemberID;
                                            }
                                        }
                                        string newjson = taskdetailjson[0].InvestigationTaskDataInJSON;
                                        var old_json = dc.USP_GET_EXISTINGJSON(taskdetailjson[0].InvestigationTaskGroupEventID).ToList();
                                        string oldjson = old_json[0].InvestigationTaskGroupEventJSON;
                                        if (newjson != null)
                                        {
                                            JObject parsedJsonold = JObject.Parse(oldjson);
                                            JObject parsedJsonnew = JObject.Parse(newjson);
                                            if (parsedJsonold[oldjsonFieldsProp] != null)
                                            {
                                                string namePropValue = string.Empty;

                                                JArray jsonold_Array = (JArray)parsedJsonold[oldjsonFieldsProp];
                                                for (int index = 0; index < jsonold_Array.Count; index++)
                                                {
                                                    if (jsonold_Array[index][oldjsonNameProp] != null)
                                                    {
                                                        namePropValue = jsonold_Array[index][oldjsonNameProp].ToString();
                                                        modifiedJsonold = dy.ModifyJsonAPropValueWithJsonBPropValue(oldjsonFieldsProp, oldjsonValueProp, oldjsonNameProp, namePropValue, parsedJsonold, parsedJsonnew, index);
                                                    }
                                                }
                                            }
                                            investigation_fileid = task_assignment[0].InvestigationFileID;
                                            //int teammemberid = Convert.ToInt32(tm_member[0].TeamMemberID);code commented for jira ticket
                                            int teammemberid = tm_mbr;
                                            // ViewBag.html1 = dy.test2(modifiedJsonold, task_assignment[0].InvestigationFileID, id);
                                            TempData["JSON-" + varcnt] = dy.Event_Generation(modifiedJsonold, task_assignment[0].InvestigationFileID, assigned[0].InvestigationTaskAssignmentID, teammemberid, id, taskdetailjson[0].ProgressInPercentage, pvttaskassignmentid[0].InvestigationTaskAssignmentID, Convert.ToInt32(taskdetailjson[0].EventSubmitStatus), Convert.ToInt32(taskdetailjson[0].InvestigationTaskGroupEventUniqueNumber.Split('E')[1]));
                                            //TempData["JSONHEADNAME" + varcnt] = assigned_name[0].FirstName;
                                            TempData["JSONHEADNAME" + varcnt] = assigned_mult_teammember;
                                            assigned_mult_teammember = string.Empty;
                                            TempData["JSONHEADDATE" + varcnt] = (!string.IsNullOrEmpty(Convert.ToString(assigned[0].ExpectedDateOfCompletion)) ? Convert.ToDateTime(assigned[0].ExpectedDateOfCompletion).ToString("dd/MM/yyyy") : "N/A");
                                            ViewData["E/DFlag-" + varcnt] = "e";
                                            ViewData["EHeader-" + varcnt] = eventheader;
                                            TempData["invstatus" + varcnt] = taskdetailjson[0].InvestigationTaskStatus;
                                            TempData["JSONHEADPROGRESS" + varcnt] = taskdetailjson[0].ProgressInPercentage;
                                            if (assigned[0].InvestigationTaskAssignmentID == id)
                                            {
                                                TempData["selectt_" + varcnt] = id;
                                            }
                                            else
                                            {
                                                TempData["selectt_" + varcnt] = 0;
                                            }
                                        }
                                        else
                                        {

                                            int investigation_fileid = task_assignment[0].InvestigationFileID;
                                            // int teammemberid = Convert.ToInt32(tm_member[0].TeamMemberID);code commented for jira ticket
                                            int teammemberid = tm_mbr;
                                            TempData["JSON-" + varcnt] = dy.Event_Generation(oldjson, investigation_fileid, assigned[0].InvestigationTaskAssignmentID, teammemberid, id, taskdetailjson[0].ProgressInPercentage, pvttaskassignmentid[0].InvestigationTaskAssignmentID, Convert.ToInt32(taskdetailjson[0].EventSubmitStatus), Convert.ToInt32(taskdetailjson[0].InvestigationTaskGroupEventUniqueNumber.Split('E')[1]));
                                            TempData["PageHead"] = InvInfo.InvestigationFileNumber;
                                            TempData["BackPage"] = "task_group_disabled";
                                            ViewData["E/DFlag-" + varcnt] = "e";
                                            TempData["lf_menu"] = "1";
                                            // TempData["JSONHEADNAME" + varcnt] = assigned_name[0].FirstName;
                                            TempData["JSONHEADNAME" + varcnt] = assigned_mult_teammember;
                                            assigned_mult_teammember = string.Empty;
                                            TempData["JSONHEADDATE" + varcnt] = (!string.IsNullOrEmpty(Convert.ToString(assigned[0].ExpectedDateOfCompletion)) ? Convert.ToDateTime(assigned[0].ExpectedDateOfCompletion).ToString("dd/MM/yyyy") : "N/A");
                                            TempData["JSONHEADPROGRESS" + varcnt] = taskdetailjson[0].ProgressInPercentage;
                                            TempData["invstatus" + varcnt] = taskdetailjson[0].InvestigationTaskStatus;
                                            ViewData["EHeader-" + varcnt] = eventheader;
                                            if (assigned[0].InvestigationTaskAssignmentID == id)
                                            {
                                                TempData["selectt_" + varcnt] = id;
                                            }
                                            else
                                            {
                                                TempData["selectt_" + varcnt] = 0;
                                            }

                                        }

                                    }

                                    else
                                    {
                                        TempData["JSON-" + varcnt] = "";
                                        ViewData["E/DFlag-" + varcnt] = "d";
                                        TempData["PageHead"] = InvInfo.InvestigationFileNumber; ;
                                        TempData["BackPage"] = "task_group_disabled";
                                        TempData["lf_menu"] = "1";
                                        TempData["JSONHEADNAME" + varcnt] = "Unassigned";
                                        TempData["JSONHEADDATE" + varcnt] = "N/A";
                                        TempData["JSONHEADPROGRESS" + varcnt] = 0;
                                        ViewData["EHeader-" + varcnt] = eventheader;
                                        TempData["invstatus" + varcnt] = taskdetailjson[0].InvestigationTaskStatus;
                                        TempData["selectt_" + varcnt] = 0;
                                    }

                                }
                                else
                                {
                                    TempData["JSON-" + varcnt] = "";
                                    ViewData["E/DFlag-" + varcnt] = "d";
                                    TempData["PageHead"] = InvInfo.InvestigationFileNumber; ;
                                    TempData["BackPage"] = "task_group_disabled";
                                    TempData["lf_menu"] = "1";
                                    TempData["JSONHEADNAME" + varcnt] = "Unassigned";
                                    TempData["JSONHEADDATE" + varcnt] = "N/A";
                                    TempData["JSONHEADPROGRESS" + varcnt] = 0;
                                    ViewData["EHeader-" + varcnt] = eventheader;
                                    TempData["selectt_" + varcnt] = 0;

                                }
                                varcnt++;
                            }
                        }
                        else
                        {
                            return RedirectToAction("Error");
                        }
                    }
                    else
                    {
                        return RedirectToAction("Error");
                    }
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("TASKController", "InvestigationDetails(int id = 0)", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }

            return View(task_assignment);
        }
        /// <summary>
        /// Save the investigation event attached files based on investigation task event by team member dashboard
        /// </summary> 
        /// 
        /// <param name="form">Receive investigation file attachments </param>
        /// <param name="id">Receive investigation task id</param>
        /// <param name="invfile_id">Receive investigation file id</param>
        /// <param name="command">Receive event manipulation command complete or not </param>
        [HttpPost]
        public ActionResult SaveFiles(FormCollection form, int id, int invfile_id, string command)
        {
            try
            {

                var filesavesessionjson = dc.InvestigationTaskDetails.AsQueryable().Where(a => a.InvestigationTaskAssignmentID == id).ToList();
                string event_desc = filesavesessionjson[0].InvestigationTaskGroupEventUniqueNumber;
                var eventname = dc.InvestigationTaskGroupEvents.AsQueryable().Where(a => a.InvestigationTaskGroupEventUniqueNumber == event_desc).FirstOrDefault();

                int event_number = Convert.ToInt32(filesavesessionjson[0].InvestigationTaskGroupEventUniqueNumber.Split('E')[1]);
                string strfilesaveessionjson_ = filesavesessionjson[0].InvestigationTaskDataInJSON;
                Session["latestJson"] = strfilesaveessionjson_;
                if (Convert.ToString(Session["ResultJson"]) == Convert.ToString(Session["latestJson"]))
                {

                    dynamic tskresult = 0;
                    string file_upl = string.Empty;
                    int j = 0;
                    Dictionary<string, List<string>> deleted_jsonnamevalue = new Dictionary<string, List<string>>();

                    for (int del_cnt = 0; del_cnt < form.Count; del_cnt++)
                    {
                        if (form.Keys[del_cnt].Contains("deleted-"))
                        {
                            string deleted_key = form.Keys[del_cnt];
                            string deleted_key_dictonary = form.Keys[del_cnt].Split('-')[1];
                            string values = form[deleted_key];
                            List<string> list = new List<string>();

                            string[] deletedvalues = values.Split(',');
                            for (int i = 0; i < deletedvalues.Length; i++)
                            {
                                list.Add(deletedvalues[i]);
                            }
                            deleted_jsonnamevalue.Add(deleted_key_dictonary, list);
                        }
                    }

                    string sl = form["TeamAssignID"];
                    int taskid = Convert.ToInt32(sl);
                    string exist_json = string.Empty;
                    var task_assignment_json = dc.USP_VIEW_TASK_ASSIGNMENT_FOR_TEAMMEMBER(taskid).ToList();

                    var taskdetails_json = dc.USP_GET_TASKDETAILS_JSON_BY_EVENT(task_assignment_json[0].InvestigationFileID, task_assignment_json[0].InvestigationTaskGroupEventID).ToList();

                    string taskdetailsjsonstr = taskdetails_json[0].InvestigationTaskDataInJSON;

                    foreach (var jsonitem in task_assignment_json)
                    {
                        exist_json = jsonitem.InvestigationTaskGroupEventJSON;
                    }
                    dynamic formDetails = JObject.Parse(exist_json);
                    var fields = formDetails.fields;
                    Dictionary<string, List<string>> jsonnamevalue = new Dictionary<string, List<string>>();
                    int txtcnt = 0;
                    int txtboxcnt = 0;
                    for (var i = 0; i < fields.Count; i++)

                    {
                        List<string> list_json = new List<string>();

                        if (fields[i].field_type == "textarea")
                        {

                            txtcnt++;
                            file_upl = fields[i].name;
                            string val = form[file_upl];
                            if (val != "")
                            {
                                j++;
                            }
                            list_json.Add(val);
                            jsonnamevalue.Add(file_upl, list_json);



                        }

                        if (fields[i].field_type == "checkbox")
                        {
                            //  int checkboxcnt = 0;
                            string field_name = fields[i].name;
                            string val = form[field_name];
                            if (val != null)
                            {

                                list_json.Add("1");
                            }
                            else
                            {
                                list_json.Add("0");
                            }
                            jsonnamevalue.Add(field_name, list_json);

                        }

                        if (fields[i].field_type == "text")
                        {
                            txtboxcnt++;
                            file_upl = fields[i].name;
                            string val = form[file_upl];
                            if (val != "")
                            {
                                j++;
                            }

                            list_json.Add(val);
                            jsonnamevalue.Add(file_upl, list_json);

                        }
                        if (fields[i].field_type == "text-tbl")
                        {
                            txtboxcnt++;
                            file_upl = fields[i].name;
                            string val = form[file_upl];
                            if (val != "")
                            {
                                j++;
                            }

                            list_json.Add(val);
                            jsonnamevalue.Add(file_upl, list_json);

                        }
                        if (fields[i].field_type == "datepicker")
                        {
                            txtboxcnt++;
                            file_upl = fields[i].name;
                            string val = form[file_upl];
                            if (val != "")
                            {
                                j++;
                            }

                            list_json.Add(val);
                            jsonnamevalue.Add(file_upl, list_json);

                        }
                        if (fields[i].field_type == "file")
                        {


                            //                  comm.CreateIfMissing(Server.MapPath("~/") + "AttachmentEventFiles");
                            int count = 0;
                            int s = 0;

                            for (int l = 0; l < Request.Files.Count; l++)
                            {
                                if (fields[i].name == Request.Files.AllKeys[l])
                                {

                                    count++;
                                    s = l;

                                }

                            }
                            int intial_index = (s - count) + 1;
                            // int bm = 0;
                            for (int intial = intial_index; intial <= s; intial++)
                            {
                                HttpPostedFileBase file = Request.Files[intial];

                                int fileSize = file.ContentLength;

                                if (file.ContentLength > 0)
                                {
                                    string sm = Request.Files.AllKeys[intial];

                                    //string fileName = file.FileName;

                                    string fileName = file.FileName.Substring(0, file.FileName.LastIndexOf('.')) + "_" + DateTime.Now.ToFileTime() + file.FileName.Substring(file.FileName.LastIndexOf('.'), ((file.FileName.Length) - (file.FileName.LastIndexOf('.'))));

                                    string mimeType = file.ContentType.Split('/')[1].ToString();

                                    int DocumentTypeId = Convert.ToInt32(dc.LKDocumentTypes.AsQueryable().Where(d => d.LKDocumentTypeName == mimeType).Select(d => d.LKDocumentTypeID).FirstOrDefault());

                                    Stream fileContent = file.InputStream;

                                    // file.SaveAs(Server.MapPath("~/") + "AttachmentEventFiles\\" + fileName);
                                    string doc_llibrary = "Event_Documents";
                                    //string[] SharepointfilePath = comm.SharePointEventDocs(fileContent, Server.MapPath("~/") + "AttachmentEventFiles\\" + fileName, Convert.ToString(investigation_fileid), Convert.ToString(investigation_fileid), task_assignment_json[0].InvestigationTaskGroupEventUniqueNumber, doc_llibrary);
                                    string[] SharepointfilePath = comm.savetoSharePointEventDocsOnPrem(fileContent, fileName, Convert.ToString(invfile_id), Convert.ToString(invfile_id), task_assignment_json[0].InvestigationTaskGroupEventUniqueNumber, doc_llibrary);

                                    ObjectParameter InvestigationDocumentAsAttachment_ID_ = new ObjectParameter("InvestigationDocumentAsAttachment_ID_out", typeof(int));
                                    var result1 = dc.USP_INST_EVENT_INVESTIGATION_DOCUMENTASATTACHMENT(0, invfile_id, 2, DocumentTypeId, SharepointfilePath[1], InvestigationDocumentAsAttachment_ID_);
                                    string sk = InvestigationDocumentAsAttachment_ID_.Value.ToString();
                                    file_upl = fields[i].name;
                                    list_json.Add(sk);

                                    dc.SaveChanges();
                                }
                                else
                                {
                                    file_upl = fields[i].name;
                                    list_json.Add("");

                                }
                            }

                            jsonnamevalue.Add(file_upl, list_json);
                        }
                        //if (Directory.Exists("AttachmentEventFiles"))

                        //    comm.DeleteDirectory(Server.MapPath("~/") + "AttachmentEventFiles");

                    }

                    if (taskdetailsjsonstr != null)
                    {
                        JObject taskformDetails = JObject.Parse(taskdetailsjsonstr);
                        int count = taskformDetails.Count;
                        var taskjsonvalues = JsonConvert.DeserializeObject<Dictionary<string, List<string>>>(taskdetailsjsonstr);
                        Dictionary<string, List<string>> deserialjson = new Dictionary<string, List<string>>();
                        deserialjson = taskjsonvalues;
                        foreach (KeyValuePair<string, List<string>> items in jsonnamevalue)
                        {
                            string key = items.Key;
                            List<string> values = items.Value;
                            if ((deserialjson.ContainsKey(key)))
                            {
                                for (int i = 0; i < values.Count(); i++)
                                {
                                    if (key.Contains("text") || key.Contains("checkbox"))
                                    {
                                        deserialjson[key].Remove(deserialjson[key][i]);
                                        deserialjson[key].Add(values[i]);

                                    }
                                    else
                                    {
                                        if (values[i] != "")
                                        {
                                            for (int m = 0; m < deserialjson[key].Count; m++)
                                            {
                                                if (deserialjson[key][m] == "")
                                                {
                                                    deserialjson[key].Remove(deserialjson[key][m]);
                                                }
                                            }
                                            deserialjson[key].Add(values[i]);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                deserialjson.Add(key, values);
                            }
                        }

                        //delete code writes here
                        foreach (KeyValuePair<string, List<string>> deleteditems in deleted_jsonnamevalue)
                        {
                            string key = deleteditems.Key;
                            List<string> values = deleteditems.Value;
                            int count_del_json = deserialjson[key].Count;
                            if ((deserialjson.ContainsKey(key)))
                            {
                                for (int i = 0; i < values.Count(); i++)
                                {

                                    for (int m = 0; m < deserialjson[key].Count; m++)
                                    {
                                        if (deserialjson[key][m] == deleteditems.Value[i].Trim())
                                        {
                                            dc.USP_INST_DocumentAttachmentHistory(task_assignment_json[0].InvestigationFileID, Convert.ToInt32(deserialjson[key][m]),/* task_assignment_json[0].TeamMemberID*/Convert.ToInt32(Session["UserId"]));
                                            deserialjson[key].Remove(deserialjson[key][m]);

                                            break;
                                        }
                                    }

                                    if (deserialjson[key].Count == 0)
                                    {
                                        deserialjson[key].Add("");
                                    }
                                }
                            }

                        }

                        string dynjson = JsonConvert.SerializeObject(deserialjson);
                        decimal progcount = 0;
                        decimal totalcount = deserialjson.Count();
                        foreach (KeyValuePair<string, List<string>> progresscount in deserialjson)
                        {
                            string key = progresscount.Key;
                            List<string> values = progresscount.Value;
                            for (int i = 0; i < values.Count; i++)
                            {
                                if (values[0] != "")
                                {
                                    progcount++;
                                    break;

                                }
                            }

                        }

                        decimal convert = (progcount / totalcount) * 100;
                        int progress_in_percentage = Convert.ToInt32(convert);

                        if (progress_in_percentage >= 80)
                        {
                            progress_in_percentage = 80;
                        }


                        foreach (string tskcmpltkey in jsonnamevalue.Keys)
                        {
                            if (tskcmpltkey.Contains(".checkbox"))
                            {
                                if (deserialjson[tskcmpltkey][0] == "1")
                                {
                                    ViewBag.task_cmplete_validation_flag = 0;
                                    break;

                                }
                                else
                                {
                                    ViewBag.task_cmplete_validation_flag = 1;
                                }

                            }

                            else if (deserialjson[tskcmpltkey][0] != "")
                            {
                                ViewBag.task_cmplete_validation_flag = 0;
                                break;

                            }
                            else
                            {
                                ViewBag.task_cmplete_validation_flag = 1;
                            }

                        }

                        if (ViewBag.task_cmplete_validation_flag == 0)
                        {

                            var result = dc.USP_UPDT_TASKDETAILS_WITH_JSON(task_assignment_json[0].InvestigationTaskGroupEventID, Convert.ToInt32(task_assignment_json[0].InvestigationFileID), progress_in_percentage, dynjson, 0, Convert.ToInt32(Session["UserId"]));
                            var inst_result = dc.USP_INST_TASKDETAILSHISTORY_WITH_JSON(task_assignment_json[0].InvestigationTaskGroupEventID, Convert.ToInt32(task_assignment_json[0].InvestigationFileID), progress_in_percentage, dynjson, 0, Convert.ToInt32(Session["UserId"]));

                            if (command == "Complete")
                            {

                                int investigation_fileid = Convert.ToInt32(invfile_id); int retval = 0;
                                int taskassignmentid = Convert.ToInt32(id);

                                var TaskGroupEventID = (from tskassign in dc.InvestigationTaskAssignments

                                                        where tskassign.InvestigationTaskAssignmentID == taskassignmentid && tskassign.InvestigationFileID == investigation_fileid

                                                        select new
                                                        {
                                                            tskassign.InvestigationTaskGroupEventID,

                                                            tskassign.InvestigationTaskGroupEventUniqueNumber

                                                        }).ToList();

                                //int group_event_id = TaskGroupEventID[0].InvestigationTaskGroupEventID; 

                                string group_event_UniqueNo = TaskGroupEventID[0].InvestigationTaskGroupEventUniqueNumber;

                                var predecessorid = (from prdid in dc.InvestigationTaskGroupEvents

                                                     where prdid.InvestigationTaskGroupPreEventUniqueNumber == group_event_UniqueNo

                                                     select new
                                                     {
                                                         prdid.InvestigationTaskGroupEventUniqueNumber

                                                     }).ToList();

                                if (predecessorid.Count > 0)
                                {
                                    string event_unique_number = predecessorid[0].InvestigationTaskGroupEventUniqueNumber;

                                    var searchsuccessor = (from tskdet in dc.InvestigationTaskDetails

                                                           where tskdet.InvestigationTaskGroupEventUniqueNumber == event_unique_number && tskdet.InvestigationFileID == investigation_fileid

                                                           select new
                                                           {
                                                               tskdet.InvestigationTaskGroupEventUniqueNumber,
                                                               tskdet.InvestigationTaskGroupEventID,

                                                           }).ToList();

                                    if (searchsuccessor.Count > 0)
                                    {
                                        retval = dc.USP_UPDT_TASKSTATUS_IN_TASKDETAIL_FOR_COMPLETE(investigation_fileid, searchsuccessor[0].InvestigationTaskGroupEventID);
                                    }
                                }

                                // Update Investigation Progress Percentage In Scope Definition Table

                                var InvEventList = dc.InvestigationTaskGroupEvents.Where(a => a.InvestigationTaskGroupEventUniqueNumber == group_event_UniqueNo).ToList();

                                //foreach (var itm in InvEventList)
                                //{
                                //    retval = dc.USP_UPDT_INVESTIGATION_SCOPEDEFINITION_PROGRESS(investigation_fileid, itm.InvestigationTaskGroupID);
                                //}
                                if (progress_in_percentage >= 80)
                                {
                                    progress_in_percentage = 80;
                                }
                                retval = dc.USP_UPDT_TASKSTATUS_IN_TASKDETAIL_TEAMMEMBER(investigation_fileid, TaskGroupEventID[0].InvestigationTaskGroupEventID, progress_in_percentage);

                                // retval = dc.USP_UPDT_TASKSTATUS_IN_TASKDETAIL(investigation_fileid, TaskGroupEventID[0].InvestigationTaskGroupEventID);

                                var tomailinfo = dc.USP_GET_USER_EMAIL_DETAILS(investigation_fileid).ToList();

                                List<string> DListUsersEmail = new List<string>();

                                List<string> DListUsers = new List<string>();
                                foreach (var users in tomailinfo)
                                {
                                    DListUsersEmail.Add(users.EmailAddress);
                                    DListUsers.Add(users.EmailAddress);


                                }
                                // string full_name = tomailinfo[0].FirstName + " " + tomailinfo[0].MiddleName + " " + tomailinfo[0].LastName;  -- on 18-june-2019

                                int userid = Convert.ToInt32(Session["Userid"]);

                                var userinfo = dc.Users.Where(a => a.UserID == userid).FirstOrDefault();

                                string full_name = Convert.ToString(userinfo.FirstName) + " " + Convert.ToString(userinfo.MiddleName) + " " + Convert.ToString(userinfo.LastName);

                                bool IsSend = mail.SendMailNotification(16, null, 0, null, DListUsers, DListUsersEmail, event_number, null, tomailinfo[0].InvestigationFileNumber, full_name, eventname.InvestigationTaskGroupEventDescription, eventname.InvestigationTaskGroupEventDescription);


                                int EventID = TaskGroupEventID[0].InvestigationTaskGroupEventID;

                                tskresult = (from tskassign in dc.InvestigationTaskDetails

                                             where tskassign.InvestigationTaskGroupEventID == EventID && tskassign.InvestigationFileID == investigation_fileid

                                             select new
                                             {
                                                 tskassign.ProgressInPercentage,

                                             }).ToList();
                            }

                            return RedirectToAction("InvestigationDetails", "task", new { @id = id });
                        }
                        else
                        {
                            return RedirectToAction("InvestigationDetails", "task", new { @id = id, @task_validt_status = ViewBag.task_cmplete_validation_flag });
                        }
                    }

                    else
                    {

                        string dynjson = JsonConvert.SerializeObject(jsonnamevalue);

                        decimal prog_count_else = 0;
                        decimal totalcount_else = jsonnamevalue.Count();
                        foreach (KeyValuePair<string, List<string>> progresscount in jsonnamevalue)
                        {
                            string key = progresscount.Key;
                            List<string> values = progresscount.Value;
                            for (int i = 0; i < values.Count; i++)
                            {
                                if (values[0] != "")
                                {
                                    prog_count_else++;
                                    break;
                                }
                            }

                        }
                        decimal convert_else = (prog_count_else / totalcount_else) * 100;
                        int progress_in_percentage_else = Convert.ToInt32(convert_else);

                        if (progress_in_percentage_else >= 80)
                        {
                            progress_in_percentage_else = 80;
                        }

                        foreach (string tskcmpltkey in jsonnamevalue.Keys)
                        {

                            if (tskcmpltkey.Contains(".checkbox"))
                            {
                                if (jsonnamevalue[tskcmpltkey][0] == "1")
                                {
                                    ViewBag.task_cmplete_validation_flag = 0;
                                    break;

                                }
                                else
                                {
                                    ViewBag.task_cmplete_validation_flag = 1;
                                }

                            }


                            else if (jsonnamevalue[tskcmpltkey][0] != "")
                            {
                                ViewBag.task_cmplete_validation_flag = 0;
                                break;

                            }
                            else
                            {
                                ViewBag.task_cmplete_validation_flag = 1;
                            }

                        }

                        if (ViewBag.task_cmplete_validation_flag == 0)
                        {

                            var result = dc.USP_UPDT_TASKDETAILS_WITH_JSON(task_assignment_json[0].InvestigationTaskGroupEventID, Convert.ToInt32(task_assignment_json[0].InvestigationFileID), progress_in_percentage_else, dynjson, 0, Convert.ToInt32(Session["UserId"]));
                            var inst_result = dc.USP_INST_TASKDETAILSHISTORY_WITH_JSON(task_assignment_json[0].InvestigationTaskGroupEventID, Convert.ToInt32(task_assignment_json[0].InvestigationFileID), progress_in_percentage_else, dynjson, 0, Convert.ToInt32(Session["UserId"]));

                            if (command == "Complete")
                            {
                                int investigation_fileid = Convert.ToInt32(invfile_id); int retval = 0;
                                int taskassignmentid = Convert.ToInt32(id);

                                var TaskGroupEventID = (from tskassign in dc.InvestigationTaskAssignments

                                                        where tskassign.InvestigationTaskAssignmentID == taskassignmentid && tskassign.InvestigationFileID == investigation_fileid

                                                        select new
                                                        {
                                                            tskassign.InvestigationTaskGroupEventID,

                                                            tskassign.InvestigationTaskGroupEventUniqueNumber

                                                        }).ToList();

                                //int group_event_id = TaskGroupEventID[0].InvestigationTaskGroupEventID; 

                                string group_event_UniqueNo = TaskGroupEventID[0].InvestigationTaskGroupEventUniqueNumber;

                                var predecessorid = (from prdid in dc.InvestigationTaskGroupEvents

                                                     where prdid.InvestigationTaskGroupPreEventUniqueNumber == group_event_UniqueNo

                                                     select new
                                                     {
                                                         prdid.InvestigationTaskGroupEventUniqueNumber

                                                     }).ToList();

                                if (predecessorid.Count > 0)
                                {
                                    string event_unique_number = predecessorid[0].InvestigationTaskGroupEventUniqueNumber;

                                    var searchsuccessor = (from tskdet in dc.InvestigationTaskDetails

                                                           where tskdet.InvestigationTaskGroupEventUniqueNumber == event_unique_number && tskdet.InvestigationFileID == investigation_fileid

                                                           select new
                                                           {
                                                               tskdet.InvestigationTaskGroupEventUniqueNumber,
                                                               tskdet.InvestigationTaskGroupEventID,

                                                           }).ToList();

                                    if (searchsuccessor.Count > 0)
                                    {
                                        retval = dc.USP_UPDT_TASKSTATUS_IN_TASKDETAIL_FOR_COMPLETE(investigation_fileid, searchsuccessor[0].InvestigationTaskGroupEventID);
                                    }
                                }

                                // Update Investigation Progress Percentage In Scope Definition Table

                                var InvEventList = dc.InvestigationTaskGroupEvents.Where(a => a.InvestigationTaskGroupEventUniqueNumber == group_event_UniqueNo).ToList();

                                //foreach (var itm in InvEventList)
                                //{
                                //    retval = dc.USP_UPDT_INVESTIGATION_SCOPEDEFINITION_PROGRESS(investigation_fileid, itm.InvestigationTaskGroupID);
                                //}



                                retval = dc.USP_UPDT_TASKSTATUS_IN_TASKDETAIL_TEAMMEMBER(investigation_fileid, TaskGroupEventID[0].InvestigationTaskGroupEventID, progress_in_percentage_else);



                                var tomailinfo = dc.USP_GET_USER_EMAIL_DETAILS(investigation_fileid).ToList();

                                List<string> DListUsersEmail = new List<string>();

                                List<string> DListUsers = new List<string>();
                                foreach (var users in tomailinfo)
                                {
                                    DListUsersEmail.Add(users.EmailAddress);
                                    DListUsers.Add(users.EmailAddress);


                                }

                                //string full_name = tomailinfo[0].FirstName + " " + tomailinfo[0].MiddleName + " " + tomailinfo[0].LastName; -- on 18-june-2019

                                int userid = Convert.ToInt32(Session["Userid"]);

                                var userinfo = dc.Users.Where(a => a.UserID == userid).FirstOrDefault();

                                string full_name = Convert.ToString(userinfo.FirstName) + " " + Convert.ToString(userinfo.MiddleName) + " " + Convert.ToString(userinfo.LastName);

                                bool IsSend = mail.SendMailNotification(16, null, 0, null, DListUsers, DListUsersEmail, event_number, null, tomailinfo[0].InvestigationFileNumber, full_name, eventname.InvestigationTaskGroupEventDescription, eventname.InvestigationTaskGroupEventDescription);

                                int EventID = TaskGroupEventID[0].InvestigationTaskGroupEventID;

                                tskresult = (from tskassign in dc.InvestigationTaskDetails

                                             where tskassign.InvestigationTaskGroupEventID == EventID && tskassign.InvestigationFileID == investigation_fileid

                                             select new
                                             {
                                                 tskassign.ProgressInPercentage,

                                             }).ToList();
                            }
                            return RedirectToAction("InvestigationDetails", "task", new { @id = id });
                        }
                        else
                        {
                            return RedirectToAction("InvestigationDetails", "task", new { @id = id, task_validt_status = ViewBag.task_cmplete_validation_flag });
                        }
                    }
                }

                else
                {
                    ViewBag.task_cmplete_validation_flag = 2;
                    return RedirectToAction("InvestigationDetails", "task", new { @id = id, task_validt_status = ViewBag.task_cmplete_validation_flag });
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("TASKController", "SaveFiles(FormCollection form, int id)", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }

        }
        /// <summary>
        /// Open the task complete view
        /// </summary> 
        /// 
        /// <param name="investigationfileid">Receive investigation file id</param>
        /// <param name="task_id">Receive investigation task id</param>
        public ActionResult EventTaskComplete(string investigationfileid, string task_id)
        {
            dynamic result = 0;

            if (Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 5 || Convert.ToInt32(Session["RoleId"]) == 7)
            {

                int investigation_fileid = Convert.ToInt32(investigationfileid); int retval = 0;
                int taskassignmentid = Convert.ToInt32(task_id);

                var TaskGroupEventID = (from tskassign in dc.InvestigationTaskAssignments

                                        where tskassign.InvestigationTaskAssignmentID == taskassignmentid && tskassign.InvestigationFileID == investigation_fileid

                                        select new
                                        {
                                            tskassign.InvestigationTaskGroupEventID,

                                            tskassign.InvestigationTaskGroupEventUniqueNumber

                                        }).ToList();

                //int group_event_id = TaskGroupEventID[0].InvestigationTaskGroupEventID; 

                string group_event_UniqueNo = TaskGroupEventID[0].InvestigationTaskGroupEventUniqueNumber;

                var predecessorid = (from prdid in dc.InvestigationTaskGroupEvents

                                     where prdid.InvestigationTaskGroupPreEventUniqueNumber == group_event_UniqueNo

                                     select new
                                     {
                                         prdid.InvestigationTaskGroupEventUniqueNumber

                                     }).ToList();

                if (predecessorid.Count > 0)
                {
                    string event_unique_number = predecessorid[0].InvestigationTaskGroupEventUniqueNumber;

                    var searchsuccessor = (from tskdet in dc.InvestigationTaskDetails

                                           where tskdet.InvestigationTaskGroupEventUniqueNumber == event_unique_number && tskdet.InvestigationFileID == investigation_fileid

                                           select new
                                           {
                                               tskdet.InvestigationTaskGroupEventUniqueNumber,
                                               tskdet.InvestigationTaskGroupEventID,

                                           }).ToList();

                    if (searchsuccessor.Count > 0)
                    {
                        retval = dc.USP_UPDT_TASKSTATUS_IN_TASKDETAIL_FOR_COMPLETE(investigation_fileid, searchsuccessor[0].InvestigationTaskGroupEventID);
                    }
                }

                // Update Investigation Progress Percentage In Scope Definition Table

                var InvEventList = dc.InvestigationTaskGroupEvents.Where(a => a.InvestigationTaskGroupEventUniqueNumber == group_event_UniqueNo).ToList();

                foreach (var itm in InvEventList)
                {
                    retval = dc.USP_UPDT_INVESTIGATION_SCOPEDEFINITION_PROGRESS(investigation_fileid, itm.InvestigationTaskGroupID);
                }


                retval = dc.USP_UPDT_TASKSTATUS_IN_TASKDETAIL(investigation_fileid, TaskGroupEventID[0].InvestigationTaskGroupEventID);

                int EventID = TaskGroupEventID[0].InvestigationTaskGroupEventID;

                result = (from tskassign in dc.InvestigationTaskDetails

                          where tskassign.InvestigationTaskGroupEventID == EventID && tskassign.InvestigationFileID == investigation_fileid

                          select new
                          {
                              tskassign.ProgressInPercentage,

                          }).ToList();

            }
            else
            {
                return RedirectToAction("Dashboard", "Home");
            }
            if (result.Count > 0)
            {
                return Content("success");
            }
            else
            {
                return Content("failed");
            }
        }
        /// <summary>
        /// View the Investigation progress percentage based on each task group and investigation file id
        /// </summary> 
        /// 
        /// <param name="invid">Receive investigation file id </param>
        /// <param name="id">Receive investigation task group id </param>
        public ActionResult InvestigationFlow(int id = 0, int invid = 0)
        {
            InvScope scope = new InvScope();

            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 3 || Convert.ToInt32(Session["RoleId"]) == 6 || Convert.ToInt32(Session["RoleId"]) == 7)
                {
                    scope.ControlFrom = "IF";
                    TempData["Taskgroupid"] = id;
                    var InvInfo = dc.USP_VIEW_INVESTIGATION_INVESTIGATIONID(invid).FirstOrDefault();

                    TempData["PageHead"] = InvInfo.InvestigationFileNumber;
                    TempData["InvId"] = InvInfo.InvestigationFileID;
                    TempData["InvFileID"] = InvInfo.InvestigationFileID;

                    TempData["active_url_View_Inv_Prg"] = "tm";
                    TempData["active_url_View_Inv_Prg_iic"] = "iic";
                    TempData["BackPage"] = "inv_prog";
                    TempData["lf_menu"] = "1";

                    var InvTaskGroupName = dc.InvestigationTaskGroups.AsQueryable().Where

                                  (a => a.InvestigationTaskGroupID == id).FirstOrDefault(); // Get Investigation task group Name

                    scope.InvestigationTaskGroupName = InvTaskGroupName.InvestigationTaskGroupName;

                    scope.InvestigationId = InvInfo.InvestigationFileID;

                    scope.UTCDateAndTimeOfOccurrence = InvInfo.UTCDateAndTimeOfOccurrence.ToString();

                    scope.LocalDateAndTimeOfOccurrence = InvInfo.LocalDateAndTimeOfOccurrence.ToString();

                    scope.NotifierName = InvInfo.NotifierName;

                    scope.NotifierOrganization = InvInfo.NotifierOrganization;

                    // var NotStartedTask = dc.USP_VIEW_INVESTIGATION_TASKGROUPEVENTS_WITHCONDITION(InvInfo.InvestigationFileID, id, "UNASSIGNED"); CODE COMMENTED FOR MULTIPLE TEAM MEMBER
                    var NotStartedTask = dc.USP_VIEW_INVESTIGATION_MULTIPLE_TEAM_MEMBER_TASKGROUPEVENTS_WITHCONDITION(InvInfo.InvestigationFileID, id, "UNASSIGNED");
                    ViewBag.NotStared_Task = NotStartedTask;

                    // var InprogressTask = dc.USP_VIEW_INVESTIGATION_TASKGROUPEVENTS_WITHCONDITION(InvInfo.InvestigationFileID, id, "INPROGRESS");
                    var InprogressTask = dc.USP_VIEW_INVESTIGATION_MULTIPLE_TEAM_MEMBER_TASKGROUPEVENTS_WITHCONDITION(InvInfo.InvestigationFileID, id, "INPROGRESS");
                    ViewBag.Inprogress_Task = InprogressTask;

                    // var OverDueTask = dc.USP_VIEW_INVESTIGATION_TASKGROUPEVENTS_WITHCONDITION(InvInfo.InvestigationFileID, id, "OVERDUE");
                    var OverDueTask = dc.USP_VIEW_INVESTIGATION_MULTIPLE_TEAM_MEMBER_TASKGROUPEVENTS_WITHCONDITION(InvInfo.InvestigationFileID, id, "OVERDUE");

                    ViewBag.OverDue_Task = OverDueTask;

                    //  var CompletedTask = dc.USP_VIEW_INVESTIGATION_TASKGROUPEVENTS_WITHCONDITION(InvInfo.InvestigationFileID, id, "COMPLETED");

                    var CompletedTask = dc.USP_VIEW_INVESTIGATION_MULTIPLE_TEAM_MEMBER_TASKGROUPEVENTS_WITHCONDITION(InvInfo.InvestigationFileID, id, "COMPLETED");
                    ViewBag.Completed_task = CompletedTask;

                    var invInfo = dc.USP_VIEW_INVESTIGATION_FORM35(invid).FirstOrDefault();

                    TempData["INV_STATUS"] = invInfo.LKInvestigationStatusTypeID;
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("TASKController", "InvestigationFlow(int invid=0)", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }
            return View(scope);
        }
        /// <summary>
        /// View the Investigation event 
        /// </summary> 
        /// 
        /// <param name="tskgrp_id">Receive investigation task group id </param>
        /// <param name="id">Receive investigation task assignment id </param>
        /// <param name="task_validt_status">Receive investigation task validation status </param>
        public ActionResult IICeventview(int id = 0, int tskgrp_id = 0, int task_validt_status = 0)
        {
            var task_assignment = dc.USP_VIEW_TASK_ASSIGNMENT_FOR_TEAMMEMBER(id).ToList();
            ViewBag.task_validt_status = task_validt_status;
            //var task_assignment = dc.USP_VIEW_TASK_ASSIGNMENT_MULTIPLE_FOR_TEAMMEMBER(id, Convert.ToInt32(Session["UserId"])).ToList();
            DynamicEventForms_IIC dynamic = new DynamicEventForms_IIC();
            try
            {
                string assigned_mult_teammember = string.Empty;
                if (Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 3 || Convert.ToInt32(Session["RoleId"]) == 6 || Convert.ToInt32(Session["RoleId"]) == 7)
                {
                    var currentiicsessionjson = dc.InvestigationTaskDetails.AsEnumerable().Where(a => a.InvestigationTaskAssignmentID == id).ToList();
                    string strcurrentiicsessionjson_ = currentiicsessionjson[0].InvestigationTaskDataInJSON;


                    Session["ResultiicJson"] = strcurrentiicsessionjson_;
                    TempData["TASKGOUPID"] = tskgrp_id;
                    TempData["InvestigationFileID"] = task_assignment[0].InvestigationFileID;
                    TempData["InvFileID"] = task_assignment[0].InvestigationFileID;
                    TempData["active_url_View_Inv_Prg_iic"] = "iic";
                    string oldjsonFieldsProp = "fields";
                    string oldjsonValueProp = "value";
                    string oldjsonNameProp = "name";
                    string modifiedJsonold = string.Empty;
                    DynamicEventforms dy = new DynamicEventforms();

                    var InvInfo = dc.USP_VIEW_INVESTIGATION_INVESTIGATIONID(task_assignment[0].InvestigationFileID).FirstOrDefault();

                    TempData["PageHead"] = InvInfo.InvestigationFileNumber;

                    var invInfo = dc.USP_VIEW_INVESTIGATION_FORM35(task_assignment[0].InvestigationFileID).FirstOrDefault();

                    TempData["INV_STATUS"] = invInfo.LKInvestigationStatusTypeID;

                    string GlobalTaskGroupEventUniqueNumber = task_assignment[0].InvestigationTaskGroupEventUniqueNumber;
                    int globalspliteventuniquenumber = Convert.ToInt32(GlobalTaskGroupEventUniqueNumber.Split('E')[1]);
                    var tskgrpid = tskgrp_id;
                    var result_json = dc.USP_GET_TASKDETAILS_JSON(task_assignment[0].InvestigationFileID, tskgrp_id /*task_assignment[0].InvestigationTaskGroupID*/).ToList();
                    // var tskgrpid = task_assignment[0].InvestigationTaskGroupID;
                    var TaskGroupName = (from taskgrp_name in dc.InvestigationTaskGroups
                                         where taskgrp_name.InvestigationTaskGroupID == tskgrpid
                                         select new
                                         {
                                             taskgrp_name.InvestigationTaskGroupName,
                                             taskgrp_name.InvestigationTaskGroupSortOrder,

                                         }).ToList();
                    TempData["TaskGroupName"] = TaskGroupName[0].InvestigationTaskGroupName;
                    var TaskGroupEventUniqueNumber = (from grpevents in dc.InvestigationTaskGroupEvents
                                                      where grpevents.InvestigationTaskGroupID == tskgrpid
                                                      select new
                                                      {
                                                          grpevents.InvestigationTaskGroupEventUniqueNumber,
                                                          grpevents.InvestigationTaskGroupEventName,
                                                          grpevents.InvestigationTaskGroupEventDescription
                                                      }).ToList();

                    int varcnt = 1;
                    ViewBag.TaskGroupEventUniqueNumberCount = TaskGroupEventUniqueNumber.Count();

                    for (int i = 0; i < TaskGroupEventUniqueNumber.Count(); i++)
                    {
                        var EventUniqueNumber = TaskGroupEventUniqueNumber[i].InvestigationTaskGroupEventUniqueNumber;
                        string eventheader = TaskGroupEventUniqueNumber[i].InvestigationTaskGroupEventName + ":" + TaskGroupEventUniqueNumber[i].InvestigationTaskGroupEventDescription;
                        var FileID = task_assignment[0].InvestigationFileID;
                        int spliteventuniquenumber = Convert.ToInt32(EventUniqueNumber.Split('E')[1]);
                        var globalcheck_for_iic = (from invtaskassignment in dc.InvestigationTaskAssignments

                                                   where invtaskassignment.InvestigationTaskGroupEventUniqueNumber == EventUniqueNumber && invtaskassignment.InvestigationFileID == FileID

                                                   select new
                                                   {
                                                       invtaskassignment.InvestigationFileID,
                                                       invtaskassignment.InvestigationTaskAssignmentID,
                                                       invtaskassignment.InvestigationTaskGroupEventID
                                                   }).ToList();


                        //if (spliteventuniquenumber <= globalspliteventuniquenumber)
                        //{

                        if (globalcheck_for_iic.Count >= 1)
                        {

                            var taskdetailjson = (from invtaskdetail in dc.InvestigationTaskDetails

                                                  where invtaskdetail.InvestigationTaskGroupEventUniqueNumber == EventUniqueNumber && invtaskdetail.InvestigationFileID == FileID


                                                  select new
                                                  {
                                                      invtaskdetail.InvestigationTaskDataInJSON,
                                                      invtaskdetail.ProgressInPercentage,
                                                      invtaskdetail.InvestigationTaskGroupEventID,
                                                      invtaskdetail.InvestigationTaskStatus,
                                                      invtaskdetail.InvestigationTaskGroupEventUniqueNumber,
                                                      invtaskdetail.EventSubmitStatus
                                                  }).ToList();


                            int taskevent_id = 0;

                            if (taskdetailjson.Count() > 0)
                            {
                                taskevent_id = taskdetailjson[0].InvestigationTaskGroupEventID;
                            }

                            var pvttaskassignmentid = (from invtaskdetail in dc.InvestigationTaskAssignments

                                                       where invtaskdetail.InvestigationFileID == FileID && invtaskdetail.InvestigationTaskGroupEventID == taskevent_id
                                                       select new
                                                       {
                                                           invtaskdetail.InvestigationTaskAssignmentID
                                                       }).ToList();


                            if (taskdetailjson.Count() > 0)
                            {
                                var invtaskgrpid = taskdetailjson[0].InvestigationTaskGroupEventID;
                                var assigned = (from tskassign in dc.InvestigationTaskAssignments

                                                join multitaskassign in dc.InvestigationMultipleTaskAssignments on tskassign.InvestigationTaskAssignmentID equals multitaskassign.InvestigationTaskAssignmentID

                                                where tskassign.InvestigationTaskGroupEventID == invtaskgrpid && tskassign.InvestigationFileID == FileID

                                                select new
                                                {
                                                    multitaskassign.TeamMemberID,
                                                    tskassign.ExpectedDateOfCompletion,
                                                    tskassign.InvestigationTaskAssignmentID
                                                }).ToList().FirstOrDefault();
                                var multi_assigned = (from tskassign in dc.InvestigationTaskAssignments

                                                      join multitaskassign in dc.InvestigationMultipleTaskAssignments on tskassign.InvestigationTaskAssignmentID equals multitaskassign.InvestigationTaskAssignmentID

                                                      where tskassign.InvestigationTaskGroupEventID == invtaskgrpid && tskassign.InvestigationFileID == FileID

                                                      select new
                                                      {
                                                          multitaskassign.TeamMemberID,
                                                          tskassign.ExpectedDateOfCompletion,
                                                          tskassign.InvestigationTaskAssignmentID
                                                      }).ToList();
                                var assignedname = assigned.TeamMemberID;

                                //int assignedname = 0;
                                //for (int m = 0; m < assigned.Count(); m++)
                                //{
                                //    if (assigned[m].TeamMemberID == Convert.ToInt32(Session["UserId"]))
                                //    {
                                //        assignedname = assigned[m].TeamMemberID;
                                //        break;

                                //    }
                                //}

                                for (int n = 0; n < multi_assigned.Count(); n++)

                                {
                                    int multi_tm_id = multi_assigned[n].TeamMemberID;

                                    var assigned_name_multi = (from user in dc.Users

                                                               where user.UserID == multi_tm_id

                                                               select new
                                                               {
                                                                   user.FirstName,
                                                                   user.MiddleName,
                                                                   user.LastName


                                                               }).ToList();


                                    assigned_mult_teammember = assigned_mult_teammember + "," + " " + assigned_name_multi[0].FirstName + " " + assigned_name_multi[0].MiddleName + " " + assigned_name_multi[0].LastName;

                                }
                                assigned_mult_teammember = assigned_mult_teammember.Remove(0, 2);
                                var assigned_name = (from user in dc.Users

                                                     where user.UserID == assignedname

                                                     select new
                                                     {
                                                         user.FirstName,

                                                     }).ToList();


                                var tm_member = (from tskassign in dc.InvestigationTaskAssignments
                                                 join invtaskdetail in dc.InvestigationTaskDetails on tskassign.InvestigationFileID equals invtaskdetail.InvestigationFileID
                                                 join multitask in dc.InvestigationMultipleTaskAssignments on tskassign.InvestigationTaskAssignmentID equals multitask.InvestigationTaskAssignmentID
                                                 where tskassign.InvestigationTaskGroupEventID == invtaskgrpid

                                                 select new
                                                 {
                                                     multitask.TeamMemberID
                                                 }).Distinct().ToList().FirstOrDefault();

                                //int tm_mbr = 0;
                                //for (int count = 0; count < tm_member.Count(); count++)
                                //{
                                //    if (assigned[count].TeamMemberID == Convert.ToInt32(Session["UserId"]))
                                //    {
                                //        tm_mbr = assigned[count].TeamMemberID;
                                //        break;

                                //    }
                                //}

                                string newjson = taskdetailjson[0].InvestigationTaskDataInJSON;
                                var old_json = dc.USP_GET_EXISTINGJSON(taskdetailjson[0].InvestigationTaskGroupEventID).ToList();
                                string oldjson = old_json[0].InvestigationTaskGroupEventJSON;
                                if (newjson != null)
                                {
                                    JObject parsedJsonold = JObject.Parse(oldjson);
                                    JObject parsedJsonnew = JObject.Parse(newjson);

                                    if (parsedJsonold[oldjsonFieldsProp] != null)
                                    {
                                        string namePropValue = string.Empty;

                                        JArray jsonold_Array = (JArray)parsedJsonold[oldjsonFieldsProp];
                                        for (int index = 0; index < jsonold_Array.Count; index++)
                                        {
                                            if (jsonold_Array[index][oldjsonNameProp] != null)
                                            {
                                                namePropValue = jsonold_Array[index][oldjsonNameProp].ToString();

                                                modifiedJsonold = dy.ModifyJsonAPropValueWithJsonBPropValue(oldjsonFieldsProp, oldjsonValueProp, oldjsonNameProp, namePropValue, parsedJsonold, parsedJsonnew, index);
                                            }
                                        }

                                    }
                                    investigation_fileid = task_assignment[0].InvestigationFileID;
                                    int teammemberid = tm_member.TeamMemberID;
                                    // ViewBag.html1 = dy.test2(modifiedJsonold, task_assignment[0].InvestigationFileID, id);
                                    TempData["JSON-" + varcnt] = dynamic.Event_Generation_iic(modifiedJsonold, task_assignment[0].InvestigationFileID, assigned.InvestigationTaskAssignmentID, teammemberid, id, taskdetailjson[0].ProgressInPercentage, pvttaskassignmentid[0].InvestigationTaskAssignmentID, taskdetailjson[0].InvestigationTaskStatus, Convert.ToInt32(taskdetailjson[0].InvestigationTaskGroupEventUniqueNumber.Split('E')[1]), tskgrpid, Convert.ToInt32(TempData["INV_STATUS"]), Convert.ToInt32(taskdetailjson[0].EventSubmitStatus));
                                    //  TempData["JSONHEADNAME" + varcnt] = assigned_name[0].FirstName;
                                    TempData["JSONHEADNAME" + varcnt] = assigned_mult_teammember;
                                    assigned_mult_teammember = string.Empty;
                                    TempData["JSONHEADDATE" + varcnt] = (!string.IsNullOrEmpty(Convert.ToString(assigned.ExpectedDateOfCompletion)) ? Convert.ToDateTime(assigned.ExpectedDateOfCompletion).ToString("dd/MM/yyyy") : "N/A");
                                    ViewData["E/DFlag-" + varcnt] = "e";
                                    TempData["BackPage"] = "task_group";
                                    ViewData["EHeader-" + varcnt] = eventheader;
                                    TempData["invstatus" + varcnt] = taskdetailjson[0].InvestigationTaskStatus;
                                    TempData["JSONHEADPROGRESS" + varcnt] = taskdetailjson[0].ProgressInPercentage;
                                    if (assigned.InvestigationTaskAssignmentID == id)
                                    {
                                        TempData["selectt_" + varcnt] = id;
                                    }
                                    else
                                    {
                                        TempData["selectt_" + varcnt] = 0;
                                    }
                                }

                                else
                                {

                                    int investigation_fileid = task_assignment[0].InvestigationFileID;
                                    int teammemberid = tm_member.TeamMemberID;
                                    TempData["JSON-" + varcnt] = dynamic.Event_Generation_iic(oldjson, investigation_fileid, assigned.InvestigationTaskAssignmentID, teammemberid, id, taskdetailjson[0].ProgressInPercentage, pvttaskassignmentid[0].InvestigationTaskAssignmentID, taskdetailjson[0].InvestigationTaskStatus, Convert.ToInt32(taskdetailjson[0].InvestigationTaskGroupEventUniqueNumber.Split('E')[1]), tskgrpid, Convert.ToInt32(TempData["INV_STATUS"]), Convert.ToInt32(taskdetailjson[0].EventSubmitStatus));
                                    TempData["PageHead"] = InvInfo.InvestigationFileNumber; ;
                                    TempData["BackPage"] = "task_group";
                                    ViewData["E/DFlag-" + varcnt] = "e";
                                    TempData["lf_menu"] = "1";
                                    // TempData["JSONHEADNAME" + varcnt] = assigned_name[0].FirstName;
                                    TempData["JSONHEADNAME" + varcnt] = assigned_mult_teammember;
                                    assigned_mult_teammember = string.Empty;

                                    TempData["JSONHEADDATE" + varcnt] = (!string.IsNullOrEmpty(Convert.ToString(assigned.ExpectedDateOfCompletion)) ? Convert.ToDateTime(assigned.ExpectedDateOfCompletion).ToString("dd/MM/yyyy") : "N/A");
                                    TempData["JSONHEADPROGRESS" + varcnt] = taskdetailjson[0].ProgressInPercentage;
                                    TempData["invstatus" + varcnt] = taskdetailjson[0].InvestigationTaskStatus;
                                    ViewData["EHeader-" + varcnt] = eventheader;
                                    if (assigned.InvestigationTaskAssignmentID == id)
                                    {
                                        TempData["selectt_" + varcnt] = id;
                                    }
                                    else
                                    {
                                        TempData["selectt_" + varcnt] = 0;
                                    }

                                }

                            }

                            else
                            {
                                TempData["JSON-" + varcnt] = "";
                                ViewData["E/DFlag-" + varcnt] = "d";
                                TempData["PageHead"] = InvInfo.InvestigationFileNumber;
                                TempData["BackPage"] = "task_group";
                                TempData["lf_menu"] = "1";
                                TempData["JSONHEADNAME" + varcnt] = "Unassigned";
                                TempData["JSONHEADDATE" + varcnt] = "N/A";
                                TempData["JSONHEADPROGRESS" + varcnt] = 0;
                                ViewData["EHeader-" + varcnt] = eventheader;
                                TempData["selectt_" + varcnt] = 0;

                            }
                        }
                        else
                        {
                            TempData["JSON-" + varcnt] = "";
                            ViewData["E/DFlag-" + varcnt] = "d";
                            TempData["PageHead"] = InvInfo.InvestigationFileNumber;
                            TempData["BackPage"] = "task_group";
                            TempData["lf_menu"] = "1";
                            TempData["JSONHEADNAME" + varcnt] = "Unassigned";
                            TempData["JSONHEADDATE" + varcnt] = "N/A";
                            TempData["JSONHEADPROGRESS" + varcnt] = 0;
                            ViewData["EHeader-" + varcnt] = eventheader;
                            TempData["selectt_" + varcnt] = 0;

                        }
                        varcnt++;


                    }
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("TASKController", "InvestigationDetails(int id = 0)", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }

            return View(task_assignment);
        }
        /// <summary>
        /// Save the attached files by IIC
        /// </summary> 
        /// 
        /// <param name="form">Receive form collection object</param>
        /// <param name="id">Receive investigation task group id </param>
        /// <param name="tskgrp_id">Receive investigation task group id </param>
        /// <param name="invfile_id">Receive investigation file id </param>
        /// <param name="command">Receive the task manipulation command whether it is complete or not</param>
        public ActionResult SaveFiles_iic(FormCollection form, int id, int tskgrp_id, int invfile_id, string command)
        {
            try
            {

                id = Convert.ToInt32(form["TeamAssignID"]);
                var iicfilesavesessionjson = dc.InvestigationTaskDetails.AsEnumerable().Where(a => a.InvestigationTaskAssignmentID == id).ToList();
                string striicfilesaveessionjson_ = iicfilesavesessionjson[0].InvestigationTaskDataInJSON;
                Session["latestiicJson"] = striicfilesaveessionjson_;
                dynamic tskresult = 0;
                string file_upl = string.Empty;
                int j = 0;
                Dictionary<string, List<string>> deleted_jsonnamevalue = new Dictionary<string, List<string>>();
                if (Convert.ToString(Session["ResultiicJson"]) == Convert.ToString(Session["latestiicJson"]))
                {

                    for (int del_cnt = 0; del_cnt < form.Count; del_cnt++)
                    {
                        if (form.Keys[del_cnt].Contains("deleted-"))
                        {
                            string deleted_key = form.Keys[del_cnt];
                            string deleted_key_dictonary = form.Keys[del_cnt].Split('-')[1];
                            string values = form[deleted_key];
                            List<string> list = new List<string>();

                            string[] deletedvalues = values.Split(',');
                            for (int i = 0; i < deletedvalues.Length; i++)
                            {
                                list.Add(deletedvalues[i]);
                            }
                            deleted_jsonnamevalue.Add(deleted_key_dictonary, list);
                        }
                    }

                    string sl = form["TeamAssignID"];
                    int taskid = Convert.ToInt32(sl);
                    string exist_json = string.Empty;
                    var task_assignment_json = dc.USP_VIEW_TASK_ASSIGNMENT_FOR_TEAMMEMBER(taskid).ToList();

                    int invfileid = task_assignment_json[0].InvestigationFileID;

                    var taskdetails_json = dc.USP_GET_TASKDETAILS_JSON_BY_EVENT(task_assignment_json[0].InvestigationFileID, task_assignment_json[0].InvestigationTaskGroupEventID).ToList();

                    string taskdetailsjsonstr = taskdetails_json[0].InvestigationTaskDataInJSON;

                    foreach (var jsonitem in task_assignment_json)
                    {
                        exist_json = jsonitem.InvestigationTaskGroupEventJSON;
                    }
                    dynamic formDetails = JObject.Parse(exist_json);
                    var fields = formDetails.fields;
                    Dictionary<string, List<string>> jsonnamevalue = new Dictionary<string, List<string>>();
                    int txtcnt = 0;
                    int txtboxcnt = 0;
                    for (var i = 0; i < fields.Count; i++)

                    {
                        List<string> list_json = new List<string>();

                        if (fields[i].field_type == "textarea")
                        {

                            txtcnt++;
                            file_upl = fields[i].name;
                            string val = form[file_upl];
                            if (val != "")
                            {
                                j++;
                            }
                            list_json.Add(val);
                            jsonnamevalue.Add(file_upl, list_json);



                        }

                        if (fields[i].field_type == "checkbox")
                        {
                            //  int checkboxcnt = 0;
                            string field_name = fields[i].name;
                            string val = form[field_name];
                            if (val != null)
                            {

                                list_json.Add("1");
                            }
                            else
                            {
                                list_json.Add("0");
                            }
                            jsonnamevalue.Add(field_name, list_json);

                        }
                        if (fields[i].field_type == "text")
                        {
                            txtboxcnt++;
                            file_upl = fields[i].name;
                            string val = form[file_upl];
                            if (val != "")
                            {
                                j++;
                            }

                            list_json.Add(val);
                            jsonnamevalue.Add(file_upl, list_json);

                        }
                        if (fields[i].field_type == "text-tbl")
                        {
                            txtboxcnt++;
                            file_upl = fields[i].name;
                            string val = form[file_upl];
                            if (val != "")
                            {
                                j++;
                            }

                            list_json.Add(val);
                            jsonnamevalue.Add(file_upl, list_json);

                        }
                        if (fields[i].field_type == "datepicker")
                        {
                            txtboxcnt++;
                            file_upl = fields[i].name;
                            string val = form[file_upl];
                            if (val != "")
                            {
                                j++;
                            }

                            list_json.Add(val);
                            jsonnamevalue.Add(file_upl, list_json);

                        }
                        if (fields[i].field_type == "file")
                        {


                            //  comm.CreateIfMissing(Server.MapPath("~/") + "AttachmentEventFiles");
                            int count = 0;
                            int s = 0;

                            for (int l = 0; l < Request.Files.Count; l++)
                            {
                                if (fields[i].name == Request.Files.AllKeys[l])
                                {

                                    count++;
                                    s = l;

                                }

                            }
                            int intial_index = (s - count) + 1;
                            // int bm = 0;
                            for (int intial = intial_index; intial <= s; intial++)
                            {
                                HttpPostedFileBase file = Request.Files[intial];

                                int fileSize = file.ContentLength;

                                if (file.ContentLength > 0)
                                {
                                    string sm = Request.Files.AllKeys[intial];

                                    //string fileName = file.FileName;

                                    string fileName = file.FileName.Substring(0, file.FileName.LastIndexOf('.')) + "_" + DateTime.Now.ToFileTime() + file.FileName.Substring(file.FileName.LastIndexOf('.'), ((file.FileName.Length) - (file.FileName.LastIndexOf('.'))));

                                    string mimeType = file.ContentType.Split('/')[1].ToString();

                                    int DocumentTypeId = Convert.ToInt32(dc.LKDocumentTypes.AsQueryable().Where(d => d.LKDocumentTypeName == mimeType).Select(d => d.LKDocumentTypeID).FirstOrDefault());

                                    Stream fileContent = file.InputStream;

                                    //file.SaveAs(Server.MapPath("~/") + "AttachmentEventFiles\\" + fileName);

                                    string doc_llibrary = "Event_Documents";
                                    //string[] SharepointfilePath = comm.SharePointEventDocs(fileContent, Server.MapPath("~/") + "AttachmentEventFiles\\" + fileName, Convert.ToString(investigation_fileid), Convert.ToString(investigation_fileid), task_assignment_json[0].InvestigationTaskGroupEventUniqueNumber, doc_llibrary);
                                    string[] SharepointfilePath = comm.savetoSharePointEventDocsOnPrem(fileContent, fileName, Convert.ToString(invfileid), Convert.ToString(invfileid), task_assignment_json[0].InvestigationTaskGroupEventUniqueNumber, doc_llibrary);
                                    ObjectParameter InvestigationDocumentAsAttachment_ID_ = new ObjectParameter("InvestigationDocumentAsAttachment_ID_out", typeof(int));
                                    var result1 = dc.USP_INST_EVENT_INVESTIGATION_DOCUMENTASATTACHMENT(0, invfileid, 2, DocumentTypeId, SharepointfilePath[1], InvestigationDocumentAsAttachment_ID_);
                                    string sk = InvestigationDocumentAsAttachment_ID_.Value.ToString();
                                    file_upl = fields[i].name;

                                    list_json.Add(sk);

                                    dc.SaveChanges();
                                }
                                else
                                {
                                    file_upl = fields[i].name;
                                    list_json.Add("");

                                }
                            }

                            jsonnamevalue.Add(file_upl, list_json);
                        }
                        // if (Directory.Exists("AttachmentEventFiles"))

                        //   comm.DeleteDirectory(Server.MapPath("~/") + "AttachmentEventFiles");

                    }

                    if (taskdetailsjsonstr != null)
                    {
                        JObject taskformDetails = JObject.Parse(taskdetailsjsonstr);
                        int count = taskformDetails.Count;
                        var taskjsonvalues = JsonConvert.DeserializeObject<Dictionary<string, List<string>>>(taskdetailsjsonstr);
                        Dictionary<string, List<string>> deserialjson = new Dictionary<string, List<string>>();
                        deserialjson = taskjsonvalues;
                        foreach (KeyValuePair<string, List<string>> items in jsonnamevalue)
                        {
                            string key = items.Key;
                            List<string> values = items.Value;
                            if ((deserialjson.ContainsKey(key)))
                            {
                                for (int i = 0; i < values.Count(); i++)
                                {
                                    if (key.Contains("text") || key.Contains("checkbox"))
                                    {
                                        deserialjson[key].Remove(deserialjson[key][i]);
                                        deserialjson[key].Add(values[i]);

                                    }
                                    else
                                    {
                                        if (values[i] != "")
                                        {
                                            for (int m = 0; m < deserialjson[key].Count; m++)
                                            {
                                                if (deserialjson[key][m] == "")
                                                {
                                                    deserialjson[key].Remove(deserialjson[key][m]);
                                                }
                                            }
                                            deserialjson[key].Add(values[i]);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                deserialjson.Add(key, values);
                            }
                        }

                        //delete code writes here
                        foreach (KeyValuePair<string, List<string>> deleteditems in deleted_jsonnamevalue)
                        {
                            string key = deleteditems.Key;
                            List<string> values = deleteditems.Value;
                            int count_del_json = deserialjson[key].Count;
                            if ((deserialjson.ContainsKey(key)))
                            {
                                for (int i = 0; i < values.Count(); i++)
                                {

                                    for (int m = 0; m < deserialjson[key].Count; m++)
                                    {
                                        if (deserialjson[key][m] == deleteditems.Value[i].Trim())
                                        {

                                            dc.USP_INST_DocumentAttachmentHistory(task_assignment_json[0].InvestigationFileID, Convert.ToInt32(deserialjson[key][m]),/* task_assignment_json[0].TeamMemberID*/Convert.ToInt32(Session["UserId"]));
                                            deserialjson[key].Remove(deserialjson[key][m]);

                                            break;
                                        }
                                    }
                                }
                                if (deserialjson[key].Count == 0)
                                {
                                    deserialjson[key].Add("");
                                }
                            }

                        }

                        string dynjson = JsonConvert.SerializeObject(deserialjson);
                        decimal progcount = 0;
                        decimal totalcount = deserialjson.Count();
                        foreach (KeyValuePair<string, List<string>> progresscount in deserialjson)
                        {
                            string key = progresscount.Key;
                            List<string> values = progresscount.Value;
                            for (int i = 0; i < values.Count; i++)
                            {
                                if (values[0] != "")
                                {
                                    progcount++;
                                    break;

                                }
                            }

                        }

                        decimal convert = (progcount / totalcount) * 100;
                        int progress_in_percentage = Convert.ToInt32(convert);

                        var event_status_flag = (from tskdet in dc.InvestigationTaskDetails

                                                 where tskdet.InvestigationTaskAssignmentID == id && tskdet.InvestigationFileID == investigation_fileid

                                                 select new
                                                 {
                                                     tskdet.InvestigationTaskStatus

                                                 }).FirstOrDefault();
                        if (event_status_flag.InvestigationTaskStatus == 1)
                        {
                            progress_in_percentage = 100;
                        }



                        foreach (string tskcmpltkey in jsonnamevalue.Keys)
                        {
                            if (tskcmpltkey.Contains(".checkbox"))
                            {
                                if (deserialjson[tskcmpltkey][0] == "1")
                                {
                                    ViewBag.task_cmplete_validation_flag = 0;
                                    break;

                                }
                                else
                                {
                                    ViewBag.task_cmplete_validation_flag = 1;
                                }

                            }

                            else if (deserialjson[tskcmpltkey][0] != "")
                            {
                                ViewBag.task_cmplete_validation_flag = 0;
                                break;

                            }
                            else
                            {
                                ViewBag.task_cmplete_validation_flag = 1;
                            }

                        }

                        if (ViewBag.task_cmplete_validation_flag == 0)
                        {


                            var result = dc.USP_UPDT_TASKDETAILS_WITH_JSON_BY_IIC(task_assignment_json[0].InvestigationTaskGroupEventID, Convert.ToInt32(task_assignment_json[0].InvestigationFileID), progress_in_percentage, dynjson, Convert.ToInt32(Session["UserId"]));
                            var inst_result = dc.USP_INST_TASKDETAILSHISTORY_WITH_JSON(Convert.ToInt32(task_assignment_json[0].InvestigationTaskGroupEventID), Convert.ToInt32(task_assignment_json[0].InvestigationFileID), progress_in_percentage, dynjson, 0, Convert.ToInt32(Session["UserId"]));

                            if (command == "complete")
                            {

                                int investigation_fileid = Convert.ToInt32(invfile_id); int retval = 0;
                                int taskassignmentid = Convert.ToInt32(id);

                                var TaskGroupEventID = (from tskassign in dc.InvestigationTaskAssignments

                                                        where tskassign.InvestigationTaskAssignmentID == taskassignmentid && tskassign.InvestigationFileID == investigation_fileid

                                                        select new
                                                        {
                                                            tskassign.InvestigationTaskGroupEventID,

                                                            tskassign.InvestigationTaskGroupEventUniqueNumber

                                                        }).ToList();

                                //int group_event_id = TaskGroupEventID[0].InvestigationTaskGroupEventID; 

                                string group_event_UniqueNo = TaskGroupEventID[0].InvestigationTaskGroupEventUniqueNumber;

                                var predecessorid = (from prdid in dc.InvestigationTaskGroupEvents

                                                     where prdid.InvestigationTaskGroupPreEventUniqueNumber == group_event_UniqueNo

                                                     select new
                                                     {
                                                         prdid.InvestigationTaskGroupEventUniqueNumber

                                                     }).ToList();

                                if (predecessorid.Count > 0)
                                {
                                    string event_unique_number = predecessorid[0].InvestigationTaskGroupEventUniqueNumber;

                                    var searchsuccessor = (from tskdet in dc.InvestigationTaskDetails

                                                           where tskdet.InvestigationTaskGroupEventUniqueNumber == event_unique_number && tskdet.InvestigationFileID == investigation_fileid

                                                           select new
                                                           {
                                                               tskdet.InvestigationTaskGroupEventUniqueNumber,
                                                               tskdet.InvestigationTaskGroupEventID,

                                                           }).ToList();

                                    if (searchsuccessor.Count > 0)
                                    {

                                        retval = dc.USP_UPDT_TASKSTATUS_IN_TASKDETAIL_FOR_COMPLETE(investigation_fileid, searchsuccessor[0].InvestigationTaskGroupEventID);
                                    }
                                }

                                // Update Investigation Progress Percentage In Scope Definition Table

                                var InvEventList = dc.InvestigationTaskGroupEvents.Where(a => a.InvestigationTaskGroupEventUniqueNumber == group_event_UniqueNo).ToList();

                                var CheckTaskStatus = dc.InvestigationTaskDetails.AsQueryable().Where(a => a.InvestigationTaskAssignmentID == id

                                                          && a.InvestigationTaskStatus == 0).FirstOrDefault();

                                if (CheckTaskStatus != null)
                                {
                                    foreach (var itm in InvEventList)
                                    {
                                        retval = dc.USP_UPDT_INVESTIGATION_SCOPEDEFINITION_PROGRESS(investigation_fileid, itm.InvestigationTaskGroupID);
                                    }
                                }
                                retval = dc.USP_UPDT_TASKSTATUS_IN_TASKDETAIL(investigation_fileid, TaskGroupEventID[0].InvestigationTaskGroupEventID);


                                int EventID = TaskGroupEventID[0].InvestigationTaskGroupEventID;

                                tskresult = (from tskassign in dc.InvestigationTaskDetails

                                             where tskassign.InvestigationTaskGroupEventID == EventID && tskassign.InvestigationFileID == investigation_fileid

                                             select new
                                             {
                                                 tskassign.ProgressInPercentage,

                                             }).ToList();
                            }

                            return RedirectToAction("IICeventview", "task", new { @id = id, @tskgrp_id = tskgrp_id });
                        }
                        else
                        {
                            return RedirectToAction("IICeventview", "task", new { @id = id, @tskgrp_id = tskgrp_id, @task_validt_status = ViewBag.task_cmplete_validation_flag });
                        }

                    }

                    else
                    {

                        string dynjson = JsonConvert.SerializeObject(jsonnamevalue);

                        decimal prog_count_else = 0;
                        decimal totalcount_else = jsonnamevalue.Count();
                        foreach (KeyValuePair<string, List<string>> progresscount in jsonnamevalue)
                        {
                            string key = progresscount.Key;
                            List<string> values = progresscount.Value;
                            for (int i = 0; i < values.Count; i++)
                            {
                                if (values[0] != "")
                                {
                                    prog_count_else++;
                                    break;
                                }
                            }

                        }
                        decimal convert_else = (prog_count_else / totalcount_else) * 100;
                        int progress_in_percentage_else = Convert.ToInt32(convert_else);

                        var event_status_flag = (from tskdet in dc.InvestigationTaskDetails

                                                 where tskdet.InvestigationTaskAssignmentID == id && tskdet.InvestigationFileID == invfile_id

                                                 select new
                                                 {
                                                     tskdet.InvestigationTaskStatus

                                                 }).FirstOrDefault();

                        if (event_status_flag.InvestigationTaskStatus == 1)
                        {
                            progress_in_percentage_else = 100;
                        }


                        foreach (string tskcmpltkey in jsonnamevalue.Keys)
                        {

                            if (tskcmpltkey.Contains(".checkbox"))
                            {
                                if (jsonnamevalue[tskcmpltkey][0] == "1")
                                {
                                    ViewBag.task_cmplete_validation_flag = 0;
                                    break;

                                }
                                else
                                {
                                    ViewBag.task_cmplete_validation_flag = 1;
                                }

                            }


                            else if (jsonnamevalue[tskcmpltkey][0] != "")
                            {
                                ViewBag.task_cmplete_validation_flag = 0;
                                break;

                            }
                            else
                            {
                                ViewBag.task_cmplete_validation_flag = 1;
                            }

                        }

                        if (ViewBag.task_cmplete_validation_flag == 0)
                        {
                            var result = dc.USP_UPDT_TASKDETAILS_WITH_JSON_BY_IIC(task_assignment_json[0].InvestigationTaskGroupEventID, Convert.ToInt32(task_assignment_json[0].InvestigationFileID), progress_in_percentage_else, dynjson, Convert.ToInt32(Session["UserId"]));
                            var inst_result = dc.USP_INST_TASKDETAILSHISTORY_WITH_JSON(Convert.ToInt32(task_assignment_json[0].InvestigationTaskGroupEventID), Convert.ToInt32(task_assignment_json[0].InvestigationFileID), progress_in_percentage_else, dynjson, 0, Convert.ToInt32(Session["UserId"]));

                            if (command == "complete")
                            {
                                int investigation_fileid = Convert.ToInt32(invfile_id); int retval = 0;
                                int taskassignmentid = Convert.ToInt32(id);

                                var TaskGroupEventID = (from tskassign in dc.InvestigationTaskAssignments

                                                        where tskassign.InvestigationTaskAssignmentID == taskassignmentid && tskassign.InvestigationFileID == investigation_fileid

                                                        select new
                                                        {
                                                            tskassign.InvestigationTaskGroupEventID,

                                                            tskassign.InvestigationTaskGroupEventUniqueNumber

                                                        }).ToList();

                                //int group_event_id = TaskGroupEventID[0].InvestigationTaskGroupEventID; 

                                string group_event_UniqueNo = TaskGroupEventID[0].InvestigationTaskGroupEventUniqueNumber;

                                var predecessorid = (from prdid in dc.InvestigationTaskGroupEvents

                                                     where prdid.InvestigationTaskGroupPreEventUniqueNumber == group_event_UniqueNo

                                                     select new
                                                     {
                                                         prdid.InvestigationTaskGroupEventUniqueNumber

                                                     }).ToList();

                                if (predecessorid.Count > 0)
                                {
                                    string event_unique_number = predecessorid[0].InvestigationTaskGroupEventUniqueNumber;

                                    var searchsuccessor = (from tskdet in dc.InvestigationTaskDetails

                                                           where tskdet.InvestigationTaskGroupEventUniqueNumber == event_unique_number && tskdet.InvestigationFileID == investigation_fileid

                                                           select new
                                                           {
                                                               tskdet.InvestigationTaskGroupEventUniqueNumber,
                                                               tskdet.InvestigationTaskGroupEventID,

                                                           }).ToList();

                                    if (searchsuccessor.Count > 0)
                                    {
                                        retval = dc.USP_UPDT_TASKSTATUS_IN_TASKDETAIL_FOR_COMPLETE(investigation_fileid, searchsuccessor[0].InvestigationTaskGroupEventID);
                                    }
                                }

                                // Update Investigation Progress Percentage In Scope Definition Table

                                var InvEventList = dc.InvestigationTaskGroupEvents.Where(a => a.InvestigationTaskGroupEventUniqueNumber == group_event_UniqueNo).ToList();
                                var CheckTaskStatus = dc.InvestigationTaskDetails.AsEnumerable().Where(a => a.InvestigationTaskAssignmentID == id

                                                         && a.InvestigationTaskStatus == 0).FirstOrDefault();

                                if (CheckTaskStatus != null)
                                {
                                    foreach (var itm in InvEventList)
                                    {
                                        retval = dc.USP_UPDT_INVESTIGATION_SCOPEDEFINITION_PROGRESS(investigation_fileid, itm.InvestigationTaskGroupID);
                                    }
                                }


                                retval = dc.USP_UPDT_TASKSTATUS_IN_TASKDETAIL(investigation_fileid, TaskGroupEventID[0].InvestigationTaskGroupEventID);

                                int EventID = TaskGroupEventID[0].InvestigationTaskGroupEventID;

                                tskresult = (from tskassign in dc.InvestigationTaskDetails

                                             where tskassign.InvestigationTaskGroupEventID == EventID && tskassign.InvestigationFileID == investigation_fileid

                                             select new
                                             {
                                                 tskassign.ProgressInPercentage,

                                             }).ToList();
                            }
                            return RedirectToAction("IICeventview", "task", new { @id = id, @tskgrp_id = tskgrp_id });
                        }
                        else
                        {
                            return RedirectToAction("IICeventview", "task", new { @id = id, @tskgrp_id = tskgrp_id, task_validt_status = ViewBag.task_cmplete_validation_flag });
                        }



                    }
                }

                else
                {

                    ViewBag.task_cmplete_validation_flag = 2;
                    return RedirectToAction("IICeventview", "task", new { @id = id, @tskgrp_id = tskgrp_id, task_validt_status = ViewBag.task_cmplete_validation_flag });
                }
            }

            catch (System.Exception ex)
            {
                comm.Exception_Log("TASKController", "SaveFiles_iic(FormCollection form, int id)", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }
            // return RedirectToAction("IICeventview", "task", new { @id = id, @tskgrp_id = tskgrp_id });
        }
        /// <summary>
        /// view Evidence management deatils by investigation file id 
        /// </summary> 
        /// <param name="page">Receive Evidence Management page no</param>
        /// <param name="InvId">Receive investigation file id </param>
        public ActionResult EvidenceManagement(int? page, int InvId = 0)
        {
            var evidence = new InvEvidence();

            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 3 || Convert.ToInt32(Session["RoleId"]) == 6 || Convert.ToInt32(Session["RoleId"]) == 7)
                {
                    var CheckInvPermission = dc.InvestigationIICAssignments.AsEnumerable().Where(a => a.InvestigationFileID == InvId).FirstOrDefault();

                    if ((CheckInvPermission.IICAssigned == Convert.ToInt32(Session["UserId"]) &&

                                           CheckInvPermission.InvestigationFileID == InvId) || Convert.ToInt32(Session["RoleId"]) == 3 || Convert.ToInt32(Session["RoleId"]) == 6 || Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 7)
                    {
                        TempData["BackPage"] = "yes";
                        TempData["active_url_EvidenceManagement"] = "tm";
                        TempData["active_url_EvidenceManagement_iic"] = "iic";
                        TempData["lf_menu"] = "1";

                        var InvInfo = dc.USP_VIEW_INVESTIGATION_INVESTIGATIONID(InvId).FirstOrDefault();

                        if (page == null)
                        {
                            ViewBag.serial_nbr = 0;
                        }
                        else
                        {
                            ViewBag.serial_nbr = 10 * (page - 1);
                        }

                        evidence = new InvEvidence()
                        {
                            InvEveden = dc.USP_VIEW_INVESTIGATION_EVIDENCE_DETAILS(InvId, 0, "", "").ToList()
                        };

                        ViewBag.page_no = page;

                        ViewBag.count_pagecontent = evidence.InvEveden.Count();

                        evidence.InvestigationFileID = InvInfo.InvestigationFileID;

                        TempData["PageHead"] = InvInfo.InvestigationFileNumber;

                        Session["InvFileID"] = InvInfo.InvestigationFileID;

                        TempData["InvId"] = InvInfo.InvestigationFileID;

                        TempData["InvFileID"] = InvInfo.InvestigationFileID;

                        evidence.LocalDateAndTimeOfOccurrence = InvInfo.LocalDateAndTimeOfOccurrence.ToString();

                        evidence.UTCDateAndTimeOfOccurrence = InvInfo.UTCDateAndTimeOfOccurrence.ToString();

                        evidence.NotifierName = InvInfo.NotifierName;

                        evidence.NotifierOrganization = InvInfo.NotifierOrganization;

                        var Evidence_Type_id = dc.LKEvidenceTypes.ToList().OrderBy(x => x.LKEvidenceTypeID);

                        SelectList Evidence_Type = new SelectList(Evidence_Type_id, "LKEvidenceTypeID", "LKEvidenceTypeName");

                        ViewBag.Evidence_Type = Evidence_Type;


                        var Evid_CurrentlyWith = dc.LKCOMMONs.ToList().Where(a => a.LKCOMMONID == 20);  // get Evidence Currently width Type data from Common Lookup Table.
                        SelectList Evid_CurrentlyWith_Select = new SelectList(Evid_CurrentlyWith, "ID", "LKVALUE");
                        ViewBag.Evid_CurrentlyWith = Evid_CurrentlyWith_Select;
                        var invInfo = dc.USP_VIEW_INVESTIGATION_FORM35(InvId).FirstOrDefault();

                        TempData["INV_STATUS"] = invInfo.LKInvestigationStatusTypeID;

                        //Investtigation Check
                        List<InvestigationTeamMember> teammembers = dc.InvestigationTeamMembers.Where(x => x.InvestigationFileID == InvId).ToList();
                        foreach (var team in teammembers)
                        {
                            if (team.TeamMemberID == Convert.ToInt32(Session["UserId"]))
                            {
                                evidence.evidenceAssigned = team.TeamMemberID;
                            }
                        }
                    }
                    else
                    {
                        return RedirectToAction("Error");
                    }
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("TASKController", "EvidenceManagement(int? page, int InvId = 0)", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }
            return View(evidence);
        }
        /// <summary>
        /// Search the Evidence details 
        /// </summary> 
        /// <param name="InvEvdSearch">Receive Evidence Management page search key</param>
        /// <param name="page">Receive Evidence Managementpage page no</param>
        /// <param name="InvId">Receive investigation file id </param>
        [HttpPost]
        public ActionResult EvidenceManagement(InvEvidence InvEvdSearch, int? page, int InvId = 0)
        {
            var evidence = new InvEvidence();

            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 3 || Convert.ToInt32(Session["RoleId"]) == 6 || Convert.ToInt32(Session["RoleId"]) == 7)
                {
                    TempData["BackPage"] = "yes";
                    TempData["active_url_EvidenceManagement"] = "tm";
                    TempData["lf_menu"] = "1";

                    if (page == null)
                    {
                        ViewBag.serial_nbr = 0;
                    }
                    else
                    {
                        ViewBag.serial_nbr = 10 * (page - 1);
                    }

                    var InvInfo = dc.USP_VIEW_INVESTIGATION_INVESTIGATIONID(InvId).FirstOrDefault();

                    //  string CurrentlyWith = string.Empty; if (InvEvdSearch.EvidenceCurrentlyWith != null) { CurrentlyWith = InvEvdSearch.EvidenceCurrentlyWith; }

                    string SearchText = string.Empty; if (InvEvdSearch.SearchText != null) { SearchText = InvEvdSearch.SearchText; }

                    if (SearchText == string.Empty)
                    {
                        evidence = new InvEvidence()
                        {
                            InvEveden = dc.USP_VIEW_INVESTIGATION_EVIDENCE_DETAILS

                            (InvId, InvEvdSearch.LKEvidenceTypeID, Convert.ToString(InvEvdSearch.EvidenceCurrentlyWith), "").ToList(),
                        };
                    }
                    else
                    {
                        evidence = new InvEvidence()
                        {
                            InvEveden = dc.USP_VIEW_INVESTIGATION_EVIDENCE_DETAILS

                           (InvId, InvEvdSearch.LKEvidenceTypeID, Convert.ToString(InvEvdSearch.EvidenceCurrentlyWith), "").ToList(),
                        };

                        evidence.InvEveden = evidence.InvEveden.Where(x =>
                         string.Equals(x.LKEvidenceCategoryName.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                        || x.LKEvidenceCategoryName.ToLower().Contains(SearchText.ToLower())
                        || string.Equals(x.EvidenceTags.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                        || x.EvidenceTags.ToLower().Contains(SearchText.ToLower())
                        || string.Equals(x.EvidenceName.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                        || x.EvidenceName.ToLower().Contains(SearchText.ToLower())
                        || string.Equals(x.EvidenceIdentificationNumber.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                        || x.EvidenceIdentificationNumber.ToLower().Contains(SearchText.ToLower())
                        || string.Equals(x.PersonReceivingEvidence.ToString(), SearchText, StringComparison.CurrentCultureIgnoreCase)
                        || x.PersonReceivingEvidence.ToLower().Contains(SearchText.ToLower())
                        ).ToList();
                    }

                    ViewBag.page_no = page;

                    ViewBag.count_pagecontent = evidence.InvEveden.Count();

                    evidence.InvestigationFileID = InvInfo.InvestigationFileID;

                    TempData["PageHead"] = InvInfo.InvestigationFileNumber;

                    TempData["SearchKey"] = InvEvdSearch.SearchText;

                    Session["InvFileID"] = InvInfo.InvestigationFileID;

                    TempData["InvId"] = InvInfo.InvestigationFileID;
                    TempData["InvFileID"] = InvInfo.InvestigationFileID;

                    evidence.LocalDateAndTimeOfOccurrence = InvInfo.LocalDateAndTimeOfOccurrence.ToString();

                    evidence.UTCDateAndTimeOfOccurrence = InvInfo.UTCDateAndTimeOfOccurrence.ToString();

                    evidence.NotifierName = InvInfo.NotifierName;

                    evidence.NotifierOrganization = InvInfo.NotifierOrganization;

                    var Evidence_Type_id = dc.LKEvidenceTypes.ToList().OrderBy(x => x.LKEvidenceTypeID);

                    SelectList Evidence_Type = new SelectList(Evidence_Type_id, "LKEvidenceTypeID", "LKEvidenceTypeName");

                    ViewBag.Evidence_Type = Evidence_Type;


                    var Evid_CurrentlyWith = dc.LKCOMMONs.ToList().Where(a => a.LKCOMMONID == 20);  // get Evidence Currently width Type data from Common Lookup Table.
                    SelectList Evid_CurrentlyWith_Select = new SelectList(Evid_CurrentlyWith, "ID", "LKVALUE");
                    ViewBag.Evid_CurrentlyWith = Evid_CurrentlyWith_Select;

                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("TASKController", "EvidenceManagement(InvEvidence InvEvdSearch, int? page, int InvId = 0)", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }
            return View(evidence);
        }
        /// <summary>
        /// view the evidence movement data
        /// </summary> 
        /// 
        /// <param name="EvMovId">Receive investigation evidence movement id </param>
        public ActionResult AddNewMovement(int EvMovId = 0)
        {
            InvEvidence evidence = new InvEvidence();

            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 3 || Convert.ToInt32(Session["RoleId"]) == 6 || Convert.ToInt32(Session["RoleId"]) == 7)
                {

                    var EvidMov_Type = dc.LKCOMMONs.ToList().Where(a => a.LKCOMMONID == 19);  // get Evidence Type data from Common Lookup Table.
                    SelectList EvidMov_Type_Select = new SelectList(EvidMov_Type, "ID", "LKVALUE");
                    ViewBag.Evidence_MovementType = EvidMov_Type_Select;

                    var shipment_id = dc.LKShipmentModes.ToList().OrderBy(x => x.LKShipmentModeID);
                    SelectList Shipment_Selection = new SelectList(shipment_id, "LKShipmentModeID", "LKShipmentModeName");
                    ViewBag.Shipment_Selection = Shipment_Selection;

                    InvestigationEvidenceMovementId = 0;

                    InvestigationEvidenceId = 0;

                    if (EvMovId == 0)
                    {
                        evidence.InvestigationEvidenceMovementID = EvMovId;

                        InvestigationEvidenceMovementId = EvMovId;

                        evidence.InvestigationEvidenceID = Convert.ToInt32(TempData["EvdId"]);

                        evidence.InvestigationFileID = dc.InvestigationEvidences.AsQueryable().Where

                            (a => a.InvestigationEvidenceID == evidence.InvestigationEvidenceID).FirstOrDefault().InvestigationFileID;

                        ViewBag.DateOfMovement = null;
                    }
                    else
                    {
                        var InvestigationEvidenceMovementInfo = dc.InvestigationEvidenceMovements.AsQueryable().Where(a => a.InvestigationEvidenceMovementID == EvMovId).FirstOrDefault();

                        evidence.EvidenceMovementType = InvestigationEvidenceMovementInfo.EvidenceMovementType;

                        evidence.MCustodianOfEvidence = InvestigationEvidenceMovementInfo.CustodianOfEvidence;

                        evidence.MOrganizationHandingOverEvidence = InvestigationEvidenceMovementInfo.OrganizationHandingOverEvidence;

                        evidence.OrganizationReceivingEvidence = InvestigationEvidenceMovementInfo.OrganizationReceivingEvidence;

                        //evidence.PersonHandingOverEvidence = InvestigationEvidenceMovementInfo.PersonHandingOverEvidence;

                        evidence.MPersonReceivingEvidence = InvestigationEvidenceMovementInfo.PersonReceivingEvidence;

                        evidence.ShipmentDetails = InvestigationEvidenceMovementInfo.ShipmentDetails;

                        evidence.LKShipmentModeID = InvestigationEvidenceMovementInfo.LKShipmentModeID;

                        evidence.EvidenceReturned = InvestigationEvidenceMovementInfo.EvidenceReturned;

                        ViewBag.DateOfMovement = convertDateTime(Convert.ToDateTime(InvestigationEvidenceMovementInfo.DateOfMovement));

                        evidence.MPersonHandingOverEvidence = InvestigationEvidenceMovementInfo.PersonHandingOverEvidence;

                        evidence.InvestigationEvidenceMovementID = EvMovId;

                        var InvestigationEvidenceInfo = dc.InvestigationEvidenceMovements.AsQueryable().Where(a => a.InvestigationEvidenceMovementID == EvMovId).FirstOrDefault();

                        evidence.InvestigationEvidenceID = InvestigationEvidenceInfo.InvestigationEvidenceID;

                        //evidence.InvestigationEvidenceID = Convert.ToInt32(TempData["EvdId"]);

                        evidence.InvestigationFileID = dc.InvestigationEvidences.AsQueryable().Where

                            (a => a.InvestigationEvidenceID == evidence.InvestigationEvidenceID).FirstOrDefault().InvestigationFileID;
                        var invstatusInfo = dc.USP_VIEW_INVESTIGATION_FORM35(evidence.InvestigationFileID).FirstOrDefault();

                        TempData["INV_STATUS"] = invstatusInfo.LKInvestigationStatusTypeID;
                    }
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("TASKController", "AddNewMovement(InvEvidence evidence)", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }

            return View(evidence);
        }
        /// <summary>
        /// Save or update evidence movement data based on investigation
        /// </summary> 
        /// 
        /// <param name="evidence">Receive investigation evidence details as object</param>
        [HttpPost]
        public ActionResult AddNewMovements(InvEvidence evidence)
        {
            var result = 0;
            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 7)
                {
                    if (Convert.ToInt32(Session["UserId"]) > 0)
                    {
                        if (evidence.InvestigationEvidenceMovementID > 0)
                        {
                            result = dc.USP_UPDT_INVESTIGATION_EVIDENCE_MOVEMENT(evidence.InvestigationEvidenceMovementID, 0, evidence.EvidenceMovementType, evidence.EvidenceReturned,

                                evidence.MPersonHandingOverEvidence, evidence.MOrganizationHandingOverEvidence, evidence.DateOfMovement, evidence.MPersonReceivingEvidence,

                                evidence.OrganizationReceivingEvidence, evidence.MCustodianOfEvidence, evidence.LKShipmentModeID, evidence.ShipmentDetails, Convert.ToInt32(Session["UserId"]));

                            result = evidence.InvestigationEvidenceMovementID;
                        }
                        else
                        {
                            //evidence.EvidenceMovementType

                            // 64- default value from common lookup table => 64	19	Evidence_MovementType	Inward

                            result = dc.USP_INST_INVESTIGATION_EVIDENCE_MOVEMENT(evidence.InvestigationEvidenceID, 64, evidence.EvidenceReturned,

                                evidence.MPersonHandingOverEvidence, evidence.MOrganizationHandingOverEvidence, evidence.DateOfMovement, evidence.MPersonReceivingEvidence,

                                evidence.OrganizationReceivingEvidence, evidence.MCustodianOfEvidence, evidence.LKShipmentModeID, evidence.ShipmentDetails, Convert.ToInt32(Session["UserId"]));

                            var InvEvidenceMovementId = dc.InvestigationEvidenceMovements.DefaultIfEmpty().Max(a => a.InvestigationEvidenceMovementID);

                            result = InvEvidenceMovementId;

                            // InvestigationEvidenceMovementId = InvEvidenceMovementId;
                        }
                    }
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("TASKController", "AddNewMovement(InvEvidence evidence)", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }

            return RedirectToAction("AddNewEvidence", "task", new { InvId = evidence.InvestigationFileID, EvMovId = result });
        }
        /// <summary>
        /// view the investigation evidence details
        /// </summary> 
        /// 
        /// <param name="EvMovId">Receive Evidence movement page no</param>
        /// <param name="InvId">Receive investigation file id </param>
        public ActionResult AddNewEvidence(int InvId = 0, int EvMovId = 0)
        {
            InvEvidence evidence = new InvEvidence();

            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 3 || Convert.ToInt32(Session["RoleId"]) == 6 || Convert.ToInt32(Session["RoleId"]) == 7)
                {
                    var CheckInvPermission = dc.InvestigationIICAssignments.AsEnumerable().Where(a => a.InvestigationFileID == InvId).FirstOrDefault();

                    if ((CheckInvPermission.IICAssigned == Convert.ToInt32(Session["UserId"]) &&

                                           CheckInvPermission.InvestigationFileID == InvId) || Convert.ToInt32(Session["RoleId"]) == 3 || Convert.ToInt32(Session["RoleId"]) == 6 || Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 7)
                    {
                        TempData["BackPage"] = "evd_mg";
                        TempData["active_url_EvidenceManagement"] = "tm";
                        TempData["active_url_EvidenceManagement_iic"] = "iic";
                        TempData["lf_menu"] = "1";

                        investigation_fileid = InvId;

                        InvestigationEvidenceMovementId = 0;

                        InvestigationEvidenceId = 0;

                        var InvInfo = dc.USP_VIEW_INVESTIGATION_INVESTIGATIONID(InvId).FirstOrDefault();


                        var invstatusInfo = dc.USP_VIEW_INVESTIGATION_FORM35(InvId).FirstOrDefault();

                        TempData["INV_STATUS"] = invstatusInfo.LKInvestigationStatusTypeID;


                        evidence.InvestigationFileID = InvInfo.InvestigationFileID;

                        TempData["PageHead"] = InvInfo.InvestigationFileNumber;

                        Session["InvFileID"] = InvInfo.InvestigationFileID;

                        TempData["InvId"] = InvInfo.InvestigationFileID;
                        TempData["InvFileID"] = InvInfo.InvestigationFileID;


                        TempData["EvMovId"] = EvMovId;

                        //Evidence Assigned
                        List<InvestigationTeamMember> teammembers = dc.InvestigationTeamMembers.Where(x => x.InvestigationFileID == InvId).ToList();
                        foreach (var team in teammembers)
                        {
                            if (team.TeamMemberID == Convert.ToInt32(Session["UserId"]))
                            {
                                evidence.evidenceAssigned = team.TeamMemberID;
                            }
                        }

                        evidence.LocalDateAndTimeOfOccurrence = InvInfo.LocalDateAndTimeOfOccurrence.ToString();

                        evidence.UTCDateAndTimeOfOccurrence = InvInfo.UTCDateAndTimeOfOccurrence.ToString();

                        evidence.NotifierName = InvInfo.NotifierName;

                        evidence.NotifierOrganization = InvInfo.NotifierOrganization;

                        var Evidence_Category_id = dc.LKEvidenceCategories.ToList().OrderBy(x => x.LKEvidenceCategoryID);
                        SelectList Evidence_Category = new SelectList(Evidence_Category_id, "LKEvidenceCategoryID", "LKEvidenceCategoryName");
                        ViewBag.Evidence_Category = Evidence_Category;

                        var Evidence_Type_id = dc.LKEvidenceTypes.ToList().OrderBy(x => x.LKEvidenceTypeID);
                        SelectList Evidence_Type = new SelectList(Evidence_Type_id, "LKEvidenceTypeID", "LKEvidenceTypeName");
                        ViewBag.Evidence_Type = Evidence_Type;

                        var EvidMov_Type = dc.LKCOMMONs.ToList().Where(a => a.LKCOMMONID == 19);  //get Evidence Type data from Common Lookup Table.
                        SelectList EvidMov_Type_Select = new SelectList(EvidMov_Type, "ID", "LKVALUE");
                        ViewBag.Evidence_MovementType = EvidMov_Type_Select;

                        var Evid_CurrentlyWith = dc.LKCOMMONs.ToList().Where(a => a.LKCOMMONID == 20);  //get Evidence Currently width Type data from Common Lookup Table.
                        SelectList Evid_CurrentlyWith_Select = new SelectList(Evid_CurrentlyWith, "ID", "LKVALUE");
                        ViewBag.Evid_CurrentlyWith = Evid_CurrentlyWith_Select;

                        var shipment_id = dc.LKShipmentModes.ToList().OrderBy(x => x.LKShipmentModeID);
                        SelectList Shipment_Selection = new SelectList(shipment_id, "LKShipmentModeID", "LKShipmentModeName");
                        ViewBag.Shipment_Selection = Shipment_Selection;

                        if (EvMovId == 0)
                        {
                            ViewBag.edit = 0;

                            ObjectParameter EvIdentificationNo = new ObjectParameter("EvIdentificationNo", typeof(string));

                            dc.USP_VIEW_INVESTIGATION_EVIDENCE_IDENTIFICATION_NUMBER(evidence.InvestigationFileID, InvInfo.InvestigationFileNumber, EvIdentificationNo);

                            evidence.EvidenceIdentificationNumber = EvIdentificationNo.Value.ToString();

                            evidence.InvestigationFileID = InvId;
                        }
                        else
                        {
                            ViewBag.edit = 1;

                            var InvestigationEvidenceMovementInfo = dc.InvestigationEvidenceMovements.AsQueryable().Where(a => a.InvestigationEvidenceMovementID == EvMovId).FirstOrDefault();

                            var evedenceMovements = dc.USP_VIEW_INVESTIGATION_EVIDENCE_MOVEMENTS(InvestigationEvidenceMovementInfo.InvestigationEvidenceID);

                            ViewBag.evedence_Movements = evedenceMovements;

                            var InvestigationEvidenceInfo = dc.InvestigationEvidences.AsQueryable().Where(a => a.InvestigationEvidenceID == InvestigationEvidenceMovementInfo.InvestigationEvidenceID).FirstOrDefault();

                            evidence.InvestigationEvidenceID = InvestigationEvidenceInfo.InvestigationEvidenceID;

                            evidence.InvestigationFileID = InvestigationEvidenceInfo.InvestigationFileID;

                            evidence.EvidenceIdentificationNumber = InvestigationEvidenceInfo.EvidenceIdentificationNumber;

                            evidence.EvidenceName = InvestigationEvidenceInfo.EvidenceName;

                            evidence.EvidenceManufacturer = InvestigationEvidenceInfo.EvidenceManufacturer;

                            evidence.EvidencePartNumber = InvestigationEvidenceInfo.EvidencePartNumber;

                            evidence.EvidenceSerialNumber = InvestigationEvidenceInfo.EvidenceSerialNumber;

                            evidence.LKEvidenceCategoryID = InvestigationEvidenceInfo.LKEvidenceCategoryID;

                            evidence.LKEvidenceTypeID = InvestigationEvidenceInfo.LKEvidenceTypeID;

                            evidence.EvidenceTags = InvestigationEvidenceInfo.EvidenceTags;

                            evidence.EvidenceDescription = InvestigationEvidenceInfo.EvidenceDescription;

                            evidence.LocationOfStorage = InvestigationEvidenceInfo.LocationOfStorage;


                            //
                            //var evedenceCurrentDepartment = dc.InvestigationEvidenceMovements.AsQueryable().

                            //    Where(a =>a.InvestigationEvidenceID == evidence.InvestigationEvidenceID ).OrderByDescending(a => a.InvestigationEvidenceID).FirstOrDefault();

                            evidence.EvidenceCurrentlyWith = Convert.ToInt32(InvestigationEvidenceInfo.EvidenceCurrentlyWith);
                            //                                        
                            evidence.CustodianOfEvidence = InvestigationEvidenceMovementInfo.CustodianOfEvidence;

                            evidence.OrganizationHandingOverEvidence = InvestigationEvidenceMovementInfo.OrganizationHandingOverEvidence;

                            evidence.OrganizationReceivingEvidence = InvestigationEvidenceMovementInfo.OrganizationReceivingEvidence;

                            evidence.PersonHandingOverEvidence = InvestigationEvidenceMovementInfo.PersonHandingOverEvidence;

                            evidence.PersonReceivingEvidence = InvestigationEvidenceMovementInfo.PersonReceivingEvidence;

                            evidence.ShipmentDetails = InvestigationEvidenceMovementInfo.ShipmentDetails;

                            evidence.LKShipmentModeID = InvestigationEvidenceMovementInfo.LKShipmentModeID;

                            evidence.EvidenceReturned = InvestigationEvidenceMovementInfo.EvidenceReturned;

                            ViewBag.DateOfMovement = convertDate(Convert.ToDateTime(InvestigationEvidenceMovementInfo.DateOfMovement));

                            ViewBag.DateOfReceipt = convertDate(Convert.ToDateTime(InvestigationEvidenceInfo.DateOfReceipt));

                            TempData["EvdId"] = InvestigationEvidenceInfo.InvestigationEvidenceID;

                            InvestigationEvidenceMovementId = EvMovId;
                        }

                        var AttachedFiles = dc.USP_VIEW_INVESTIGATION_EVIDENCE_ATTACHMENTFILE(EvMovId);
                        ViewBag.Attached_Files = AttachedFiles;
                    }
                    else
                    {
                        return RedirectToAction("Dashboard", "Home");
                    }
                }
                else
                {
                    return RedirectToAction("Error");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("TASKController", "InvestigationFlow(int invid=0)", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }
            return View(evidence);
        }
        /// <summary>
        /// Save the New Evidence details
        /// </summary> 
        /// 
        /// <param name="evidence">Receive investigation evidence details </param>
        [HttpPost]
        public ActionResult AddNewEvidence(InvEvidence evidence)
        {
            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 7)
                {
                    if (Convert.ToInt32(Session["UserId"]) > 0)
                    {
                        TempData["BackPage"] = "evd_mg";
                        TempData["active_url_EvidenceManagement"] = "tm";
                        TempData["lf_menu"] = "1";

                        var InvInfo = dc.USP_VIEW_INVESTIGATION_INVESTIGATIONID(evidence.InvestigationFileID).FirstOrDefault();

                        evidence.InvestigationFileID = InvInfo.InvestigationFileID;

                        TempData["PageHead"] = InvInfo.InvestigationFileNumber;

                        Session["InvFileID"] = InvInfo.InvestigationFileID;

                        TempData["InvId"] = InvInfo.InvestigationFileID;

                        evidence.LocalDateAndTimeOfOccurrence = InvInfo.LocalDateAndTimeOfOccurrence.ToString();

                        evidence.UTCDateAndTimeOfOccurrence = InvInfo.UTCDateAndTimeOfOccurrence.ToString();

                        evidence.NotifierName = InvInfo.NotifierName;

                        evidence.NotifierOrganization = InvInfo.NotifierOrganization;

                        var result = 0;

                        if (evidence.DateOfReceipt == null)
                        {
                            evidence.DateOfReceipt = DateTime.Now;
                        }

                        if (evidence.InvestigationEvidenceID > 0)
                        {
                            result = dc.USP_UPDT_INVESTIGATION_EVIDENCE(evidence.InvestigationEvidenceID, evidence.InvestigationFileID, evidence.EvidenceIdentificationNumber,

                                evidence.EvidencePartNumber, evidence.EvidenceSerialNumber, evidence.LKEvidenceCategoryID, evidence.LKEvidenceTypeID,

                                evidence.EvidenceName, evidence.EvidenceManufacturer, evidence.EvidenceDescription, evidence.LocationOfStorage, Convert.ToString(evidence.EvidenceTags),

                                Convert.ToString(evidence.EvidenceCurrentlyWith), Convert.ToInt32(Session["UserId"]), evidence.DateOfReceipt);
                        }
                        else
                        {
                            result = dc.USP_INST_INVESTIGATION_EVIDENCE(evidence.InvestigationFileID, evidence.EvidencePartNumber, evidence.EvidenceSerialNumber,

                                evidence.LKEvidenceCategoryID, evidence.LKEvidenceTypeID, evidence.EvidenceName,evidence.EvidenceManufacturer, evidence.EvidenceDescription,

                                evidence.LocationOfStorage, Convert.ToString(evidence.EvidenceTags), Convert.ToString(evidence.EvidenceCurrentlyWith),

                                Convert.ToInt32(Session["UserId"]), InvInfo.InvestigationFileNumber, evidence.DateOfReceipt);

                            InvestigationEvidenceId = dc.InvestigationEvidences.DefaultIfEmpty().Max(a => a.InvestigationEvidenceID);

                            evidence.InvestigationEvidenceID = InvestigationEvidenceId;

                            result = dc.USP_INST_INVESTIGATION_EVIDENCE_MOVEMENT(InvestigationEvidenceId, evidence.EvidenceMovementType, evidence.EvidenceReturned,

                               evidence.PersonHandingOverEvidence, evidence.OrganizationHandingOverEvidence, evidence.DateOfReceipt, evidence.PersonReceivingEvidence,

                               evidence.OrganizationReceivingEvidence, evidence.CustodianOfEvidence, evidence.LKShipmentModeID, evidence.ShipmentDetails, Convert.ToInt32(Session["UserId"]));

                            InvestigationEvidenceMovementId = dc.InvestigationEvidenceMovements.DefaultIfEmpty().Max(a => a.InvestigationEvidenceMovementID);
                        }

                        string EvidenceIdentificationNumber = dc.InvestigationEvidences.Where(a => a.InvestigationEvidenceID == evidence.InvestigationEvidenceID).FirstOrDefault().EvidenceIdentificationNumber;

                        SaveEvidenceFiles(Request.Files, EvidenceIdentificationNumber);

                        ViewBag.edit = 1;

                        var Evidence_Category_id = dc.LKEvidenceCategories.ToList().OrderBy(x => x.LKEvidenceCategoryID);
                        SelectList Evidence_Category = new SelectList(Evidence_Category_id, "LKEvidenceCategoryID", "LKEvidenceCategoryName");
                        ViewBag.Evidence_Category = Evidence_Category;

                        var Evidence_Type_id = dc.LKEvidenceTypes.ToList().OrderBy(x => x.LKEvidenceTypeID);
                        SelectList Evidence_Type = new SelectList(Evidence_Type_id, "LKEvidenceTypeID", "LKEvidenceTypeName");
                        ViewBag.Evidence_Type = Evidence_Type;

                        var EvidMov_Type = dc.LKCOMMONs.ToList().Where(a => a.LKCOMMONID == 19);  // get Evidence Type data from Common Lookup Table.
                        SelectList EvidMov_Type_Select = new SelectList(EvidMov_Type, "ID", "LKVALUE");
                        ViewBag.Evidence_MovementType = EvidMov_Type_Select;

                        var shipment_id = dc.LKShipmentModes.ToList().OrderBy(x => x.LKShipmentModeID);
                        SelectList Shipment_Selection = new SelectList(shipment_id, "LKShipmentModeID", "LKShipmentModeName");
                        ViewBag.Shipment_Selection = Shipment_Selection;

                        var AttachedFiles = dc.USP_VIEW_INVESTIGATION_EVIDENCE_ATTACHMENTFILE(InvestigationEvidenceMovementId);
                        ViewBag.Attached_Files = AttachedFiles;

                        var evedenceMovements = dc.USP_VIEW_INVESTIGATION_EVIDENCE_MOVEMENTS(evidence.InvestigationEvidenceID);
                        ViewBag.evedence_Movements = evedenceMovements;

                        ViewBag.Dateof_Receipt = evidence.DateOfReceipt;
                    }
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("TASKController", " AddNewEvidence(InvEvidence evidence)", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }

            return RedirectToAction("AddNewEvidence", "task", new { InvId = evidence.InvestigationFileID, EvMovId = InvestigationEvidenceMovementId });
        }
        //[HttpPost]
        //public void SaveEvidenceFiles()
        //{
        //    try
        //    {
        //        if (Convert.ToInt32(Session["UserId"]) > 0)
        //        {
        //            if (InvestigationEvidenceMovementId > 0)
        //            {
        //                if (Request.Files.Count > 0)
        //                {
        //                    //comm.CreateIfMissing(Server.MapPath("~/") + "AttachmentFiles");

        //                    for (int i = 0; i < Request.Files.Count; i++)
        //                    {
        //                        string[] filePath = null;

        //                        HttpPostedFileBase file = Request.Files[i];

        //                        int fileSize = file.ContentLength;

        //                        string fileName = file.FileName.Substring(0, file.FileName.LastIndexOf('.')) + "_" + DateTime.Now.ToFileTime() + file.FileName.Substring(file.FileName.LastIndexOf('.'), ((file.FileName.Length) - (file.FileName.LastIndexOf('.'))));
        //                        string mimeType = file.ContentType.Split('/')[1].ToString();

        //                        int DocumentTypeId = Convert.ToInt32(dc.LKDocumentTypes.AsQueryable().Where(d => d.LKDocumentTypeName == mimeType).Select(d => d.LKDocumentTypeID).FirstOrDefault());

        //                        Stream fileContent = file.InputStream;

        //                        //  file.SaveAs(Server.MapPath("~/") + "AttachmentFiles\\" + fileName);

        //                        //filePath = comm.savetoSharePoint(fileContent, Server.MapPath("~/") + "AttachmentFiles\\" + fileName, InvestigationEvidenceMovementId);

        //                        filePath = comm.savetoSharePointOtherDocsOnPrem(fileContent, fileName, Convert.ToString(InvestigationEvidenceMovementId), "Evidence_Documents");

        //                        if (filePath[0] != string.Empty)
        //                        {
        //                            var result = dc.USP_INST_INVESTIGATION_EVIDENCE_DOCUMENTASATTACHMENT(0, InvestigationEvidenceMovementId, 1, DocumentTypeId, filePath[0].ToString(), Convert.ToInt32(Session["UserId"]), filePath[1]);
        //                        }
        //                    }
        //                    //  comm.DeleteDirectory(Server.MapPath("~/") + "AttachmentFiles");
        //                }

        //                ViewBag.edit = 1;

        //                //InvestigationEvidenceMovementId = 0;
        //            }
        //            else
        //            {
        //                if (Request.Files.Count > 0)
        //                {
        //                    // comm.CreateIfMissing(Server.MapPath("~/") + "AttachmentFiles");

        //                    InvestigationEvidenceMovementId = (Int32)(dc.InvestigationEvidenceMovements.DefaultIfEmpty().Max(m => m == null ? 0 : m.InvestigationEvidenceMovementID)) + 1;

        //                    for (int i = 0; i < Request.Files.Count; i++)
        //                    {
        //                        string[] filePath = null;

        //                        HttpPostedFileBase file = Request.Files[i];

        //                        int fileSize = file.ContentLength;

        //                        string fileName = file.FileName;

        //                        string mimeType = file.ContentType.Split('/')[1].ToString();

        //                        int DocumentTypeId = Convert.ToInt32(dc.LKDocumentTypes.AsQueryable().Where(d => d.LKDocumentTypeName == mimeType).Select(d => d.LKDocumentTypeID).FirstOrDefault());

        //                        Stream fileContent = file.InputStream;

        //                        //     file.SaveAs(Server.MapPath("~/") + "AttachmentFiles\\" + fileName);

        //                      //  filePath = comm.savetoSharePoint(fileContent, fileName, InvestigationEvidenceMovementId);

        //                        filePath = comm.savetoSharePointOtherDocsOnPrem(fileContent, fileName, Convert.ToString(InvestigationEvidenceMovementId), "Evidence_Documents");

        //                        if (filePath[i] != string.Empty)
        //                        {
        //                            var result = dc.USP_INST_INVESTIGATION_EVIDENCE_DOCUMENTASATTACHMENT(0, InvestigationEvidenceMovementId, 1, DocumentTypeId, filePath[0].ToString(), Convert.ToInt32(Session["UserId"]), filePath[1].ToString());
        //                        }
        //                    }
        //                    //  comm.DeleteDirectory(Server.MapPath("~/") + "AttachmentFiles");

        //                    //InvestigationEvidenceMovementId = 0;
        //                }
        //            }
        //        }
        //    }
        //    catch (System.Exception ex)
        //    {
        //        comm.Exception_Log("TaskController", "SaveEvidenceFiles", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
        //    }
        //}       
        /// <summary>
        /// Save the attached Evidence files
        /// </summary> 
        /// 
        /// <param name="files">Receive investigation file attachment details </param>
        /// <param name="EVFILE_NUMER">Receive investigation evidence file no </param>
        public void SaveEvidenceFiles(HttpFileCollectionBase files, string EVFILE_NUMER)
        {
            try
            {
                if (files.Count > 0)
                {
                    if (EVFILE_NUMER != string.Empty)
                    {
                        int filecount = 0;

                        for (int i = 0; i < files.Count; i++)
                        {
                            HttpPostedFileBase file = files[i];

                            if (file.ContentLength > 0 && !string.IsNullOrEmpty(file.FileName))
                            {
                                int fileSize = file.ContentLength;

                                string fileName = file.FileName.Substring(0, file.FileName.LastIndexOf('.')) + "_" + DateTime.Now.ToFileTime() + file.FileName.Substring(file.FileName.LastIndexOf('.'), ((file.FileName.Length) - (file.FileName.LastIndexOf('.'))));

                                string mimeType = file.ContentType.Split('/')[1].ToString();

                                int DocumentTypeId = Convert.ToInt32(dc.LKDocumentTypes.AsQueryable().Where(d => d.LKDocumentTypeName == mimeType).Select(d => d.LKDocumentTypeID).FirstOrDefault());

                                Stream fileContent = file.InputStream;

                                string[] attachURLs = comm.savetoSharePointOtherDocsOnPrem(fileContent, fileName, EVFILE_NUMER, "Evidence_Documents");

                                var result = dc.USP_INST_INVESTIGATION_EVIDENCE_DOCUMENTASATTACHMENT(0, InvestigationEvidenceMovementId, 1, DocumentTypeId, attachURLs[0].ToString(), Convert.ToInt32(Session["UserId"]), attachURLs[1].ToString());

                                SharepointfilePath[filecount] = attachURLs[0];

                                SharepointAnonymousPath[filecount] = attachURLs[1];

                                filecount++;
                            }
                        }
                    }

                    var fullpathoffile = Server.MapPath("~/App_Data/Evidence_Files/" + investigation_fileid);

                    if (System.IO.Directory.Exists(fullpathoffile))
                    {
                        comm.DeleteDirectory(fullpathoffile);

                        System.IO.Directory.Delete(fullpathoffile);
                    }
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("DIController", "SaveFiles", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }
        }
        /// <summary>
        /// Delete attached Evidence files
        /// </summary> 
        /// 
        /// <param name="id">Receive Evidence attachment file id</param>
        /// <param name="path">Receive investigation attachment file path </param>
        [HttpPost]
        public JsonResult DeleteEvidenceAttachment(Int32 id, string path)
        {
            string results = string.Empty;

            try
            {
                var result = 0;

                if (Convert.ToInt32(Session["UserId"]) > 0)
                {
                    if (Convert.ToInt32(id) > 0)
                    {
                        bool IsFileDeleted = comm.deleteFiles(path);

                        result = dc.USP_INST_INVESTIGATION_EVIDENCE_DOCUMENTASATTACHMENT(Convert.ToInt32(id), 0, 0, 0, null, Convert.ToInt32(Session["UserId"]), "");

                        results = "success";
                    }
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("TaskController", "DeleteEvidenceAttachment", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }
            return Json(results);
        }
        /// <summary>
        /// View investigation team member list
        /// </summary> 
        /// 
        /// <param name="TaskEventId">Receive investigation task event id</param>
        /// <param name="InvestigationId">Receive investigation file id</param>
        [HttpPost]
        public ActionResult GetInvestigationTeamMemberList(int TaskEventId = 0, int InvestigationId = 0)
        {
            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();

            dynamic UserInfo = null;

            try
            {
                UserInfo = dc.USP_VIEW_TEAMMEMBERNAME_BY_TASKGROUPID(InvestigationId, TaskEventId).ToList();

                //UserInfo = javaScriptSerializer.Serialize(UserInfo);
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("DIController", " AddAirOperatorName", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }

            return Json(UserInfo);
        }
        /// <summary>
        /// View all team member with cases  based on investigation file id
        /// </summary> 
        /// 
        /// <param name="id">Receive investigation file id</param>
        public ActionResult Teammanagement(int id)
        {
            dynamic accid_team = null;

            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 3 || Convert.ToInt32(Session["RoleId"]) == 6 || Convert.ToInt32(Session["RoleId"]) == 7)
                {
                    var CheckInvPermission = dc.InvestigationIICAssignments.AsEnumerable().Where(a => a.InvestigationFileID == id).FirstOrDefault();

                    if ((CheckInvPermission.IICAssigned == Convert.ToInt32(Session["UserId"]) &&

                                           CheckInvPermission.InvestigationFileID == id) || Convert.ToInt32(Session["RoleId"]) == 3 || Convert.ToInt32(Session["RoleId"]) == 6 || Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 7)
                    {
                        var InvInfo = dc.USP_VIEW_INVESTIGATION_INVESTIGATIONID(id).FirstOrDefault();
                        TempData["PageHead"] = InvInfo.InvestigationFileNumber;
                        TempData["BackPage"] = "yes";
                        TempData["InvId"] = id;
                        TempData["InvFileID"] = id;
                        TempData["active_teammgmt"] = "iic";

                        TempData["active_url"] = "tm";
                        TempData["lf_menu"] = "1";
                        var model = new teammanagement();
                        gcaa_ismEntities gcaa = new gcaa_ismEntities();

                        accid_team = new teammanagement()
                        {
                            accidteammembers = gcaa.USP_VIEW_ACCID_TEAM_MEMBERS(id).ToList(),
                            assignteammembers = gcaa.USP_VIEW_ASSIGN_TEAM_MEMBERS(id).ToList(),
                            Inv_team = gcaa.USP_VIEW_INVESTIGATION_FORM35(id).ToList()

                        };

                        ViewBag.invfile = id;

                        var invInfo = dc.USP_VIEW_INVESTIGATION_FORM35(id).FirstOrDefault();

                        TempData["INV_STATUS"] = invInfo.LKInvestigationStatusTypeID;
                    }
                    else
                    {
                        return RedirectToAction("PageNotFound", "Error");
                    }
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("TASKController", " AddNewEvidence(InvEvidence evidence)", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }

            return View(accid_team);
        }
        /// <summary>
        /// save the new team member to particular investigation 
        /// </summary> 
        /// 
        /// <param name="tm">Receive investigation Team member details</param>
        [HttpPost]
        public ActionResult Teammanagement(AddTeammember tm)
        {
            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 3 || Convert.ToInt32(Session["RoleId"]) == 7)
                {
                    if (Convert.ToInt32(Session["UserId"]) > 0)
                    {
                        var team_member = dc.USP_VIEW_TEAMMEMBER(tm.invfile).ToList();
                        if (team_member.Count() == 0)
                        {
                            if (tm.UserID == null)
                            {
                                return RedirectToAction("Teammanagement", "task", new { id = tm.invfile });
                            }
                            else
                            {
                                for (int i = 0; i < tm.UserID.Length; i++)
                                {
                                    dc.USP_INST_TEAMMEMBERS(tm.invfile, tm.UserID[i], Convert.ToInt32(Session["UserId"]));
                                }
                            }
                        }
                        else
                        {
                            if (tm.UserID == null)
                            {
                                var tm_member = dc.USP_VIEW_TEAMMEMBER(tm.invfile).ToList();
                                foreach (var item_teammember in tm_member)
                                {
                                    dc.USP_INST_TEAMMEMBERHISTORY_WITHOUT_REASON(item_teammember.InvestigationFileID, item_teammember.TeamMemberID, Convert.ToInt32(Session["UserId"]));
                                    dc.USP_DELETE_TEAMMEMBER(item_teammember.InvestigationFileID, item_teammember.TeamMemberID);
                                }
                            }
                            else
                            {
                                for (int i = 0; i < tm.UserID.Length; i++)
                                {

                                    dc.USP_Teammember_Removal_Process(tm.invfile, tm.UserID[i], Convert.ToInt32(Session["UserId"]));

                                }
                                var temp_invfile = dc.USP_TeamTemp_Retrieval(tm.invfile).ToList();

                                foreach (var item_temp in temp_invfile)
                                {
                                    dc.USP_DELETE_TEAMMEMBER(item_temp.InvestigationFileID, item_temp.TeamMemberID);
                                    dc.USP_INST_TEAMMEMBERHISTORY_WITHOUT_REASON(item_temp.InvestigationFileID, item_temp.TeamMemberID, Convert.ToInt32(Session["UserId"]));

                                }
                                dc.USP_CLEAR_TEAMTEMP(tm.invfile);
                            }
                        }
                    }
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("", "", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }

            return RedirectToAction("Teammanagement", "task", new { id = tm.invfile });
        }
        /// <summary>
        /// Remove team member from the investigation 
        /// </summary>  
        /// 
        /// <param name="Userid">Receive investigation user details</param>
        /// <param name="invfile">Receive investigation file details</param>
        public ActionResult teamremoval(int Userid, int invfile)
        {
            string result = string.Empty;
            var team_member = 0;

            var team_mem_id_rem = dc.USP_VIEW_TEAMMEMBER_FOR_DELETING(invfile, Userid).ToList();
            // var team_member_rem = dc.USP_VIEW_TEAMMEMBER_FOR_REMOVAL(invfile, Userid).ToList();
            var event_assigned_mbr = dc.USP_VIEW_REMOVE_EVENT_ASSIGNED_TEAM_MEMBER(invfile, Userid).ToList();
            if (event_assigned_mbr != null)
            {
                for (int i = 0; i < event_assigned_mbr.Count(); i++)
                {
                    int eventmbr = event_assigned_mbr[i].InvestigationTaskAssignmentID;

                    var eassigned_mbr = (from multtask in dc.InvestigationMultipleTaskAssignments
                                         where multtask.InvestigationTaskAssignmentID == eventmbr
                                         select new
                                         {
                                             multtask.InvestigationMultipleTaskAssignmentID,
                                             multtask.TeamMemberID,
                                             multtask.InvestigationTaskAssignment
                                         }).ToList();
                    if (eassigned_mbr.Count() == 1)
                    {
                        team_member = 4;

                        break;
                    }
                }


            }

            var team_member_rem = dc.USP_VIEW_MULTIPLE_TEAMMEMBER_FOR_REMOVAL(invfile, Userid).ToList();

            if (team_member == 4)
            {
                team_member = 4;
            }

            else if (team_member_rem.Count() > 0 && team_mem_id_rem.Count() > 0)
            {
                team_member = 1;
            }
            else if (team_mem_id_rem.Count() > 0)
            {

                team_member = 2;
            }
            else
            {

                team_member = 3;
            }

            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            result = javaScriptSerializer.Serialize(team_member);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Remove team member with reason from the investigation 
        /// </summary> 
        /// 
        /// <param name="UserID">Receive the current user Id</param>
        /// <param name="invfile">Receive investigation file Id</param>
        /// <param name="removal_reason">Receive team member removal reason</param>
        public ActionResult Team_member_removal(int UserID, int invfile, string removal_reason)
        {
            string result = string.Empty;

            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 3)
                {
                    if (Convert.ToInt32(Session["UserId"]) > 0)
                    {
                        gcaa_ismEntities gc = new gcaa_ismEntities();
                        gc.USP_DELETE_TEAMMEMBER(invfile, UserID);
                        gc.USP_INST_TEAMMEMBERHISTORY(invfile, UserID, Convert.ToInt32(Session["UserId"]), removal_reason);
                        //  var team_member_rem = gc.USP_VIEW_TEAMMEMBER_FOR_REMOVAL(invfile, UserID).ToList(); code commented for jira ticket multiple teammember by jackson@xminds.in
                        var team_member_rem = gc.USP_VIEW_MULTIPLE_TEAMMEMBER_FOR_REMOVAL(invfile, UserID).ToList();// code code added jira ticket multiple teammember//jackson@xminds.in
                        foreach (var item in team_member_rem)
                        {
                            // gc.USP_UPDATE_INVESTIGATION_TASK_ASSIGNMENT(invfile, UserID, item.InvestigationTaskGroupEventID, Convert.ToInt32(Session["UserId"]), removal_reason);
                            gc.USP_UPDATE_MULTIPLE_INVESTIGATION_TASK_ASSIGNMENT(invfile, UserID, item.InvestigationTaskGroupEventID, Convert.ToInt32(Session["UserId"]), removal_reason, item.InvestigationTaskAssignmentID);
                        }

                        var success = 1;
                        JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                        result = javaScriptSerializer.Serialize(success);
                    }
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("", "", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// View distribution list based on the investigation 
        /// </summary>  
        /// <param name="page">Receive investigation distribution list page no</param>
        /// <param name="InvId">Receive investigation file id</param>
        public ActionResult Managedistributionlist(int? page, int InvId = 0)
        {
            InvDistributionList invDistribution = new InvDistributionList();

            var ManageDisList = new InvDistributionList();

            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 3 || Convert.ToInt32(Session["RoleId"]) == 6 || Convert.ToInt32(Session["RoleId"]) == 7)
                {
                    var CheckInvPermission = dc.InvestigationIICAssignments.AsEnumerable().Where(a => a.InvestigationFileID == InvId).FirstOrDefault();

                    if ((CheckInvPermission.IICAssigned == Convert.ToInt32(Session["UserId"]) &&

                                           CheckInvPermission.InvestigationFileID == InvId) || Convert.ToInt32(Session["RoleId"]) == 3 || Convert.ToInt32(Session["RoleId"]) == 6 || Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 7)
                    {
                        TempData["BackPage"] = "yes";
                        TempData["active_url_Managedistributionlist"] = "tm";
                        TempData["active_url_Managedistributionlist_iic"] = "iic";

                        TempData["lf_menu"] = "1";

                        if (page == null)
                        {
                            ViewBag.serial_nbr = 0;
                        }
                        else
                        {
                            ViewBag.serial_nbr = 10 * (page - 1);
                        }


                        ManageDisList = new InvDistributionList()
                        {
                            InvDistrList = dc.USP_VIEW_INVESTIGATION_DISTRIBUTIONLIST(InvId).ToList()
                        };

                        var InvInfo = dc.USP_VIEW_INVESTIGATION_INVESTIGATIONID(InvId).FirstOrDefault();

                        ManageDisList.InvestigationFileID = InvInfo.InvestigationFileID;

                        TempData["PageHead"] = InvInfo.InvestigationFileNumber;

                        Session["InvFileID"] = InvInfo.InvestigationFileID;

                        TempData["InvId"] = InvInfo.InvestigationFileID;

                        TempData["InvFileID"] = InvInfo.InvestigationFileID;

                        ManageDisList.NotifierName = InvInfo.NotifierName;

                        ManageDisList.NotifierOrganization = InvInfo.NotifierOrganization;

                        ManageDisList.LocalDateAndTimeOfOccurrence = InvInfo.LocalDateAndTimeOfOccurrence.ToString();

                        ManageDisList.UTCDateAndTimeOfOccurrence = InvInfo.UTCDateAndTimeOfOccurrence.ToString();

                        var disListInfo = dc.USP_VIEW_INVESTIGATION_DISTRIBUTIONLIST(InvInfo.InvestigationFileID);

                        List<InvestigationTeamMember> teammembers = dc.InvestigationTeamMembers.Where(x => x.InvestigationFileID == InvId).ToList();
                        foreach (var team in teammembers)
                        {
                            if (team.TeamMemberID == Convert.ToInt32(Session["UserId"]))
                            {
                                ManageDisList.invAssigned = team.TeamMemberID;
                            }
                        }

                        ViewBag.Distribution_List = disListInfo;

                        ViewBag.page_count = ManageDisList.InvDistrList.Count();

                        ViewBag.count_pagecontent = ManageDisList.InvDistrList.Count();

                        ViewBag.page_no = page;

                        var invInfo = dc.USP_VIEW_INVESTIGATION_FORM35(InvId).FirstOrDefault();

                        TempData["INV_STATUS"] = invInfo.LKInvestigationStatusTypeID;
                    }
                    else
                    {
                        return RedirectToAction("Error");
                    }
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("TASKController", "Managedistributionlist(int invid=0)", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }

            return View(ManageDisList);
        }
        /// <summary>
        /// Add new  distribution list based on the particular investigation 
        /// </summary> 
        /// <param name="DistId">Receive investigation distribution list id</param>
        /// <param name="InvId">Receive investigation file id</param>
        public ActionResult Addnewdistributionlist(int InvId = 0, int DistId = 0)
        {
            InvDistributionList invDistribution = new InvDistributionList();

            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 3 || Convert.ToInt32(Session["RoleId"]) == 6 || Convert.ToInt32(Session["RoleId"]) == 7)
                {
                    var CheckInvPermission = dc.InvestigationIICAssignments.AsEnumerable().Where(a => a.InvestigationFileID == InvId).FirstOrDefault();

                    if ((CheckInvPermission.IICAssigned == Convert.ToInt32(Session["UserId"]) &&

                                           CheckInvPermission.InvestigationFileID == InvId) || Convert.ToInt32(Session["RoleId"]) == 3 || Convert.ToInt32(Session["RoleId"]) == 6 || Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 7)
                    {
                        TempData["BackPage"] = "man_dist";
                        TempData["active_url_Managedistributionlist"] = "tm";

                        TempData["active_url_Managedistributionlist_iic"] = "iic";
                        TempData["lf_menu"] = "1";

                        var InvInfo = dc.USP_VIEW_INVESTIGATION_INVESTIGATIONID(InvId).FirstOrDefault();

                        invDistribution.InvestigationFileID = InvInfo.InvestigationFileID;

                        TempData["PageHead"] = InvInfo.InvestigationFileNumber;

                        Session["InvFileID"] = InvInfo.InvestigationFileID;

                        TempData["InvId"] = InvInfo.InvestigationFileID;
                        TempData["InvFileID"] = InvInfo.InvestigationFileID;



                        invDistribution.NotifierName = InvInfo.NotifierName;

                        invDistribution.NotifierOrganization = InvInfo.NotifierOrganization;

                        invDistribution.LocalDateAndTimeOfOccurrence = InvInfo.LocalDateAndTimeOfOccurrence.ToString();

                        invDistribution.UTCDateAndTimeOfOccurrence = InvInfo.UTCDateAndTimeOfOccurrence.ToString();

                        if (DistId > 0)
                        {
                            invDistribution.EmailDistributionListID = DistId;

                            var EmailDisInfo = dc.EmailDistributionLists.AsQueryable().Where(a => a.EmailDistributionListID == DistId).FirstOrDefault();

                            invDistribution.EmailDistributionListName = EmailDisInfo.EmailDistributionListName;

                            invDistribution.EmailDistributionListDescription = EmailDisInfo.EmailDistributionListDescription;

                            var UserInfo = (from didlist in dc.EmailDistrubutionListRecipients

                                                //join usr in dc.Users on didlist.UserID equals usr.UserID

                                            where didlist.EmailDistrubutionListID == DistId

                                            //orderby didlist.UserID

                                            select new { didlist.RecipientName });

                            SelectList User_Info = new SelectList(UserInfo, "RecipientName", "RecipientName");

                            ViewBag.User_Info = User_Info;
                        }
                        else
                        {
                            invDistribution.Recipients_Info = null;

                            invDistribution.EmailDistributionListID = 0;

                            ViewBag.User_Info = null;
                        }
                    }
                    else
                    {
                        return RedirectToAction("Error");
                    }
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("TASKController", "Addnewdistributionlist(int invid=0,int DistId=0)", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }

            return View(invDistribution);
        }
        /// <summary>
        /// Delete  distribution list by distribution list id based on the investigation
        /// </summary> 
        /// 
        /// <param name="DistId">Receive investigation distribution list id</param>
        public ActionResult DeleteDistributionlist(Int32 DistId)
        {
            string results = string.Empty;

            int? InvestigationId = 0;

            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 7)
                {
                    if (Convert.ToInt32(Session["UserId"]) > 0)
                    {
                        if (Convert.ToInt32(DistId) > 0)
                        {
                            var OldRecipientsList = dc.EmailDistrubutionListRecipients.Where(a => a.EmailDistrubutionListID == DistId).ToList();

                            var OldDistList = dc.EmailDistributionLists.Where(a => a.EmailDistributionListID == DistId).ToList();

                            InvestigationId = OldDistList.SingleOrDefault().InvestigationFileID;

                            if (OldRecipientsList != null)
                            {
                                foreach (var i in OldRecipientsList)
                                {
                                    dc.EmailDistrubutionListRecipients.Remove(i);
                                }

                                foreach (var j in OldDistList)
                                {
                                    dc.EmailDistributionLists.Remove(j);
                                }

                                dc.SaveChanges();
                            }

                            results = "success";
                        }
                    }
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("TaskController", "DeleteDistributionlist", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }

            return RedirectToAction("Managedistributionlist", "Task", new { InvId = InvestigationId });

        }
        /// <summary>
        /// Save new  distribution list
        /// </summary> 
        /// 
        /// <param name="invDistribution">Receive investigation distribution list details as object</param>
        [HttpPost]
        public ActionResult Addnewdistributionlist(InvDistributionList invDistribution)
        {
            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 7)
                {
                    TempData["BackPage"] = "man_dist";
                    TempData["active_url_Managedistributionlist"] = "tm";
                    TempData["active_url_Managedistributionlist_iic"] = "iic";

                    TempData["lf_menu"] = "1";

                    var result = 0;

                    if (Convert.ToInt32(Session["UserId"]) > 0)
                    {
                        if (invDistribution.EmailDistributionListID > 0)
                        {
                            result = dc.USP_INST_INVESTIGATION_EMAILDISTRIBUTIONLIST(invDistribution.EmailDistributionListID, invDistribution.InvestigationFileID, 2, invDistribution.EmailDistributionListName,

                                     invDistribution.EmailDistributionListDescription, Convert.ToInt32(Session["UserId"]));

                            if (result >= 1)
                            {
                                var OldRecipientsList = dc.EmailDistrubutionListRecipients.Where(a => a.EmailDistrubutionListID == invDistribution.EmailDistributionListID).ToList();

                                if (OldRecipientsList != null)
                                {
                                    foreach (var i in OldRecipientsList)
                                    {
                                        dc.EmailDistrubutionListRecipients.Remove(i);
                                    }

                                    dc.SaveChanges();
                                }

                                if (invDistribution.Recipients_Info != null)
                                {
                                    string[] Recip_Info = invDistribution.Recipients_Info;

                                    foreach (string recipName in Recip_Info)
                                    {
                                        var user_Info = dc.Users.AsQueryable().Where(a => a.FirstName == recipName).FirstOrDefault();

                                        string EmailAddress = string.Empty; int userId = 0;

                                        if (user_Info != null)
                                        {
                                            EmailAddress = user_Info.EmailAddress;

                                            userId = user_Info.UserID;
                                        }
                                        else
                                        {
                                            EmailAddress = recipName;
                                        }

                                        if (recipName != null)
                                        {
                                            result = dc.USP_INST_INVESTIGATION_EMAILDISTRIBUTIONLIST_RECEPIENT(0, invDistribution.EmailDistributionListID,

                                               userId, EmailAddress, Convert.ToInt32(Session["UserId"]), recipName);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            result = dc.USP_INST_INVESTIGATION_EMAILDISTRIBUTIONLIST(0, invDistribution.InvestigationFileID, 2, invDistribution.EmailDistributionListName,

                                      invDistribution.EmailDistributionListDescription, Convert.ToInt32(Session["UserId"]));

                            invDistribution.EmailDistributionListID = dc.EmailDistributionLists.DefaultIfEmpty().Max(a => a.EmailDistributionListID);

                            if (result >= 1)
                            {
                                if (invDistribution.Recipients_Info != null)
                                {
                                    string[] Recip_Info = null;

                                    if (invDistribution.Recipients_Info[0].ToString().Contains(','))
                                    {
                                        Recip_Info = invDistribution.Recipients_Info[0].ToString().Split(',');
                                    }
                                    else
                                    {
                                        Recip_Info = invDistribution.Recipients_Info;
                                    }

                                    foreach (string recipName in Recip_Info)
                                    {
                                        var user_Info = dc.Users.AsQueryable().Where(a => a.FirstName == recipName).FirstOrDefault();

                                        string EmailAddress = string.Empty; int userId = 0;

                                        if (user_Info != null)
                                        {
                                            EmailAddress = user_Info.EmailAddress;

                                            userId = user_Info.UserID;
                                        }
                                        else
                                        {
                                            EmailAddress = recipName;
                                        }

                                        if (recipName != null)
                                        {
                                            result = dc.USP_INST_INVESTIGATION_EMAILDISTRIBUTIONLIST_RECEPIENT(0, invDistribution.EmailDistributionListID,

                                            userId, EmailAddress, Convert.ToInt32(Session["UserId"]), recipName);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("TASKController", "Addnewdistributionlist(int invid=0)", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }

            return RedirectToAction("Managedistributionlist", "Task", new { InvId = invDistribution.InvestigationFileID });
        }
        /// <summary>
        /// View close investigation by id
        /// </summary> 
        /// 
        /// <param name="Id">Receive investigation Clouser id</param>
        public ActionResult CloseInvestigation(int Id = 0) // Id => Investigation ClouserId
        {
            InvestigationModel _InvModel = new InvestigationModel();

            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 3 || Convert.ToInt32(Session["RoleId"]) == 7)
                {
                    var InvClouderDetails = dc.InvestigationClosureDetails.Where(a => a.InvestigationClosureDetailID == Id).FirstOrDefault();

                    var InvInfo = dc.USP_VIEW_INVESTIGATION_INVESTIGATIONID(InvClouderDetails.InvestigationFileID).FirstOrDefault();

                    var InvCloseRecommendebByInfo = dc.Users.Where(a => a.UserID == InvClouderDetails.ClosureRecommendedBy).FirstOrDefault();

                    Session["InvFileID"] = InvInfo.InvestigationFileID;

                    TempData["InvId"] = InvInfo.InvestigationFileID;

                    _InvModel.InvestigationFileID = InvInfo.InvestigationFileID;

                    _InvModel.NotifierName = InvInfo.NotifierName;

                    _InvModel.NotifierOrganization = InvInfo.NotifierOrganization;

                    _InvModel.LocalDateAndTimeOfOccurrence = InvInfo.LocalDateAndTimeOfOccurrence;

                    _InvModel.UTCDateAndTimeOfOccurrence = InvInfo.UTCDateAndTimeOfOccurrence;

                    _InvModel.InvestigationFileNumber = InvInfo.InvestigationFileNumber.ToString();

                    _InvModel.InvestigationClosureReason = InvClouderDetails.InvestigationClosureReason.ToString();

                    _InvModel.InvestigationClosureRecommendedBy = InvCloseRecommendebByInfo.FirstName.ToString();

                    _InvModel.InvestigationClosureRecommendedDate = InvClouderDetails.DateCreated.ToString("dd/MMM/yyyy");

                    _InvModel.InvestigationClosureDetailID = InvClouderDetails.InvestigationClosureDetailID;

                    var Inv_ClosureStatus = dc.USP_GET_MASTERVALUES(21);
                    SelectList InvClosureStatus = new SelectList(Inv_ClosureStatus, "ID", "LKVALUE");
                    ViewBag.ClosureStatus = InvClosureStatus;
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("TaskController", "CloseInvestigation(int InvId = 0)", "Message : " + "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }

            return View(_InvModel);
        }
        /// <summary>
        /// Save close investigation status by investigation file id
        /// </summary> 
        /// 
        /// <param name="InvModel">Receive investigation file details</param>
        [HttpPost]
        public ActionResult CloseInvestigation(InvestigationModel InvModel)
        {
            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 3 || Convert.ToInt32(Session["RoleId"]) == 7)
                {
                    if (Convert.ToInt32(Session["UserId"]) > 0)
                    {
                        if (InvModel.InvestigationClosureStatusId == 0)
                        {
                            #region Save Close Investigation

                            var Result = dc.USP_INST_INVESTIGATION_CLOSUREDETAIL(InvModel.InvestigationFileID, InvModel.InvestigationClosureReason, Convert.ToInt32(Session["UserId"]),

                                         0, 0, Convert.ToInt32(Session["UserId"]), 0, 0);

                            if (Result > 0)
                            {
                                List<string> ListUsers = new List<string>();

                                List<string> ListUsersEmail = new List<string>();

                                int UserId = Convert.ToInt32(Session["UserId"]);

                                var TeammberInfo = dc.Users.AsQueryable().Where(a => a.UserRoleID == 3).FirstOrDefault();

                                var InvInfo = dc.InvestigationFiles.AsQueryable().Where(a => a.InvestigationFileID == InvModel.InvestigationFileID).FirstOrDefault();

                                var LoginUserInfo = dc.Users.AsQueryable().Where(a => a.UserID == UserId).FirstOrDefault();

                                ListUsersEmail.Add(TeammberInfo.EmailAddress.ToString());

                                //ListUsersEmail.Add("ismdirector1@gmail.com");

                                ListUsers.Add(TeammberInfo.FirstName.ToString());

                                string siteURL = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));

                                bool IsSend = mail.SendMailNotification(11, LoginUserInfo.EmailAddress, LoginUserInfo.UserRoleID, siteURL, ListUsers, ListUsersEmail, InvInfo.InvestigationFileID,

                                    null, InvInfo.InvestigationFileNumber, null, "", "");
                            }

                            return RedirectToAction("IICTasks", "IIC", new { id = InvModel.InvestigationFileID });

                            #endregion
                        }
                        else
                        {
                            #region Update Close Investigation

                            var Result = dc.USP_INST_INVESTIGATION_CLOSUREDETAIL(InvModel.InvestigationFileID, InvModel.InvestigationClosureReason, Convert.ToInt32(Session["UserId"]),

                                         Convert.ToInt32(Session["UserId"]), InvModel.InvestigationClosureDetailID, Convert.ToInt32(Session["UserId"]), Convert.ToInt32(Session["UserId"]),

                                         InvModel.InvestigationClosureStatusId);

                            if (Result > 0)
                            {
                                List<string> ListUsers = new List<string>();

                                List<string> ListUsersEmail = new List<string>();

                                int UserId = Convert.ToInt32(Session["UserId"]);

                                var IICAssignedInfo = dc.InvestigationIICAssignments.AsEnumerable().Where(a => a.InvestigationFileID == InvModel.InvestigationFileID).FirstOrDefault();

                                var TeammberInfo = dc.Users.AsQueryable().Where(a => a.UserID == IICAssignedInfo.IICAssigned).FirstOrDefault();

                                var InvInfo = dc.InvestigationFiles.AsQueryable().Where(a => a.InvestigationFileID == InvModel.InvestigationFileID).FirstOrDefault();

                                var LoginUserInfo = dc.Users.AsQueryable().Where(a => a.UserID == UserId).FirstOrDefault();

                                ListUsersEmail.Add(TeammberInfo.EmailAddress.ToString());

                                // ListUsersEmail.Add("isminvestigator1@gmail.com");

                                ListUsers.Add(TeammberInfo.FirstName.ToString());

                                var InvClosurStatus = dc.LKCOMMONs.Where(a => a.ID == InvModel.InvestigationClosureStatusId).FirstOrDefault();

                                string siteURL = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));

                                bool IsSend = mail.SendMailNotification(12, LoginUserInfo.EmailAddress, LoginUserInfo.UserRoleID, siteURL, ListUsers, ListUsersEmail, InvInfo.InvestigationFileID,

                                    null, InvInfo.InvestigationFileNumber, null, InvClosurStatus.LKVALUE, "");
                            }

                            return RedirectToAction("Dashboard", "Home");

                            #endregion
                        }
                    }
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("TaskController", "CloseInvestigation(int InvId = 0)", "Message : " + "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }

            return RedirectToAction("IICTasks", "IIC", new { id = InvModel.InvestigationFileID });
        }
        /// <summary>
        ///  DateTime convert
        /// </summary> 
        /// 
        /// <param name="dt">Receive datetime value</param>
        public string convertDateTime(DateTime dt)
        {
            string conv_Time = string.Empty;
            if (dt != null)
                conv_Time = Convert.ToDateTime(dt).ToString("dd-MM-yyyy HH:mm");
            // //conv_Time = Convert.ToDateTime(dt).ToString("yyyy-MM-dd HH:mm:");

            return conv_Time;
        }
        /// <summary>
        ///  DateTime convert
        /// </summary> 
        /// 
        /// <param name="dt">Receive datetime value</param>
        public string convertDate(DateTime dt)
        {
            string conv_Time = string.Empty;
            if (dt != null)
                conv_Time = Convert.ToDateTime(dt).ToString("dd/MM/yyyy");
            //conv_Time = Convert.ToDateTime(dt).ToString("yyyy-MM-dd HH:mm:");
            return conv_Time;
        }
        /// <summary>
        ///  Validate Email id
        /// </summary> 
        /// 
        /// <param name="strIn">Receive email address as string</param>
        public bool IsValidEmail(string strIn)
        {
            // Return true if strIn is in valid email format.
            try
            {
                return Regex.IsMatch(strIn,
                      @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                      @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-0-9a-z]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                      RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }
        /// <summary>
        ///  Validate Recipience Email
        /// </summary> 
        /// 
        /// <param name="RecEmail">Receive email address as string</param>
        public int ValidateRecipienceEmail(string[] RecEmail)
        {
            int result = 1;

            try
            {
                string[] RecEmailAddress = null;

                bool IsSingleValue = RecEmail[0].ToString().Contains(',');

                if (IsSingleValue)
                {
                    RecEmailAddress = RecEmail[0].ToString().Split(',');
                }
                else
                {
                    RecEmailAddress = RecEmail;
                }

                foreach (string RecName in RecEmailAddress)
                {
                    if (RecName != string.Empty)
                    {
                        var user_Info = dc.Users.AsQueryable().Where(a => a.FirstName == RecName).FirstOrDefault();

                        if (user_Info == null)
                        {
                            bool retval = IsValidEmail(RecName);

                            if (!retval)
                            {
                                return result = 0;
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("TaskController", "ValidateRecipienceEmail", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }

            return result;
        }
        /// <summary>
        ///  send external link to External stack holders 
        /// </summary>         
        /// <param name="inv_file_id">Receive investigation file id</param>
        public ActionResult sendexternallink(int inv_file_id = 0)
        {
            var Ext_holder = new ExternalStake();

            try
            {

                Ext_holder = new ExternalStake()
                {
                    Ext_stk_holder = dc.USP_GET_EXTERNAL_STAKE_HOLDERSNAME(inv_file_id).ToList(),
                    Ext_stk_holder_email = dc.USP_GET_EXTERNAL_STAKE_HOLDERSNAME_FOR_EMAIL(inv_file_id).ToList()
                };
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("TaskController", "CloseInvestigation(int InvId = 0)", "Message : " + "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }

            return View(Ext_holder);
        }
        /// <summary>
        /// Save  External Holder Details
        /// </summary> 
        /// <param name="Estake">Receive investigation External Holder Details</param>
        public ActionResult ExternalHolder_Details(ExternalStake Estake)
        {
            try
            {
                Dictionary<int, string> stakeholder = new Dictionary<int, string>();
                Dictionary<int, string> stakeholder_oprator = new Dictionary<int, string>();

                for (int k = 0; k <= Estake.OperatorName.Count(); k++)
                {
                    string email_address = Estake.Email_id[k];
                    string ext_holdername = Estake.OperatorName[k];
                    var userinfo = (from usr in dc.Users
                                    where usr.EmailAddress == email_address
                                    select new
                                    {
                                        usr.UserID,
                                        usr.UserRoleID
                                    }).ToList();
                    var Exholderunqname = (from ext in dc.InvestigationExternalHoldersDetails
                                           where ext.ExternalHolderName == ext_holdername
                                           select new
                                           {
                                               ext.ExternalHolderUniqueName
                                           }).ToList();
                    if (userinfo.Count() <= 0)
                    {


                    }
                    stakeholder.Add(Convert.ToInt32(userinfo[0].UserID), Estake.Email_id[k]);
                    stakeholder_oprator.Add(Convert.ToInt32(userinfo[0].UserID), Convert.ToString(Exholderunqname[0]));
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("TaskController", "CloseInvestigation(int InvId = 0)", "Message : " + "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }
            return View();
        }
        /// <summary>
        /// get Other Values from Event JSON
        /// </summary>
        /// <param name="ExpectedKey">Receive the Expected Key as string array</param>
        /// <param name="JSON"> Receive JSON as string</param>
        /// <param name="AddDictItems">Receive the Add new Dictionary Items</param>
        /// <returns></returns>
        private Dictionary<string, string> getOtherValuesfromEventJSON(string[] ExpectedKey, string JSON, Dictionary<string, string> AddDictItems)
        {
            try
            {
                JObject taskformDetails = JObject.Parse(JSON);
                var values = JsonConvert.DeserializeObject<Dictionary<string, List<string>>>(JSON);
                Dictionary<string, List<string>> keyValues = new Dictionary<string, List<string>>();
                keyValues = values;
                int digcount = 0;
                for (int i = 0; i < ExpectedKey.Length; i++)
                {
                    foreach (KeyValuePair<string, List<string>> NewJson in keyValues)
                    {
                        if (NewJson.Key.Contains(ExpectedKey[i]))
                        {
                            AddDictItems.Add(NewJson.Key, !string.IsNullOrEmpty(Convert.ToString(NewJson.Value[0])) ? (Convert.ToString(NewJson.Value[0])) : "N/A");
                        }

                    }
                    if (ExpectedKey[i].Contains("Diagram"))
                    {
                        if (AddDictItems.Keys.Contains(ExpectedKey[i]))
                        {
                            digcount++;
                            int val = Convert.ToInt32(AddDictItems[ExpectedKey[i]]);
                            var docurl = dc.sp_get_event_attahments_reports(null, val).FirstOrDefault();

                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("FormsController", "getOtherValuesfromEventJSON", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }
            return AddDictItems;
        }
        /// <summary>
        /// get File for document generation 
        /// </summary>
        /// <param name="keyandValues">document input parameter</param>
        /// <param name="keyAircraftValues">document input parameter</param>
        /// <param name="Files">document input parameter</param>
        /// <param name="sourceFile">document input parameter</param>
        /// <param name="destinationFile">document input parameter</param>
        /// <param name="FileType">document input parameter</param>
        /// <returns></returns>
        public MemoryStream getFile(Dictionary<string, string> keyandValues, Dictionary<string, List<string>> keyAircraftValues, dynamic Files, string sourceFile, string destinationFile, string FileType)
        {
            MemoryStream mem = new MemoryStream();
            try
            {
                int noofaircrafts = 0;
                MemoryStream documentStream;
                if (new FileInfo(sourceFile).Length > 0)
                {
                    using (Stream tplStream = System.IO.File.OpenRead(sourceFile))
                    {
                        documentStream = new MemoryStream((int)tplStream.Length);
                        CopyStream(tplStream, documentStream);
                        documentStream.Position = 0L;
                    }
                    if (documentStream.Length > 0)
                    {
                        using (WordprocessingDocument template = WordprocessingDocument.Open(documentStream, true))
                        {
                            template.ChangeDocumentType(DocumentFormat.OpenXml.WordprocessingDocumentType.Document);
                            MainDocumentPart mainPartS = template.MainDocumentPart;
                            mainPartS.DocumentSettingsPart.AddExternalRelationship("http://schemas.openxmlformats.org/officeDocument/2006/relationships/attachedTemplate", new Uri(sourceFile, UriKind.Absolute));
                            mainPartS.Document.Save();
                        }
                        System.IO.File.WriteAllBytes(destinationFile, documentStream.ToArray());
                    }
                }
                if (new FileInfo(destinationFile).Length > 0)
                {
                    using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(destinationFile, true))
                    {
                        string docText = null;
                        MainDocumentPart mainPart = wordDoc.MainDocumentPart;
                        using (StreamReader sr = new StreamReader(wordDoc.MainDocumentPart.GetStream()))
                        {
                            docText = sr.ReadToEnd();
                        }
                        //replacing text in the original form
                        foreach (KeyValuePair<string, string> item in keyandValues)
                        {
                            //to get the no of aircrafts involved
                            if (item.Key == "NoOfAircraftInvolved")
                                noofaircrafts = Convert.ToInt32(item.Value);
                            Regex regexText = new Regex(item.Key);
                            SearchandReplace.SearchAndReplace(wordDoc, item.Key, item.Value, false);
                        }
                    }
                    if (FileType == "F35")
                    {
                        using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(destinationFile, true))
                        {
                            Table table = wordDoc.MainDocumentPart.Document.Body.Descendants<Table>().FirstOrDefault();
                            if (noofaircrafts > 1)
                            {
                                for (int count = 1; count < noofaircrafts; count++)
                                {
                                    Table tbl = new Table(table.CloneNode(true));
                                    wordDoc.MainDocumentPart.Document.Body.InsertAfter(tbl, table);
                                    wordDoc.MainDocumentPart.Document.Save();
                                }
                            }
                            int tblCount = wordDoc.MainDocumentPart.Document.Body.Elements<Table>().Count();
                            for (int ii = 0; ii < tblCount; ii++)
                            {
                                Table tbll = wordDoc.MainDocumentPart.Document.Body.Elements<Table>().ElementAt(ii);
                                foreach (TableRow row in tbll.Descendants<TableRow>())
                                {
                                    foreach (TableCell cell in row.Elements<TableCell>())
                                    {
                                        foreach (var para in cell.Elements<Paragraph>())
                                        {
                                            foreach (var run in para.Elements<Run>())
                                            {

                                                foreach (var text in run.Elements<Text>())
                                                {
                                                    string count = Convert.ToString(ii + 1);

                                                    foreach (var item in keyAircraftValues)
                                                    {
                                                        if (text.Text == item.Key)
                                                        {
                                                            text.Text = (!string.IsNullOrEmpty(Convert.ToString(item.Value[ii])) ? item.Value[ii] : "N/A");
                                                            break;
                                                        }

                                                    }
                                                    if (text.Text.Trim() == "ii")
                                                        text.Text = "Aircraft - " + count;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            if (Files != null)
                            {
                                Table Filetable = createTable(Files, wordDoc);

                                includeContent(Filetable, "FilesAttach", wordDoc);
                            }
                        }
                    }

                    if (FileType == "F39")
                    {
                        using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(destinationFile, true))
                        {
                            Table table = wordDoc.MainDocumentPart.Document.Body.Descendants<Table>().ElementAt(1);
                            if (noofaircrafts > 1)
                            {
                                for (int count = 1; count < noofaircrafts; count++)
                                {
                                    Table tbl = new Table(table.CloneNode(true));
                                    wordDoc.MainDocumentPart.Document.Body.InsertAfter(tbl, table);
                                    wordDoc.MainDocumentPart.Document.Save();
                                }
                            }
                            string Team = string.Empty;
                            if (Files != null)
                            {
                                if (Files.Count > 0)
                                {
                                    foreach (var files in Files)
                                    {
                                        if (files.UsrType == "TM")
                                        {
                                            if (string.IsNullOrEmpty(Team))
                                                Team = files.FirstName;
                                            else
                                                Team += "," + files.FirstName;
                                        }
                                    }
                                }
                                else
                                {
                                    Team = "N/A";
                                }
                                //    SearchandReplace.SearchAndReplace(wordDoc, tosearch, Team, false);
                            }
                            else
                                Team = "N/A";
                            Table MainTable = wordDoc.MainDocumentPart.Document.Body.Descendants<Table>().FirstOrDefault();
                            foreach (TableRow row in MainTable.Descendants<TableRow>())
                            {
                                foreach (TableCell cell in row.Elements<TableCell>())
                                {
                                    foreach (var para in cell.Elements<Paragraph>())
                                    {
                                        foreach (var run in para.Elements<Run>())
                                        {

                                            foreach (var text in run.Descendants<Text>())
                                            {
                                                if (text.Text == "MemberName")
                                                {
                                                    text.Text = Team;
                                                    break;
                                                }
                                            }
                                        }
                                        break;
                                    }
                                }
                            }
                            int tblCount = wordDoc.MainDocumentPart.Document.Body.Elements<Table>().Count();
                            for (int ii = 1; ii < tblCount; ii++)
                            {
                                Table tbll = wordDoc.MainDocumentPart.Document.Body.Elements<Table>().ElementAt(ii);
                                foreach (TableRow row in tbll.Descendants<TableRow>())
                                {
                                    foreach (TableCell cell in row.Elements<TableCell>())
                                    {
                                        foreach (var para in cell.Elements<Paragraph>())
                                        {
                                            foreach (var run in para.Elements<Run>())
                                            {

                                                foreach (var text in run.Elements<Text>())
                                                {
                                                    string count = Convert.ToString(ii);

                                                    foreach (var item in keyAircraftValues)
                                                    {
                                                        if (text.Text == item.Key)
                                                        {
                                                            text.Text = (!string.IsNullOrEmpty(Convert.ToString(item.Value[ii - 1])) ? item.Value[ii - 1] : "N/A");
                                                            break;
                                                        }

                                                    }
                                                    if (text.Text.Trim() == "ii")
                                                        text.Text = "Aircraft - " + count;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (FileType == "F43")
                    {
                        using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(destinationFile, true))
                        {
                            Table table = wordDoc.MainDocumentPart.Document.Body.Descendants<Table>().ElementAt(2);
                            if (noofaircrafts > 1)
                            {
                                for (int count = 1; count < noofaircrafts; count++)
                                {
                                    Table tbl = new Table(table.CloneNode(true));
                                    wordDoc.MainDocumentPart.Document.Body.InsertAfter(tbl, table);
                                    wordDoc.MainDocumentPart.Document.Save();
                                }
                            }
                            int tblCount = wordDoc.MainDocumentPart.Document.Body.Elements<Table>().Count();
                            for (int ii = 1; ii < tblCount; ii++)
                            {
                                Table tbll = wordDoc.MainDocumentPart.Document.Body.Elements<Table>().ElementAt(ii);
                                foreach (TableRow row in tbll.Descendants<TableRow>())
                                {
                                    foreach (TableCell cell in row.Elements<TableCell>())
                                    {
                                        foreach (var para in cell.Elements<Paragraph>())
                                        {
                                            foreach (var run in para.Elements<Run>())
                                            {
                                                foreach (var text in run.Elements<Text>())
                                                {
                                                    string count = Convert.ToString(ii);

                                                    foreach (var item in keyAircraftValues)
                                                    {
                                                        if (text.Text == item.Key)
                                                        {
                                                            text.Text = (!string.IsNullOrEmpty(Convert.ToString(item.Value[ii - 1])) ? item.Value[ii - 1] : "N/A");
                                                            break;
                                                        }
                                                    }
                                                    if (text.Text.Trim() == "ii")
                                                        text.Text = "Aircraft - " + count;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (FileType == "Rep-PRE")
                    {
                        using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(destinationFile, true))
                        {
                            IEnumerable<TableProperties> tableProperties = wordDoc.MainDocumentPart.Document.Body.Descendants<TableProperties>().Where(tp => tp.TableCaption != null);

                            foreach (TableProperties tProp in tableProperties)
                            {
                                if (tProp.TableCaption.Val == "AircraftDetails") // see comment, this is actually StringValue
                                {
                                    Table table = (Table)tProp.Parent;
                                    //Table table = wordDoc.MainDocumentPart.Document.Body.Descendants<Table>().ElementAt(1);
                                    if (noofaircrafts > 1)
                                    {
                                        for (int count = 1; count < noofaircrafts; count++)
                                        {
                                            Table tbl = new Table(table.CloneNode(true));
                                            wordDoc.MainDocumentPart.Document.Body.InsertAfter(tbl, table);
                                            wordDoc.MainDocumentPart.Document.Save();
                                        }
                                    }
                                    break;
                                }
                            }
                            foreach (TableProperties tProp in tableProperties)
                            {
                                if (tProp.TableCaption.Val == "Injuriespersons") // see comment, this is actually StringValue
                                {
                                    Table table = (Table)tProp.Parent;
                                    //Table table = wordDoc.MainDocumentPart.Document.Body.Descendants<Table>().ElementAt(1);
                                    if (noofaircrafts > 1)
                                    {
                                        for (int count = 1; count < noofaircrafts; count++)
                                        {
                                            Table tbl = new Table(table.CloneNode(true));
                                            wordDoc.MainDocumentPart.Document.Body.InsertAfter(tbl, table);
                                            wordDoc.MainDocumentPart.Document.Save();
                                        }
                                    }
                                    break;
                                }
                            }
                            int tblCount = wordDoc.MainDocumentPart.Document.Body.Elements<Table>().Count();
                            int airtbcount = -1;//for getting 0th array in dictionary value
                            int airtable = 1; int injpertable = 1; int injpertablecount = -1;

                            for (int ii = 0; ii < tblCount; ii++)
                            {
                                Table tbll = wordDoc.MainDocumentPart.Document.Body.Elements<Table>().ElementAt(ii);
                                IEnumerable<TableProperties> tableProperties1 = tbll.Descendants<TableProperties>().Where(tp => tp.TableCaption != null);

                                foreach (TableProperties tProp in tableProperties1)
                                {
                                    if (tProp.TableCaption.Val == "AircraftDetails") // see comment, this is actually StringValue
                                    {
                                        airtbcount++;
                                        foreach (TableRow row in tbll.Descendants<TableRow>())
                                        {
                                            foreach (TableCell cell in row.Elements<TableCell>())
                                            {
                                                foreach (var para in cell.Elements<Paragraph>())
                                                {
                                                    foreach (var run in para.Elements<Run>())
                                                    {
                                                        foreach (var text in run.Elements<Text>())
                                                        {
                                                            string count = Convert.ToString(airtable);

                                                            foreach (var item in keyAircraftValues)
                                                            {
                                                                if (text.Text == item.Key)
                                                                {
                                                                    text.Text = (!string.IsNullOrEmpty(Convert.ToString(item.Value[airtbcount])) ? item.Value[airtbcount] : "N/A");
                                                                    break;
                                                                }
                                                            }
                                                            if (text.Text.Trim() == "ii")
                                                                text.Text = "Aircraft - " + count;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        airtable++;
                                        break;
                                    }
                                    if (tProp.TableCaption.Val == "Injuriespersons") // see comment, this is actually StringValue
                                    {
                                        injpertablecount++;
                                        foreach (TableRow row in tbll.Descendants<TableRow>())
                                        {
                                            foreach (TableCell cell in row.Elements<TableCell>())
                                            {
                                                foreach (var para in cell.Elements<Paragraph>())
                                                {
                                                    foreach (var run in para.Elements<Run>())
                                                    {
                                                        foreach (var text in run.Elements<Text>())
                                                        {
                                                            string count = Convert.ToString(injpertable);

                                                            foreach (var item in keyAircraftValues)
                                                            {
                                                                if (text.Text == item.Key)
                                                                {
                                                                    text.Text = (!string.IsNullOrEmpty(Convert.ToString(item.Value[injpertablecount])) ? item.Value[injpertablecount] : "N/A");
                                                                    break;
                                                                }
                                                            }
                                                            if (text.Text.Trim() == "ii")
                                                                text.Text = "Aircraft - " + count;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        injpertable++;
                                        break;
                                    }

                                }
                            }

                            Drawing image = new Drawing();
                            for (int iii = 0; iii < attachments.Length; iii++)
                            {
                                if (!string.IsNullOrEmpty(attachments[iii]))
                                {
                                    ImagePart imgfootPart = wordDoc.MainDocumentPart.AddImagePart(ImagePartType.Jpeg);
                                    using (FileStream stream = new FileStream(attachments[iii], FileMode.Open))
                                    {
                                        if (stream != null)
                                        {
                                            imgfootPart.FeedData(stream);
                                            image = AddImageToBody(wordDoc.MainDocumentPart.GetIdOfPart(imgfootPart), attachments[iii]);
                                            includeContent(image, "FG2", wordDoc);
                                        }
                                    }
                                }
                            }
                        }
                        using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(destinationFile, true))
                        {
                            XElement ToC = wordDoc
                     .MainDocumentPart
                     .GetXDocument()
                     .Descendants(W.p).Descendants(W.r).Descendants(W.t)
                     .Where(z => z.Value == "TOC1")
                    .FirstOrDefault();
                            ToCAdder.AddToc(wordDoc, ToC,
                                                    @"TOC \o '1-3' \h \z \u", "Table of Contents", null);
                        }
                    }
                }

                //  SetRunFont(destinationFile);
                mem = new MemoryStream(System.IO.File.ReadAllBytes(destinationFile));
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("FormsController", "getFile", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }
            return mem;
        }
        /// <summary>
        /// Add Image To Body for document generation 
        /// </summary>
        /// <param name="relationshipId">document input parameter</param>
        /// <param name="filename">document input parameter</param>
        /// <returns></returns>
        private static Drawing AddImageToBody(string relationshipId, string filename)
        {
            // Define the reference of the image.

            var element =
                 new Drawing(
                     new DW.Inline(
                         new DW.Extent() { Cx = 990000L, Cy = 792000L },
                         new DW.EffectExtent()
                         {
                             LeftEdge = 0L,
                             TopEdge = 0L,
                             RightEdge = 0L,
                             BottomEdge = 0L
                         },
                         new DW.DocProperties()
                         {
                             Id = (UInt32Value)1U,
                             Name = "Picture 1"
                         },
                         new DW.NonVisualGraphicFrameDrawingProperties(
                             new A1.GraphicFrameLocks() { NoChangeAspect = true }),
                         new A1.Graphic(
                             new A1.GraphicData(
                                 new PIC.Picture(
                                     new PIC.NonVisualPictureProperties(
                                         new PIC.NonVisualDrawingProperties()
                                         {
                                             Id = (UInt32Value)0U,
                                             Name = "New Bitmap Image.jpg"
                                         },
                                         new PIC.NonVisualPictureDrawingProperties()),
                                     new PIC.BlipFill(
                                         new A1.Blip(
                                             new A1.BlipExtensionList(
                                                 new A1.BlipExtension()
                                                 {
                                                     Uri =
                                                       "{28A0092B-C50C-407E-A947-70E740481C1C}"
                                                 })
                                         )
                                         {
                                             Embed = relationshipId,
                                             CompressionState = A1.BlipCompressionValues.Print
                                         },
                                         new A1.Stretch(
                                             new A1.FillRectangle())),
                                     new PIC.ShapeProperties(
                                         new A1.Transform2D(
                                             new A1.Offset() { X = 0L, Y = 0L },
                                             new A1.Extents() { Cx = 990000L, Cy = 792000L }),
                                         new A1.PresetGeometry(
                                             new A1.AdjustValueList()
                                         )
                                         { Preset = A1.ShapeTypeValues.Rectangle }))
                             )
                             { Uri = "http://schemas.openxmlformats.org/drawingml/2006/picture" })
                     )
                     {
                         DistanceFromTop = (UInt32Value)114300U,
                         DistanceFromBottom = (UInt32Value)0U,
                         DistanceFromLeft = (UInt32Value)114300U,
                         DistanceFromRight = (UInt32Value)0U,
                         EditId = "50D07946"
                     });



            return element;
        }
        /// <summary>
        /// Copy Stream for document generation 
        /// </summary>
        /// <param name="source">document input parameter</param>
        /// <param name="target">document input parameter</param>
        public static void CopyStream(Stream source, Stream target)
        {
            if (source != null)
            {
                MemoryStream mstream = source as MemoryStream;
                if (mstream != null) mstream.WriteTo(target);
                else
                {
                    byte[] buffer = new byte[2048];
                    int length = buffer.Length, size;
                    while ((size = source.Read(buffer, 0, length)) != 0)
                        target.Write(buffer, 0, size);
                }
            }
        }
        /// <summary>
        /// create Table
        /// </summary>
        /// <param name="Files">document input parameter</param>
        /// <param name="doc">document input parameter</param>
        /// <returns></returns>
        private static Table createTable(dynamic Files, WordprocessingDocument doc)
        {
            Table table = new Table();

            // Create a TableProperties object and specify its border information.
            TableProperties tblProp = new TableProperties(
                new TableBorders(
                    new TopBorder()
                    {
                        Val =
                        new EnumValue<BorderValues>(BorderValues.Single),
                        Size = 10
                    },
                    new BottomBorder()
                    {
                        Val =
                        new EnumValue<BorderValues>(BorderValues.Single),
                        Size = 10
                    },
                    new LeftBorder()
                    {
                        Val =
                        new EnumValue<BorderValues>(BorderValues.Single),
                        Size = 10
                    },
                    new RightBorder()
                    {
                        Val =
                        new EnumValue<BorderValues>(BorderValues.Single),
                        Size = 10
                    },
                    new InsideHorizontalBorder()
                    {
                        Val =
                        new EnumValue<BorderValues>(BorderValues.Single),
                        Size = 10
                    },
                    new InsideVerticalBorder()
                    {
                        Val =
                        new EnumValue<BorderValues>(BorderValues.Single),
                        Size = 10
                    }
                )
            );

            // Append the TableProperties object to the empty table.
            table.AppendChild<TableProperties>(tblProp);

            if (Files.Count > 0)
            {
                int file_Count = 0;
                foreach (var file in Files)
                {
                    file_Count++;
                    var tr = new TableRow();
                    var tc = new TableCell();
                    tc.Append(new TableCellProperties(new TableCellWidth { Type = TableWidthUnitValues.Dxa, Width = "4650" }));
                    tc.Append(new Paragraph(new Run(new Text(Convert.ToString(file_Count)))));
                    tr.Append(tc);
                    string filename = Convert.ToString(file.AttachmentPublicUrl);
                    filename = filename.Substring(filename.LastIndexOf('/') + 1);
                    var tc1 = new TableCell();
                    HyperlinkRelationship rel = doc.MainDocumentPart.AddHyperlinkRelationship(new Uri(file.AttachmentPublicUrl), true);
                    string relationshipId = rel.Id;
                    tc1.Append(new TableCellProperties(new TableCellWidth { Type = TableWidthUnitValues.Dxa, Width = "4650" }));
                    tc1.Append(new Paragraph(new Hyperlink(new Run(new Text(filename))) { History = OnOffValue.FromBoolean(true), Id = relationshipId }));
                    tr.Append(tc1);
                    table.Append(tr);
                }
            }
            else
            {
                var tr = new TableRow();
                var tc = new TableCell();
                tc.Append(new TableCellProperties(new TableCellWidth { Type = TableWidthUnitValues.Dxa, Width = "4650" }));
                tc.Append(new Paragraph(new Run(new Text(Convert.ToString("No attachments available")))));
                tr.Append(tc);
                table.Append(tr);
            }
            // Append the table to the document.
            return table;
        }
        /// <summary>
        /// include Content document generation 
        /// </summary> 
        /// 
        ///<param name="content">Receive investigation  File content</param>
        ///<param name="findText">Receive search key</param>
        ///<param name="doc">Receive investigation Word processing Document</param>
        public void includeContent(dynamic content, string findText, WordprocessingDocument doc)
        {
            var body = doc.MainDocumentPart.Document.Body;
            var paras = body.Elements<Paragraph>();
            foreach (var para in paras)
            {
                foreach (var run in para.Elements<Run>())
                {
                    foreach (var text in run.Elements<Text>())
                    {
                        if (text.Text.Contains(findText))
                        {
                            text.RemoveAllChildren();
                            // text.Text = "found text but not replaced";
                            run.Append(content);
                            run.Append(new Break());
                            run.Append(new Break());

                            if (content.GetType().Name == "Drawing")
                            {

                                Paragraph para2 = body.AppendChild(new Paragraph());
                                Run run2 = para.AppendChild(new Run());
                                run2.AppendChild(new Text { Text = " ", Space = SpaceProcessingModeValues.Preserve });

                                RunProperties runProperties = run2.AppendChild(new RunProperties());

                                FontSize fontSize = new FontSize();

                                fontSize.Val = "22";
                                runProperties.Color = new DocumentFormat.OpenXml.Wordprocessing.Color() { Val = "1F497D" };
                                RunFonts font1 = new RunFonts() { Ascii = "Calibri (Body)" };
                                runProperties.AppendChild(fontSize);
                                runProperties.Append(font1);
                                Bold bold = new Bold();

                                bold.Val = OnOffValue.FromBoolean(true);

                                runProperties.AppendChild(bold);
                                run2.AppendChild(new Text("Figure 1: The Clipboard Tab"));
                            }
                            break;
                        }
                    }
                    break;
                }
            }
        }
        /// <summary>
        /// Evidence File upload
        /// </summary> 
        /// 
        [HttpPost]
        public JsonResult EvidenceFileupload()
        {
            try
            {
                comm.CreateIfMissing(Server.MapPath("~/") + "App_Data/Evidence_Files/" + investigation_fileid);

                foreach (string file in Request.Files)
                {
                    var fileContent = Request.Files[file];

                    if (fileContent != null && fileContent.ContentLength > 0)
                    {
                        var stream = fileContent.InputStream;

                        var fileName = Path.GetFileName(file);

                        var path = Path.Combine(Server.MapPath("~/App_Data/Evidence_Files/" + investigation_fileid), fileContent.FileName);

                        using (var fileStream = System.IO.File.Create(path))
                        {
                            stream.CopyTo(fileStream);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("", "", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return Json("Upload failed");
            }

            return Json("File uploaded successfully");
        }
        /// <summary>
        /// Remove attached evidence file
        /// </summary> 
        /// 
        /// <param name="filename">Receive investigation Evidence Attached File name</param>
        [HttpPost]
        public JsonResult RemoveEvidenceAttachedFile(string filename)
        {
            string result = "failed";

            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 4)
                {
                    if (Convert.ToInt32(Session["UserId"]) > 0)
                    {
                        string[] fName = filename.Split('\\');

                        if (fName.Count() >= 2)
                        {
                            var fullpathoffile = Server.MapPath("~/App_Data/Evidence_Files/" + investigation_fileid + "/" + fName[2]);

                            if (System.IO.File.Exists(fullpathoffile))
                            {
                                System.IO.File.Delete(fullpathoffile);
                            }

                            result = "success";
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("Task", "RemoveEvidenceAttachedFile", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }

            return Json(result);
        }

        [HttpPost]
        public ActionResult ReopenInvestigation(InvestigationModel InvModel)
        {
            try
            {
                int invClosureDetailsId = dc.InvestigationClosureDetails.Where(x => x.InvestigationFileID == InvModel.InvestigationFileID).FirstOrDefault().InvestigationClosureDetailID;
                InvModel.InvestigationClosureDetailID = invClosureDetailsId;
                if (Convert.ToInt32(Session["RoleId"]) == 3)
                {
                    if (Convert.ToInt32(Session["UserId"]) > 0)
                    {
                        var Result = dc.USP_INST_INVESTIGATION_CLOSUREDETAIL(InvModel.InvestigationFileID, InvModel.InvestigationReopenReason, Convert.ToInt32(Session["UserId"]),

                                        Convert.ToInt32(Session["UserId"]), InvModel.InvestigationClosureDetailID, Convert.ToInt32(Session["UserId"]), Convert.ToInt32(Session["UserId"]),

                                        InvModel.InvestigationClosureStatusId);
                        return RedirectToAction("Dashboard", "Home");

                    }
                }

            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("Task", "ReopenInvestigation", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }
            return RedirectToAction("IICTasks", "IIC", new { id = InvModel.InvestigationFileID });
        }
    }
}