﻿using ISM_Project.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace ISM_Project.Controllers
{
    /// <summary>
    /// SR - Controller contains various process of the investigation safety recommendation work flow and print SR details as PDF
    /// </summary> 
    /// 
    public class SRController : Controller
    {
        Common comm = new Common();
        MailConfiguration mail = new MailConfiguration();
        gcaa_ismEntities db = new gcaa_ismEntities();
        PdfGeneration pdf = new PdfGeneration();
        static string[] SharepointfilePath = new string[100];
        static string[] SharepointAnonymousPath = new string[100];
        static int[] DocumentTypeId = new int[100];
        static int InvFileId = 0;
        
        /// <summary>
        /// Create new  safety recommendation the following action method will perform
        /// </summary> 
        /// 
        /// <param name="id">Receive the investigation file id of the particular investigation</param>
        /// <param name="SRid">Receive the safety recommendation id  if already exist.</param>
        public ActionResult AddSR(int id = 0, int SRid = 0)
        {
            var newSR = new SR();
            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 7)
                {
                    if (id > 0)
                    {
                        newSR = new SR
                        {
                            Inv = db.USP_VIEW_INVESTIGATION_FORM35(id).ToList(),
                            Inv_AircraftDet = db.USP_VIEW_INVESTIGATION_AIRCRAFT_DETAIL_FORM35(id).ToList(),
                        };
                    }

                    InvFileId = id;
                    var InvInfo = db.USP_VIEW_INVESTIGATION_FORM35(id).FirstOrDefault();
                    string filenum = InvInfo.InvestigationNumber.Split('/')[1];
                    string SRNumber = getSRNumber(id, 2069, DateTime.Now.Year); // set default SR type from Lookup table.
                    //var SR_NUM = db.USP_SR_NUMBER(id, Convert.ToInt32(filenum)).FirstOrDefault();
                    //TempData["SR_NUM"] = SR_NUM.CONTROL_NUM;
                    TempData["SR_NUM"] = SRNumber;
                    TempData["PageHead"] = InvInfo.InvestigationNumber;
                    TempData["BackPage"] = "sr_mg";
                    TempData["active_url_SR_View"] = "tm";
                    TempData["active_url_SR_View_iic"] = "iic";
                    TempData["lf_menu"] = "1";
                    TempData["InvId"] = InvInfo.InvestigationFileID;
                    TempData["InvFileID"] = InvInfo.InvestigationFileID;
                    Session["InvNum"] = InvInfo.InvestigationNumber;

                    var TypeOfSafetyRecomm = db.LKCOMMONs.Where(a => a.LKCOMMONID == 23).ToList().OrderBy(x => x.ID); // from common lookup table
                    SelectList Safety_Recomm = new SelectList(TypeOfSafetyRecomm, "ID", "LKVALUE");
                    ViewBag.TypeOf_SafetyRecomm = Safety_Recomm;

                    ViewBag.SR_Date = DateTime.Now.Year.ToString();
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("SRController", "AddSR", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserID"]));

                return RedirectToAction("PageNotFound", "Error");
            }
            //ViewBag.mode = mode;
            return View(newSR);
        }
        /// <summary>
        /// Get maximum number of safety recommendation based on investigation - "Now this method not used because of manual SR No"
        /// </summary> 
        /// 
        /// <param name="id">Receive the investigation file id</param>
        /// <param name="srtype">Receive the type of safety recommendation</param>
        /// <param name="sryear">Year of safety recommendation year</param>
        public string getSRNumber(int id, int srtype, int sryear)
        {
            if (sryear <= 0)
            {
                sryear = DateTime.Now.Year;
            }

            string SR_NUM = string.Empty;
            //var SRDet = db.USP_GET_PRE_SR_NUMBER(id, srtype).FirstOrDefault();
            //var InvDet = db.InvestigationFiles.Where(a => a.InvestigationFileID == id).Select(a => a.InvestigationFileNumber).FirstOrDefault();

            //if (srtype == 2069) // Prompt Safety Recommendation - from common lookup table
            //{
            //    if (SRDet != null)
            //    {
            //        if (SRDet.MAXSRNUM >= 1)
            //        {
            //            //SRNum = SRNum.Split('/')[3].Split('-')[0].Split('R')[1];
            //            int newSRNo = Convert.ToInt32(SRDet.MAXSRNUM) + 1;
            //            if (newSRNo < 10)
            //                SR_NUM = InvDet + "/" + "PSR" + "0" + newSRNo + "-" + sryear;
            //            else
            //                SR_NUM = InvDet + "/" + "PSR" + newSRNo + "-" + sryear;
            //        }
            //    }
            //    else
            //    {
            //        SR_NUM = InvDet + "/" + "PSR" + "01" + "-" + sryear;
            //    }
            //}
            //else if (srtype == 2070) // Safety Recommendation - from common lookup table
            //{
            //    if (SRDet != null)
            //    {
            //        if (SRDet.MAXSRNUM >= 1)
            //        {
            //            int newSRNo = Convert.ToInt32(SRDet.MAXSRNUM) + 1;
            //            if (newSRNo < 10)
            //                SR_NUM = InvDet + "/" + "SR" + "0" + newSRNo + "-" + sryear;
            //            else
            //                SR_NUM = InvDet + "/" + "SR" + newSRNo + "-" + sryear;
            //        }
            //    }
            //    else
            //    {
            //        SR_NUM = InvDet + "/" + "SR" + "01" + "-" + sryear;
            //    }
            //}
            //else if (srtype == 2071) // Foreign Safety Recommendation - from common lookup table
            //{
            //    if (SRDet != null)
            //    {
            //        if (SRDet.MAXSRNUM >= 1)
            //        {
            //            int newSRNo = Convert.ToInt32(SRDet.MAXSRNUM) + 1;
            //            if (newSRNo < 10)
            //                SR_NUM = InvDet + "/" + "FSR" + "0" + newSRNo + "-" + sryear;
            //            else
            //                SR_NUM = InvDet + "/" + "FSR" + newSRNo + "-" + sryear;
            //        }
            //    }
            //    else
            //    {
            //        SR_NUM = InvDet + "/" + "FSR" + "01" + "-" + sryear;
            //    }
            //}

            return SR_NUM;
        }
        /// <summary>
        /// Save the new safety recommendation to database based on the investigation file number
        /// </summary> 
        /// 
        /// <param name="NewSR">Receive the safety recommendation details as object </param>
        [HttpPost]
        public ActionResult AddSR(SR NewSR)
        {
            try
            {               
                saveFilestoSharePoint(Request.Files, NewSR.SafetyRecommendationNumber); // save the attached safety recommendation files to sharepoint
                string siteURL = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                if (Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 7)
                {
                    int msgID = 0;
                    if (Convert.ToInt32(Session["UserID"]) > 0)
                    {
                        var checkSRNumber = db.InvestigationSafetyRecommendations.Where(a => a.SafetyRecommendationNumber == NewSR.SafetyRecommendationNumber
                        
                                              && a.InvestigationFileID == NewSR.InvestigationFileID)

                                              .Select(b => b.SafetyRecommendationNumber).FirstOrDefault();

                        if (checkSRNumber == null)
                        {
                            //getSRNumber(NewSR.InvestigationFileID, NewSR.TypeofsafetyRecommendation, NewSR.safetyRecommendationyear)
                            var msg = db.USP_INST_SR("INSERT",NewSR.SafetyRecommendationNumber, NewSR.SafetyRecommendation, Convert.ToInt32(Session["UserId"]), NewSR.InvestigationFileID, "Pending", 0, null, NewSR.TypeofsafetyRecommendation,NewSR.SafetyRecommendationAssignedTo).FirstOrDefault();
                            msgID = Convert.ToInt32(msg.SRID);
                            for (int i = 0; i < SharepointfilePath.Length; i++)
                            {
                                if (SharepointfilePath[i] != null)
                                    db.USP_SR_ATTACHMENTS("insert", Convert.ToInt32(msgID), 0, DocumentTypeId[i], SharepointfilePath[i], SharepointAnonymousPath[i], Convert.ToInt32(Session["UserId"]));
                                else
                                    break;
                            }
                            // commented since mail notification to DAAI is not required now.
                            //var DList = db.Users.AsQueryable().Where(d => d.UserID == 3).Select(x => new { x.EmailAddress, x.FirstName }).FirstOrDefault();
                            //List<string> DListUsers = new List<string>();
                            //List<string> DListUsersEmail = new List<string>();
                            //DListUsersEmail.Add(DList.EmailAddress);
                            //DListUsers.Add(DList.FirstName);
                            //  mailSent = mail.SendMailNotification(8, Convert.ToString(Session["FirstName"]), Convert.ToInt32(Session["roleid"]), siteURL, DListUsers, DListUsersEmail, Convert.ToInt32(Session["InvFileID"]), null, NewSR.SafetyRecommendationNumber, Convert.ToString(Session["InvNum"]), null, null);
                        }
                        else
                        {
                            this.RedirectToAction("AddSR", "SR", new { id = NewSR.InvestigationFileID, SRid = NewSR.SRID });
                        }
                    }
                    Array.Clear(SharepointfilePath, 0, SharepointfilePath.Length);
                    Array.Clear(DocumentTypeId, 0, DocumentTypeId.Length);
                    Array.Clear(SharepointAnonymousPath, 0, SharepointAnonymousPath.Length);
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("SRController", "AddSR::POST", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserID"]));

                return RedirectToAction("PageNotFound", "Error");
            }
            return RedirectToAction("ViewAllSR", "SR", new { id = NewSR.InvestigationFileID });
        }
        /// <summary>
        /// View all safety recommendation details based on particular investigation as page by page
        /// </summary> 
        /// 
        ///<param name="page">Receive the safety recommendation page no</param>
        /// <param name="id">Receive the investigation file id</param>
        public ActionResult ViewAllSR(int? page, int id = 0)
        {
            var SRInfo = new SR();
            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 3 || Convert.ToInt32(Session["RoleId"]) == 6 || Convert.ToInt32(Session["RoleId"]) == 7)
                {
                    if (page == null)
                    {
                        ViewBag.SR_serial_nbr = 0;
                    }
                    else
                    {
                        ViewBag.SR_serial_nbr = 10 * (page - 1);
                    }
                    if (id > 0)
                    {
                        SRInfo = new SR
                        {
                            AllSR = db.USP_VIEW_ALL_SR(id).ToList(),
                            Inv = db.USP_VIEW_INVESTIGATION_FORM35(id).ToList(),
                            Inv_AircraftDet = db.USP_VIEW_INVESTIGATION_AIRCRAFT_DETAIL_FORM35(id).ToList(),
                            SR_Status = db.USP_VIEW_LATEST_SR_STATUS(id).ToList()
                        };
                    }
                    var InvInfo = db.USP_VIEW_INVESTIGATION_FORM35(id).FirstOrDefault();
                    ViewBag.SRpage_no = page;
                    ViewBag.count_SRpagecontent = SRInfo.AllSR.Count();
                    TempData["PageHead"] = InvInfo.InvestigationNumber;
                    TempData["BackPage"] = "yes";
                    TempData["active_url_SR_View"] = "tm";
                    TempData["active_url_SR_View_iic"] = "iic";

                    TempData["lf_menu"] = "1";
                    TempData["InvId"] = InvInfo.InvestigationFileID;
                    TempData["InvFileID"] = InvInfo.InvestigationFileID;
                    Session["Inv_SR"] = SRInfo.AllSR.ToList();
                    TempData["INV_STATUS"] = InvInfo.LKInvestigationStatusTypeID;
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("SRController", "ViewAllSR", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserID"]));

                return RedirectToAction("PageNotFound", "Error");
            }
            return View(SRInfo);
        }
        /// <summary>
        /// View all safety recommendation based on the investigation for bashboard search 
        /// </summary> 
        ///<param name="searchSR">Receive the safety recommendation search details based on investigation</param>            
        ///<param name="page">Receive the safety recommendation page no</param>
        /// <param name="id">Receive the investigation file id</param>
        [HttpPost]
        public ActionResult ViewAllSR(SR searchSR, int? page, int id = 0)
        {
            var SRInfo = new SR();
            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 3 || Convert.ToInt32(Session["RoleId"]) == 6 || Convert.ToInt32(Session["RoleId"]) == 7)
                {
                    if (page == null)
                    {
                        ViewBag.SR_serial_nbr = 0;
                    }
                    else
                    {
                        ViewBag.SR_serial_nbr = 10 * (page - 1);
                    }
                    string searchText = searchSR.SafetyRecommendationNumber;
                    if (id > 0)
                    {
                        SRInfo = new SR
                        {
                            //AllSR = db.USP_VIEW_ALL_SR(id).ToList(),
                            Inv = db.USP_VIEW_INVESTIGATION_FORM35(id).ToList(),
                            Inv_AircraftDet = db.USP_VIEW_INVESTIGATION_AIRCRAFT_DETAIL_FORM35(id).ToList()
                        };
                    }
                    var InvInfo = db.USP_VIEW_INVESTIGATION_FORM35(id).FirstOrDefault();

                    SRInfo.AllSR = (List<USP_VIEW_ALL_SR_Result>)Session["Inv_SR"];
                    //if (corres.LKCorrespondenceMessageTypeID != null)
                    //    Corres.Corres = Corres.Corres.Where(x => x.LKCorrespondenceMessageTypeID == corres.LKCorrespondenceMessageTypeID).ToList();
                    if (!string.IsNullOrEmpty(searchSR.SafetyRecommendationNumber))
                    {
                        //|| comm.convertDateTime(x.LastCorrespondenceDate) == comm.convertDateTime(Convert.ToDateTime(searchText))
                        SRInfo.AllSR = SRInfo.AllSR.Where(x => string.Equals(x.SafetyRecommendationNumber, searchText, StringComparison.CurrentCultureIgnoreCase) || x.SafetyRecommendationNumber.ToLower().Contains(searchText.ToLower()) || string.Equals(x.SafetyRecommendation, searchText, StringComparison.CurrentCultureIgnoreCase) || x.SafetyRecommendation.ToLower().Contains(searchText.ToLower())).ToList();
                    }
                    ViewBag.count_SRpagecontent = SRInfo.AllSR.Count();
                    TempData["PageHead"] = InvInfo.InvestigationNumber;
                    TempData["BackPage"] = "yes";
                    TempData["active_url_SR_View"] = "tm";
                    TempData["lf_menu"] = "1";
                    TempData["InvId"] = InvInfo.InvestigationFileID;
                    TempData["InvFileID"] = InvInfo.InvestigationFileID;
                    TempData["SearchKey"] = searchText;
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("SRController", "ViewAllSR", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserID"]));

                return RedirectToAction("PageNotFound", "Error");
            }
            return View(SRInfo);
        }
        /// <summary>
        /// View safety recommendation by investigation id and safety recommendation id
        /// </summary> 
        /// 
        /// <param name="id">Receive the investigation file id</param>
        /// <param name="SRid">Receive the safety recommendation file id</param>
        public ActionResult ViewSR(int id = 0, int SRid = 0)
        {
            var newSR = new SR();
            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 3 || Convert.ToInt32(Session["RoleId"]) == 6 || Convert.ToInt32(Session["RoleId"]) == 7)
                {
                    if (id > 0)
                    {
                        newSR = new SR
                        {
                            Inv = db.USP_VIEW_INVESTIGATION_FORM35(id).ToList(),
                            Inv_AircraftDet = db.USP_VIEW_INVESTIGATION_AIRCRAFT_DETAIL_FORM35(id).ToList(),
                            SRInfo = db.USP_VIEW_SR(SRid).ToList(),
                            //SRHIST = db.USP_VIEW_SR_UPDT_HISTORY(SRid).ToList(),
                            SRCOMMU = db.USP_VIEW_SR_HISTORIES(SRid).ToList(),
                            SR_Attach = db.USP_SR_ATTACHMENTS("view", SRid, 0, null, null, null, null).ToList()
                        };
                    }
                    newSR.SRID = SRid;
                    var Status = new List<SelectListItem> {
                        new SelectListItem(){Text="Approved",Value="Approved" },
                        new SelectListItem(){Text="Approved and Communicated",Value="Approved and Communicated" },
                        new SelectListItem(){Text="Rejected",Value="Rejected" }
                    };
                    ViewBag.Status = Status; var ImplementaionStatus = db.LKSRImplementationStatus.ToList().OrderBy(a => a.ID);
                    SelectList ImplStatus = new SelectList(ImplementaionStatus, "ID", "SRIMPLEMENTATIONSTATUS");
                    ViewBag.ImpStatus = ImplStatus;

                    var TypeOfSafetyRecomm = db.LKCOMMONs.Where(a => a.LKCOMMONID == 23).ToList().OrderBy(x => x.ID); // from common lookup table
                    SelectList Safety_Recomm = new SelectList(TypeOfSafetyRecomm, "ID", "LKVALUE");
                    ViewBag.TypeOf_SafetyRecomm = Safety_Recomm;

                    foreach (var SRi in newSR.SRInfo)
                    {
                        ViewBag.SRStatus = SRi.SRStatus;
                    }
                    if (ViewBag.SRStatus == 2)
                    {
                        var SR = db.USP_VIEW_SR_STAKEHOLDER_NORESPONSE(SRid).FirstOrDefault();
                        ViewBag.ResponseSentDays = Convert.ToInt32((DateTime.Now - Convert.ToDateTime(SR.CreatedOn)).TotalDays);
                        ViewBag.CorresRecID = Convert.ToInt32(SR.CorrespondenceRecipientID);
                    }
                    var InvInfo = db.USP_VIEW_INVESTIGATION_FORM35(id).FirstOrDefault();
                    TempData["PageHead"] = InvInfo.InvestigationNumber;
                    TempData["BackPage"] = "sr_mg";
                    TempData["active_url_SR_View"] = "tm";
                    TempData["active_url_SR_View_iic"] = "iic";
                    TempData["lf_menu"] = "1";
                    TempData["InvId"] = InvInfo.InvestigationFileID;
                    TempData["InvFileID"] = InvInfo.InvestigationFileID;
                    var SR_NUM = db.USP_VIEW_SR(SRid).FirstOrDefault();
                    newSR.SafetyRecommendationAssignedTo = SR_NUM.SafetyRecommendationAssignedTo;
                    newSR.TypeofsafetyRecommendation = Convert.ToInt32(SR_NUM.SRType);
                    TempData["SR_ID"] = SR_NUM.InvestigationSafetyRecommendationID;
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("SRController", "ViewSR", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserID"]));

                return RedirectToAction("PageNotFound", "Error");
            }
            return View(newSR);
        }
        /// <summary>
        /// Update safety recommendation by respective safety recommendation id  based on transactional safety recommendation
        /// </summary> 
        /// 
        /// <param name="SRHist">Receive the safety recommendation details as object</param>
        [HttpPost]
        public ActionResult UpdateSR(SR SRHist)
        {
            try
            {
                string siteURL = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                if (Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 3 || Convert.ToInt32(Session["RoleId"]) == 7)
                {
                    string command = SRHist.SRCommandAction;
                    bool mailSent = false;
                    //set status based on button command,if forward to SR committee is clicked,corresponding status will be set, if rejected corresponding status set as rejected.
                    if (command == "SenttoCommittee")
                        SRHist.SRStatus = 4;
                    else if (command == "Rejected")
                        SRHist.SRStatus = 5;
                    else if (command == "Closed")
                        SRHist.SRStatus = 8;
                    var SR = db.InvestigationSafetyRecommendations.Where(a => a.InvestigationSafetyRecommendationID == SRHist.SRID).FirstOrDefault();
                    SRHist.InvestigationFileID = SR.InvestigationFileID;
                    if (Convert.ToInt32(Session["UserID"]) > 0)
                    {
                        //updating IIC status and comments on Stakeholder response
                        if (SRHist.SRStatus == 4 || SRHist.SRStatus == 5)
                            db.USP_INST_SR_History(SRHist.SRHistoryID, SRHist.SRResponseID, SRHist.SRID, null, SRHist.IICCommentsforSHResponse, SRHist.IICImplementaionStatus, null, null, null, SRHist.SRStatus, Convert.ToInt32(Session["UserId"]), Convert.ToInt32(Session["UserId"]), "UPDATEBYIICFORSH", null);
                        //insert into Communication History if stakeholder didn't respond for more than 90 days.
                        else if (SRHist.SRStatus == 2)
                        {
                            SRHist.SRStatus = 4;
                            db.USP_INST_SR_History(null, SRHist.SRResponseID, SRHist.SRID, null, SRHist.IICCommentsforSHResponse, SRHist.IICImplementaionStatus, null, null, null, SRHist.SRStatus, Convert.ToInt32(Session["UserId"]), Convert.ToInt32(Session["UserId"]), "INSERTBYIICNORES", SRHist.CorresRecipientID);
                        }
                        //updating IIC status and comments on SR Committee Action
                        else
                            db.USP_INST_SR_History(SRHist.SRHistoryID, SRHist.SRResponseID, SRHist.SRID, null, null, null, null, null, SRHist.IICClosureComments, SRHist.SRStatus, Convert.ToInt32(Session["UserId"]), Convert.ToInt32(Session["UserId"]), "UPDATEBYIICFORSRC", null);
                        if (command == "SenttoCommittee" || command == "SendtoCommitteonNoRes")
                        {
                            var DList = db.USP_GET_DL(1088).ToList();
                            List<string> DListUsers = new List<string>();
                            List<string> DListUsersEmail = new List<string>();
                            string randomURL = string.Empty;
                            foreach (var users in DList)
                            {
                                DListUsersEmail.Add(users.EmailAddress);
                                DListUsers.Add(users.FirstName);
                            }
                            string randomCode = Convert.ToString(Guid.NewGuid());
                            var verifyurl = "/Anonymous/SRCommittee/" + randomCode;
                            randomURL = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, verifyurl);
                            //to get the Communication History ID in case if no response received from stakeholder
                            var SRHistory = db.InvestigationSafetyRecommendationCommunicationHIstories.Where(a => a.SRResponseID == SRHist.SRResponseID).FirstOrDefault();
                            foreach (var ToUsers in DListUsersEmail.ToList())
                            {
                                gcaa_ismEntities dbcontext = new gcaa_ismEntities();
                                var AlreadyAddedUsers = db.InvestigationSafetyRecommendationSRCommittees.Where(a => a.SRID == SRHist.SRID && a.SRHistoryID == SRHist.SRHistoryID && a.SRCommitteeEmailID == ToUsers).FirstOrDefault();
                                if (AlreadyAddedUsers == null)
                                    dbcontext.USP_INST_SRCOMMITTEE_RANDOMLINK(SRHistory.SRHistoryID, SRHist.SRID, randomCode, false, ToUsers, Convert.ToInt32(Session["UserId"]), null);
                                dbcontext.SaveChanges();
                            }
                            mailSent = mail.SendMailNotification(9, Convert.ToString(Session["FirstName"]), Convert.ToInt32(Session["roleid"]), randomURL, DListUsers, DListUsersEmail, Convert.ToInt32(Session["InvFileID"]), null, SR.SafetyRecommendationNumber, null, null, null);


                        }
                        else if (command.ToLower() == "closed")
                        {
                            var DList = db.USP_GET_SRCLOSURE_NOTIFY_USERS(SRHist.SRHistoryID).ToList();
                            List<string> DListUsers = new List<string>();
                            List<string> DListUsersEmail = new List<string>();
                            string randomURL = string.Empty;
                            foreach (var users in DList)
                            {
                                DListUsersEmail.Add(users.TOUSER);
                            }
                            mailSent = mail.SendMailNotification(20, Convert.ToString(Session["FirstName"]), Convert.ToInt32(Session["roleid"]), null, null, DListUsersEmail, Convert.ToInt32(Session["InvFileID"]), null, SR.SafetyRecommendationNumber, null, SRHist.SRIMPLEMENTATIONSTATUS, System.Text.RegularExpressions.Regex.Replace(SR.SafetyRecommendation, @"<[^>]+>|&nbsp;", String.Empty));
                        }
                        else if (command.ToLower() == "rejected")  // rejected response shall to sent to the assignee again for further corrective action
                        {
                            var srinfo = db.InvestigationSafetyRecommendations.Where(a => a.InvestigationSafetyRecommendationID == SRHist.SRID).FirstOrDefault();

                            var corrmsginfo = db.CorrespondenceMessages.Where(a => a.SR_Number == srinfo.SafetyRecommendationNumber &&
                            
                                                                             a.InvestigationSafetyRecommendationID == SRHist.SRID).FirstOrDefault();

                            var corrinfo = db.Correspondences.Where(a => a.CorrespondenceID == corrmsginfo.CorrespondenceID).FirstOrDefault();

                            if (corrmsginfo.DAAIApprovalRequired == false || corrmsginfo.DAAIApprovalRequired == true)
                            {
                                var Recepients = db.USP_CORRES_RECIPIENTS("viewuser", corrmsginfo.CorrespondenceID, null, null, null, null, null, Convert.ToInt32(Session["UserId"])).ToList();

                                List<string> DListUsers = new List<string>();
                                Dictionary<string, List<string>> DListUsersEmail = new Dictionary<string, List<string>>();
                                Dictionary<string, string> DListRandomLink = new Dictionary<string, string>();
                                foreach (var emailrec in Recepients)
                                {
                                    List<string> lst = new List<string>();
                                    if (DListUsersEmail.ContainsKey(emailrec.RECTYPE))
                                        DListUsersEmail[emailrec.RECTYPE].Add(emailrec.RECIPIENT);
                                    else
                                        DListUsersEmail.Add(emailrec.RECTYPE, lst);
                                    lst.Add(emailrec.RECIPIENT);
                                    DListUsers.Add(emailrec.FirstName);
                                }

                                var Attachments = db.USP_CORRES_ATTACHMENTS("view", corrmsginfo.CorrespondenceMessageID, null, null, null, null).ToList();
                                int filesCount = 0;
                                string[] filesAttached = new string[50];

                                foreach (var attachment in Attachments)
                                {
                                    if (!string.IsNullOrEmpty(attachment.AttachmentPublicUrl))
                                    {
                                        filesAttached[filesCount] = comm.getEventFilesOrOtherFilesOnPrem(Convert.ToString(attachment.AttachmentPublicUrl));
                                        filesCount++;
                                    }
                                }

                                foreach (var ToUsers in DListUsersEmail.ToList())
                                {
                                    if (ToUsers.Key.ToLower() == "to")
                                    {
                                        foreach (var usermail in ToUsers.Value.ToList())
                                        {
                                            string randomCode = Convert.ToString(Guid.NewGuid());
                                            string verifyurl = string.Empty;
                                            verifyurl = "/Anonymous/CorrespondenceResponse/" + randomCode;
                                            string randomURL = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, verifyurl);
                                            if (!DListRandomLink.Keys.Contains(usermail))
                                                DListRandomLink.Add(usermail, randomURL);
                                            //var ResponsedUsers = db.CorrespondenceRecipients.Where(a => a.CorrespondenceID == corrinfo.CorrespondenceID && a.RecipientEmailID == usermail).FirstOrDefault();
                                            //if (ResponsedUsers == null)
                                            db.USP_ADD_CORRES_RESPONSE_DETAILS(corrinfo.CorrespondenceID, randomCode, 0, usermail, Convert.ToInt32(Session["UserId"]), Convert.ToInt32(Session["UserId"]));
                                            //if (corrmsginfo.LKCorrespondenceMessageTypeID == 6)
                                            db.USP_ADD_SR_STAKEHOLDER_RESPONSE_DETAILS(SRHist.SRID, randomCode, false, usermail, Convert.ToInt32(Session["UserId"]), Convert.ToInt32(Session["UserId"]));
                                            // int SRResponseID = Convert.ToInt32(db.InvestigationSafetyRecommendationStakeholders.Max(a => a.SRResponseID));
                                            //  db.USP_INST_SR_History(null, SRResponseID, SRHist.SRID, null, SRHist.IICCommentsforSHResponse, SRHist.IICImplementaionStatus, null, null, null, SRHist.SRStatus, Convert.ToInt32(Session["UserId"]), Convert.ToInt32(Session["UserId"]), "INSERTBYIICREJECT", SRHist.CorresRecipientID);

                                        }
                                    }
                                }

                                //if (corrmsginfo.LKCorrespondenceMessageTypeID == 6)
                                //{
                                var SRDetails = db.USP_VIEW_SR(SRHist.SRID).FirstOrDefault();

                                var SRattachments = db.USP_SR_ATTACHMENTS("view", SRHist.SR_ID, 0, null, null, null, null).ToList();

                                if (SRattachments.Count > 0)
                                {
                                    foreach (var SRattachment in SRattachments)
                                    {
                                        if (!string.IsNullOrEmpty(SRattachment.AttachmentPublicUrl))
                                        {
                                            filesAttached[filesCount] = comm.getEventFilesOrOtherFilesOnPrem(Convert.ToString(SRattachment.AttachmentPublicUrl));
                                            filesCount++;
                                        }
                                    }
                                }
                                filesAttached[filesCount] = pdf.generatePdf(SRDetails, null, null, "ResendCorresSR", "");
                                // }

                                mailSent = mail.SendMailNotificationwithAttach(7, Convert.ToString(Session["FirstName"]), Convert.ToInt32(Session["roleid"]), siteURL, DListUsers,

                                    DListUsersEmail, DListRandomLink, Convert.ToInt32(Session["InvFileID"]), filesAttached, corrinfo.CorrespondenceControlNumber,

                                    Convert.ToString(Session["InvNum"]), corrinfo.Subject, null, corrmsginfo.CorrespondenceContent, corrmsginfo.CorrespondenceMessageFrom);

                                Array.Clear(SharepointfilePath, 0, SharepointfilePath.Length);
                                Array.Clear(DocumentTypeId, 0, DocumentTypeId.Length);
                                //Array.Clear(DLId, 0, DLId.Length);
                                //Array.Clear(userId, 0, userId.Length);
                                //Array.Clear(RecepientType, 0, RecepientType.Length);
                                //Array.Clear(extMailids, 0, extMailids.Length);
                                //Array.Clear(mailType, 0, extMailids.Length);
                                Array.Clear(SharepointAnonymousPath, 0, SharepointAnonymousPath.Length);
                            }
                            //else if(corrmsginfo.DAAIApprovalRequired == true)
                            //{
                            //    var DList = db.Users.AsQueryable().Where(d => d.UserRoleID == 3).Select(x => new { x.EmailAddress, x.FirstName }).FirstOrDefault();
                            //    List<string> DListUsers = new List<string>();
                            //    List<string> DListUsersEmail = new List<string>();
                            //    DListUsersEmail.Add(DList.EmailAddress);
                            //    DListUsers.Add(DList.FirstName);
                            //    mailSent = mail.SendMailNotification(6, Convert.ToString(Session["FirstName"]), Convert.ToInt32(Session["roleid"]),

                            //    siteURL, DListUsers, DListUsersEmail, Convert.ToInt32(Session["InvFileID"]), null, corrinfo.CorrespondenceControlNumber, Convert.ToString(Session["InvNum"]), null, null);
                            //}
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("SRController", "UpdateSR::POST", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserID"]));

                return RedirectToAction("PageNotFound", "Error");
            }
            return RedirectToAction("ViewAllSR", "SR", new { id = SRHist.InvestigationFileID });
        }
        /// <summary>
        /// save Files to SharePoint
        /// </summary>
        /// <param name="files">Received Attached safety recommendation file attachment</param>
        /// <param name="CONTROL_NUMER">Investigation file number</param>
        private void saveFilestoSharePoint(HttpFileCollectionBase files, string CONTROL_NUMER)
        {
            try
            {
                if (files.Count > 0)
                {
                    if (string.IsNullOrEmpty(CONTROL_NUMER))
                        CONTROL_NUMER = "0";
                    // comm.CreateIfMissing(Server.MapPath("~/") + "CorrespondenceFiles");
                    int filecount = 0;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];

                        if (file.ContentLength > 0 && !string.IsNullOrEmpty(file.FileName))
                        {

                            int fileSize = file.ContentLength;

                            //string fileName = file.FileName;
                            string fileName = file.FileName.Substring(0, file.FileName.LastIndexOf('.')) + "_" + DateTime.Now.ToFileTime() + file.FileName.Substring(file.FileName.LastIndexOf('.'), ((file.FileName.Length) - (file.FileName.LastIndexOf('.'))));

                            string mimeType = file.ContentType.Split('/')[1].ToString();

                            DocumentTypeId[i] = Convert.ToInt32(db.LKDocumentTypes.AsQueryable().Where(d => d.LKDocumentTypeName == mimeType).Select(d => d.LKDocumentTypeID).FirstOrDefault());

                            Stream fileContent = file.InputStream;

                            //   file.SaveAs(Server.MapPath("~/") + "CorrespondenceFiles\\" + fileName);

                            //string[] attachURLs = comm.savetoSharePointDocs(fileContent, Server.MapPath("~/") + "CorrespondenceFiles\\" + fileName, CONTROL_NUMER, "Corres_Documents");

                            string[] attachURLs = comm.savetoSharePointOtherDocsOnPrem(fileContent, fileName, CONTROL_NUMER, "SR_Documents");

                            SharepointfilePath[filecount] = attachURLs[0];

                            SharepointAnonymousPath[filecount] = attachURLs[1];

                            //   var result = gcaa.USP_INST_INVESTIGATION_DOCUMENTASATTACHMENT(0, InvestigationFileID, 1, DocumentTypeId, SharepointfilePath);

                            //   gcaa.SaveChanges();
                            filecount++;
                        }
                    }
                    //   comm.DeleteDirectory(Server.MapPath("~/") + "CorrespondenceFiles");

                    var fullpathoffile = Server.MapPath("~/App_Data/SR_Files/" + InvFileId);

                    if (System.IO.Directory.Exists(fullpathoffile))
                    {
                        comm.DeleteDirectory(fullpathoffile);

                        System.IO.Directory.Delete(fullpathoffile);
                    }

                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("SRController", "saveFilestoSharePoint", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }
        }
        //public ActionResult ViewSR(SR NewSR)
        //{
        //    try
        //    {
        //        string siteURL = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
        //        bool mailSent = false;
        //        int msgID = 0;
        //        if (Convert.ToInt32(Session["UserID"]) > 0)
        //        {
        //            if (Convert.ToInt32(Session["roleid"]) == 3)
        //            {
        //                db.USP_INST_SR("updatebyDAAI", NewSR.SafetyRecommendationNumber, NewSR.SafetyRecommendation, Convert.ToInt32(Session["UserId"]), NewSR.InvestigationFileID, NewSR.DAAIStatus, NewSR.SR_ID, NewSR.DAAIFeedback).FirstOrDefault();
        //                db.SaveChanges();
        //                var CreatedIIC = db.InvestigationSafetyRecommendations.AsQueryable().Where(d => d.InvestigationSafetyRecommendationID == NewSR.SR_ID).Select(x => x.CreatedBy).FirstOrDefault();
        //                var DList = db.Users.AsQueryable().Where(d => d.UserID == (CreatedIIC)).Select(x => new { x.EmailAddress, x.FirstName }).FirstOrDefault();
        //                List<string> DListUsers = new List<string>();
        //                List<string> DListUsersEmail = new List<string>();
        //                DListUsersEmail.Add(DList.EmailAddress);
        //                DListUsers.Add(DList.FirstName);
        //                mailSent = mail.SendMailNotification(9, Convert.ToString(Session["FirstName"]), Convert.ToInt32(Session["roleid"]), siteURL, DListUsers, DListUsersEmail, Convert.ToInt32(Session["InvFileID"]), null, NewSR.SafetyRecommendationNumber, Convert.ToString(Session["InvNum"]), NewSR.DAAIStatus, NewSR.DAAIFeedback);
        //            }
        //            else
        //            {
        //                db.USP_UPDT_SR_HISTORY(NewSR.SR_ID, NewSR.SafetyRecommendation, NewSR.ReasonForUpdate, Convert.ToInt32(Session["UserId"]), NewSR.DateChanged, NewSR.DAAIFeedback);
        //                //On Rejected SRs, investigator can update SR and send it for DAAI approval, hence giving the status as 'pending'
        //                db.USP_INST_SR("updatebyIIC", NewSR.SafetyRecommendationNumber, NewSR.SafetyRecommendation, Convert.ToInt32(Session["UserId"]), NewSR.InvestigationFileID, "Pending", NewSR.SR_ID, NewSR.DAAIFeedback);
        //                db.SaveChanges();
        //                //if (NewSR.DAAIStatus.ToLower() != "approved" && NewSR.DAAIStatus.ToLower() != "rejected")
        //                //{
        //                var DList = db.Users.AsQueryable().Where(d => d.UserID == 3).Select(x => new { x.EmailAddress, x.FirstName }).FirstOrDefault();
        //                List<string> DListUsers = new List<string>();
        //                List<string> DListUsersEmail = new List<string>();
        //                DListUsersEmail.Add(DList.EmailAddress);
        //                DListUsers.Add(DList.FirstName);
        //                mailSent = mail.SendMailNotification(10, Convert.ToString(Session["FirstName"]), Convert.ToInt32(Session["roleid"]), siteURL, DListUsers, DListUsersEmail, Convert.ToInt32(Session["InvFileID"]), null, NewSR.SafetyRecommendationNumber, Convert.ToString(Session["InvNum"]), null, null);
        //            }
        //            for (int i = 0; i < SharepointfilePath.Length; i++)
        //            {
        //                if (SharepointfilePath[i] != null)
        //                    db.USP_SR_ATTACHMENTS("insert", Convert.ToInt32(NewSR.SR_ID),0, DocumentTypeId[i], SharepointfilePath[i], SharepointAnonymousPath[i], Convert.ToInt32(Session["UserId"]));
        //                else
        //                    break;
        //            }
        //        }
        //        Array.Clear(SharepointfilePath, 0, SharepointfilePath.Length);
        //        Array.Clear(DocumentTypeId, 0, DocumentTypeId.Length);
        //    }
        //    catch (System.Exception ex)
        //    {
        //        comm.Exception_Log("SRController", "ViewSR::POST", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserID"]));

        //        return RedirectToAction("PageNotFound", "Error");
        //    }
        //    return RedirectToAction("ViewAllSR", "SR", new
        //    {
        //        id = NewSR.InvestigationFileID
        //    });
        //}
        /// <summary>
        /// Save safety recommendation attachment files to database table
        /// </summary> 
        /// 
        /// <param name="control">Receive the investigation file no </param>
        [HttpPost]
        public void SaveFiles(string control)
        {
            try
            {
                string CONTROL_NUMER = control;
                if (CONTROL_NUMER != null)
                {
                    if (Request.Files.Count > 0)
                    {
                        //   comm.CreateIfMissing(Server.MapPath("~/") + "SRFiles");
                        int filecount = 0;
                        for (int i = 0; i < Request.Files.Count; i++)
                        {
                            HttpPostedFileBase file = Request.Files[i];

                            int fileSize = file.ContentLength;

                            //string fileName = file.FileName;

                            string fileName = file.FileName.Substring(0, file.FileName.LastIndexOf('.')) + "_" + DateTime.Now.ToFileTime() + file.FileName.Substring(file.FileName.LastIndexOf('.'), ((file.FileName.Length) - (file.FileName.LastIndexOf('.'))));

                            string mimeType = file.ContentType.Split('/')[1].ToString();

                            DocumentTypeId[i] = Convert.ToInt32(db.LKDocumentTypes.AsQueryable().Where(d => d.LKDocumentTypeName == mimeType).Select(d => d.LKDocumentTypeID).FirstOrDefault());

                            Stream fileContent = file.InputStream;

                            //  file.SaveAs(Server.MapPath("~/") + "SRFiles\\" + fileName);

                            //string[] attachURLs = comm.savetoSharePointDocs(fileContent, Server.MapPath("~/") + "SRFiles\\" + fileName, CONTROL_NUMER, "SR_Documents");

                            string[] attachURLs = comm.savetoSharePointOtherDocsOnPrem(fileContent, fileName, CONTROL_NUMER, "SR_Documents");

                            SharepointfilePath[filecount] = attachURLs[0];

                            SharepointAnonymousPath[filecount] = attachURLs[1];

                            //   var result = gcaa.USP_INST_INVESTIGATION_DOCUMENTASATTACHMENT(0, InvestigationFileID, 1, DocumentTypeId, SharepointfilePath);

                            //   gcaa.SaveChanges();
                            filecount++;
                        }
                        //     comm.DeleteDirectory(Server.MapPath("~/") + "SRFiles");


                    }
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("SRontroller", "SaveFiles", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                RedirectToAction("PageNotFound", "Error");
            }
        }
        /// <summary>
        ///  Delete safety recommendation attachment files from database 
        /// </summary> 
        /// 
        /// <param name="id">Receive the investigation file no </param>
        /// <param name="path">Receive the attachment file path </param>
        [HttpPost]
        public JsonResult DeleteAttachment(int id, string path)
        {
            string results = string.Empty;
            try
            {
                if (Convert.ToInt32(id) > 0)
                {
                    bool IsFileDeleted = comm.deleteFiles(path);
                    var result = db.USP_SR_ATTACHMENTS("delete", id, 0, null, null, null, null);
                    db.SaveChanges();
                    results = "success";
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("SRController", "DeleteAttachment", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                RedirectToAction("PageNotFound", "Error");
            }
            return Json(results);
        }
        /// <summary>
        /// Get safety recommendation Response by SR history id
        /// </summary> 
        /// 
        /// <param name="SRHistoryID">Receive the safety recommendation History ID </param>
        [HttpPost]
        public JsonResult getSRResponse(int SRHistoryID)
        {
            string result = string.Empty;
            try
            {
                //var SRResponse = db.InvestigationSafetyRecommendationCommunicationHIstories.Where(a => a.SRResponseID == SRResponseId).FirstOrDefault();
                var SRHistory = db.USP_VIEW_SR_COMMUNICATION_HISTORY(SRHistoryID).FirstOrDefault();
                if (SRHistory != null)
                {
                    JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                    result = javaScriptSerializer.Serialize(SRHistory);
                }
                else
                    result = string.Empty;
            }
            catch (System.Exception ex)
            {
                result = string.Empty;
                comm.Exception_Log("SRController", "getSRResponse", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Get number of safety recommendation Response based on investigation
        /// </summary> 
        /// 
        /// <param name="SRID">Receive the safety recommendation ID </param>
        [HttpPost]
        public JsonResult getSRForNoResponse(int SRID)
        {
            string result = string.Empty;
            try
            {
                //var SRResponse = db.InvestigationSafetyRecommendationCommunicationHIstories.Where(a => a.SRResponseID == SRResponseId).FirstOrDefault();
                var SRREs = db.USP_VIEW_SR_STAKEHOLDER_NORESPONSE(SRID).FirstOrDefault();
                if (SRREs != null)
                {
                    JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                    result = javaScriptSerializer.Serialize(SRREs);
                }
                else
                    result = string.Empty;
            }
            catch (System.Exception ex)
            {
                result = string.Empty;
                comm.Exception_Log("SRController", "getSRForNoResponse", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Get number of safety recommendation Response with attachment based on investigation
        /// </summary> 
        /// 
        /// <param name="SRID">Receive the safety recommendation ID </param>
        /// <param name="SRResponseID">Receive the safety recommendation Response ID </param>
        [HttpPost]
        public JsonResult getSRResponseAttachments(int SRID, int SRResponseID)
        {
            string result = string.Empty;
            try
            {
                //var SRResponse = db.InvestigationSafetyRecommendationCommunicationHIstories.Where(a => a.SRResponseID == SRResponseId).FirstOrDefault();
                var SRAttachmetns = db.USP_SR_ATTACHMENTS("view", SRID, SRResponseID, 0, null, null, 0).ToList();
                if (SRAttachmetns != null)
                {
                    JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                    result = javaScriptSerializer.Serialize(SRAttachmetns);
                }
                else
                    result = string.Empty;
            }
            catch (System.Exception ex)
            {
                result = string.Empty;
                comm.Exception_Log("SRController", "DeleteAttachment", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Upload safety recommendation file to system folder 
        /// </summary> 
        /// 
        [HttpPost]
        public JsonResult UploadSRFile()
        {
            try
            {
                comm.CreateIfMissing(Server.MapPath("~/") + "App_Data/SR_Files/" + InvFileId);

                foreach (string file in Request.Files)
                {
                    var fileContent = Request.Files[file];

                    if (fileContent != null && fileContent.ContentLength > 0)
                    {
                        var stream = fileContent.InputStream;

                        var fileName = Path.GetFileName(file);

                        var path = Path.Combine(Server.MapPath("~/App_Data/SR_Files/" + InvFileId), fileContent.FileName);

                        using (var fileStream = System.IO.File.Create(path))
                        {
                            stream.CopyTo(fileStream);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("DIController", "UploadSRFile", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return Json("Upload failed");
            }

            return Json("File uploaded successfully");
        }
        /// <summary>
        /// Remove safety recommendation attached file from database
        /// </summary> 
        /// 
        /// <param name="filename">Receive the attachment file name </param>
        [HttpPost]
        public JsonResult RemoveSRAttachedFile(string filename)
        {
            string result = "failed";

            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 7)
                {
                    if (Convert.ToInt32(Session["UserId"]) > 0)
                    {
                        string[] fName = filename.Split('\\');

                        if (fName.Count() >= 2)
                        {
                            var fullpathoffile = Server.MapPath("~/App_Data/SR_Files/" + InvFileId + "/" + fName[2]);

                            if (System.IO.File.Exists(fullpathoffile))
                            {
                                System.IO.File.Delete(fullpathoffile);
                            }

                            result = "success";
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("SR", "RemoveSRAttachedFile", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }

            return Json(result);
        }
        /// <summary>
        /// Print SR Details
        /// </summary>
        /// <param name="id"> Received the investigation File Id</param>
        /// <returns>Show all  the safety recommendation details based on the investigation id</returns>
       // [HttpPost]
        public ActionResult PrintSRDetails(int id = 0)
        {
            try
            {
                var srinfo = db.USP_PRINT_SR(id).ToList();

                if (srinfo != null)
                {
                    string invno = db.InvestigationFiles.Where(a => a.InvestigationFileID == id).Select(b => b.InvestigationFileNumber).FirstOrDefault();

                    //string fileName = "SRreport_" + Convert.ToString(DateTime.Now.ToFileTime()) + ".pdf";

                    // comm.CreateIfMissing(Server.MapPath("~/") + "SRReport");

                    // string filepath = Server.MapPath(Path.Combine("~/SRReport", fileName));

                    StringReader srn = new StringReader((pdf.generateHTMLContent(srinfo, id, invno, "PrintSR", "")));

                    StringBuilder sb = new StringBuilder();

                    Byte[] res = comm.PdfSharpConvert(srn.ReadToEnd().ToString());

                    //using (FileStream fis = new FileStream(filepath, FileMode.Create))
                    //{
                    //    fis.Write(res, 0, res.Length);

                    //    fis.Position = 0;

                    //    fis.Close();
                    //}

                    MemoryStream pdfStream = new MemoryStream();

                    pdfStream.Write(res, 0, res.Length);

                    pdfStream.Position = 0;

                    return new FileStreamResult(pdfStream, "application/pdf");
                }
                else
                {
                    return RedirectToAction("NotFound", "Error");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("SR", "gen_forms", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("NotFound", "Error");
            }
        }
        /// <summary>
        /// update SR details by safety recommendation Id
        /// </summary>
        /// <param name="srno"> Get all the SR details as object</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ViewSR(SR srno)
        {
            try
            {
                var srNoinfo = db.InvestigationSafetyRecommendations.Where(a => a.InvestigationSafetyRecommendationID == srno.SR_ID)
                    
                           .Select(a=>a.SafetyRecommendationNumber).FirstOrDefault();

                if (srNoinfo != null)
                {
                    var CorresMessageID = db.CorrespondenceMessages.Where(a => a.SR_Number == srNoinfo
                    
                                               & a.InvestigationSafetyRecommendationID == srno.SR_ID

                    ).Select(a=>a.CorrespondenceMessageID).FirstOrDefault();

                    if (CorresMessageID == 0)
                    {
                        var result = db.USP_UPDATE_SR_FILENO(srno.SR_ID, srNoinfo, 0, srno.SafetyRecommendationAssignedTo);
                    }
                    else
                    {
                        var result = db.USP_UPDATE_SR_FILENO(srno.SR_ID, srNoinfo, CorresMessageID, srno.SafetyRecommendationAssignedTo);
                    }
                }                                              

                saveFilestoSharePoint(Request.Files, srno.SafetyRecommendationNumber);

                for (int i = 0; i < SharepointfilePath.Length; i++)
                {
                    if (SharepointfilePath[i] != null)
                        db.USP_SR_ATTACHMENTS("insert", Convert.ToInt32(srno.SR_ID), 0, DocumentTypeId[i], SharepointfilePath[i], SharepointAnonymousPath[i], Convert.ToInt32(Session["UserId"]));
                    else
                        break;
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("SR", "ViewSR", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("NotFound", "Error");
            }

            return this.RedirectToAction("ViewSR", "SR", new { id = srno.InvestigationFileID, SRid = srno.SR_ID });
        }
        /// <summary>
        /// Search SR details in dashboard
        /// </summary>
        /// <param name="searchSR">Receive the Search SR details as object</param>
        /// <param name="page">Receive the Safety Recommendation page number </param>
        /// <param name="searchkey">Receive the search key value</param>
        /// <returns></returns>
        public ActionResult SearchSR(SR searchSR, int? page,string searchkey)
        {
            var SRInfo = new SR();
            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 1)
                {
                    TempData["Active_Admin_Page"] = "SRDelete";

                    if (page == null)
                    {
                        ViewBag.SR_serial_nbr = 0;
                    }
                    else
                    {
                        ViewBag.SR_serial_nbr = 10 * (page - 1);
                    }

                    ViewBag.SRpage_no = page;

                    string searchText = string.Empty;

                    if (searchSR.SafetyRecommendationNumber != null)
                    {
                        searchText = searchSR.SafetyRecommendationNumber;
                    }
                    else
                    {
                        searchText = searchkey;
                    }
 
                    SRInfo = new SR
                    {
                        SR_info = db.USP_VIEW_ALL_SR_FORDELETE().ToList(),
                    };

                    if (!string.IsNullOrEmpty(searchText))
                    {
                        SRInfo.SR_info = SRInfo.SR_info.Where(x => string.Equals(x.SafetyRecommendationNumber, searchText,
                            StringComparison.CurrentCultureIgnoreCase) || x.SafetyRecommendationNumber.ToLower().Contains(searchText.ToLower()) ||
                            string.Equals(x.InvestigationFileNumber, searchText, StringComparison.CurrentCultureIgnoreCase) ||
                            x.InvestigationFileNumber.ToLower().Contains(searchText.ToLower())).ToList();
                    }

                    SRInfo.SafetyRecommendationNumber = searchText;

                    ViewBag.count_SRpagecontent = SRInfo.SR_info.Count();
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("SRController", "SearchSR", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserID"]));

                return RedirectToAction("PageNotFound", "Error");
            }

            return View(SRInfo);
        }
        /// <summary>
        /// Delete Safety Recommendation for just newly added only not in transactional  - "Admin Dashboard"
        /// </summary>
        /// <param name="id">Receive the Safety Recommendation file Id</param>
        /// <returns></returns>
        /// 
        [HttpPost]
        public ActionResult DeleteSR(int id)
        {
            try
            {
                var result = db.USP_DELETE_SR(id);
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("SR", "DeleteSR", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }

            return RedirectToAction("SearchSR", "SR");
        }
        /// <summary>
        /// Validate SR Number 
        /// </summary>
        /// <param name="Srno">Receive the safety recommendation number for validation</param>
        /// <param name="Srid">Receive the SR Id</param>
        /// <param name="Srtypeid">Receive the type of SR</param>
        /// <returns></returns>
        /// 
        public JsonResult ValidateSR(string Srno,int Srid,int Srtypeid)
        {
            try
            {
                //string[] InvNoSplit = (Srno.Split('/'));

                //if (InvNoSplit[0] != "AIFN")
                //{
                //    return Json("0");
                //}

                //if (InvNoSplit[1].Length != 4)
                //{
                //    return Json("0");
                //}

                //if (InvNoSplit[2].Length != 4)
                //{
                //    return Json("0");
                //}

                //string InvNo = InvNoSplit[0] + "/" + InvNoSplit[1] + "/" + InvNoSplit[2];

                //var checkInvNo = db.InvestigationFiles.Where(a => a.InvestigationFileNumber == InvNo).FirstOrDefault();

                //if(checkInvNo == null)
                //{
                //    return Json("0");
                //}

                string[] srno = Convert.ToString(Srno).Split('-');

                if (Srtypeid == 2070)
                {
                    if (!srno[0].Contains("SR"))
                    {
                        return Json("0");
                    }

                    if (srno[0].Length > 5)
                    {
                        return Json("0");
                    }

                    if (srno[1].Length != 4)
                    {
                        return Json("0");
                    }
                }
                else if (Srtypeid == 2069)
                {
                    if (!srno[0].Contains("PSR"))
                    {
                        return Json("0");
                    }

                    if (srno[0].Length > 5)
                    {
                        return Json("0");
                    }

                    if (srno[1].Length != 4)
                    {
                        return Json("0");
                    }
                }
                else if (Srtypeid == 2071)
                {
                    if (!srno[0].Contains("FSR"))
                    {
                        return Json("0");
                    }

                    if (srno[0].Length > 5)
                    {
                        return Json("0");
                    }

                    if (srno[1].Length != 4)
                    {
                        return Json("0");
                    }
                }
                else
                {
                    return Json("0");
                }

                dynamic AlreadyExist = null;                               

                if (Srid > 0)
                {
                     AlreadyExist = db.InvestigationSafetyRecommendations.Where(a => a.SafetyRecommendationNumber == Srno
                                                && a.InvestigationSafetyRecommendationID != Srid
                                                && a.InvestigationFileID == InvFileId                                                
                                                ).FirstOrDefault();
                }
                else
                {
                    AlreadyExist = db.InvestigationSafetyRecommendations.Where(a => a.SafetyRecommendationNumber == Srno                                
                                                && a.InvestigationFileID == InvFileId).FirstOrDefault();
                }

                if (AlreadyExist != null)
                {
                    return Json("2");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("SR", "DeleteSR", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return Json("0");
            }

            return Json(true);
        }
    }
}