﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ISM_Project.Models
{
    public class Form39
    {
        public IEnumerable<LKCOMMON> MasterValues { get; set; }
        public IEnumerable<InvestigationFileAssessment> Ass { get; set; }
        public IEnumerable<USP_VIEW_INVESTIGATION_FORM35_Result> Form35_DI { get; set; }
        public IEnumerable<InvestigationFileDetail> Inv { get; set; }
        public IEnumerable<USP_view_Assign_IIC_Result> assign_iic { get; set; }
        public IEnumerable<USP_IIC_VIEW_PROFILE_Result> iic_view_profile { get; set; }
        public IEnumerable<USP_IIC_VIEW_PROFILE_2_Result> iic_view_profile_2 { get; set; }
        public IEnumerable<USP_VIEW_IIC_CASES_Result> IIC_Cases { get; set; }
        public IEnumerable<USP_VIEW_INVESTIGATION_AIRCRAFT_DETAIL_FORM35_Result> Inv_AircraftDet { get; set; }
        public IEnumerable<USP_VIEW_INVESTIGATION_ATTACHMENTFILE_Result> Attachment { get; set; }
        public IEnumerable<USP_LIST_IIC_INCHARGE_WITH_CASE_Result> list_iic_incharge { get; set; }
        public IEnumerable<USP_VIEW_INVESTIGATION_Result> InvList { get; set; }
        public IEnumerable<USP_VIEW_ALL_OCCURRENCE_Result> OccList { get; set; }
        /*Investigation*/
        public IEnumerable<USP_VIEW_TASKGROUPS_Result> Grp { get; set; }
        public IEnumerable<USP_VIEW_IIC_TASKS_Result> IIC_Tasks { get; set; }
        public IEnumerable<USP_VIEW_INVESTIGATION_SCOPE_DEFINITION_INVESTIGATIONID_Result> Inv_Group_Scope { get; set; }
        /*Correspondence*/
        public IEnumerable<USP_VIEW_PENDING_CORRESPONDENCE_Result> Corres { get; set; }
        public IEnumerable<USP_VIEW_CORRESPONDENCE_Result> corres_Det { get; set; }
        public IEnumerable<USP_VIEW_CORRES_ATTACHMENTS_Result> Corres_Attach { get; set; }
        /*Safety Recommendation*/
        public IEnumerable<USP_VIEW_SR_Result> SRInfo { get; set; }
        public IEnumerable<USP_VIEW_SR_UPDT_HISTORY_Result> SRHIST { get; set; }
        public IEnumerable<USP_VIEW_ALL_SR_Result> AllSR { get; set; }
        public IEnumerable<USP_SR_ATTACHMENTS_Result> SR_Attach { get; set; }
        public IEnumerable<USP_VIEW_PENDNIG_SR_Result> PENDING_SR { get; set; }


        /*Clouser Detail*/
        public IEnumerable<USP_VIEW_INVESTIGATION_CLOUSERDETAILS_Result> ClouserDet { get; set; }

        public int InvestigationIICAssignmentID { get; set; }
        public int InvestigationFileID { get; set; }
        public int IICAssigned { get; set; }
        public int AssignedBy { get; set; }
        public System.DateTime DateCreated { get; set; }
        public System.DateTime DateUpdated { get; set; }
        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }

        /*Investigator Dashboard Detail*/
        public IEnumerable<USP_VIEW_INVESTIGATIONDETAILS_BY_OCCURRENCESTATUSTYPE_ID_Result> InvDashBoard { get; set; }
        //public IEnumerable<USP_VIEW_INVESTIGATIONDETAILS_FOR_TEAMMEMBER_DASHBOARD_Result> InvTeamMemDashBoard { get; set; } code commented for jira ticket
        public IEnumerable<USP_VIEW_INVESTIGATIONDETAILS_FOR_MULTIPLE_TEAMMEMBER_DASHBOARD_Result> InvTeamMemDashBoard { get; set; }

        public IEnumerable<USP_VIEW_INVESTIGATIONDETAILS_BY_OCCURRENCESTATUSTYPE_IN_SR_Result> srInvestigationDashBoard { get; set; }
        public string[] TeammemberList { get; set; }


        /*DI dashboard*/

        public IEnumerable<USP_VIEW_OCCURRENCE_FOR_DI_Result> diDashBoard { get; set; }
        public int Occurrenceyear { get; set; }
        public int LKOccurrenceStatusTypeId { get; set; }

        public int InvOccurrenceyear { get; set; }
        public int InvLKOccurrenceStatusTypeId { get; set; }
        public int TaskInvOccurrenceyear { get; set; }
        public int InvInvestigationId { get; set; }


        /**Master Table**/
        //public int ID { get; set; }
        //public Nullable<int> LKCOMMONID { get; set; }
        //public string LKCOMMONTYPE { get; set; }
        //public string LKVALUE { get; set; }

        /**InvestigationFile**/


        //public int InvestigationFileID { get; set; }
        //public string InvestigationFileNumber { get; set; }
        //public int OccurrenceID { get; set; }
        //public string NotificationID { get; set; }
        //public int LKInvestigationStatusTypeID { get; set; }
        //public System.DateTime DateCreated { get; set; }
        //public Nullable<System.DateTime> DateUpdated { get; set; }
        //public int CreatedBy { get; set; }
        //public Nullable<int> UpdatedBy { get; set; }


        /**InvestigationFileDetail**/
        //public int InvestigationFileDetailID { get; set; }
        ////public Nullable<int> InvestigationFileID { get; set; }
        //public Nullable<int> LKIncidentTypeID { get; set; }
        //public string NotifierName { get; set; }
        //public string NotifierPosition { get; set; }
        //public string NotifierOrganization { get; set; }
        //public string NotifierContactNumber { get; set; }
        //public Nullable<System.DateTime> NotifierCallDate { get; set; }
        //public Nullable<System.DateTime> LocalDateAndTimeOfOccurrence { get; set; }
        //public Nullable<System.DateTime> UTCDateAndTimeOfOccurrence { get; set; }
        //public Nullable<int> LKCountryID { get; set; }
        //public string PlaceOfIncident { get; set; }
        //public string FlightNumber { get; set; }
        //public Nullable<decimal> PlaceOfIncidentLatitude { get; set; }
        //public Nullable<decimal> PlaceOfIncidentLongitude { get; set; }
        //public Nullable<int> LKDepartureAirportID { get; set; }
        //public Nullable<int> LKDestinationAirportID { get; set; }
        //public byte[] EventDescription { get; set; }
        //public Nullable<int> PassengersOnBoard { get; set; }
        //public Nullable<int> CrewOnBoard { get; set; }
        //public Nullable<int> PassengersKilled { get; set; }
        //public Nullable<int> CrewsKilled { get; set; }
        //public Nullable<int> OthersKilled { get; set; }
        //public Nullable<int> PassengersWithFatalInjury { get; set; }
        //public Nullable<int> CrewsWithFatalInjury { get; set; }
        //public Nullable<int> OthersWithFatalInjury { get; set; }
        //public Nullable<int> PassengersWithSeriousInjury { get; set; }
        //public Nullable<int> CrewsWithSeriousInjury { get; set; }
        //public Nullable<int> OthersWithSeriousInjury { get; set; }
        //public Nullable<int> PassengersWithMinorInjury { get; set; }
        //public Nullable<int> CrewsWithMinorInjury { get; set; }
        //public Nullable<int> OthersWithMinorInjury { get; set; }
        //public Nullable<int> LKAircraftMakeID { get; set; }
        //public string AircraftModel { get; set; }
        //public Nullable<int> LKAircraftTypeID { get; set; }
        //public string RegistrationMarkingsOnAircraft { get; set; }
        //public byte[] AircraftEngineInfo { get; set; }
        //public Nullable<int> LKAircraftCategoryId { get; set; }
        //public string AircraftMaximumMass { get; set; }
        //public string NameOfRegisteredOwner { get; set; }
        //public Nullable<int> LKAirlineOperatorID { get; set; }
        //public string LKAirlineOperatorOther { get; set; }
        //public string OperatorState { get; set; }
        //public string OperatorContactNumber { get; set; }
        //public string QualificationOfPilot { get; set; }
        //public string PilotCertificateNumber { get; set; }
        //public byte[] DutyInvestigatorComments { get; set; }
        //public byte[] DutyInvestigatorSuggestions { get; set; }
        //public string DutyInvestigatorFurtherSuggestion { get; set; }
        //public string InvestigationType { get; set; }
        //public System.DateTime DateCreated { get; set; }
        //public Nullable<System.DateTime> DateUpdated { get; set; }
        //public Nullable<int> CreatedBy { get; set; }
        //public Nullable<int> UpdatedBy { get; set; }

        //public virtual InvestigationFile InvestigationFile { get; set; }
        //public virtual LKIncidentType LKIncidentType { get; set; }
        //public virtual LKCountry LKCountry { get; set; }
        //public virtual LKAirport LKAirport { get; set; }
        //public virtual LKAirport LKAirport1 { get; set; }
        //public virtual LKAircraftMake LKAircraftMake { get; set; }
        //public virtual LKAircraftType LKAircraftType { get; set; }
        //public virtual LKAirlineOperator LKAirlineOperator { get; set; }
        //public virtual LKAircraftCategory LKAircraftCategory { get; set; }


        //public IEnumerable<InvestigationClosureDetail> Inv_Close { get; set; }
        // public int InvestigationFileID { get; set; }
        public string InvestigationClosureReason { get; set; }
        public Nullable<int> ClosureRecommendedBy { get; set; }
        public int InvestigationClosedBy { get; set; }
        //public System.DateTime DateCreated { get; set; }
        //public System.DateTime DateUpdated { get; set; }
        //public int CreatedBy { get; set; }
        //public int UpdatedBy { get; set; }

        public string InvSearchtext { get; set; }
        public string InvestigationNumber { get; set; }       
    }

}