﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using ISM_Project.Models;

namespace ISM_Project
{
    /// <summary>
    ///  Background Worker class used for upload and save the investigation large files 
    /// </summary> 
    /// 
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1001:TypesThatOwnDisposableFieldsShouldBeDisposable")]
    public class Backgroundworker
    {
        Common comm = new Common();       
        static string[] SharepointfilePath = new string[100];
        static string[] SharepointAnonymousPath = new string[100];
        gcaa_ismEntities gcaa = new gcaa_ismEntities();
        static int[] DocumentTypeId = new int[100];

        /// <summary>
        /// Background worker used for save the large files into sharepoint 
        /// </summary> 
        /// 
        ///<param name="UploadedFile"> Receive attached file content </param>
        public void startprocess(HttpFileCollectionBase UploadedFile)
        {
            int i = 0;
            //  comm.CreateIfMissing(System.Web.HttpContext.Current.Server.MapPath("~/") + "App_Data/Form35_Files/" + OccurrenceId);

            for (int filecount = 0; filecount < UploadedFile.Count; filecount++)
            {
                HttpPostedFileBase file = UploadedFile[filecount];

                if (file.ContentLength > 0 && !string.IsNullOrEmpty(file.FileName))
                {
                    int fileSize = file.ContentLength;

                    //string fileName = file.FileName;
                    string fileName = file.FileName.Substring(0, file.FileName.LastIndexOf('.')) + "_" + DateTime.Now.ToFileTime() + file.FileName.Substring(file.FileName.LastIndexOf('.'), ((file.FileName.Length) - (file.FileName.LastIndexOf('.'))));

                    string mimeType = file.ContentType.Split('/')[1].ToString();

                    DocumentTypeId[i] = Convert.ToInt32(gcaa.LKDocumentTypes.AsQueryable().Where(d => d.LKDocumentTypeName == mimeType).Select(d => d.LKDocumentTypeID).FirstOrDefault());

                    Stream fileContent = file.InputStream;

                    //   file.SaveAs(Server.MapPath("~/") + "CorrespondenceFiles\\" + fileName);

                    //string[] attachURLs = comm.savetoSharePointDocs(fileContent, Server.MapPath("~/") + "CorrespondenceFiles\\" + fileName, CONTROL_NUMER, "Corres_Documents");

                    string[] attachURLs = comm.savetoSharePointLargeFilessOnPrem(fileContent, fileName, 1, "Documents");

                    SharepointfilePath[filecount] = attachURLs[0];

                    SharepointAnonymousPath[filecount] = attachURLs[1];

                    //   var result = gcaa.USP_INST_INVESTIGATION_DOCUMENTASATTACHMENT(0, InvestigationFileID, 1, DocumentTypeId, SharepointfilePath);

                    //   gcaa.SaveChanges();
                    filecount++;
                }
            }
        }
    }
}