﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ISM_Project.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class Occurrence_detail
    {
        public int OccurrenceDetailID { get; set; }
        public int NoofAircraft { get; set; }
        public string[] FlightNumber { get; set; }
        public int[] LKDestinationAirportID { get; set; }
        public int[] LKDepartureAirportID { get; set; }
        public string[] LongLat { get; set; }
        public int[] PassengerOnboard { get; set; }
        public int[] CrewOnboard { get; set; }
        public int[] CrewsWithFatalInjury { get; set; }
        public int[] PassengersWithFatalInjury { get; set; }
        public int[] OthersWithFatalInjury { get; set; }
        public int[] CrewsWithSeriousInjury { get; set; }
        public int[] PassengersWithSeriousInjury { get; set; }
        public int[] OthersWithSeriousInjury { get; set; }
        public int[] CrewsWithMinorInjury { get; set; }
        public int[] PassengersWithMinorInjury { get; set; }
        public int[] OthersWithMinorInjury { get; set; }
        public int[] CrewsKilled { get; set; }
        public int[] PassengersKilled { get; set; }
        public int[] OthersKilled { get; set; }
        public int[] LKAircraftModelID { get; set; }
        public int[] LKAircraftcategoryid { get; set; }
        public int[] LKAircraftRegistrationID { get; set; }
        public string[] EngineInfo { get; set; }
        public string[] MaximumMass { get; set; }
        public int[] AircraftCategoryId { get; set; }
        public string[] StateofRegistry { get; set; }
        public int[] LKAirlineOperatorID { get; set; }
        public string[] stateofoperatorid { get; set; }
        public string[] OperatorContacts { get; set; }
        public string[] msn { get; set; }
        public Nullable<int> LKCountryID { get; set; }
        public string PlaceofIncident { get; set; }
        public Nullable<System.DateTime> LocalDateAndTimeOfOccurrence { get; set; }
        public Nullable<System.DateTime> DatetimeofCall { get; set; }
        public Nullable<System.DateTime> UTCDateAndTimeOfOccurrence { get; set; }
        public Nullable<System.DateTime> OccurrenceDateTime { get; set; }
        
        public Nullable<int> LKIncidentTypeID { get; set; }
        public string PrevailingWeatherConditions { get; set; }
        public string ExtendOfDamageToAircraft { get; set; }
        public string DamagesToGroundObjects { get; set; }
        public string DescriptionOfExplosivesAndDangerousArticles { get; set; }
        public string DescriptionOfOccurrence { get; set; }
        public int LKOccurrenceStatusTypeId { get; set; }
        public Nullable<int> LKOccurrenceInformerID { get; set; }
        public System.DateTime DateCreated { get; set; }
        public System.DateTime DateUpdated { get; set; }
        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }
        public string Notifier { get; set; }
        public string Notifier_Contact { get; set; }
        public bool DIsecurityNotify { get; set; }
        public string NotificationDescription { get; set; }

        public int[] CabinCrew { get; set; }
        public Nullable<int> OccurrenceCategorizationId { get; set; }

        public string UpdatedInformation { get; set; }
        public int[] TotalCrewsInjuries { get; set; }
        public int[] TotalPassengersInjuries { get; set; }
        public int[] LKDivertedToAirportID { get; set; }

        [DisplayFormat(DataFormatString = "{NOTN/0000/0000}", ApplyFormatInEditMode = true)]
        [Display(Prompt = "Enter Valid Format")]
        public string NotificationId { get; set; }        

    }
}