﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ISM_Project.Models
{
    public class Notify
    {
        public int notn_id { get; set; }
        public string notn_num { get; set; }
        public string aircraft_make { get; set; }
        public string model { get; set; }
        public string aircraft_type_id { get; set; }
        public string flight_number { get; set; }
        public string nationality { get; set; }
        public string registration { get; set; }
        public string distribution_list { get; set; }
        public string path_to_form35 { get; set; }
        public Nullable<int> created_by { get; set; }
        public Nullable<System.DateTime> created_date { get; set; }
        public Nullable<int> status { get; set; }
        public string aircraft_marking { get; set; }
        public string operator_name { get; set; }
        public string qualification_of_pilot { get; set; }
        public string pilot_certificate_no { get; set; }
        public Nullable<System.DateTime> date_and_time_accident { get; set; }
        public string last_of_point_depart_id { get; set; }
        public string point_intended_landing_id { get; set; }
        public string geographical_postion { get; set; }
        public string no_of_person_abroad { get; set; }
        public string no_of_serious_injury { get; set; }
        public string no_killed { get; set; }
        public string no_injured { get; set; }
        public string type_of_accident_id { get; set; }
        public string weather_condition { get; set; }
        public string extent_of_damage { get; set; }
        public string damage_to_object_on_ground { get; set; }
        public string informing_party_id { get; set; }
        public string description_of_explosives { get; set; }
    }
}