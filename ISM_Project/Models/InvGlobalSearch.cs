﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ISM_Project.Models
{
    public class InvGlobalSearch
    {
        public IEnumerable<USP_VIEW_OCCURRENCE_FOR_DI_Result> InvDiDashboard { get; set; }

        public IEnumerable<USP_VIEW_INVESTIGATIONDETAILS_BY_OCCURRENCESTATUSTYPE_ID_Result> InvDashBoard { get; set; }

        public IEnumerable<USP_VIEW_INVESTIGATIONDETAILS_FOR_TEAMMEMBER_DASHBOARD_Result> InvTeamMemDashBoard { get; set; }

        public IEnumerable<USP_VIEW_ALL_OCCURRENCE_Result> InvAllOccurance { get; set; }

        public IEnumerable<USP_VIEW_INVESTIGATION_Result> InvAllInvestig { get; set; }

        public IEnumerable<USP_VIEW_ALL_USERS_Result> invUser { get; set; }

        public IEnumerable<USP_VIEW_GLOBAL_DISTRIBUTIONLIST_Result> invGlobalDL { get; set; }
    }
}