//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ISM_Project.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class InvestigationMultipleTaskAssignment
    {
        public int InvestigationMultipleTaskAssignmentID { get; set; }
        public int InvestigationTaskAssignmentID { get; set; }
        public int TeamMemberID { get; set; }
        public System.DateTime DateCreated { get; set; }
        public System.DateTime DateUpdated { get; set; }
        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }
    
        public virtual InvestigationTaskAssignment InvestigationTaskAssignment { get; set; }
    }
}
