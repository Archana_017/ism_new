﻿//using IronPdf;
using ISM_Project.Models;
//using iTextSharp.text;
//using iTextSharp.tool.xml;
//using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using TheArtOfDev.HtmlRenderer.PdfSharp;
//using iTextSharp.text.html;
//using TheArtOfDev.HtmlRenderer.PdfSharp;

namespace ISM_Project
{
    /// <summary>
    /// Pdf Generation class
    /// </summary>
    public class PdfGeneration
    {
        gcaa_ismEntities db = new gcaa_ismEntities();
        Common comm = new Common();
        //public string generatePdf(dynamic data, dynamic subdata, dynamic files, string type)
        //{
        //    string filepath = string.Empty;
        //    try
        //    {
        //        string fileName = string.Empty;
        //        if (type == "Form35" || type == "Form39")
        //            fileName = "Form 035_" + Convert.ToString(DateTime.Now.ToFileTime()) + ".pdf";
        //        if (type == "notify")
        //            fileName = "Notification_" + Convert.ToString(DateTime.Now.ToFileTime()) + ".pdf";
        //        if (type == "CorresSR")
        //            fileName = "SR_" + Convert.ToString(DateTime.Now.ToFileTime()) + ".pdf";
        //        //filepath = System.Web.HttpContext.Current.Server.MapPath(Path.Combine("~/App_data", fileName));
        //        comm.CreateIfMissing(HttpContext.Current.Server.MapPath("~/") + "MailAttachments");
        //        filepath = HttpContext.Current.Server.MapPath(Path.Combine("~/MailAttachments", fileName));

        //        StringReader srn = new StringReader((generateHTMLContent(data, subdata, files, type)));

        //        Byte[] res = PdfSharpConvert(srn.ReadToEnd().ToString());
        //        FileStream fs1 = new FileStream(filepath, FileMode.Create);
        //        fs1.Write(res, 0, res.Length);
        //        fs1.Close();


        //        using (FileStream fs = new FileStream(filepath, FileMode.Create))
        //        {

        //            Document document1 = new Document(PageSize.A4, 0, 0, 0, 0);
        //            Document document = new Document(PageSize.A4, 0, 0, 0, 0);
        //            //   HTMLWorker htmlparser = new HTMLWorker(document);

        //            PdfWriter writer = PdfWriter.GetInstance(document, fs);


        //            //StreamReader str = new StreamReader(System.Web.HttpContext.Current.Server.MapPath(Path.Combine("~/App_data", "Form-35_Filled.html")));
        //            //string content = str.ReadToEnd();
        //            //content = ReplaceContent(content,data);
        //            //str.Close();
        //            //StringReader sr = new StringReader(content);

        //            StringReader sr = new StringReader((generateHTMLContent(data, subdata, files, type)));

        //            document.AddAuthor("Micke Blomquist");

        //            document.AddCreator("Sample application using iTextSharp");

        //            document.AddKeywords("PDF tutorial education");

        //            document.AddSubject("Document subject - Describing the steps creating a PDF document");

        //            document.AddTitle("The document title - PDF creation using iTextSharp");

        //            document.Open();

        //            // Add a simple and wellknown phrase to the document in a flow layout manner
        //            //    htmlparser.Parse(sr);
        //            XMLWorkerHelper.GetInstance().ParseXHtml(writer, document, sr);
        //            //document.Add(new Paragraph("Hello World!"));

        //            // Close the document

        //            document.Close();
        //            // Close the writer instance

        //            writer.Close();
        //            // Always close open filehandles explicity
        //            fs.Close();
        //        }
        //    }
        //    catch (System.Exception ex)
        //    {
        //        comm.Exception_Log("PdfGeneration", "generatePdf", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(HttpContext.Current.Session["UserId"]));

        //        //  filepath = string.Empty;
        //    }
        //    return filepath;
        //}
        /// <summary>
        /// generate Pdf
        /// </summary>
        /// <param name="data">Receive set of source files</param>
        /// <param name="subdata">Receive set of source files</param>
        /// <param name="files">Receive set of attachment files</param>
        /// <param name="type">Receive file type</param>
        /// <param name="siteURL">Receive the URL</param>
        /// <returns></returns>
        public string generatePdf(dynamic data, dynamic subdata, dynamic files, string type,string siteURL)
        {
            string filepath = string.Empty;

            //Document document = new Document(PageSize.A4, 0, 0, 0, 0);

            try
            {
                string fileName = string.Empty;
                if (type == "Form35" || type == "Form39")
                    fileName = "Form 035_" + Convert.ToString(DateTime.Now.ToFileTime()) + ".pdf";
                if (type == "notify")
                    fileName = "Notification_" + Convert.ToString(DateTime.Now.ToFileTime()) + ".pdf";
                if (type == "CorresSR" || type == "ResendCorresSR")
                    fileName = "SR_" + Convert.ToString(DateTime.Now.ToFileTime()) + ".pdf";
                //filepath = System.Web.HttpContext.Current.Server.MapPath(Path.Combine("~/App_data", fileName));
                comm.CreateIfMissing(HttpContext.Current.Server.MapPath("~/") + "MailAttachments");
                filepath = HttpContext.Current.Server.MapPath(Path.Combine("~/MailAttachments", fileName));

                //HtmlToPdf Renderer = new HtmlToPdf();
                //Renderer.RenderHtmlAsPdf(srn.ReadToEnd().ToString()).SaveAs(filepath);

                #region comm on 23 oct 2019

                //StringReader srn = new StringReader((generateHTMLContent(data, subdata, files, type)));

                //StringBuilder sb = new StringBuilder();

                //Byte[] res = comm.PdfSharpConvert(srn.ReadToEnd().ToString());

                //using (FileStream fis = new FileStream(filepath, FileMode.Create))
                //{
                //    fis.Write(res, 0, res.Length);

                //    fis.Position = 0;

                //    fis.Close();
                //}

                #endregion

                filepath = generateHTMLContent(data, subdata, files, type, siteURL);

                //using (FileStream fs = new FileStream(filepath, FileMode.Create))
                //{               
                //    //   HTMLWorker htmlparser = new HTMLWorker(document);

                //    PdfWriter writer = PdfWriter.GetInstance(document, fs);

                //    //StreamReader str = new StreamReader(System.Web.HttpContext.Current.Server.MapPath(Path.Combine("~/App_data", "Form-35_Filled.html")));
                //    //string content = str.ReadToEnd();
                //    //content = ReplaceContent(content,data);
                //    //str.Close();
                //    //StringReader sr = new StringReader(content);

                //    StringReader sr = new StringReader((generateHTMLContent(data, subdata, files, type)));

                //    document.AddAuthor("Micke Blomquist");

                //    document.AddCreator("Sample application using iTextSharp");

                //    document.AddKeywords("PDF tutorial education");

                //    document.AddSubject("Document subject - Describing the steps creating a PDF document");

                //    document.AddTitle("The document title - PDF creation using iTextSharp");

                //    document.Open();

                //    // Add a simple and wellknown phrase to the document in a flow layout manner
                //    //    htmlparser.Parse(sr);
                //    XMLWorkerHelper.GetInstance().ParseXHtml(writer, document, sr);
                //    //document.Add(new Paragraph("Hello World!"));

                //    // Close the document

                //    document.Close(); 
                //    // Close the writer instance

                //    writer.Close();
                //    // Always close open filehandles explicity
                //    fs.Close(); 
                //}
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("PdfGeneration", "generatePdf", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(HttpContext.Current.Session["UserId"]));

                //  filepath = string.Empty;
            }
            finally
            {
                //document.Dispose();
            }
            return filepath;
        }
        /// <summary>
        /// generate HTML Content
        /// </summary>
        /// <param name="parentValue">Receive set of source files</param>
        /// <param name="childValue">Receive set of source files</param>
        /// <param name="files">Receive set of attachment files</param>
        /// <param name="type">Receive file type</param>
        /// <param name="siteurl">Receive the URL</param>
        /// <returns></returns>
        public string generateHTMLContent(dynamic parentValue, dynamic childValue, dynamic files, string type,string siteurl)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                if (parentValue != null)
                {
                    if (type == "Form35" || type == "Form39")
                    {
                        //foreach (var items in parentValue)
                        //{
                        sb.Append("<div class='header'>");
                        sb.Append("<div style='width: 595px; margin: auto;'>");
                        sb.Append("<div style='border: 1px solid #00818c;margin-bottom: 15px;border: 1px solid #999;background-color:#09c4d2;font-weight:normal;text-align: center;padding: 8px;color: #fff;font-family: 'Titillium Web', sans-serif;font-size: 22px;'>DUTY INVESTIGATOR NOTIFICATION</div>");
                        sb.Append("<table style='width:595px;margin: auto;border-spacing: 0;border: 1px solid #00818c;margin-bottom: 15px;'>");

                        sb.Append("<thead>");
                        sb.Append("<tr>");
                        sb.Append("<th colspan='4' style='border:1px solid #999;background-color:#09c4d2;font-weight:bold;text-align: left;padding: 8px;color: #fff;font-family: 'Titillium Web', sans-serif;' > DI Notification </th>");
                        sb.Append("</tr>");
                        sb.Append("</thead>");

                        sb.Append("<tbody>");
                        if (type == "Form39")
                        {
                            sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                            sb.Append("<td style='border-right: 1px solid #00818c;border-bottom: 1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight: bold;font-size: 14px;'> AAI Case No </td><td colspan='3' style='border-right:1px solid #00818c;border-bottom: 1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight: normal;font-size: 14px;'> " + parentValue.InvestigationNumber + " </td>");
                            sb.Append("</tr >");
                        }
                        sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                        sb.Append("<td style='border-right: 1px solid #00818c;border-bottom: 1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight: bold;font-size: 14px;'> Notification Number </td><td colspan='3' style='border-right:1px solid #00818c;border-bottom: 1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight: normal;font-size: 14px;'> " + parentValue.NotificationID + " </td>");
                        sb.Append("</tr >");


                        sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                        sb.Append("<td style='border-right: 1px solid #00818c;border-bottom: 1px solid #00818c;padding:8px;font-family:Open Sans',sans-serif;font-weight:bold;font-size:14px;'> Notifier Name </td>");
                        sb.Append("<td  style='border-right: 1px solid #00818c;border-bottom: 1px solid #00818c;padding:8px;font-family:Open Sans',sans-serif;font-weight:normal;font-size:14px;'> " + parentValue.NotifierName + " </td>");
                        sb.Append("<td style='border-right: 1px solid #00818c;border-bottom: 1px solid #00818c;padding:8px;font-family:Open Sans',sans-serif;font-weight:bold;font-size:14px;'> Notifier Position </td>");
                        sb.Append("<td  style='border-right: 1px solid #00818c;border-bottom: 1px solid #00818c;padding:8px;font-family:Open Sans',sans-serif;font-weight:normal;font-size:14px;'> " + parentValue.NotifierPosition + " </td>");
                        sb.Append("</tr>");

                        sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'> Notifier Organization </td>");
                        sb.Append("<td  style='border-right:1px solid #00818c;border-bottom: 1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'> " + parentValue.NotifierOrganization + " </td>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom: 1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'> Notifier Contact No. </td>");
                        sb.Append("<td  style='border-right:1px solid #00818c;border-bottom: 1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'> " + parentValue.NotifierContactNumber + " </td>");
                        sb.Append("</tr>");

                        sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                        sb.Append("<td style='border-right:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'> Date & Time of Call </td><td colspan='3' style='border-right:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'> " + comm.convertDateTime(parentValue.NotifierCallDate) + " </td>");
                        sb.Append("</tr>");

                        sb.Append("</tbody>");
                        sb.Append("</table>");

                        //Occurence Info
                        sb.Append("<table style='width:595px;margin:auto;border-spacing:0;border:1px solid #00818c;margin-bottom:15px;'>");

                        sb.Append("<thead>");
                        sb.Append("<tr style='border:1px solid #00818c;'>");
                        sb.Append("<th colspan='2' style='border:1px solid #999;background-color:#09c4d2;font-weight:bold;text-align:left;padding:8px;color:#fff;font-family:'Titillium Web',sans-serif;'> Occurrence Info </th>");
                        sb.Append("</tr>");
                        sb.Append("</thead>");

                        sb.Append("<tbody style=''>");
                        sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                        sb.Append("<td  style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'> UTC Date & Time of Occurrence </td><td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'> " + comm.convertDateTime(parentValue.UTCDateAndTimeOfOccurrence) + "  </td>");
                        sb.Append("</tr>");

                        sb.Append("<tr style='border -bottom: 1px solid #00818c;'>");
                        sb.Append("<td  style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'> Local Date & Time of Occurrence </td><td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'> " + comm.convertDateTime(parentValue.LocalDateAndTimeOfOccurrence) + "  </td>");
                        sb.Append("</tr>");

                        sb.Append("<tr style='border -bottom: 1px solid #00818c;'>");
                        sb.Append("<td  style='border-right:1px solid #00818c;border-bottom: 1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>Place </td>");
                        sb.Append("<td  style='border-right:1px solid #00818c;border-bottom: 1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'> " + parentValue.PlaceOfIncident + "  </td>");
                        sb.Append("</tr>");

                        sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                        sb.Append("<td  style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Number of Aircrafts Involved</td>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + parentValue.NoOfAircraftInvolved + "</ td>");
                        sb.Append("</tr>");

                        //sb.Append("<tr style = 'border-bottom: 1px solid #00818c;'>");
                        //sb.Append("<td style = 'border-right: 1px solid #00818c;padding: 8px;font-family: 'Open Sans', sans-serif;font-weight: bold;font-size: 14px;' >None </ td >");
                        //sb.Append("< td style = 'border-right: 1px solid #00818c;padding: 8px;font-family: 'Open Sans', sans-serif;font-weight: normal;font-size: 14px;' >  </ td >");
                        //sb.Append("< td style = 'border-right: 1px solid #00818c;padding: 8px;font-family: 'Open Sans', sans-serif;font-weight: bold;font-size: 14px;' > </ td >");
                        //sb.Append("< td style = 'border-right: 1px solid #00818c;padding: 8px;font-family: 'Open Sans', sans-serif;font-weight: normal;font-size: 14px;' >  </ td >");
                        //sb.Append("</tr>");

                        //sb.Append("</tbody>");
                        //sb.Append("</table>");

                        ////Aircraft Info
                        //sb.Append("<table style='width:900px;margin:auto;border-spacing:0;border:1px solid #00818c;margin-bottom:15px;'>");

                        //sb.Append("<thead>");
                        //sb.Append("<tr style='border:1px solid #00818c;'>");
                        //sb.Append("<th  style='border:1px solid #999;background-color:#09c4d2;font-weight:bold;text-align:left;padding:8px;color:#fff;font-family:'Titillium Web',sans-serif;'>Aircraft Info </th>");
                        //sb.Append("</tr>");
                        //sb.Append("</thead>");

                        //sb.Append("<tbody style=''>");

                        //sb.Append("</tbody>");
                        //sb.Append("</table>");

                        ////Operator Info
                        //sb.Append("<table class='table' style='width:900px;margin:auto;border-spacing:0;border:1px solid #00818c;margin-bottom:15px;'>");

                        //sb.Append("<thead>");
                        //sb.Append("<tr style='border:1px solid #00818c;'>");
                        //sb.Append("<th  style='border:1px solid #999;background-color:#09c4d2;font-weight:bold;text-align:left;padding:8px;color:#fff;font-family:'Titillium Web',sans-serif;'>Operator Info </th>");
                        //sb.Append("</tr>");
                        //sb.Append("</thead>");

                        //sb.Append("<tbody style = ''>");

                        sb.Append("</tbody>");
                        sb.Append("</table>");

                        #region On 03-Aug-2018

                        sb.Append("<table class='table' style='width:595px;margin:auto;border-spacing:0;border:1px solid #00818c;margin-bottom:15px;'>");

                        sb.Append("<thead>");
                        sb.Append("<tr style='border:1px solid #00818c;'>");
                        sb.Append("<th colspan='4' style='border:1px solid #999;background-color:#09c4d2;font-weight:bold;text-align:left;padding:8px;color:#fff;font-family:'Titillium Web',sans-serif;'> Injuries to Persons</th>");
                        sb.Append("</tr>");
                        sb.Append("</thead>");

                        sb.Append("<tbody style=''>");
                        sb.Append("<tr style='border-bottom:1px solid #00818c;'>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>Injuries </td>");
                        sb.Append("<td  style='border-right: 1px solid #00818c;border-bottom: 1px solid #00818c;padding:8px;font-family:'Open Sans', sans-serif;font-weight:normal;font-size:14px;'> Crew</td>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom: 1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>Passengers  </td>");
                        sb.Append("<td  style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'> Others </td>");
                        sb.Append("</tr>");

                        sb.Append("<tr style='border-bottom:1px solid #00818c;'>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>Fatal </td>");
                        sb.Append("<td  style='border-right: 1px solid #00818c;border-bottom: 1px solid #00818c;padding:8px;font-family:'Open Sans', sans-serif;font-weight:normal;font-size:14px;'>  " + parentValue.CrewsWithFatalInjuryInfo + " </td>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom: 1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>" + parentValue.PassengersWithFatalInjuryInfo + "  </td>");
                        sb.Append("<td  style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'> " + parentValue.OthersWithFatalInjuryInfo + "  </td>");
                        sb.Append("</tr>");

                        sb.Append("<tr style='border-bottom:1px solid #00818c;'>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>Serious </td>");
                        sb.Append("<td  style='border-right: 1px solid #00818c;border-bottom: 1px solid #00818c;padding:8px;font-family:'Open Sans', sans-serif;font-weight:normal;font-size:14px;'>  " + parentValue.CrewsWithSeriousInjuryInfo + " </td>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom: 1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>" + parentValue.PassengersWithSeriousInjuryInfo + "  </td>");
                        sb.Append("<td  style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'> " + parentValue.OthersWithSeriousInjuryInfo + "  </td>");
                        sb.Append("</tr>");

                        sb.Append("<tr style='border-bottom:1px solid #00818c;'>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>Minor </td>");
                        sb.Append("<td  style='border-right: 1px solid #00818c;border-bottom: 1px solid #00818c;padding:8px;font-family:'Open Sans', sans-serif;font-weight:normal;font-size:14px;'>  " + parentValue.CrewsWithMinorInjuryInfo + " </td>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom: 1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>" + parentValue.PassengersWithMinorInjuryInfo + "  </td>");
                        sb.Append("<td  style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'> " + parentValue.OthersWithMinorInjuryInfo + "  </td>");
                        sb.Append("</tr>");

                        sb.Append("<tr style='border-bottom:1px solid #00818c;'>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>None </td>");
                        sb.Append("<td  style='border-right: 1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans', sans-serif;font-weight:normal;font-size:14px;'>  " + parentValue.CrewsKilledInfo + " </td>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>" + parentValue.PassengersKilledInfo + "  </td>");
                        sb.Append("<td  style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'> " + parentValue.OthersKilledInfo + "  </td>");
                        sb.Append("</tr>");

                        sb.Append("<tr style='border-bottom:1px solid #00818c;'>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>Total </td>");
                        sb.Append("<td  style='border-right: 1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans', sans-serif;font-weight:normal;font-size:14px;'>  " + parentValue.CrewsTotalInjury + " </td>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>" + parentValue.PassengersTotalInjury + "  </td>");
                        sb.Append("<td  style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'> " + parentValue.OthersTotalInjury + "  </td>");
                        sb.Append("</tr>");

                        sb.Append("</tbody>");
                        sb.Append("</table>");

                        #endregion

                        if (childValue.Count > 0)
                        {
                            int i = 0;
                            sb.Append("<table style='width:595px;margin:auto;border-spacing:0;margin-bottom:15px;'>");
                            sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                            sb.Append("<td  style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:0px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>");
                            foreach (var childItem in childValue)
                            {
                                i++;
                                sb.Append("<table class='table' style='width: 595px;margin:auto;border-spacing:0;border:1px solid #00818c;border-right:1px solid #00818c;margin-bottom:0px;'>");
                                sb.Append("<thead>");
                                sb.Append("<tr style='border:1px solid #00818c;'>");
                                sb.Append("<th colspan='4' style='border:1px solid #999;background-color:#09c4d2;font-weight:bold;text-align:left;padding:8px;color:#fff;font-family:'Titillium Web',sans-serif;'>Aircraft - " + i + " </th>");
                                sb.Append("</tr>");
                                sb.Append("</thead>");

                                sb.Append("<tbody style=''>");
                                sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                                sb.Append("<td style='border-right:1px solid #00818c;padding:8px;border-bottom: 1px solid #00818c;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'> Flight Number </td >");
                                sb.Append("<td  style='border-right:1px solid #00818c;border-bottom: 1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'> " + childItem.FlightNumber + "  </td>");
                                sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>Long & Lat</td>");
                                sb.Append("<td  style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'>" + childItem.latitudeAndlongitude + " </td>");
                                sb.Append("</tr>");

                                sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                                sb.Append("<td style='border-right:1px solid #00818c;border-bottom: 1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>Departure Airport  </td>");
                                sb.Append("<td  style='border-right:1px solid #00818c;border-bottom: 1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'> " + childItem.DepatureAirport + "  </td>");
                                sb.Append("<td style='border-right:1px solid #00818c;padding:8px;border-bottom: 1px solid #00818c;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'> Destination Airport</td >");
                                sb.Append("<td  style='border-right:1px solid #00818c;border-bottom: 1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'> " + childItem.DestinationAirport + "  </td>");
                                sb.Append("</tr>");

                                sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                                sb.Append("<td style='border-right:1px solid #00818c;border-bottom: 1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>Passengers Onboard  </td>");
                                sb.Append("<td  style='border-right:1px solid #00818c;border-bottom: 1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'> " + childItem.PassengersOnBoard + "  </td>");
                                sb.Append("<td style='border-right:1px solid #00818c;padding:8px;border-bottom: 1px solid #00818c;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'> Crew Onboard </td >");
                                sb.Append("<td  style='border-right:1px solid #00818c;border-bottom: 1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'> " + childItem.CrewOnBoard + "  </td>");
                                sb.Append("</tr>");

                                //sb.Append("<tr style='border-bottom:1px solid #00818c;'>");
                                //sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>Injuries </td>");
                                //sb.Append("<td  style='border-right: 1px solid #00818c;border-bottom: 1px solid #00818c;padding:8px;font-family:'Open Sans', sans-serif;font-weight:normal;font-size:14px;'> Crew</td>");
                                //sb.Append("<td style='border-right:1px solid #00818c;border-bottom: 1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>Passengers  </td>");
                                //sb.Append("<td  style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'> Others </td>");
                                //sb.Append("</tr>");

                                //sb.Append("<tr style='border-bottom:1px solid #00818c;'>");
                                //sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>Fatal </td>");
                                //sb.Append("<td  style='border-right: 1px solid #00818c;border-bottom: 1px solid #00818c;padding:8px;font-family:'Open Sans', sans-serif;font-weight:normal;font-size:14px;'>  " + childItem.CrewsWithFatalInjury + " </td>");
                                //sb.Append("<td style='border-right:1px solid #00818c;border-bottom: 1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>" + childItem.PassengersWithFatalInjury + "  </td>");
                                //sb.Append("<td  style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'> " + childItem.OthersWithFatalInjury + "  </td>");
                                //sb.Append("</tr>");

                                //sb.Append("<tr style='border-bottom:1px solid #00818c;'>");
                                //sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>Serious </td>");
                                //sb.Append("<td  style='border-right: 1px solid #00818c;border-bottom: 1px solid #00818c;padding:8px;font-family:'Open Sans', sans-serif;font-weight:normal;font-size:14px;'>  " + childItem.CrewsWithSeriousInjury + " </td>");
                                //sb.Append("<td style='border-right:1px solid #00818c;border-bottom: 1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>" + childItem.PassengersWithSeriousInjury + "  </td>");
                                //sb.Append("<td  style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'> " + childItem.OthersWithSeriousInjury + "  </td>");
                                //sb.Append("</tr>");

                                //sb.Append("<tr style='border-bottom:1px solid #00818c;'>");
                                //sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>Minor </td>");
                                //sb.Append("<td  style='border-right: 1px solid #00818c;border-bottom: 1px solid #00818c;padding:8px;font-family:'Open Sans', sans-serif;font-weight:normal;font-size:14px;'>  " + childItem.CrewsWithMinorInjury + " </td>");
                                //sb.Append("<td style='border-right:1px solid #00818c;border-bottom: 1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>" + childItem.PassengersWithMinorInjury + "  </td>");
                                //sb.Append("<td  style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'> " + childItem.OthersWithMinorInjury + "  </td>");
                                //sb.Append("</tr>");

                                //sb.Append("<tr style='border-bottom:1px solid #00818c;'>");
                                //sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>None </td>");
                                //sb.Append("<td  style='border-right: 1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans', sans-serif;font-weight:normal;font-size:14px;'>  " + childItem.CrewsKilled + " </td>");
                                //sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>" + childItem.PassengersKilled + "  </td>");
                                //sb.Append("<td  style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'> " + childItem.OthersKilled + "  </td>");
                                //sb.Append("</tr>");

                                sb.Append("<tr style='border-bottom:1px solid #00818c;'>");
                                sb.Append("<td style='border-bottom:1px solid #00818c;border-right:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>Make & Model </td>");
                                sb.Append("<td  style='border-bottom:1px solid #00818c;border-right:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'> " + childItem.AircraftModel + " </td>");
                                sb.Append("<td style='border-bottom:1px solid #00818c;border-right:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'> Registration </td>");
                                sb.Append("<td  style='border-bottom:1px solid #00818c;border-right:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'> " + childItem.AircraftRegistration + " </td>");
                                sb.Append("</tr>");

                                sb.Append("<tr style='border-bottom:1px solid #00818c;'>");
                                sb.Append("<td style='border-bottom:1px solid #00818c;border-right:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>Engine Info </td>");
                                sb.Append("<td   style='border-bottom:1px solid #00818c;border-right:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'> " + childItem.AircraftEngineInfo + " </td>");
                                sb.Append("<td style='border-bottom:1px solid #00818c;border-right:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'> Maximum Take Off Mass (kg) </td>");
                                sb.Append("<td   style='border-bottom:1px solid #00818c;border-right:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'> " + childItem.AircraftMaximumMass + " </td>");
                                sb.Append("</tr>");

                                sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                                sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>Aircraft Category </td>");
                                sb.Append("<td  style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'> " + childItem.AircraftCategory + " </td>");
                                sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'> State of Registry </td>");
                                sb.Append("<td  style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'> " + childItem.AircraftStateofRegistry + " </td>");
                                sb.Append("</tr>");

                                sb.Append("<tr style='border-bottom:1px solid #00818c;'>");
                                sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight: bold;font-size: 14px;'>Operator Name </td>");
                                sb.Append("<td  style='border-right: 1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight: normal;font-size: 14px;'> " + childItem.OperatorName + " </td>");
                                sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight: bold;font-size: 14px;'>State of the Operator</td>");
                                sb.Append("<td  style='border-right: 1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight: normal;font-size: 14px;'> " + childItem.StateofOperator + " </td>");
                                sb.Append("</tr>");

                                sb.Append("<tr style='border-bottom:1px solid #00818c;'>");
                                sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>Operator Contacts </td><td  style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'>" + childItem.OperatorContactNo + "</td>");
                                sb.Append("</tr>");

                                sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                                sb.Append("<td style='border-right:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'> MSN</td>");
                                sb.Append("<td  style='border-right:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'>" + childItem.MSNvalue + "</td>");
                                sb.Append("</tr>");

                                sb.Append("</tbody>");
                                sb.Append("</table>");
                            }
                            sb.Append("</td>");
                            sb.Append("</tr>");
                            sb.Append("</table>");
                        }

                        ////Classification
                        sb.Append("<table style='width:595px;margin:auto;border-spacing:0;border:1px solid #00818c;margin-bottom:15px;'>");

                        sb.Append("<thead>");
                        sb.Append("<tr style ='border:1px solid #00818c;'>");
                        sb.Append("<th colspan='2' style='border:1px solid #999;background-color:#09c4d2;font-weight:bold;text-align:left;padding:8px;color:#fff;font-family:'Titillium Web',sans-serif;'> Classification </th>");
                        sb.Append("</tr>");
                        sb.Append("</thead>");

                        sb.Append("<tbody style=''>");

                        sb.Append("<tr style='border-bottom:1px solid #00818c;'>");
                        sb.Append("<td style='border-right: 1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'> Status </td><td  style='border-right: 1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'>" + parentValue.IncidentType + "</td>");
                        sb.Append("</tr>");

                        sb.Append("<tr style='border-bottom:1px solid #00818c;'>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'> Occurrence Description </td><td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'>" + parentValue.EventDescription + " </td>");
                        sb.Append("</tr>");

                        sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Prevailing weather conditons at the accident or incident site</td>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + parentValue.PrevailingWeatherConditions + "</ td>");
                        sb.Append("</tr>");

                        sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Damage to objects or structures on the ground, if any</td>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + parentValue.DamagesToGroundObjects + "</ td>");
                        sb.Append("</tr>");

                        sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Extent of the damage to the aircraft</td>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + parentValue.ExtendOfDamageToAircraft + "</ td>");
                        sb.Append("</tr>");

                        sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                        sb.Append("<td style='border-right:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Description of any explosives,radioactive  materials or any other dangerous articles carried on the aircraft.</td>");
                        sb.Append("<td style='border-right:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + parentValue.DescriptionOfExplosivesAndDangerousArticles + "</ td>");
                        sb.Append("</tr>");

                        sb.Append("</tbody>");
                        sb.Append("</table>");

                        ////Attachments

                        sb.Append("<table class='table' style='width:595px;margin:auto;border-spacing:0;border:1px solid #00818c;margin-bottom:15px;'>");

                        sb.Append("<thead>");
                        sb.Append("<tr style='border:1px solid #00818c;'>");
                        sb.Append("<th  style='border:1px solid #999;background-color:#09c4d2;font-weight:bold;text-align:left;padding:8px;color:#fff;font-family:'Titillium Web',sans-serif;'> Attachments</th>");
                        sb.Append("</tr>");
                        sb.Append("</thead>");

                        sb.Append("<tbody style =''>");
                        int file_Count = 0;
                        if (files.Count > 0)
                        {
                            foreach (var file in files)
                            {
                                file_Count++;
                                string filename = Convert.ToString(file.AttachmentPublicUrl);
                                filename = filename.Substring(filename.LastIndexOf('/') + 1);
                                sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                                sb.Append("<td style='border-right: 1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>" + file_Count + " </td><td  style='border-right:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'> " + filename + "</td>");
                                sb.Append("</tr>");
                            }
                        }
                        else
                        {
                            sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                            sb.Append("<td  style='border-right:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'> No Attachments</td>");
                            sb.Append("</tr>");
                        }
                        sb.Append("</tbody>");
                        sb.Append("</table>");

                        ////DI Occurrence Assessment

                        sb.Append("<table style='width:595px;margin:auto;border-spacing:0;border:1px solid #00818c;margin-bottom:15px;'>");

                        sb.Append("<thead>");
                        sb.Append("<tr style='border:1px solid #00818c;'>");
                        sb.Append("<th colspan='2' style='border:1px solid #999;background-color:#09c4d2;font-weight:bold;text-align:left;padding:8px;color:#fff;font-family:'Titillium Web',sans-serif;'> DI Occurrence Assessment</th>");
                        sb.Append("</tr>");
                        sb.Append("</thead>");

                        sb.Append("<tbody style = ''>");

                        sb.Append("<tr style='border-bottom:1px solid #00818c;'>");
                        sb.Append("<td style='border-bottom:1px solid #00818c;border-right:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'> Is the occurrence classified as an Accident or Serious Incident?</td>");
                        sb.Append("<td  style='border-bottom:1px solid #00818c;border-right:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'>" + parentValue.AccidentOrSeriousIncident + "</td>");
                        sb.Append("</tr>");

                        sb.Append("<tr style='border-bottom:1px solid #00818c;'>");
                        sb.Append("<td style='border-bottom:1px solid #00818c;border-right:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'> Does the occurrence involve a large or small aircraft?</td>");
                        sb.Append("<td  style='border-bottom:1px solid #00818c;border-right:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'> " + parentValue.LargeOrSmallAircraft + "</td>");
                        sb.Append("</tr>");

                        sb.Append("<tr style='border-bottom:1px solid #00818c;'>");
                        sb.Append("<td style='border-bottom:1px solid #00818c;border-right:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'> Commercial or Private use?</td>");
                        sb.Append("<td  style='border-bottom:1px solid #00818c;border-right:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'> " + parentValue.CommercialOrPrivate + "</td>");
                        sb.Append("</tr>");

                        sb.Append("<tr style='border-bottom:1px solid #00818c;'>");
                        sb.Append("<td style='border-bottom:1px solid #00818c;border-right:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>Involve a UAE registered aircraft or UAE operator?</td>");
                        sb.Append("<td  style='border-bottom:1px solid #00818c;border-right:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'> " + parentValue.UAERegisteredAircraft + "</td>");
                        sb.Append("</tr>");

                        sb.Append("<tr style='border-bottom:1px solid #00818c;'>");
                        sb.Append("<td style='border-bottom:1px solid #00818c;border-right:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>Is there a trend on the same operator, aircraft type, kind of operation as per the review of ROSI?</td>");
                        sb.Append("<td  style='border-bottom:1px solid #00818c;border-right:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'>" + parentValue.TrendOnOperatorOrAircraftType + "</td>");
                        sb.Append("</tr>");

                        sb.Append("<tr style='border-bottom:1px solid #00818c;'>");
                        sb.Append("<td style='border-bottom:1px solid #00818c;border-right:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'> Is there any anticipated safety value of an investigation?</td>");
                        sb.Append("<td  style='border-bottom:1px solid #00818c;border-right:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'> " + parentValue.AnticipatedSafetyValueOfInvestigation + "</td>");
                        sb.Append("</tr>");

                        sb.Append("<tr style='border-bottom:1px solid #00818c;'>");
                        sb.Append("<td style='border-bottom:1px solid #00818c;border-right:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>Is it expected to make a potential impact on public confidence on aviation safety?</td>");
                        sb.Append("<td  style='border-bottom:1px solid #00818c;border-right:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'> " + parentValue.PotentialImpactOnPublicConfidence + "</td>");
                        sb.Append("</tr>");

                        sb.Append("<tr style='border-bottom:1px solid #00818c;'>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'> Have a relevance to an identified and targeted safety program?</td>");
                        sb.Append("<td  style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'>" + parentValue.HaveRelevanceToSafetyProgram + "</td>");
                        sb.Append("</tr>");

                        sb.Append("<tr style='border-bottom:1px solid #00818c;' >");
                        sb.Append("<td style='border-bottom:1px solid #00818c;border-right:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>Are there enough available resources and projected to be available in the event of conflicting priorities?</td>");
                        sb.Append("<td  style='border-bottom:1px solid #00818c;border-right:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'>" + parentValue.ResourceAvailability + "</td>");
                        sb.Append("</tr>");

                        sb.Append("<tr style='border-bottom:1px solid #00818c;'>");
                        sb.Append("<td style='border-bottom:1px solid #00818c;border-right:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>Is there any other opportunity to be investigated by other investigation authority? Or to be internally investigated by the concerned operator?</td>");
                        sb.Append("<td  style='border-bottom:1px solid #00818c;border-right:1px solid #00818c;padding:8px;font-family:'Open Sans', sans-serif;font-weight:normal;font-size:14px;'>" + parentValue.OppurtunityForOtherInvestigationAuthorities + "</td>");
                        sb.Append("</tr>");

                        sb.Append("<tr style='border-bottom:1px solid #00818c;'>");
                        sb.Append("<td style='border-bottom:1px solid #00818c;border-right:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'> What is the time gap between the occurrence and its notification?</td>");
                        sb.Append("<td  style='border-bottom:1px solid #00818c;border-right:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + parentValue.TimeBetweenOccurrenceAndNotification + "</td>");
                        sb.Append("</tr>");

                        sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom: 1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>Does the occurrence have training benefits for the AAIS investigators?</td>");
                        sb.Append("<td  style='border-right:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + parentValue.TrainingBenefitsToAAISInvestigators + "</td>");
                        sb.Append("</tr>");

                        sb.Append("</tbody>");
                        sb.Append("</table>");

                        //DI Suggestions
                        sb.Append("<table style='width:595px;margin:auto;border-spacing:0;border:1px solid #00818c;margin-bottom:15px;'>");

                        sb.Append("<thead>");
                        sb.Append("<tr style='border:1px solid #00818c;'>");
                        sb.Append("<th colspan='2' style='border:1px solid #999;background-color:#09c4d2;font-weight:bold;text-align:left;padding:8px;color:#fff;font-family:'Titillium Web',sans-serif;'> DI Suggestions and Further Investigation </th>");
                        sb.Append("</tr>");
                        sb.Append("</thead>");

                        sb.Append("<tbody style=''>");

                        sb.Append("<tr style='border-bottom:1px solid #00818c;'>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'> DI Name</ td>");
                        sb.Append("<td  style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'>" + parentValue.FirstName + "</td>");
                        sb.Append(" </tr>");

                        sb.Append("<tr style='border-bottom:1px solid #00818c;'>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'> Comments</td>");
                        sb.Append("<td  style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'> " + parentValue.DutyInvestigatorComments + "</td>");
                        sb.Append("</tr>");

                        sb.Append("<tr style='border-bottom:1px solid #00818c;'>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'> DI Suggestion</td>");
                        sb.Append("<td  style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'> " + parentValue.DutyInvestigatorSuggestions + "</td>");
                        sb.Append("</tr>");

                        sb.Append("<tr style='border-bottom:1px solid #00818c;'>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'> DI Further Suggestion</td>");
                        sb.Append("<td  style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'> " + parentValue.DutyInvestigatorFurtherSuggestion + "</td>");
                        sb.Append("</tr>");

                        sb.Append("<tr style='border-bottom:1px solid #00818c;'>");
                        sb.Append("<td style='border-right:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'> Investigation Type</td>");
                        sb.Append("<td  style='border-right:1px solid #00818c;adding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'> " + parentValue.InvestigationType + "</td>");
                        sb.Append("</tr>");

                        sb.Append("</tbody>");
                        sb.Append("</table>");

                        sb.Append("</div>");
                        sb.Append("</div>");
                    }

                    #region commented on 15-oct-2019  based on client advice
                    //if (type == "notify")
                    //{
                    //    //foreach (var items in parentValue)
                    //    // {

                    //    sb.Append("<div class='header' style='width: 595px;'>");
                    //    sb.Append("<div style='width:595px; margin: auto;'>");
                    //    sb.Append("<div style='border: 1px solid #00818c;margin-bottom: 15px;border: 1px solid #999;background-color:#09c4d2;font-weight:normal;text-align: center;padding: 8px;color: #fff;font-family: 'Titillium Web', sans-serif;font-size: 22px;'>DUTY INVESTIGATOR NOTIFICATION</div>");
                    //    sb.Append("<table style='width: 595px;margin:auto;border-spacing:0;border:1px solid #00818c;margin-bottom:15px;'>");

                    //    sb.Append("<thead>");
                    //    sb.Append("<tr style='border:1px solid #00818c;'>");
                    //    sb.Append("<th colspan='2' style='border:1px solid #999;background-color:#09c4d2;font-weight:bold;text-align:left;padding:8px;color:#fff;font-family:'Titillium Web',sans-serif;'>Initial Information </th>");
                    //    sb.Append("</tr>");
                    //    sb.Append("</thead>");

                    //    sb.Append("<tbody>");

                    //    sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                    //    sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Notification Number</td>");
                    //    sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + parentValue.NotificationId + "</ td>");
                    //    sb.Append("</tr>");

                    //    sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                    //    sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Number of Aircrafts Involved</td>");
                    //    sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + parentValue.NoOfAircraftInvolved + "</ td>");
                    //    sb.Append("</tr>");

                    //    sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                    //    sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Date and Time of Accident </td>");
                    //    sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + comm.convertDateTime(parentValue.OccurrenceDate) + "</ td>");
                    //    sb.Append("</tr>");

                    //    sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                    //    sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Date and Time of Call </td>");
                    //    sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + comm.convertDateTime(parentValue.CallDate) + "</ td>");
                    //    sb.Append("</tr>");

                    //    sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                    //    //sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Nationality </td>");

                    //    sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Location of Occurrence </td>");
                    //    sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + parentValue.Location + "</ td>");
                    //    sb.Append("</tr>");

                    //    sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                    //    sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Type of accident</td>");
                    //    sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + parentValue.IncidentType + "</ td>");
                    //    sb.Append("</tr>");

                    //    //sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                    //    //sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Prevailing weather conditons at the accident or incident site</td>");
                    //    //sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + parentValue.PrevailingWeatherConditions + "</ td>");
                    //    //sb.Append("</tr>");

                    //    //sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                    //    //sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Damage to objects or structures on the ground, if any</td>");
                    //    //sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + parentValue.DamagesToGroundObjects + "</ td>");
                    //    //sb.Append("</tr>");

                    //    //sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                    //    //sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Extent of the damage to the aircraft</td>");
                    //    //sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + parentValue.ExtendOfDamageToAircraft + "</ td>");
                    //    //sb.Append("</tr>");

                    //    //sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                    //    //sb.Append("<td style='border-right:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Description of any explosives,radioactive  materials or any other dangerous articles carried on the aircraft.</td>");
                    //    //sb.Append("<td style='border-right:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + parentValue.DescriptionOfExplosivesAndDangerousArticles + "</ td>");
                    //    //sb.Append("</tr>");

                    //    sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                    //    sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Description of Occurrence</td>");
                    //    sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + parentValue.OccurrenceDescription + "</ td>");
                    //    sb.Append("</tr>");

                    //    sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                    //    sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Notifier</td>");
                    //    sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + parentValue.OccurrenceInformer + "</ td>");
                    //    sb.Append("</tr>");

                    //    sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                    //    sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Notifier Contact</td>");
                    //    sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + parentValue.NotifierContact + "</ td>");
                    //    sb.Append("</tr>");

                    //    sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                    //    sb.Append("<td colspan='2' style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>");
                    //    int i = 0;
                    //    foreach (var childItem in childValue)
                    //    {
                    //        i++;

                    //        sb.Append("<table class='table' style='width:595px;margin:auto;border-spacing:0;border:1px solid #00818c;margin-bottom:15px;'>");

                    //        sb.Append("<thead>");
                    //        sb.Append("<tr style='border:1px solid #00818c;'>");
                    //        sb.Append("<th colspan='2' style='border:1px solid #999;background-color:#09c4d2;font-weight:bold;text-align:left;padding:8px;color:#fff;font-family:'Titillium Web',sans-serif;'>Aircraft - " + i + " </th>");
                    //        sb.Append("</tr>");
                    //        sb.Append("</thead>");

                    //        sb.Append("<tbody style=''>");

                    //        sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                    //        sb.Append("<td colspan='1' style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Flight Number </td>");
                    //        sb.Append("<td colspan='1' style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + childItem.FlightNumber + "</ td>");
                    //        sb.Append("</tr>");

                    //        sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                    //        sb.Append("<td colspan='1' style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Departure Airport </td>");
                    //        sb.Append("<td colspan='1' style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + childItem.DepartureAirport + "</ td>");
                    //        sb.Append("</tr>");

                    //        sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                    //        sb.Append("<td colspan='1' style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Destination Airport </td>");
                    //        sb.Append("<td colspan='1' style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + childItem.DestinationAirport + "</ td>");
                    //        sb.Append("</tr>");

                    //        sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                    //        sb.Append("<td colspan='1' style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Long & Lat </td>");
                    //        sb.Append("<td colspan='1' style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + childItem.LatitudeandLongitude + "</ td>");
                    //        sb.Append("</tr>");

                    //        sb.Append("<tr style='border-bottom:1px solid #00818c;'>");
                    //        sb.Append("<td colspan='1' style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>Passengers Onboard</td>");
                    //        sb.Append("<td colspan='1'  style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'>" + childItem.PassengersOnBoard + "</td>");
                    //        sb.Append("</tr>");

                    //        sb.Append("<tr style='border-bottom:1px solid #00818c;'>");
                    //        sb.Append("<td colspan='1' style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>Crew Onboard</td>");
                    //        sb.Append("<td  colspan='1' style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'>" + childItem.CrewOnBoard + "</td>");
                    //        sb.Append("</tr>");

                    //        //sb.Append("<tr style='border-bottom:1px solid #00818c;'>");
                    //        //sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>Injuries </td>");
                    //        //sb.Append("<td colspan='2' style='border-right: 1px solid #00818c;border-bottom: 1px solid #00818c;padding:8px;font-family:'Open Sans', sans-serif;font-weight:normal;font-size:14px;'> Crew</td>");
                    //        //sb.Append("<td style='border-right:1px solid #00818c;border-bottom: 1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>Passengers  </td>");
                    //        //sb.Append("<td colspan='2' style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'> Others </td>");
                    //        //sb.Append("</tr>");

                    //        //sb.Append("<tr style='border-bottom:1px solid #00818c;'>");
                    //        //sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>Fatal </td>");
                    //        //sb.Append("<td colspan='2' style='border-right: 1px solid #00818c;border-bottom: 1px solid #00818c;padding:8px;font-family:'Open Sans', sans-serif;font-weight:normal;font-size:14px;'>  " + childItem.CrewsWithFatalInjury + " </td>");
                    //        //sb.Append("<td style='border-right:1px solid #00818c;border-bottom: 1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>" + childItem.PassengersWithFatalInjury + "  </td>");
                    //        //sb.Append("<td colspan='2' style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'> " + childItem.OthersWithFatalInjury + "  </td>");
                    //        //sb.Append("</tr>");

                    //        //sb.Append("<tr style='border-bottom:1px solid #00818c;'>");
                    //        //sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>Serious </td>");
                    //        //sb.Append("<td colspan='2' style='border-right: 1px solid #00818c;border-bottom: 1px solid #00818c;padding:8px;font-family:'Open Sans', sans-serif;font-weight:normal;font-size:14px;'>  " + childItem.CrewsWithSeriousInjury + " </td>");
                    //        //sb.Append("<td style='border-right:1px solid #00818c;border-bottom: 1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>" + childItem.PassengersWithSeriousInjury + "  </td>");
                    //        //sb.Append("<td colspan='2' style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'> " + childItem.OthersWithSeriousInjury + "  </td>");
                    //        //sb.Append("</tr>");

                    //        //sb.Append("<tr style='border-bottom:1px solid #00818c;'>");
                    //        //sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>Minor </td>");
                    //        //sb.Append("<td colspan='2' style='border-right: 1px solid #00818c;border-bottom: 1px solid #00818c;padding:8px;font-family:'Open Sans', sans-serif;font-weight:normal;font-size:14px;'>  " + childItem.CrewsWithMinorInjury + " </td>");
                    //        //sb.Append("<td style='border-right:1px solid #00818c;border-bottom: 1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>" + childItem.PassengersWithMinorInjury + "  </td>");
                    //        //sb.Append("<td colspan='2' style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'> " + childItem.OthersWithMinorInjury + "  </td>");
                    //        //sb.Append("</tr>");

                    //        //sb.Append("<tr style='border-bottom:1px solid #00818c;'>");
                    //        //sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>None </td>");
                    //        //sb.Append("<td colspan='2' style='border-right: 1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans', sans-serif;font-weight:normal;font-size:14px;'>  " + childItem.CrewsKilled + " </td>");
                    //        //sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>" + childItem.PassengersKilled + "  </td>");
                    //        //sb.Append("<td colspan='2' style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'> " + childItem.OthersKilled + "  </td>");
                    //        //sb.Append("</tr>");

                    //        sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                    //        sb.Append("<td colspan='1' style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Make & Model </td>");
                    //        sb.Append("<td colspan='1' style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + childItem.LKAircraftModelName + "</ td>");
                    //        sb.Append("</tr>");

                    //        sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                    //        sb.Append("<td colspan='1' style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Registration</td>");
                    //        sb.Append("<td colspan='1' style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + childItem.AircraftRegistration + "</ td>");
                    //        sb.Append("</tr>");

                    //        sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                    //        sb.Append("<td colspan='1' style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Engine Info </td>");
                    //        sb.Append("<td colspan='1' style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + childItem.AircraftEngineInfo + "</ td>");
                    //        sb.Append("</tr>");

                    //        sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                    //        sb.Append("<td colspan='1' style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Maximum Take Off Mass (kg)</td>");
                    //        sb.Append("<td colspan='1' style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + childItem.AircraftMaximumMass + "</ td>");
                    //        sb.Append("</tr>");

                    //        sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                    //        sb.Append("<td colspan='1' style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Aircraft Category</td>");
                    //        sb.Append("<td colspan='1' style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + childItem.AircraftCategory + "</ td>");
                    //        sb.Append("</tr>");

                    //        sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                    //        sb.Append("<td colspan='1' style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>State of Registry</td>");
                    //        sb.Append("<td colspan='1' style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + childItem.AircraftStateofRegistry + "</ td>");
                    //        sb.Append("</tr>");

                    //        sb.Append("<tr style='border-bottom:1px solid #00818c;'>");
                    //        sb.Append("<td colspan='1' style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight: bold;font-size: 14px;'>Operator Name </td>");
                    //        sb.Append("<td  colspan='1' style='border-right: 1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight: normal;font-size: 14px;'> " + childItem.LKAirlineOperatorName + " </td>");
                    //        sb.Append("</tr>");

                    //        sb.Append("<tr style='border-bottom:1px solid #00818c;'>");
                    //        sb.Append("<td colspan='1' style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight: bold;font-size: 14px;'>State of the Operator</td>");
                    //        sb.Append("<td  colspan='1' style='border-right: 1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight: normal;font-size: 14px;'> " + childItem.StateofOperator + " </td>");
                    //        sb.Append("</tr>");

                    //        sb.Append("<tr style='border-bottom:1px solid #00818c;'>");
                    //        sb.Append("<td colspan='1' style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>Operator Contacts </td>");
                    //        sb.Append("<td  colspan='1' style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'>" + childItem.OperatorContactNo + "</td>");
                    //        sb.Append("</tr>");

                    //        sb.Append("<tr style='border-bottom:1px solid #00818c;'>");
                    //        sb.Append("<td colspan='1' style='border-right:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size:14px;'>MSN</td>");
                    //        sb.Append("<td  colspan='1' style='border-right:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size:14px;'>" + childItem.MSN + "</td>");
                    //        sb.Append("</tr>");

                    //        sb.Append("</tbody>");
                    //        sb.Append("</table>");

                    //    }
                    //    sb.Append("</td>");
                    //    sb.Append("</tr>");

                    //    sb.Append("</tbody>");
                    //    sb.Append("</table>");

                    //    sb.Append("</div>");
                    //    sb.Append("</div>");
                    //}
                    #endregion

                    #region commented on 23-oct-2019  based on client new requirment
                    //if (type == "notify")
                    //{
                    //    sb.Append("<div class='header' style='width: 595px;'>");
                    //    sb.Append("<div style='width:595px; margin: auto;'>");
                    //    sb.Append("<div style='border: 1px solid #00818c;margin-bottom: 15px;border: 1px solid #999;background-color:#09c4d2;font-weight:normal;text-align: center;padding: 8px;color: #fff;font-family: 'Titillium Web', sans-serif;font-size: 22px;'>DUTY INVESTIGATOR NOTIFICATION</div>");
                    //    sb.Append("<table style='width: 595px;margin:auto;border-spacing:0;border:1px solid #00818c;margin-bottom:15px;'>");

                    //    sb.Append("<thead>");
                    //    sb.Append("<tr style='border:1px solid #00818c;'>");
                    //    sb.Append("<th colspan='2' style='border:1px solid #999;background-color:#09c4d2;font-weight:bold;text-align:left;padding:8px;color:#fff;font-family:'Titillium Web',sans-serif;'>Initial Occurrence Information </th>");
                    //    sb.Append("</tr>");
                    //    sb.Append("</thead>");

                    //    sb.Append("<tbody>");

                    //    //sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                    //    //sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Notification Number</td>");
                    //    //sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + parentValue.NotificationId + "</ td>");
                    //    //sb.Append("</tr>");

                    //    //sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                    //    //sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Number of Aircrafts Involved</td>");
                    //    //sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + parentValue.NoOfAircraftInvolved + "</ td>");
                    //    //sb.Append("</tr>");

                    //    sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                    //    sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Date and Time of Occurrence </td>");
                    //    sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + comm.convertDateTime(parentValue.OccurrenceDate) + "</ td>");
                    //    sb.Append("</tr>");

                    //    //sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                    //    //sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Date and Time of Call </td>");
                    //    //sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + comm.convertDateTime(parentValue.CallDate) + "</ td>");
                    //    //sb.Append("</tr>");

                    //    sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                    //    sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Operator</td>");
                    //    sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + files + "</ td>");
                    //    sb.Append("</tr>");

                    //    sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                    //    sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Location of Occurrence </td>");
                    //    sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + parentValue.Location + "</ td>");
                    //    sb.Append("</tr>");

                    //    sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                    //    sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Description of Occurrence</td>");
                    //    sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + parentValue.OccurrenceDescription + "</ td>");
                    //    sb.Append("</tr>");

                    //    sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                    //    sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Notifier</td>");
                    //    sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + parentValue.OccurrenceInformer + "</ td>");
                    //    sb.Append("</tr>");

                    //    //sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                    //    //sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Notifier Contact</td>");
                    //    //sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + parentValue.NotifierContact + "</ td>");
                    //    //sb.Append("</tr>");

                    //    sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                    //    sb.Append("<td colspan='2' style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>");

                    //    sb.Append("</td>");
                    //    sb.Append("</tr>");

                    //    sb.Append("</tbody>");
                    //    sb.Append("</table>");

                    //    sb.Append("</div>");
                    //    sb.Append("</div>");
                    //}
                    #endregion

                    else if (type == "notify")
                    {
                        sb.Append("<div class='header' style='width: 595px;'>");
                        sb.Append("<div style='width:595px; margin: auto;'>");
                       
                        sb.Append("<table style='width: 595px;margin:auto;border-spacing:0;border:1px solid #00818c;margin-bottom:15px;border-collapse: collapse;'>");

                        sb.Append("<thead>");
                        sb.Append("<tr style='border:1px solid #00818c;'>");                        
                        sb.Append("<th style='border:1px solid #00818c;'><img src = cid:myImageID width='286' height='76'></th>");
                        sb.Append("<th style='border:1px solid #999;background-color:#09c4d2;font-weight:bold;text-align:center;color:#fff;'>Air Accident Investigation Sector Occurrence Notification</ th>");
                        sb.Append("</tr>");
                        sb.Append("</thead>");

                        sb.Append("<tbody>");

                        sb.Append("<tr style='border-bottom:1px solid #00818c;'>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;'>Occurrence Classification</td>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;'>" + parentValue.IncidentType + "</ td>");
                        sb.Append("</tr>");
                                            
                        sb.Append("<tr style='border-bottom:1px solid #00818c;'>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;'>Title of Occurrence </td>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;'>" + parentValue.OccurrenceCategorizationTitle + "</ td>");
                        sb.Append("</tr>");

                        sb.Append("<tr style='border-bottom:1px solid #00818c;'>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;'>Date and Time of Occurrence </td>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;'>" + comm.convertDateTime(parentValue.UTCDateAndTimeOfOccurrence) + " UTC" + "</ td>");
                        sb.Append("</tr>");
                        
                        sb.Append("<tr style='border-bottom:1px solid #00818c;'>");                      
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;'>Location of Occurrence </td>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;'>" + parentValue.Location + "</ td>");
                        sb.Append("</tr>");

                        sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;'>Operator </td>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;'>" + parentValue.OperatorName + "</ td>");
                        sb.Append("</tr>");

                        sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;'>Flight No </td>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;'>" + parentValue.FlightNumber + "</ td>");
                        sb.Append("</tr>");

                        sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;'>Departure Airport </td>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;'>" + parentValue.DepAirport + "</ td>");
                        sb.Append("</tr>");

                        sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;'>Destination Airport </td>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;'>" + parentValue.DesigAiport + "</ td>");
                        sb.Append("</tr>");

                        if (parentValue.DivAirport != "N/A")
                        {
                            sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                            sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;'>Diverted To Airport </td>");
                            sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;'>" + parentValue.DivAirport + "</ td>");
                            sb.Append("</tr>");
                        }

                        sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;'>Aircraft Type </td>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;'>" + parentValue.AircraftModelName + "</ td>");
                        sb.Append("</tr>");

                        sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;'>Aircraft Registration </td>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;'>" + parentValue.AircraftRegistrationName + "</ td>");
                        sb.Append("</tr>");

                        int CrewOnBoard = parentValue.CrewOnBoard + parentValue.CabinCrew;

                        sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;'>No. of Crew on board </td>");
                        if(CrewOnBoard == 0)
                        {
                            sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;'></ td>");

                        }
                        else
                        {
                            sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;'>" + CrewOnBoard + "</ td>");

                        }
                        sb.Append("</tr>");

                        sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;'>No. of Passengers on board </td>");
                        if (parentValue.PassengersOnBoard == 0)
                        {
                            sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;'></ td>");

                        }
                        else
                        {
                            sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;'>" + parentValue.PassengersOnBoard + "</ td>");

                        }
                        
                        sb.Append("</tr>");

                        sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;'>Injuries Crew </td>");
                        if (parentValue.TotalCrewsInjuries == 0)
                        {
                            sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;'></ td>");

                        }
                        else
                        {
                            sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;'>" + parentValue.TotalCrewsInjuries + "</ td>");

                        }
                        sb.Append("</tr>");

                        sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;'>Injuries Passengers</td>");
                        if (parentValue.TotalPassengersInjuries == 0)
                        {
                            sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;'></ td>");

                        }
                        else
                        {
                            sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;'>" + parentValue.TotalPassengersInjuries + "</ td>");

                        }
                        sb.Append("</tr>");

                        sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;'>Description of Occurrence</td>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;'>" + parentValue.OccurrenceDescription + "</ td>");
                        sb.Append("</tr>");

                        sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;'>Updated Information </td>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;'>" + parentValue.UpdatedInformation + "</ td>");
                        sb.Append("</tr>");

                        sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;'>Notified By </td>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;'>" + parentValue.OccurrenceInformer + "</ td>");
                        sb.Append("</tr>");                                              

                        //sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                        //sb.Append("<td colspan='2' style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>");
                       
                        sb.Append("</td>");
                        sb.Append("</tr>");

                        sb.Append("</tbody>");
                        sb.Append("</table>");

                        sb.Append("</div>");
                        sb.Append("</div>");
                    }

                    else if (type == "CorresSR")
                    {
                        //foreach (var items in parentValue)
                        // {
                        sb.Append("<div class='header'>");
                        sb.Append("<div style='width: 595px; margin: auto;'>");
                        sb.Append("<div style='border: 1px solid #00818c;margin-bottom: 15px;border: 1px solid #999;background-color:#09c4d2;font-weight:normal;text-align: center;padding: 8px;color: #fff;font-family: 'Titillium Web', sans-serif;font-size: 22px;'>SAFETY RECOMMENDATION</div>");
                        sb.Append("<table  style='width: 595px;margin:auto;border-spacing:0;border:1px solid #00818c;margin-bottom:15px;''>");

                        //sb.Append("<thead>");
                        //sb.Append("<tr style='border:1px solid #00818c;'>");
                        ////sb.Append("<th  style='border:1px solid #999;background-color:#09c4d2;font-weight:bold;text-align:left;padding:8px;color:#fff;font-family:'Titillium Web',sans-serif;'>Initial Information </th>");
                        //sb.Append("</tr>");
                        //sb.Append("</thead>");

                        sb.Append("<tbody style=''>");

                        sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>SR Identification Number</td>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + parentValue.SafetyRecommendationNumber + "</ td>");
                        sb.Append("</tr>");

                        sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Prepared by</td>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + parentValue.SRCreatedBy + "</ td>");
                        sb.Append("</tr>");

                        sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Date of creation </td>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + comm.convertDateTime(parentValue.SafetyRecommendationDateCreated) + "</ td>");
                        sb.Append("</tr>");

                        sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Safety Recommendation Content</td>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + parentValue.SafetyRecommendation + "</ td>");
                        sb.Append("</tr>");


                        sb.Append("</tbody>");
                        sb.Append("</table>");

                        sb.Append("</div>");
                        sb.Append("</div>");
                    }

                    else if (type == "ResendCorresSR")
                    {
                        //foreach (var items in parentValue)
                        // {
                        sb.Append("<div class='header'>");
                        sb.Append("<div style='width: 595px; margin: auto;'>");
                        sb.Append("<div style='border: 1px solid #00818c;margin-bottom: 15px;border: 1px solid #999;background-color:#09c4d2;font-weight:normal;text-align: center;padding: 8px;color: #fff;font-family: 'Titillium Web', sans-serif;font-size: 22px;'>SAFETY RECOMMENDATION - (REJECTED RESPONSE) </div>");
                        sb.Append("<table  style='width: 595px;margin:auto;border-spacing:0;border:1px solid #00818c;margin-bottom:15px;''>");

                        //sb.Append("<thead>");
                        //sb.Append("<tr style='border:1px solid #00818c;'>");
                        ////sb.Append("<th  style='border:1px solid #999;background-color:#09c4d2;font-weight:bold;text-align:left;padding:8px;color:#fff;font-family:'Titillium Web',sans-serif;'>Initial Information </th>");
                        //sb.Append("</tr>");
                        //sb.Append("</thead>");

                        sb.Append("<tbody style=''>");

                        sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>SR Identification Number</td>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + parentValue.SafetyRecommendationNumber + "</ td>");
                        sb.Append("</tr>");

                        sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Prepared by</td>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + parentValue.SRCreatedBy + "</ td>");
                        sb.Append("</tr>");

                        sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Date of creation </td>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + comm.convertDateTime(parentValue.SafetyRecommendationDateCreated) + "</ td>");
                        sb.Append("</tr>");

                        sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Safety Recommendation Content</td>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + parentValue.SafetyRecommendation + "</ td>");
                        sb.Append("</tr>");

                        sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:8px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>IIC  Comments</td>");
                        sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 8px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + parentValue.IICCommentsforSHResponse + "</ td>");
                        sb.Append("</tr>");

                        sb.Append("</tbody>");
                        sb.Append("</table>");

                        sb.Append("</div>");
                        sb.Append("</div>");
                    }

                    else if (type == "PrintSR")
                    {
                        sb.Append("<div class='header' style='width: 595px;'>");
                        sb.Append("<div style='width:595px; margin: auto;'>");
                        sb.Append("<div style='border: 1px solid #00818c;margin-bottom: 15px;border: 1px solid #999;background-color:#09c4d2;font-weight:normal;text-align: center;padding: 8px;color: #fff;font-family: 'Titillium Web', sans-serif;font-size: 22px;'>STATUS REPORT FOR THE INVESTIGATION SAFETY RECOMMENDATION</div>");
                        sb.Append("<table style='width: 595px;margin:auto;border-spacing:0;border:1px solid #00818c;margin-bottom:15px;'>");

                        sb.Append("<thead>");
                        sb.Append("<tr style='border:1px solid #00818c;'>");
                        sb.Append("<th colspan='2' style='border:1px solid #999;background-color:#09c4d2;font-weight:bold;text-align:left;padding:8px;color:#fff;font-family:'Titillium Web',sans-serif;'> Investigation File No -  " + files + "  </th>");
                        sb.Append("</tr>");
                        sb.Append("</thead>");

                        int srcount = 0;

                        foreach (var items in parentValue)
                        {
                            srcount++;

                            sb.Append("<tbody>");
                           
                            sb.Append("<tr style='border:1px solid #00818c;'>");
                            sb.Append("<th colspan='2' style='border:1px solid #999;background-color:#09c4d2;font-weight:bold;text-align:center;padding:8px;color:#fff;font-family:'Titillium Web',sans-serif;'> SR Number -  " + srcount + " </th>");
                            sb.Append("</tr>");                         

                            sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                            sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:2px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>SR File Number</td>");
                            sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 2px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + items.SafetyRecommendationNumber + "</ td>");
                            sb.Append("</tr>");

                            sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                            sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:2px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>SR Type</td>");
                            sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 2px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + items.SRTYPE + "</ td>");
                            sb.Append("</tr>");

                            sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                            sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:2px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>SR Created Date </td>");
                            sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 2px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + comm.convertDateTime(items.SafetyRecommendationDateCreated) + "</ td>");
                            sb.Append("</tr>");


                            sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                            sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:2px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Investigator </td>");
                            sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 2px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + items.InvestigatorName + "</ td>");
                            sb.Append("</tr>");


                            sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                            sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:2px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Aircraft Registration</td>");
                            sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 2px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + items.RegistrationName + "</ td>");
                            sb.Append("</tr>");

                            sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                            sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:2px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Aircraft Model</td>");
                            sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 2px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + items.AIRCRAFTMODELS + "</ td>");
                            sb.Append("</tr>");

                            sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                            sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:2px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>SR Content </td>");
                            sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 2px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + items.SafetyRecommendation + "</ td>");
                            sb.Append("</tr>");

                            if(items.SRSTATUS =="Closed")
                            {
                                sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                                sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:2px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>SR Closed By </td>");
                                sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 2px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + items.InvestigatorName + "</ td>");
                                sb.Append("</tr>");

                                sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                                sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:2px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>SR  Closed Date </td>");
                                sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 2px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + items.SRClosedDate + "</ td>");
                                sb.Append("</tr>");
                            }

                            var srhistory = db.USP_VIEW_INVESTIGATION_GETSRRESPONSEHISTORY(items.SafetyRecommendationNumber,items.InvestigationFileID);

                            string srhisDetails = ""; 

                            int count = 0;

                            foreach (var item in srhistory)
                            {                             
                                string resdate = string.Empty;

                                if (item.ResponseDate != null)
                                {
                                    resdate = " - On " + item.ResponseDate;
                                }

                                srhisDetails = " Mail Send Date: " + item.initiatedDate + " - " + " " + item.ResponseStatus + resdate + Environment.NewLine;

                                if (item.initiatedDate != null || item.ResponseStatus != "")
                                {
                                    count++;

                                    sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                                    sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:2px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 10px;'> SR History - " + count + " </td>");
                                    sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 2px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 10px;'>" + srhisDetails.ToString() + "</ td>");
                                    sb.Append("</tr>");
                                }
                            }
                            
                            sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                            sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:2px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>Current SR Status </td>");
                            sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 2px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + items.CurrentsrStatus + "</ td>");
                            sb.Append("</tr>");

                            //sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                            //sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:2px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>SR Status</td>");
                            //sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 2px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + items.SRSTATUS + "</ td>");
                            //sb.Append("</tr>");

                            //sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                            //sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:2px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>DAAI Status </td>");
                            //sb.Append("<td style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding: 2px;font-family:'Open Sans',sans-serif;font-weight:normal;font-size: 14px;'>" + items.DAAIStatus + "</ td>");
                            //sb.Append("</tr>");

                            sb.Append("<tr style='border-bottom: 1px solid #00818c;'>");
                            sb.Append("<td colspan='2' style='border-right:1px solid #00818c;border-bottom:1px solid #00818c;padding:2px;font-family:'Open Sans',sans-serif;font-weight:bold;font-size: 14px;'>");
                            
                            
                            sb.Append("</tbody>");
                        }

                        sb.Append("</table>");

                        sb.Append("</div>");
                        sb.Append("</div>");

                    }
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("PdfGeneration", "generateContent", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(HttpContext.Current.Session["UserId"]));
            }
            return sb.ToString();
        }    
    }
}