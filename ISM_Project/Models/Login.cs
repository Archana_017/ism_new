﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ISM_Project.Models
{
    public class Login
    {
        public string username{ get; set; }

        public string password{ get; set; }

        public string confirmpassword { get; set; }

        public bool rememberme { get; set; }
    }
}