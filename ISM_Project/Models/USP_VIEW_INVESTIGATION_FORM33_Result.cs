//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ISM_Project.Models
{
    using System;
    
    public partial class USP_VIEW_INVESTIGATION_FORM33_Result
    {
        public string INVNO { get; set; }
        public string OCCDATE { get; set; }
        public string PLOCC { get; set; }
        public string STREG { get; set; }
        public string STOPR { get; set; }
        public string OCCTYPE { get; set; }
        public string AIRTYPE { get; set; }
        public string AIRREG { get; set; }
    }
}
