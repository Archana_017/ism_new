//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ISM_Project.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Correspondence
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Correspondence()
        {
            this.CorrespondenceMessages = new HashSet<CorrespondenceMessage>();
            this.CorrespondenceMessageRecipients = new HashSet<CorrespondenceMessageRecipient>();
            this.CorrespondenceRecipients = new HashSet<CorrespondenceRecipient>();
        }
    
        public int CorrespondenceID { get; set; }
        public string CorrespondenceControlNumber { get; set; }
        public Nullable<int> InvestigationFileID { get; set; }
        public System.DateTime CorrespondenceInitiatedDate { get; set; }
        public System.DateTime LastCorrespondenceDate { get; set; }
        public Nullable<System.DateTime> CorrespondenceDueDate { get; set; }
        public string CorrespondenceResponseDays { get; set; }
        public string Subject { get; set; }
        public string CorrespondenceTags { get; set; }
        public Nullable<bool> IsResponseReceived { get; set; }
        public System.DateTime DateCreated { get; set; }
        public Nullable<System.DateTime> DateUpdated { get; set; }
        public int CreatedBy { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public bool IsViewed { get; set; }
    
        public virtual InvestigationFile InvestigationFile { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CorrespondenceMessage> CorrespondenceMessages { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CorrespondenceMessageRecipient> CorrespondenceMessageRecipients { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CorrespondenceRecipient> CorrespondenceRecipients { get; set; }
    }
}
