﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using Microsoft.SharePoint.Client;
using Microsoft.SharePoint.Client.Sharing;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Security;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using ISM_Project.Models;
using A1 = DocumentFormat.OpenXml.Drawing;
using DW = DocumentFormat.OpenXml.Drawing.Wordprocessing;
using PIC = DocumentFormat.OpenXml.Drawing.Pictures;
using System.Xml.Linq;
using System.Web.Script.Serialization;

namespace ISM_Project.Controllers
{
    /// <summary>
    /// Notification - controller contains functionality of Occurrence notification , Aircraft common lookup  and Notify as email alert 
    /// to director and global distribution list peoples 
    /// </summary> 
    /// 
    public class NotificationsController : Controller
    {
        Common comm = new Common();
        MailConfiguration mail = new MailConfiguration();
        DocumentGeneration doc = new DocumentGeneration();
        PdfGeneration pdf = new PdfGeneration();
        gcaa_ismEntities db = new gcaa_ismEntities();
        
        /// <summary>
        /// Index
        /// </summary> 
        /// 
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// Notify
        /// </summary> 
        /// 
        public ActionResult Notify()
        {
            return View("Notify");
        }
        /// <summary>
        /// Occurrence notify for creating new occurrence and update existing occurrence
        /// </summary> 
        /// 
        /// <param name="occur">Receive the notification details as object for both save and update</param>        
        public ActionResult OccurenceNotify(Occurrence_detail occur)
        {
            var DbTransaction = db.Database.BeginTransaction();
            try
            {
                DateTime UTCdatetimeofaccident = new DateTime();
                if (occur.LocalDateAndTimeOfOccurrence == null)
                {
                    occur.LocalDateAndTimeOfOccurrence = DateTime.Now;
                    UTCdatetimeofaccident = occur.LocalDateAndTimeOfOccurrence.Value.ToUniversalTime();
                }
                else
                {
                    UTCdatetimeofaccident = occur.OccurrenceDateTime.Value.ToUniversalTime();
                }
                if (occur.DatetimeofCall == null)
                {
                    occur.DatetimeofCall = DateTime.Now;
                }
                if (Convert.ToInt32(Session["UserId"]) > 0)
                {
                    if (occur.OccurrenceDetailID == 0)
                    {
                        //string[] NotifNoSplit = null;

                        //if (occur.NotificationId != null)
                        //{
                        //    NotifNoSplit = (occur.NotificationId.Split('/'));
                        //}
                        //else
                        //{
                        //    DbTransaction.Rollback();
                        //    return Json("Format");
                        //}

                        //if (NotifNoSplit[0] != "NOTN")
                        //{
                        //    DbTransaction.Rollback();
                        //    return Json("Format");
                        //}

                        //if (NotifNoSplit[1].Length != 4)
                        //{
                        //    DbTransaction.Rollback();
                        //    return Json("Format");
                        //}

                        //if (NotifNoSplit[2].Length != 4)
                        //{
                        //    DbTransaction.Rollback();
                        //    return Json("Format");
                        //}

                        //var AlredyExist = db.OccurrenceDetails.Where(a => a.NotificationId == occur.NotificationId).FirstOrDefault();

                        //if (AlredyExist != null)
                        //{
                        //    DbTransaction.Rollback();
                        //    return Json("Exist");
                        //}

                        if(occur.OccurrenceDateTime == null)
                        {
                            occur.OccurrenceDateTime = DateTime.Now;
                        }

                        System.Data.Entity.Core.Objects.ObjectParameter OccId = new System.Data.Entity.Core.Objects.ObjectParameter("OccId", typeof(Int32));

                        db.USP_API_INST_OCCURENCE(occur.NoofAircraft, Convert.ToInt32(Session["UserId"]), occur.LKOccurrenceStatusTypeId, occur.LKIncidentTypeID, occur.LKOccurrenceInformerID,

                            occur.PlaceofIncident, occur.DatetimeofCall, occur.OccurrenceDateTime, UTCdatetimeofaccident, occur.PrevailingWeatherConditions, occur.ExtendOfDamageToAircraft,

                            occur.DamagesToGroundObjects, occur.DescriptionOfExplosivesAndDangerousArticles, occur.DescriptionOfOccurrence, Convert.ToInt32(Session["UserId"]), OccId,

                            occur.Notifier, occur.Notifier_Contact, occur.DIsecurityNotify, occur.NotificationDescription, occur.OccurrenceCategorizationId, occur.UpdatedInformation, occur.NotificationId);

                        for (int i = 0; i < occur.NoofAircraft; i++)
                        {
                            int LKAircraftRegistrationID = Convert.ToInt32(occur.LKAircraftRegistrationID[i]);

                            var airoperinfo = db.LKAircraftRegistrations.Where(a => a.LKAircraftRegistrationID == LKAircraftRegistrationID).FirstOrDefault();

                            int LKAirlineOperatorID = 0; int LKAirlineCategoryID = 0; int LKAircraftModelID = 0;

                            if (airoperinfo != null)
                            {
                                LKAirlineOperatorID = Convert.ToInt32(airoperinfo.LKAirlineOperatorID);

                                LKAirlineCategoryID = Convert.ToInt32(airoperinfo.LKAircraftCategoryID);

                                LKAircraftModelID = Convert.ToInt32(airoperinfo.LKAircraftModelID);
                            }

                            db.USP_API_INST_OCCURENCE_AIRCRAFT_DETAIL(Convert.ToInt32(OccId.Value), LKAircraftModelID, LKAirlineCategoryID, occur.LKDepartureAirportID[i], occur.LKDestinationAirportID[i],
                            Convert.ToString(occur.MaximumMass[i]), occur.LKAircraftRegistrationID[i], occur.EngineInfo[i], occur.StateofRegistry[i], occur.LongLat[i], occur.FlightNumber[i], LKAirlineOperatorID
                            , occur.stateofoperatorid[i], occur.OperatorContacts[i], (occur.PassengerOnboard[i]), (occur.CrewOnboard[i]), 0, 0, 0,
                           0, 0, 0, 0, 0, 0, 0, 0, 0, occur.msn[i], Convert.ToInt32(Session["UserId"]), occur.CabinCrew[i], occur.TotalCrewsInjuries[i], occur.TotalPassengersInjuries[i], occur.LKDivertedToAirportID[i]);

                        }

                        DbTransaction.Commit();

                        return Json(Convert.ToInt32(OccId.Value));
                    }
                    else
                    {
                        //string[] NotifNoSplit = null;

                        //if (occur.NotificationId != null)
                        //{
                        //    NotifNoSplit = (occur.NotificationId.Split('/'));
                        //}
                        //else
                        //{
                        //    DbTransaction.Rollback();
                        //    return Json("Format");
                        //}

                        //if (NotifNoSplit[0] != "NOTN")
                        //{
                        //    DbTransaction.Rollback();
                        //    return Json("Format");
                        //}

                        //if (NotifNoSplit[1].Length != 4)
                        //{
                        //    DbTransaction.Rollback();
                        //    return Json("Format");
                        //}

                        //if (NotifNoSplit[2].Length != 4)
                        //{
                        //    DbTransaction.Rollback();
                        //    return Json("Format");
                        //}

                        //var AlredyExist = db.OccurrenceDetails.Where(a => a.NotificationId == occur.NotificationId && a.OccurrenceDetailID != occur.OccurrenceDetailID).FirstOrDefault();

                        //if (AlredyExist != null)
                        //{
                        //    DbTransaction.Rollback();
                        //    return Json("Exist");
                        //}

                        System.Data.Entity.Core.Objects.ObjectParameter OccId = new System.Data.Entity.Core.Objects.ObjectParameter("OccId", typeof(Int32));

                        db.USP_API_UPDT_OCCURENCE(occur.OccurrenceDetailID, occur.LKOccurrenceStatusTypeId, occur.PlaceofIncident, occur.LKIncidentTypeID, occur.DatetimeofCall,
                            occur.OccurrenceDateTime, UTCdatetimeofaccident, null, null, null, null, occur.DescriptionOfOccurrence, occur.LKOccurrenceInformerID,
                            Convert.ToInt32(Session["UserId"]), OccId, occur.NoofAircraft, occur.Notifier, occur.Notifier_Contact,
                            occur.DIsecurityNotify, occur.NotificationDescription, occur.OccurrenceCategorizationId, occur.UpdatedInformation, occur.NotificationId);

                        db.USP_API_DEL_OCCURENCE_AIRCRAFTDETAIL(occur.OccurrenceDetailID);

                        for (int i = 0; i < occur.NoofAircraft; i++)
                        {
                            int LKAircraftRegistrationID = Convert.ToInt32(occur.LKAircraftRegistrationID[i]);

                            var airoperinfo = db.LKAircraftRegistrations.Where(a => a.LKAircraftRegistrationID == LKAircraftRegistrationID).FirstOrDefault();

                            int LKAirlineOperatorID = 0; int LKAirlineCategoryID = 0; int LKAircraftModelID = 0;

                            if (airoperinfo != null)
                            {
                                LKAirlineOperatorID = Convert.ToInt32(airoperinfo.LKAirlineOperatorID);

                                LKAirlineCategoryID = Convert.ToInt32(airoperinfo.LKAircraftCategoryID);

                                LKAircraftModelID = Convert.ToInt32(airoperinfo.LKAircraftModelID);
                            }

                            db.USP_API_INST_OCCURENCE_AIRCRAFT_DETAIL(Convert.ToInt32(OccId.Value), LKAircraftModelID, LKAirlineCategoryID, occur.LKDepartureAirportID[i], occur.LKDestinationAirportID[i],
                               Convert.ToString(occur.MaximumMass[i]), occur.LKAircraftRegistrationID[i], occur.EngineInfo[i], occur.StateofRegistry[i], occur.LongLat[i], occur.FlightNumber[i], LKAirlineOperatorID
                                , occur.stateofoperatorid[i], occur.OperatorContacts[i], (occur.PassengerOnboard[i]), (occur.CrewOnboard[i]), 0, 0, 0,
                               0, 0, 0, 0, 0, 0,
                               0, 0, 0, occur.msn[i], Convert.ToInt32(Session["UserId"]), occur.CabinCrew[i], occur.TotalCrewsInjuries[i], occur.TotalPassengersInjuries[i], occur.LKDivertedToAirportID[i]);
                        }

                        DbTransaction.Commit();

                        return Json(Convert.ToInt32(OccId.Value));
                    }
                }
                else
                {
                    DbTransaction.Rollback();

                    return Json(0);
                }
            }
            catch (System.Exception ex)
            {
                DbTransaction.Rollback();

                comm.Exception_Log("Notification", "OccurenceNotify(Occurrence_detail occur)", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace,

                    Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }
        }
        /// <summary>
        /// Remove Notification Aircraft details by aircraft detailed id
        /// </summary> 
        /// 
        /// <param name="air_id">Receive the Aircraft id</param>        
        [HttpPost]
        public JsonResult RemoveNotificationAircraft(int air_id)
        {
            using (gcaa_ismEntities api = new gcaa_ismEntities())
            {
                System.Data.Entity.Core.Objects.ObjectParameter OccId = new System.Data.Entity.Core.Objects.ObjectParameter("OccId", typeof(Int32));

                api.USP_API_REMOVE_OCCURENCE_AIRCRAFTDETAIL(air_id, OccId);

                api.USP_API_UPDT_AIRCRAFTINVOLVED_OCCURENCE_AIRCRAFTDETAIL(Convert.ToInt32(OccId.Value));
                string result = string.Empty;

                result = "success";

                return Json(result);
            }
        }
        /// <summary>
        /// Redirect to Add notification view page 
        /// </summary> 
        /// 
        /// <param name="id">Receive the notification id</param>  
        public ActionResult Addnotification(int id)
        {
            if (id == 0)
            {
                TempData["active_notification"] = "notify";
            }
            gcaa_ismEntities gcaa = new gcaa_ismEntities();
            var makes = gcaa.LKAircraftMakes.ToList().OrderBy(x => x.LKAircraftMakeName);
            SelectList makelist = new SelectList(makes, "LKAircraftMakeID", "LKAircraftMakeName");
            ViewBag.makes = makelist;
            var informer = gcaa.LKOccurrenceInformers.ToList().OrderBy(x => x.LKOccurrenceInformerName);
            SelectList informerlist = new SelectList(informer, "LKOccurrenceInformerID", "LKOccurrenceInformerName");
            ViewBag.informer = informerlist;
            var accident_type = gcaa.LKIncidentTypes.ToList().OrderBy(x => x.LKIncidentTypeID);
            SelectList accidenttype = new SelectList(accident_type, "LKIncidentTypeID", "LKIncidentTypeName");
            ViewBag.accident_type = accidenttype;
            var aircraft_type = gcaa.LKAircraftTypes.ToList().OrderBy(x => x.LKAircraftTypeName);
            SelectList aircrafttype = new SelectList(aircraft_type, "LKAircraftTypeID", "LKAircraftTypeName");
            ViewBag.aircraft_type = aircrafttype;
            var airline_operator = gcaa.LKAirlineOperators.ToList().OrderBy(x => x.LKAirlineOperatorName);
            SelectList airlineoperator = new SelectList(airline_operator, "LKAirlineOperatorID", "LKAirlineOperatorName");
            ViewBag.airline_operator = airlineoperator;
            var country_id = gcaa.LKCountries.ToList().OrderBy(x => x.LKCountryName);
            SelectList countryid = new SelectList(country_id, "LKCountryID", "LKCountryName");
            ViewBag.country_id = countryid;
            var Registration_id = gcaa.LKAircraftRegistrations.ToList().OrderBy(x => x.LKAircraftRegistrationName);
            SelectList Registrationid = new SelectList(Registration_id, "LKAircraftRegistrationID", "LKAircraftRegistrationName");
            ViewBag.Registration_id = Registrationid;
            var AircraftModel_id = gcaa.LKAircraftModels.ToList().OrderBy(x => x.LKAircraftModelName);
            SelectList AircraftModelId = new SelectList(AircraftModel_id, "LKAircraftModelId", "LKAircraftModelName");
            ViewBag.AircraftModel_Id = AircraftModelId;
            var AircraftCategory_id = gcaa.LKAircraftCategories.ToList().OrderBy(x => x.LKAircraftCategoryName);
            SelectList categoryid = new SelectList(AircraftCategory_id, "LKAircraftCategoryId", "LKAircraftCategoryName");
            ViewBag.AircraftCategory_id = categoryid;

            var Occurrence_Categorization = gcaa.LKOccurrenceCategorizations.ToList().OrderBy(x => x.LKOccurrenceCategorizationId);
            SelectList OccurCategorization = new SelectList(Occurrence_Categorization, "LKOccurrenceCategorizationId", "OccurrenceCategorization");
            ViewBag.Occur_Categorization = OccurCategorization;

            Occurrence_detail occur1 = new Occurrence_detail();
            //OccurrenceDetail occur_result1 = new OccurrenceDetail();

            //occur_result1.LKAircraftModelID = 0;
            if (id != 0)
            {
                gcaa_ismEntities dc = new gcaa_ismEntities();
                var user = dc.USP_VIEW_OCCURRENCE(id, Convert.ToInt32(Session["UserId"])).FirstOrDefault();
                if (user != null)
                {
                    var aircraft_occurence = dc.USP_VIEW_OCCURRENCE_AIRCRAFTDETAIL(id, Convert.ToInt32(Session["UserId"])).ToList();
                    SelectList aircraftoccurence = new SelectList(aircraft_occurence);
                    ViewBag.aircraftinfo = aircraft_occurence;

                    if (user.LKOccurrenceStatusTypeId == 1)
                    {
                        Occurrence_detail occur_result = new Occurrence_detail();

                        // occur_result.LKCountryID = user.LKCountryID;

                        occur_result.NoofAircraft = user.NoOfAircraftInvolved;
                        DateTime dt = Convert.ToDateTime(user.LocalDateAndTimeOfOccurrence);
                        string localTime = dt.ToString("yyyy-MM-dd HH:mm");
                        string time = Convert.ToDateTime(user.UTCDateAndTimeOfOccurrence).ToString("yyyy-MM-dd HH:mm");
                        occur_result.LocalDateAndTimeOfOccurrence = Convert.ToDateTime(time);
                        ViewBag.LocalTime = comm.convertDateTime(Convert.ToDateTime(occur_result.LocalDateAndTimeOfOccurrence));

                        #region  change_on_2208219
                        ViewBag.OccurrenceDateTime = comm.convertDateTime(Convert.ToDateTime(user.LocalDateAndTimeOfOccurrence));
                        #endregion

                        occur_result.LKIncidentTypeID = user.LKIncidentTypeID;
                        occur_result.PrevailingWeatherConditions = user.PrevailingWeatherConditions;
                        occur_result.ExtendOfDamageToAircraft = user.ExtendOfDamageToAircraft;
                        occur_result.DamagesToGroundObjects = user.DamagesToGroundObjects;
                        occur_result.DescriptionOfExplosivesAndDangerousArticles = user.DescriptionOfExplosivesAndDangerousArticles;
                        occur_result.OccurrenceDetailID = user.OccurrenceDetailID;

                        occur_result.LKOccurrenceInformerID = user.LKOccurrenceInformerID;
                        occur_result.Notifier = user.Notifier;
                        occur_result.Notifier_Contact = user.NotifierContact;
                        //occur_result.DatetimeofCall = Convert.ToDateTime(user.NotifierCallDate);
                        ViewBag.CallTime = comm.convertDateTime(Convert.ToDateTime(user.NotifierCallDate));
                        occur_result.PlaceofIncident = user.Location;
                        occur_result.DescriptionOfOccurrence = user.OccurrenceDescription;
                        occur_result.DIsecurityNotify = Convert.ToBoolean(user.DISecurityNotification);
                        occur_result.NotificationDescription = Convert.ToString(user.NotificationDescription);
                        occur_result.OccurrenceCategorizationId = Convert.ToInt32(user.OccurrenceCategorizationId);
                        occur_result.UpdatedInformation = Convert.ToString(user.UpdatedInformation);
                        occur_result.NotificationId = Convert.ToString(user.NotificationId);
                        return View(occur_result);
                    }
                    else
                    {
                        return RedirectToAction("Dashboard", "Home");
                    }
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            else
            {
                return View(occur1);
            }
        }
        /// <summary>
        /// Get all registration list of aircraft 
        /// </summary> 
        /// 
        /// <param name="aircraftregid">Receive the notification id</param>  
        public ActionResult getregistrationList(string aircraftregid)
        {
            gcaa_ismEntities db = new gcaa_ismEntities();
            int aircraftregistration_id = Convert.ToInt32(aircraftregid);
            string result = string.Empty;
            using (gcaa_ismEntities gc = new gcaa_ismEntities())
            {
                var aircraftregmark = (gc.LKAircraftRegistrations.Where(x => x.LKAircraftTypeID == aircraftregistration_id).Select(a => new
                {
                    LKAircraftRegistrationID = a.LKAircraftRegistrationID,
                    LKAircraftRegistrationName = a.LKAircraftRegistrationName
                })).ToList();
                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                result = javaScriptSerializer.Serialize(aircraftregmark);
            }
            return Json(result);
        }
        /// <summary>
        /// Get all aircraft make list
        /// </summary> 
        /// 
        /// <param name="aircraftmakeid">Receive the aircraft make id</param>  
        public ActionResult geMakeList(string aircraftmakeid)
        {
            gcaa_ismEntities db = new gcaa_ismEntities();
            int aircraftregistration_id = Convert.ToInt32(aircraftmakeid);
            string result = string.Empty;
            using (gcaa_ismEntities gc = new gcaa_ismEntities())
            {
                var aircraftregmark = (gc.LKAircraftModels.Where(x => x.LKAircraftMakeID == aircraftregistration_id).Select(a => new
                {
                    LKAircraftRegistrationID = a.LKAircraftModelID,
                    LKAircraftRegistrationName = a.LKAircraftModelName
                })).ToList();
                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                result = javaScriptSerializer.Serialize(aircraftregmark);
            }
            return Json(result);
        }
        /// <summary>
        /// Get all make value
        /// </summary> 
        /// 
        public ActionResult geMakeValues()
        {
            gcaa_ismEntities db = new gcaa_ismEntities();

            string result = string.Empty;
            using (gcaa_ismEntities gc = new gcaa_ismEntities())
            {

                var aircraftmakes = (gc.LKAircraftModels.ToList().OrderBy(x => x.LKAircraftModelName).Select(a => new
                {
                    LKAircraftMakeID = a.LKAircraftMakeID,
                    LKAircraftMakeName = a.LKAircraftModelName
                })).ToList();
                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                result = javaScriptSerializer.Serialize(aircraftmakes);
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Get all registration value
        /// </summary> 
        /// 
        public ActionResult getregistrationValues()
        {
            string result = string.Empty;

            using (gcaa_ismEntities gc = new gcaa_ismEntities())
            {

                var aircraftregs = (gc.LKAircraftRegistrations.ToList().OrderBy(x => x.LKAircraftRegistrationName).Select(a => new
                {
                    LKAircraftRegistrationID = a.LKAircraftRegistrationID,
                    LKAircraftRegistrationName = a.LKAircraftRegistrationName,

                })).ToList();


                //aircraftregs.AddRange(aircraftregs.EngineInfo);
                //LKAircraftCategoryID = a.LKAircraftCategoryID,
                //LKAircraftModelID = a.LKAircraftModelID,
                //LKAirlineOperatorID = a.LKAirlineOperatorID ,
                //MaximumMass = a.MaximumMass,
                //MSN = a.MSN,
                //OperatorContacts = a.OperatorContacts,
                //StateofOperator = a.StateofOperator,
                //StateofRegistry = a.StateofRegistry,           

                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();

                result = javaScriptSerializer.Serialize(aircraftregs);
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// get category value
        /// </summary> 
        /// 
        public ActionResult geCategoryValues()
        {
            gcaa_ismEntities db = new gcaa_ismEntities();

            string result = string.Empty;
            using (gcaa_ismEntities gc = new gcaa_ismEntities())
            {

                var aircraftcategory = (gc.LKAircraftCategories.ToList().OrderBy(x => x.LKAircraftCategoryName).Select(a => new
                {
                    LKAircraftCategoryId = a.LKAircraftCategoryId,
                    LKAircraftCategoryName = a.LKAircraftCategoryName
                })).ToList();
                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                result = javaScriptSerializer.Serialize(aircraftcategory);
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// get operator name
        /// </summary> 
        /// 
        public ActionResult getOperatorNames()
        {

            string result = string.Empty;
            using (gcaa_ismEntities gc = new gcaa_ismEntities())
            {

                var aircraftoperator = (gc.LKAirlineOperators.ToList().OrderBy(x => x.LKAirlineOperatorName).Select(a => new
                {
                    LKAirlineOperatorID = a.LKAirlineOperatorID,
                    LKAirlineOperatorName = a.LKAirlineOperatorName
                })).ToList();
                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                result = javaScriptSerializer.Serialize(aircraftoperator);
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// get aircraft operator name by aircraft registration id
        /// </summary> 
        /// 
        /// <param name="RegId">Receive the aircraft registration id</param>  
        public ActionResult getOperatorNameByRegistrationId(int RegId)  // On 12-June-2018
        {
            string result = string.Empty;

            var airoperatorInfo = (from reginfo in db.LKAircraftRegistrations

                                   join operinfo in db.LKAirlineOperators on reginfo.LKAirlineOperatorID equals operinfo.LKAirlineOperatorID

                                   where reginfo.LKAircraftRegistrationID == RegId

                                   select new { operinfo.LKAirlineOperatorID, operinfo.LKAirlineOperatorName });

            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();

            result = javaScriptSerializer.Serialize(airoperatorInfo);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        ///  get aircraft operator name by aircraft registration
        /// </summary> 
        ///
        /// <param name="RegId">Receive the aircraft registration id</param>  
        public ActionResult getAircraftDetailsByRegistrationId(int RegId)  // On 18-June-2018
        {
            string result = string.Empty;

            try
            {
                var AircraftDetails = db.USP_VIEW_AIRCRAFTREGISTRATION_REG_ID(RegId).ToList();

                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();

                result = javaScriptSerializer.Serialize(AircraftDetails);
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("Notification", "getAircraftDetailsByRegistrationId(int RegId)", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace,

                    Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 
        /// </summary> 
        /// 
        public ActionResult getAirportList()
        {
            gcaa_ismEntities db = new gcaa_ismEntities();

            string result = string.Empty;

            using (gcaa_ismEntities gc = new gcaa_ismEntities())
            {

                var airportnames = (gc.LKAirports.ToList().OrderBy(x => x.LKAirportName).Select(a => new
                {
                    LKAirportID = a.LKAirportID,
                    LKAirportName = a.LKAirportName
                })).ToList();
                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                result = javaScriptSerializer.Serialize(airportnames);
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 
        /// </summary> 
        /// 
        public ActionResult getSkillNames()
        {
            gcaa_ismEntities db = new gcaa_ismEntities();

            string result = string.Empty;

            using (gcaa_ismEntities gc = new gcaa_ismEntities())
            {

                var skillnames = (gc.LKSkillTypes.ToList().OrderBy(x => x.LKSkillTypeName).Select(a => new
                {
                    LKskillID = a.LKSkillTypeID,
                    LKskillName = a.LKSkillTypeName
                })).ToList();
                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                result = javaScriptSerializer.Serialize(skillnames);
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 
        /// </summary> 
        /// 
        public ActionResult getAirCraftTypes()
        {
            gcaa_ismEntities db = new gcaa_ismEntities();

            string result = string.Empty;
            using (gcaa_ismEntities gc = new gcaa_ismEntities())
            {

                var aircraftypes = (gc.LKAircraftTypes.ToList().OrderBy(x => x.LKAircraftTypeName).Select(a => new
                {
                    LKAircraftTypeId = a.LKAircraftTypeID,
                    LKAircraftTypeName = a.LKAircraftTypeName
                })).ToList();
                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                result = javaScriptSerializer.Serialize(aircraftypes);
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 
        /// </summary> 
        /// 
        //public ActionResult PdfConversionOnClick()
        //{
        //    var pdfResult = new Rotativa.ViewAsPdf("Form_35")
        //    {
        //        FileName = "Form_35.pdf",
        //        CustomSwitches =
        //  "--footer-center \"Name: " + "XYZ" + "  DOS: " +
        //  DateTime.Now.Date.ToString("MM/dd/yyyy") + "  Page: [page]/[toPage]\"" +
        //  " --footer-line --footer-font-size \"9\" --footer-spacing 6 --footer-font-name \"calibri light\""

        //    };

        //    byte[] applicationPDFData = pdfResult.BuildFile(this.ControllerContext);

        //    MemoryStream ms = new MemoryStream();

        //    // byte[] byteInfo = pdf.Output();
        //    ms.Write(applicationPDFData, 0, applicationPDFData.Length);
        //    ms.Position = 0;

        //    using (var clientContext = new ClientContext("https://xmindsinfotech.sharepoint.com"))
        //    {
        //        SecureString passWord = new SecureString();

        //        foreach (char c in "xminds@123".ToCharArray()) passWord.AppendChar(c);
        //        var credentials = new SharePointOnlineCredentials("admin@xmindsinfotech.onmicrosoft.com", passWord);
        //        clientContext.Credentials = credentials;

        //        var fi = new FileInfo(pdfResult.FileName);
        //        var list = clientContext.Web.Lists.GetByTitle("Documents");
        //        clientContext.Load(list.RootFolder);
        //        clientContext.ExecuteQuery();
        //        var fileUrl = String.Format("{0}/{1}", list.RootFolder.ServerRelativeUrl, fi.Name);

        //        Microsoft.SharePoint.Client.File.SaveBinaryDirect(clientContext, fileUrl, ms, true);
        //    }
        //    return pdfResult;
        //}
        /* Region Word file creation from template which will be saved in the temporary file
         * Gets the input value as JSON which will be replaced in the new document.
         * Searches for the JSON key in the document which will be replaced with the JSON value
         */
        /// <summary>
        /// 
        /// </summary> 
        /// 
        /// <param name="inptext">Receive the aircraft registration id</param>  
        /// <param name="tableVal">Receive the aircraft registration id</param>  
        public ActionResult WordConversionOnClick(string inptext, string tableVal)
        {
            string status = string.Empty;
            try
            {
                // string imgName = "";
                string newImage = "";
                string imgName = @"C:\Aishwarya\panda.jpg";
                MemoryStream mem;
                MemoryStream documentStream;
                Stream req = Request.InputStream;
                req.Seek(0, System.IO.SeekOrigin.Begin);
                string sourceFile = Server.MapPath(Path.Combine("~/App_data", "ReportTemplate.dotx"));
                string fileName = "Report_" + Convert.ToString(DateTime.Now.ToFileTime()) + ".docx";
                string destinationFile = Server.MapPath(Path.Combine("~/App_data", fileName));
                var values = JsonConvert.DeserializeObject<Dictionary<string, string>>(inptext);
                Dictionary<string, string> keyValues = new Dictionary<string, string>();
                keyValues = values;
                if (new FileInfo(sourceFile).Length > 0)
                {
                    using (Stream tplStream = System.IO.File.OpenRead(sourceFile))
                    {
                        documentStream = new MemoryStream((int)tplStream.Length);
                        CopyStream(tplStream, documentStream);
                        documentStream.Position = 0L;
                    }
                    if (documentStream.Length > 0)
                    {
                        using (WordprocessingDocument template = WordprocessingDocument.Open(documentStream, true))
                        {
                            template.ChangeDocumentType(DocumentFormat.OpenXml.WordprocessingDocumentType.Document);
                            MainDocumentPart mainPartS = template.MainDocumentPart;
                            mainPartS.DocumentSettingsPart.AddExternalRelationship("http://schemas.openxmlformats.org/officeDocument/2006/relationships/attachedTemplate", new Uri(sourceFile, UriKind.Absolute));
                            mainPartS.Document.Save();
                        }
                        System.IO.File.WriteAllBytes(destinationFile, documentStream.ToArray());
                    }
                }
                if (new FileInfo(destinationFile).Length > 0)
                {
                    using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(destinationFile, true))
                    {
                        string docText = null;
                        MainDocumentPart mainPart = wordDoc.MainDocumentPart;
                        using (StreamReader sr = new StreamReader(wordDoc.MainDocumentPart.GetStream()))
                        {
                            docText = sr.ReadToEnd();
                        }
                        foreach (KeyValuePair<string, string> item in keyValues)
                        {
                            Regex regexText = new Regex(item.Key);
                            //regexText = regexText.ToString().ToLower();
                            //  docText = regexText.Replace(docText, item.Value);
                            SearchandReplace.SearchAndReplace(wordDoc, item.Key, item.Value, false);
                            // if (item.Key == "Image1")
                            //    imgName = item.Value;
                        }
                        //for image url
                        //if (!string.IsNullOrEmpty(imgName))
                        //{
                        //    newImage = Server.MapPath(Path.Combine("~/App_data", "test.gif"));
                        //    createImage(imgName, newImage);
                        //}
                        if (tableVal != null && tableVal != "")
                        {
                            dynamic formDetails = JObject.Parse(tableVal);
                            var tablevalue = formDetails.table;
                            var headers = formDetails.header;
                            var data = formDetails.data;
                            if (headers != null && data != null)
                            {
                                Table tbl = createTable(headers, data);
                                includeContent(tbl, "FG1", wordDoc);
                            }
                        }
                        Drawing image = new Drawing();
                        if (!string.IsNullOrEmpty(imgName))
                        {
                            ImagePart imgfootPart = mainPart.AddImagePart(ImagePartType.Jpeg);
                            using (FileStream stream = new FileStream(imgName, FileMode.Open))
                            {
                                if (stream != null)
                                {
                                    imgfootPart.FeedData(stream);
                                    image = AddImageToBody(mainPart.GetIdOfPart(imgfootPart), imgName);
                                    includeContent(image, "FG2", wordDoc);
                                }
                            }
                        }

                        //using (StreamWriter sw = new StreamWriter(
                        //          wordDoc.MainDocumentPart.GetStream(FileMode.Create)))
                        //{
                        //    sw.Write(docText);
                        //}
                        FooterPart footerPart = mainPart.AddNewPart<FooterPart>();
                        var sectionProps = wordDoc.MainDocumentPart.Document.Body.Elements<SectionProperties>().LastOrDefault();
                        if (newImage != null && newImage != "")
                        {
                            ImagePart imgfootPart = footerPart.AddImagePart(ImagePartType.Jpeg);
                            using (FileStream stream = new FileStream(newImage, FileMode.Open))
                            {
                                if (stream != null)
                                {
                                    imgfootPart.FeedData(stream);
                                    var rId = mainPart.GetIdOfPart(footerPart);
                                    var footerRef = new FooterReference { Id = rId };
                                    if (sectionProps == null)
                                    {
                                        sectionProps = new SectionProperties();
                                        wordDoc.MainDocumentPart.Document.Body.Append(sectionProps);
                                    }
                                    sectionProps.RemoveAllChildren<FooterReference>();
                                    sectionProps.Append(footerRef);
                                    var element = AddImageToBody(footerPart.GetIdOfPart(imgfootPart), newImage);
                                    var footer = new Footer();
                                    var paragraph = new Paragraph();
                                    var run = new Run();
                                    run.Append(element);
                                    paragraph.Append(run);
                                    footer.Append(paragraph);
                                    //line which adds image in the footer section
                                    footerPart.Footer = footer;
                                    footerPart.Footer.Save();
                                }
                            }
                        }
                        //*Code ends here*/
                        //@code - adding page numbers in footer                       
                        FooterPart footerPart1 = mainPart.AddNewPart<FooterPart>();
                        var rId1 = mainPart.GetIdOfPart(footerPart1);
                        var footerRef1 = new FooterReference { Id = rId1 };
                        if (sectionProps == null)
                        {
                            sectionProps = new SectionProperties();
                            wordDoc.MainDocumentPart.Document.Body.Append(sectionProps);
                        }
                        //sectionProps.RemoveAllChildren<FooterReference>();
                        sectionProps.Append(footerRef1);
                        footerPart1.Footer = GeneratePageFooterPart();
                        footerPart1.Footer.Save();
                        //@code ends
                    }
                    using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(destinationFile, true))
                    {
                        XElement ToC1 = wordDoc
                   .MainDocumentPart
                   .GetXDocument()
                   .Descendants(W.p).Descendants(W.r).Descendants(W.t)
                   .Where(z => z.Value == "ToF")
                  .FirstOrDefault();
                        //@code - add ToC
                        XElement ToC = wordDoc
                       .MainDocumentPart
                       .GetXDocument()
                       .Descendants(W.p).Descendants(W.r).Descendants(W.t)
                       .Where(z => z.Value == "TOC1")
                      .FirstOrDefault();
                        ToCAdder.AddToc(wordDoc, ToC,
                        @"TOC \o '1-3' \h \z \u", "Table of Contents", null);

                        // XElement ToF = wordDoc
                        // .MainDocumentPart
                        //.GetXDocument()
                        //.Descendants(W.p).Descendants(W.r).Descendants(W.t)
                        //.Where(z => z.Value == "TBF1")
                        //.FirstOrDefault();
                        ToCAdder.AddTof(wordDoc, ToC1,
                         @"TOC \h \z \c ""Figure""", null);

                    }
                    // byte[] bytes = System.IO.File.ReadAllBytes(destinationFile);
                    // stream = new MemoryStream(bytes);
                    //mainPart.Document.Save();
                    mem = new MemoryStream(System.IO.File.ReadAllBytes(destinationFile));
                    mem.Seek(0, SeekOrigin.Begin);
                    System.IO.File.Delete(destinationFile);
                    return File(mem, "application/octet-stream", fileName);
                }
            }
            // Process.Start(destinationFile);
            catch (System.Exception ex)
            {
                status = "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace;
            }
            ViewBag.status = status;
            return View();
        }
        /// <summary>
        /// create Image
        /// </summary> 
        /// 
        /// <param name="imgUrl">Receive the image url </param>  
        /// <param name="newImage">Receive the image file name</param>  
        private void createImage(string imgUrl, string newImage)
        {

            System.Net.WebClient wc = new WebClient();

            wc.DownloadFile(new Uri(imgUrl), newImage);

        }
        /// <summary>
        /// 
        /// </summary> 
        /// 
        public void includeContent(dynamic content, string findText, WordprocessingDocument doc)
        {
            var body = doc.MainDocumentPart.Document.Body;
            var paras = body.Elements<Paragraph>();
            foreach (var para in paras)
            {
                foreach (var run in para.Elements<Run>())
                {
                    foreach (var text in run.Elements<Text>())
                    {
                        if (text.Text.Contains(findText))
                        {
                            text.RemoveAllChildren();
                            // text.Text = "found text but not replaced";
                            run.Append(content);
                            run.Append(new Break());
                            run.Append(new Break());

                            if (content.GetType().Name == "Drawing")
                            {

                                Paragraph para2 = body.AppendChild(new Paragraph());
                                Run run2 = para.AppendChild(new Run());
                                run2.AppendChild(new Text { Text = " ", Space = SpaceProcessingModeValues.Preserve });

                                RunProperties runProperties = run2.AppendChild(new RunProperties());

                                FontSize fontSize = new FontSize();

                                fontSize.Val = "22";
                                runProperties.Color = new DocumentFormat.OpenXml.Wordprocessing.Color() { Val = "1F497D" };
                                //Color color= new Color() { ThemeColor = ThemeColorValues.Accent2 };
                                RunFonts font1 = new RunFonts() { Ascii = "Calibri (Body)" };
                                runProperties.AppendChild(fontSize);
                                // runProperties.Append(color);
                                runProperties.Append(font1);
                                Bold bold = new Bold();

                                bold.Val = OnOffValue.FromBoolean(true);

                                runProperties.AppendChild(bold);
                                run2.AppendChild(new Text("Figure 1: The Clipboard Tab"));
                            }
                            break;

                        }
                    }
                }
            }
        }
        /// <summary>
        /// 
        /// </summary> 
        /// 
        public static void SearchAndReplace(string document, Dictionary<string, string> dict)
        {
            using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(document, true))
            {
                string docText = null;
                using (StreamReader sr = new StreamReader(wordDoc.MainDocumentPart.GetStream()))
                {
                    docText = sr.ReadToEnd();
                }
                foreach (KeyValuePair<string, string> item in dict)
                {
                    Regex regexText = new Regex(item.Key);
                    //regexText = regexText.ToString().ToLower();
                    docText = regexText.Replace(docText, item.Value);
                }
                using (StreamWriter sw = new StreamWriter(wordDoc.MainDocumentPart.GetStream(FileMode.Create)))
                {
                    sw.Write(docText);
                }
            }
            byte[] bytes = System.IO.File.ReadAllBytes(document);
            MemoryStream stream = new MemoryStream();
            stream.Seek(0, SeekOrigin.Begin);


        }
        /// <summary>
        /// 
        /// </summary> 
        /// 
        public static void CopyStream(Stream source, Stream target)
        {
            if (source != null)
            {
                MemoryStream mstream = source as MemoryStream;
                if (mstream != null) mstream.WriteTo(target);
                else
                {
                    byte[] buffer = new byte[2048];
                    int length = buffer.Length, size;
                    while ((size = source.Read(buffer, 0, length)) != 0)
                        target.Write(buffer, 0, size);
                }
            }
        }
        /// <summary>
        /// Generate PgNo Footer
        /// </summary>
        /// <returns></returns>
        private static Footer GeneratePgNoFooter()
        {
            var element =
               new Footer(
                   new SdtBlock(
                       new SdtProperties(
                           new SdtId() { Val = 198822112 },
                           new SdtContentDocPartObject(
                               new DocPartGallery() { Val = "Page Numbers (Bottom of Page)" },
                               new DocPartUnique())),
                       new SdtContentBlock(
                           new Paragraph(
                               new ParagraphProperties(
                                   new ParagraphStyleId() { Val = "Footer" },
                                   new Justification() { Val = JustificationValues.Center }),
                               new SimpleField(
                                   new Run(
                                       new RunProperties(
                                           new NoProof()),
                                       new Text("1"))
                               )
                               { Instruction = " PAGE   \\* MERGEFORMAT " }
                           )
                           { RsidParagraphAddition = "00BB29E4", RsidRunAdditionDefault = "00BB29E4" })),
                   new Paragraph(
                       new ParagraphProperties(
                           new ParagraphStyleId() { Val = "Footer" })
                   )
                   { RsidParagraphAddition = "00BB29E4", RsidRunAdditionDefault = "00BB29E4" });
            return element;

        }
        /// <summary>
        /// Generate Page Footer Part
        /// </summary>
        /// <returns></returns>
        private static Footer GeneratePageFooterPart()
        {
            var element =
                new Footer(
                    new Paragraph(
                        new ParagraphProperties(
                            new ParagraphStyleId() { Val = "Footer" }, new Justification() { Val = JustificationValues.Center }),
                        new Run(
                            new Text(""),
                            // *** Adaptation: This will output the page number dynamically ***
                            new SimpleField() { Instruction = "PAGE" })
                            ));
            return element;
        }
        /// <summary>
        /// Add Image To Body
        /// </summary>
        /// <param name="relationshipId">file relationship Id</param>
        /// <param name="filename">file name</param>
        /// <returns></returns>
        private static Drawing AddImageToBody(string relationshipId, string filename)
        {
            // Define the reference of the image.

            var element =
                 new Drawing(
                     new DW.Inline(
                         new DW.Extent() { Cx = 990000L, Cy = 792000L },
                         new DW.EffectExtent()
                         {
                             LeftEdge = 0L,
                             TopEdge = 0L,
                             RightEdge = 0L,
                             BottomEdge = 0L
                         },
                         new DW.DocProperties()
                         {
                             Id = (UInt32Value)1U,
                             Name = "Picture 1"
                         },
                         new DW.NonVisualGraphicFrameDrawingProperties(
                             new A1.GraphicFrameLocks() { NoChangeAspect = true }),
                         new A1.Graphic(
                             new A1.GraphicData(
                                 new PIC.Picture(
                                     new PIC.NonVisualPictureProperties(
                                         new PIC.NonVisualDrawingProperties()
                                         {
                                             Id = (UInt32Value)0U,
                                             Name = "New Bitmap Image.jpg"
                                         },
                                         new PIC.NonVisualPictureDrawingProperties()),
                                     new PIC.BlipFill(
                                         new A1.Blip(
                                             new A1.BlipExtensionList(
                                                 new A1.BlipExtension()
                                                 {
                                                     Uri =
                                                       "{28A0092B-C50C-407E-A947-70E740481C1C}"
                                                 })
                                         )
                                         {
                                             Embed = relationshipId,
                                             CompressionState = A1.BlipCompressionValues.Print
                                         },
                                         new A1.Stretch(
                                             new A1.FillRectangle())),
                                     new PIC.ShapeProperties(
                                         new A1.Transform2D(
                                             new A1.Offset() { X = 0L, Y = 0L },
                                             new A1.Extents() { Cx = 990000L, Cy = 792000L }),
                                         new A1.PresetGeometry(
                                             new A1.AdjustValueList()
                                         )
                                         { Preset = A1.ShapeTypeValues.Rectangle }))
                             )
                             { Uri = "http://schemas.openxmlformats.org/drawingml/2006/picture" })
                     )
                     {
                         DistanceFromTop = (UInt32Value)114300U,
                         DistanceFromBottom = (UInt32Value)0U,
                         DistanceFromLeft = (UInt32Value)114300U,
                         DistanceFromRight = (UInt32Value)0U,
                         EditId = "50D07946"
                     });

            // Append the reference to body, the element should be in a Run.
            // wordDoc.MainDocumentPart.Document.Body.AppendChild(new Paragraph(new Run(element)));
            //  return element;
            //to return header and append image in it.

            return element;
        }
        /// <summary>
        /// create Table
        /// </summary>
        /// <param name="header"> table header</param>
        /// <param name="rowData"> table data</param>
        /// <returns></returns>
        private static Table createTable(dynamic header, dynamic rowData)
        {
            Table table = new Table();

            // Create a TableProperties object and specify its border information.
            TableProperties tblProp = new TableProperties(
                new TableBorders(
                    new TopBorder()
                    {
                        Val =
                        new EnumValue<BorderValues>(BorderValues.Single),
                        Size = 24
                    },
                    new BottomBorder()
                    {
                        Val =
                        new EnumValue<BorderValues>(BorderValues.Single),
                        Size = 24
                    },
                    new LeftBorder()
                    {
                        Val =
                        new EnumValue<BorderValues>(BorderValues.Single),
                        Size = 24
                    },
                    new RightBorder()
                    {
                        Val =
                        new EnumValue<BorderValues>(BorderValues.Single),
                        Size = 24
                    },
                    new InsideHorizontalBorder()
                    {
                        Val =
                        new EnumValue<BorderValues>(BorderValues.Single),
                        Size = 24
                    },
                    new InsideVerticalBorder()
                    {
                        Val =
                        new EnumValue<BorderValues>(BorderValues.Single),
                        Size = 24
                    }
                )
            );

            // Append the TableProperties object to the empty table.
            table.AppendChild<TableProperties>(tblProp);
            Dictionary<string, string> keyValuesHeader = new Dictionary<string, string>();
            Dictionary<string, string> keyValuesData = new Dictionary<string, string>();

            for (int i = 0; i < header.Count; i++)
            {
                TableRowProperties tblHeaderRowProps = new TableRowProperties(new CantSplit() { Val = OnOffOnlyValues.On }, new TableHeader() { Val = OnOffOnlyValues.On });

                var tr = new TableRow();
                string headername = Convert.ToString(header[i]);
                var values = JsonConvert.DeserializeObject<Dictionary<string, string>>(headername);
                keyValuesHeader = values;
                tr.AppendChild<TableRowProperties>(tblHeaderRowProps);

                foreach (var item in keyValuesHeader)
                {
                    var tc = new TableCell();

                    tc.Append(new TableCellProperties(new TableCellWidth { Type = TableWidthUnitValues.Dxa, Width = "4650" }));

                    tc.TableCellProperties.Shading = new Shading()
                    {
                        Color = "auto",
                        //Fill = "FF0000",#f1f4f5
                        Fill = "f1f4f5",
                        Val = ShadingPatternValues.Clear
                    };
                    tc.Append(new Paragraph(new Run(new Text(item.Value))));
                    tr.Append(tc);
                }
                // Assume you want columns that are automatically sized.
                table.Append(tr);
            }
            for (int i = 0; i < rowData.Count; i++)
            {
                var tr = new TableRow();
                string rowDataval = Convert.ToString(rowData[i]);
                var values = JsonConvert.DeserializeObject<Dictionary<string, string>>(rowDataval);
                keyValuesData = values;
                foreach (var item in keyValuesData)
                {
                    var tc = new TableCell();
                    //    new TableCellWidth() { Type = TableWidthUnitValues.Dxa, Width = "2400" }));

                    tc.Append(new TableCellProperties(new TableCellWidth { Type = TableWidthUnitValues.Dxa, Width = "4650" }));
                    tc.Append(new Paragraph(new Run(new Text(item.Value))));
                    tr.Append(tc);
                }
                // Assume you want columns that are automatically sized.
                table.Append(tr);
            }

            // Append the table to the document.
            //doc.MainDocumentPart.Document.Body.Append(table);
            return table;
        }
        /// <summary>
        ///  Save doc to SharePoint
        /// </summary> 
        /// <param name="ms">Receive stream object</param>  
        /// <param name="filepath">Receive the attachment file path </param>  
        /// 
        #region  Save the generated word document into SharePoint 
        public static void savetoSharePoint(Stream ms, string filepath)
        {
            using (var clientContext = new ClientContext("https://xmindsinfotech.sharepoint.com"))

            {
                SecureString passWord = new SecureString();
                foreach (char c in "xminds@123".ToCharArray()) passWord.AppendChar(c);
                var credentials = new SharePointOnlineCredentials("admin@xmindsinfotech.onmicrosoft.com", passWord);
                clientContext.Credentials = credentials;
                var list = clientContext.Web.Lists.GetByTitle("Documents");
                clientContext.Load(list.RootFolder);
                clientContext.ExecuteQuery();
                var fi = new FileInfo(filepath);
                var fileUrl = String.Format("{0}/{1}", list.RootFolder.ServerRelativeUrl, fi.Name);
                FileCreationInformation fileInfo = new FileCreationInformation();
                fileInfo.Content = System.IO.File.ReadAllBytes(filepath);
                fileInfo.Url = fileUrl;
                fileInfo.Overwrite = true;
                //Microsoft.SharePoint.Client.File.SaveBinaryDirect(clientContext, fileUrl, ms, true);
                Microsoft.SharePoint.Client.File uploadedFile = list.RootFolder.Files.Add(fileInfo);
                // Create invitation request list to multiple users 
                var users = new List<string>() { "raishwarya@xminds.in", "deepu@xminds.in", "jackson@xminds.in", "lekshmi@xminds.com" };
                var userRoles = new List<UserRoleAssignment>();
                foreach (var user in users)
                {
                    UserRoleAssignment role = new UserRoleAssignment();
                    role.UserId = user;
                    role.Role = Role.Edit;
                    userRoles.Add(role);
                }
                clientContext.Load(uploadedFile);
                clientContext.Load(clientContext.Web);
                clientContext.ExecuteQuery();
                string absoluteFileUrl = clientContext.Web.Url + uploadedFile.ServerRelativeUrl;
                //DocumentSharingManager.UpdateDocumentSharingInfo(context, absoluteFileUrl, userRoles, true, true, true, customMsg, true, true);
                DocumentSharingManager.UpdateDocumentSharingInfo(clientContext, absoluteFileUrl, userRoles, true, true, true, "Invites you", true, true);
                clientContext.ExecuteQuery();
                // Send invitation requests to external users                        
                clientContext.Load(uploadedFile, f => f.ListItemAllFields);
                clientContext.ExecuteQuery();
                //Print List Item Id
                string uploadedUrl = clientContext.Web.Url + uploadedFile.ListItemAllFields["FileRef"];
            }
        }
        #endregion
        /// <summary>
        /// View IIC Profile information
        /// </summary> 
        /// 
        /// <param name="userid">Receive user id</param>  
        public ActionResult IIC_View_Profile(int userid = 0)
        {
            var model = new Form39();
            try
            {
                gcaa_ismEntities gcaa = new gcaa_ismEntities();

                model = new Form39
                {

                    iic_view_profile = gcaa.USP_IIC_VIEW_PROFILE(userid).ToList(),
                    iic_view_profile_2 = gcaa.USP_IIC_VIEW_PROFILE_2(userid).ToList(),
                    IIC_Cases = gcaa.USP_VIEW_IIC_CASES(userid).ToList()
                };
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("", "", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("NotFound", "Error");
            }
            return View(model);
        }
        /// <summary>
        /// Notify by email from global distribution list people 
        /// </summary> 
        /// 
        /// <param name="occur_Id">Receive notification id</param>  
        [HttpPost]
        public JsonResult NotifyDL(int occur_Id)
        {
            bool issent = false;
            try
            {
                if (occur_Id != 0)
                {
                    using (gcaa_ismEntities db = new gcaa_ismEntities())
                    {
                        var DList = db.USP_GET_DL(1).ToList(); // refer global destribution list
                        var DList2 = db.USP_GET_DL(1087).ToList(); // refer global destribution list
                        List<string> DListUsers = new List<string>();
                        List<string> DListUsersEmail = new List<string>();
                        foreach (var users in DList)
                        {
                            DListUsersEmail.Add(users.EmailAddress);
                            DListUsers.Add(users.FirstName);
                        }
                        var DIsecurity = db.USP_VIEW_OCCURRENCE(occur_Id, Convert.ToInt32(Session["UserId"])).FirstOrDefault();
                        if (DIsecurity.DISecurityNotification == true)
                        {
                            foreach (var users in DList2)
                            {
                                DListUsersEmail.Add(users.EmailAddress);
                                DListUsers.Add(users.FirstName);
                            }
                        }
                        string siteURL = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));

                        var aircraft_operator = db.USP_VIEW_OCCURRENCE_PDF(occur_Id, Convert.ToInt32(Session["UserId"])).ToList();

                        string aircraft_oper = null;

                        foreach (dynamic air_operator in aircraft_operator)
                        {
                            if (aircraft_oper == null)
                            {
                                aircraft_oper = air_operator.OperatorName;
                            }
                            else
                            {
                                aircraft_oper += " , " + air_operator.OperatorName;
                            }
                        }

                        dynamic Occ = db.USP_VIEW_OCCURRENCE_PDF(occur_Id, Convert.ToInt32(Session["UserId"])).FirstOrDefault();
                        dynamic occAircraft = db.USP_VIEW_OCCURRENCE_AIRCRAFTDETAIL_PDF(occur_Id, Convert.ToInt32(Session["UserId"])).ToList();
                        string sourceFile = pdf.generatePdf(Occ, occAircraft, aircraft_oper, "notify", siteURL);
                        issent = mail.SendMailNotificationAsInline(1, Convert.ToString(Session["FirstName"]), Convert.ToInt32(Session["roleid"]), siteURL, DListUsers, DListUsersEmail, occur_Id, sourceFile, Occ.NotificationId, null, null, sourceFile);
                    }
                }
            }
            catch (System.Exception ex)
            {
                issent = false;
                comm.Exception_Log("Notifications", "NotifyDL", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }
            return Json(issent);
        }
        /// <summary>
        /// search Airport details from database
        /// </summary> 
        /// 
        /// <param name="keyword">Receive search keyword</param>  
        public ActionResult searchAirport(string keyword)
        {
            gcaa_ismEntities gcaa = new gcaa_ismEntities();
            string result = string.Empty;
            try
            {
                //if (Session["Airports"] == null)
                //{
                var item = gcaa.USP_VIEW_AIRPORT_DETAILS(keyword, null).ToList();
                // Session["Airports"] = item;
                //}
                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                result = javaScriptSerializer.Serialize(item);
            }
            catch (System.Exception ex)
            {
                result = string.Empty;
                comm.Exception_Log("NotificationsController", "searchAirport", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// search Operator details from database
        /// </summary> 
        /// 
        /// <param name="keyword">Receive search keyword</param> 
        public ActionResult searchOperator(string keyword)
        {
            string result = string.Empty;
            try
            {
                var item = db.USP_VIEW_AIRCRAFT_OPERATOR(keyword).ToList();

                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();

                result = javaScriptSerializer.Serialize(item);
            }
            catch (System.Exception ex)
            {
                result = string.Empty;
                comm.Exception_Log("NotificationsController", "searchOperator", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// search aircraft Category from database
        /// </summary> 
        /// 
        /// <param name="keyword">Receive search keyword</param> 
        public ActionResult searchCategory(string keyword)
        {
            string result = string.Empty;
            try
            {
                var item = db.USP_VIEW_AIRCRAFT_CATEGORY(keyword).ToList();

                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();

                result = javaScriptSerializer.Serialize(item);
            }
            catch (System.Exception ex)
            {
                result = string.Empty;
                comm.Exception_Log("NotificationsController", "searchCategory", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// search aircraft Model from database
        /// </summary> 
        /// 
        /// <param name="keyword">Receive search keyword</param> 
        public ActionResult searchModel(string keyword)
        {
            string result = string.Empty;

            try
            {
                var item = db.USP_VIEW_AIRCRAFT_MODEL(keyword).ToList();

                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();

                result = javaScriptSerializer.Serialize(item);
            }
            catch (System.Exception ex)
            {
                result = string.Empty;
                comm.Exception_Log("NotificationsController", "searchModel", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        ///  get Airport details by airport id
        /// </summary> 
        ///
        /// <param name="AirportId">Receive the AirportId registration id</param>  
        public ActionResult getAirportDetailsByAirportId(int AirportId)
        {
            LKAirportDetail _airReg = new LKAirportDetail();

            try
            {

            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("Notifications", " getAirportDetailsByAirportId(int AirportId)", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }

            return View("AddNewAirport", _airReg);
        }
        /// <summary>
        ///  Receive Airport details
        /// </summary> 
        ///
        /// <param name="Airportdet">Receive all Airport details</param>  
        public ActionResult saveAirportDetails(LKAirportDetail Airportdet)
        {
            LKAirport _airReg = new LKAirport();

            try
            {
                //var result = "";

                //var checkairportname = db.LKAirportDetails.Where(a => a.ICAO == Airportdet.ICAO && a.AirportName == Airportdet.AirportName &&

                //                                              a.LKAirportDetailID != Airportdet.LKAirportDetailID).ToList();

                if (Airportdet.AirportName.Contains('-'))
                {
                    Airportdet.ICAO = Airportdet.AirportName.Split('-')[0].ToString();

                    Airportdet.AirportName = Airportdet.AirportName.Split('-')[1].ToString();
                }
                else
                {
                    Airportdet.ICAO = Airportdet.AirportName.ToString();

                    Airportdet.AirportName = Airportdet.AirportName.ToString();
                }

                var LKAirport_DetailID = db.LKAirportDetails.Where(a => a.ICAO == Airportdet.ICAO).FirstOrDefault();

                if (LKAirport_DetailID != null)
                {
                    Airportdet.LKAirportDetailID = LKAirport_DetailID.LKAirportDetailID;
                }

                if (Airportdet.LKAirportDetailID > 0)
                {
                    db.USP_UPDT_AIRPORTDETAIL(Airportdet.LKAirportDetailID, Airportdet.ICAO, Airportdet.AirportName);
                }
                else
                {
                    db.USP_INST_AIRPORTDETAIL(Airportdet.LKAirportDetailID, Airportdet.ICAO, Airportdet.AirportName);
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("Notifications", " saveAirportDetails", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }

            return View(_airReg);
        }
        /// <summary>
        /// Save / Update Occurrence Categorization details
        /// </summary> 
        ///
        /// <param name="occCat">Receive Occurrence Categorization details</param>  
        public ActionResult AddOccurCategory(LKOccurrenceCategorization occCat)
        {
            LKOccurrenceCategorization _airReg = new LKOccurrenceCategorization();

            try
            {
                if (occCat.LKOccurrenceCategorizationId > 0)
                {
                    db.USP_UPDT_OccurrenceCategorization(occCat.LKOccurrenceCategorizationId, occCat.OccurrenceCategorization, occCat.OccurrenceCategorizationAcronym);
                }
                else
                {
                    db.USP_INST_OccurrenceCategorization(occCat.OccurrenceCategorization, occCat.OccurrenceCategorizationAcronym);
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("Notifications", " AddOccurCategory", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }

            return View(occCat);
        }
        /// <summary>
        ///  get Occurence Category By Id
        /// </summary> 
        ///
        /// <param name="OccurCatId">Receive Occur Category Id</param>  
        public ActionResult getOccurCategoryById(int OccurCatId)
        {
            LKOccurrenceCategorization _airReg = new LKOccurrenceCategorization();

            try
            {
                var info = db.LKOccurrenceCategorizations.Where(a => a.LKOccurrenceCategorizationId == OccurCatId).FirstOrDefault();

                if (info != null)
                {
                    _airReg.LKOccurrenceCategorizationId = info.LKOccurrenceCategorizationId;

                    _airReg.OccurrenceCategorization = info.OccurrenceCategorization;

                    _airReg.OccurrenceCategorizationAcronym = info.OccurrenceCategorizationAcronym;
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("Notifications", " getOccurCategoryById(int OccurCatId)", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }

            return View("AddOccurCategory", _airReg);
        }
    }
}