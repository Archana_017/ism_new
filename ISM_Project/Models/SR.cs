﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ISM_Project.Models
{
    public class SR
    {
        public IEnumerable<USP_VIEW_SR_Result> SRInfo { get; set; }
        public IEnumerable<USP_VIEW_SR_UPDT_HISTORY_Result> SRHIST { get; set; }
        public IEnumerable <USP_VIEW_SR_HISTORIES_Result> SRCOMMU { get; set; }
        public IEnumerable<USP_VIEW_ALL_SR_Result> AllSR { get; set; }
        public IEnumerable<USP_VIEW_INVESTIGATION_FORM35_Result> Inv { get; set; }
        public IEnumerable<USP_VIEW_INVESTIGATION_AIRCRAFT_DETAIL_FORM35_Result> Inv_AircraftDet { get; set; }
        public IEnumerable<USP_SR_ATTACHMENTS_Result> SR_Attach { get; set; }
        public IEnumerable<USP_VIEW_LATEST_SR_STATUS_Result> SR_Status { get; set; }
        public string SafetyRecommendationNumber { get; set; }
        [AllowHtml]
        public string SafetyRecommendation { get; set; }
        public int InvestigationFileID { get; set; }
        public System.DateTime SafetyRecommendationDateCreated { get; set; }
        public string FirstName { get; set; }
        public string FirstName1 { get; set; }
        public System.DateTime DateChanged { get; set; }
        public string ReasonForUpdate { get; set; }
        public string DAAIFeedback { get; set; }
        public string DAAIStatus { get; set; }
        public int SR_ID { get; set; }
        public HttpPostedFileBase[] SRFiles { get; set; }

        public int SRResponseID { get; set; }
        public Nullable<int> SRID { get; set; }
        public string EncodedURL { get; set; }
        public Nullable<bool> StakeholederResponseStatus { get; set; }
        public string StakeholederEmailID { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public string SRCommandAction { get; set; } 

        public int SRHistoryID { get; set; }
        public string StakeholderResponse { get; set; }
        public string IICCommentsforSHResponse { get; set; }
        public int IICImplementationStatus { get; set; }
        public string SRCommitteeComments { get; set; }
        public string SRCommitteeClosureStatus { get; set; }
        public string IICClosureComments { get; set; }
        public int SRStatus { get; set; }

        public int SRCommitteeRandomLinkID { get; set; }
        public Nullable<bool> SRCommitteeActionStatus { get; set; }
        public string SRCommitteeEmailID { get; set; }
        public Nullable<int> IICImplementaionStatus { get; set; }
        public string SRIMPLEMENTATIONSTATUS { get; set; }
        public string HistoryOfFlight { get; set; }

        public int CorresRecipientID { get; set; }

        public int TypeofsafetyRecommendation { get; set; }
        public int safetyRecommendationyear { get; set; }

        public IEnumerable<USP_VIEW_ALL_SR_FORDELETE_Result> SR_info { get; set; }

        public string SafetyRecommendationAssignedTo { get; set; }
    }
}