//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ISM_Project.Models
{
    using System;
    
    public partial class USP_VIEW_INVESTIGATION_IIC_TM_Result
    {
        public int IICAssigned { get; set; }
        public string FirstName { get; set; }
        public string UsrType { get; set; }
    }
}
