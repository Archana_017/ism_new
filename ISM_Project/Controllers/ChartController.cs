﻿using ISM_Project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ISM_Project.Controllers
{
    /// <summary>
    /// Chart Controller for display some information in director dashboard.
    /// </summary> 
    /// 
    public class ChartController : Controller
    {
        gcaa_ismEntities db = new gcaa_ismEntities();
     
        /// <summary>
        /// Type of occurrence listed based on  years selection 
        /// </summary> 
        /// 
        public ActionResult OccurenceType()
        {
            InvChart invChrt = new InvChart();
                        
            var Year = db.USP_VIEW_OCCURRENCE_YEAR(DateTime.Now.Year).ToList();

            //Year.Add(new SelectListItem() { Text = "2018", Value = "2018" });
            //Year.Add(new SelectListItem() { Text = "2017", Value = "2017" });
            //Year.Add(new SelectListItem() { Text = "2016", Value = "2016" });
            //Year.Add(new SelectListItem() { Text = "2015", Value = "2015" });
            //Year.Add(new SelectListItem() { Text = "2014", Value = "2014" });

            SelectList year_list = new SelectList(Year);

            ViewBag.Year_id = year_list;

            ViewBag.CurrentYear = DateTime.Now.Year;
            return View(invChrt);
        }
        /// <summary>
        /// Get Occurrence chart data based on year selection
        /// </summary> 
        /// 
        /// <param name="_year">Receive the selected calendar year  .</param>
        public JsonResult GetChartData(int _year)  // Bar Chart
        {            
            var ChartInfo = db.USP_VIEW_OCCURRENCE_BY_INCIDENTTYPE(_year).ToList();

            return Json(ChartInfo, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Get SR chart data based on year selection
        /// </summary> 
        /// 
        /// <param name="_year">Receive the selected calendar year  .</param>
        public JsonResult GetSRChartData(int _year) // Donut Chart
        {
            var ChartInfo = db.USP_VIEW_SR_FOR_PIECHART(_year).ToList();

            return Json(ChartInfo, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Get Investigation chart data based on year selection
        /// </summary> 
        /// 
        /// <param name="_year">Receive the selected calendar year  .</param>
        public JsonResult GetInvestigationChart(int _year) // Line Chart
        {
            var ChartInfo = db.USP_VIEW_INVESTIGATION_FOR_LINECHART(_year).ToList();

            return Json(ChartInfo, JsonRequestBehavior.AllowGet);
        }
    }
}