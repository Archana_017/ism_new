//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ISM_Project.Models
{
    using System;
    
    public partial class USP_VIEW_CORRES_RESPONSES_Result
    {
        public int CorrespondenceID { get; set; }
        public string CorrespondenceControlNumber { get; set; }
        public string CorrespondenceMessageFrom { get; set; }
        public string EmailAddress { get; set; }
        public Nullable<int> UserID { get; set; }
        public string RecType { get; set; }
        public string ISReceived { get; set; }
        public Nullable<int> LKCorrespondenceMessageTypeID { get; set; }
        public Nullable<System.DateTime> ResopnseDueDate { get; set; }
    }
}
