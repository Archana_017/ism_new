﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ISM_Project.Models
{
    public class AnalysisOptionsAdd
    {

        public string add_org_influences { get; set;}
        public string unsafe_supervision { get; set; }
       public string add_precondition { get; set; }
        public string unsafe_act { get; set; }
    }
}