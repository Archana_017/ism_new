﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ISM_Project.Models;
using System.Text;
using Newtonsoft.Json;
using System.Web.Script.Serialization;

namespace ISM_Project.Controllers
{
    /// <summary>
    /// Remove API Controller functionality of remove aircraft details of particular occurrence.
    /// </summary> 
    /// 
    public class RemoveController : ApiController
    {
        /// <summary>
        /// API controller used to remove the aircraft details
        /// </summary> 
        /// 
        /// <param name="aircraft_id">Receive notification aircraft id</param>
        public HttpResponseMessage Post([FromBody] int aircraft_id)
        {
            using (gcaa_ismEntities api = new gcaa_ismEntities())
            {
                System.Data.Entity.Core.Objects.ObjectParameter OccId = new System.Data.Entity.Core.Objects.ObjectParameter("OccId", typeof(Int32));
                api.USP_API_REMOVE_OCCURENCE_AIRCRAFTDETAIL(aircraft_id,OccId);
                api.USP_API_UPDT_AIRCRAFTINVOLVED_OCCURENCE_AIRCRAFTDETAIL(Convert.ToInt32(OccId.Value));
                return Request.CreateResponse(HttpStatusCode.Created,1);            
            }            
        }       
    }
}
