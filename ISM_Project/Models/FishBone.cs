﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ISM_Project.Models
{
    public class FishBone
    {
        public int  Invfileid { get; set;}
        public string mainfbupleft { get; set;}
        public string mainfbupmid { get; set; }
        public string mainfbupright { get; set; }


        public string subfbup1 { get; set; }
        public string subfbup2 { get; set; }
        public string subfbup3 { get; set; }
        public string subfbupmid1 { get; set; }
        public string subfbupmid2 { get; set; }
        public string subfbupmid3 { get; set; }
        public string subfbupright1 { get; set; }
        public string subfbupright2 { get; set; }
        public string subfbupright3 { get; set; }
        public string mainfbmiddle { get; set;}

        public string mainfbdownleft { get; set;}
        public string mainfbdownmid { get; set; }
        public string mainfbdownright { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string subfbdownleft1 { get; set;}

        public string subfbdownleft2 { get; set;}
        public string subfbdownleft3 { get; set; }
        public string subfbdownmid1 { get; set; }
        public string subfbdownmid2 { get; set; }
        public string subfbdownmid3 { get; set; }
        public string subfbdownright1 { get; set; }
        public string subfbdownright2 { get; set; }
        public string subfbdownright3 { get; set; }


    }
}