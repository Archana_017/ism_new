﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ISM_Project.Models;
using System.Web.Mvc;

namespace ISM_Project.Controllers
{
    /// <summary>
    ///  Login - Controller
    /// </summary> 
    /// 
    public class LoginController : ApiController
    {
        /// <summary>
        ///  Http Post Method for Login 
        /// </summary> 
        /// 
        /// <param name="user">Receive the user information as object</param>
        
        public HttpResponseMessage Post([FromBody] Login user)
        {
            using (gcaa_ismEntities gc = new gcaa_ismEntities())
            {
                var user_details = gc.Users.Where(a => a.EmailAddress == user.username && a.PasswordSalt == user.password).FirstOrDefault();
                var rolename = gc.Users.Where(a => a.EmailAddress == user.username && a.PasswordSalt == user.password).Select(a => a.UserRoleID).SingleOrDefault();
                if (user_details != null)
                {
                   
                    
                }
                //   var message = Request.CreateResponse(HttpStatusCode.Created, rolename){ Content = new StringContent("Your message here") } ;
                var message = new HttpResponseMessage(HttpStatusCode.OK) { Content = new StringContent(Convert.ToString(rolename)) };
                var retmsg = Request.CreateResponse(rolename);
                return message;
            }
        }
    }
}
