﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ISM_Project.Models
{
    public class AdminModel
    {
        public int user_id { get; set; }
        public string first_name { get; set; }
        public string middle_name { get; set; }
        public string last_name { get; set; }
        public string Staff_id { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public Nullable<System.DateTime> dob { get; set; }
        public string phone { get; set; }
        public string[] CerticateName { get; set; }
        public System.DateTime[] Certified_dt { get; set; }
        public System.DateTime[] Renewal_dt { get; set; }
        public string[] validity_pd { get; set; }
        public int[] Skillname { get; set; }
        public int[] work_experience { get; set; }
        public int[] Rating { get; set; }
        public int is_active { get; set; }
        public int roleid { get; set; }
        public string roleName { get; set; }
        public int DesingationID { get; set; }
        public string DesignationName { get; set; }
        public int No_of_certificate { get; set; }
        public int No_of_Skill { get; set; }
        public System.DateTime created_date { get; set; }
        public System.DateTime updated_date { get; set; }
        public IEnumerable<USP_VIEW_ALL_USERS_Result> All_Users { get; set; }
        public bool ActAsDirector { get; set;}
        public int LKDesignationID { get; set; }
        public string LKDesignationName { get; set; }
    }
}