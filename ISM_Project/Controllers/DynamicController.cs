﻿using ISM_Project.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Helpers;
using System.Web.Hosting;
using System.Web.Mvc;

namespace ISM_Project.Controllers
{
    /// <summary>
    /// Dynamic Controller - functionality of dynamic form generation for various investigation task enents.
    /// </summary> 
    /// 
    public class DynamicController : Controller
    {
        /// <summary>
        /// 
        /// </summary> 
        /// 
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// Redirect to dynamic form view page and generate form based on investigation task 
        /// </summary> 
        /// 
        public ActionResult DynamicForm()
        {
            return View();
        }
        /// <summary>
        /// redirect to jamesreasons
        /// </summary> 
        /// 
        public ActionResult JamesReasons()
        {
            return View();
        }
        /// <summary>
        /// Redirect to  Shellmodel-1 view page
        /// </summary> 
        /// 
        public ActionResult Shellmodel()
        {
            return View();
        }
        /// <summary>
        /// Redirect to Shellmodel-2 view page
        /// </summary> 
        /// 
        public ActionResult Shellmodel2()
        {
            return View();
        }
        /// <summary>
        /// Redirect to Fishbone view page
        /// </summary> 
        /// 
        public ActionResult Fishbone()
        {
            return View();
        }
        /// <summary>
        /// upload file to sharepoint 
        /// </summary> 
        /// 
        public ActionResult uploadfiles()
        {
            return View();
        }
        /// <summary>
        /// upload large file to database (more than 1 GB)
        /// </summary> 
        /// 
        /// <param name="form">Receive the large file attachment to be database uplode.</param>
        [HttpPost]
        public ActionResult uploadfilespost(FormCollection form)
        {
            HttpFileCollectionBase file = Request.Files;
            HostingEnvironment.QueueBackgroundWorkItem(cancellationToken => new Backgroundworker().startprocess(Request.Files));
            return RedirectToAction("uploadfiles");
        }
        /// <summary>
        /// generate form based on control type from the JSON array.
        /// </summary> 
        /// 
        [HttpPost]
        public JsonResult GenerateForm()
        {
            Stream req = Request.InputStream;
            req.Seek(0, System.IO.SeekOrigin.Begin);
            string json = new StreamReader(req).ReadToEnd();
            var formContent = "";
            dynamic formDetails = JObject.Parse(json);
            var event_name = formDetails.form_title;
            event_name = Convert.ToString(event_name);
            var form_name = Convert.ToString(formDetails.form_name);
            var form_action = Convert.ToString(formDetails.form_action);
            var field = formDetails.fields;

            for (var i = 0; i < field.Count; i++)
            {
                if (field[i].field_type == "text")
                {
                    formContent += textControll(field[i]);
                }
                else if (field[i].field_type == "h2")
                {
                    formContent += h2Control(field[i]);
                }
                else if (field[i].field_type == "radio")
                {
                    formContent += radioControl(field[i]);
                }
                else if (field[i].field_type == "dropdown")
                {
                    formContent += dropdownControl(field[i]);
                }
                else if (field[i].field_type == "date")
                {
                    formContent += dateControl(field[i]);
                }
                else if (field[i].field_type == "textarea")
                {
                    formContent += textareaControl(field[i]);
                }
                else if (field[i].field_type == "file")
                {
                    formContent += fileInputControll(field[i]);
                }
                else if (field[i].field_type == "checkbox")
                {
                    formContent += checkboxControl(field[i]);
                }
                else if (field[i].field_type == "label")
                {
                    formContent += labelControl(field[i]);
                }
                else if (field[i].field_type == "button")
                {
                    formContent += buttonControll(field[i]);
                }
                else if (field[i].field_type == "datepicker")
                {
                    formContent += datepickercontrol(field[i]);
                }
            }
            //formContent += btn;
            return Json(new { nform_title = event_name, nform_name = form_name, action_name = form_action, fields = formContent });
        }
        /// <summary>
        ///  generate text control for corresponding input
        /// </summary> 
        /// 
        /// <param name="field">Receive the dynamic textbox control field name.</param>
        public string textControll(dynamic field)
        {
            var tc = "<div class='form-group'>" +
                "<label class='col-sm-4 col-form-label' for='exampleInputEmail1'>" +
                field.label +
                "<span class='text-danger'>*</span>" +
                "</label>" +
                "<div class='col-sm-8 col-form-box'>" +
                "<input type='text' class='form-control' value='" + field.value + "' id='exampleInputEmail1' placeholder=''>" +
                "</div>" +
                "</div>";
            return tc;
        }
        /// <summary>
        /// generate file control for corresponding input
        /// </summary> 
        /// 
        /// <param name="field">Receive the dynamic file control field name.</param>
        public string fileControll(dynamic field)
        {
            if (field.value != null && field.value != "")
            {
                return anchorControl(field);
            }
            var fc = "<div class='form-group'>" +
                "<label class='col-sm-4 col-form-label' for='exampleInputPassword1' >" + field.label + "<span class='text-danger' >*</span ></label >" +
                "<div class='col-sm-8 col-form-box' > " +
                "<div class='input-group inver-file-upload inver-file-upload-dynamic' > " +
                "<input id= 'uploadFile' class='form-control' value='" + field.value + "' placeholder= 'Choose File' disabled= 'disabled' > " +
                "<div class='input-group-btn' > " +
                "<div class='fileUpload btn btn-success' > " +
                "<span > <i class='fa fa-upload'></i> Upload</span > " +
                "<input id= 'uploadBtn' type= 'file' class='upload' > " +
                "</div > " +
                "</div > " +
                "</div > " +
                "</div > " +
                "</div>";
            return fc;
        }
        /// <summary>
        /// generate radio control for corresponding input
        /// </summary> 
        /// 
        /// <param name="field">Receive the dynamic radio button control field name.</param>
        public string radioControl(dynamic field)
        {
            var rc = "<div class='form-group'>" +
                "<label class='col-sm-4 col-form-label' for='exampleInputEmail1'>" +
                field.label + "<span class='text-danger'>*</span>" +
                "</label>" +
                "<div class='col-sm-8 radio-btn-div'>";
            for (var i = 0; i < field.field_options.options.Count; i++)
            {
                var ch = field.field_options.options[i].isChecked == "true" ? "checked" : "";
                rc += "<label class='radio-inline' > " +
                    "<input type='radio' name='optradio' " + ch + ">" + field.field_options.options[i].label +
                    "</label>";

            }
            rc += "</div>" +
                "</div>";
            return rc;
        }
        /// <summary>
        /// generate dropdown control for corresponding input
        /// </summary> 
        /// 
        /// <param name="field">Receive the dynamic dropdown control field name.</param>
        public string dropdownControl(dynamic field)
        {
            var dc = "<div class='form-group'>" +
                "<label class='col-sm-4 col-form-label' for='exampleInputEmail1' >" + field.label + "<span class='text-danger' >*</span ></label >" +
                "<div class='col-sm-8 col-form-box'>" +
                "<select class='form-control' value='' id='exampleFormControlSelect1'>";
            for (var i = 0; i < field.field_options.options.Count; i++)
            {
                dc += "<option>" + field.field_options.options[i].label + "</option>";
            }
            dc += "</select>" +
                "</div>" +
                "</div>";
            return dc;
        }
        /// <summary>
        /// generate text area control for corresponding input
        /// </summary> 
        /// 
        /// <param name="field">Receive the dynamic text area control field name.</param>
        public string textareaControl(dynamic field)
        {

            if (field.external == 1)
            {
                if (field.value[0] != "")
                {

                    var tc = "<div class='form-group'><label class='col-sm-4 col-form-label' for='exampleInputPassword1'>" + field.label +
                "<span class='text-danger'>*</span></label><span style='width:100%'>" + field.value + "</span></div>";
                    return tc;
                }

                else
                {
                    var tc = "<div class='form-group'><label class='col-sm-4 col-form-label' for='exampleInputPassword1' >" + field.label +
                    "<span class='text-danger' >*</span ></label><span style='background-color:#cac9c7;padding:1px 5px;position:absolute;z-index:9;width:100%;border:1px dotted #333;border-radius:4px;cursor:not-allowed;color: #333;'>External Member Field</span></div>";
                    return tc;
                }


            }
            else
            {
                var tc = "<div class='form-group'>" +
                    "<label class='col-sm-4 col-form-label' for='exampleInputPassword1' >" + field.label +
                    "<span class='text-danger' >*</span ></label >" +
                    "<div class='col-sm-8 col-form-box'>" +
                    "<textarea class='form-control' id='form-69-textarea' rows='3'>" + field.value + "</textarea>" +
                    "</div> " +
                    "</div>";
                return tc;

            }


        }
        /// <summary>
        /// generate data control for corresponding input
        /// </summary> 
        /// 
        /// <param name="field">Receive the dynamic date control field name.</param>
        public string dateControl(dynamic field)
        {
            var dc = "<div class='form-group'>" +
                "<label class='col-sm-4 col-form-label' for='exampleInputEmail1'>" +
                field.label +
                "<span class='text-danger'>*</span>" +
                "</label>" +
                "<div class='col-sm-8 col-form-box'>" +
                "<input type='date' class='form-control' value='" + field.value + "' id='exampleInputEmail1' placeholder=''>" +
                "</div>" +
                "</div>";
            return dc;
        }
        /// <summary>
        /// generate checkbox control for corresponding input
        /// </summary> 
        /// 
        /// <param name="field">Receive the dynamic checkbox control field name.</param>
        public string checkboxControl(dynamic field)
        {
            var tc = "<div class='form-group'>" +
                "<label class='col-sm-4 col-form-label' for='exampleInputEmail1'>" +
                field.label +
                "<span class='text-danger'>*</span>" +
                "</label>" +
                "<div class='col-sm-8 col-form-box'>" +
                "<input type='checkbox' class='form-control checkbox-dynamic' id='exampleInputEmail1' placeholder=''>" +
                "</div>" +
                "</div>";
            return tc;
        }
        /// <summary>
        /// generate anchor conrol for corresponding input
        /// </summary> 
        /// 
        /// <param name="field">Receive the dynamic anchor control field name.</param>
        public string anchorControl(dynamic field)
        {
            var tc = "<div class='form-group'>" +
                "<label class='col-sm-4 col-form-label' for='exampleInputEmail1'>" +
                field.label +
                "<span class='text-danger'>*</span>" +
                "</label>" +
                "<div class='col-sm-8 col-form-box'>" +
                "<a href='" + field.value + "' target='_blank'>View </a>" +
                "</div>" +
                "</div>";
            return tc;
        }
        /// <summary>
        /// generate date picker control for corresponding input
        /// </summary> 
        /// 
        /// <param name="field">Receive the dynamic date picker control field name.</param>
        public string datepickercontrol(dynamic field)
        {
            var tc = "<div class='form-group'>" +
                 "<label class='col-sm-4 col-form-label' for='exampleInputEmail1'>" +
                field.label +
                "</label>" +
                "<div type='date' class='input-group eventPicker'>" +
            "<input type = 'text' id = '" + field.name + "' class='form-control' name='" + field.name + "' value=''/>" +
               "<span class='input-group-addon'>" +
                                                        "<span class='fa fa-calendar'></span>" +
                                                    "</span>" +
                                                "</div> </div>";
            return tc;
        }
        /// <summary>
        /// generate button control for corresponding input
        /// </summary> 
        /// 
        /// <param name="field">Receive the dynamic button control field name.</param>
        public string buttonControll(dynamic field)
        {
            var tc = "<div class='sub-sav-btn'><button type = 'button' class='btn btn-save'>" + field.label + "</button></div>";

            return tc;

        }
        /// <summary>
        /// generate label control for corresponding input
        /// </summary> 
        /// 
        /// <param name="field">Receive the dynamic label box control field name.</param>
        public string labelControl(dynamic field)
        {
            var tc = "<div class='form-group'>" +
                            "<label class='" + field.class_name + "'>" +
                            field.label +
                            "</label>" +
                            "</div>";
            return tc;

        }
        /// <summary>
        /// generate file Input control for corresponding input
        /// </summary> 
        /// 
        /// <param name="field">Receive the dynamic file input control field name.</param>
        public string fileInputControll(dynamic field)
        {

            var tc = "<div class='form-group'>" +

                    "<label class='col-sm-6 col-form-label'>" +
                  field.label +
                     "</label>" +
             "<div class='col-sm-6 col-form-box'>" +
             "<input type='hidden'  id='" + field.name + "_count' value='1'/>" +
             "<label class='values-tick'<i class='fa fa-times' aria-hidden='true'></i></label>" +
              "<div class='input-group inver-file-upload' style='margin - left:15px; margin - bottom: 15px; margin - top: 15px; width: 643px;'>";
            if (field.external == 1)
            {
                tc += "<span style='background-color:#cac9c7;padding:1px 5px;position:absolute;z-index:9;width:100%;border:1px dotted #333;border-radius:4px;cursor:not-allowed;color: #333;'>External Member Field</span>";
            }
            else
            {
                tc += "<input id = '" + field.name + "_uploadFile_1' class='form-control' placeholder='Choose File' disabled='disabled'>" +
                           "<div class='input-group-btn'>" +
                           "<div class='fileUpload btn btn-success'>" +
                                  "<span><i class='fa fa-upload'></i> Upload</span>" +
                                   "<input id ='" + field.name + "_uploadBtn_1' type ='file' class='event_upload' name='" + field.name + "[]'/>" +
                               "</div></div>";

            }

            tc += "</div></div></div>";

            if (field.allow_multiple == 1)
            {
                if (field.external == 1)
                {

                }
                else
                {
                    tc += "<div class='repeat_uploads'><div class='col-sm-6 col-form-label'>" +
                                      "<label class='cur " + field.multiple_class + "' data-control_name=" + field.name + " data-divname='" + field.div_name + "'> Add New <i class='fa fa-plus-square' aria-hidden='true'></i></label>" +
                                     "</div>" +
                                     "<div class='col-sm-6 " + field.div_name + " no-padding'>" +
                                     "</div>";

                }

            }
            return tc;

        }
        /// <summary>
        /// generate h2 control for corresponding input
        /// </summary> 
        /// 
        /// <param name="field">Receive the dynamic textbox control field name.</param>
        public string h2Control(dynamic field)
        {
            if (field.parent_div_class != null)
            {
                var tc = "<div class='" + field.parent_div_class + "'><h2 class='" + field.class_name + "'>" + field.label + "</h2></div>";
                return tc;
            }
            else
            {
                var tc = "<div class='col-md-12'><h2 class='" + field.class_name + "'>" + field.label + "</h2></div>";
                return tc;
            }
        }
    }
}