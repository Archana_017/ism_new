﻿using ISM_Project.App_Start;
using ISM_Project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.WebHost;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.SessionState;

namespace ISM_Project
{
    public class MvcApplication : System.Web.HttpApplication
    {
        gcaa_ismEntities dc = new gcaa_ismEntities();

        Common comm = new Common();

        protected void Application_Start()
        {
            try
            {
                AreaRegistration.RegisterAllAreas();
                GlobalConfiguration.Configure(WebApiConfig.Register);
                FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
                RouteConfig.RegisterRoutes(RouteTable.Routes);
                BundleConfig.RegisterBundles(BundleTable.Bundles);
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("global", "Application_Start", ex.Message.ToString() + " line No :" + ex.StackTrace.ToString() + "" + ex.InnerException.ToString(),0);

               
            }

        }
        protected void Application_PostAuthorizeRequest()
        {
            System.Web.HttpContext.Current.SetSessionStateBehavior(System.Web.SessionState.SessionStateBehavior.Required);
        }

    }
}
