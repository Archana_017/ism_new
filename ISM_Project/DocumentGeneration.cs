﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net.Http;
using DocumentFormat.OpenXml.Packaging;
using System.Text.RegularExpressions;
using Microsoft.SharePoint.Client;
using System.Security;
using Microsoft.SharePoint.Client.Sharing;
using System.Diagnostics;

namespace ISM_Project
{
    /// <summary>
    /// Document Generation class file used for replace the document templete key to values.
    /// </summary> 
    /// 
    public class DocumentGeneration
    {
        /// <summary>
        /// get Document method contains both Form35 and Final report  template file text replacement feature
        /// </summary> 
        /// 
        ///<param name="dict">Recieve list of document details for replace key and value</param>
        ///<param name="Form_Type">Receive form type</param>
        public string get_Document(string Form_Type, Dictionary<string, string> dict)
        {
            Form_Type = Form_Type.ToLower().Trim();
            string Template = string.Empty;
            string filename = string.Empty;
            switch (Form_Type)
            {
                case "form_35":
                    Template = HttpContext.Current.Server.MapPath(Path.Combine("~/App_data", "FORM_035.docx"));
                    filename = Convert.ToString(DateTime.Now.ToFileTime()) + ".docx";
                    break;
                case "final_report":
                    Template = HttpContext.Current.Server.MapPath(Path.Combine("~/App_data", "Template.docx"));
                    filename = "Final_Report.docx";
                    break;
            }
            string destinationFile = HttpContext.Current.Server.MapPath(Path.Combine("~/App_data", filename));
            System.IO.File.Copy(Template, destinationFile, true);
            // create key value pair, key represents words to be replace and 
            //values represent values in document in place of keys.
            Dictionary<string, string> keyValues = new Dictionary<string, string>();
            keyValues = dict;
            //keyValues.Add("name", "aISHU");
            SearchAndReplace(destinationFile, keyValues);
            Process.Start(destinationFile);
            if (Form_Type == "form_35")
            {
                byte[] bytes = System.IO.File.ReadAllBytes(destinationFile);
                MemoryStream ms = new MemoryStream();
                ms.Write(bytes, 0, bytes.Length);
                // doc.SaveToStream(ms, Spire.Doc.FileFormat.Docx);
                ms.Position = 0;
                savetoSharePoint(ms, destinationFile);
                System.IO.File.Delete(destinationFile);
            }
            return destinationFile;
        }
        /// <summary>
        /// Search And Replace common functioality in template file
        /// </summary> 
        /// 
        ///<param name="document">Recieve name of document </param>
        ///<param name="dict">Receive list of document details for replacement </param>
        public static void SearchAndReplace(string document, Dictionary<string, string> dict)
        {
            using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(document, true))
            {
                string docText = null;
                using (StreamReader sr = new StreamReader(wordDoc.MainDocumentPart.GetStream()))
                {
                    docText = sr.ReadToEnd();
                }

                foreach (KeyValuePair<string, string> item in dict)
                {
                    Regex regexText = new Regex(item.Key);
                    //regexText = regexText.ToString().ToLower();
                    docText = regexText.Replace(docText, item.Value);
                }

                using (StreamWriter sw = new StreamWriter(
                          wordDoc.MainDocumentPart.GetStream(FileMode.Create)))
                {
                    sw.Write(docText);
                }
            }            
        }
        #region Save doc to SharePoint
        /* Save the generated word document into SharePoint  */
        /// <summary>
        ///  Save the generated word document into SharePoint
        /// </summary> 
        /// 
        ///<param name="filepath">Receive investigation file path</param>
        ///<param name="ms">Receive memory stream data</param>
        public static void savetoSharePoint(Stream ms, string filepath)
        {
            using (var clientContext = new ClientContext("https://xmindsinfotech.sharepoint.com"))
            {
                SecureString passWord = new SecureString();
                foreach (char c in "xminds@123".ToCharArray()) passWord.AppendChar(c);
                var credentials = new SharePointOnlineCredentials("admin@xmindsinfotech.onmicrosoft.com", passWord);
                clientContext.Credentials = credentials;
                var list = clientContext.Web.Lists.GetByTitle("Documents");
                clientContext.Load(list.RootFolder);
                clientContext.ExecuteQuery();
                var fi = new FileInfo(filepath);
                var fileUrl = String.Format("{0}/{1}", list.RootFolder.ServerRelativeUrl, fi.Name);
                FileCreationInformation fileInfo = new FileCreationInformation();
                fileInfo.Content = System.IO.File.ReadAllBytes(filepath);
                fileInfo.Url = fileUrl;
                //Microsoft.SharePoint.Client.File.SaveBinaryDirect(clientContext, fileUrl, ms, true);
                Microsoft.SharePoint.Client.File uploadedFile = list.RootFolder.Files.Add(fileInfo);
                // Create invitation request list to multiple users 
                var users = new List<string>() { "raishwarya@xminds.in", "deepu@xminds.in", "jackson@xminds.in" };
                var userRoles = new List<UserRoleAssignment>();
                foreach (var user in users)
                {
                    UserRoleAssignment role = new UserRoleAssignment();
                    role.UserId = user;
                    role.Role = Role.Edit;
                    userRoles.Add(role);
                }
                string message = "Please accept this invite to access our SharePoint Site.";
                clientContext.Load(uploadedFile);
                clientContext.Load(clientContext.Web);
                clientContext.ExecuteQuery();
                string absoluteFileUrl = clientContext.Web.Url + uploadedFile.ServerRelativeUrl;
                DocumentSharingManager.UpdateDocumentSharingInfo(clientContext, absoluteFileUrl, userRoles, true, true, true, message, true, true);
                clientContext.ExecuteQuery();
                clientContext.Load(uploadedFile, f => f.ListItemAllFields);
                clientContext.ExecuteQuery();
                string uploadedUrl = clientContext.Web.Url + uploadedFile.ListItemAllFields["FileRef"];
            }
        }
        #endregion
    }
}