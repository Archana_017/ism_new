﻿using ClosedXML.Excel;
using ISM_Project.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace ISM_Project.Controllers
{
    public class SRManagerController : Controller
    {
        Common comm = new Common();
        gcaa_ismEntities dc = new gcaa_ismEntities();
        // GET: SRManager
        public ActionResult SafetyRecommentationReport(string Rptid, int? page, DateTime? FromDate, DateTime? ToDate, string invFileNo)
        {
            Reports objRpt = new Reports();
            try
            {
                if (FromDate == null || ToDate == null)
                {
                    objRpt.FromDate = Convert.ToDateTime(DateTime.Now.AddYears(-1).ToString("dd/MMM/yyyy"));

                    objRpt.ToDate = Convert.ToDateTime(DateTime.Now.ToString("dd/MMM/yyyy"));

                    ViewBag.FromDate = DateTime.Now.AddYears(-1).ToString("dd/MM/yyyy");

                    ViewBag.ToDate = DateTime.Now.ToString("dd/MM/yyyy");
                }
                else
                {
                    objRpt.FromDate = Convert.ToDateTime(objRpt.FromDate.ToString("dd/MMM/yyyy"));

                    objRpt.ToDate = Convert.ToDateTime(objRpt.ToDate.ToString("dd/MMM/yyyy"));

                    ViewBag.FromDate = Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy");

                    ViewBag.ToDate = Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy");
                }

                if (page == 0)
                {
                    page = null;
                }
                else if (page >= 1)
                {
                    objRpt.FromDate = Convert.ToDateTime(FromDate); objRpt.ToDate = Convert.ToDateTime(ToDate);
                }

                if (page == null)
                {
                    ViewBag.SR_serial_nbr = 0;
                }
                else
                {
                    ViewBag.SR_serial_nbr = 10 * (page - 1);
                }

                ViewBag.SRpage_no = page;

                TempData["BackPage"] = "Report";


                if (Rptid == "Sr")
                {


                    ViewBag.Report_Name = "Safety Recommendation Report";

                    objRpt.InvestigationFileNumber = invFileNo;

                    if (objRpt.InvestigationFileNumber == null) { objRpt.InvestigationFileNumber = string.Empty; }
                    if (objRpt.SR_Number == null) { objRpt.SR_Number = string.Empty; }

                    objRpt.ExcelInfo = dc.USP_VIEW_SR_FOR_EXCEEXPORT(objRpt.SR_Number, objRpt.LKAircraftRegistrationID, objRpt.InvestigationFileNumber, objRpt.LKAirportID,

                                                                  objRpt.LKOccurrenceCategorizationId).ToList();

                    ViewBag.count_SRpagecontent = objRpt.ExcelInfo.Count();
                }


                var Registration_id = dc.LKAircraftRegistrations.ToList().OrderBy(x => x.LKAircraftRegistrationName);
                SelectList Registrationid = new SelectList(Registration_id, "LKAircraftRegistrationID", "LKAircraftRegistrationName");
                ViewBag.Registration_id = Registrationid;

                var airport_id = dc.LKAirports.ToList().OrderBy(x => x.LKAirportName);
                SelectList airportid = new SelectList(airport_id, "LKAirportID", "LKAirportName");
                ViewBag.Airport_list = airportid;

                var occ_title = dc.LKOccurrenceCategorizations.ToList().OrderBy(x => x.LKOccurrenceCategorizationId);
                SelectList occTitle = new SelectList(occ_title, "LKOccurrenceCategorizationId", "OccurrenceCategorization");
                ViewBag.OccurCateg_list = occTitle;
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("SRManager", "SafetyRecommentationReport", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }
            return View(objRpt);
        }
        /// <summary>
        /// common report for Notification ,Investigation and safety recommendation 
        /// </summary>
        /// <param name="objRpt">Specify type of report </param>
        /// <param name="page">Receive the common page number</param>
        /// <param name="btnname">Receive the button whether it is show or export</param>      
        /// <returns></returns>
        [HttpPost]
        public ActionResult SafetyRecommentationReport(Reports objRpt, int? page, string btnname)
        {
            try
            {
                var Registration_id = dc.LKAircraftRegistrations.ToList().OrderBy(x => x.LKAircraftRegistrationName);
                SelectList Registrationid = new SelectList(Registration_id, "LKAircraftRegistrationID", "LKAircraftRegistrationName");
                ViewBag.Registration_id = Registrationid;

                var airport_id = dc.LKAirports.ToList().OrderBy(x => x.LKAirportName);
                SelectList airportid = new SelectList(airport_id, "LKAirportID", "LKAirportName");
                ViewBag.Airport_list = airportid;

                var occ_title = dc.LKOccurrenceCategorizations.ToList().OrderBy(x => x.LKOccurrenceCategorizationId);
                SelectList occTitle = new SelectList(occ_title, "LKOccurrenceCategorizationId", "OccurrenceCategorization");
                ViewBag.OccurCateg_list = occTitle;

                ViewBag.FromDate = objRpt.FromDate.ToString("dd/MM/yyyy");

                objRpt.FromDate = Convert.ToDateTime(objRpt.FromDate.ToString("dd/MMM/yyyy"));

                ViewBag.ToDate = objRpt.ToDate.ToString("dd/MM/yyyy");

                objRpt.ToDate = Convert.ToDateTime(objRpt.ToDate.ToString("dd/MMM/yyyy"));

                TempData["BackPage"] = "Report";

                if (objRpt.IsSrReport == "SR")
                {


                    ViewBag.Report_Name = "Safety Recommendation Report";

                    if (btnname == "Show")
                    {
                        if (page == null)
                        {
                            ViewBag.SR_serial_nbr = 0;
                        }
                        else
                        {
                            ViewBag.SR_serial_nbr = 10 * (page - 1);
                        }

                        if (objRpt.InvestigationFileNumber == null) { objRpt.InvestigationFileNumber = string.Empty; }
                        if (objRpt.SR_Number == null) { objRpt.SR_Number = string.Empty; }

                        objRpt.ExcelInfo = dc.USP_VIEW_SR_FOR_EXCEEXPORT(objRpt.SR_Number, objRpt.LKAircraftRegistrationID, objRpt.InvestigationFileNumber, objRpt.LKAirportID,

                                                                      objRpt.LKOccurrenceCategorizationId).ToList();

                        ViewBag.count_SRpagecontent = objRpt.ExcelInfo.Count();
                    }
                    else if (btnname == "Export")
                    {
                        DataTable dt = new DataTable("SRReport");

                        dt.Columns.AddRange(new DataColumn[11] { new DataColumn("Safety Recommendation Number"),
                           new DataColumn("Assigned Investigator Name"),
                           new DataColumn("Raised By Person"),
                           new DataColumn("Current Status"),
                           new DataColumn("Raised Date"),
                           new DataColumn("Raised Against (Department or Supplier)"),
                           new DataColumn("Investigation File Number"),
                           new DataColumn("Description of Occurrence"),
                           new DataColumn("SR Content"),
                           new DataColumn("Aircraft Registration "),
                           new DataColumn("Aircraft Model"),

                        });

                        if (objRpt.InvestigationFileNumber == null) { objRpt.InvestigationFileNumber = string.Empty; }
                        if (objRpt.SR_Number == null) { objRpt.SR_Number = string.Empty; }

                        var srinfo = dc.USP_VIEW_SR_FOR_EXCEEXPORT(objRpt.SR_Number, objRpt.LKAircraftRegistrationID, objRpt.InvestigationFileNumber, objRpt.LKAirportID,

                                                                   objRpt.LKOccurrenceCategorizationId).ToList();

                        foreach (var dr in srinfo)
                        {
                            dt.Rows.Add(dr.SafetyRecommendationNumber, dr.AssignedInvestigator, dr.AssignedInvestigator, dr.CurrentsrStatus, dr.SRRaisedDate, dr.RaisedAgainst, dr.InvestigationFileNumber,

                                        RemoveHtmlTags(dr.OccurrenceDescription), RemoveHtmlTags(dr.SafetyRecommendation), dr.RegistrationName, dr.AIRCRAFTMODELS);
                        }

                        using (XLWorkbook wb = new XLWorkbook())
                        {
                            wb.Worksheets.Add(dt);

                            using (MemoryStream stream = new MemoryStream())
                            {
                                wb.SaveAs(stream);
                                return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "SR_" + DateTime.Now + ".xlsx");
                            }
                        }
                    }
                }

            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("SRManager", "SafetyRecommentationReport", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }

            return View(objRpt);
        }
        public static string RemoveHtmlTags(string strHtml)
        {
            string strText = string.Empty;

            try
            {
                strText = Regex.Replace(strHtml, "<(.|\n)*?>", String.Empty);
                strText = HttpUtility.HtmlDecode(strText);
                strText = Regex.Replace(strText, @"\s+", " ");
            }
            catch
            {
                throw;
            }
            finally
            {

            }
            return strText;
        }
    }
}
