﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ISM_Project.Models;

namespace ISM_Project.Controllers
{
    /// <summary>
    /// DI - Controller - contains the functionality of Form35 , Notify ACCID and save and update of Aircraft details 
    /// </summary> 
    /// 
    public class DIController : Controller
    {
        MailConfiguration mail = new MailConfiguration();
        Common comm = new Common();
        static int InvestigationFileID = 0; static int OccurrenceId = 0; static int TabIndex = 0; static string NotificationId = string.Empty;
        PdfGeneration pdf = new PdfGeneration();
        gcaa_ismEntities dc = new gcaa_ismEntities();

        /// <summary>
        /// Convert date time value as string format
        /// </summary> 
        /// 
        /// <param name="dt">Receive the date time value from calendar .</param>
        public string convertDateTime(DateTime dt)
        {
            string conv_Time = string.Empty;
            if (dt != null)
                conv_Time = Convert.ToDateTime(dt).ToString("dd-MM-yyyy HH:mm");


            return conv_Time;
        }
        /// <summary>
        /// Fill DropDown Value And Default Data while page load 
        /// </summary> 
        /// 
        public void FillDroppedDownValueAndDefaultData()
        {
            #region Fill Master Data In ViewBag that is used to fill dropped down in view page of form35

            try
            {
                var country_id = dc.LKCountries.ToList().OrderBy(x => x.LKCountryName);
                SelectList countryid = new SelectList(country_id, "LKCountryID", "LKCountryName");
                ViewBag.country_id = countryid;

                var airport_id = dc.LKAirports.ToList().OrderBy(x => x.LKAirportName);
                SelectList airportid = new SelectList(airport_id, "LKAirportID", "LKAirportName");
                ViewBag.airport_id = airportid;

                var makes = dc.LKAircraftMakes.ToList().OrderBy(x => x.LKAircraftMakeName);
                SelectList makelist = new SelectList(makes, "LKAircraftMakeID", "LKAircraftMakeName");
                ViewBag.makes = makelist;

                var aircraft_type = dc.LKAircraftTypes.ToList().OrderBy(x => x.LKAircraftTypeName);
                SelectList aircrafttype = new SelectList(aircraft_type, "LKAircraftTypeID", "LKAircraftTypeName");
                ViewBag.aircraft_type = aircrafttype;

                var airline_operator = dc.LKAirlineOperators.ToList().OrderBy(x => x.LKAirlineOperatorName);
                SelectList airlineoperator = new SelectList(airline_operator, "LKAirlineOperatorID", "LKAirlineOperatorName");
                ViewBag.airline_operator = airlineoperator;

                var Category_id = dc.LKAircraftCategories.ToList().OrderBy(x => x.LKAircraftCategoryName);
                SelectList Category = new SelectList(Category_id, "LKAircraftCategoryId", "LKAircraftCategoryName");
                ViewBag.Category_id = Category;

                var common_OccurenceClassification = dc.USP_GET_INCIDENTTYPE().OrderBy(x => x.LKIncidentTypeID);
                SelectList OccurenceClassification = new SelectList(common_OccurenceClassification, "LKIncidentTypeID", "LKIncidentTypeName");
                ViewBag.common_OccurenceClassification = common_OccurenceClassification;

                var Accident_Incident = dc.USP_GET_MASTERVALUES(1); //10
                SelectList AccidentIncident = new SelectList(Accident_Incident, "ID", "LKVALUE");
                ViewBag.Accident_Incident = Accident_Incident;

                var largeaircraft_smallaircraft = dc.USP_GET_MASTERVALUES(2); // 11
                SelectList largeaircraftsmallaircraft = new SelectList(largeaircraft_smallaircraft, "ID", "LKVALUE");
                ViewBag.largeaircraft_smallaircraft = largeaircraft_smallaircraft;

                var Commercial_Private = dc.USP_GET_MASTERVALUES(3); //12
                SelectList CommercialPrivate = new SelectList(Commercial_Private, "ID", "LKVALUE");
                ViewBag.Commercial_Private = Commercial_Private;

                var UAEregisteredaircraft_UAEoperator = dc.USP_GET_MASTERVALUES(4); //13
                SelectList UAEregisteredaircraftUAEoperator = new SelectList(UAEregisteredaircraft_UAEoperator, "ID", "LKVALUE");
                ViewBag.UAEregisteredaircraft_UAEoperator = UAEregisteredaircraft_UAEoperator;

                var DI_Suggestion = dc.USP_GET_MASTERVALUES(5);
                SelectList DISuggestion = new SelectList(DI_Suggestion, "ID", "LKVALUE");
                ViewBag.DI_Suggestion = DI_Suggestion;

                var DI_FurtherSuggestion = dc.USP_GET_MASTERVALUES(6);
                SelectList DIFurtherSuggestion = new SelectList(DI_FurtherSuggestion, "ID", "LKVALUE");
                ViewBag.DI_FurtherSuggestion = DI_FurtherSuggestion;

                var Investigation_Type = dc.USP_GET_MASTERVALUES(7);
                SelectList InvestigationType = new SelectList(Investigation_Type, "ID", "LKVALUE");
                ViewBag.Investigation_Type = Investigation_Type;

                var Yes_No1 = dc.USP_GET_MASTERVALUES(17);
                SelectList YesNo = new SelectList(Yes_No1, "ID", "LKVALUE");
                ViewBag.Yes_No_1 = Yes_No1;

                var Yes_No2 = dc.USP_GET_MASTERVALUES(17);
                SelectList YesNo2 = new SelectList(Yes_No2, "ID", "LKVALUE");
                ViewBag.Yes_No_2 = Yes_No2;

                var Yes_No3 = dc.USP_GET_MASTERVALUES(17);
                SelectList YesNo3 = new SelectList(Yes_No3, "ID", "LKVALUE");
                ViewBag.Yes_No_3 = Yes_No3;

                var Yes_No4 = dc.USP_GET_MASTERVALUES(17);
                SelectList YesNo4 = new SelectList(Yes_No4, "ID", "LKVALUE");
                ViewBag.Yes_No_4 = Yes_No4;

                var Yes_No5 = dc.USP_GET_MASTERVALUES(17);
                SelectList YesNo5 = new SelectList(Yes_No5, "ID", "LKVALUE");
                ViewBag.Yes_No_5 = Yes_No5;

                var Yes_No6 = dc.USP_GET_MASTERVALUES(17);
                SelectList YesNo6 = new SelectList(Yes_No6, "ID", "LKVALUE");
                ViewBag.Yes_No_6 = Yes_No6;

                var Yes_No7 = dc.USP_GET_MASTERVALUES(17);
                SelectList YesNo7 = new SelectList(Yes_No7, "ID", "LKVALUE");
                ViewBag.Yes_No_7 = Yes_No7;

                var Yes_No8 = dc.USP_GET_MASTERVALUES(17);
                SelectList YesNo8 = new SelectList(Yes_No8, "ID", "LKVALUE");
                ViewBag.Yes_No_8 = Yes_No8;

                var Attach_Files = dc.USP_VIEW_INVESTIGATION_ATTACHMENTFILE(InvestigationFileID);
                SelectList AttachFiles = new SelectList(Attach_Files, "InvestigationDocumentAsAttachmentID", "AttachmentPublicUrl");
                ViewBag.Attach_Files = Attach_Files;

                var AircraftModel_id = dc.LKAircraftModels.ToList().OrderBy(x => x.LKAircraftModelName);
                SelectList AircraftModelId = new SelectList(AircraftModel_id, "LKAircraftModelId", "LKAircraftModelName");
                ViewBag.AircraftModel_Id = AircraftModelId;

                ViewBag.TimeGapInDays = string.Empty;

                ViewBag.TimeGapInHours = string.Empty;

                var Registration_id = dc.LKAircraftRegistrations.ToList().OrderBy(x => x.LKAircraftRegistrationName);
                SelectList Registrationid = new SelectList(Registration_id, "LKAircraftRegistrationID", "LKAircraftRegistrationName");
                ViewBag.Registration_id = Registrationid;

                var UAEoperator = dc.USP_GET_MASTERVALUES(22); //14
                SelectList UAE_operator = new SelectList(UAEoperator, "ID", "LKVALUE");
                ViewBag.UAE_operatorvalue = UAEoperator;

                var Occurrence_Categorization = dc.LKOccurrenceCategorizations.ToList().OrderBy(x => x.LKOccurrenceCategorizationId);
                SelectList OccurCategorization = new SelectList(Occurrence_Categorization, "LKOccurrenceCategorizationId", "OccurrenceCategorization");
                ViewBag.Occur_Categorization = OccurCategorization;
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("DIController", "FillDroppedDownValueAndDefaultData", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }

            #endregion
        }
        /// <summary>
        /// page redirect to Form35 view page with saved data
        /// </summary> 
        /// 
        /// <param name="id">Receive the investigation file id .</param>
        /// <param name="F35_status">Receive the status of investigation .</param>
        public ActionResult Form_35_Tab(int id = 0, string F35_status = "")
        {
            InvestigationModel _investigationMdl = new InvestigationModel();

            try
            {
                if (Session != null)
                {
                    if (Convert.ToInt32(Session["RoleId"]) == 2)
                    {
                        var CheckCaseInfo = dc.OccurrenceDetails.AsEnumerable().Where(a => a.CreatedBy == Convert.ToInt32(Session["Userid"]) &&

                                            a.OccurrenceDetailID == id).FirstOrDefault();

                        if (CheckCaseInfo != null)
                        {
                            if (F35_status == "Success")
                            {
                                ViewBag.F35_status = F35_status;
                            }
                            else if (F35_status == "F35_SubmitSuccess")
                            {
                                ViewBag.F35_SubmitSuccess = F35_status;
                            }

                            F35_status = "";

                            FillDroppedDownValueAndDefaultData();

                            #region Assign Values From Occurrence table

                            var _occurrenceDetails = dc.USP_VIEW_OCCURRENCE(id, Convert.ToInt32(Session["UserId"])).FirstOrDefault();

                            TempData["PageHead"] = _occurrenceDetails.NotificationId;

                            _investigationMdl.InvestigationFileID = 0; InvestigationFileID = 0; OccurrenceId = 0;

                            _investigationMdl.NotificationID = _occurrenceDetails.NotificationId;

                            NotificationId = _occurrenceDetails.NotificationId;

                            _investigationMdl.OccurrenceID = _occurrenceDetails.OccurrenceDetailID;

                            _investigationMdl.LKOccurrenceStatusTypeId = _occurrenceDetails.LKOccurrenceStatusTypeId;

                            ViewBag.LocalTime = convertDateTime(Convert.ToDateTime(_occurrenceDetails.LocalDateAndTimeOfOccurrence));

                            ViewBag.UTCDateAndTime = convertDateTime(_occurrenceDetails.LocalDateAndTimeOfOccurrence.Value.ToUniversalTime());

                            _investigationMdl.NotifierName = _occurrenceDetails.Notifier;

                            _investigationMdl.NotifierCallDate = _occurrenceDetails.NotifierCallDate;

                            //_investigationMdl.FlightNumber = _occurrenceDetails.FlightNumber;

                            //_investigationMdl.LKDepartureAirportID = _occurrenceDetails.LKDepartureAirportID;

                            //_investigationMdl.LKDestinationAirportID = _occurrenceDetails.LKDestinationAirportID;

                            //_investigationMdl.AircraftModelID = _occurrenceDetails.LKAircraftModelID;

                            //_investigationMdl.LKAircraftMakeID = _occurrenceDetails.LKAircraftMakeID;

                            //_investigationMdl.LKAircraftTypeID = _occurrenceDetails.LKAircraftTypeID;

                            //_investigationMdl.LKAirlineOperatorID = _occurrenceDetails.LKAirlineOperatorID;

                            //_investigationMdl.NameOfRegisteredOwner = _occurrenceDetails.NameOfRegisteredOwner;

                            //_investigationMdl.LKCountryID = _occurrenceDetails.LKCountryID;

                            _investigationMdl.PlaceOfIncident = _occurrenceDetails.CountryName;

                            ViewBag.NotiCallDate = convertDateTime(Convert.ToDateTime(_occurrenceDetails.NotifierCallDate));

                            _investigationMdl.LKIncidentTypeID = _occurrenceDetails.LKIncidentTypeID;

                            _investigationMdl.NoOfAircraftInvolved = _occurrenceDetails.NoOfAircraftInvolved;

                            _investigationMdl.EventDescription = _occurrenceDetails.OccurrenceDescription;

                            _investigationMdl.OccurrenceCategorizationId = _occurrenceDetails.OccurrenceCategorizationId;

                            var aircraft_occurence = dc.USP_VIEW_OCCURRENCE_AIRCRAFTDETAIL(id, 0).ToList();
                            ViewBag.aircraftinfo = aircraft_occurence;

                            #endregion

                            #region Assign Values From Investigation Table

                            var InvestDetails = dc.USP_VIEW_INVESTIGATION_BY_OCCURRENCE_ID(_investigationMdl.OccurrenceID).FirstOrDefault();

                            OccurrenceId = _investigationMdl.OccurrenceID;

                            if (TabIndex > 0)
                            {
                                ViewBag.InvTabIndex = TabIndex;

                                _investigationMdl.InvTabIndex = TabIndex;
                            }
                            else
                            {
                                ViewBag.InvTabIndex = 1;  // Set Default Tab Index

                                _investigationMdl.InvTabIndex = 1;

                                TabIndex = 1;
                            }

                            if (InvestDetails != null)
                            {
                                InvestigationFileID = InvestDetails.InvestigationFileID;

                                var Attach_Files = dc.USP_VIEW_INVESTIGATION_ATTACHMENTFILE(InvestDetails.InvestigationFileID);
                                SelectList AttachFiles = new SelectList(Attach_Files, "InvestigationDocumentAsAttachmentID", "AttachmentPublicUrl");
                                ViewBag.Attach_Files = Attach_Files;

                                var aircraft_Investigation = dc.USP_VIEW_INVESTIGATION_AIRCRAFT_DETAIL(InvestDetails.InvestigationFileID, Convert.ToInt32(Session["UserId"])).ToList();
                                ViewBag.aircraftInvesinfo = aircraft_Investigation;

                                _investigationMdl.InvestigationFileID = InvestDetails.InvestigationFileID;

                                _investigationMdl.AccidentOrSeriousIncident = InvestDetails.AccidentOrSeriousIncident;

                                _investigationMdl.NoOfAircraftInvolved = InvestDetails.NoOfAircraftInvolved;

                                _investigationMdl.DamagesToGroundObjects = InvestDetails.DamagesToGroundObjects;

                                _investigationMdl.PrevailingWeatherConditions = InvestDetails.PrevailingWeatherConditions;

                                _investigationMdl.DescriptionOfExplosivesAndDangerousArticles = InvestDetails.DescriptionOfExplosivesAndDangerousArticles;

                                _investigationMdl.ExtendOfDamageToAircraft = InvestDetails.ExtendOfDamageToAircraft;

                                //_investigationMdl.AircraftEngineInfo = InvestDetails.AircraftEngineInfo;

                                //_investigationMdl.AircraftMaximumMass = InvestDetails.AircraftMaximumMass;

                                //_investigationMdl.AircraftModelID = InvestDetails.LKAircraftModelID;

                                //_investigationMdl.LKAircraftMakeID = InvestDetails.LKAircraftMakeID;

                                //_investigationMdl.FlightNumber = InvestDetails.FlightNumber;

                                _investigationMdl.CrewsKilledInfo = InvestDetails.CrewsKilledInfo;

                                _investigationMdl.CrewsWithFatalInjuryInfo = InvestDetails.CrewsWithFatalInjuryInfo;

                                _investigationMdl.CrewsWithMinorInjuryInfo = InvestDetails.CrewsWithMinorInjuryInfo;

                                _investigationMdl.CrewsWithSeriousInjuryInfo = InvestDetails.CrewsWithSeriousInjuryInfo;

                                _investigationMdl.OthersKilledInfo = InvestDetails.OthersKilledInfo;

                                _investigationMdl.OthersWithFatalInjuryInfo = InvestDetails.OthersWithFatalInjuryInfo;

                                _investigationMdl.OthersWithMinorInjuryInfo = InvestDetails.OthersWithMinorInjuryInfo;

                                _investigationMdl.OthersWithSeriousInjuryInfo = InvestDetails.OthersWithSeriousInjuryInfo;

                                _investigationMdl.PassengersKilledInfo = InvestDetails.PassengersKilledInfo;

                                _investigationMdl.PassengersWithFatalInjuryInfo = InvestDetails.PassengersWithFatalInjuryInfo;

                                _investigationMdl.PassengersWithMinorInjuryInfo = InvestDetails.PassengersWithMinorInjuryInfo;

                                _investigationMdl.PassengersWithSeriousInjuryInfo = InvestDetails.PassengersWithSeriousInjuryInfo;

                                _investigationMdl.CrewsTotalInjury = InvestDetails.CrewsTotalInjury;

                                _investigationMdl.PassengersTotalInjury = InvestDetails.PassengersTotalInjury;

                                _investigationMdl.OthersTotalInjury = InvestDetails.OthersTotalInjury;

                                _investigationMdl.CabincrewWithFatalInjuryInfo = InvestDetails.CabincrewWithFatalInjuryInfo;
                                _investigationMdl.CabincrewWithSeriousInjuryInfo = InvestDetails.CabincrewWithSeriousInjuryInfo;
                                _investigationMdl.CabincrewWithMinorInjuryInfo = InvestDetails.CabincrewWithMinorInjuryInfo;
                                _investigationMdl.CabincrewsWithotherInjuryInfo = InvestDetails.CabincrewsWithotherInjuryInfo;
                                _investigationMdl.CabincrewsWithTotalInjuryInfo = InvestDetails.CabincrewsWithTotalInjuryInfo;
                                _investigationMdl.OthercrewWithFatalInjuryInfo = InvestDetails.OthercrewWithFatalInjuryInfo;
                                _investigationMdl.OthercrewWithSeriousInjuryInfo = InvestDetails.OthercrewWithSeriousInjuryInfo;
                                _investigationMdl.OthercrewWithMinorInjuryInfo = InvestDetails.OthercrewWithMinorInjuryInfo;
                                _investigationMdl.OthercrewsWithotherInjuryInfo = InvestDetails.OthercrewsWithotherInjuryInfo;
                                _investigationMdl.OthercrewsWithTotalInjuryInfo = InvestDetails.OthercrewsWithTotalInjuryInfo;
                                _investigationMdl.TotalOnBoardInfo = InvestDetails.TotalOnBoardInfo;
                                _investigationMdl.OtherOccurClassification = InvestDetails.OtherOccurClassification;
                                _investigationMdl.OccurrenceCategorizationId = InvestDetails.LKOccurrenceCategorizationId;
                                _investigationMdl.NameOfForeignInvestigator = InvestDetails.NameOfForeignInvestigator;
                                //_investigationMdl.PilotCertificateNumber = InvestDetails.PilotCertificateNumber;

                                //_investigationMdl.NameOfRegisteredOwner = InvestDetails.NameOfRegisteredOwner;

                                //_investigationMdl.PlaceOfIncidentLatitude = InvestDetails.PlaceOfIncidentLatitude;

                                //_investigationMdl.PlaceOfIncidentLongitude = InvestDetails.PlaceOfIncidentLongitude;

                                //_investigationMdl.OperatorContactNumber = InvestDetails.OperatorContactNumber;

                                //_investigationMdl.OperatorState = InvestDetails.OperatorState;

                                //_investigationMdl.QualificationOfPilot = InvestDetails.QualificationOfPilot;

                                //_investigationMdl.RegistrationMarkingsOnAircraft = InvestDetails.RegistrationMarkingsOnAircraft;

                                //_investigationMdl.LKAircraftCategoryId = InvestDetails.LKAircraftCategoryId;

                                //_investigationMdl.LKDepartureAirportID = InvestDetails.LKDepartureAirportID;

                                //_investigationMdl.LKDestinationAirportID = InvestDetails.LKDestinationAirportID;

                                //_investigationMdl.LKIncidentTypeID = InvestDetails.LKIncidentTypeID;

                                //_investigationMdl.LKAirlineOperatorOther = InvestDetails.LKAirlineOperatorOther;

                                //_investigationMdl.LKAirlineOperatorID = InvestDetails.LKAirlineOperatorID;

                                //_investigationMdl.PilotCertificateNumber = InvestDetails.PilotCertificateNumber;

                                //_investigationMdl.QualificationOfPilot = InvestDetails.QualificationOfPilot;

                                _investigationMdl.AnticipatedSafetyValueOfInvestigation = InvestDetails.AnticipatedSafetyValueOfInvestigation;

                                _investigationMdl.CommercialOrPrivate = InvestDetails.CommercialOrPrivate;

                                _investigationMdl.DutyInvestigatorComments = InvestDetails.DutyInvestigatorComments;

                                _investigationMdl.DutyInvestigatorFurtherSuggestion = InvestDetails.DutyInvestigatorFurtherSuggestion;

                                _investigationMdl.DutyInvestigatorSuggestions = InvestDetails.DutyInvestigatorSuggestions;

                                _investigationMdl.EventDescription = InvestDetails.EventDescription;

                                _investigationMdl.HaveRelevanceToSafetyProgram = InvestDetails.HaveRelevanceToSafetyProgram;

                                _investigationMdl.InvestigationType = InvestDetails.InvestigationType;

                                //_investigationMdl.LocalDateAndTimeOfOccurrence = InvestDetails.LocalDateAndTimeOfOccurrence;

                                //_investigationMdl.NotifierCallDate = InvestDetails.NotifierCallDate;

                                _investigationMdl.NotifierContactNumber = InvestDetails.NotifierContactNumber;

                                _investigationMdl.NotifierName = InvestDetails.NotifierName;

                                _investigationMdl.NotifierOrganization = InvestDetails.NotifierOrganization;

                                _investigationMdl.NotifierPosition = InvestDetails.NotifierPosition;

                                _investigationMdl.OppurtunityForOtherInvestigationAuthorities = InvestDetails.OppurtunityForOtherInvestigationAuthorities;

                                //_investigationMdl.PlaceOfIncident = InvestDetails.PlaceOfIncident;

                                InvestDetails.InvPlaceofIncident = InvestDetails.InvPlaceofIncident;

                                _investigationMdl.PotentialImpactOnPublicConfidence = InvestDetails.PotentialImpactOnPublicConfidence;

                                _investigationMdl.ResourceAvailability = InvestDetails.ResourceAvailability;

                                _investigationMdl.TimeBetweenOccurrenceAndNotification = InvestDetails.TimeBetweenOccurrenceAndNotification;

                                _investigationMdl.LargeOrSmallAircraft = InvestDetails.LargeOrSmallAircraft;

                                _investigationMdl.TrainingBenefitsToAAISInvestigators = InvestDetails.TrainingBenefitsToAAISInvestigators;

                                _investigationMdl.TrendOnOperatorOrAircraftType = InvestDetails.TrendOnOperatorOrAircraftType;

                                _investigationMdl.UAEOperator = InvestDetails.UAEOperator;

                                _investigationMdl.UAERegisteredAircraft = InvestDetails.UAERegisteredAircraft;

                                if (InvestDetails.UTCDateAndTimeOfOccurrence != null)
                                {
                                    if (InvestDetails.UTCDateAndTimeOfOccurrence != DateTime.MinValue)
                                    {
                                        ViewBag.UTCDateAndTime = convertDateTime(InvestDetails.LocalDateAndTimeOfOccurrence.Value.ToUniversalTime());
                                    }
                                    else
                                    {
                                        ViewBag.UTCDateAndTime = null;
                                    }
                                }
                                else
                                {
                                    ViewBag.UTCDateAndTime = null;
                                }

                                if (InvestDetails.InvDetNotifierCallDate != null)
                                {
                                    if (InvestDetails.InvDetNotifierCallDate != DateTime.MinValue)
                                    {
                                        ViewBag.NotiCallDate = convertDateTime(Convert.ToDateTime(InvestDetails.InvDetNotifierCallDate));
                                    }
                                    else
                                    {
                                        ViewBag.NotiCallDate = null;
                                    }
                                }
                                else
                                {
                                    ViewBag.NotiCallDate = null;
                                }

                                if (InvestDetails.LocalDateAndTimeOfOccurrence != null)
                                {
                                    if (InvestDetails.LocalDateAndTimeOfOccurrence != DateTime.MinValue)
                                    {
                                        ViewBag.LocalTime = convertDateTime(Convert.ToDateTime(InvestDetails.LocalDateAndTimeOfOccurrence));
                                    }
                                    else
                                    {
                                        ViewBag.LocalTime = null;
                                    }
                                }
                                else
                                {
                                    ViewBag.LocalTime = null;
                                }

                                _investigationMdl.LKIncidentTypeID = InvestDetails.LKIncidentTypeID;

                                _investigationMdl.LKCountryID = InvestDetails.LKCountryID;

                                //_investigationMdl.PlaceOfIncident = InvestDetails.PlaceOfIncident;

                                _investigationMdl.PlaceOfIncident = InvestDetails.InvPlaceofIncident;

                                _investigationMdl.DIName = InvestDetails.DIName;

                                _investigationMdl.LKOccurrenceStatusTypeId = InvestDetails.LKOccurrenceStatusTypeId;

                                if (InvestDetails.TimeBetweenOccurrenceAndNotification != null)
                                {
                                    String[] TimeBetweenOccurrenceAndNotification = InvestDetails.TimeBetweenOccurrenceAndNotification.ToString().Split(',');

                                    ViewBag.TimeGapInDays = TimeBetweenOccurrenceAndNotification[0].ToString().Split('D')[0].Trim();

                                    ViewBag.TimeGapInHours = TimeBetweenOccurrenceAndNotification[1].ToString().Split('H')[0].Trim();
                                }
                            }
                            else
                            {
                                ViewBag.Attach_Files = null;

                                ViewBag.InvTabIndex = 1;  // Set Default Tab Index

                                _investigationMdl.InvTabIndex = 1;

                                TabIndex = 1;
                            }
                            #endregion
                        }
                        else
                        {
                            return RedirectToAction("Dashboard", "Home");
                        }
                    }
                    else
                    {
                        return RedirectToAction("Dashboard", "Home");
                    }
                }
                else
                {
                    // return RedirectToAction("login");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("DIController", "Form_35_Tab(int id = 0)", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("NotFound", "Error");
            }

            return View(_investigationMdl);
        }
        /// <summary>
        /// save or update values in form_35 to different database table with attachment files
        /// </summary> 
        /// 
        /// <param name="InvFile">Receive the investigation file details.</param>
        [HttpPost]
        public ActionResult Form_35_Tab(InvestigationModel InvFile)
        {
            InvestigationModel _investigationMdl = new InvestigationModel();

            var DbTransaction = dc.Database.BeginTransaction();

            try
            {
                if (Session != null)
                {
                    if (Convert.ToInt32(Session["RoleId"]) == 2)
                    {
                        if (Convert.ToInt32(Session["UserId"]) > 0)
                        {
                            if (InvFile.TimeBetweenOccurrenceAndNotification == null) { InvFile.TimeBetweenOccurrenceAndNotification = string.Empty; }

                            if (InvFile.HoursBetweenOccurrenceAndNotification == null) { InvFile.HoursBetweenOccurrenceAndNotification = string.Empty; }

                            if (InvFile.TimeBetweenOccurrenceAndNotification == null)
                            {
                                InvFile.TimeBetweenOccurrenceAndNotification = "0";
                            }

                            if (InvFile.HoursBetweenOccurrenceAndNotification == null)
                            {
                                InvFile.HoursBetweenOccurrenceAndNotification = "0";
                            }

                            DateTime UTC = DateTime.MinValue;

                            if (InvFile.LocalDateAndTimeOfOccurrence != null)
                            {
                                UTC = InvFile.LocalDateAndTimeOfOccurrence.Value.ToUniversalTime();
                            }

                            if (InvFile.NotifierCallDate == null)
                            {
                                InvFile.NotifierCallDate = DateTime.MinValue;
                            }

                            if (InvFile.LocalDateAndTimeOfOccurrence == null)
                            {
                                InvFile.LocalDateAndTimeOfOccurrence = DateTime.MinValue;
                            }

                            if (InvestigationFileID == 0)
                            {
                                System.Data.Entity.Core.Objects.ObjectParameter InvFileId = new System.Data.Entity.Core.Objects.ObjectParameter("InvestFile_Id", typeof(Int32));

                                var result = dc.USP_INST_INVESTIGATION_FILE(InvFileId, InvFile.OccurrenceID, InvFile.NotificationID, Convert.ToInt32(Session["UserId"]), 0);

                                InvestigationFileID = Convert.ToInt32(dc.InvestigationFiles.Max(m => m.InvestigationFileID));

                                InvFile.InvestigationFileID = InvestigationFileID; OccurrenceId = InvFile.OccurrenceID;

                                SaveFiles(Request.Files, InvFile.InvestigationFileID);

                                if (InvFile.InvestigationFileID > 0)
                                {
                                    result = dc.USP_INST_INVESTIGATION_DETAIL(InvFile.InvestigationFileID, InvFile.LKIncidentTypeID, InvFile.NotifierName, InvFile.NotifierPosition,

                                        InvFile.NotifierOrganization, InvFile.NotifierContactNumber, InvFile.NotifierCallDate, InvFile.LocalDateAndTimeOfOccurrence,

                                        UTC, InvFile.LKCountryID, InvFile.PlaceOfIncident, InvFile.EventDescription,

                                        InvFile.DutyInvestigatorComments, InvFile.DutyInvestigatorSuggestions, InvFile.DutyInvestigatorFurtherSuggestion,

                                        InvFile.InvestigationType, InvFile.DIName, Convert.ToInt32(Session["UserId"]), InvFile.PrevailingWeatherConditions,

                                        InvFile.ExtendOfDamageToAircraft, InvFile.DamagesToGroundObjects, InvFile.DescriptionOfExplosivesAndDangerousArticles,

                                        InvFile.PassengersKilledInfo, InvFile.CrewsKilledInfo, InvFile.OthersKilledInfo,

                                        InvFile.PassengersWithFatalInjuryInfo, InvFile.CrewsWithFatalInjuryInfo, InvFile.OthersWithFatalInjuryInfo, InvFile.PassengersWithSeriousInjuryInfo,

                                        InvFile.CrewsWithSeriousInjuryInfo, InvFile.OthersWithSeriousInjuryInfo, InvFile.PassengersWithMinorInjuryInfo,

                                        InvFile.CrewsWithMinorInjuryInfo, InvFile.OthersWithMinorInjuryInfo, InvFile.CrewsTotalInjury, InvFile.PassengersTotalInjury, InvFile.OthersTotalInjury,

                                        InvFile.CabincrewWithFatalInjuryInfo, InvFile.CabincrewWithSeriousInjuryInfo, InvFile.CabincrewWithMinorInjuryInfo, InvFile.CabincrewsWithotherInjuryInfo, InvFile.CabincrewsWithTotalInjuryInfo,

                                        InvFile.OthercrewWithFatalInjuryInfo, InvFile.OthercrewWithSeriousInjuryInfo, InvFile.OthercrewWithMinorInjuryInfo, InvFile.OthercrewsWithotherInjuryInfo, InvFile.OthercrewsWithTotalInjuryInfo,

                                        InvFile.TotalOnBoardInfo,InvFile.OtherOccurClassification,InvFile.OccurrenceCategorizationId);

                                    result = dc.USPI_INST_INVESTIGATION_FILE_ASSESSMENT(InvFile.InvestigationFileID, InvFile.AccidentOrSeriousIncident, InvFile.LargeOrSmallAircraft,

                                        InvFile.CommercialOrPrivate, InvFile.UAERegisteredAircraft, InvFile.UAEOperator, InvFile.TrendOnOperatorOrAircraftType, InvFile.AnticipatedSafetyValueOfInvestigation,

                                        InvFile.PotentialImpactOnPublicConfidence, InvFile.HaveRelevanceToSafetyProgram, InvFile.ResourceAvailability, InvFile.OppurtunityForOtherInvestigationAuthorities,

                                        (InvFile.TimeBetweenOccurrenceAndNotification.ToString() + " Days, " + InvFile.HoursBetweenOccurrenceAndNotification.ToString() + " Hours"), InvFile.TrainingBenefitsToAAISInvestigators,InvFile.NameOfForeignInvestigator);
                                    try
                                    {
                                        for (int i = 0; i < InvFile.NoOfAircraftInvolved; i++)
                                        {
                                            int LKAircraftRegistrationID = Convert.ToInt32(InvFile.LKAircraftRegistrationID[i]);

                                            var airoperinfo = dc.LKAircraftRegistrations.Where(a => a.LKAircraftRegistrationID == LKAircraftRegistrationID).FirstOrDefault();

                                            int LKAirlineOperatorID = 0; int LKAircraftModelID = 0; int AircraftCategoryId = 0;

                                            if (airoperinfo != null)
                                            {
                                                LKAirlineOperatorID = Convert.ToInt32(airoperinfo.LKAirlineOperatorID);

                                                LKAircraftModelID = Convert.ToInt32(airoperinfo.LKAircraftModelID);

                                                AircraftCategoryId = Convert.ToInt32(airoperinfo.LKAircraftCategoryID);
                                            }

                                            #region Commented On 02-Aug-2018

                                            //result = dc.USP_INST_INVESTIGATION_AIRCRAFT_DETAIL(InvFile.InvestigationFileID, LKAircraftModelID,

                                            //   AircraftCategoryId, (InvFile.LKDepartureAirportID[i]), (InvFile.LKDestinationAirportID[i]),

                                            //   Convert.ToString(InvFile.MaximumMass[i]), (InvFile.EngineInfo[i]),

                                            //    InvFile.StateofRegistry[i], InvFile.LongLat[i], InvFile.FlightNumber[i].ToString(), InvFile.stateofoperatorid[i].ToString(), InvFile.LKAircraftRegistrationID[i],

                                            //    LKAirlineOperatorID, (InvFile.NameOfRegisteredOwner), (InvFile.QualificationOfPilot),

                                            //    InvFile.PilotCertificateNumber, InvFile.msn[i].ToString(), InvFile.OperatorContacts[i].ToString(),

                                            //    InvFile.PassengerOnboard[i], InvFile.CrewOnboard[i], InvFile.PassengersKilled[i], InvFile.CrewsKilled[i], InvFile.OthersKilled[i],

                                            //    InvFile.PassengersWithFatalInjury[i], InvFile.CrewsWithFatalInjury[i], InvFile.OthersWithFatalInjury[i],

                                            //    InvFile.PassengersWithSeriousInjury[i], InvFile.CrewsWithSeriousInjury[i], InvFile.OthersWithSeriousInjury[i],

                                            //    InvFile.PassengersWithMinorInjury[i], InvFile.CrewsWithMinorInjury[i], InvFile.OthersWithMinorInjury[i],

                                            //    Convert.ToInt32(Session["UserId"]));

                                            #endregion

                                            result = dc.USP_INST_INVESTIGATION_AIRCRAFT_DETAIL(InvFile.InvestigationFileID, LKAircraftModelID,

                                                   AircraftCategoryId, (InvFile.LKDepartureAirportID[i]), (InvFile.LKDestinationAirportID[i]),

                                                   Convert.ToString(InvFile.MaximumMass[i]), (InvFile.EngineInfo[i]),

                                                    InvFile.StateofRegistry[i], InvFile.LongLat[i], InvFile.FlightNumber[i].ToString(), InvFile.stateofoperatorid[i].ToString(), InvFile.LKAircraftRegistrationID[i],

                                                    LKAirlineOperatorID, (InvFile.NameOfRegisteredOwner), (InvFile.QualificationOfPilot),

                                                    InvFile.PilotCertificateNumber, InvFile.msn[i].ToString(), InvFile.OperatorContacts[i].ToString(),

                                                    InvFile.PassengerOnboard[i], InvFile.CrewOnboard[i], 0, 0, 0,

                                                    0, 0, 0, 0, 0, 0, 0, 0, 0,

                                                    Convert.ToInt32(Session["UserId"]),Convert.ToInt32(InvFile.CabinCrew[i]));
                                        }
                                    }
                                    catch
                                    {
                                        result = dc.USP_DEL_INVESTIGATION_AIRCRAFT_DETAIL(InvFile.InvestigationFileID);

                                        var OccurAirInfo = dc.OccurrenceAircraftDetails.Where(a => a.OccurrenceDetailID == OccurrenceId).ToList();

                                        foreach (var info in OccurAirInfo)
                                        {
                                            result = dc.USP_INST_INVESTIGATION_AIRCRAFT_DETAIL(InvFile.InvestigationFileID, (info.LKAircraftModelID),

                                                Convert.ToInt32(info.LkAircraftCategoryID), (info.LKDepartureAirportID), (info.LKDestinationAirportID),

                                                Convert.ToString(info.AircraftMaximumMass), (info.AircraftEngineInfo),

                                                info.AircraftStateofRegistry, info.latitudeAndlongitude, info.FlightNumber, info.StateofOperator.ToString(),

                                                info.LKAircraftRegistrationId,

                                                info.LkOperatorid, "", "",

                                                "", info.MSN, info.OperatorContactNo,

                                                info.PassengersOnBoard, info.CrewOnBoard, info.PassengersKilled, info.CrewsKilled, info.OthersKilled,

                                                info.PassengersWithFatalInjury, info.CrewsWithFatalInjury, info.OthersWithFatalInjury,

                                                info.PassengersWithSeriousInjury, info.CrewsWithSeriousInjury, info.OthersWithSeriousInjury,

                                                info.PassengersWithMinorInjury, info.CrewsWithMinorInjury, info.OthersWithMinorInjury,

                                                Convert.ToInt32(Session["UserId"]),info.CabinCrew);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                InvFile.InvestigationFileID = InvestigationFileID;

                                SaveFiles(Request.Files, InvFile.InvestigationFileID);

                                var result = dc.USP_UPDT_INVESTIGATION_FILE(InvFile.InvestigationFileID, Convert.ToInt32(Session["UserId"]), InvFile.NoOfAircraftInvolved);

                                result = dc.USP_UPDT_INVESTIGATION_DETAIL(InvFile.InvestigationFileID, InvFile.LKIncidentTypeID, InvFile.NotifierName, InvFile.NotifierPosition,

                                       InvFile.NotifierOrganization, InvFile.NotifierContactNumber, InvFile.NotifierCallDate, InvFile.LocalDateAndTimeOfOccurrence,

                                       UTC, InvFile.LKCountryID, InvFile.PlaceOfIncident, InvFile.EventDescription, InvFile.DutyInvestigatorComments, InvFile.DutyInvestigatorSuggestions, InvFile.DutyInvestigatorFurtherSuggestion,

                                       InvFile.InvestigationType, InvFile.DIName, Convert.ToInt32(Session["UserId"]),

                                       InvFile.PrevailingWeatherConditions,

                                        InvFile.ExtendOfDamageToAircraft, InvFile.DamagesToGroundObjects, InvFile.DescriptionOfExplosivesAndDangerousArticles,

                                        InvFile.PassengersKilledInfo, InvFile.CrewsKilledInfo, InvFile.OthersKilledInfo,

                                        InvFile.PassengersWithFatalInjuryInfo, InvFile.CrewsWithFatalInjuryInfo, InvFile.OthersWithFatalInjuryInfo, InvFile.PassengersWithSeriousInjuryInfo,

                                        InvFile.CrewsWithSeriousInjuryInfo, InvFile.OthersWithSeriousInjuryInfo, InvFile.PassengersWithMinorInjuryInfo,

                                        InvFile.CrewsWithMinorInjuryInfo, InvFile.OthersWithMinorInjuryInfo, InvFile.CrewsTotalInjury, InvFile.PassengersTotalInjury, InvFile.OthersTotalInjury,
                                        
                                        InvFile.CabincrewWithFatalInjuryInfo, InvFile.CabincrewWithSeriousInjuryInfo, InvFile.CabincrewWithMinorInjuryInfo, InvFile.CabincrewsWithotherInjuryInfo, InvFile.CabincrewsWithTotalInjuryInfo,

                                        InvFile.OthercrewWithFatalInjuryInfo, InvFile.OthercrewWithSeriousInjuryInfo, InvFile.OthercrewWithMinorInjuryInfo, InvFile.OthercrewsWithotherInjuryInfo, InvFile.OthercrewsWithTotalInjuryInfo,

                                        InvFile.TotalOnBoardInfo, InvFile.OtherOccurClassification,InvFile.OccurrenceCategorizationId);

                                result = dc.USP_UPDT_INVESTIGATION_FILE_ASSESSMENT(InvFile.InvestigationFileID, InvFile.AccidentOrSeriousIncident, InvFile.LargeOrSmallAircraft,

                                        InvFile.CommercialOrPrivate, InvFile.UAERegisteredAircraft, InvFile.UAEOperator, InvFile.TrendOnOperatorOrAircraftType, InvFile.AnticipatedSafetyValueOfInvestigation,

                                        InvFile.PotentialImpactOnPublicConfidence, InvFile.HaveRelevanceToSafetyProgram, InvFile.ResourceAvailability, InvFile.OppurtunityForOtherInvestigationAuthorities,

                                        (InvFile.TimeBetweenOccurrenceAndNotification.ToString() + " Days, " + InvFile.HoursBetweenOccurrenceAndNotification.ToString() + " Hours"), InvFile.TrainingBenefitsToAAISInvestigators,InvFile.NameOfForeignInvestigator);

                                result = dc.USP_DEL_INVESTIGATION_AIRCRAFT_DETAIL(InvFile.InvestigationFileID);

                                try
                                {
                                    for (int i = 0; i < InvFile.NoOfAircraftInvolved; i++)
                                    {

                                        int LKAircraftRegistrationID = Convert.ToInt32(InvFile.LKAircraftRegistrationID[i]);

                                        var airoperinfo = dc.LKAircraftRegistrations.Where(a => a.LKAircraftRegistrationID == LKAircraftRegistrationID).FirstOrDefault();

                                        int LKAirlineOperatorID = 0; int LKAircraftModelID = 0; int AircraftCategoryId = 0;

                                        if (airoperinfo != null)
                                        {
                                            LKAirlineOperatorID = Convert.ToInt32(airoperinfo.LKAirlineOperatorID);

                                            LKAircraftModelID = Convert.ToInt32(airoperinfo.LKAircraftModelID);

                                            AircraftCategoryId = Convert.ToInt32(airoperinfo.LKAircraftCategoryID);
                                        }

                                        #region Commented on 02-Aug-2018

                                        //result = dc.USP_INST_INVESTIGATION_AIRCRAFT_DETAIL(InvFile.InvestigationFileID, LKAircraftModelID,

                                        //    AircraftCategoryId, (InvFile.LKDepartureAirportID[i]), (InvFile.LKDestinationAirportID[i]),

                                        //    Convert.ToString(InvFile.MaximumMass[i]), (InvFile.EngineInfo[i]),

                                        //    InvFile.StateofRegistry[i], InvFile.LongLat[i], InvFile.FlightNumber[i].ToString(), InvFile.stateofoperatorid[i].ToString(), InvFile.LKAircraftRegistrationID[i],

                                        //    LKAirlineOperatorID, (InvFile.NameOfRegisteredOwner), (InvFile.QualificationOfPilot),

                                        //    InvFile.PilotCertificateNumber, InvFile.msn[i].ToString(), InvFile.OperatorContacts[i].ToString(),

                                        //    InvFile.PassengerOnboard[i], InvFile.CrewOnboard[i], InvFile.PassengersKilled[i], InvFile.CrewsKilled[i], InvFile.OthersKilled[i],

                                        //    InvFile.PassengersWithFatalInjury[i], InvFile.CrewsWithFatalInjury[i], InvFile.OthersWithFatalInjury[i],

                                        //    InvFile.PassengersWithSeriousInjury[i], InvFile.CrewsWithSeriousInjury[i], InvFile.OthersWithSeriousInjury[i],

                                        //    InvFile.PassengersWithMinorInjury[i], InvFile.CrewsWithMinorInjury[i], InvFile.OthersWithMinorInjury[i],

                                        //    Convert.ToInt32(Session["UserId"]));

                                        #endregion

                                        result = dc.USP_INST_INVESTIGATION_AIRCRAFT_DETAIL(InvFile.InvestigationFileID, LKAircraftModelID,

                                                AircraftCategoryId, (InvFile.LKDepartureAirportID[i]), (InvFile.LKDestinationAirportID[i]),

                                                Convert.ToString(InvFile.MaximumMass[i]), (InvFile.EngineInfo[i]),

                                                InvFile.StateofRegistry[i], InvFile.LongLat[i], InvFile.FlightNumber[i].ToString(), InvFile.stateofoperatorid[i].ToString(), InvFile.LKAircraftRegistrationID[i],

                                                LKAirlineOperatorID, (InvFile.NameOfRegisteredOwner), (InvFile.QualificationOfPilot),

                                                InvFile.PilotCertificateNumber, InvFile.msn[i].ToString(), InvFile.OperatorContacts[i].ToString(),

                                                InvFile.PassengerOnboard[i], InvFile.CrewOnboard[i], 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,

                                                Convert.ToInt32(Session["UserId"]),Convert.ToInt32(InvFile.CabinCrew[i]));
                                    }
                                }
                                catch
                                {
                                    result = dc.USP_DEL_INVESTIGATION_AIRCRAFT_DETAIL(InvFile.InvestigationFileID);

                                    var OccurAirInfo = dc.OccurrenceAircraftDetails.Where(a => a.OccurrenceDetailID == OccurrenceId).ToList();

                                    foreach (var info in OccurAirInfo)
                                    {
                                        result = dc.USP_INST_INVESTIGATION_AIRCRAFT_DETAIL(InvFile.InvestigationFileID, (info.LKAircraftModelID),

                                            Convert.ToInt32(info.LkAircraftCategoryID), (info.LKDepartureAirportID), (info.LKDestinationAirportID),

                                            Convert.ToString(info.AircraftMaximumMass), (info.AircraftEngineInfo),

                                            info.AircraftStateofRegistry, info.latitudeAndlongitude, info.FlightNumber, info.StateofOperator.ToString(),

                                            info.LKAircraftRegistrationId,

                                            info.LkOperatorid, "", "",

                                            "", info.MSN, info.OperatorContactNo,

                                            info.PassengersOnBoard, info.CrewOnBoard, info.PassengersKilled, info.CrewsKilled, info.OthersKilled,

                                            info.PassengersWithFatalInjury, info.CrewsWithFatalInjury, info.OthersWithFatalInjury,

                                            info.PassengersWithSeriousInjury, info.CrewsWithSeriousInjury, info.OthersWithSeriousInjury,

                                            info.PassengersWithMinorInjury, info.CrewsWithMinorInjury, info.OthersWithMinorInjury,

                                            Convert.ToInt32(Session["UserId"]),info.CabinCrew);
                                    }
                                }
                            }

                            DbTransaction.Commit();

                            //TabIndex = InvFile.InvTabIndex;  on 20-jul-2018

                            TabIndex = 0;

                            #region Assign Values From Investigation Table

                            //FillDroppedDownValueAndDefaultData();

                            //var InvestDetails = dc.USP_VIEW_INVESTIGATION_BY_OCCURRENCE_ID(OccurrenceId).FirstOrDefault();

                            //_investigationMdl.InvestigationFileID = InvFile.InvestigationFileID;

                            //TabIndex = InvFile.InvTabIndex;

                            //if (InvestDetails != null)
                            //{
                            //    TempData["PageHead"] = InvestDetails.NotificationID;

                            //    var Attach_Files = dc.USP_VIEW_INVESTIGATION_ATTACHMENTFILE(InvestDetails.InvestigationFileID);
                            //    SelectList AttachFiles = new SelectList(Attach_Files, "InvestigationDocumentAsAttachmentID", "AttachmentPublicUrl");
                            //    ViewBag.Attach_Files = Attach_Files;

                            //    var aircraft_Investigation = dc.USP_VIEW_INVESTIGATION_AIRCRAFT_DETAIL(InvestDetails.InvestigationFileID, Convert.ToInt32(Session["UserId"])).ToList();
                            //    ViewBag.aircraftInvesinfo = aircraft_Investigation;

                            //    _investigationMdl.InvestigationFileID = InvestDetails.InvestigationFileID;

                            //    _investigationMdl.AccidentOrSeriousIncident = InvestDetails.AccidentOrSeriousIncident;

                            //    _investigationMdl.AnticipatedSafetyValueOfInvestigation = InvestDetails.AnticipatedSafetyValueOfInvestigation;

                            //    _investigationMdl.CommercialOrPrivate = InvestDetails.CommercialOrPrivate;

                            //    _investigationMdl.DutyInvestigatorComments = InvestDetails.DutyInvestigatorComments;

                            //    _investigationMdl.DutyInvestigatorFurtherSuggestion = InvestDetails.DutyInvestigatorFurtherSuggestion;

                            //    _investigationMdl.DutyInvestigatorSuggestions = InvestDetails.DutyInvestigatorSuggestions;

                            //    _investigationMdl.EventDescription = InvestDetails.EventDescription;

                            //    _investigationMdl.HaveRelevanceToSafetyProgram = InvestDetails.HaveRelevanceToSafetyProgram;

                            //    _investigationMdl.InvestigationType = InvestDetails.InvestigationType;

                            //    _investigationMdl.LocalDateAndTimeOfOccurrence = InvestDetails.LocalDateAndTimeOfOccurrence;

                            //    _investigationMdl.NotifierCallDate = InvestDetails.NotifierCallDate;

                            //    _investigationMdl.NotifierContactNumber = InvestDetails.NotifierContactNumber;

                            //    _investigationMdl.NotifierName = InvestDetails.NotifierName;

                            //    _investigationMdl.NotifierOrganization = InvestDetails.NotifierOrganization;

                            //    _investigationMdl.NotifierPosition = InvestDetails.NotifierPosition;

                            //    //_investigationMdl.AircraftEngineInfo = InvestDetails.AircraftEngineInfo;

                            //    //_investigationMdl.AircraftMaximumMass = InvestDetails.AircraftMaximumMass;

                            //    //_investigationMdl.AircraftModelID = InvestDetails.LKAircraftModelID;

                            //    //_investigationMdl.LKAircraftMakeID = InvestDetails.LKAircraftMakeID;

                            //    //_investigationMdl.CrewOnBoard = InvestDetails.CrewOnBoard;

                            //    //_investigationMdl.CrewsKilled = InvestDetails.CrewsKilled;

                            //    //_investigationMdl.CrewsWithFatalInjury = InvestDetails.CrewsWithFatalInjury;

                            //    //_investigationMdl.CrewsWithMinorInjury = InvestDetails.CrewsWithMinorInjury;

                            //    //_investigationMdl.CrewsWithSeriousInjury = InvestDetails.CrewsWithSeriousInjury;

                            //    //_investigationMdl.FlightNumber = InvestDetails.FlightNumber;

                            //    //_investigationMdl.NameOfRegisteredOwner = InvestDetails.NameOfRegisteredOwner;

                            //    //_investigationMdl.OperatorContactNumber = InvestDetails.OperatorContactNumber;

                            //    //_investigationMdl.OperatorState = InvestDetails.OperatorState;

                            //    //_investigationMdl.OthersKilled = InvestDetails.OthersKilled;

                            //    //_investigationMdl.OthersWithFatalInjury = InvestDetails.OthersWithFatalInjury;

                            //    //_investigationMdl.OthersWithMinorInjury = InvestDetails.OthersWithMinorInjury;

                            //    //_investigationMdl.OthersWithSeriousInjury = InvestDetails.OthersWithSeriousInjury;

                            //    //_investigationMdl.PassengersKilled = InvestDetails.PassengersKilled;

                            //    //_investigationMdl.PassengersOnBoard = InvestDetails.PassengersOnBoard;

                            //    //_investigationMdl.PassengersWithFatalInjury = InvestDetails.PassengersWithFatalInjury;

                            //    //_investigationMdl.PassengersWithMinorInjury = InvestDetails.PassengersWithMinorInjury;

                            //    //_investigationMdl.PassengersWithSeriousInjury = InvestDetails.PassengersWithSeriousInjury;

                            //    //_investigationMdl.PilotCertificateNumber = InvestDetails.PilotCertificateNumber;

                            //    //_investigationMdl.PlaceOfIncidentLatitude = InvestDetails.PlaceOfIncidentLatitude;

                            //    //_investigationMdl.PlaceOfIncidentLongitude = InvestDetails.PlaceOfIncidentLongitude;

                            //    //_investigationMdl.QualificationOfPilot = InvestDetails.QualificationOfPilot;

                            //    //_investigationMdl.RegistrationMarkingsOnAircraft = InvestDetails.RegistrationMarkingsOnAircraft;

                            //    //_investigationMdl.LKAircraftCategoryId = InvestDetails.LKAircraftCategoryId;

                            //    //_investigationMdl.LKDepartureAirportID = InvestDetails.LKDepartureAirportID;

                            //    //_investigationMdl.LKDestinationAirportID = InvestDetails.LKDestinationAirportID;

                            //    //_investigationMdl.LKAirlineOperatorOther = InvestDetails.LKAirlineOperatorOther;

                            //    //_investigationMdl.LKAirlineOperatorID = InvestDetails.LKAirlineOperatorID;

                            //    //_investigationMdl.PilotCertificateNumber = InvestDetails.PilotCertificateNumber;

                            //    //_investigationMdl.QualificationOfPilot = InvestDetails.QualificationOfPilot;

                            //    _investigationMdl.NoOfAircraftInvolved = InvestDetails.NoOfAircraftInvolved;

                            //    _investigationMdl.OppurtunityForOtherInvestigationAuthorities = InvestDetails.OppurtunityForOtherInvestigationAuthorities;

                            //    _investigationMdl.PlaceOfIncident = InvestDetails.PlaceOfIncident;

                            //    _investigationMdl.PotentialImpactOnPublicConfidence = InvestDetails.PotentialImpactOnPublicConfidence;

                            //    _investigationMdl.ResourceAvailability = InvestDetails.ResourceAvailability;

                            //    _investigationMdl.TimeBetweenOccurrenceAndNotification = InvestDetails.TimeBetweenOccurrenceAndNotification;

                            //    _investigationMdl.LargeOrSmallAircraft = InvestDetails.LargeOrSmallAircraft;

                            //    _investigationMdl.TrainingBenefitsToAAISInvestigators = InvestDetails.TrainingBenefitsToAAISInvestigators;

                            //    _investigationMdl.TrendOnOperatorOrAircraftType = InvestDetails.TrendOnOperatorOrAircraftType;

                            //    _investigationMdl.UAEOperator = InvestDetails.UAEOperator;

                            //    _investigationMdl.UAERegisteredAircraft = InvestDetails.UAERegisteredAircraft;

                            //    if (InvestDetails.UTCDateAndTimeOfOccurrence != null)
                            //    {
                            //        ViewBag.UTCDateAndTime = convertDateTime(InvestDetails.LocalDateAndTimeOfOccurrence.Value.ToUniversalTime());
                            //    }
                            //    else
                            //    {
                            //        ViewBag.UTCDateAndTime = null;
                            //    }

                            //    if (InvestDetails.NotifierCallDate != null)
                            //    {
                            //        ViewBag.NotiCallDate = convertDateTime(Convert.ToDateTime(InvestDetails.NotifierCallDate));
                            //    }
                            //    else
                            //    {
                            //        ViewBag.NotiCallDate = null;
                            //    }

                            //    if (InvestDetails.LocalDateAndTimeOfOccurrence != null)
                            //    {
                            //        ViewBag.LocalTime = convertDateTime(Convert.ToDateTime(InvestDetails.LocalDateAndTimeOfOccurrence));
                            //    }
                            //    else
                            //    {
                            //        ViewBag.LocalTime = null;
                            //    }

                            //    _investigationMdl.LKIncidentTypeID = InvestDetails.LKIncidentTypeID;

                            //    _investigationMdl.LKCountryID = InvestDetails.LKCountryID;

                            //    _investigationMdl.PlaceOfIncident = InvestDetails.PlaceOfIncident;

                            //    _investigationMdl.DIName = InvestDetails.DIName;

                            //    _investigationMdl.LKOccurrenceStatusTypeId = InvestDetails.LKOccurrenceStatusTypeId;

                            //    if (InvestDetails.TimeBetweenOccurrenceAndNotification != null)
                            //    {
                            //        String[] TimeBetweenOccurrenceAndNotification = InvestDetails.TimeBetweenOccurrenceAndNotification.ToString().Split(',');

                            //        ViewBag.TimeGapInDays = TimeBetweenOccurrenceAndNotification[0].ToString().Split('D')[0].Trim();

                            //        ViewBag.TimeGapInHours = TimeBetweenOccurrenceAndNotification[1].ToString().Split('H')[0].Trim();
                            //    }
                            //}

                            #endregion
                        }
                        else
                        {
                            return RedirectToAction("Dashboard", "Home");
                        }

                        var fullpathoffile = Server.MapPath("~/App_Data/Form35_Files/" + OccurrenceId);

                        if (System.IO.Directory.Exists(fullpathoffile))
                        {
                            comm.DeleteDirectory(fullpathoffile);

                            System.IO.Directory.Delete(fullpathoffile);
                        }

                    }

                    return RedirectToAction("Form_35_Tab", new { id = InvFile.OccurrenceID, F35_status = "Success" });

                    //  return RedirectToAction("Dashboard", "Home");
                }
                else
                {
                    // return RedirectToAction("login");
                }
            }
            catch (System.Exception ex)
            {
                DbTransaction.Rollback();

                comm.Exception_Log("DIController", "Form_35_Tab", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace,

                    Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }

            return View(_investigationMdl);
        }
        //[HttpPost]
        //public void SaveFiles()
        //{
        //    try
        //    {
        //        if (InvestigationFileID > 0)
        //        {
        //            if (Request.Files.Count > 0)
        //            {
        //                //comm.CreateIfMissing(Server.MapPath("~/") + "AttachmentFiles");

        //                for (int i = 0; i < Request.Files.Count; i++)
        //                {
        //                    string[] filePath = null;

        //                    HttpPostedFileBase file = Request.Files[i];

        //                    int fileSize = file.ContentLength;

        //                    //   string fileName = file.FileName;

        //                    string fileName = file.FileName.Substring(0, file.FileName.LastIndexOf('.')) + "_" + DateTime.Now.ToFileTime() + file.FileName.Substring(file.FileName.LastIndexOf('.'), ((file.FileName.Length) - (file.FileName.LastIndexOf('.'))));

        //                    string mimeType = file.ContentType.Split('/')[1].ToString();

        //                    int DocumentTypeId = Convert.ToInt32(dc.LKDocumentTypes.AsQueryable().Where(d => d.LKDocumentTypeName == mimeType).Select(d => d.LKDocumentTypeID).FirstOrDefault());

        //                    Stream fileContent = file.InputStream;

        //                    //file.SaveAs(Server.MapPath("~/") + "AttachmentFiles\\" + fileName);

        //                    //  filePath = comm.savetoSharePoint(fileContent, Server.MapPath("~/") + "AttachmentFiles\\" + fileName, InvestigationFileID);

        //                    filePath = comm.savetoSharePointInvOnPrem(fileContent, fileName, InvestigationFileID);

        //                    if (filePath[0] != string.Empty)
        //                    {
        //                        var result = dc.USP_INST_INVESTIGATION_DOCUMENTASATTACHMENT(0, InvestigationFileID, 1, DocumentTypeId, filePath[0].ToString(), filePath[1].ToString(), Convert.ToInt32(Session["UserId"]));
        //                    }
        //                }

        //                //comm.DeleteDirectory(Server.MapPath("~/") + "AttachmentFiles");
        //            }
        //        }
        //        else
        //        {
        //            if (Request.Files.Count > 0)
        //            {
        //                //  comm.CreateIfMissing(Server.MapPath("~/") + "AttachmentFiles");

        //                int maxInvId = dc.InvestigationFiles.DefaultIfEmpty().Max(m => m.InvestigationFileID) + 1;

        //                for (int i = 0; i < Request.Files.Count; i++)
        //                {
        //                    string[] filePath = null;

        //                    HttpPostedFileBase file = Request.Files[i];

        //                    int fileSize = file.ContentLength;

        //                    string fileName = file.FileName;

        //                    string mimeType = file.ContentType.Split('/')[1].ToString();

        //                    int DocumentTypeId = Convert.ToInt32(dc.LKDocumentTypes.AsQueryable().Where(d => d.LKDocumentTypeName == mimeType).Select(d => d.LKDocumentTypeID).FirstOrDefault());

        //                    Stream fileContent = file.InputStream;

        //                    // file.SaveAs(Server.MapPath("~/") + "AttachmentFiles\\" + fileName);

        //                    //filePath = comm.savetoSharePoint(fileContent, fileName, InvestigationFileID);

        //                    filePath = comm.savetoSharePointInvOnPrem(fileContent, fileName, InvestigationFileID);

        //                    if (filePath[0] != string.Empty)
        //                    {
        //                        var result = dc.USP_INST_INVESTIGATION_DOCUMENTASATTACHMENT(0, maxInvId, 1, DocumentTypeId, filePath[0].ToString(), filePath[1].ToString(), Convert.ToInt32(Session["UserId"]));
        //                    }
        //                }

        //                //comm.DeleteDirectory(Server.MapPath("~/") + "AttachmentFiles");
        //            }
        //        }
        //    }
        //    catch (System.Exception ex)
        //    {
        //        comm.Exception_Log("DIController", "SaveFiles", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
        //    }
        //}
        static string[] SharepointfilePath = new string[100]; 
        static string[] SharepointAnonymousPath = new string[100];
        /// <summary>
        /// save all the attached file to share point from form 35 and particular file sharepoint filepath saved to database
        /// </summary> 
        /// 
        /// <param name="files">Receive the investigation file attachment details as http file collection.</param>
        /// <param name="INVFILE_NUMER">Receive the investigation file number.</param>
        public void SaveFiles(HttpFileCollectionBase files, int INVFILE_NUMER)
        {
            try
            {
                if (files.Count > 0)
                {
                    if (INVFILE_NUMER > 0)
                    {
                        int filecount = 0;

                        for (int i = 0; i < files.Count; i++)
                        {
                            HttpPostedFileBase file = files[i];

                            if (file.ContentLength > 0 && !string.IsNullOrEmpty(file.FileName))
                            {
                                int fileSize = file.ContentLength;

                                string fileName = file.FileName.Substring(0, file.FileName.LastIndexOf('.')) + "_" + DateTime.Now.ToFileTime() + file.FileName.Substring(file.FileName.LastIndexOf('.'), ((file.FileName.Length) - (file.FileName.LastIndexOf('.'))));

                                string mimeType = file.ContentType.Split('/')[1].ToString();

                                int DocumentTypeId = Convert.ToInt32(dc.LKDocumentTypes.AsQueryable().Where(d => d.LKDocumentTypeName == mimeType).Select(d => d.LKDocumentTypeID).FirstOrDefault());

                                Stream fileContent = file.InputStream;

                                string[] attachURLs = comm.savetoSharePointInvOnPrem(fileContent, fileName, INVFILE_NUMER);

                                var result = dc.USP_INST_INVESTIGATION_DOCUMENTASATTACHMENT(0, INVFILE_NUMER, 1, DocumentTypeId, attachURLs[0].ToString(), attachURLs[1].ToString(), Convert.ToInt32(Session["UserId"]));

                                SharepointfilePath[filecount] = attachURLs[0];

                                SharepointAnonymousPath[filecount] = attachURLs[1];

                                filecount++;
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("DIController", "SaveFiles", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }
        }
        /// <summary>
        ///   form 35 Validate while save the information
        /// </summary> 
        /// 
        /// <param name="invId">Receive the investigation file id.</param>
        public int ValidateForm35(int invId = 0)
        {
            int result = 1;

            try
            {
                if (Convert.ToInt32(Session["UserId"]) > 0)
                {
                    var resu = dc.USP_VALIDATE_INVESTIGATION_DETAILS(invId);

                    string val = (resu.SingleOrDefault()).ToString();

                    if (val == string.Empty)
                    {
                        result = 1;
                    }
                    else
                    {
                        result = Convert.ToInt32(val);

                        if (result > 0)
                        {
                            result = 0;
                        }
                        else
                        {
                            result = 1;
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("DIController", "ValidateForm35", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }

            return result;
        }
        /// <summary>
        /// While submit form35 form the status of the occurrence status chahged and tranfer to director dashboard for further investigation process
        /// </summary> 
        /// 
        /// <param name="InvFile">Receive the investigation file details as object.</param>
        [HttpPost]
        public ActionResult SubmitForm35(InvestigationModel InvFile)
        {
            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 2)
                {
                    if (Convert.ToInt32(Session["Userid"]) > 0)
                    {
                        if (InvFile.OccurrenceID > 0)
                        {
                            // change the occurrence status (5) - for more info verify lookup table

                            var result = dc.USP_UPDT_OCCURRANCESTATUS_BY_OCCURRANCE_ID(InvFile.OccurrenceID, 5, Convert.ToInt32(Session["UserId"]));

                            dc.SaveChanges();

                            TabIndex = 1;

                            NotifyDL(InvFile.OccurrenceID);
                        }
                    }
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("DIController", "SubmitForm35", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }

            return RedirectToAction("Form_35_Tab", new { id = InvFile.OccurrenceID, F35_status = "F35_SubmitSuccess" });

            //  return RedirectToAction("/DI/");
        }

        public JsonResult NotifyDL(int occur_Id)
        {
            bool issent = false;
            try
            {
                if (occur_Id != 0)
                {
                    using (gcaa_ismEntities db = new gcaa_ismEntities())
                    {
                        var DList = (from users in db.Users where users.UserRoleID == 3 select users).ToList();
                        List<string> DListUsers = new List<string>();
                        List<string> DListUsersEmail = new List<string>();
                        foreach (var users in DList)
                        {
                            DListUsersEmail.Add(users.EmailAddress);
                            DListUsers.Add(users.FirstName);
                        }
                        string siteURL = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));

                        var aircraft_operator = db.USP_VIEW_OCCURRENCE_PDF(occur_Id, Convert.ToInt32(Session["UserId"])).ToList();

                        string aircraft_oper = null;

                        foreach (dynamic air_operator in aircraft_operator)
                        {
                            if (aircraft_oper == null)
                            {
                                aircraft_oper = air_operator.OperatorName;
                            }
                            else
                            {
                                aircraft_oper += " , " + air_operator.OperatorName;
                            }
                        }

                        dynamic Occ = db.USP_VIEW_OCCURRENCE_PDF(occur_Id, Convert.ToInt32(Session["UserId"])).FirstOrDefault();
                        dynamic occAircraft = db.USP_VIEW_OCCURRENCE_AIRCRAFTDETAIL_PDF(occur_Id, Convert.ToInt32(Session["UserId"])).ToList();
                        string sourceFile = pdf.generatePdf(Occ, occAircraft, aircraft_oper, "notify", siteURL);
                        issent = mail.SendMailNotificationAsInline(27, Convert.ToString(Session["FirstName"]), Convert.ToInt32(Session["roleid"]), siteURL, DListUsers, DListUsersEmail, occur_Id, sourceFile, Occ.NotificationId, null, null, sourceFile);
                    }
                }
            }
            catch (System.Exception ex)
            {
                issent = false;
                comm.Exception_Log("Notifications", "NotifyDL", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }
            return Json(issent);
        }
        /// <summary>
        /// Delete attachment files remove from database and also sharepoint server
        /// </summary> 
        /// 
        /// <param name="id">Receive the investigation file id.</param>
        /// <param name="path">Receive the investigation file attachment path.</param>
        [HttpPost]
        public JsonResult DeleteAttachment(Int32 id, string path)
        {
            string results = string.Empty;

            try
            {
                var result = 0;

                if (Convert.ToInt32(Session["RoleId"]) == 2)
                {
                    if (Convert.ToInt32(id) > 0)
                    {
                        if (Convert.ToInt32(Session["UserId"]) > 0)
                        {
                            //var attachedFILE = dc.USP_VIEW_ATTACHMENT(id).FirstOrDefault();

                            bool IsFileDeleted = comm.deleteFiles(path);

                            result = dc.USP_INST_INVESTIGATION_DOCUMENTASATTACHMENT(Convert.ToInt32(id), 0, 0, 0, null, "", Convert.ToInt32(Session["UserId"]));

                            results = "success";
                        }
                    }
                }
                else
                {
                    results = "Failed";

                    return Json(results);
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("DIController", "DeleteAttachment", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }

            return Json(results);
        }
        /// <summary>
        /// NotifyACCID method change the status of investigation as form35 completed
        /// </summary> 
        /// 
        /// <param name="Inv_Id">Receive the investigation file id.</param>
        [HttpPost]
        public JsonResult NotifyACCID(int Inv_Id)
        {
            bool Isent = false;
            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 2)
                {
                    using (gcaa_ismEntities db = new gcaa_ismEntities())
                    {
                        var DList = db.USP_GET_DL(2).ToList();
                        List<string> DListUsers = new List<string>();
                        List<string> DListUsersEmail = new List<string>();
                        foreach (var users in DList)
                        {
                            DListUsersEmail.Add(users.EmailAddress);
                            DListUsers.Add(users.FirstName);
                        }
                        string baseURL = HttpContext.Request.Url.Host;
                        string siteURL = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                        dynamic Inv = dc.USP_VIEW_INVESTIGATION_FORM35_PDF(Inv_Id).FirstOrDefault();
                        dynamic FILES = dc.USP_VIEW_INVESTIGATION_ATTACHMENTFILE(Inv_Id).ToList();
                        dynamic invAircraft = dc.USP_VIEW_INVESTIGATION_AIRCRAFT_DETAIL_FORM35_PDF(Inv_Id).ToList();
                        string sourceFile = pdf.generatePdf(Inv, invAircraft, FILES, "Form35","");
                        Isent = mail.SendMailNotification(2, Convert.ToString(Session["FirstName"]), Convert.ToInt32(Session["roleid"]), siteURL, DListUsers, DListUsersEmail, Inv_Id, sourceFile, Inv.NotificationID, null, null, null);
                    }
                }
                else
                {

                }
            }
            catch (System.Exception ex)
            {
                Isent = false;

                comm.Exception_Log("DIController", "NotifyACCID", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }
            return Json(Isent);
        }
        /// <summary>
        /// Remove Aircraft Details from database table based on aircraft detailed id
        /// </summary> 
        /// 
        [HttpPost]
        public JsonResult RemoveAircraftDetails(int air_id)
        {
            string results = string.Empty;

            try
            {
                var result = 0;

                if (Convert.ToInt32(Session["RoleId"]) == 2)
                {
                    if (Convert.ToInt32(air_id) > 0)
                    {
                        if (Convert.ToInt32(Session["UserId"]) > 0)
                        {
                            result = dc.USP_UPDT_INVESTIGATION_NOOFAIRCRAFT(air_id);

                            result = dc.USP_REMOVE_INVESTIGATION_AIRCRAFT_DETAIL(air_id);

                            dc.SaveChanges();

                            results = "success";
                        }
                    }
                }
                else
                {

                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("DIController", "RemoveAircraftDetails", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }

            return Json(results);
        }
        /// <summary>
        /// Add aircraft registration view the aircraft information
        /// </summary> 
        /// 
        /// <param name="AircraftRegistrationID">Receive the investigation aircraft registration id.</param>
        public ActionResult AddAircraftRegistration(int AircraftRegistrationID = 0)
        {
            AircraftRegistration _airReg = new AircraftRegistration();

            try
            {
                var StateOfOperator_id = dc.USP_VIEW_ALL_OPERATORSTATE();

                SelectList StateOf_Operator = new SelectList(StateOfOperator_id.ToList(), "StateofOperatorID", "StateofOperator");

                ViewBag.State_Of_Operator = StateOf_Operator;

                var StateOfRegistry_id = dc.USP_VIEW_ALL_STATEOFREGISTRY();

                SelectList StateOf_Registry = new SelectList(StateOfRegistry_id.ToList(), "StateofRegistryID", "StateofRegistry");

                ViewBag.State_Of_Registry = StateOf_Registry;

                var Registrationinfo = dc.USP_VIEW_AIRCRAFTREGISTRATION_REG_ID(AircraftRegistrationID).FirstOrDefault();

                if (Registrationinfo != null)
                {
                    _airReg.LKAircraftRegistrationName = Registrationinfo.LKAircraftRegistrationName;

                    _airReg.EngineInfo = Registrationinfo.EngineInfo;

                    _airReg.LKAircraftCategoryID = Registrationinfo.LKAircraftCategoryID;

                    _airReg.LKAircraftModelID = Registrationinfo.LKAircraftModelID;

                    _airReg.LKAircraftRegistrationDescription = Registrationinfo.LKAircraftRegistrationDescription;

                    _airReg.LKAircraftRegistrationID = Registrationinfo.LKAircraftRegistrationID;

                    _airReg.LKAirlineOperatorID = Registrationinfo.LKAirlineOperatorID;

                    _airReg.MaximumMass = Registrationinfo.MaximumMass;

                    _airReg.MSN = Registrationinfo.MSN;

                    _airReg.OperatorContacts = Registrationinfo.OperatorContacts;

                    _airReg.StateofOperatorID = Registrationinfo.StateofOperatorID;

                    _airReg.StateofRegistryID = Registrationinfo.StateofRegistryID;

                    _airReg.LKAircraftCategoryName = Convert.ToString(Registrationinfo.LKAircraftCategoryName);

                    _airReg.LKAircraftModelName = Convert.ToString(Registrationinfo.LKAircraftModelName);

                    _airReg.LKAirlineOperatorName = Convert.ToString(Registrationinfo.LKAirlineOperatorName);
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("DIController", "AddAircraftRegistration(int AircraftRegistrationID = 0)", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }

            return View(_airReg);
        }
        /// <summary>
        /// Save and update all the details of the aircraft before the mandatory field validation and existence validation
        /// </summary> 
        /// 
        /// <param name="airReg">Receive the aircraft registration details.</param>
        [HttpPost]
        public JsonResult AddAircraftRegistrations(AircraftRegistration airReg)
        {
            LKAircraftRegistration _airReg = new LKAircraftRegistration();

            var DbTransaction = dc.Database.BeginTransaction();

            try
            {
                var CheckAirRegName = dc.LKAircraftRegistrations.AsEnumerable().Where(a => a.LKAirlineOperatorID == Convert.ToInt32(airReg.LKAirlineOperatorID) &&

                                         a.LKAircraftRegistrationName == Convert.ToString(airReg.LKAircraftRegistrationName)

                                          && a.LKAircraftRegistrationID != airReg.LKAircraftRegistrationID).FirstOrDefault(); // Check Aircraft name both save and update 

                if (CheckAirRegName == null)
                {
                    if (Convert.ToInt32(airReg.LKAirlineOperatorID) == 0 && airReg.LKAirlineOperatorName != string.Empty)
                    {
                        LKAirlineOperator airOper = new LKAirlineOperator();

                        airOper.LKAirlineOperatorName = Convert.ToString(airReg.LKAirlineOperatorName);

                        airOper.LKAirlineOperatorDescription = string.Empty;

                        airOper.DateCreated = DateTime.Now;

                        var result = dc.LKAirlineOperators.Add(airOper);

                        dc.SaveChanges();

                        _airReg.LKAirlineOperatorID = Convert.ToInt32(airOper.LKAirlineOperatorID);

                        airReg.LKAirlineOperatorID = Convert.ToInt32(airOper.LKAirlineOperatorID);
                    }
                    else
                    {
                        _airReg.LKAirlineOperatorID = Convert.ToInt32(airReg.LKAirlineOperatorID);
                    }

                    if (Convert.ToInt32(airReg.LKAircraftModelID) == 0 && airReg.LKAircraftModelName != string.Empty && airReg.LKAircraftModelName != null)
                    {
                        LKAircraftModel airMdl = new LKAircraftModel();

                        airMdl.LKAircraftMakeID = 0;

                        airMdl.LKAircraftModelName = Convert.ToString(airReg.LKAircraftModelName);

                        airMdl.LKAircraftModelDescription = string.Empty;

                        airMdl.DateCreated = DateTime.Now;

                        var result = dc.LKAircraftModels.Add(airMdl);

                        int Id = dc.SaveChanges();

                        airReg.LKAircraftModelID = Convert.ToInt32(airMdl.LKAircraftModelID);

                        _airReg.LKAircraftModelID = Convert.ToInt32(airMdl.LKAircraftModelID);
                    }
                    else
                    {
                        _airReg.LKAircraftModelID = Convert.ToInt32(airReg.LKAircraftModelID);
                    }

                    if (Convert.ToInt32(airReg.LKAircraftCategoryID) == 0 && airReg.LKAircraftCategoryName != string.Empty && airReg.LKAircraftCategoryName != null)
                    {
                        LKAircraftCategory airCategory = new LKAircraftCategory();

                        airCategory.LKAircraftCategoryName = Convert.ToString(airReg.LKAircraftCategoryName);

                        airCategory.LKAircraftCategoryDescription = string.Empty;

                        airCategory.DateCreated = DateTime.Now;

                        var result = dc.LKAircraftCategories.Add(airCategory);

                        dc.SaveChanges();

                        _airReg.LKAircraftCategoryID = Convert.ToInt32(airCategory.LKAircraftCategoryId);

                        airReg.LKAircraftCategoryID = Convert.ToInt32(airCategory.LKAircraftCategoryId);
                    }
                    else
                    {
                        _airReg.LKAircraftCategoryID = Convert.ToInt32(airReg.LKAircraftCategoryID);
                    }

                    if (airReg.LKAircraftRegistrationID > 0)  // update
                    {
                        var result = dc.USP_UPDT_AIRCRAFT_REGISTRATION(airReg.LKAircraftRegistrationID, airReg.LKAircraftRegistrationName, airReg.LKAircraftRegistrationDescription,

                                     airReg.LKAirlineOperatorID, airReg.LKAircraftModelID, airReg.EngineInfo, airReg.MaximumMass, airReg.LKAircraftCategoryID, airReg.StateofRegistryID,

                                     airReg.StateofOperatorID, airReg.OperatorContacts, airReg.MSN, Convert.ToInt32(Session["UserId"]));
                        DbTransaction.Commit();

                        return Json(airReg.LKAircraftRegistrationID);
                    }
                    else                                     // Insert
                    {
                        _airReg.LKAircraftRegistrationID = airReg.LKAircraftRegistrationID;

                        _airReg.DateCreated = DateTime.Now; _airReg.DateUpdated = DateTime.Now;

                        _airReg.CreatedBy = Convert.ToInt32(Session["UserId"]); _airReg.UpdatedBy = 0;

                        _airReg.LKAircraftRegistrationDescription = (airReg.LKAircraftRegistrationDescription == null) ? string.Empty : airReg.LKAircraftRegistrationDescription;

                        _airReg.LKAircraftRegistrationName = Convert.ToString(airReg.LKAircraftRegistrationName);

                        _airReg.MaximumMass = (airReg.MaximumMass == null) ? string.Empty : airReg.MaximumMass;

                        _airReg.MSN = (airReg.MSN == null) ? string.Empty : airReg.MSN;

                        _airReg.StateofOperatorID = airReg.StateofOperatorID;

                        _airReg.StateofRegistryID = airReg.StateofRegistryID;

                        _airReg.OperatorContacts = (airReg.OperatorContacts == null) ? string.Empty : airReg.OperatorContacts;

                        _airReg.EngineInfo = (airReg.EngineInfo == null) ? string.Empty : airReg.EngineInfo;

                        dc.LKAircraftRegistrations.Add(_airReg);

                        dc.SaveChanges();

                    }

                    DbTransaction.Commit();

                    return Json(_airReg.LKAircraftRegistrationID);
                }
                else
                {
                    return Json(0);
                }
            }
            catch (System.Exception ex)
            {
                DbTransaction.Rollback();

                comm.Exception_Log("DIController", " AddAircraftRegistration(LKAircraftRegistration airReg)", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }
            return Json(0);

        }
        /// <summary>
        /// 
        /// </summary> 
        /// 
        [HttpGet]
        public ActionResult AddAirOperatorName()
        {
            LKAirlineOperator _airoepr = new LKAirlineOperator();

            try
            {

            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("DIController", " AddAirOperatorName", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }

            return View();
        }
        /// <summary>
        /// Save all the details of the Operator Name before the mandatory field validation
        /// </summary> 
        /// 
        /// <param name="airoepr">Receive the aircraft operator details.</param>
        [HttpPost]
        public JsonResult AddAirOperatorName(LKAirlineOperator airoepr)
        {
            LKAirlineOperator _airoepr = new LKAirlineOperator();

            var DbTransaction = dc.Database.BeginTransaction();

            try
            {
                var CheckOperName = dc.LKAirlineOperators.AsEnumerable().Where(a => a.LKAirlineOperatorName == Convert.ToString(airoepr.LKAirlineOperatorName)).FirstOrDefault();

                if (CheckOperName == null)
                {
                    _airoepr.DateCreated = DateTime.Now;

                    _airoepr.LKAirlineOperatorDescription = (airoepr.LKAirlineOperatorDescription == null) ? string.Empty : airoepr.LKAirlineOperatorDescription;

                    _airoepr.LKAirlineOperatorName = airoepr.LKAirlineOperatorName;

                    dc.LKAirlineOperators.Add(_airoepr);

                    dc.SaveChanges();

                    DbTransaction.Commit();
                }
                else
                {
                    return Json(0);
                }
            }
            catch (System.Exception ex)
            {
                DbTransaction.Rollback();

                comm.Exception_Log("DIController", " AddAirOperatorName(LKAirlineOperator airoepr)", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }

            return Json(1);
        }
        /// <summary>
        /// Open Incident Type popup to update Incident Type status
        /// </summary> 
        /// 
        /// <param name="Incidentid">Receive the aircraft incident type id</param>
        /// <param name="InvId">Receive the investigation file id</param>       
        public ActionResult ChangeIncidentType(int Incidentid = 0,int InvId=0)
        {
            InvestigationModel inv = new InvestigationModel();

            try
            {
                var Incident_Type = dc.USP_GET_INCIDENTTYPE().ToList();

                ViewBag.Incident_TypeList = Incident_Type;

                ViewBag.Incident_ID = Incidentid;

                inv.LKIncidentTypeID = Incidentid;

                var otherclassinfo = dc.InvestigationFileDetails.Where(a => a.InvestigationFileID == InvId).ToList().FirstOrDefault();

                inv.OtherOccurClassification = otherclassinfo.OtherOccurClassification;
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("DIController", " AddAirOperatorName", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }

            return View(inv);
        }
        /// <summary>
        /// Validate incident type in aircraft details
        /// </summary> 
        /// 
        /// <param name="LKIncidentTypeID">Receive the aircraft incident type id.</param>
        /// <param name="InvestigationFileID">Receive the investigation file id.</param>
        /// <param name="OtherOccType">Receive the occurrnce type id</param>
        [HttpPost]
        public JsonResult ChangeIncidentTypeSave(int LKIncidentTypeID, int InvestigationFileID,string OtherOccType)
        {
            if (LKIncidentTypeID > 0)
            {
                var result = dc.USP_UPDT_INVESTICATION_INCIDENTTYPE_INVESTIGATION_ID(InvestigationFileID, LKIncidentTypeID, Convert.ToInt32(Session["UserId"]), OtherOccType);

                return Json(1);
            }

            return Json(0);
        }
        /// <summary>
        ///  To update file attachment of form35 file saved to sharepoint and sharepoint file path saved in database
        /// </summary> 
        /// 
        [HttpPost]
        public JsonResult UploadFile()
        {
            try
            {
                if (OccurrenceId > 0)
                {
                    comm.CreateIfMissing(Server.MapPath("~/") + "App_Data/Form35_Files/" + OccurrenceId);

                    foreach (string file in Request.Files)
                    {
                        var fileContent = Request.Files[file];

                        if (fileContent != null && fileContent.ContentLength > 0)
                        {
                            var stream = fileContent.InputStream;

                            var fileName = Path.GetFileName(file);

                            var path = Path.Combine(Server.MapPath("~/App_Data/Form35_Files/" + OccurrenceId), fileContent.FileName);

                            using (var fileStream = System.IO.File.Create(path))
                            {
                                stream.CopyTo(fileStream);
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("DIController", "UploadFile", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return Json("Upload failed");
            }

            return Json("File uploaded successfully");
        }
        /// <summary>
        /// remove attached file in form35 view page 
        /// </summary> 
        /// 
        /// <param name="filename">Receive the attachment file name.</param>
        [HttpPost]
        public JsonResult RemoveAttachedFile(string filename)
        {
            string result = "failed";

            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 2)
                {
                    if (Convert.ToInt32(Session["UserId"]) > 0)
                    {
                        string[] fName = filename.Split('\\');

                        if (fName.Count() >= 2)
                        {
                            var fullpathoffile = Server.MapPath("~/App_Data/Form35_Files/" + OccurrenceId + "/" + fName[2]);

                            if (System.IO.File.Exists(fullpathoffile))
                            {
                                System.IO.File.Delete(fullpathoffile);
                            }

                            result = "success";
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("DIController", "RemoveAttachedFile", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }

            return Json(result);
        }
        /// <summary>
        /// Delete Notification by Occurrence Id
        /// </summary>
        /// <param name="id">Receive the input of Occurrence Id</param>
        /// <returns></returns>
        public ActionResult DeleteNotification(int id)
        {
            try
            {
                var result = dc.USP_DELETE_OCCURRENCE(id);
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("DIController", " DeleteNotification", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }

            return RedirectToAction("DIDashboard", "Dashboard");
        }
    }
}
