﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ISM_Project.Models
{
    public class InvestigationModel
    {
        public int InvestigationFileID { get; set; }
        public string InvestigationFileNumber { get; set; }
        public int OccurrenceID { get; set; }
        public string NotificationID { get; set; }
        public int LKInvestigationStatusTypeID { get; set; }
        public System.DateTime DateCreated { get; set; }
        public Nullable<System.DateTime> DateUpdated { get; set; }
        public int CreatedBy { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public string DIName { get; set; }
        public string Investigation_Process { get; set; }
        public string Interim_statement { get; set; }
        public string Ongoing_Investigation_Activitiea { get; set; }

        public int InvestigationFileDetailID { get; set; }
        public Nullable<int> LKIncidentTypeID { get; set; }
        public string NotifierName { get; set; }
        public string NotifierPosition { get; set; }
        public string NotifierOrganization { get; set; }
        public string NotifierContactNumber { get; set; }
        public Nullable<System.DateTime> NotifierCallDate { get; set; }
        public Nullable<System.DateTime> LocalDateAndTimeOfOccurrence { get; set; }
        public Nullable<System.DateTime> UTCDateAndTimeOfOccurrence { get; set; }
        public Nullable<int> LKCountryID { get; set; }
        public string PlaceOfIncident { get; set; }
        //public string FlightNumber { get; set; }
        //public Nullable<decimal> PlaceOfIncidentLatitude { get; set; }
        //public Nullable<decimal> PlaceOfIncidentLongitude { get; set; }
        //public Nullable<int> LKDepartureAirportID { get; set; }
        //public Nullable<int> LKDestinationAirportID { get; set; }
        public string EventDescription { get; set; }
        public Nullable<int> PassengersOnBoardInfo { get; set; }
        public Nullable<int> CrewOnBoardInfo { get; set; }
        public Nullable<int> PassengersKilledInfo { get; set; }
        public Nullable<int> CrewsKilledInfo { get; set; }
        public Nullable<int> OthersKilledInfo { get; set; }
        public Nullable<int> PassengersWithFatalInjuryInfo { get; set; }
        public Nullable<int> CrewsWithFatalInjuryInfo { get; set; }
        public Nullable<int> OthersWithFatalInjuryInfo { get; set; }
        public Nullable<int> PassengersWithSeriousInjuryInfo { get; set; }
        public Nullable<int> CrewsWithSeriousInjuryInfo { get; set; }
        public Nullable<int> OthersWithSeriousInjuryInfo { get; set; }
        public Nullable<int> PassengersWithMinorInjuryInfo { get; set; }
        public Nullable<int> CrewsWithMinorInjuryInfo { get; set; }
        public Nullable<int> OthersWithMinorInjuryInfo { get; set; }
        public Nullable<int> CrewsTotalInjury { get; set; }
        public Nullable<int> PassengersTotalInjury { get; set; }
        public Nullable<int> OthersTotalInjury { get; set; }
        //public Nullable<int> LKAircraftMakeID { get; set; }
        //public Nullable<int> AircraftModelID { get; set; }
        //public Nullable<int> LKAircraftTypeID { get; set; }
        public string RegistrationMarkingsOnAircraft { get; set; }
        //public string AircraftEngineInfo { get; set; }
        public Nullable<int> LKAircraftCategoryId { get; set; }
        public string AircraftMaximumMass { get; set; }
        public string NameOfRegisteredOwner { get; set; }
        //public Nullable<int> LKAirlineOperatorID { get; set; }
        public string LKAirlineOperatorOther { get; set; }
        public string OperatorState { get; set; }
        public string OperatorContactNumber { get; set; }
        public string QualificationOfPilot { get; set; }
        public string PilotCertificateNumber { get; set; }
        public string DutyInvestigatorComments { get; set; }
        public string DutyInvestigatorSuggestions { get; set; }
        public string DutyInvestigatorFurtherSuggestion { get; set; }
        public string InvestigationType { get; set; }

        public int InvestigationFileAssessmentID { get; set; }
        public Nullable<int> AccidentOrSeriousIncident { get; set; }
        public Nullable<int> LargeOrSmallAircraft { get; set; }
        public Nullable<int> CommercialOrPrivate { get; set; }
        public Nullable<short> UAERegisteredAircraft { get; set; }
        public Nullable<short> UAEOperator { get; set; }
        public Nullable<short> TrendOnOperatorOrAircraftType { get; set; }
        public Nullable<short> AnticipatedSafetyValueOfInvestigation { get; set; }
        public Nullable<short> PotentialImpactOnPublicConfidence { get; set; }
        public Nullable<short> HaveRelevanceToSafetyProgram { get; set; }
        public Nullable<short> ResourceAvailability { get; set; }
        public Nullable<short> OppurtunityForOtherInvestigationAuthorities { get; set; }
        public string TimeBetweenOccurrenceAndNotification { get; set; }
        public string HoursBetweenOccurrenceAndNotification { get; set; }
        public Nullable<short> TrainingBenefitsToAAISInvestigators { get; set; }

        public Nullable<int> LKOccurrenceStatusTypeId { get; set; }

        public int InvTabIndex { get; set; }

        public Nullable<int> NoOfAircraftInvolved { get; set; }
        public string[] FlightNumber { get; set; }
        public int[] LKDestinationAirportID { get; set; }
        public int[] LKDepartureAirportID { get; set; }
        public string[] LongLat { get; set; }

        public int[] PassengerOnboard { get; set; }
        public int[] CrewOnboard { get; set; }
        public int[] CrewsWithFatalInjury { get; set; }
        public int[] PassengersWithFatalInjury { get; set; }
        public int[] OthersWithFatalInjury { get; set; }
        public int[] CrewsWithSeriousInjury { get; set; }
        public int[] PassengersWithSeriousInjury { get; set; }
        public int[] OthersWithSeriousInjury { get; set; }
        public int[] CrewsWithMinorInjury { get; set; }
        public int[] PassengersWithMinorInjury { get; set; }
        public int[] OthersWithMinorInjury { get; set; }
        public int[] CrewsKilled { get; set; }
        public int[] PassengersKilled { get; set; }
        public int[] OthersKilled { get; set; }
        public int[] LKAircraftModelID { get; set; }
        public int[] LKAircraftRegistrationID { get; set; }
        public string[] EngineInfo { get; set; }
        public string[] MaximumMass { get; set; }
        public int[] AircraftCategoryId { get; set; }
        public string[] StateofRegistry { get; set; }
        public int[] LKAirlineOperatorID { get; set; }
        public string[] stateofoperatorid { get; set; }
        public string[] OperatorContacts { get; set; }
        public string[] msn { get; set; }
        public string[] CabinCrew { get; set; }

        public string InvestigationClosureReason { get; set; }
        public string InvestigationClosureRecommendedBy { get; set; }
        public string InvestigationClosureRecommendedDate { get; set; }
        public int InvestigationClosureStatusId { get; set; }
        public int InvestigationClosureDetailID { get; set; }

        public IEnumerable<USP_VIEW_OCCURRENCE_FOR_DI_Result> InvOccurrence { get; set; }

        public string SearchText { get; set; }


        public string PrevailingWeatherConditions { get; set; }
        public string ExtendOfDamageToAircraft { get; set; }
        public string DamagesToGroundObjects { get; set; }
        public string DescriptionOfExplosivesAndDangerousArticles { get; set; }

        public HttpPostedFileBase[] InvestigationFiles { get; set; }

        public Nullable<short> UAERegisteredOperator { get; set; }

        public Nullable<int> CabincrewWithFatalInjuryInfo { get; set; }
        public Nullable<int> CabincrewWithSeriousInjuryInfo { get; set; }
        public Nullable<int> CabincrewWithMinorInjuryInfo { get; set; }
        public Nullable<int> CabincrewsWithotherInjuryInfo { get; set; }
        public Nullable<int> CabincrewsWithTotalInjuryInfo { get; set; }
        public Nullable<int> OthercrewWithFatalInjuryInfo { get; set; }
        public Nullable<int> OthercrewWithSeriousInjuryInfo { get; set; }
        public Nullable<int> OthercrewWithMinorInjuryInfo { get; set; }
        public Nullable<int> OthercrewsWithotherInjuryInfo { get; set; }
        public Nullable<int> OthercrewsWithTotalInjuryInfo { get; set; }
        public Nullable<int> TotalOnBoardInfo { get; set; }
        public string OtherOccurClassification { get; set; }
        public string NameOfForeignInvestigator { get; set; }
        public Nullable<int> OccurrenceCategorizationId { get; set; }
        public string InvestigationReopenReason { get; set; }
    }
}