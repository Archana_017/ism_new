﻿
function GetDynamicTextBox(count) {
    $("#DynamicArea").html("");

    //@*var dynDiv = '<div class="form-group">' +
    //    '<div class="row" >' +
    //    '<div class="col-md-3" > <small>Certification Name</small>' +
    //    ' @Html.TextBoxFor(m => m.Certificates[count].CertificateName, new { @placeholder = "Enter Certificate Name", required = "required", @type = "text", @class = "form-control" })' +
    //    '</div>' +
    //    '<div class="col-md-3">' +
    //    '<small>Certified Date</small>' +
    //    '@Html.TextBoxFor(m => m.Certificates[count].CertifiedDate, "{0:yyyy-MM-dd}", new { @placeholder = "Enter Certificate Date", required = "required", @type = "date", @class = "form-control" })' +
    //    '</div> ' +
    //    '<div class="col-md-3">' +
    //    '<small>Date of Renewal</small>' +
    //    '@Html.TextBoxFor(m => m.Certificates[count].DateofRenewal, "{0:yyyy-MM-dd}", new { @placeholder = "Enter Date of Renewal", required = "required", @type = "date", @class = "form-control" })' +
    //    '</div>' +
    //    '<div class="col-md-2">' +
    //    '<small>validity period</small>' +
    //    '@Html.TextBoxFor(m => m.Certificates[count].ValidityDays, new { required = "required", @type = "text", @class = "form-control" })' +
    //    '</div>' +
    //    '<div class="col-md-1">' +
    //    '<br />' +
    //    '<button type="button" class="btn btn-danger btn-number" onclick="RemoveTextBox(this)" data-type="minus">' +
    //    '<span class="glyphicon glyphicon-minus"></span>' +
    //    '</button>' +
    //    '</div>' +
    //    '</div>' +
    //    '</div>';*@
            var dynDiv = "";
    for (var i = 0; i < count; i++) {
        if (certiArray) {
            if (i < certiArray.length) {

                dynDiv += '<div class="form-group"><div class="row"><div class="col-md-3"> <small>Certification Name</small> <input class="form-control" id="Certificates_' + i + '__CertificateName" name="Certificates[' + i + '].CertificateName" placeholder="Enter Certificate Name" required="required" type="text" value=' + certiArray[i].CertificateName + '></div><div class="col-md-3"><small>Certified Date</small><input class="form-control" data-val="true" data-val-date="The field CertifiedDate must be a date." data-val-required="The CertifiedDate field is required." id="Certificates_' + i + '__CertifiedDate" name="Certificates[' + i + '].CertifiedDate" placeholder="Enter Certificate Date" required="required" type="date" value="' + parseJsonDate(certiArray[i].CertifiedDate) + '" onkeydown="return false"></div> <div class="col-md-3"><small>Date of Renewal</small><input class="form-control" data-val="true" data-val-date="The field DateofRenewal must be a date." data-val-required="The DateofRenewal field is required." id="Certificates_' + i + '__DateofRenewal" name="Certificates[' + i + '].DateofRenewal" placeholder="Enter Date of Renewal" required="required" type="date" value="' + parseJsonDate(certiArray[i].DateofRenewal) + '" onkeydown="return false" onchange="CalculateValidityPeriod(' + i + ');"></div><div class="col-md-2"><small>validity period</small><input class="form-control" data-val="true" data-val-number="The field ValidityDays must be a number." data-val-required="The ValidityDays field is required." id="Certificates_' + i + '__ValidityDays" name="Certificates[' + i + '].ValidityDays" required="required" type="text" value="' + CalculateValidityPeriodByValue(parseJsonDate(certiArray[i].DateofRenewal)) + '" readonly></div><div class="col-md-1"><br><button type="button" class="btn btn-danger btn-number" onclick="RemoveTextBox(this,' + i + ')" data-type="minus"><span class="glyphicon glyphicon-minus"></span></button></div></div></div>';
            }
            else {

                dynDiv += '<div class="form-group"><div class="row"><div class="col-md-3"> <small>Certification Name</small> <input class="form-control" id="Certificates_' + i + '__CertificateName" name="Certificates[' + i + '].CertificateName" placeholder="Enter Certificate Name" required="required" type="text" value=""></div><div class="col-md-3"><small>Certified Date</small><input class="form-control" data-val="true" data-val-date="The field CertifiedDate must be a date." data-val-required="The CertifiedDate field is required." id="Certificates_' + i + '__CertifiedDate" name="Certificates[' + i + '].CertifiedDate" placeholder="Enter Certificate Date" required="required" type="date" value=""></div> <div class="col-md-3"><small>Date of Renewal</small><input class="form-control" data-val="true" data-val-date="The field DateofRenewal must be a date." data-val-required="The DateofRenewal field is required." id="Certificates_' + i + '__DateofRenewal" name="Certificates[' + i + '].DateofRenewal" placeholder="Enter Date of Renewal" required="required" type="date" value="" onkeydown="return false" onchange="CalculateValidityPeriod(' + i + ');"></div><div class="col-md-2"><small>validity period</small><input class="form-control" data-val="true" data-val-number="The field ValidityDays must be a number." data-val-required="The ValidityDays field is required." id="Certificates_' + i + '__ValidityDays" name="Certificates[' + i + '].ValidityDays" required="required" type="text" value="" readonly></div><div class="col-md-1"><br><button type="button" class="btn btn-danger btn-number" onclick="RemoveTextBox(this,' + i + ')" data-type="minus"><span class="glyphicon glyphicon-minus"></span></button></div></div></div>';

            }
        }
        else {

            dynDiv += '<div class="form-group"><div class="row"><div class="col-md-3"> <small>Certification Name</small> <input class="form-control" id="Certificates_' + i + '__CertificateName" name="Certificates[' + i + '].CertificateName" placeholder="Enter Certificate Name" required="required" type="text" value=""></div><div class="col-md-3"><small>Certified Date</small><input class="form-control" data-val="true" data-val-date="The field CertifiedDate must be a date." data-val-required="The CertifiedDate field is required." id="Certificates_' + i + '__CertifiedDate" name="Certificates[' + i + '].CertifiedDate" placeholder="Enter Certificate Date" required="required" type="date" value=""></div> <div class="col-md-3"><small>Date of Renewal</small><input class="form-control" data-val="true" data-val-date="The field DateofRenewal must be a date." data-val-required="The DateofRenewal field is required." id="Certificates_' + i + '__DateofRenewal" name="Certificates[' + i + '].DateofRenewal" placeholder="Enter Date of Renewal" required="required" type="date" value="" onkeydown="return false" onchange="CalculateValidityPeriod(' + i + ');"></div><div class="col-md-2"><small>validity period</small><input class="form-control" data-val="true" data-val-number="The field ValidityDays must be a number." data-val-required="The ValidityDays field is required." id="Certificates_' + i + '__ValidityDays" name="Certificates[' + i + '].ValidityDays" required="required" type="text" value="" readonly></div><div class="col-md-1"><br><button type="button" class="btn btn-danger btn-number" onclick="RemoveTextBox(this,' + i + ')" data-type="minus"><span class="glyphicon glyphicon-minus"></span></button></div></div></div>';

        }


    }
    return dynDiv;
}
function AddTextBox() {
    saveCertificateArray(-1);
    var count = $("#DynamicArea").children().length + 1;
    var div = GetDynamicTextBox(count);
    $("#DynamicArea").append(div);
}

function RemoveTextBox(button, index) {
    saveCertificateArray(index);
    //$(button).parent().remove();
    var count = $("#DynamicArea").children().length - 1;
    $(button).closest('.form-group').remove();
    var div = GetDynamicTextBox(count);
    $("#DynamicArea").append(div);
}
function saveCertificateArray(index) {
    var tempArry = [];
    var v1, v2, v3, v4;
    for (var i = 0; i < $("#DynamicArea").children().length; i++) {
        if (index != i || index == -1) {
            v1 = $("#Certificates_" + i + "__CertificateName").val();
            v2 = $("#Certificates_" + i + "__CertifiedDate").val();
            v3 = $("#Certificates_" + i + "__DateofRenewal").val();
            v4 = $("#Certificates_" + i + "__ValidityDays");
            tempArry.push({ "CertificateName": v1, "CertifiedDate": v2, "DateofRenewal": v3, "ValidityDays": v4 });
        }

    }
    certiArray = tempArry;

}
function parseJsonDate(jsonDateString) {
    var dateStr = jsonDateString;
    if (jsonDateString.toString().startsWith("/Date")) {
        var date = new Date(Number(jsonDateString.match(/\d+/)[0]));
        var day = date.getDate();
        day = day = (day < 10) ? ("0" + day) : day;
        var month = date.getMonth() + 1;
        month = (month < 10) ? ("0" + month) : month;
        dateStr = date.getFullYear() + "-" + month + "-" + day;
    }
    return dateStr;
}
function CalculateValidityPeriod(index) {
    var date1 = $("#Certificates_" + index + "__DateofRenewal").val();
    $("#Certificates_" + index + "__ValidityDays").val(calDays(date1));
}
function CalculateValidityPeriodByValue(value) {
    if (value) {
        return calDays(value);
    }
    return "";
}
function calDays(d) {
    var date1 = new Date(d);
    var date2 = new Date();
    var timeDiff = date1.getTime() - date2.getTime();
    var diffDays = Math.round(timeDiff / (1000 * 3600 * 24));
    var rst = "";
    if (diffDays < 0) {
        rst = "Expired";
    }
    else if (diffDays == 0) {
        rst = "Expire Today";
    }
    else if (diffDays == 1) {
        rst = diffDays + " day";
    }
    else {
        rst = diffDays + " days";
    }
    return rst;
}
