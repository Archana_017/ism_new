//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ISM_Project.Models
{
    using System;
    
    public partial class USP_VIEW_TEAMMEMBER_FOR_REMOVAL_Result
    {
        public int InvestigationFileID { get; set; }
        public Nullable<int> TeamMemberID { get; set; }
        public int InvestigationTaskGroupEventID { get; set; }
    }
}
