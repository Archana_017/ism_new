﻿using ISM_Project.Models;
using Microsoft.SharePoint.Client;
using Microsoft.SharePoint.Client.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Security;
using System.Web;

namespace ISM_Project
{
    /// <summary>
    /// Mail Configuration class contains the entire application email notification process
    /// </summary> 
    /// 
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1001:TypesThatOwnDisposableFieldsShouldBeDisposable")]
    public class MailConfiguration
    {
        gcaa_ismEntities db = new gcaa_ismEntities();
        Common comm = new Common();
        /// <summary>
        /// Send Mail Notification function send a notification email without an attachment 
        /// </summary> 
        /// <param name="templateID">Receive mail template id</param> 
        /// <param name="fromUser">Receive mail from id</param> 
        /// <param name="fromUserDesig">Receive mail from user designation</param> 
        /// <param name="RedirectUrl">Receive Redirect Url</param>
        /// <param name="ToUser">Receive to user mail from id</param> 
        /// <param name="ToUserMail">Receive to user mail address</param> 
        /// <param name="ID">Receive investigation id</param> 
        /// <param name="filePath">Receive attachment file id</param>
        /// <param name="refNo">Receive file refno</param> 
        /// <param name="refno1">Receive file refno </param> 
        /// <param name="status">Receive ststus of mail</param> 
        /// <param name="Feedback">Receive any feedback as string</param>
        public bool SendMailNotification(int templateID, string fromUser, int fromUserDesig, string RedirectUrl, List<string> ToUser, List<string> ToUserMail, int ID, string filePath, string refNo, string refno1, string status, string Feedback)
        {
            bool Isent = false;
            try  
            {
                string[] SMTPData = comm.getMailValues();
                //var fromemail = new MailAddress("postmaster@x-minds.org");
                //var fromemailpassword = "7201477db2faa00fe632a46d804a650e";
                MailMessage mail = new MailMessage();
                string[] mailcontents = getMailTemplate(templateID, fromUser, RedirectUrl, fromUserDesig, ID, refNo, refno1, status, filePath, null);
                string subject = mailcontents[0];
                string body = mailcontents[1];
                if (!string.IsNullOrEmpty(subject) && !string.IsNullOrEmpty(body) && !string.IsNullOrEmpty(SMTPData[0]))
                {
                    if (ToUserMail != null)
                    {
                        foreach (var usermail in ToUserMail)
                        {
                            mail.To.Add(usermail);
                        }
                        SmtpClient client = new SmtpClient();

                        mail.From = new MailAddress(SMTPData[4]);
                        if (refno1 != null)
                        {
                            try {
                                mail.From = new MailAddress(refno1);
                            } catch (System.Exception e)
                            {
                                mail.From = new MailAddress(SMTPData[4]);
                            }
                        }
                        client.Host = SMTPData[0];
                        client.DeliveryMethod = SmtpDeliveryMethod.Network;
                        client.UseDefaultCredentials = false;

                        if (SMTPData[1] != "0")
                        {
                            client.Port = Convert.ToInt32(SMTPData[1]);
                            client.EnableSsl = true;
                            client.Credentials = new NetworkCredential(new MailAddress(SMTPData[2]).Address, SMTPData[3]);
                        }

                        //client.Credentials = new NetworkCredential(fromemail.Address, fromemailpassword);

                        mail.Subject = subject;
                        mail.IsBodyHtml = true;
                        mail.Body = body;

                        if (System.IO.File.Exists(filePath))
                        {
                            System.Net.Mail.Attachment attachment;
                            attachment = new System.Net.Mail.Attachment(filePath);
                            mail.Attachments.Add(attachment);
                        }
                        client.Send(mail);
                        mail.Attachments.Dispose();
                        Isent = true;
                    }
                }
                if (Directory.Exists(HttpContext.Current.Server.MapPath("~/") + "MailAttachments"))
                    Directory.Delete(HttpContext.Current.Server.MapPath("~/") + "MailAttachments", true);
                if (System.IO.File.Exists(filePath))
                {
                    System.IO.File.Delete(filePath);
                }
            }
            catch (System.Exception ex)
            {
                Isent = false;
                comm.Exception_Log("MailConfiguration", "SendMailNotification", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(HttpContext.Current.Session["UserId"]));
            }
            finally
            {

            }
            return Isent;
        }
        /// <summary>
        /// Send Mail Notification function send a notification email with an attachment 
        /// </summary> 
        /// 
        /// <param name="templateID">Receive mail template id</param> 
        /// <param name="fromUser">Receive mail from id</param> 
        /// <param name="fromUserDesig">Receive mail from user designation</param> 
        /// <param name="RedirectUrl">Receive Redirect Url</param>
        /// <param name="ToUser">Receive to user mail from id</param> 
        /// <param name="dicUserMails">Receive distribution list user mail address</param> 
        ///<param name="dicRespMails">Receive distribution list user mail address</param>          
        /// <param name="ID">Receive investigation id</param> 
        /// <param name="filePath">Receive attachment file id</param>
        /// <param name="refNo">Receive file refno</param> 
        /// <param name="refno1">Receive file refno </param> 
        /// <param name="status">Receive ststus of mail</param> 
        /// <param name="Feedback">Receive any feedback as string</param>
        /// <param name="CorresContent">Receive Correspondence Content</param>
        /// <param name="fromMail">Receive from user email address</param>
        public bool SendMailNotificationwithAttach(int templateID, string fromUser, int fromUserDesig, string RedirectUrl, List<string> ToUser, Dictionary<string, List<string>> dicUserMails, Dictionary<string, string> dicRespMails, int ID, string[] filePath, string refNo, string refno1, string status, string Feedback, string CorresContent, string fromMail)
        {
            bool Isent = false;
            try
            {
                string[] SMTPData = comm.getMailValues();
                string[] mailcontents = new string[10];
                string Tomail = string.Empty;
                if (dicUserMails != null)
                {
                    string subject = "";
                    string body = "";
                    List<string> toEmails = new List<string>();
                    if (dicUserMails.Count() > 0)
                    {
                        foreach (var recipients in dicUserMails)
                        {
                            if (recipients.Key.ToLower() == "to")
                            {
                                foreach (var usermail in recipients.Value)
                                {
                                    foreach (var users in dicRespMails)
                                    {
                                        if (users.Key == usermail)
                                        {
                                            mailcontents = getMailTemplate(15, fromUser, users.Value, fromUserDesig, ID, refNo, refno1, status, Feedback, CorresContent);
                                            toEmails.Add(usermail);
                                            Tomail = usermail;
                                        }
                                    }
                                }
                                subject = mailcontents[0];
                                body = mailcontents[1];
                                if (!string.IsNullOrEmpty(subject) && !string.IsNullOrEmpty(body) && !string.IsNullOrEmpty(SMTPData[0]))
                                {
                                    if (dicUserMails != null)
                                    {
                                        MailMessage mail = new MailMessage();
                                        SmtpClient client = new SmtpClient();
                                        mail.From = new MailAddress(fromMail);
                                        toEmails.ForEach(email =>
                                        {
                                            mail.To.Add(email);
                                        });
                                        client.Host = SMTPData[0];
                                        client.DeliveryMethod = SmtpDeliveryMethod.Network;
                                        client.UseDefaultCredentials = false;
                                        if (SMTPData[1] != "0")
                                        {
                                            client.Port = Convert.ToInt32(SMTPData[1]);
                                            client.EnableSsl = true;
                                            client.Credentials = new NetworkCredential(new MailAddress(SMTPData[2]).Address, SMTPData[3]);
                                        }
                                        mail.Subject = subject;
                                        mail.IsBodyHtml = true;
                                        mail.Body = body;
                                        for (int i = 0; i < filePath.Length; i++)
                                        {
                                            if (!string.IsNullOrEmpty(filePath[i]))
                                            {
                                                if (System.IO.File.Exists(filePath[i]))
                                                {
                                                    System.Net.Mail.Attachment attachment;
                                                    attachment = new System.Net.Mail.Attachment(filePath[i]);
                                                    mail.Attachments.Add(attachment);
                                                }
                                            }
                                        }
                                        client.Send(mail);
                                        mail.Attachments.Dispose();
                                        Isent = true;
                                    }
                                }
                            }
                            else
                            {
                                mailcontents = getMailTemplate(7, fromUser, RedirectUrl, fromUserDesig, ID, refNo, refno1, status, Feedback, CorresContent);
                                subject = mailcontents[0];
                                body = mailcontents[1];
                                if (!string.IsNullOrEmpty(subject) && !string.IsNullOrEmpty(body) && !string.IsNullOrEmpty(SMTPData[0]))
                                {
                                    if (dicUserMails != null)
                                    {
                                        MailMessage mail = new MailMessage();
                                        SmtpClient client = new SmtpClient();
                                        mail.From = new MailAddress(fromMail);
                                        foreach (var usermail in recipients.Value)
                                        {
                                            mail.CC.Add(usermail);
                                        }
                                        client.Host = SMTPData[0];
                                        client.DeliveryMethod = SmtpDeliveryMethod.Network;
                                        client.UseDefaultCredentials = false;
                                        if (SMTPData[1] != "0")
                                        {
                                            client.Port = Convert.ToInt32(SMTPData[1]);
                                            client.EnableSsl = true;
                                            client.Credentials = new NetworkCredential(new MailAddress(SMTPData[2]).Address, SMTPData[3]);
                                        }

                                        mail.Subject = subject;
                                        mail.IsBodyHtml = true;
                                        mail.Body = body;
                                        for (int i = 0; i < filePath.Length; i++)
                                        {
                                            if (!string.IsNullOrEmpty(filePath[i]))
                                            {
                                                if (System.IO.File.Exists(filePath[i]))
                                                {
                                                    System.Net.Mail.Attachment attachment;
                                                    attachment = new System.Net.Mail.Attachment(filePath[i]);
                                                    mail.Attachments.Add(attachment);
                                                }
                                            }
                                            else
                                                break;
                                        }
                                        client.Send(mail);
                                        mail.Attachments.Dispose();
                                        Isent = true;
                                    }
                                }
                            }
                        }
                    }
                }
                //for external stakeholders mail in Correspondence with SR - response notification(to single user hence the logic for sending mail is separately written)
                else
                {
                    mailcontents = getMailTemplate(19, fromUser, RedirectUrl, fromUserDesig, ID, refNo, refno1, status, Feedback, CorresContent);
                    string subject = mailcontents[0];
                    string body = mailcontents[1];
                    if (!string.IsNullOrEmpty(subject) && !string.IsNullOrEmpty(body) && !string.IsNullOrEmpty(SMTPData[0]))
                    {
                        if (ToUser != null)
                        {
                            MailMessage mail = new MailMessage();
                            SmtpClient client = new SmtpClient();
                            mail.From = new MailAddress(SMTPData[4]);
                            if (fromMail != null)
                            {
                                try
                                {
                                    mail.From = new MailAddress(fromMail);
                                }
                                catch (System.Exception e)
                                {
                                    mail.From = new MailAddress(SMTPData[4]);
                                }
                            }
                            foreach (var users in ToUser)
                            {
                                mail.To.Add(users);
                            }
                            client.Host = SMTPData[0];
                            client.DeliveryMethod = SmtpDeliveryMethod.Network;
                            client.UseDefaultCredentials = false;

                            if (SMTPData[1] != "0")
                            {
                                client.Port = Convert.ToInt32(SMTPData[1]);
                                client.EnableSsl = true;
                                client.Credentials = new NetworkCredential(new MailAddress(SMTPData[2]).Address, SMTPData[3]);
                            }

                            mail.Subject = subject;
                            mail.IsBodyHtml = true;
                            mail.Body = body;
                            for (int i = 0; i < filePath.Length; i++)
                            {
                                if (!string.IsNullOrEmpty(filePath[i]))
                                {
                                    if (System.IO.File.Exists(filePath[i]))
                                    {
                                        System.Net.Mail.Attachment attachment;
                                        attachment = new System.Net.Mail.Attachment(filePath[i]);
                                        mail.Attachments.Add(attachment);
                                    }
                                }
                                else
                                    break;
                            }
                            client.Send(mail);
                            mail.Attachments.Dispose();
                            Isent = true;
                        }
                    }
                }
                //comm.DeleteDirectory(HttpContext.Current.Server.MapPath("~/") + "CorrespondenceAttachments");
                if (Directory.Exists(HttpContext.Current.Server.MapPath("~/") + "EventsAttachmentReports"))
                    Directory.Delete(HttpContext.Current.Server.MapPath("~/") + "EventsAttachmentReports", true);
                if (Directory.Exists(HttpContext.Current.Server.MapPath("~/") + "MailAttachments"))
                    Directory.Delete(HttpContext.Current.Server.MapPath("~/") + "MailAttachments", true);
                //if (System.IO.File.Exists(filePath))
                //{
                //    System.IO.File.Delete(filePath);
                //}
            }
            catch (System.Exception ex)
            {
                Isent = false;

                comm.Exception_Log("MailConfiguration", "SendMailNotificationwithAttach", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(HttpContext.Current.Session["UserId"]));
            }
            finally
            {

            }
            return Isent;
        }
        /// <summary>
        /// Generate default mail template based on the Id ("Receive  from the corresponding controller action method")
        /// </summary> 
        /// 
        /// <param name="id">Receive mail template id</param> 
        /// <param name="fromUser">Receive mail from id</param> 
        /// <param name="FromUserDesig">Receive mail from user designation</param> 
        /// <param name="redUrl">Receive Redirect Url</param>
        /// <param name="Item_ID">Receive to user mail from id</param> 
        /// <param name="refNo">Receive attachment file id</param>
        /// <param name="refNum1">Receive file refno</param> 
        /// <param name="DAAIFeedback">Receive DAAI Feedback status</param> 
        /// <param name="status">Receive ststus of mail</param> 
        /// <param name="CorresContent">Receive Corresspondence Content</param>
        public string[] getMailTemplate(int id, string fromUser, string redUrl, int FromUserDesig, int Item_ID, string refNo, string refNum1, string status, string DAAIFeedback, string CorresContent)
        {
            string[] MailContent = new string[100];

            try
            {
                if (CorresContent != null && CorresContent.Contains(Environment.NewLine)) {
                    CorresContent = CorresContent.Replace(Environment.NewLine, "<br />");
                }
                string header; string footer; string Content; string body; string subject;
                body = header = footer = Content = subject = string.Empty;
                string refNos = string.Empty;
                var roles = db.USP_GET_ROLES(FromUserDesig).FirstOrDefault();
                string UserDesig = roles.LKUserRoleName;
                /*mail content section*/
                //mail for new occurrence notification
                if (id == 1)
                {
                    //var Occ_Detail = db.USP_VIEW_OCCURRENCE(Item_ID, Convert.ToInt32(HttpContext.Current.Session["UserId"])).FirstOrDefault();
                    //refNo = Occ_Detail.NotificationId;
                    subject = "Occurrence Notification " + refNo;
                    header = "";
                    //header = "<div><table>"
                    //              + "<tr><td><p>You have received a notification on a new occurence</p></td></tr>"
                    //              + "</table></div>";
                    Content = "<div><table>"
                            + "<tr><td>Greetings, </td></tr>"
                            + "<tr><td>This is to inform that a new occurrence is reported.</ td></tr>"
                            + "</table></div>" + Environment.NewLine + Environment.NewLine + DAAIFeedback;
                }
                //mail for form 35 notification
                else if (id == 2)
                {
                    var Inv_Detail = db.USP_VIEW_INVESTIGATION_FORM35(Item_ID).FirstOrDefault();
                    if (Inv_Detail != null)
                    {
                        var Occ_Detail = db.USP_VIEW_OCCURRENCE(Inv_Detail.OccurrenceDetailID, Convert.ToInt32(HttpContext.Current.Session["UserId"])).FirstOrDefault();
                        refNo = Inv_Detail.NotificationID;
                        subject = "Form 035 Submitted - " + refNo;
                        if (Occ_Detail != null)
                        {
                            Content = "<div><table>"
                            + "<tr><td>Greetings, </td></tr>"
                            + "<tr><td>This is to inform that below Form 035 information is updated.</ td></tr>"
                            + "</table></div>";
                        }
                    }
                }
                //mail for IIC assignment Notification
                else if (id == 3)
                {
                    var Inv_Detail = db.USP_VIEW_INVESTIGATION_FORM35(Item_ID).FirstOrDefault();
                    if (Inv_Detail != null)
                    {
                        refNo = Inv_Detail.InvestigationNumber;
                        string Not_No = Inv_Detail.NotificationID;
                        subject = "Assigned as IIC - " + refNo;
                        Content = "<table>"
                               + "<tr><td>Greetings, </td></tr>"
                               + "<tr><td>This is to inform that you have been assigned as an Investigator In-Charge officer for the below investigation.</td></tr>"
                               + "</table>";
                    }
                }
                //mail for team member assignment Notification
                else if (id == 4)
                {
                    var Inv_Detail = db.USP_VIEW_INVESTIGATION_FORM35(Item_ID).FirstOrDefault();
                    if (Inv_Detail != null)
                    {
                        refNo = Inv_Detail.InvestigationNumber;
                        subject = "New Investigation " + refNo;
                        header = "";
                        Content = "<table>"
                              + "<tr><td>You have been selected as a Team member for a new investigation.</td></tr>"
                              + "</table>";

                    }
                }
                //mail for new correspondence sent for DAAI approval
                else if (id == 6)
                {
                    subject = "New Correspondence " + refNo;
                    header = "";
                    Content = "<table>"
                            + "<tr><td>New correspondence have been added for investigation " + refNum1 + ".</td></tr>"
                            + "</table>";
                }
                //mail for new correspondence sent for Intended Recipients
                else if (id == 7)
                {
                    subject = status;
                    header = "";
                    Content = "<table>"

                            + "<tr><td>" + CorresContent + "</td></tr>"
                            + "</table>";
                }
                //external stakeholders notification from IIC
                else if (id == 15)
                {
                    subject = status;
                    header = "";
                    Content = "<table>"
                            + "<tr><td>" + CorresContent + "</td></tr>"
                            + "<tr><td> Please use this <a href=" + redUrl + ">link</a> to send your response.</td></tr>"
                            + "</table>";
                }
                //external stakeholders response notification to IIC
                else if (id == 18)
                {
                    subject = "Response received for Correspondence " + refNo;
                    header = "";
                    Content = "<table>"
                            + "<tr><td>This is to inform that " + refNum1 + " have responded to the correspondence which you have sent.</td></tr>"
                            + "</table>";
                }
                //external stakeholders response notification
                else if (id == 19)
                {
                    subject = "Response received for Correspondence " + status;
                    header = "";
                    Content = "<table>"
                            + "<tr><td>You have responded to the correspondence with the following comments:</td></tr>"
                            + "<tr><td>" + CorresContent + "</td></tr>"
                            + "</table>";
                }
                //mail for Safety Recommendation
                else if (id == 8)
                {
                    subject = "New Safety Recommendation " + refNo;
                    header = "";
                    Content = "<table>"
                            + "<tr><td>This is to inform that a new Safety Recommendation have been recommended for investigation " + refNum1 + ".</td></tr>"
                            + "</table>";
                }
                //mail for Safety Recommendation with DAAI status
                //else if (id == 9)
                //{
                //    string mailContent = string.Empty;
                //    string mailFeedback = string.Empty;
                //    subject = "DAAI Update on Safety Recommendation " + refNo;
                //    header = "";
                //    if (!string.IsNullOrEmpty(DAAIFeedback))
                //    {
                //        Content = "<table>"
                //            + "<tr><td>This is to inform that Safety Recommendation have been " + status + " with the following feedback. </td></tr> "
                //            + "<tr><td>" + DAAIFeedback + ".</td></tr>"
                //            + "</table>";
                //    }
                //    else
                //    {
                //        Content = "<table>"
                //              + "<tr><td>This is to inform that Safety Recommendation have been " + status + ".</td></tr> "
                //              + "</table>";
                //    }
                //}
                ////mail for Safety Recommendation with IIC update
                //else if (id == 10)
                //{
                //    subject = "IIC Update on Safety Recommendation " + refNo;
                //    header = "";
                //    Content = "<table>"
                //            //+ "<tr><td> This is to inform that Safety Recommendation have been updated based on your feedback.</td></tr>"
                //            + "<tr><td> This is to inform that Safety Recommendation have been updated.</td></tr>"
                //            + "</table>";
                //} 
                else if (id == 9)
                {
                    subject = "IIC comments for Safety Recommendation " + refNo;
                    header = "";
                    Content = "<table>"
                            + "<tr><td>This is to inform that the Safety Recommendation have been updated by IIC with Stakeholder Response.</td></tr>"
                            + "<tr><td> Click <a href = " + redUrl + "> here </a> to view and take action on the recommended SR</td></tr>"
                            + "</table>";
                }
                ////mail for Safety Recommendation with IIC update
                //else if (id == 10)
                //{
                //    subject = "IIC Update on Safety Recommendation " + refNo;
                //    header = "";
                //    Content = "<table>"
                //            //+ "<tr><td> This is to inform that Safety Recommendation have been updated based on your feedback.</td></tr>"
                //            + "<tr><td> This is to inform that Safety Recommendation have been updated.</td></tr>"
                //            + "</table>";
                //}
                //mail for notifying IIC with SR Committee Comments and Action.
                else if (id == 10)
                {
                    subject = "SR Committee Update on Safety Recommendation " + refNo;
                    header = "";
                    Content = "<table>"
                            //+ "<tr><td> This is to inform that Safety Recommendation have been updated based on your feedback.</td></tr>"
                            + "<tr><td> This is to inform that SR Committee have taken on the SR which you have updated.</td></tr>"
                            + "</table>";
                }
                else if (id == 23)
                {
                    subject = "IIC Rejected Safety Recommendation " + refNo;
                    header = "";
                    Content = "<table>"
                            + "<tr><td>This is to inform that the Safety Recommendation have been Rejected by IIC please take further corrective action.</td></tr>"
                            + "<tr><td> Click <a href = " + redUrl + "> here </a> to view and take action on the recommended SR</td></tr>"
                            + "</table>";
                }
                else if (id == 20)
                {
                    subject = "SR " + refNo + " Closed";
                    header = "";
                    Content = "<table>"
                            //+ "<tr><td> This is to inform that Safety Recommendation have been updated based on your feedback.</td></tr>"
                            + "<tr><td> This is to inform that SR have been closed.</td></tr>"
                            + "<tr><td> SR Implementation Status :" + status + "</td></tr>"
                            //+ "<tr><td>Safety Recommendation Content: </td></tr>"
                            + "<tr><td>" + DAAIFeedback + " </td></tr>"
                            + "</table>";
                }
                //SR mail tempalte ends
                //For Close Investigation
                else if (id == 11)
                {
                    subject = "Investigation closure request - " + refNo;
                    header = "";
                    Content = "<table>"
                            + "<tr><td> This is to inform that IIC has requested for the closure of investigation.</td></tr>"
                            + "</table>";
                }
                else if (id == 12)
                {
                    subject = "DAAI update on closure request - " + refNo;
                    header = "";
                    Content = "<table>"
                            + "<tr><td> This is to inform that DAAI has " + status + " the closure request of investigation.</td></tr>"
                            + "</table>";
                }
                //Close investigation tempalte ends
                //New User Registration
                else if (id == 13)
                {
                    subject = "New User - Access to AIMS";
                    header = "";
                    Content = "<table>"
                    + "<tr><td> This is to inform that you are added in AIMS application as " + status + " ." + "</td></tr>"
                    + "<tr><td>Login credentials :" + "</td></tr>"
                    + "<tr><td>Username :" + refNo + "</td></tr>"
                    + "<tr><td>Password : " + refNum1 + "</td></tr>"
                    + "</table>";
                } // for removing existing iic 
                else if (id == 14)
                {
                    var Inv_Detail = db.USP_VIEW_INVESTIGATION_FORM35(Item_ID).FirstOrDefault();
                    if (Inv_Detail != null)
                    {
                        refNo = Inv_Detail.InvestigationNumber;
                        string Not_No = Inv_Detail.NotificationID;
                        subject = "Re-Assign IIC - " + refNo;
                        Content = "<table>"
                               + "<tr><td>Greetings, </td></tr>"
                               + "<tr><td>This is to inform that you are no longer part of investigation " + refNo + "  ,Thanks for all your support ..</td></tr>"
                               + "</table>";
                    }
                }
                else if (id == 16)
                {
                    subject = "Event " + Item_ID + " of Investigation " + refNo + " Submitted";
                    header = "";
                    Content = "<table>"
                     + "<tr><td>" + refNum1 + " " + "has submitted EVENT " + Item_ID + " - (" + DAAIFeedback + ")" + " " + "of Investigation " + refNo + ". Please review and take action.</td></tr>"
                    + "</table>";
                }
                else if (id == 17)
                {
                    subject = "Event " + Item_ID + " - Clarification Request";
                    header = "";
                    Content = "<table>"
                   + "<tr><td>This is to inform you that, EVENT " + Item_ID + " " + "of Investigation " + refNo + " require more clarifications and is reopened now.</ td></tr>"
                   + "<tr><td> Following is the comment from IIC  : </td></tr>"
                   + "<tr><td>" + DAAIFeedback + "</td></tr>"
                    + "</table>";
                }
                else if (id == 22)
                {
                    subject = "Reset Password AIMS";
                    header = "";
                    Content = "Hi, <br/><br/><a href=" + redUrl + ">Click here to reset your password</a><br/><br/>" + DateTime.Now + " End of message";
                }
                else if (id == 24)
                {
                    //subject = "New Event - " + refNum1 + " Assigned - " + refNo + "";
                    subject = DAAIFeedback;
                    header = "";
                    Content = "<table>"
                   + "<tr><td>This is to inform you that, Sub-Task " + refNum1.Remove(0, 1) + " " + "(" + status + ")" + " of Investigation " + refNo + " has been assigned to you.</ td></tr>"
                    //+ "<tr><td><p>Click <a href =" + redUrl + ">here </a>to view and take action</p></td></tr>"
                   + "</table>";
                }
                else if (id == 25)
                {
                    subject = "Event " + refNum1 + " Unassigned ";
                    header = "";
                    Content = "<table>"
                   + "<tr><td>This is to inform you that, EVENT " + refNum1 + " " + "of Investigation " + refNo + " has been removed from you.</ td></tr>"
                   + "</table>";
                    footer = "<table>"
                   + "<tr><td><p> Warm Regards,</p></td></tr>"
                   + "<tr><td>Investigator In Charge</td></tr>"
                   + "</table>";
                }
                //mail for Assign Accredited Representative Notification
                else if (id == 26)
                {
                    var Inv_Detail = db.USP_VIEW_INVESTIGATION_FORM35(Item_ID).FirstOrDefault();
                    if (Inv_Detail != null)
                    {
                        refNo = Inv_Detail.InvestigationNumber;
                        string Not_No = Inv_Detail.NotificationID;
                        subject = "Assigned as Accredited Representative - " + refNo;
                        Content = "<table>"
                               + "<tr><td>Greetings, </td></tr>"
                               + "<tr><td>This is to inform that you have been assigned as an Accredited Representative for the below investigation.</td></tr>"
                               + "</table>";
                    }
                }
                /*mail content section ends*/
                /*footer section*/
                if (id == 1 || id == 7 || id == 14 || id == 9 || id == 20 || id == 15 || id ==23 )
                {
                    footer = "<table>"
                    + "<tr><td><p> Warm Regards,</p></td></tr>"
                    + "<tr><td> " + UserDesig + "</td></tr>"
                    + "</table>";
                }
                else if (id == 16)
                {
                    footer = "<table>"
                    + "<tr><td><p> Warm Regards,</p></td></tr>"
                    + "<tr><td> " + refNum1 + "</td></tr>"
                    + "</table>";
                }
                else if (id == 18 || id == 10)
                {
                    footer = "<table style='margin-top:10px;'>"
                              + "<tr><td><p>Click <a href =" + redUrl + ">here </a>to view and take action</p></td></tr>"
                              //+ "<tr><td><a href=" + redUrl + " >" + refNo + "</a></td></tr>"
                              + "</table>";
                }
                else if (id == 19)
                {
                    footer = null;
                }
                else if (id == 17)
                {
                    footer = "<table>"
                    + "<tr><td><p> Warm Regards,</p></td></tr>"
                    + "<tr><td>Investigator In Charge</td></tr>"
                    + "</table>";
                }               
                else if (id == 22)
                {
                    footer = null;
                }
                //mail when form35 is submitted
                else if (id == 27)
                {
                    //var Occ_Detail = db.USP_VIEW_OCCURRENCE(Item_ID, Convert.ToInt32(HttpContext.Current.Session["UserId"])).FirstOrDefault();
                    //refNo = Occ_Detail.NotificationId;
                    subject = "Occurrence Notification " + refNo;
                    header = "";
                    //header = "<div><table>"
                    //              + "<tr><td><p>You have received a notification on a new occurence</p></td></tr>"
                    //              + "</table></div>";
                    Content = "<div><table>"
                            + "<tr><td>Greetings, </td></tr>"
                            + "<tr><td>This is to inform you that the Notification Form035 is completed and submitted by Duty Investigator. The following are the case details:</ td></tr>"
                            + "</table></div>" + Environment.NewLine + Environment.NewLine + DAAIFeedback;
                }
                else
                {
                    footer = "<table style='margin-top:10px;'>"
   + "<tr><td><p>Click <a href =" + redUrl + ">here </a>to view and take action</p></td></tr>"
  //+ "<tr><td><a href=" + redUrl + " >" + refNo + "</a></td></tr>"
  + "</table>"
  + "<table>"
   + "<tr><td><p> Warm Regards,</p></td></tr>"
   //+ "<tr><td>" + fromUser + "</td></tr>"
   + "<tr><td> " + UserDesig + "</td></tr>"
   + "</table>";
                }
                /*footer section ends*/
                body = header + Content + footer;
                MailContent[0] = subject;
                MailContent[1] = body;
                //"<tr style='border-left:5px solid;'><td><table style='font-style:italic; margin-left: 27pt; margin-top:15px; margin-bottom:10px;'><tr><td style='border-left: 6px solid #FE6F97; width:30px;'></td><td><p>Dear <b>" + toUser.Name + "</b>,</p><p>You have been appreciated by <b>" + fromUser.Name + "</b>.</p><p>Message:</p></td></tr></table></td></tr>" +
                //"<tr><td><table style='margin-left: 28.75pt; background:#003f72;'><tr><td style='border: 3px solid #FE6F97;'><p style='line-height:20.5pt; font-style:italic; paddding:20px; margin-right:12px; margin-left:12px; color:#fff; width:400px !important; height:auto;'>" + body + "</p></td></tr></table></td></tr></table>" +
                //"<table style='width: 80%; font-family: Calibri; margin: 0px auto; text-align:center'><tr ><td><p><span style='font-family:Calibri;font-style:italic;float:left;'>This is an automatic system generated email. Please do not reply.</span><td></p></tr></table>";
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("MailConfiguration", "getMailTemplate", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(HttpContext.Current.Session["UserId"]));

            }
            return MailContent;
        }
        /// <summary>
        /// Send Mail Notification As Inline especially the occurrence email notification 
        /// </summary>
        /// <param name="templateID">Receive mail template id</param> 
        /// <param name="fromUser">Receive mail from id</param> 
        /// <param name="fromUserDesig">Receive mail from user designation</param> 
        /// <param name="RedirectUrl">Receive Redirect Url</param>
        /// <param name="ToUser">Receive to user mail from id</param> 
        /// <param name="ToUserMail">Receive to user mail address</param> 
        /// <param name="ID">Receive investigation id</param> 
        /// <param name="filePath">Receive attachment file id</param>
        /// <param name="refNo">Receive file refno</param> 
        /// <param name="refno1">Receive file refno </param> 
        /// <param name="status">Receive ststus of mail</param> 
        /// <param name="Feedback">Receive any feedback as string</param>
        public bool SendMailNotificationAsInline(int templateID, string fromUser, int fromUserDesig, string RedirectUrl, List<string> ToUser, List<string> ToUserMail, int ID, string filePath, string refNo, string refno1, string status, string Feedback)
        {
            bool Isent = false;
            try
            {
                string[] SMTPData = comm.getMailValues();
                
                MailMessage mail = new MailMessage();
                string[] mailcontents = getMailTemplate(templateID, fromUser, RedirectUrl, fromUserDesig, ID, refNo, refno1, status, filePath, null);
                string subject = mailcontents[0];
                string body = mailcontents[1];
                if (!string.IsNullOrEmpty(subject) && !string.IsNullOrEmpty(body) && !string.IsNullOrEmpty(SMTPData[0]))
                {
                    if (ToUserMail != null)
                    {
                        foreach (var usermail in ToUserMail)
                        {
                            mail.To.Add(usermail);
                        }

                        SmtpClient client = new SmtpClient();

                        mail.From = new MailAddress(SMTPData[4]);
                        client.Host = SMTPData[0];
                        client.DeliveryMethod = SmtpDeliveryMethod.Network;
                        client.UseDefaultCredentials = false;

                        if (SMTPData[1] != "0")
                        {
                            client.Port = Convert.ToInt32(SMTPData[1]);
                            client.EnableSsl = true;
                            client.Credentials = new NetworkCredential(new MailAddress(SMTPData[2]).Address, SMTPData[3]);
                        }
                      
                        mail.Subject = subject;

                        mail.IsBodyHtml = true;

                        string imgurl= HttpContext.Current.Server.MapPath("~/") + "img/logo2020.png";

                        // Create the HTML view
                        AlternateView htmlView = AlternateView.CreateAlternateViewFromString(
                                                                     body.ToString(),
                                                                     System.Text.Encoding.UTF8,
                                                                     MediaTypeNames.Text.Html);
                        // Create a plain text message for client that don't support HTML
                        AlternateView plainView = AlternateView.CreateAlternateViewFromString(
                                                                    System.Text.RegularExpressions.Regex.Replace(body.ToString(),
                                                                                  "<[^>]+?>",
                                                                                  string.Empty),
                                                                    System.Text.Encoding.UTF8,
                                                                    MediaTypeNames.Text.Plain);
                        string mediaType = MediaTypeNames.Image.Jpeg;
                        LinkedResource img = new LinkedResource(imgurl, mediaType);
                        // Make sure you set all these values!!!
                        img.ContentId = "myImageID";
                        img.ContentType.MediaType = mediaType;
                        img.TransferEncoding = TransferEncoding.Base64;
                        img.ContentType.Name = img.ContentId;
                        img.ContentLink = new Uri("cid:" + img.ContentId);
                        htmlView.LinkedResources.Add(img);
                        //////////////////////////////////////////////////////////////
                        mail.AlternateViews.Add(plainView);
                        mail.AlternateViews.Add(htmlView);
                        mail.IsBodyHtml = true;

                        filePath = "";

                        if (System.IO.File.Exists(filePath))
                        {
                            System.Net.Mail.Attachment attachment;
                            attachment = new System.Net.Mail.Attachment(filePath);
                            mail.Attachments.Add(attachment);
                        }

                        client.Send(mail);
                        mail.Attachments.Dispose();
                        Isent = true;
                    }
                }
                if (Directory.Exists(HttpContext.Current.Server.MapPath("~/") + "MailAttachments"))
                    Directory.Delete(HttpContext.Current.Server.MapPath("~/") + "MailAttachments", true);
                if (System.IO.File.Exists(filePath))
                {
                    System.IO.File.Delete(filePath);
                }
            }
            catch (System.Exception ex)
            {
                Isent = false;
                comm.Exception_Log("MailConfiguration", "SendMailNotification", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(HttpContext.Current.Session["UserId"]));
            }
            finally
            {

            }
            return Isent;
        }
    }
}