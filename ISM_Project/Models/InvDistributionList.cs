﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ISM_Project.Models
{
    public class InvDistributionList
    {
        public int InvestigationFileID { get; set; }
        public string InvestigationNumber { get; set; }
        public string LocalDateAndTimeOfOccurrence { get; set; }
        public string UTCDateAndTimeOfOccurrence { get; set; }
        public string NotifierName { get; set; }
        public string NotifierOrganization { get; set; }

        public int EmailDistributionListID { get; set; }
        public int LKDistributionListTypeID { get; set; }
        public string EmailDistributionListName { get; set; }
        public string EmailDistributionListDescription { get; set; }

        public int EmailDistrubutionListRecipientID { get; set; }
        public Nullable<int> UserID { get; set; }
        public string EmailAddress { get; set; }
        public string RecipientName { get; set; }        

        public int CreatedBy { get; set; }
        public System.DateTime DateCreated { get; set; }
        public System.DateTime DateUpdated { get; set; }
        public int UpdatedBy { get; set; }
        public string[] Recipients_Info { get; set; }

        public IEnumerable<USP_VIEW_INVESTIGATION_DISTRIBUTIONLIST_Result> InvDistrList { get; set; }

        public int invAssigned { get; set; }
    }
}