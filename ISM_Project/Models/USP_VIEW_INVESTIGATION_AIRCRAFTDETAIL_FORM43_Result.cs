//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ISM_Project.Models
{
    using System;
    
    public partial class USP_VIEW_INVESTIGATION_AIRCRAFTDETAIL_FORM43_Result
    {
        public Nullable<int> InvestigationID { get; set; }
        public Nullable<int> LKAircraftModelID { get; set; }
        public string LKAircraftModelName { get; set; }
        public Nullable<int> LKAircraftRegistrationID { get; set; }
        public string LKAircraftRegistrationName { get; set; }
        public string FlightNumber { get; set; }
        public Nullable<int> LKAirlineOperatorID { get; set; }
        public string OperatorName { get; set; }
        public string OprContNo { get; set; }
        public string OprState { get; set; }
        public Nullable<int> LKDepartureAirportID { get; set; }
        public Nullable<int> LKDestinationAirportID { get; set; }
        public string DepAirport { get; set; }
        public string DestAirport { get; set; }
        public string latitudeAndlongitude { get; set; }
        public Nullable<int> CrewOnBoard { get; set; }
        public Nullable<int> PassengersOnBoard { get; set; }
        public Nullable<int> CrewsWithFatalInjury { get; set; }
        public Nullable<int> PassengersWithFatalInjury { get; set; }
        public Nullable<int> OthersWithFatalInjury { get; set; }
        public Nullable<int> CrewsWithSeriousInjury { get; set; }
        public Nullable<int> PassengersWithSeriousInjury { get; set; }
        public Nullable<int> OthersWithSeriousInjury { get; set; }
    }
}
