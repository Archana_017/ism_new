﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ISM_Project.Models
{
    public class teammanagement
    {


        public IEnumerable<USP_VIEW_ACCID_TEAM_MEMBERS_Result> accidteammembers { get; set; }
        public List<USP_VIEW_ASSIGN_TEAM_MEMBERS_Result> assignteammembers { get; set; }
        public string rem_reason { get; set; }
        public IEnumerable<USP_VIEW_INVESTIGATION_FORM35_Result> Inv_team { get; set; }
    }
}