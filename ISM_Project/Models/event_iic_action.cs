﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ISM_Project.Models
{
    public class event_iic_action
    {
        public int invest_file_id { get; set; }
        public int  inv_task_assignment_id { get; set; }
        public int  task_grp_id { get; set; }
        public int event_uniq_no { get; set; }
        public  int event_action_status { get; set; }
        public string comment { get; set; }
 }
}