﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ISM_Project.Models
{
    public class InvChart
    {
        public int OccurrenceYear { get; set; }
        public string[] OccurenceType { get; set; }
        public int[] NoofIncident { get; set; }
    }
}