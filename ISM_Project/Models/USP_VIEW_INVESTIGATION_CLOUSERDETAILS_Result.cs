//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ISM_Project.Models
{
    using System;
    
    public partial class USP_VIEW_INVESTIGATION_CLOUSERDETAILS_Result
    {
        public string InvestigationClosureReason { get; set; }
        public System.DateTime DateCreated { get; set; }
        public int InvestigationClosureDetailID { get; set; }
        public string InvestigationFileNumber { get; set; }
        public string ClosureRecommendedUserName { get; set; }
        public Nullable<int> ClosureRecommendedBy { get; set; }
        public int InvestigationClosedBy { get; set; }
    }
}
