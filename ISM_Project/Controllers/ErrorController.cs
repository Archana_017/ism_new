﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ISM_Project.Controllers
{
    /// <summary>
    ///  Error - Controller for handling application error
    /// </summary> 
    /// 

    public class ErrorController : Controller
    {
        /// <summary>
        /// Showing common error page
        /// </summary> 
        /// 
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// Page not found error page display if 404 exception occurred in application
        /// </summary> 
        /// 
        public ActionResult NotFound()
        {
            Response.StatusCode = 404;  

            return View();
        }
        /// <summary>
        /// Page not found error page display if 404 exception occurred in application
        /// </summary> 
        /// 
        public ActionResult PageNotFound()
        {
            Response.StatusCode = 404;

            return View();
        }

    }
}