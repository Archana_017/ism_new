﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ISM_Project.Models
{
    public class AircraftRegistration
    {
        public int LKAircraftRegistrationID { get; set; }
        public string LKAircraftRegistrationName { get; set; }
        public string LKAircraftRegistrationDescription { get; set; }
        public int LKAircraftTypeID { get; set; }
        public Nullable<int> LKAirlineOperatorID { get; set; }
        public Nullable<int> LKAircraftModelID { get; set; }
        public string EngineInfo { get; set; }
        public string MaximumMass { get; set; }
        public Nullable<int> LKAircraftCategoryID { get; set; }
        public Nullable<int> StateofRegistryID { get; set; }
        public Nullable<int> StateofOperatorID { get; set; }
        public string OperatorContacts { get; set; }
        public string MSN { get; set; }
        public Nullable<System.DateTime> DateCreated { get; set; }
        public Nullable<System.DateTime> DateUpdated { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<int> UpdatedBy { get; set; }

        public string LKAircraftModelName { get; set; }
        public string LKAircraftCategoryName { get; set; }
        public string LKAirlineOperatorName { get; set; }
    }
}