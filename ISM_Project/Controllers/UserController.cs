﻿using ISM_Project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace ISM_Project.Controllers
{
    /// <summary>
    /// User Controller contains the functionality of user Forgot Password,Reset Password and application Logout
    /// </summary> 
    /// 
    public class UserController : Controller
    {        
        MailConfiguration mail = new MailConfiguration();
        gcaa_ismEntities dc = new gcaa_ismEntities();

        /// <summary>
        /// Index
        /// </summary> 
        /// 
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// Forgot Password
        /// </summary> 
        /// 
        public ActionResult ForgotPassword()
        {
            return View();
        }
        /// <summary>
        /// AIMS User forget password page displayed
        /// </summary> 
        /// 
        /// <param name="loginmodel">Receive the user login info of particular user </param>
        [HttpPost]
        public ActionResult ForgotPassword(Login loginmodel)
        {          
            using (gcaa_ismEntities gc = new gcaa_ismEntities())
            {
                var acc = gc.Users.Where(a => a.EmailAddress == loginmodel.username).FirstOrDefault();

                if (acc != null)
                {
                    string resetcode = Guid.NewGuid().ToString();                   

                    SendVerificationLinkEmail(loginmodel.username, resetcode, "ResetPassword");

                    acc.ResetPasswordCode = resetcode;

                    gc.Configuration.ValidateOnSaveEnabled = true;

                    gc.USP_UPDT_USER_FORGOTPASSWORD(0,acc.UserID, resetcode, 0, DateTime.Now.AddDays(1), DateTime.Now, DateTime.Now);

                    gc.SaveChanges();
                }
                else
                {
                    ViewBag.Message = "Email Not Found";
                    return View(loginmodel);
                }
            }

            ViewBag.Message = "Reset password link send to your email address.";
            return View("ForgotPasswordPost");
        }
        /// <summary>
        /// Forgot Password page
        /// </summary> 
        /// 
        public ActionResult ForgotPasswordPost()
        {
            return View();
        }
        /// <summary>
        /// Reset Password page 
        /// </summary> 
        /// 
        /// <param name="id">Receive the user Password Token </param>
        public ActionResult ResetPassword(string id)
        {
            using (gcaa_ismEntities dc = new gcaa_ismEntities())
            {
                var user = dc.UserForgotPasswords.Where(a => a.PasswordToken == id).FirstOrDefault();
                if (user != null)
                {
                    ResetPasswordModel model = new ResetPasswordModel();
                    model.ResetCode = id;
                    return View(model);
                }
                else
                {
                    return HttpNotFound();
                }
            }
        }
        /// <summary>
        /// Reset Password functionalty of AIMS Application users
        /// </summary> 
        /// 
        /// <param name="resetmodel">Receive the user Password Reset details </param>
        [HttpPost]
        public ActionResult ResetPassword(ResetPasswordModel resetmodel)
        {
            var message = "";

            if (ModelState.IsValid)
            {
                if (resetmodel.NewPassword != resetmodel.ConfirmPassword)
                {
                    ViewBag.Message = "Passwords Not Matching";

                    return View(resetmodel);
                }

                LoginPassword pass = new LoginPassword();

                using (gcaa_ismEntities dc = new gcaa_ismEntities())
                {
                    var user = dc.UserForgotPasswords.Where(a => a.PasswordToken == resetmodel.ResetCode && a.TokenAlreadyClaimed==0).FirstOrDefault();

                    if (user != null)
                    {
                        string PasswordSalt = pass.createsalt(5);

                        string PasswordHash = pass.Generatehash(resetmodel.NewPassword, PasswordSalt);

                        dc.Configuration.ValidateOnSaveEnabled = false;

                        dc.SaveChanges();

                        dc.USP_UPDT_USER_FORGOTPASSWORD(user.UserForgotPasswordID, user.UserID, resetmodel.ResetCode, 1, DateTime.Now.AddDays(1), DateTime.Now, DateTime.Now);

                        dc.SaveChanges();

                        dc.USP_UPDT_USER_PASSWORD(user.UserID, PasswordSalt, PasswordHash);

                        resetmodel.ResetCode = null;

                        //user.ResetPasswordCode = "";

                        message = "New Password Updated Successfully";
                    }
                    else
                    {
                        message = "Invalid Reset Code";
                    }
                }
            }
            else
            {
                message = "Something Invalid";
            }

            ViewBag.Message = message;

            return View("ForgotPasswordPost");
        }
        /// <summary>
        /// Send Verification Link Email for reset AIMS user password
        /// </summary> 
        /// 
        /// <param name="emailID">Receive the user email </param>
        /// <param name="activationcode">Receive the activation code </param>
        /// <param name="emailFor">Receive the Reset password status command</param>
        [NonAction]
        private void SendVerificationLinkEmail(string emailID, string activationcode, string emailFor)
        {
            var verifyurl = "/User/" + emailFor + "/" + activationcode;
            var link = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, verifyurl);
            var toemail = new MailAddress(emailID);
            string subject = string.Empty;
            string body = "";

            if (emailFor == "ResetPassword")
            {
                subject = "Reset Password AIMS";
                body = "Hi, <br/><br/><a href=" + link + ">Click here to reset your password</a><br/><br/>" + DateTime.Now + " End of message";
            }

            List<string> DListUsersEmail = new List<string>();
            DListUsersEmail.Add(emailID);

            bool Isent = mail.SendMailNotification(22, Convert.ToString(Session["FirstName"]), Convert.ToInt32(Session["roleid"]),

                link.ToString(), null, DListUsersEmail, 0, null, emailID, "", Convert.ToString(""), null);
        }
        /// <summary>
        /// Application Logout
        /// </summary> 
        /// 
        public ActionResult Logout()
        {
            Session.Clear();
            Session.RemoveAll();
            return RedirectToAction("Index", "Home");
        }
    }
}