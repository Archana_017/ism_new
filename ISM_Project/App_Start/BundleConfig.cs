﻿using System.Web;
using System.Web.Optimization;

namespace ISM_Project
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            //            "~/Scripts/jquery.{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/DatePickerCss").Include(
                      "~/Content/bootstrap-datetimepicker.css"
                      ));
            bundles.Add(new ScriptBundle("~/bundles/DatePickerJs").Include(
                      "~/Scripts/moment.min.js",
                      "~/Scripts/moment-with-locales.min.js",
                      "~/Scripts/bootstrap-datetimepicker.js"));

            bundles.Add(new StyleBundle("~/Content/custom").Include(                      
                      "~/css/bootstrap.min.css",
                      "~/css/font-awesome.min.css",
                      "~/css/sb-admin.css",
                      "~/css/style.css",
                      "~/css/responsive.css",
                       "~/css/circle.css",
                        "~/css/bootstrap-select.min.css"
                      
                      ));
            BundleTable.EnableOptimizations = true;
        }
    }
}
