﻿using DocumentFormat.OpenXml.Wordprocessing;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;
using ISM_Project.Models;
using Newtonsoft.Json;
using System.Reflection;
using System.Text.RegularExpressions;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using Newtonsoft.Json.Linq;
using A1 = DocumentFormat.OpenXml.Drawing;
using DW = DocumentFormat.OpenXml.Drawing.Wordprocessing;
using PIC = DocumentFormat.OpenXml.Drawing.Pictures;
using ClosedXML.Excel;
using System.Data;
using System.Web.Script.Serialization;

namespace ISM_Project.Controllers
{
    /// <summary>
    /// Forms Controller - contains different type of investigation file document such as Form35,Form39,Form43,Form46 and Form33
    /// </summary> 
    /// 
    public class FormsController : Controller
    {
        static string SharepointfilePath = string.Empty;
        gcaa_ismEntities dc = new gcaa_ismEntities();
        Common comm = new Common();
        string[] attachments = new string[50];
        Dictionary<string, List<string>> EventsAttachments = new Dictionary<string, List<string>>();
        Dictionary<string, string> FilesCaptions = new Dictionary<string, string>();
        string AddInfo = " ";
        /// <summary>
        /// Form35 view page downloading as doc format 
        /// </summary> 
        /// 
        /// <param name="invFileID">Receive the investigation file id.</param>
        public ActionResult Form35(int invFileID = 0)
        {
            try
            {
                string status = string.Empty;
                string sourceFile = Server.MapPath(Path.Combine("~/App_data", "Form-035.dotx"));
                string fileName = "Form 035_" + Convert.ToString(DateTime.Now.ToFileTime()) + ".docx";
                string destinationFile = Server.MapPath(Path.Combine("~/App_data", fileName));
                Dictionary<string, string> keyValues = new Dictionary<string, string>();
                var form35values = dc.USP_VIEW_INVESTIGATION_FORM35_PDF(invFileID).ToList();
                var form35AircraftValues = dc.USP_VIEW_INVESTIGATION_AIRCRAFT_DETAIL_FORM35_PDF(invFileID).ToList();
                //Main details of the investigation
                foreach (USP_VIEW_INVESTIGATION_FORM35_PDF_Result PDFResult in form35values)
                {
                    Type type = PDFResult.GetType();
                    PropertyInfo[] props = type.GetProperties();
                    string[] str = new string[50];
                    foreach (var prop in props)
                    {
                        if (prop.PropertyType.FullName.Contains("DateTime"))
                        {
                            string convDateTime = "N/A";
                            if (prop.GetValue(PDFResult) != null)
                                convDateTime = comm.convertDateTime(Convert.ToDateTime(prop.GetValue(PDFResult)));
                            keyValues.Add(prop.Name, (!string.IsNullOrEmpty(Convert.ToString(convDateTime)) ? (Convert.ToString(convDateTime)) : "N/A"));
                        }
                        else
                        {
                            keyValues.Add(prop.Name, (!string.IsNullOrEmpty(Convert.ToString(prop.GetValue(PDFResult))) ? (Convert.ToString(prop.GetValue(PDFResult))) : "N/A"));
                        }
                    }
                }
                //Aircraft details of the investigation
                Dictionary<string, List<string>> keyValuesAirDet = new Dictionary<string, List<string>>();
                if (form35AircraftValues != null)
                {
                    foreach (USP_VIEW_INVESTIGATION_AIRCRAFT_DETAIL_FORM35_PDF_Result PDFAirResult in form35AircraftValues)
                    {
                        Type type = PDFAirResult.GetType();
                        PropertyInfo[] props = type.GetProperties();
                        //string str = "{";
                        string[] str = new string[50];
                        foreach (var prop in props)
                        {
                            List<string> list = new List<string>();
                            if (keyValuesAirDet.ContainsKey(prop.Name))
                                keyValuesAirDet[prop.Name].Add(!string.IsNullOrEmpty(Convert.ToString(prop.GetValue(PDFAirResult))) ? (Convert.ToString(prop.GetValue(PDFAirResult))) : "N/A");
                            else
                            {
                                keyValuesAirDet.Add(prop.Name, list);
                                list.Add(!string.IsNullOrEmpty(Convert.ToString(prop.GetValue(PDFAirResult))) ? (Convert.ToString(prop.GetValue(PDFAirResult))) : "N/A");
                            }
                        }
                    }
                }
                //For attachments section
                dynamic FILES = dc.USP_VIEW_INVESTIGATION_ATTACHMENTFILE(invFileID).ToList();
                MemoryStream fileMem = getFile(keyValues, keyValuesAirDet, FILES, sourceFile, destinationFile, "F35");
                fileMem.Seek(0, SeekOrigin.Begin);
                System.IO.File.Delete(destinationFile);
                return File(fileMem, "application/octet-stream", fileName);
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("FormsController", "Form35", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("NotFound", "Error");
            }
            //return View();
        }
        /// <summary>
        /// Transfer of Possession view page downloading as doc format 
        /// </summary> 
        /// 
        /// <param name="invFileID">Receive the investigation file id.</param>
        public ActionResult TransferOfPossession(int invFileID = 0)
        {
            try
            {
                string status = string.Empty;
                string sourceFile = Server.MapPath(Path.Combine("~/App_data", "PROPERTY POSSESSION.dotx"));
                string fileName = "Transfer of Possession_" + Convert.ToString(DateTime.Now.ToFileTime()) + ".doc";
                string destinationFile = Server.MapPath(Path.Combine("~/App_data", fileName));
                Dictionary<string, string> keyValues = new Dictionary<string, string>();
                var possessionFormValues = dc.USP_VIEW_POSSESSION_FORM_PDF(invFileID).ToList();
                //Main details of the investigation
                foreach (USP_VIEW_POSSESSION_FORM_PDF_Result PDFResult in possessionFormValues)
                {
                    Type type = PDFResult.GetType();
                    PropertyInfo[] props = type.GetProperties();
                    string[] str = new string[50];
                    foreach (var prop in props)
                    {
                        if (prop.PropertyType.FullName.Contains("DateTime"))
                        {
                            string convDateTime = "N/A";
                            if (prop.GetValue(PDFResult) != null)
                                convDateTime = comm.convertDate(Convert.ToDateTime(prop.GetValue(PDFResult)));
                            keyValues.Add(prop.Name, (!string.IsNullOrEmpty(Convert.ToString(convDateTime)) ? (Convert.ToString(convDateTime)) : "N/A"));
                        }
                        else
                        {
                            keyValues.Add(prop.Name, (!string.IsNullOrEmpty(Convert.ToString(prop.GetValue(PDFResult))) ? (Convert.ToString(prop.GetValue(PDFResult))) : "N/A"));
                        }
                    }
                }
                var form35values = dc.USP_VIEW_INVESTIGATION_FORM35_PDF(invFileID).ToList();
                //Main details of the investigation
                foreach (USP_VIEW_INVESTIGATION_FORM35_PDF_Result PDFResult in form35values)
                {
                    Type type = PDFResult.GetType();
                    PropertyInfo[] props = type.GetProperties();
                    string[] str = new string[50];
                    foreach (var prop in props)
                    {
                        if (prop.PropertyType.FullName.Contains("DateTime"))
                        {
                            string convDateTime = "N/A";
                            if (prop.GetValue(PDFResult) != null)
                                convDateTime = comm.convertDateTime(Convert.ToDateTime(prop.GetValue(PDFResult)));
                            keyValues.Add(prop.Name, (!string.IsNullOrEmpty(Convert.ToString(convDateTime)) ? (Convert.ToString(convDateTime)) : "N/A"));
                        }
                        else
                        {
                            keyValues.Add(prop.Name, (!string.IsNullOrEmpty(Convert.ToString(prop.GetValue(PDFResult))) ? (Convert.ToString(prop.GetValue(PDFResult))) : "N/A"));
                        }
                    }
                }
                //For attachments section
                dynamic FILES = dc.USP_VIEW_INVESTIGATION_ATTACHMENTFILE(invFileID).ToList();
                MemoryStream fileMem = getFile(keyValues, null, FILES, sourceFile, destinationFile, "Possession");
                fileMem.Seek(0, SeekOrigin.Begin);
                System.IO.File.Delete(destinationFile);
                return File(fileMem, "application/octet-stream", fileName);
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("FormsController", "Form35", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("NotFound", "Error");
            }
            //return View();
        }
        /// <summary>
        /// Form39 view page downloading as doc format 
        /// </summary> 
        /// 
        /// <param name="invFileID">Receive the investigation file id.</param>
        public ActionResult Form39(int invFileID = 0)
        {
            try
            {
                string status = string.Empty;
                string sourceFile = Server.MapPath(Path.Combine("~/App_data", "Form-039.dotx"));
                string fileName = "Form 039_" + Convert.ToString(DateTime.Now.ToFileTime()) + ".docx";
                string destinationFile = Server.MapPath(Path.Combine("~/App_data", fileName));
                Dictionary<string, string> keyValues = new Dictionary<string, string>();
                var form35values = dc.USP_VIEW_INVESTIGATION_FORM35_PDF(invFileID).ToList();
                var form35AircraftValues = dc.USP_VIEW_INVESTIGATION_AIRCRAFT_DETAIL_FORM35_PDF(invFileID).ToList();
                foreach (USP_VIEW_INVESTIGATION_FORM35_PDF_Result PDFResult in form35values)
                {
                    Type type = PDFResult.GetType();
                    PropertyInfo[] props = type.GetProperties();
                    string[] str = new string[50];
                    foreach (var prop in props)
                    {
                        if (prop.PropertyType.FullName.Contains("DateTime"))
                        {
                            string convDateTime = "N/A";
                            if (prop.GetValue(PDFResult) != null)
                                convDateTime = comm.convertDateTime(Convert.ToDateTime(prop.GetValue(PDFResult)));
                            keyValues.Add(prop.Name, (!string.IsNullOrEmpty(Convert.ToString(convDateTime)) ? (Convert.ToString(convDateTime)) : "N/A"));
                        }
                        else
                        {
                            keyValues.Add(prop.Name, (!string.IsNullOrEmpty(Convert.ToString(prop.GetValue(PDFResult))) ? (Convert.ToString(prop.GetValue(PDFResult))) : "N/A"));
                        }
                    }
                }
                Dictionary<string, List<string>> keyValuesAirDet = new Dictionary<string, List<string>>();

                if (form35AircraftValues != null)
                {
                    foreach (USP_VIEW_INVESTIGATION_AIRCRAFT_DETAIL_FORM35_PDF_Result PDFAirResult in form35AircraftValues)
                    {
                        Type type = PDFAirResult.GetType();
                        PropertyInfo[] props = type.GetProperties();
                        //string str = "{";
                        string[] str = new string[50];
                        foreach (var prop in props)
                        {

                            List<string> list = new List<string>();
                            if (keyValuesAirDet.ContainsKey(prop.Name))
                                keyValuesAirDet[prop.Name].Add(!string.IsNullOrEmpty(Convert.ToString(prop.GetValue(PDFAirResult))) ? (Convert.ToString(prop.GetValue(PDFAirResult))) : "N/A");
                            else
                            {
                                keyValuesAirDet.Add(prop.Name, list);
                                list.Add(!string.IsNullOrEmpty(Convert.ToString(prop.GetValue(PDFAirResult))) ? (Convert.ToString(prop.GetValue(PDFAirResult))) : "N/A");
                            }
                        }
                    }
                }
                var teammembers = dc.USP_VIEW_INVESTIGATION_IIC_TM(invFileID).ToList();
                //Dictionary<string, List<string>> keyValuesTM = new Dictionary<string, List<string>>();
                //if (teammembers != null)
                //{
                //    foreach (USP_VIEW_INVESTIGATION_IIC_TM_Result TMResult in teammembers)
                //    {
                //        Type type = TMResult.GetType();
                //        PropertyInfo[] props = type.GetProperties();
                //        //string str = "{";
                //        string[] str = new string[50];
                //        int i = 0;
                //        foreach (var prop in props)
                //        {
                //            List<string> list = new List<string>();
                //            if (keyValuesTM.ContainsKey(prop.Name))
                //                keyValuesTM[prop.Name].Add(!string.IsNullOrEmpty(Convert.ToString(prop.GetValue(TMResult))) ? (Convert.ToString(prop.GetValue(TMResult))) : "N/A");
                //            else
                //            {
                //                keyValuesTM.Add(prop.Name, list);
                //                list.Add(!string.IsNullOrEmpty(Convert.ToString(prop.GetValue(TMResult))) ? (Convert.ToString(prop.GetValue(TMResult))) : "N/A");
                //            }
                //        }
                //    }
                //}
                //dynamic IIC_TM = dc.USP_VIEW_INVESTIGATION_IIC_TM(invFileID).ToList();
                MemoryStream fileMem = getFile(keyValues, keyValuesAirDet, teammembers, sourceFile, destinationFile, "F39");
                fileMem.Seek(0, SeekOrigin.Begin);
                System.IO.File.Delete(destinationFile);
                return File(fileMem, "application/octet-stream", fileName);
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("FormsController", "Form39", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("NotFound", "Error");
            }
            // return View();
        }
        /// <summary>
        /// Form43 view page downloading as doc format 
        /// </summary> 
        /// 
        /// <param name="invFileID">Receive the investigation file id.</param>
        public ActionResult Form43(int invFileID = 0)
        {
            try
            {
                string status = string.Empty;
                string sourceFile = Server.MapPath(Path.Combine("~/App_data", "Form-043.dotx"));
                string country = string.Empty;
                Dictionary<string, string> keyValues = new Dictionary<string, string>();
                Dictionary<string, string> keyFullValues = new Dictionary<string, string>();
                var form43values = dc.USP_VIEW_INVESTIGATION_FORM43(invFileID).ToList();
                var form43AircraftValues = dc.USP_VIEW_INVESTIGATION_AIRCRAFTDETAIL_FORM43(invFileID).ToList();
                foreach (USP_VIEW_INVESTIGATION_FORM43_Result PDFResult in form43values)
                {
                    Type type = PDFResult.GetType();
                    PropertyInfo[] props = type.GetProperties();
                    string[] str = new string[50];
                    foreach (var prop in props)
                    {
                        if (prop.PropertyType.FullName.Contains("DateTime"))
                        {
                            string convDateTime = "N/A";
                            if (prop.GetValue(PDFResult) != null)
                                convDateTime = comm.convertDateTime(Convert.ToDateTime(prop.GetValue(PDFResult)));
                            keyValues.Add(prop.Name, (!string.IsNullOrEmpty(Convert.ToString(convDateTime)) ? (Convert.ToString(convDateTime)) : "N/A"));
                        }
                        else
                        {
                            keyValues.Add(prop.Name, (!string.IsNullOrEmpty(Convert.ToString(prop.GetValue(PDFResult))) ? (Convert.ToString(prop.GetValue(PDFResult))) : "N/A"));
                        }
                        //for generating the file name with the country
                        if (prop.Name == "CountryName")
                            country = (!string.IsNullOrEmpty(Convert.ToString(prop.GetValue(PDFResult))) ? (Convert.ToString(prop.GetValue(PDFResult))) : "N/A");
                        if (prop.Name == "InvestigationTaskDataInJSON")
                        {
                            string[] keys = { "Report_043_investigationextent.text", "Report_043_characteristicsofaccident.text", "Report_043_Qualificationofthecommander.text" };

                            if (!string.IsNullOrEmpty(Convert.ToString(prop.GetValue(PDFResult))))
                            {
                                keyValues = getOtherValuesfromEventJSON(keys, null, Convert.ToString(prop.GetValue(PDFResult)), keyValues, "Form-043");
                            }
                            else
                            {
                                for (int ii = 0; ii < keys.Length; ii++)
                                {
                                    keyValues.Add(keys[ii], " ");
                                }
                            }

                        }
                    }
                }
                string fileName = "AAI 043-Notification to Foreign States[" + country + "] .docx";
                string destinationFile = Server.MapPath(Path.Combine("~/App_data", fileName));
                Dictionary<string, List<string>> keyValuesAirDet = new Dictionary<string, List<string>>();
                if (form43AircraftValues != null)
                {
                    foreach (USP_VIEW_INVESTIGATION_AIRCRAFTDETAIL_FORM43_Result PDFAirResult in form43AircraftValues)
                    {
                        Type type = PDFAirResult.GetType();
                        PropertyInfo[] props = type.GetProperties();
                        //string str = "{";
                        string[] str = new string[50];
                        foreach (var prop in props)
                        {

                            List<string> list = new List<string>();
                            if (keyValuesAirDet.ContainsKey(prop.Name))
                                keyValuesAirDet[prop.Name].Add(!string.IsNullOrEmpty(Convert.ToString(prop.GetValue(PDFAirResult))) ? (Convert.ToString(prop.GetValue(PDFAirResult))) : "N/A");
                            else
                            {
                                keyValuesAirDet.Add(prop.Name, list);
                                list.Add(!string.IsNullOrEmpty(Convert.ToString(prop.GetValue(PDFAirResult))) ? (Convert.ToString(prop.GetValue(PDFAirResult))) : "N/A");
                            }
                        }
                    }
                }
                MemoryStream fileMem = getFile(keyValues, keyValuesAirDet, null, sourceFile, destinationFile, "F43");
                fileMem.Seek(0, SeekOrigin.Begin);
                System.IO.File.Delete(destinationFile);
                return File(fileMem, "application/octet-stream", fileName);
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("FormsController", "Form43", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("NotFound", "Error");
            }
            //  return View();
        }
        /// <summary>
        /// Form46 view page downloading as doc format 
        /// </summary> 
        /// 
        /// <param name="invFileID">Receive the investigation file id.</param>
        public ActionResult Form46(int invFileID = 0)
        {
            try
            {
                string status = string.Empty;
                string sourceFile = Server.MapPath(Path.Combine("~/App_data", "Form-046.dotx"));
                string country = string.Empty;
                Dictionary<string, string> keyValues = new Dictionary<string, string>();
                var form46Values = dc.USP_VIEW_INVESTIGATION_FORM46(invFileID).ToList();
                foreach (USP_VIEW_INVESTIGATION_FORM46_Result PDFResult in form46Values)
                {
                    Type type = PDFResult.GetType();
                    PropertyInfo[] props = type.GetProperties();
                    string[] str = new string[50];
                    foreach (var prop in props)
                    {
                        if (prop.PropertyType.FullName.Contains("DateTime"))
                        {
                            string convDateTime = "N/A";
                            if (prop.GetValue(PDFResult) != null)
                                convDateTime = comm.convertDateTime(Convert.ToDateTime(prop.GetValue(PDFResult)));
                            keyValues.Add(prop.Name, (!string.IsNullOrEmpty(Convert.ToString(convDateTime)) ? (Convert.ToString(convDateTime)) : "N/A"));
                        }
                        else
                        {
                            keyValues.Add(prop.Name, (!string.IsNullOrEmpty(Convert.ToString(prop.GetValue(PDFResult))) ? (Convert.ToString(prop.GetValue(PDFResult))) : "N/A"));
                        }
                        if (prop.Name == "CountryName")
                            country = (!string.IsNullOrEmpty(Convert.ToString(prop.GetValue(PDFResult))) ? (Convert.ToString(prop.GetValue(PDFResult))) : "N/A");
                    }
                }
                string fileName = "AAI 046 - Reports Approval Checklist_" + Convert.ToString(DateTime.Now.ToFileTime()) + ".docx";
                string destinationFile = Server.MapPath(Path.Combine("~/App_data", fileName));
                MemoryStream fileMem = getFile(keyValues, null, null, sourceFile, destinationFile, "F46");
                fileMem.Seek(0, SeekOrigin.Begin);
                System.IO.File.Delete(destinationFile);
                return File(fileMem, "application/octet-stream", fileName);
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("FormsController", "Form46", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("NotFound", "Error");
            }
            //  return View();
        }
        /// <summary>
        /// Form33 view page downloading as doc format 
        /// </summary> 
        /// 
        /// <param name="invFileID">Receive the investigation file id.</param>
        public ActionResult Form33(int invFileID = 0)
        {
            try
            {
                string status = string.Empty;
                string sourceFile = Server.MapPath(Path.Combine("~/App_data", "Form-033.dotx"));
                string country = string.Empty;
                Dictionary<string, string> keyValues = new Dictionary<string, string>();
                var form33Values = dc.USP_VIEW_INVESTIGATION_FORM33(invFileID).ToList();
                foreach (USP_VIEW_INVESTIGATION_FORM33_Result PDFResult in form33Values)
                {
                    Type type = PDFResult.GetType();
                    PropertyInfo[] props = type.GetProperties();
                    string[] str = new string[50];
                    foreach (var prop in props)
                    {
                        if (prop.PropertyType.FullName.Contains("DateTime"))
                        {
                            string convDateTime = "N/A";
                            if (prop.GetValue(PDFResult) != null)
                                convDateTime = comm.convertDateTime(Convert.ToDateTime(prop.GetValue(PDFResult)));
                            keyValues.Add(prop.Name, (!string.IsNullOrEmpty(Convert.ToString(convDateTime)) ? (Convert.ToString(convDateTime)) : "N/A"));
                        }
                        else
                        {
                            keyValues.Add(prop.Name, (!string.IsNullOrEmpty(Convert.ToString(prop.GetValue(PDFResult))) ? (Convert.ToString(prop.GetValue(PDFResult))) : "N/A"));
                        }
                        if (prop.Name == "CountryName")
                            country = (!string.IsNullOrEmpty(Convert.ToString(prop.GetValue(PDFResult))) ? (Convert.ToString(prop.GetValue(PDFResult))) : "N/A");
                    }
                }
                string fileName = "AAI 033_ GCAA Response to Draft Reports_" + Convert.ToString(DateTime.Now.ToFileTime()) + ".docx";
                string destinationFile = Server.MapPath(Path.Combine("~/App_data", fileName));
                MemoryStream fileMem = getFile(keyValues, null, null, sourceFile, destinationFile, "F46");
                fileMem.Seek(0, SeekOrigin.Begin);
                System.IO.File.Delete(destinationFile);
                return File(fileMem, "application/octet-stream", fileName);
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("FormsController", "Form33", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("NotFound", "Error");
            }
            // return View();
        }
        /// <summary>
        /// Investigation Preliminary Report downloading as doc format 
        /// </summary> 
        /// 
        /// <param name="Prereport">Receive the investigation file id.</param>
        //commented post method as client said it is not required 
        //[HttpPost]
        public ActionResult PrelimReport(int Prereport = 0)
        {
            try
            {
                string status = string.Empty;
                string sourceFile = Server.MapPath(Path.Combine("~/App_data", "PreliminaryReport.dotx"));
                string country = string.Empty;
                Dictionary<string, string> keyValues = new Dictionary<string, string>();
                Dictionary<string, List<string>> keyJSONValues = new Dictionary<string, List<string>>();
                var PrelimReportvalues = dc.USP_VIEW_INVESTIGATION_REPORT_PRELIM(Prereport).ToList();
                var PrelimReportAircraftvalues = dc.USP_VIEW_INVESTIGATION_AIRCRAFT_DETAIL_REPORT_PRELIM(Prereport).ToList();
                var PrelimEventsJSON = dc.USP_VIEW_EVENTSJSON_REPORTS(Prereport, "e1,e3,e6,e7,e8,e11,e14,e15,e18,e21,e22,e23,e24,e27,e29,e42,e45,e46").ToList();
                //dc.USP_UPDT_INVDETAILS_PRELIM(Prereport.Ongoing_Investigation_Activitiea, Prereport);

                foreach (USP_VIEW_INVESTIGATION_REPORT_PRELIM_Result PDFResult in PrelimReportvalues)
                {
                    Type type = PDFResult.GetType();
                    PropertyInfo[] props = type.GetProperties();
                    string[] str = new string[50];
                    foreach (var prop in props)
                    {
                        if (prop.PropertyType.FullName.Contains("DateTime"))
                        {
                            string convDateTime = "N/A";
                            if (prop.GetValue(PDFResult) != null)
                                convDateTime = comm.convertDateTime(Convert.ToDateTime(prop.GetValue(PDFResult)));
                            keyValues.Add(prop.Name, (!string.IsNullOrEmpty(Convert.ToString(convDateTime)) ? (Convert.ToString(convDateTime)) : " "));
                        }
                        else
                        {
                            keyValues.Add(prop.Name, (!string.IsNullOrEmpty(Convert.ToString(prop.GetValue(PDFResult))) ? (Convert.ToString(prop.GetValue(PDFResult))) : " "));
                        }
                    }
                    keyValues.Add("CURDATE", (DateTime.Today).ToString("dd-MMMM-yyyy"));
                    //keyValues.Add("InvOngoingActivities", Prereport.Ongoing_Investigation_Activitiea);
                }
                string[] keys = new string[100];
                string eventNo = string.Empty;
                string fileName = "AAI Preliminary_" + Convert.ToString(DateTime.Now.ToFileTime()) + ".docx";
                string destinationFile = Server.MapPath(Path.Combine("~/App_data", fileName));
                foreach (USP_VIEW_EVENTSJSON_REPORTS_Result PDFJSONResult in PrelimEventsJSON)
                {
                    Type type = PDFJSONResult.GetType();
                    PropertyInfo[] props = type.GetProperties();
                    //List<string> keys = new List<string>();
                    foreach (var prop in props)
                    {
                        if (prop.Name == "InvestigationTaskGroupEventUniqueNumber")
                        {
                            if (!string.IsNullOrEmpty(Convert.ToString(prop.GetValue(PDFJSONResult))))
                            {
                                eventNo = Convert.ToString(prop.GetValue(PDFJSONResult));
                                if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e1")
                                {
                                    keys = new string[] { "reportflighthistory.text", "reportcrewmembers.text", "reportPassengers.text", "reportothers.text", "reportInvestigationProcess.text", "reportNameofownertxtbox.text", "reportOrganizationalandManagement.text", "reportAdditionalInformation.text", "reportreportpublishedlinktxtbox.text" };
                                }
                                else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e3")
                                {
                                    keys = new string[] { "reportflightpath.text","reportdateofmanufacturetxtbox.text", "reportManufacturertxtbox.text", "reportflighthourscyclestxtbox.text", "reportflightpathdiagram", "reportcabincrew.text" ,
                                                        "pilotage-trtxtbox.text","pilotgender-trtxtbox.text","pilotage-cotrtxtbox.text","pilotgender-cotrtxtbox.text",
                                                        "typeoflicn-trtxtbox.text","validto-trtxtbox.text","rating-trtxtbox.text","totalflytm-trtxtbox.text","totalontype-trtxtbox.text",
                                                        "totallastdays-trtxtbox.text","totalontypelast90days-trtxtbox.text","totalontypelast7days-trtxtbox.text","totalontypelast24hours-trtxtbox.text",
                                                        "lastrectrng-trtxtbox.text","lastprofcheck-trtxtbox.text","lastlinecheck-trtxtbox.text","routeairdrmrecncy-trtxtbox.text","medicalclass-trtxtbox.text","validtomedical-trtxtbox.text","medicallimit-trtxtbox.text",
                                                        "typeoflicn-cotrtxtbox.text","pilotvalidto-cotrtxtbox.text","rating-cotrtxtbox.text",
                                                        "totalflytm-cotrtxtbox.text","totalontype-cotrtxtbox.text","totallastdays-cotrtxtbox.text",
                                                        "totalontypelast90days-cotrtxtbox.text","totalontypelast7days-cotrtxtbox.text","totalontypelast24hours-cotrtxtbox.text","lastrectrng-cotrtxtbox.text","lastprofcheck-cotrtxtbox.text","lastlinecheck-cotrtxtbox.text","routeairdrmrecncy-cotrtxtbox.text","medicalclass-cotrtxtbox.text","validtomedical-cotrtxtbox.text","medicallimit-cotrtxtbox.text"};
                                }
                                else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e6")
                                {
                                    keys = new string[] { "reportFlightRecordersdetails.text" };
                                }
                                else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e7")
                                {
                                    keys = new string[] { "reportConclusion7.text" };
                                }
                                else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e8")
                                {
                                    keys = new string[] { "reportairportpersonnel.text","reportAirtrafficcontrollers.text","reportAerodromeInformation.text","reportAerodromecode(ICAO/IATA)txtbox.text","reportAirportnametxtbox.text","reportAirportaddresstxtbox.text",
                                                      "reportAirportclasstxtbox.text","reportAirportauthoritytxtbox.text","reportAirportservicetxtbox.text","reportTypeoftrafficpermittedtxtbox.text","reportCoordinatestxtbox.text","reportElevationreferencetemperaturetxtbox.text",
                                                      "reportRunwaylengthtxtbox.text","reportRunwaywidthtxtbox.text","reportStopwaytxtbox.text","reportRunwayendsafetyareatxtbox.text","reportAzimuthtxtbox.text","reportCategoryFirefightingtxtbox.text"};
                                }
                                else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e11")
                                {
                                    keys = new string[] {"reportCertificateofAirworthinesstxtbox.text","reportValidtoCertificateofAirworthinesstxtbox.text","reportCertificateofRegistrationtxtbox.text","reportValidtoCertificateofRegistrationtxtbox.text","reportTotalHoursSinceNewtxtbox.text",
                                                     "reportTotalCyclesSinceNewtxtbox.text","reportTotalHoursSinceOverhaultxtbox.text","reportTotalCyclesSinceOverhaultxtbox.text","reportTotalHoursSinceLastInspectiontxtbox.text","reportTotalCyclesSinceLastInspectiontxtbox.text"};
                                }
                                else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e14")
                                {
                                    keys = new string[] {"reportPropellerTypetxtbox.text","reportPropellerManufacturertxtbox.text","reportManufacturertypetxtbox.text",
                                                     "reportPropellernumberone(Left)txtbox.text","reportleftSerialNumbertxtbox.text","reportleftTotalTimeSinceNewtxtbox.text","reportleftTotalTimeSinceOverhaultxtbox.text",
                                                     "reportPropellernumbertwo(Right)txtbox.text","reportRightSerialNumbertxtbox.text","reportrightTotalTimeSinceNewtxtbox.text","reportrightTotalTimeSinceOverhaultxtbox.text",
                                                     "reportEngineTypetxtbox.text","reportEngineManufacturertxtbox.text","reportEngineManufacturerTypetxtbox.text",
                                                     "reportEnginenumberone(Left)txtbox.text","reportEngineleftSerialNumbertxtbox.text","reportEngineleftTotalTimeSinceNewtxtbox.text","reportEngineleftTotalTimeSinceOverhaultxtbox.text",
                                                     "reportEnginenumberone(Right)txtbox.text","reportEngineRightSerialNumbertxtbox.text","reportEnginerightTotalTimeSinceNewtxtbox.text","reportEnginerightTotalTimeSinceOverhaultxtbox.text","reportConclusion14.text"};
                                }
                                else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e15")
                                {
                                    keys = new string[] { "reportWreckageDistribution.text" };
                                }
                                else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e18")
                                {
                                    keys = new string[] { "reportCrewmemberMedical.text" };
                                }
                                else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e21")
                                {
                                    keys = new string[] { "reportConclusion21.text", "reportReviewWeatherDoc.text" };
                                }
                                else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e22")
                                {
                                    keys = new string[] { "reportATCcommunication.text" };
                                }
                                else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e23")
                                {
                                    keys = new string[] { "reportEvacuationProcess.text", "reportPassengerbehavior.text" };
                                }
                                else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e24")
                                {
                                    keys = new string[] { "reportAircraftCabinData.text", "reportCabinLayoutDiagram" };
                                }
                                else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e27")
                                {
                                    keys = new string[] { "reportInflightFire.text", "reportGroundFire.text" };
                                }
                                else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e29")
                                {
                                    keys = new string[] { "reportDamagetoAerodrome.text", "reportRunwaydamage.text", "reportDamagetonavigation.text", "reportDamagetoenvironment.text", "reportAircraftInitialimpact.text", "reportAircraftInitialimpactdiagram" };
                                }
                                else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e42")
                                {
                                    keys = new string[] { "reportMaximumtakeoffweighttxtbox.text", "reportMaximumLandingweighttxtbox.text", "reportMaximumzerofuelweighttxtbox.text", "reportTakeoffweighttxtbox.text", "reportLandingweighttxtbox.text", "reportZeroweighttxtbox.text" };
                                }
                                else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e45")
                                {
                                    keys = new string[] { "reportConclusion45.text" };
                                }
                                else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e46")
                                {
                                    keys = new string[] { "reportFireCrew.text", "reportARFFSresponse.text" };
                                }
                                else
                                {
                                    keys = null;
                                }
                            }
                            else
                            {
                                keys = null;
                            }
                        }
                        if (prop.Name == "InvestigationTaskDataInJSON")
                        {
                            if (!string.IsNullOrEmpty(Convert.ToString(prop.GetValue(PDFJSONResult))))
                            {
                                keyValues = getOtherValuesfromEventJSON(keys, eventNo, Convert.ToString(prop.GetValue(PDFJSONResult)), keyValues, "prelim");
                            }
                            else
                            {
                                if (keys != null)
                                {
                                    if (keys.Length > 0)
                                    {
                                        for (int ii = 0; ii < keys.Length; ii++)
                                        {
                                            if (keys[ii].Contains("txtbox") || keys[ii].ToLower().Contains("diagram"))
                                                keyValues.Add(keys[ii], " ");
                                            if ((!keys[ii].Contains("txtbox")) && !(keys[ii].ToLower().Contains("diagram")))
                                                keyValues.Add(keys[ii], "This information will be added in future or in Final Report.");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (keyValues.Keys.Contains("reportPilotinCommand.text"))
                {
                    if ((keyValues["reportPilotinCommand.text"] == "This information will be added in future or in Final Report.") && (keyValues["reportcopilot.text"] == "This information will be added in future or in Final Report.") && (keyValues["reportcabincrew.text"] == "This information will be added in future or in Final Report.") && (keyValues["reportairportpersonnel.text"] == "This information will be added in future or in Final Report.") && keyValues["reportAirtrafficcontrollers.text"] == "This information will be added in future or in Final Report." && (keyValues["reportFireCrew.text"] == "This information will be added in future or in Final Report."))
                    {
                        keyValues["reportPilotinCommand.text"] = "This information will be added in future or in Final Report.";
                        keyValues["reportairportpersonnel.text"] = keyValues["reportcopilot.text"] = keyValues["reportcabincrew.text"] = keyValues["reportFireCrew.text"] = keyValues["reportAirtrafficcontrollers.text"] = " ";
                    }
                    else
                    {
                        if ((keyValues["reportPilotinCommand.text"] == "This information will be added in future or in Final Report."))
                        {
                            keyValues["reportPilotinCommand.text"] = " ";
                        }
                        if ((keyValues["reportcopilot.text"] == "This information will be added in future or in Final Report."))
                        {
                            keyValues["reportcopilot.text"] = " ";
                        }
                        if ((keyValues["reportcabincrew.text"] == "This information will be added in future or in Final Report."))
                        {
                            keyValues["reportcabincrew.text"] = " ";
                        }
                        if ((keyValues["reportairportpersonnel.text"] == "This information will be added in future or in Final Report."))
                        {
                            keyValues["reportairportpersonnel.text"] = " ";
                        }
                        if ((keyValues["reportAirtrafficcontrollers.text"] == "This information will be added in future or in Final Report."))
                        {
                            keyValues["reportAirtrafficcontrollers.text"] = " ";
                        }
                        if ((keyValues["reportFireCrew.text"] == "This information will be added in future or in Final Report."))
                        {
                            keyValues["reportFireCrew.text"] = " ";
                        }
                    }
                }
                if (keyValues.Keys.Contains("reportWreckageDistribution.text") && keyValues.Keys.Contains("reportFlightRecordersdetails.text"))
                {
                    if ((keyValues["reportWreckageDistribution.text"] == "This information will be added in future or in Final Report.") && (keyValues["reportFlightRecordersdetails.text"] == "This information will be added in future or in Final Report."))
                    {
                        keyValues["reportWreckageDistribution.text"] = "This information will be added in future or in Final Report.";
                        keyValues["reportFlightRecordersdetails.text"] = " ";
                    }
                    else if ((keyValues["reportWreckageDistribution.text"] != "This information will be added in future or in Final Report.") && (keyValues["reportFlightRecordersdetails.text"] == "This information will be added in future or in Final Report."))
                    {
                        keyValues["reportFlightRecordersdetails.text"] = " ";
                    }
                    else if ((keyValues["reportFlightRecordersdetails.text"] != "This information will be added in future or in Final Report.") && (keyValues["reportWreckageDistribution.text"] == "This information will be added in future or in Final Report."))
                    {
                        keyValues["reportWreckageDistribution.text"] = " ";
                    }
                }

                if (keyValues.Keys.Contains("reportInflightFire.text") && keyValues.Keys.Contains("reportGroundFire.text"))
                {

                    if ((keyValues["reportInflightFire.text"] == "This information will be added in future or in Final Report.") && (keyValues["reportGroundFire.text"] == "This information will be added in future or in Final Report."))
                    {
                        keyValues["reportInflightFire.text"] = "This information will be added in future or in Final Report.";
                        keyValues["reportGroundFire.text"] = " ";
                    }
                    else if ((keyValues["reportInflightFire.text"] != "This information will be added in future or in Final Report.") && (keyValues["reportGroundFire.text"] == "This information will be added in future or in Final Report."))
                    {
                        keyValues["reportGroundFire.text"] = " ";
                    }
                    else if ((keyValues["reportGroundFire.text"] != "This information will be added in future or in Final Report.") && (keyValues["reportInflightFire.text"] == "This information will be added in future or in Final Report."))
                    {
                        keyValues["reportInflightFire.text"] = " ";
                    }
                }
                if (keyValues.Keys.Contains("reportcrewmembers.text") && keyValues.Keys.Contains("reportPassengers.text") && keyValues.Keys.Contains("reportothers.text"))
                {
                    if ((keyValues["reportcrewmembers.text"] == "This information will be added in future or in Final Report.") && (keyValues["reportPassengers.text"] == "This information will be added in future or in Final Report.") && (keyValues["reportothers.text"] == "This information will be added in future or in Final Report."))
                    {
                        keyValues["reportInjDet"] = "This information will be added in future or in Final Report.";
                        keyValues["reportcrewmembers.text"] = keyValues["reportPassengers.text"] = keyValues["reportothers.text"] = " ";
                    }
                    else
                    {
                        keyValues["reportInjDet"] = " ";
                        // keyValues["reportPassengers.text"] = keyValues["reportothers.text"] = " ";
                    }
                }
                if (keyValues.Keys.Contains("reportConclusion7.text") && keyValues.Keys.Contains("reportConclusion21.text") && keyValues.Keys.Contains("reportReviewWeatherDoc.text"))
                {
                    if ((keyValues["reportConclusion7.text"] == "This information will be added in future or in Final Report.") && (keyValues["reportConclusion21.text"] == "This information will be added in future or in Final Report.") && (keyValues["reportReviewWeatherDoc.text"] == "This information will be added in future or in Final Report."))
                    {
                        keyValues["reportConclusion7.text"] = "This information will be added in future or in Final Report.";
                        keyValues["reportReviewWeatherDoc.text"] = keyValues["reportConclusion21.text"] = " ";
                    }
                    else if ((keyValues["reportConclusion7.text"] != "This information will be added in future or in Final Report.") && (keyValues["reportReviewWeatherDoc.text"] == "This information will be added in future or in Final Report.") && (keyValues["reportConclusion21.text"] == "This information will be added in future or in Final Report."))
                    {
                        keyValues["reportReviewWeatherDoc.text"] = keyValues["reportConclusion21.text"] = " ";
                    }
                    else if ((keyValues["reportConclusion21.text"] != "This information will be added in future or in Final Report.") && (keyValues["reportReviewWeatherDoc.text"] == "This information will be added in future or in Final Report.") && (keyValues["reportConclusion21.text"] == "This information will be added in future or in Final Report."))
                    {
                        keyValues["reportReviewWeatherDoc.text"] = keyValues["reportConclusion7.text"] = " ";
                    }
                    else if ((keyValues["reportReviewWeatherDoc.text"] != "This information will be added in future or in Final Report.") && (keyValues["reportConclusion21.text"] == "This information will be added in future or in Final Report.") && (keyValues["reportConclusion7.text"] == "This information will be added in future or in Final Report."))
                    {
                        keyValues["reportConclusion21.text"] = keyValues["reportConclusion7.text"] = " ";
                    }
                }
                Dictionary<string, List<string>> keyValuesAirDet = new Dictionary<string, List<string>>();
                if (PrelimReportAircraftvalues != null)
                {
                    foreach (USP_VIEW_INVESTIGATION_AIRCRAFT_DETAIL_REPORT_PRELIM_Result PDFAirResult in PrelimReportAircraftvalues)
                    {
                        Type type = PDFAirResult.GetType();
                        PropertyInfo[] props = type.GetProperties();
                        //string str = "{";
                        string[] str = new string[50];
                        foreach (var prop in props)
                        {

                            List<string> list = new List<string>();
                            if (keyValuesAirDet.ContainsKey(prop.Name))
                                keyValuesAirDet[prop.Name].Add(!string.IsNullOrEmpty(Convert.ToString(prop.GetValue(PDFAirResult))) ? (Convert.ToString(prop.GetValue(PDFAirResult))) : "N/A");
                            else
                            {
                                keyValuesAirDet.Add(prop.Name, list);
                                list.Add(!string.IsNullOrEmpty(Convert.ToString(prop.GetValue(PDFAirResult))) ? (Convert.ToString(prop.GetValue(PDFAirResult))) : "N/A");
                            }
                        }
                    }
                }
                MemoryStream fileMem = getFile(keyValues, keyValuesAirDet, null, sourceFile, destinationFile, "Rep-PRE");
                fileMem.Seek(0, SeekOrigin.Begin);
                System.IO.File.Delete(destinationFile);
                return File(fileMem, "application/octet-stream", fileName);
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("FormsController", "PrelimReport", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("NotFound", "Error");
            }
            // return View();
        }
        /// <summary>
        /// Investigation Final Report downloading as doc format 
        /// </summary> 
        /// 
        /// <param name="id">Receive the investigation file id.</param>
        public ActionResult FinalReport(int id = 0)
        {
            try
            {
                string status = string.Empty;
                string sourceFile = Server.MapPath(Path.Combine("~/App_data", "FinalReport.dotx"));
                string country = string.Empty;
                Dictionary<string, string> keyValues = new Dictionary<string, string>();
                Dictionary<string, List<string>> keyJSONValues = new Dictionary<string, List<string>>();
                var PrelimReportvalues = dc.USP_VIEW_INVESTIGATION_REPORT_PRELIM(id).ToList();
                var PrelimReportAircraftvalues = dc.USP_VIEW_INVESTIGATION_AIRCRAFT_DETAIL_REPORT_PRELIM(id).ToList();
                var PrelimEventsJSON = dc.USP_VIEW_EVENTSJSON_REPORTS(id, "e1,e3,e6,e7,e8,e11,e14,e15,e18,e21,e22,e23,e24,e27,e29,e42,e45,e46,e2,e64,e65,e66").ToList();
                foreach (USP_VIEW_INVESTIGATION_REPORT_PRELIM_Result PDFResult in PrelimReportvalues)
                {
                    Type type = PDFResult.GetType();
                    PropertyInfo[] props = type.GetProperties();
                    string[] str = new string[50];
                    foreach (var prop in props)
                    {
                        if (prop.PropertyType.FullName.Contains("DateTime"))
                        {
                            string convDateTime = "N/A";
                            if (prop.GetValue(PDFResult) != null)
                                convDateTime = comm.convertDateTime(Convert.ToDateTime(prop.GetValue(PDFResult)));
                            keyValues.Add(prop.Name, (!string.IsNullOrEmpty(Convert.ToString(convDateTime)) ? (Convert.ToString(convDateTime)) : " "));
                        }
                        else
                        {
                            keyValues.Add(prop.Name, (!string.IsNullOrEmpty(Convert.ToString(prop.GetValue(PDFResult))) ? (Convert.ToString(prop.GetValue(PDFResult))) : " "));
                        }
                    }
                    //footer current date
                    keyValues.Add("CURDATE", (DateTime.Today).ToString("dd-MMMM-yyyy"));
                }
                string[] keys = new string[100];
                string eventNo = string.Empty;
                string fileName = "AAI Final_" + Convert.ToString(DateTime.Now.ToFileTime()) + ".docx";
                string destinationFile = Server.MapPath(Path.Combine("~/App_data", fileName));
                foreach (USP_VIEW_EVENTSJSON_REPORTS_Result PDFJSONResult in PrelimEventsJSON)
                {
                    Type type = PDFJSONResult.GetType();
                    PropertyInfo[] props = type.GetProperties();
                    //List<string> keys = new List<string>();
                    foreach (var prop in props)
                    {
                        if (prop.Name == "InvestigationTaskGroupEventUniqueNumber")
                        {
                            if (!string.IsNullOrEmpty(Convert.ToString(prop.GetValue(PDFJSONResult))))
                            {
                                eventNo = Convert.ToString(prop.GetValue(PDFJSONResult));
                                if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e1")
                                {
                                    keys = new string[] { "reportflighthistory.text", "reportcrewmembers.text", "reportPassengers.text", "reportothers.text", "reportInvestigationProcess.text", "reportNameofownertxtbox.text", "reportOrganizationalandManagement.text", "reportAdditionalInformation.text", "reportreportpublishedlinktxtbox.text" };
                                }
                                else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e3")
                                {
                                    keys = new string[] { "reportflightpath.text","reportdateofmanufacturetxtbox.text", "reportManufacturertxtbox.text", "reportflighthourscyclestxtbox.text", "reportflightpathdiagram", "reportcabincrew.text" ,
                                                        "pilotage-trtxtbox.text","pilotgender-trtxtbox.text","pilotage-cotrtxtbox.text","pilotgender-cotrtxtbox.text",
                                                        "typeoflicn-trtxtbox.text","validto-trtxtbox.text","rating-trtxtbox.text","totalflytm-trtxtbox.text","totalontype-trtxtbox.text",
                                                        "totallastdays-trtxtbox.text","totalontypelast90days-trtxtbox.text","totalontypelast7days-trtxtbox.text","totalontypelast24hours-trtxtbox.text",
                                                        "lastrectrng-trtxtbox.text","lastprofcheck-trtxtbox.text","lastlinecheck-trtxtbox.text","routeairdrmrecncy-trtxtbox.text","medicalclass-trtxtbox.text","validtomedical-trtxtbox.text","medicallimit-trtxtbox.text",
                                                        "typeoflicn-cotrtxtbox.text","pilotvalidto-cotrtxtbox.text","rating-cotrtxtbox.text",
                                                        "totalflytm-cotrtxtbox.text","totalontype-cotrtxtbox.text","totallastdays-cotrtxtbox.text",
                                                        "totalontypelast90days-cotrtxtbox.text","totalontypelast7days-cotrtxtbox.text","totalontypelast24hours-cotrtxtbox.text","lastrectrng-cotrtxtbox.text","lastprofcheck-cotrtxtbox.text","lastlinecheck-cotrtxtbox.text","routeairdrmrecncy-cotrtxtbox.text","medicalclass-cotrtxtbox.text","validtomedical-cotrtxtbox.text","medicallimit-cotrtxtbox.text"};
                                }
                                else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e6")
                                {
                                    keys = new string[] { "reportFlightRecordersdetails.text" };
                                }
                                else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e7")
                                {
                                    keys = new string[] { "reportConclusion7.text" };
                                }
                                else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e8")
                                {
                                    keys = new string[] { "reportairportpersonnel.text","reportAirtrafficcontrollers.text","reportAerodromeInformation.text","reportAerodromecode(ICAO/IATA)txtbox.text","reportAirportnametxtbox.text","reportAirportaddresstxtbox.text",
                                                      "reportAirportclasstxtbox.text","reportAirportauthoritytxtbox.text","reportAirportservicetxtbox.text","reportTypeoftrafficpermittedtxtbox.text","reportCoordinatestxtbox.text","reportElevationreferencetemperaturetxtbox.text",
                                                      "reportRunwaylengthtxtbox.text","reportRunwaywidthtxtbox.text","reportStopwaytxtbox.text","reportRunwayendsafetyareatxtbox.text","reportAzimuthtxtbox.text","reportCategoryFirefightingtxtbox.text"};
                                }
                                //else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e11")
                                //{
                                //    keys = new string[] {"reportCertificateofAirworthinesstxtbox.text","reportValidtoCertificateofAirworthinesstxtbox.text","reportCertificateofRegistrationtxtbox.text","reportValidtoCertificateofRegistrationtxtbox.text","reportTotalHoursSinceNewtxtbox.text",
                                //                     "reportTotalCyclesSinceNewtxtbox.text","reportTotalHoursSinceOverhaultxtbox.text","reportTotalCyclesSinceOverhaultxtbox.text","reportTotalHoursSinceLastInspectiontxtbox.text","reportTotalCyclesSinceLastInspectiontxtbox.text"};
                                //}
                                else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e14")
                                {
                                    keys = new string[] {"reportPropellerTypetxtbox.text","reportPropellerManufacturertxtbox.text","reportManufacturertypetxtbox.text",
                                                     "reportPropellernumberone(Left)txtbox.text","reportleftSerialNumbertxtbox.text","reportleftTotalTimeSinceNewtxtbox.text","reportleftTotalTimeSinceOverhaultxtbox.text",
                                                     "reportPropellernumbertwo(Right)txtbox.text","reportRightSerialNumbertxtbox.text","reportrightTotalTimeSinceNewtxtbox.text","reportrightTotalTimeSinceOverhaultxtbox.text",
                                                     "reportEngineTypetxtbox.text","reportEngineManufacturertxtbox.text","reportEngineManufacturerTypetxtbox.text",
                                                     "reportEnginenumberone(Left)txtbox.text","reportEngineleftSerialNumbertxtbox.text","reportEngineleftTotalTimeSinceNewtxtbox.text","reportEngineleftTotalTimeSinceOverhaultxtbox.text",
                                                     "reportEnginenumberone(Right)txtbox.text","reportEngineRightSerialNumbertxtbox.text","reportEnginerightTotalTimeSinceNewtxtbox.text","reportEnginerightTotalTimeSinceOverhaultxtbox.text","reportConclusion14.text"};
                                }
                                else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e15")
                                {
                                    keys = new string[] { "reportWreckageDistribution.text" };
                                }
                                else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e18")
                                {
                                    keys = new string[] { "reportCrewmemberMedical.text" };
                                }
                                else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e21")
                                {
                                    keys = new string[] { "reportConclusion21.text", "reportReviewWeatherDoc.text" };
                                }
                                else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e22")
                                {
                                    keys = new string[] { "reportATCcommunication.text" };
                                }
                                else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e23")
                                {
                                    keys = new string[] { "reportEvacuationProcess.text", "reportPassengerbehavior.text" };
                                }
                                else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e24")
                                {
                                    keys = new string[] { "reportAircraftCabinData.text", "reportCabinLayoutDiagram" };
                                }
                                else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e27")
                                {
                                    keys = new string[] { "reportInflightFire.text", "reportGroundFire.text" };
                                }
                                else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e29")
                                {
                                    keys = new string[] { "reportDamagetoAerodrome.text", "reportRunwaydamage.text", "reportDamagetonavigation.text", "reportDamagetoenvironment.text", "reportAircraftInitialimpact.text", "reportAircraftInitialimpactdiagram" };
                                }
                                else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e42")
                                {
                                    keys = new string[] { "reportMaximumtakeoffweighttxtbox.text", "reportMaximumLandingweighttxtbox.text", "reportMaximumzerofuelweighttxtbox.text", "reportTakeoffweighttxtbox.text", "reportLandingweighttxtbox.text", "reportZeroweighttxtbox.text" };
                                }
                                else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e45")
                                {
                                    keys = new string[] { "reportConclusion45.text" };
                                }
                                else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e46")
                                {
                                    keys = new string[] { "reportFireCrew.text", "reportARFFSresponse.text" };
                                }
                                else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e2")
                                {
                                    keys = new string[] { "reportAdditionalInformation.text" };
                                }
                                else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e64")
                                {
                                    keys = new string[] { "reportAdditionalInformation.text" };
                                }
                                else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e65")
                                {
                                    keys = new string[] { "reportAdditionalInformation.text" };
                                }
                                else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e66")
                                {
                                    keys = new string[] { "reportAnalyseInformation.text", "reportAssembleFindings.text", "reportDetermineCauses.text", "reportSafetyRecommendations.text", "reportSynopsisClosure.text", "reportAdditionalInformation.text", "reportOtherfactors.text", "reportsafetyaction.text", "reportConclusion66.text" };
                                }
                                else
                                {
                                    keys = null;
                                }
                            }
                            else
                            {
                                keys = null;
                            }
                        }
                        if (prop.Name == "InvestigationTaskDataInJSON")
                        {
                            if (!string.IsNullOrEmpty(Convert.ToString(prop.GetValue(PDFJSONResult))))
                            {
                                keyValues = getOtherValuesfromEventJSON(keys, eventNo, Convert.ToString(prop.GetValue(PDFJSONResult)), keyValues, "final");
                            }
                            else
                            {
                                if (keys != null)
                                {
                                    //if assigned member doesnot take any action in the event (JSON is null)
                                    if (keys.Length > 0)
                                    {
                                        for (int ii = 0; ii < keys.Length; ii++)
                                        {
                                            if (keys[ii].ToLower() != "reportadditionalinformation.text")
                                            {
                                                //if (keys[ii].Contains("txtbox") || keys[ii].ToLower().Contains("diagram"))
                                                //    keyValues.Add(keys[ii], " ");
                                                //if ((!keys[ii].Contains("txtbox")) && !(keys[ii].ToLower().Contains("diagram")))
                                                keyValues.Add(keys[ii], " ");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (!keyValues.Keys.Contains("reportAdditionalInformation.text"))
                    keyValues.Add("reportAdditionalInformation.text", AddInfo);
                if ((keyValues["reportcrewmembers.text"] == " ") && (keyValues["reportPassengers.text"] == " ") && (keyValues["reportothers.text"] == " "))
                {
                    keyValues["reportInjDet"] = " ";
                }
                else
                {
                    keyValues["reportInjDet"] = " ";
                }
                Dictionary<string, List<string>> keyValuesAirDet = new Dictionary<string, List<string>>();
                if (PrelimReportAircraftvalues != null)
                {
                    foreach (USP_VIEW_INVESTIGATION_AIRCRAFT_DETAIL_REPORT_PRELIM_Result PDFAirResult in PrelimReportAircraftvalues)
                    {
                        Type type = PDFAirResult.GetType();
                        PropertyInfo[] props = type.GetProperties();
                        string[] str = new string[50];
                        foreach (var prop in props)
                        {

                            List<string> list = new List<string>();
                            if (keyValuesAirDet.ContainsKey(prop.Name))
                                keyValuesAirDet[prop.Name].Add(!string.IsNullOrEmpty(Convert.ToString(prop.GetValue(PDFAirResult))) ? (Convert.ToString(prop.GetValue(PDFAirResult))) : "N/A");
                            else
                            {
                                keyValuesAirDet.Add(prop.Name, list);
                                list.Add(!string.IsNullOrEmpty(Convert.ToString(prop.GetValue(PDFAirResult))) ? (Convert.ToString(prop.GetValue(PDFAirResult))) : "N/A");
                            }
                        }
                    }
                }

                MemoryStream fileMem = getFile(keyValues, keyValuesAirDet, null, sourceFile, destinationFile, "Rep-PRE");


                fileMem.Seek(0, SeekOrigin.Begin);
                System.IO.File.Delete(destinationFile);
                return File(fileMem, "application/octet-stream", fileName);
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("FormsController", "FinalRep", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("NotFound", "Error");
            }
            // return View();
        }
        /// <summary>
        /// Investigation Closure Report downloading as doc format 
        /// </summary> 
        /// 
        /// <param name="id">Receive the investigation file id.</param>
        public ActionResult ClosureReport(int id = 0)
        {
            try
            {
                string status = string.Empty;
                string sourceFile = Server.MapPath(Path.Combine("~/App_data", "Closure_Report.docx"));
                string country = string.Empty;

                Dictionary<string, string> keyValues = new Dictionary<string, string>();
                Dictionary<string, List<string>> keyJSONValues = new Dictionary<string, List<string>>();

                var ClosureReportvalues = dc.USP_VIEW_INVESTIGATION_DETAIL_FOR_CLOSURE_REPORT(id).ToList();

                var ClosureEventsJSON = dc.USP_VIEW_EVENTSJSON_REPORTS(id, "e1,e66").ToList();

                foreach (USP_VIEW_INVESTIGATION_DETAIL_FOR_CLOSURE_REPORT_Result PDFResult in ClosureReportvalues)
                {
                    Type type = PDFResult.GetType();
                    PropertyInfo[] props = type.GetProperties();
                    string[] str = new string[50];

                    foreach (var prop in props)
                    {
                        if (prop.PropertyType.FullName.Contains("DateTime"))
                        {
                            string convDateTime = "N/A";
                            if (prop.GetValue(PDFResult) != null)
                                convDateTime = comm.convertDateTime(Convert.ToDateTime(prop.GetValue(PDFResult)));
                            keyValues.Add(prop.Name, (!string.IsNullOrEmpty(Convert.ToString(convDateTime)) ? (Convert.ToString(convDateTime)) : "N/A"));
                        }
                        else
                        {
                            keyValues.Add(prop.Name, (!string.IsNullOrEmpty(Convert.ToString(prop.GetValue(PDFResult))) ? (Convert.ToString(prop.GetValue(PDFResult))) : "N/A"));
                        }
                    }
                    keyValues.Add("CURDATE", (DateTime.Today).ToString("dd-MMMM-yyyy"));
                }

                string[] keys = new string[100];

                foreach (USP_VIEW_EVENTSJSON_REPORTS_Result PDFJSONResult in ClosureEventsJSON)
                {
                    Type type = PDFJSONResult.GetType();
                    PropertyInfo[] props = type.GetProperties();

                    foreach (var prop in props)
                    {
                        if (prop.Name == "InvestigationTaskGroupEventUniqueNumber")
                        {
                            if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e1")
                            {
                                keys = new string[] { "reportflighthistory.text", "reportInvestigationProcess.text" };
                            }
                            else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e66")
                            {
                                keys = new string[] { "reportSynopsisClosure.text" };
                            }
                            else
                            {
                                keys = null;
                            }
                        }
                        if (prop.Name == "InvestigationTaskDataInJSON")
                        {
                            if (!string.IsNullOrEmpty(Convert.ToString(prop.GetValue(PDFJSONResult))))
                            {
                                keyValues = getOtherValuesfromEventJSON(keys, "", Convert.ToString(prop.GetValue(PDFJSONResult)), keyValues, "Closure");
                            }
                            else
                            {
                                if (keys != null)
                                {
                                    if (keys.Length > 0)
                                    {
                                        for (int ii = 0; ii < keys.Length; ii++)
                                        {
                                            //if (keys[ii].Contains("txtbox") || keys[ii].ToLower().Contains("diagram"))
                                            //    keyValues.Add(keys[ii], " ");
                                            //if ((!keys[ii].Contains("txtbox")) && !(keys[ii].ToLower().Contains("diagram")))
                                            keyValues.Add(keys[ii], " ");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                string fileName = "AAI Closure_" + Convert.ToString(DateTime.Now.ToFileTime()) + ".docx";
                string destinationFile = Server.MapPath(Path.Combine("~/App_data", fileName));
                MemoryStream fileMem = getFile(keyValues, null, null, sourceFile, destinationFile, "Rep-PRE");
                fileMem.Seek(0, SeekOrigin.Begin);
                System.IO.File.Delete(destinationFile);
                return File(fileMem, "application/octet-stream", fileName);
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("FormsController", "ClosureReport", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("NotFound", "Error");
            }

            // return View();
        }
        /// <summary>
        /// Investigation InterimRport Report downloading as doc format 
        /// </summary> 
        /// 
        //commented post method as client says it is not required 
        //[HttpPost]
        ///<param name="Intreport">Receive the investigation file id.</param>
        public ActionResult InterimRport(int Intreport)
        {
            //string invprocess = Intreport.Investigation_Process;
            //string interim_stmt = Intreport.Interim_statement;
            //int? invfile_id = Intreport.InvestigationFileID;
            //dc.USP_UPDT_INVDETAILS_INTERIM(invprocess, interim_stmt, invfile_id);
            int invfile_id = Intreport;
            try
            {
                string status = string.Empty;
                string sourceFile = Server.MapPath(System.IO.Path.Combine("~/App_data", "InterimAirAccidentInvestigationreport_1.dotx"));
                string country = string.Empty;
                Dictionary<string, string> keyValues = new Dictionary<string, string>();
                Dictionary<string, List<string>> keyJSONValues = new Dictionary<string, List<string>>();
                var PrelimReportvalues = dc.USP_VIEW_INVESTIGATION_REPORT_INTERIM(invfile_id).ToList();
                var PrelimReportAircraftvalues = dc.USP_VIEW_INVESTIGATION_AIRCRAFT_DETAIL_REPORT_PRELIM(invfile_id).ToList();
                DateTime start = DateTime.Parse(PrelimReportvalues[0].UTCDATEOCC);
                DateTime End = DateTime.Now;
                String Count = string.Empty;
                TimeSpan diff = End - start;

                if (Convert.ToInt32(diff.Days) <= 365)
                {
                    Count = "First";
                }
                else if (Convert.ToInt32(diff.Days) > 365 && Convert.ToInt32(diff.Days) <= 730)
                {
                    Count = "Second";
                }
                else if (Convert.ToInt32(diff.Days) > 730 && Convert.ToInt32(diff.Days) <= 1095)
                {
                    Count = "Third";
                }

                else if (Convert.ToInt32(diff.Days) > 1095 && Convert.ToInt32(diff.Days) <= 1460)
                {
                    Count = "Fourth";
                }
                else if (Convert.ToInt32(diff.Days) > 1460 && Convert.ToInt32(diff.Days) <= 1825)
                {
                    Count = "Fifth";
                }
                else if (Convert.ToInt32(diff.Days) > 1825 && Convert.ToInt32(diff.Days) <= 2190)
                {
                    Count = "Sixth";
                }
                else if (Convert.ToInt32(diff.Days) > 2190 && Convert.ToInt32(diff.Days) <= 2555)
                {
                    Count = "Seventh";
                }
                else if (Convert.ToInt32(diff.Days) > 2555 && Convert.ToInt32(diff.Days) <= 2920)
                {
                    Count = "Eight";
                }
                else if (Convert.ToInt32(diff.Days) > 2920 && Convert.ToInt32(diff.Days) <= 3285)
                {
                    Count = "Nineth";
                }
                else if (Convert.ToInt32(diff.Days) > 3285 && Convert.ToInt32(diff.Days) <= 3650)
                {
                    Count = "Tenth";
                }


                var PrelimEventsJSON = dc.USP_VIEW_EVENTSJSON_REPORTS(invfile_id, "e1,e3,e6,e7,e8,e11,e14,e15,e18,e21,e22,e23,e24,e27,e29,e42,e45,e46").ToList();
                foreach (USP_VIEW_INVESTIGATION_REPORT_INTERIM_Result PDFResult in PrelimReportvalues)
                {
                    Type type = PDFResult.GetType();
                    PropertyInfo[] props = type.GetProperties();
                    string[] str = new string[50];
                    foreach (var prop in props)
                    {
                        if (prop.PropertyType.FullName.Contains("DateTime"))
                        {
                            string convDateTime = "N/A";
                            if (prop.GetValue(PDFResult) != null)
                                convDateTime = comm.convertDateTime(Convert.ToDateTime(prop.GetValue(PDFResult)));
                            keyValues.Add(prop.Name, (!string.IsNullOrEmpty(Convert.ToString(convDateTime)) ? (Convert.ToString(convDateTime)) : " "));
                        }
                        else
                        {
                            keyValues.Add(prop.Name, (!string.IsNullOrEmpty(Convert.ToString(prop.GetValue(PDFResult))) ? (Convert.ToString(prop.GetValue(PDFResult))) : " "));
                        }
                    }
                    keyValues.Add("CURDATE", (DateTime.Today).ToString("dd-MMMM-yyyy"));
                    keyValues.Add("TIMES", Count);
                }
                string[] keys = new string[100];

                foreach (USP_VIEW_EVENTSJSON_REPORTS_Result PDFJSONResult in PrelimEventsJSON)
                {
                    Type type = PDFJSONResult.GetType();
                    PropertyInfo[] props = type.GetProperties();
                    //List<string> keys = new List<string>();
                    foreach (var prop in props)
                    {
                        if (prop.Name == "InvestigationTaskGroupEventUniqueNumber")
                        {
                            if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e1")
                            {
                                keys = new string[] { "reportflighthistory.text", "reportcrewmembers.text", "reportPassengers.text", "reportothers.text", "reportInvestigationProcess.text", "reportNameofowner.text", "reportOrganizationalandManagement.text", "reportAdditionalInformation.text", "reportreportpublishedlinktxtbox.text" };
                            }
                            else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e3")
                            {
                                keys = new string[] { "reportflightpath.text", "reportdateofmanufacturetxtbox.text", "reportManufacturertxtbox.text", "reportflighthourscyclestxtbox.text", "reportPilotinCommand.text", "reportcopilot.text", "reportcabincrew.text" };
                            }
                            else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e6")
                            {
                                keys = new string[] { "reportFlightRecordersdetails.text" };
                            }
                            else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e7")
                            {
                                keys = new string[] { "reportConclusion7.text" };
                            }
                            else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e8")
                            {
                                keys = new string[] { "reportairportpersonnel.text","reportAirtrafficcontrollers.text","reportAerodromeInformation.text","reportAerodromecode(ICAO/IATA)txtbox.text","reportAirportnametxtbox.text","reportAirportaddresstxtbox.text",
                                                      "reportAirportclasstxtbox.text","reportAirportauthoritytxtbox.text","reportAirportservicetxtbox.text","reportTypeoftrafficpermittedtxtbox.text","reportCoordinatestxtbox.text","reportElevationreferencetemperaturetxtbox.text",
                                                      "reportRunwaylengthtxtbox.text","reportRunwaywidthtxtbox.text","reportStopwaytxtbox.text","report Runwayendsafetyareatxtbox.text","reportAzimuthtxtbox.text","reportCategoryFirefightingtxtbox.text"};
                            }
                            else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e11")
                            {
                                keys = new string[] {"reportCertificateofAirworthinesstxtbox.text","reportValid toCertificateofAirworthinesstxtbox.text","reportCertificateofRegistrationtxtbox.text","reportValidtoCertificateofRegistrationtxtbox.text","reportTotalHoursSinceNewtxtbox.text",
                                                     "reportTotalCyclesSinceNewtxtbox.text","reportTotalHoursSinceOverhaultxtbox.text","reportTotalCyclesSinceOverhaultxtbox.text","reportTotalHoursSinceLastInspectiontxtbox.text","reportTotalCyclesSinceLastInspectiontxtbox.text"};
                            }
                            else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e14")
                            {
                                keys = new string[] {"reportPropellerTypetxtbox.text","reportPropellerManufacturertxtbox.text","reportManufacturertypetxtbox.text",
                                                     "reportPropellernumberone(Left)txtbox.text","reportleftSerialNumbertxtbox.text","reportleftTotalTimeSinceNewtxtbox.text","reportleftTotalTimeSinceOverhaultxtbox.text",
                                                     "reportPropellernumbertwo(Right)txtbox.text","reportRightSerialNumbertxtbox.text","reportrightTotalTimeSinceNewtxtbox.text","reportrightTotalTimeSinceOverhaultxtbox.text",
                                                     "reportEngineTypetxtbox.text","reportEngineManufacturertxtbox.text","reportEngineManufacturerTypetxtbox.text",
                                                     "reportEnginenumberone(Left)txtbox.text","reportEngineleftSerialNumbertxtbox.text","reportEngineleftTotalTimeSinceNewtxtbox.text","reportEngineleftTotalTimeSinceOverhaultxtbox.text",
                                                     "reportEnginenumberone(Right)txtbox.text","reportEngineRightSerialNumbertxtbox.text","reportEnginerightTotalTimeSinceNewtxtbox.text","reportEnginerightTotalTimeSinceOverhaultxtbox.text","reportConclusion14.text"};
                            }
                            else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e15")
                            {
                                keys = new string[] { "reportWreckageDistribution.text" };
                            }
                            else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e18")
                            {
                                keys = new string[] { "reportCrewmemberMedical.text" };
                            }
                            else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e21")
                            {
                                keys = new string[] { "reportConclusion21.text", "reportReviewWeatherDoc.text" };
                            }
                            else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e22")
                            {
                                keys = new string[] { "reportATCcommunication.text" };
                            }
                            else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e23")
                            {
                                keys = new string[] { "reportEvacuationProcess.text", "reportPassengerbehavior.text" };
                            }
                            else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e24")
                            {
                                keys = new string[] { "reportAircraftCabinData.text", "prelimCabinLayoutDiagram" };
                            }
                            else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e27")
                            {
                                keys = new string[] { "reportInflightFire.text", "reportGroundFire.text" };
                            }
                            else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e29")
                            {
                                keys = new string[] { "reportDamagetoAerodrome.text", "reportRunwaydamage.text", "reportDamagetonavigation.text", "reportDamagetoenvironment.text", "reportAircraftInitialimpact.text", "reportAircraftInitialimpactdiagram" };
                            }
                            else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e42")
                            {
                                keys = new string[] { "reportMaximumtakeoffweight.text", "prelimMaximumLandingweight.text", "reportMaximumzerofuelweight.text", "reportTakeoffweight.text", "reportLandingweight.text", "reportZeroweight.text" };
                            }
                            else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e45")
                            {
                                keys = new string[] { "reportConclusion45.text" };
                            }
                            else if (Convert.ToString(prop.GetValue(PDFJSONResult)).ToLower() == "e46")
                            {
                                keys = new string[] { "reportFireCrew.text", "reportARFFSresponse.text" };
                            }
                            else
                            {
                                keys = null;
                            }
                        }
                        if (prop.Name == "InvestigationTaskDataInJSON")
                        {
                            if (!string.IsNullOrEmpty(Convert.ToString(prop.GetValue(PDFJSONResult))))
                            {
                                // keyValues = getOtherValuesfromEventJSON(keys, null, Convert.ToString(prop.GetValue(PDFResult)), keyValues, "Form-043");

                                keyValues = getOtherValuesfromEventJSON(keys, null, Convert.ToString(prop.GetValue(PDFJSONResult)), keyValues, "Interim");
                            }
                            else
                            {
                                if (keys != null)
                                {
                                    if (keys.Length > 0)
                                    {
                                        for (int ii = 0; ii < keys.Length; ii++)
                                        {
                                            if (keys[ii].Contains("txtbox"))
                                                keyValues.Add(keys[ii], " ");
                                            if (!keys[ii].Contains("txtbox"))
                                                keyValues.Add(keys[ii], "This information will be added in future or in Final Report");
                                        }
                                    }
                                }


                            }
                        }

                    }
                }
                string fileName = "AAI Interim_" + Convert.ToString(DateTime.Now.ToFileTime()) + ".docx";
                string destinationFile = Server.MapPath(Path.Combine("~/App_data", fileName));
                Dictionary<string, List<string>> keyValuesAirDet = new Dictionary<string, List<string>>();
                if (PrelimReportAircraftvalues != null)
                {
                    foreach (USP_VIEW_INVESTIGATION_AIRCRAFT_DETAIL_REPORT_PRELIM_Result PDFAirResult in PrelimReportAircraftvalues)
                    {
                        Type type = PDFAirResult.GetType();
                        PropertyInfo[] props = type.GetProperties();
                        //string str = "{";
                        string[] str = new string[50];
                        foreach (var prop in props)
                        {

                            List<string> list = new List<string>();
                            if (keyValuesAirDet.ContainsKey(prop.Name))
                                keyValuesAirDet[prop.Name].Add(!string.IsNullOrEmpty(Convert.ToString(prop.GetValue(PDFAirResult))) ? (Convert.ToString(prop.GetValue(PDFAirResult))) : "N/A");
                            else
                            {
                                keyValuesAirDet.Add(prop.Name, list);
                                list.Add(!string.IsNullOrEmpty(Convert.ToString(prop.GetValue(PDFAirResult))) ? (Convert.ToString(prop.GetValue(PDFAirResult))) : "N/A");
                            }
                        }
                    }
                }
                MemoryStream fileMem = getFile(keyValues, keyValuesAirDet, null, sourceFile, destinationFile, "Rep-PRE");

                fileMem.Seek(0, SeekOrigin.Begin);
                System.IO.File.Delete(destinationFile);
                return File(fileMem, "application/octet-stream", fileName);

            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("TASK", "gen_forms", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("NotFound", "Error");
            }
            // return View();
        }
        /// <summary>
        ///  Dictionary return type method used for doc generation
        /// </summary> 
        /// 
        /// <param name="ExpectedKey">Receive the key to replace text in document.</param>
        /// <param name="EventNo">Receive the event no for replace string.</param>
        /// <param name="AddDictItems">Receive the default JSON from database.</param>
        /// <param name="JSON">Receive the foundes key values to dictionary .</param>        
        /// <param name="reportType">Receive the type ofinvestigation report.</param>
        private Dictionary<string, string> getOtherValuesfromEventJSON(string[] ExpectedKey, string EventNo, string JSON, Dictionary<string, string> AddDictItems, string reportType)
        {
            try
            {
                JObject taskformDetails = JObject.Parse(JSON);
                var values = JsonConvert.DeserializeObject<Dictionary<string, List<string>>>(JSON);
                Dictionary<string, List<string>> keyValues = new Dictionary<string, List<string>>();
                keyValues = values;
                foreach (KeyValuePair<string, List<string>> NewJson in keyValues)
                {
                    for (int i = 0; i < ExpectedKey.Length; i++)
                    {
                        //for textbox and textarea values
                        if (!ExpectedKey[i].ToLower().Contains("diagram"))
                        {
                            if (NewJson.Key == (ExpectedKey[i]))
                            {
                                if (ExpectedKey[i].ToLower() == "reportadditionalinformation.text")
                                {
                                    AddInfo += NewJson.Value[0];
                                }
                                else
                                {
                                    if (reportType.ToLower() == "prelim")
                                    {
                                        if (ExpectedKey[i].Contains("txtbox") && string.IsNullOrEmpty(NewJson.Value[0]))
                                            NewJson.Value[0] = " ";
                                        if (!ExpectedKey[i].Contains("txtbox") && string.IsNullOrEmpty(NewJson.Value[0]))
                                            NewJson.Value[0] = "This information will be added in future or in Final Report.";
                                    }
                                    else if (reportType.ToLower() == "final")
                                    {
                                        if (string.IsNullOrEmpty(NewJson.Value[0]))
                                            NewJson.Value[0] = " ";
                                    }
                                    else if (reportType.ToLower() == "Closure")
                                    {
                                        if (string.IsNullOrEmpty(NewJson.Value[0]))
                                            NewJson.Value[0] = " ";
                                    }
                                    else if (reportType.ToLower() == "form-043")
                                    {
                                        if (string.IsNullOrEmpty(NewJson.Value[0]))
                                            NewJson.Value[0] = " ";
                                    }
                                    AddDictItems.Add(NewJson.Key, !string.IsNullOrEmpty(Convert.ToString(NewJson.Value[0])) ? (Convert.ToString(NewJson.Value[0])) : "N/A");
                                }
                            }
                        }
                        if (ExpectedKey[i].ToLower().Contains("diagram"))
                        {
                            if (NewJson.Key.Contains(ExpectedKey[i]))
                            {
                                //to get the figure caption for the files uploaded to be shown in the report.
                                var EventJSON = dc.USP_VIEW_EVENTJSON_TASKDETAIL_REPORTS(EventNo).FirstOrDefault();
                                string FigName = getFileControlName(EventJSON.InvestigationTaskGroupEventJSON, ExpectedKey[i]);
                                for (int digvalue = 0; digvalue < NewJson.Value.Count(); digvalue++)
                                {
                                    if (!string.IsNullOrEmpty(NewJson.Value[digvalue]))
                                    {
                                        //to get the attachment url from the attachment ID
                                        var docurl = dc.sp_get_event_attahments_reports(null, Convert.ToInt32(NewJson.Value[digvalue])).FirstOrDefault();
                                        if (!string.IsNullOrEmpty(docurl.AttachmentPublicUrl))
                                        {
                                            string docType = docurl.AttachmentPublicUrl.Substring((docurl.AttachmentPublicUrl.LastIndexOf('.') + 1), (docurl.AttachmentPublicUrl.Length - (docurl.AttachmentPublicUrl.LastIndexOf('.') + 1)));
                                            if (docType.ToLower() == "png" || docType.ToLower() == "jpeg" || docType.ToLower() == "jpeg" || docType.ToLower() == "jpg" || docType.ToLower() == "bmp")
                                            {
                                                //string file = comm.getEventFiles(docurl.AttachmentPublicUrl);
                                                string file = comm.getEventFilesOrOtherFilesOnPrem(docurl.AttachmentPublicUrl);
                                                List<string> list = new List<string>();
                                                //adding the image files to be shown in the report in a separate dictionary
                                                if (EventsAttachments.ContainsKey(NewJson.Key))
                                                    EventsAttachments[NewJson.Key].Add(!string.IsNullOrEmpty(file) ? (Convert.ToString(file)) : "N/A");
                                                else
                                                {
                                                    EventsAttachments.Add(NewJson.Key, list);
                                                    list.Add(!string.IsNullOrEmpty(file) ? (Convert.ToString(file)) : "N/A");
                                                }
                                            }
                                            else
                                            {
                                                //added in main dictionary since it needs to be replaced with the empty string in case of non image files
                                                AddDictItems.Add(NewJson.Key, " ");
                                            }
                                        }
                                        else
                                        {
                                            //added in main dictionary since it needs to be replaced with the empty string if the image value is empty
                                            AddDictItems.Add(NewJson.Key, " ");
                                        }
                                    }
                                    else
                                    {
                                        //added in main dictionary since it needs to be replaced with the empty string if the image value is empty
                                        AddDictItems.Add(NewJson.Key, " ");
                                    }
                                }
                                //dictionary that contains the figure name captions
                                FilesCaptions.Add(NewJson.Key, FigName);
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("FormsController", "getOtherValuesfromEventJSON", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }
            return AddDictItems;
        }
        /// <summary>
        ///  To find name of the control in JSON array
        /// </summary> 
        /// 
        /// <param name="investigationTaskGroupEventJSON">Receive the default JSON from database.</param>
        /// <param name="findKey">Receive the key for control name.</param>
        private string getFileControlName(string investigationTaskGroupEventJSON, string findKey)
        {
            string ControlName = string.Empty;
            try
            {
                dynamic formDetails = JObject.Parse(investigationTaskGroupEventJSON);
                var fields = formDetails.fields;
                for (var i = 0; i < fields.Count; i++)
                {
                    List<string> list_json = new List<string>();

                    if (fields[i].name == findKey)
                    {
                        ControlName = fields[i].label;
                        break;
                    }
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("FormsController", "getFileControlName", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }
            return ControlName;
        }
        /// <summary>
        /// Include image file in JSON array
        /// </summary> 
        /// 
        /// <param name="eventAttachments">Receive the event attachment name as string array.</param>
        /// <param name="requiredKey">Receive the required key.</param>
        /// <param name="destinationFile">Receive the destination file location.</param>
        private void includeImage(string[] eventAttachments, string requiredKey, string destinationFile)
        {
            Drawing image = new Drawing();
            dynamic[] imgs = new dynamic[50];
            string imgNames = string.Empty;
            using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(destinationFile, true))
                for (int iii = 0; iii < attachments.Length; iii++)
                {
                    if (!string.IsNullOrEmpty(attachments[iii]))
                    {
                        ImagePart imgfootPart = wordDoc.MainDocumentPart.AddImagePart(ImagePartType.Jpeg);
                        using (FileStream stream = new FileStream(attachments[iii], FileMode.Open))
                        {
                            if (stream != null)
                            {
                                imgfootPart.FeedData(stream);
                                image = AddImageToBody(wordDoc.MainDocumentPart.GetIdOfPart(imgfootPart), attachments[iii]);
                                imgs[iii] = image;
                                imgNames = attachments[iii].Substring(attachments[iii].LastIndexOf('\\') + 1, ((attachments[iii].Length) - (attachments[iii].LastIndexOf('\\') + 1)));
                                //             includeImgContent(imgs, requiredKey, wordDoc, imgNames);
                            }
                        }
                    }
                }
        }
        /// <summary>
        /// Include image to document body
        /// </summary> 
        /// 
        /// <param name="relationshipId">Receive the relationship id of image file.</param>
        /// <param name="filename">Receive the image file location.</param>
        private static Drawing AddImageToBody(string relationshipId, string filename)
        {
            //string filepath =System.Web.HttpContext.Current.Server.MapPath(filename);
            // Define the reference of the image.
            //using (Bitmap bmp = new Bitmap("C:\\Users\\xminds\\Downloads\\lakeandballoon.jpg"))
            //{
            //    iWidth = bmp.Width;
            //    iHeight = bmp.Height;
            //}
            //iWidth = (int)Math.Round((decimal)iWidth * 9525);
            //iHeight = (int)Math.Round((decimal)iHeight * 9525);
            var element =
                 new Drawing(
                     new DW.Inline(
                         //new DW.Extent() { Cx = 990000L, Cy = 990000L },
                         new DW.Extent() { Cx = 6700000, Cy = 3000000 },
                         new DW.EffectExtent()
                         {
                             LeftEdge = 0L,
                             TopEdge = 0L,
                             RightEdge = 0L,
                             BottomEdge = 0L
                         },
                         new DW.DocProperties()
                         {
                             Id = (UInt32Value)1U,
                             Name = "Picture 1"
                         },
                         new DW.NonVisualGraphicFrameDrawingProperties(
                             new A1.GraphicFrameLocks() { NoChangeAspect = true }),
                         new A1.Graphic(
                             new A1.GraphicData(
                                 new PIC.Picture(
                                     new PIC.NonVisualPictureProperties(
                                         new PIC.NonVisualDrawingProperties()
                                         {
                                             Id = (UInt32Value)0U,
                                             Name = "New Bitmap Image.jpg"
                                         },
                                         new PIC.NonVisualPictureDrawingProperties()),
                                     new PIC.BlipFill(
                                         new A1.Blip(
                                             new A1.BlipExtensionList(
                                                 new A1.BlipExtension()
                                                 {
                                                     Uri =
                                                       "{28A0092B-C50C-407E-A947-70E740481C1C}"
                                                 })
                                         )
                                         {
                                             Embed = relationshipId,
                                             CompressionState = A1.BlipCompressionValues.Print
                                         },
                                         new A1.Stretch(
                                             new A1.FillRectangle())),
                                     new PIC.ShapeProperties(
                                         new A1.Transform2D(
                                             new A1.Offset() { X = 0L, Y = 0L },
                                             new A1.Extents() { Cx = 990000L, Cy = 990000L }),
                                         new A1.PresetGeometry(
                                             new A1.AdjustValueList()
                                         )
                                         { Preset = A1.ShapeTypeValues.Rectangle }))
                             )
                             { Uri = "http://schemas.openxmlformats.org/drawingml/2006/picture" })
                     )
                     {
                         DistanceFromTop = (UInt32Value)114300U,
                         DistanceFromBottom = (UInt32Value)0U,
                         DistanceFromLeft = (UInt32Value)114300U,
                         DistanceFromRight = (UInt32Value)0U,
                         EditId = "50D07946"
                     });

            // Append the reference to body, the element should be in a Run.
            // wordDoc.MainDocumentPart.Document.Body.AppendChild(new Paragraph(new Run(element)));
            //  return element;
            //to return header and append image in it.

            return element;
        }
        /// <summary>
        /// copy the stream of data into the word processing document
        /// </summary> 
        /// 
        /// <param name="source">Receive the source file as stream object.</param>
        /// <param name="target">Receive the target file as stream object.</param>
        public static void CopyStream(Stream source, Stream target)
        {
            if (source != null)
            {
                MemoryStream mstream = source as MemoryStream;
                if (mstream != null) mstream.WriteTo(target);
                else
                {
                    byte[] buffer = new byte[2048];
                    int length = buffer.Length, size;
                    while ((size = source.Read(buffer, 0, length)) != 0)
                        target.Write(buffer, 0, size);
                }
            }
        }
        /// <summary>
        /// Create table in word processing document 
        /// </summary> 
        /// 
        /// <param name="Files">Receive the files as dynamic object.</param>
        /// <param name="doc">Receive the Word processing Document.</param>
        private static Table createTable(dynamic Files, WordprocessingDocument doc)
        {
            Table table = new Table();

            // Create a TableProperties object and specify its border information.
            TableProperties tblProp = new TableProperties(
                new TableBorders(
                    new TopBorder()
                    {
                        Val =
                        new EnumValue<BorderValues>(BorderValues.Single),
                        Size = 10
                    },
                    new BottomBorder()
                    {
                        Val =
                        new EnumValue<BorderValues>(BorderValues.Single),
                        Size = 10
                    },
                    new LeftBorder()
                    {
                        Val =
                        new EnumValue<BorderValues>(BorderValues.Single),
                        Size = 10
                    },
                    new RightBorder()
                    {
                        Val =
                        new EnumValue<BorderValues>(BorderValues.Single),
                        Size = 10
                    },
                    new InsideHorizontalBorder()
                    {
                        Val =
                        new EnumValue<BorderValues>(BorderValues.Single),
                        Size = 10
                    },
                    new InsideVerticalBorder()
                    {
                        Val =
                        new EnumValue<BorderValues>(BorderValues.Single),
                        Size = 10
                    }
                )
            );

            // Append the TableProperties object to the empty table.
            table.AppendChild<TableProperties>(tblProp);

            if (Files.Count > 0)
            {
                int file_Count = 0;
                foreach (var file in Files)
                {
                    file_Count++;
                    var tr = new TableRow();
                    var tc = new TableCell();
                    tc.Append(new TableCellProperties(new TableCellWidth { Type = TableWidthUnitValues.Dxa, Width = "4650" }));
                    tc.Append(new Paragraph(new Run(new Text(Convert.ToString(file_Count)))));
                    tr.Append(tc);
                    string filename = Convert.ToString(file.AttachmentPublicUrl);
                    filename = filename.Substring(filename.LastIndexOf('/') + 1);
                    var tc1 = new TableCell();
                    HyperlinkRelationship rel = doc.MainDocumentPart.AddHyperlinkRelationship(new Uri(file.AttachmentOriginalUrl), true);
                    string relationshipId = rel.Id;
                    tc1.Append(new TableCellProperties(new TableCellWidth { Type = TableWidthUnitValues.Dxa, Width = "4650" }));
                    tc1.Append(new Paragraph(new Hyperlink(new Run(new Text(filename))) { History = OnOffValue.FromBoolean(true), Id = relationshipId }));
                    tr.Append(tc1);
                    table.Append(tr);
                }
            }
            else
            {
                var tr = new TableRow();
                var tc = new TableCell();
                tc.Append(new TableCellProperties(new TableCellWidth { Type = TableWidthUnitValues.Dxa, Width = "4650" }));
                tc.Append(new Paragraph(new Run(new Text(Convert.ToString("No attachments available")))));
                tr.Append(tc);
                table.Append(tr);
            }
            // Append the table to the document.
            return table;
        }
        /// <summary>
        /// Include image content in word processing document
        /// </summary> 
        /// 
        /// <param name="content">Receive the image file content.</param>
        /// <param name="findText">Receive the find text value.</param>
        /// <param name="doc">Receive the Word processing Document.</param>
        /// <param name="imgName">Receive the image name.</param>
        public void includeImgContent(dynamic[] content, string findText, WordprocessingDocument doc, string[] imgName)
        {
            var body = doc.MainDocumentPart.Document.Body;
            var paras = body.Descendants<Paragraph>();

            foreach (var para in paras)
            {
                foreach (var run in para.Elements<Run>())
                {
                    foreach (var text in run.Elements<Text>())
                    {
                        if (text.Text.Contains(findText))
                        {
                            text.RemoveAllChildren();
                            // text.Text = "found text but not replaced";
                            for (int i = 0; i < content.Length; i++)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(content[i])))
                                {
                                    //adds the image in the report
                                    para.Append(content[i]);
                                    para.Append(new Break());
                                    para.Append(new Break());

                                    if (content[i].GetType().Name == "Drawing")
                                    {

                                        //Paragraph para2 = body.AppendChild(new Paragraph());
                                        //Run run2 = para.AppendChild(new Run());
                                        //run2.AppendChild(new Text { Text = " ", Space = SpaceProcessingModeValues.Preserve });

                                        //RunProperties runProperties = run2.AppendChild(new RunProperties());

                                        //FontSize fontSize = new FontSize();

                                        //fontSize.Val = "22";
                                        //runProperties.Color = new DocumentFormat.OpenXml.Wordprocessing.Color() { Val = "1F497D" };
                                        //RunFonts font1 = new RunFonts() { Ascii = "Calibri (Body)" };
                                        //runProperties.AppendChild(fontSize);
                                        //runProperties.Append(font1);
                                        //Bold bold = new Bold();

                                        //bold.Val = OnOffValue.FromBoolean(true);

                                        //runProperties.AppendChild(bold);
                                        //run2.AppendChild(new Text("Figure 1: The Clipboard Tab"));
                                        Run run1 = new Run(new Text() { Text = "Figure" + " ", Space = SpaceProcessingModeValues.Preserve });
                                        SimpleField simpleField = new SimpleField(new Run(new RunProperties(new NoProof()), new Text() { Text = " ", Space = SpaceProcessingModeValues.Preserve }));
                                        simpleField.Instruction = @"SEQ " + "Figure";
                                        Run runLabel = new Run(new Text() { Text = " : " + imgName[i], Space = SpaceProcessingModeValues.Preserve });
                                        ParagraphProperties captionPr = new ParagraphProperties(new ParagraphStyleId() { Val = "Caption" });
                                        para.ParagraphProperties = captionPr;
                                        para.Append(run1);
                                        para.Append(simpleField);
                                        para.Append(runLabel);
                                        para.Append(new Break());
                                        para.Append(new Break());
                                        //Paragraph paragraph = new Paragraph();
                                        //paragraph.ParagraphProperties = captionPr;
                                        //paragraph.Append(run1);
                                        //paragraph.Append(simpleField);
                                        //paragraph.Append(runLabel);
                                    }
                                }
                                else { break; }

                            }

                            break;
                        }
                    }
                    break;
                }
            }
        }
        /// <summary>
        /// Include table content in word processing document
        /// </summary> 
        /// 
        /// <param name="content">Receive the image file content.</param>
        /// <param name="findText">Receive the find text value.</param>
        /// <param name="doc">Receive the Word processing Document.</param>
        /// <param name="imgName">Receive the image name.</param>
        public void includeTableContent(dynamic content, string findText, WordprocessingDocument doc, string imgName)
        {
            var body = doc.MainDocumentPart.Document.Body;
            var paras = body.Elements<Paragraph>();
            foreach (var para in paras)
            {
                foreach (var run in para.Elements<Run>())
                {
                    foreach (var text in run.Elements<Text>())
                    {
                        if (text.Text.Contains(findText))
                        {
                            text.RemoveAllChildren();
                            // text.Text = "found text but not replaced";
                            run.Append(content);
                            run.Append(new Break());
                            run.Append(new Break());

                            if (content.GetType().Name == "Drawing")
                            {

                                //Paragraph para2 = body.AppendChild(new Paragraph());
                                //Run run2 = para.AppendChild(new Run());
                                //run2.AppendChild(new Text { Text = " ", Space = SpaceProcessingModeValues.Preserve });

                                //RunProperties runProperties = run2.AppendChild(new RunProperties());

                                //FontSize fontSize = new FontSize();

                                //fontSize.Val = "22";
                                //runProperties.Color = new DocumentFormat.OpenXml.Wordprocessing.Color() { Val = "1F497D" };
                                //RunFonts font1 = new RunFonts() { Ascii = "Calibri (Body)" };
                                //runProperties.AppendChild(fontSize);
                                //runProperties.Append(font1);
                                //Bold bold = new Bold();

                                //bold.Val = OnOffValue.FromBoolean(true);

                                //runProperties.AppendChild(bold);
                                //run2.AppendChild(new Text("Figure 1: The Clipboard Tab"));
                                Run run1 = new Run(new Text() { Text = "Figure" + " ", Space = SpaceProcessingModeValues.Preserve });
                                SimpleField simpleField = new SimpleField(new Run(new RunProperties(new NoProof()), new Text() { Text = " ", Space = SpaceProcessingModeValues.Preserve }));
                                simpleField.Instruction = @"SEQ " + "Figure";
                                Run runLabel = new Run(new Text() { Text = " : " + imgName, Space = SpaceProcessingModeValues.Preserve });

                                ParagraphProperties captionPr = new ParagraphProperties(new ParagraphStyleId() { Val = "Caption" });
                                para.ParagraphProperties = captionPr;
                                para.AppendChild(run1);
                                para.Append(simpleField);
                                para.Append(runLabel);

                                //Paragraph paragraph = new Paragraph();
                                //paragraph.ParagraphProperties = captionPr;
                                //paragraph.Append(run1);
                                //paragraph.Append(simpleField);
                                //paragraph.Append(runLabel);

                            }
                            break;
                        }
                    }
                    break;
                }
            }
        }
        /// <summary> get file from word processing document</summary>          
        /// <param name="keyandValues">Receive the key values as dictionary.</param>
        /// <param name="keyAircraftValues">Receive the Aircraft key values as dictionary.</param>
        /// <param name="Files">Receive the Word processing Document.</param>
        /// <param name="sourceFile">Receive the source file.</param>
        /// <param name="destinationFile">Receive the destination file name.</param>
        /// <param name="FileType">Receive the file type.</param>       
        public MemoryStream getFile(Dictionary<string, string> keyandValues, Dictionary<string, List<string>> keyAircraftValues, dynamic Files, string sourceFile, string destinationFile, string FileType)
        {
            MemoryStream mem = new MemoryStream();
            try
            {
                int noofaircrafts = 0;
                MemoryStream documentStream;
                if (new FileInfo(sourceFile).Length > 0)
                {
                    using (Stream tplStream = System.IO.File.OpenRead(sourceFile))
                    {
                        documentStream = new MemoryStream((int)tplStream.Length);
                        CopyStream(tplStream, documentStream);
                        documentStream.Position = 0L;
                    }
                    if (documentStream.Length > 0)
                    {
                        using (WordprocessingDocument template = WordprocessingDocument.Open(documentStream, true))
                        {
                            template.ChangeDocumentType(DocumentFormat.OpenXml.WordprocessingDocumentType.Document);
                            MainDocumentPart mainPartS = template.MainDocumentPart;
                            mainPartS.DocumentSettingsPart.AddExternalRelationship("http://schemas.openxmlformats.org/officeDocument/2006/relationships/attachedTemplate", new Uri(sourceFile, UriKind.Absolute));
                            mainPartS.Document.Save();
                        }
                        System.IO.File.WriteAllBytes(destinationFile, documentStream.ToArray());
                    }
                }
                if (new FileInfo(destinationFile).Length > 0)
                {
                    using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(destinationFile, true))
                    {
                        string docText = null;
                        MainDocumentPart mainPart = wordDoc.MainDocumentPart;
                        using (StreamReader sr = new StreamReader(wordDoc.MainDocumentPart.GetStream()))
                        {
                            docText = sr.ReadToEnd();
                        }
                        //replacing text in the original form
                        foreach (KeyValuePair<string, string> item in keyandValues)
                        {
                            //to get the no of aircrafts involved
                            if (item.Key == "NoOfAircraftInvolved")
                                noofaircrafts = Convert.ToInt32(item.Value);
                            Regex regexText = new Regex(item.Key);
                            SearchandReplace.SearchAndReplace(wordDoc, item.Key, item.Value, false);
                        }
                    }
                    if (FileType == "F35")
                    {
                        using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(destinationFile, true))
                        {
                            if (noofaircrafts > 1)
                            {
                                //Table table = wordDoc.MainDocumentPart.Document.Body.Descendants<Table>().FirstOrDefault();
                                IEnumerable<TableProperties> tableProperties = wordDoc.MainDocumentPart.Document.Body.Descendants<TableProperties>().Where(tp => tp.TableCaption != null);
                                foreach (TableProperties tProp in tableProperties)
                                {
                                    if (tProp.TableCaption.Val == "AircraftDetails") // see comment, this is actually StringValue
                                    {
                                        Table table = (Table)tProp.Parent;
                                        //Table table = wordDoc.MainDocumentPart.Document.Body.Descendants<Table>().ElementAt(1);
                                        if (noofaircrafts > 1)
                                        {
                                            for (int count = 1; count < noofaircrafts; count++)
                                            {
                                                Table tbl = new Table(table.CloneNode(true));
                                                wordDoc.MainDocumentPart.Document.Body.InsertAfter(tbl, table);
                                                wordDoc.MainDocumentPart.Document.Save();
                                            }
                                        }
                                        break;
                                    }
                                }
                            }

                            //for cloning the table for aircraft details
                            int tblCount = wordDoc.MainDocumentPart.Document.Body.Elements<Table>().Count();
                            int airtbcount = -1;//for getting 0th array in dictionary value
                            int airtable = 1;

                            for (int ii = 0; ii < tblCount; ii++)
                            {
                                Table tbll = wordDoc.MainDocumentPart.Document.Body.Elements<Table>().ElementAt(ii);
                                IEnumerable<TableProperties> tableProperties1 = tbll.Descendants<TableProperties>().Where(tp => tp.TableCaption != null);
                                foreach (TableProperties tProp in tableProperties1)
                                {
                                    if (tProp.TableCaption.Val == "AircraftDetails") // see comment, this is actually StringValue
                                    {
                                        airtbcount++;
                                        foreach (TableRow row in tbll.Descendants<TableRow>())
                                        {
                                            foreach (TableCell cell in row.Elements<TableCell>())
                                            {
                                                foreach (var para in cell.Elements<Paragraph>())
                                                {
                                                    foreach (var run in para.Elements<Run>())
                                                    {
                                                        foreach (var text in run.Elements<Text>())
                                                        {
                                                            string count = Convert.ToString(airtable);

                                                            foreach (var item in keyAircraftValues)
                                                            {
                                                                if (text.Text == item.Key)
                                                                {
                                                                    text.Text = (!string.IsNullOrEmpty(Convert.ToString(item.Value[airtbcount])) ? item.Value[airtbcount] : "N/A");
                                                                    break;
                                                                }
                                                            }
                                                            if (text.Text.Trim() == "ii")
                                                                text.Text = "Aircraft - " + count;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        airtable++;
                                        break;
                                    }
                                }
                            }

                            if (Files != null)
                            {
                                Table Filetable = createTable(Files, wordDoc);

                                includeTableContent(Filetable, "FilesAttach", wordDoc, "");
                            }
                        }
                    }
                    if (FileType == "F39")
                    {
                        using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(destinationFile, true))
                        {
                            Table table = wordDoc.MainDocumentPart.Document.Body.Descendants<Table>().ElementAt(1);
                            if (noofaircrafts > 1)
                            {
                                for (int count = 1; count < noofaircrafts; count++)
                                {
                                    Table tbl = new Table(table.CloneNode(true));
                                    wordDoc.MainDocumentPart.Document.Body.InsertAfter(tbl, table);
                                    wordDoc.MainDocumentPart.Document.Save();
                                }
                            }
                            string Team = string.Empty;
                            if (Files != null)
                            {
                                if (Files.Count > 0)
                                {
                                    foreach (var files in Files)
                                    {
                                        if (files.UsrType == "TM")
                                        {
                                            if (string.IsNullOrEmpty(Team))
                                                Team = files.FirstName;
                                            else
                                                Team += "," + files.FirstName;
                                        }
                                    }
                                }
                                else
                                {
                                    Team = "N/A";
                                }
                                //    SearchandReplace.SearchAndReplace(wordDoc, tosearch, Team, false);
                            }
                            else
                                Team = "N/A";
                            Table MainTable = wordDoc.MainDocumentPart.Document.Body.Descendants<Table>().FirstOrDefault();
                            foreach (TableRow row in MainTable.Descendants<TableRow>())
                            {
                                foreach (TableCell cell in row.Elements<TableCell>())
                                {
                                    foreach (var para in cell.Elements<Paragraph>())
                                    {
                                        foreach (var run in para.Elements<Run>())
                                        {

                                            foreach (var text in run.Descendants<Text>())
                                            {
                                                if (text.Text == "MemberName")
                                                {
                                                    text.Text = Team;
                                                    break;
                                                }
                                            }
                                        }
                                        break;
                                    }
                                }
                            }
                            int tblCount = wordDoc.MainDocumentPart.Document.Body.Elements<Table>().Count();
                            for (int ii = 1; ii < tblCount; ii++)
                            {
                                Table tbll = wordDoc.MainDocumentPart.Document.Body.Elements<Table>().ElementAt(ii);
                                foreach (TableRow row in tbll.Descendants<TableRow>())
                                {
                                    foreach (TableCell cell in row.Elements<TableCell>())
                                    {
                                        foreach (var para in cell.Elements<Paragraph>())
                                        {
                                            foreach (var run in para.Elements<Run>())
                                            {

                                                foreach (var text in run.Elements<Text>())
                                                {
                                                    string count = Convert.ToString(ii);

                                                    foreach (var item in keyAircraftValues)
                                                    {
                                                        if (text.Text == item.Key)
                                                        {
                                                            text.Text = (!string.IsNullOrEmpty(Convert.ToString(item.Value[ii - 1])) ? item.Value[ii - 1] : "N/A");
                                                            break;
                                                        }

                                                    }
                                                    if (text.Text.Trim() == "ii")
                                                        text.Text = "Aircraft - " + count;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (FileType == "F43")
                    {
                        using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(destinationFile, true))
                        {
                            Table table = wordDoc.MainDocumentPart.Document.Body.Descendants<Table>().ElementAt(2);
                            if (noofaircrafts > 1)
                            {
                                for (int count = 1; count < noofaircrafts; count++)
                                {
                                    Table tbl = new Table(table.CloneNode(true));
                                    wordDoc.MainDocumentPart.Document.Body.InsertAfter(tbl, table);
                                    wordDoc.MainDocumentPart.Document.Save();
                                }
                            }
                            int tblCount = wordDoc.MainDocumentPart.Document.Body.Elements<Table>().Count();
                            for (int ii = 1; ii < tblCount; ii++)
                            {
                                Table tbll = wordDoc.MainDocumentPart.Document.Body.Elements<Table>().ElementAt(ii);
                                foreach (TableRow row in tbll.Descendants<TableRow>())
                                {
                                    foreach (TableCell cell in row.Elements<TableCell>())
                                    {
                                        foreach (var para in cell.Elements<Paragraph>())
                                        {
                                            foreach (var run in para.Elements<Run>())
                                            {
                                                foreach (var text in run.Elements<Text>())
                                                {
                                                    string count = Convert.ToString(ii);

                                                    foreach (var item in keyAircraftValues)
                                                    {
                                                        if (text.Text == item.Key)
                                                        {
                                                            text.Text = (!string.IsNullOrEmpty(Convert.ToString(item.Value[ii - 1])) ? item.Value[ii - 1] : "N/A");
                                                            break;
                                                        }
                                                    }
                                                    if (text.Text.Trim() == "ii")
                                                        text.Text = "Aircraft - " + count;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //Preliminary Report
                    if (FileType == "Rep-PRE")
                    {
                        using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(destinationFile, true))
                        {
                            IEnumerable<TableProperties> tableProperties = wordDoc.MainDocumentPart.Document.Body.Descendants<TableProperties>().Where(tp => tp.TableCaption != null);

                            foreach (TableProperties tProp in tableProperties)
                            {
                                if (tProp.TableCaption.Val == "AircraftDetails") // see comment, this is actually StringValue
                                {
                                    Table table = (Table)tProp.Parent;
                                    //Table table = wordDoc.MainDocumentPart.Document.Body.Descendants<Table>().ElementAt(1);
                                    if (noofaircrafts > 1)
                                    {
                                        for (int count = 1; count < noofaircrafts; count++)
                                        {
                                            Table tbl = new Table(table.CloneNode(true));
                                            wordDoc.MainDocumentPart.Document.Body.InsertAfter(tbl, table);
                                            wordDoc.MainDocumentPart.Document.Save();
                                        }
                                    }
                                    break;
                                }
                            }
                            int tblCount = wordDoc.MainDocumentPart.Document.Body.Elements<Table>().Count();
                            int airtbcount = -1;//for getting 0th array in dictionary value
                            int airtable = 1;

                            for (int ii = 0; ii < tblCount; ii++)
                            {
                                Table tbll = wordDoc.MainDocumentPart.Document.Body.Elements<Table>().ElementAt(ii);
                                IEnumerable<TableProperties> tableProperties1 = tbll.Descendants<TableProperties>().Where(tp => tp.TableCaption != null);

                                foreach (TableProperties tProp in tableProperties1)
                                {
                                    if (tProp.TableCaption.Val == "AircraftDetails") // see comment, this is actually StringValue
                                    {
                                        airtbcount++;
                                        foreach (TableRow row in tbll.Descendants<TableRow>())
                                        {
                                            foreach (TableCell cell in row.Elements<TableCell>())
                                            {
                                                foreach (var para in cell.Elements<Paragraph>())
                                                {
                                                    foreach (var run in para.Elements<Run>())
                                                    {
                                                        foreach (var text in run.Elements<Text>())
                                                        {
                                                            string count = Convert.ToString(airtable);

                                                            foreach (var item in keyAircraftValues)
                                                            {
                                                                if (text.Text == item.Key)
                                                                {
                                                                    text.Text = (!string.IsNullOrEmpty(Convert.ToString(item.Value[airtbcount])) ? item.Value[airtbcount] : "N/A");
                                                                    break;
                                                                }
                                                            }
                                                            if (text.Text.Trim() == "ii")
                                                                text.Text = "Aircraft - " + count;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        airtable++;
                                        break;
                                    }
                                }
                            }

                            Drawing image = new Drawing();
                            if (EventsAttachments.Count() > 0)
                            {
                                foreach (KeyValuePair<string, List<string>> item in EventsAttachments)
                                {
                                    dynamic[] imgs = new dynamic[50];
                                    int iii = 0;
                                    string[] imgNames = new string[50];
                                    for (int val = 0; val < item.Value.Count(); val++)
                                    {

                                        string imgval = item.Value[val];
                                        if (!string.IsNullOrEmpty(Convert.ToString(item.Value[val])))
                                        {
                                            string docType = imgval.Substring((imgval.LastIndexOf('.') + 1), (imgval.Length - (imgval.LastIndexOf('.') + 1)));
                                            if (docType.ToLower() == "png" || docType.ToLower() == "jpeg" || docType.ToLower() == "jpeg" || docType.ToLower() == "jpg" || docType.ToLower() == "bmp")
                                            {

                                                ImagePart imgfootPart = wordDoc.MainDocumentPart.AddImagePart(ImagePartType.Jpeg);
                                                using (FileStream stream = new FileStream(item.Value[val], FileMode.Open))
                                                {
                                                    if (stream != null)
                                                    {
                                                        imgfootPart.FeedData(stream);
                                                        image = AddImageToBody(wordDoc.MainDocumentPart.GetIdOfPart(imgfootPart), item.Value[val]);
                                                        imgs[iii] = image;
                                                        imgNames[iii] = FilesCaptions[item.Key];
                                                    }
                                                }
                                                iii++;
                                            }
                                        }
                                    }
                                    if (imgs[0] != null)
                                        includeImgContent(imgs, item.Key, wordDoc, imgNames);
                                }
                            }
                        }

                        using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(destinationFile, true))
                        {
                            XElement ToC = wordDoc
                     .MainDocumentPart
                     .GetXDocument()
                     .Descendants(W.p).Descendants(W.r).Descendants(W.t)
                     .Where(z => z.Value == "TOC1")
                    .FirstOrDefault();
                            ToCAdder.AddToc(wordDoc, ToC,
                                                    @"TOC \o '1-3' \h \z \u", "Table of Contents", null);

                            XElement ToF = wordDoc
                            .MainDocumentPart
                           .GetXDocument()
                           .Descendants(W.p).Descendants(W.r).Descendants(W.t)
                           .Where(z => z.Value == "TOF1")
                           .FirstOrDefault();
                            ToCAdder.AddTof(wordDoc, ToF,
                             @"TOC \h \z \c ""Figure""", null);
                        }
                    }

                }

                //string[] doNotUse_Words = dc.LKCOMMONs.Where(a => a.LKCOMMONID == 24).FirstOrDefault().LKVALUE.ToString().Split(',');

                //AppendStyle(destinationFile, doNotUse_Words, "FFFF0000");

                //  SetRunFont(destinationFile);
                mem = new MemoryStream(System.IO.File.ReadAllBytes(destinationFile));
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("FormsController", "getFile", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }
            return mem;
        }
        /// <summary>
        /// Set running font to the word processing document
        /// </summary> 
        /// 
        /// <param name="fileName">Receive the file name.</param>     
        public static void SetRunFont(string fileName)
        {
            // Open a Wordprocessing document for editing.
            using (WordprocessingDocument package = WordprocessingDocument.Open(fileName, true))
            {
                // Set the font to Arial to the first Run.
                // Use an object initializer for RunProperties and rPr.

                var body = package.MainDocumentPart.Document.Body;
                var paras = body.Descendants<Paragraph>();
                foreach (var para in paras)
                {
                    foreach (var run in para.Descendants<Run>())
                    {
                        RunProperties rPr = new RunProperties(
                          new RunFonts()
                          {
                              Ascii = "Arial"
                          });
                        run.PrependChild<RunProperties>(rPr);
                        package.MainDocumentPart.Document.Save();
                    }
                }
            }
        }
        /// <summary>
        /// While upload fine report to system 
        /// </summary> 
        /// 
        /// <param name="page">Receive the final report page no.</param>     
        /// <param name="id">Receive the investigation file id.</param>     
        public ActionResult UploadFinalReport(int? page, int id = 0)
        {
            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 3 || Convert.ToInt32(Session["RoleId"]) == 6 || Convert.ToInt32(Session["RoleId"]) == 7)
                {
                    Reports rpt = new Reports();
                    if (id > 0)
                    {
                        if (page == null)
                        {
                            ViewBag.serial_nbr = 0;
                        }
                        else
                        {
                            ViewBag.serial_nbr = 10 * (page - 1);
                        }
                        rpt.RPTs = dc.USP_VIEW_REPORTS(id, "Final").ToList();
                        rpt.RPTInv = dc.USP_VIEW_INVESTIGATION_FORM35(id).ToList();
                        var invInfo = dc.USP_VIEW_INVESTIGATION_FORM35(id).FirstOrDefault();
                        rpt.InvestigationFileNumber = invInfo.InvestigationNumber;
                        rpt.InvestigationFileID = id;
                        ViewBag.page_no = page;
                        TempData["InvId"] = id;
                        ViewBag.count_pagecontent = rpt.RPTs.Count();
                        TempData["PageHead"] = invInfo.InvestigationNumber;
                        TempData["BackPage"] = "forms_reports";
                        return View(rpt);
                    }
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("FormsController", "UploadFinalReport", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }
            return View();
        }
        /// <summary>
        /// To save the final report to the system
        /// </summary> 
        /// 
        /// <param name="RPT">Receive the investigation file report object.</param>     
        [HttpPost]
        public ActionResult UploadFinalReport(Reports RPT)
        {
            try
            {
                if (Convert.ToInt32(Session["UserId"]) > 0)
                {
                    saveFilestoSharePoint(Request.Files, RPT.InvestigationFileNumber);
                    if (!string.IsNullOrEmpty(SharepointfilePath))
                    {
                        dc.USP_INST_REPORTS(RPT.InvestigationFileID, SharepointfilePath, RPT.FileVersion, "Final", Convert.ToInt32(Session["UserId"]), Convert.ToInt32(Session["UserId"]));
                        dc.SaveChanges();
                    }
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("FormsController", "UploadFinalReport::POST", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }
            return RedirectToAction("UploadFinalReport", new { id = RPT.InvestigationFileID });
        }
        /// <summary>
        /// Save all attached file to database table
        /// </summary> 
        /// <param name="InvId">Receive the investigation file id.</param>     
        [HttpPost]
        public void SaveFiles(string InvId)
        {
            try
            {
                if (Convert.ToInt32(Session["UserId"]) > 0)
                {
                    string CONTROL_NUMER = InvId;
                    if (CONTROL_NUMER != null)
                    {
                        if (CONTROL_NUMER.Contains("/"))
                            CONTROL_NUMER = CONTROL_NUMER.Replace("/", "_");
                        if (Request.Files.Count > 0)
                        {
                            //comm.CreateIfMissing(Server.MapPath("~/") + "Reports");

                            for (int i = 0; i < Request.Files.Count; i++)
                            {
                                HttpPostedFileBase file = Request.Files[i];

                                int fileSize = file.ContentLength;

                                //string fileName = file.FileName;
                                string fileName = file.FileName.Substring(0, file.FileName.LastIndexOf('.')) + "_" + DateTime.Now.ToFileTime() + file.FileName.Substring(file.FileName.LastIndexOf('.'), ((file.FileName.Length) - (file.FileName.LastIndexOf('.'))));

                                string mimeType = file.ContentType.Split('/')[1].ToString();

                                Stream fileContent = file.InputStream;

                                //  file.SaveAs(Server.MapPath("~/") + "Reports\\" + fileName);

                                //string[] attachURLs = comm.savetoSharePointDocs(fileContent, Server.MapPath("~/") + "CorrespondenceFiles\\" + fileName, CONTROL_NUMER, "Corres_Documents");

                                string[] attachURLs = comm.savetoSharePointReportsOnPrem(fileContent, fileName, CONTROL_NUMER, "Investigation_Reports");

                                SharepointfilePath = attachURLs[0];


                                //   var result = gcaa.USP_INST_INVESTIGATION_DOCUMENTASATTACHMENT(0, InvestigationFileID, 1, DocumentTypeId, SharepointfilePath);

                                //   gcaa.SaveChanges();
                            }
                            //comm.DeleteDirectory(Server.MapPath("~/") + "Reports");
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("IICController", "SaveFiles", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                RedirectToAction("PageNotFound", "Error");
            }
        }
        /// <summary>
        ///  Save attached file to share point based on investigation
        /// </summary> 
        /// 
        /// <param name="files">Receive the investigation file attachments.</param>     
        /// <param name="CONTROL_NUMER">Receive the investigation file id.</param>     
        private void saveFilestoSharePoint(HttpFileCollectionBase files, string CONTROL_NUMER)
        {
            try
            {
                if (files.Count > 0)
                {
                    if (string.IsNullOrEmpty(CONTROL_NUMER))
                        CONTROL_NUMER = "0";
                    else
                    {
                        if (CONTROL_NUMER.Contains("/"))
                            CONTROL_NUMER = CONTROL_NUMER.Replace("/", "_");
                    }
                    int filecount = 0;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];

                        if (file.ContentLength > 0 && !string.IsNullOrEmpty(file.FileName))
                        {

                            int fileSize = file.ContentLength;

                            string fileName = file.FileName.Substring(0, file.FileName.LastIndexOf('.')) + "_" + DateTime.Now.ToFileTime() + file.FileName.Substring(file.FileName.LastIndexOf('.'), ((file.FileName.Length) - (file.FileName.LastIndexOf('.'))));

                            string mimeType = file.ContentType.Split('/')[1].ToString();

                            Stream fileContent = file.InputStream;

                            string[] attachURLs = comm.savetoSharePointReportsOnPrem(fileContent, fileName, CONTROL_NUMER, "Investigation_Reports");

                            SharepointfilePath = attachURLs[0];

                            filecount++;
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("IICController", "saveFilestoSharePoint", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }
        }
        /// <summary>
        /// While upload the final report,check if the version already released or not
        /// </summary> 
        /// 
        /// <param name="id">Receive the investigation file id.</param>     
        /// <param name="version">Receive the investigation file document version name.</param>     
        public JsonResult checkVersionExist(int id, string version)
        {
            bool IsVersionExist;
            var versionHist = dc.USP_GET_REPORTS_VERSIONS(id, "final", version).ToList();
            if (versionHist.Count() > 0)
                IsVersionExist = true;
            else
                IsVersionExist = false;
            return Json(IsVersionExist);
        }
        /// <summary>
        /// Append doc Style
        /// </summary>
        /// <param name="document"></param>
        /// <param name="word"></param>
        /// <param name="Textcolor"></param>
        private void AppendStyle(string document, string[] word, string Textcolor)
        {
            try
            {
                foreach (string st_word in word)
                {
                    WordprocessingDocument wordDocument = WordprocessingDocument.Open(document, true);
                    MainDocumentPart mainDocument = wordDocument.MainDocumentPart;
                    Document doc = mainDocument.Document;
                    Body body = doc.Body;

                    var paragraphs = body.Descendants<Paragraph>();

                    foreach (Paragraph para in paragraphs)
                    {
                        if (hasParticularText(para, st_word))
                        {
                            HighLightText(para, st_word);

                            //wordDocument.MainDocumentPart.Document.Save();
                            //wordDocument.Close();
                        }
                    }

                    CloseAndSaveDocument(wordDocument);
                }
            }
            catch (System.Exception ex)
            {

            }
        }
        /// <summary>
        /// Close And Save word Document
        /// </summary>
        /// <param name="wordDocument"></param>
        private void CloseAndSaveDocument(WordprocessingDocument wordDocument)
        {
            wordDocument.MainDocumentPart.Document.Save();
            wordDocument.Close();
        }
        /// <summary>
        /// High Light Text
        /// </summary>
        /// <param name="paragraph"></param>
        /// <param name="text"></param>
        private void HighLightText(Paragraph paragraph, string text)
        {
            try
            {
                string textOfRun = string.Empty;
                var runCollection = paragraph.Descendants<Run>();
                Run runAfter = null;

                //find the run part which contains the characters
                foreach (Run run in runCollection)
                {
                    try { textOfRun = run.GetFirstChild<Text>().Text; } catch { }
                    if (textOfRun.Contains(text))
                    {
                        //remove the character from thsi run part
                        run.GetFirstChild<Text>().Text = textOfRun.Replace(text, "");
                        runAfter = run;
                        break;
                    }
                }

                //create a new run with your customization font and the character as its text
                Run HighLightRun = new Run();
                RunProperties runPro = new RunProperties();
                // RunFonts runFont = new RunFonts() { Ascii = "Curlz MT", HighAnsi = "Curlz MT" };
                // Bold bold = new Bold();
                DocumentFormat.OpenXml.Wordprocessing.Color color = new DocumentFormat.OpenXml.Wordprocessing.Color() { Val = "FF0000" };
                // FontSize fontSize = new FontSize() { Val = "32" };
                // FontSizeComplexScript fontSizeComplex = new FontSizeComplexScript() { Val = "24" };
                Text runText = new Text() { Text = text };

                // runPro.Append(runFont);
                //  runPro.Append(bold);
                runPro.Append(color);
                // runPro.Append(fontSize);
                // runPro.Append(fontSizeComplex);

                HighLightRun.Append(runPro);
                HighLightRun.Append(runText);

                //insert the new created run part
                paragraph.InsertAfter(HighLightRun, runAfter);

            }
            catch (System.Exception ex)
            {

            }
        }
        /// <summary>
        /// Text convertion for word document file info replacement
        /// </summary>
        /// <param name="paragraph"> input the paragraph</param>
        /// <param name="text"> Receive the input of search key value</param>
        /// <returns></returns>
        private bool hasParticularText(Paragraph paragraph, string text)
        {
            bool has = false;
            try
            {
                string textOfRun = string.Empty;
                var runCollection = paragraph.Descendants<Run>();
                foreach (Run run in runCollection)
                {
                    try
                    {
                        textOfRun = Convert.ToString(run.GetFirstChild<Text>().Text);
                        // textOfRun = Convert.ToString(run.InnerText);
                    }
                    catch
                    {
                    }
                    if (textOfRun.Contains(text))
                    {
                        has = true;
                        break;
                    }
                }
            }
            catch
            {

            }
            return has;
        }
        /// <summary>
        /// common report for Notification ,Investigation and safety recommendation 
        /// </summary>
        /// <param name="Rptid"> Specify type of report </param>
        /// <param name="page">Receive the common page number  </param>       
        /// <param name="FromDate">Investigation or notification from date</param>
        /// <param name="ToDate">Investigation or notification to date</param>
        /// <param name="invFileNo">Receive the investigation file number for search</param>
        /// <returns></returns>
        /// 
        public ActionResult InvestigationReport(string Rptid, int? page, DateTime? FromDate, DateTime? ToDate, string invFileNo)
        {
            Reports objRpt = new Reports();
            try
            {
                if (FromDate == null || ToDate == null)
                {
                    objRpt.FromDate = Convert.ToDateTime(DateTime.Now.AddYears(-1).ToString("dd/MMM/yyyy"));

                    objRpt.ToDate = Convert.ToDateTime(DateTime.Now.ToString("dd/MMM/yyyy"));

                    ViewBag.FromDate = DateTime.Now.AddYears(-1).ToString("dd/MM/yyyy");

                    ViewBag.ToDate = DateTime.Now.ToString("dd/MM/yyyy");
                }
                else
                {
                    objRpt.FromDate = Convert.ToDateTime(objRpt.FromDate.ToString("dd/MMM/yyyy"));

                    objRpt.ToDate = Convert.ToDateTime(objRpt.ToDate.ToString("dd/MMM/yyyy"));

                    ViewBag.FromDate = Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy");

                    ViewBag.ToDate = Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy");
                }

                if (page == 0)
                {
                    page = null;
                }
                else if (page >= 1)
                {
                    objRpt.FromDate = Convert.ToDateTime( FromDate); objRpt.ToDate = Convert.ToDateTime(ToDate);
                }

                if (page == null)
                {
                    ViewBag.SR_serial_nbr = 0;
                }
                else
                {
                    ViewBag.SR_serial_nbr = 10 * (page - 1);
                }

                ViewBag.SRpage_no = page;

                TempData["BackPage"] = "Report";

                if (Rptid == "Inv")
                {
                    ViewBag.Report_Name = "Investigation Report";

                    TempData["Active_Admin_Page"] = "InvReport";

                    if (objRpt.InvestigationFileNumber == null) { objRpt.InvestigationFileNumber = string.Empty; }

                    objRpt.ExcelInvInfo = dc.USP_VIEW_INVESTIGATIONDETAILS_FOR_EXCEEXPORT(Convert.ToString(objRpt.InvestigationFileNumber), objRpt.LKAircraftRegistrationID, objRpt.FromDate, objRpt.ToDate).ToList();

                    ViewBag.count_SRpagecontent = objRpt.ExcelInvInfo.Count();
                }
                else if (Rptid == "Sr")
                {
                    TempData["Active_Admin_Page"] = "SRReport";

                    ViewBag.Report_Name = "Safety Recommendation Report";

                    objRpt.InvestigationFileNumber = invFileNo;

                    if (objRpt.InvestigationFileNumber == null) { objRpt.InvestigationFileNumber = string.Empty; }
                    if (objRpt.SR_Number == null) { objRpt.SR_Number = string.Empty; }

                    objRpt.ExcelInfo = dc.USP_VIEW_SR_FOR_EXCEEXPORT(objRpt.SR_Number, objRpt.LKAircraftRegistrationID, objRpt.InvestigationFileNumber, objRpt.LKAirportID,

                                                                  objRpt.LKOccurrenceCategorizationId).ToList();

                    ViewBag.count_SRpagecontent = objRpt.ExcelInfo.Count();
                }
                else if (Rptid == "Noti")
                {
                    TempData["Active_Admin_Page"] = "NotiReport";

                    ViewBag.Report_Name = "Notification Report";

                    if (objRpt.NotificationNo == null) { objRpt.NotificationNo = string.Empty; }

                    objRpt.ExcelOccInfo = dc.USP_VIEW_OCCURRENCEDETAILS_FOR_EXCEEXPORT(Convert.ToString(objRpt.NotificationNo), objRpt.LKAircraftRegistrationID, objRpt.FromDate, objRpt.ToDate).ToList();

                    ViewBag.count_SRpagecontent = objRpt.ExcelOccInfo.Count();
                }

                var Registration_id = dc.LKAircraftRegistrations.ToList().OrderBy(x => x.LKAircraftRegistrationName);
                SelectList Registrationid = new SelectList(Registration_id, "LKAircraftRegistrationID", "LKAircraftRegistrationName");
                ViewBag.Registration_id = Registrationid;

                var airport_id = dc.LKAirports.ToList().OrderBy(x => x.LKAirportName);
                SelectList airportid = new SelectList(airport_id, "LKAirportID", "LKAirportName");
                ViewBag.Airport_list = airportid;

                var occ_title = dc.LKOccurrenceCategorizations.ToList().OrderBy(x => x.LKOccurrenceCategorizationId);
                SelectList occTitle = new SelectList(occ_title, "LKOccurrenceCategorizationId", "OccurrenceCategorization");
                ViewBag.OccurCateg_list = occTitle;
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("Forms", "InvestigationReport", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }
            return View(objRpt);
        }
        /// <summary>
        /// common report for Notification ,Investigation and safety recommendation 
        /// </summary>
        /// <param name="objRpt">Specify type of report </param>
        /// <param name="page">Receive the common page number</param>
        /// <param name="btnname">Receive the button whether it is show or export</param>      
        /// <returns></returns>
        [HttpPost]
        public ActionResult InvestigationReport(Reports objRpt, int? page, string btnname)
        {
            try
            {
                var Registration_id = dc.LKAircraftRegistrations.ToList().OrderBy(x => x.LKAircraftRegistrationName);
                SelectList Registrationid = new SelectList(Registration_id, "LKAircraftRegistrationID", "LKAircraftRegistrationName");
                ViewBag.Registration_id = Registrationid;

                var airport_id = dc.LKAirports.ToList().OrderBy(x => x.LKAirportName);
                SelectList airportid = new SelectList(airport_id, "LKAirportID", "LKAirportName");
                ViewBag.Airport_list = airportid;

                var occ_title = dc.LKOccurrenceCategorizations.ToList().OrderBy(x => x.LKOccurrenceCategorizationId);
                SelectList occTitle = new SelectList(occ_title, "LKOccurrenceCategorizationId", "OccurrenceCategorization");
                ViewBag.OccurCateg_list = occTitle;

                ViewBag.FromDate = objRpt.FromDate.ToString("dd/MM/yyyy");

                objRpt.FromDate = Convert.ToDateTime(objRpt.FromDate.ToString("dd/MMM/yyyy"));

                ViewBag.ToDate = objRpt.ToDate.ToString("dd/MM/yyyy");

                objRpt.ToDate = Convert.ToDateTime(objRpt.ToDate.ToString("dd/MMM/yyyy"));

                TempData["BackPage"] = "Report";

                if (objRpt.IsSrReport == "SR")
                {
                    TempData["Active_Admin_Page"] = "SRReport";

                    ViewBag.Report_Name = "Safety Recommendation Report";

                    if (btnname == "Show")
                    {
                        if (page == null)
                        {
                            ViewBag.SR_serial_nbr = 0;
                        }
                        else
                        {
                            ViewBag.SR_serial_nbr = 10 * (page - 1);
                        }

                        if (objRpt.InvestigationFileNumber == null) { objRpt.InvestigationFileNumber = string.Empty; }
                        if (objRpt.SR_Number == null) { objRpt.SR_Number = string.Empty; }

                        objRpt.ExcelInfo = dc.USP_VIEW_SR_FOR_EXCEEXPORT(objRpt.SR_Number, objRpt.LKAircraftRegistrationID, objRpt.InvestigationFileNumber, objRpt.LKAirportID,

                                                                      objRpt.LKOccurrenceCategorizationId).ToList();

                        ViewBag.count_SRpagecontent = objRpt.ExcelInfo.Count();
                    }
                    else if (btnname == "Export")
                    {
                        DataTable dt = new DataTable("SRReport");

                        dt.Columns.AddRange(new DataColumn[11] { new DataColumn("Safety Recommendation Number"),
                           new DataColumn("Assigned Investigator Name"),
                           new DataColumn("Raised By Person"),
                           new DataColumn("Current Status"),
                           new DataColumn("Raised Date"),
                           new DataColumn("Raised Against (Department or Supplier)"),
                           new DataColumn("Investigation File Number"),
                           new DataColumn("Description of Occurrence"),
                           new DataColumn("SR Content"),
                           new DataColumn("Aircraft Registration "),
                           new DataColumn("Aircraft Model"),

                        });

                        if (objRpt.InvestigationFileNumber == null) { objRpt.InvestigationFileNumber = string.Empty; }
                        if (objRpt.SR_Number == null) { objRpt.SR_Number = string.Empty; }

                        var srinfo = dc.USP_VIEW_SR_FOR_EXCEEXPORT(objRpt.SR_Number, objRpt.LKAircraftRegistrationID, objRpt.InvestigationFileNumber, objRpt.LKAirportID,

                                                                   objRpt.LKOccurrenceCategorizationId).ToList();

                        foreach (var dr in srinfo)
                        {
                            dt.Rows.Add(dr.SafetyRecommendationNumber, dr.AssignedInvestigator, dr.AssignedInvestigator, dr.CurrentsrStatus, dr.SRRaisedDate, dr.RaisedAgainst, dr.InvestigationFileNumber,

                                        RemoveHtmlTags(dr.OccurrenceDescription), RemoveHtmlTags(dr.SafetyRecommendation), dr.RegistrationName, dr.AIRCRAFTMODELS);
                        }

                        using (XLWorkbook wb = new XLWorkbook())
                        {
                            wb.Worksheets.Add(dt);

                            using (MemoryStream stream = new MemoryStream())
                            {
                                wb.SaveAs(stream);
                                return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "SR_" + DateTime.Now + ".xlsx");
                            }
                        }
                    }
                }
                else if (objRpt.IsSrReport == "INV")
                {
                    ViewBag.Report_Name = "Investigation Report";

                    TempData["Active_Admin_Page"] = "InvReport";

                    if (btnname == "Show")
                    {
                        if (page == null)
                        {
                            ViewBag.SR_serial_nbr = 0;
                        }
                        else
                        {
                            ViewBag.SR_serial_nbr = 10 * (page - 1);
                        }

                        if (objRpt.InvestigationFileNumber == null) { objRpt.InvestigationFileNumber = string.Empty; }

                        objRpt.ExcelInvInfo = dc.USP_VIEW_INVESTIGATIONDETAILS_FOR_EXCEEXPORT(Convert.ToString(objRpt.InvestigationFileNumber), objRpt.LKAircraftRegistrationID, objRpt.FromDate, objRpt.ToDate).ToList();

                        ViewBag.count_SRpagecontent = objRpt.ExcelInvInfo.Count();
                    }
                    else if (btnname == "Export")
                    {
                        DataTable dt = new DataTable("InvestigationReport");

                        dt.Columns.AddRange(new DataColumn[10] { new DataColumn("Investigation File Number"),
                                            new DataColumn("Notification File Number"),
                                            new DataColumn("Investigation Status"),
                                            new DataColumn("Place Of Incident"),
                                            new DataColumn("Investigator Name"),
                                            new DataColumn("Aircraft Registration"),
                                            new DataColumn("Aircraft Model"),
                                            new DataColumn("Notifier Call Date"),
                                            new DataColumn("UTC Date And Time Of Occurrence"),
                                            new DataColumn("Local Date And Time Of Occurrence")
                                            });

                        if (objRpt.InvestigationFileNumber == null) { objRpt.InvestigationFileNumber = string.Empty; }

                        var repinfo = dc.USP_VIEW_INVESTIGATIONDETAILS_FOR_EXCEEXPORT(Convert.ToString(objRpt.InvestigationFileNumber), objRpt.LKAircraftRegistrationID, objRpt.FromDate, objRpt.ToDate).ToList();

                        foreach (var dr in repinfo)
                        {
                            dt.Rows.Add(dr.InvestigationFileNumber, dr.NotificationID, dr.InvestigationStatus, dr.PlaceOfIncident,

                                dr.InvestigatorName, dr.RegistrationName, dr.AIRCRAFTMODELS, dr.NotifierCallDate, dr.UTCDateAndTimeOfOccurrence, dr.LocalDateAndTimeOfOccurrence);
                        }

                        using (XLWorkbook wb = new XLWorkbook())
                        {
                            wb.Worksheets.Add(dt);

                            using (MemoryStream stream = new MemoryStream())
                            {
                                wb.SaveAs(stream);
                                return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "InvestigationReport_" + DateTime.Now + ".xlsx");
                            }
                        }
                    }
                }
                else if (objRpt.IsSrReport == "Noti")
                {
                    ViewBag.Report_Name = "Notification Report";

                    TempData["Active_Admin_Page"] = "NotiReport";

                    if (btnname == "Show")
                    {
                        if (page == null)
                        {
                            ViewBag.SR_serial_nbr = 0;
                        }
                        else
                        {
                            ViewBag.SR_serial_nbr = 10 * (page - 1);
                        }

                        if (objRpt.NotificationNo == null) { objRpt.NotificationNo = string.Empty; }

                        objRpt.ExcelOccInfo = dc.USP_VIEW_OCCURRENCEDETAILS_FOR_EXCEEXPORT(Convert.ToString(objRpt.NotificationNo), objRpt.LKAircraftRegistrationID,(objRpt.FromDate), (objRpt.ToDate)).ToList();

                        ViewBag.count_SRpagecontent = objRpt.ExcelOccInfo.Count();
                    }
                    else if (btnname == "Export")
                    {
                        DataTable dt = new DataTable("NotificationReport");

                        dt.Columns.AddRange(new DataColumn[11] {
                                            new DataColumn("Notification Number"),
                                            new DataColumn("Place Of Incident"),
                                            new DataColumn("Occurrence Category"),
                                            new DataColumn("Notifier Name"),
                                            new DataColumn("Aircraft Registration"),
                                            new DataColumn("Aircraft Model"),
                                            new DataColumn("Occurrence Status"),
                                            new DataColumn("Notifier Call Date"),
                                            new DataColumn("UTC Date And Time Of Occurrence"),
                                            new DataColumn("Local Date And Time Of Occurrence"),
                                            new DataColumn("Description of Occurrence"),
                                            });

                        if (objRpt.NotificationNo == null) { objRpt.NotificationNo = string.Empty; }

                        var repinfo = dc.USP_VIEW_OCCURRENCEDETAILS_FOR_EXCEEXPORT(Convert.ToString(objRpt.NotificationNo), objRpt.LKAircraftRegistrationID, objRpt.FromDate, objRpt.ToDate).ToList();

                        foreach (var dr in repinfo)
                        {
                            dt.Rows.Add(dr.NotificationID, dr.PlaceOfIncident, dr.OccurrenceCategorization, dr.Notifier,

                                dr.RegistrationName, dr.AIRCRAFTMODELS, dr.OccStatus, dr.NotifierCallDate, dr.UTCDateAndTimeOfOccurrence, dr.LocalDateAndTimeOfOccurrence, dr.OccurrenceDescription);
                        }

                        using (XLWorkbook wb = new XLWorkbook())
                        {
                            wb.Worksheets.Add(dt);

                            using (MemoryStream stream = new MemoryStream())
                            {
                                wb.SaveAs(stream);
                                return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "NotificationReport_" + DateTime.Now + ".xlsx");
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("Forms", "InvestigationReport", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }

            return View(objRpt);
        }
        /// <summary>
        /// get All Investigation By No
        /// </summary>
        /// <param name="invno">Receive the investigation file number</param>
        /// <returns></returns>
        /// 
        public JsonResult getAllInvestigationByNo(string invno)
        {
            string Investigations = string.Empty;
            try
            {
                var Inv_Use = dc.USP_VIEW_ALL_INVESTIGATIONS(invno).ToList();
                JavaScriptSerializer JSS = new JavaScriptSerializer();
                Investigations = JSS.Serialize(Inv_Use);
            }
            catch (System.Exception ex)
            {
                Investigations = "";
                comm.Exception_Log("Forms", "getInvestigations", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }
            return Json(Investigations, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Remove Html Tags - used for export excel purpose
        /// </summary>
        /// <param name="strHtml">Receive the html input</param>
        /// <returns>Get normal text</returns>
        public static string RemoveHtmlTags(string strHtml)
        {
            string strText = string.Empty;

            try
            {
                strText = Regex.Replace(strHtml, "<(.|\n)*?>", String.Empty);
                strText = HttpUtility.HtmlDecode(strText);
                strText = Regex.Replace(strText, @"\s+", " ");
            }
            catch
            {
                throw;
            }
            finally
            {

            }
            return strText;
        }
    }
}
