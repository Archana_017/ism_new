//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ISM_Project.Models
{
    using System;
    
    public partial class USP_VIEW_TASK_ASSIGNMENT_MULTIPLE_FOR_TEAMMEMBER_Result
    {
        public string InvestigationFileNumber { get; set; }
        public int InvestigationTaskGroupEventID { get; set; }
        public int InvestigationFileID { get; set; }
        public Nullable<System.DateTime> LocalDateAndTimeOfOccurrence { get; set; }
        public Nullable<System.DateTime> UTCDateAndTimeOfOccurrence { get; set; }
        public Nullable<int> InvestigationTaskGroupID { get; set; }
        public string InvestigationTaskGroupEventUniqueNumber { get; set; }
        public int TeamMemberID { get; set; }
        public string InvestigationTaskGroupEventJSON { get; set; }
    }
}
