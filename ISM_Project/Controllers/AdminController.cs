﻿using ISM_Project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.IO;
using System.Data;
using ClosedXML.Excel;

namespace ISM_Project.Controllers
{
    /// <summary>
    /// Admin Controller - Contains functionality of user creation , Global disrtibution list and Inventory module
    /// </summary> 
    /// 
    public class AdminController : Controller
    {
        #region DB object,some common class object and sharepoint attached file path declaration. 
        gcaa_ismEntities dc = new gcaa_ismEntities();
        Common comm = new Common();
        MailConfiguration mail = new MailConfiguration();
        static string[] SharepointfilePath = new string[100];
        static string[] SharepointAnonymousPath = new string[100];
        static int[] DocumentTypeId = new int[100];
        #endregion

        /// <summary>
        /// Display all global distribution list details as table view
        /// </summary> 
        ///         
        public ActionResult Global_DL()
        {
            return View();
        }
        /// <summary>
        /// Save the new user details and update existing user details
        /// </summary> 
        /// 
        /// <param name="adm">Used to indicate Admin Model object.</param>
        public JsonResult SaveAdminform(AdminModel adm)
        {
            using (gcaa_ismEntities dc = new gcaa_ismEntities())
            {
                int director_count = 0;
                int admin_count = 0;
                if (Convert.ToInt32(Session["UserId"]) > 0)
                {
                    var CheckTempDirector = 0;

                    if (adm.ActAsDirector == true)
                    {
                        CheckTempDirector = dc.Users.Where(a => a.ActAsDirector == true && a.UserID != adm.user_id).Count();
                    }

                    if (CheckTempDirector == 1)
                    {
                        return Json(7);// Temp Director status
                    }

                    if (adm.ActAsDirector == true && adm.roleid != 4)
                    {
                        return Json(8);// Temp Director status
                    }

                    if (adm.roleid == 2)
                    {
                        var CheckDIExistence = dc.Users.Where(a => a.UserRoleID == 2 && a.UserID != adm.user_id).FirstOrDefault();

                        if (CheckDIExistence != null)
                        {
                            return Json(9); // Check DI Existence
                        }

                        if (adm.is_active == 0)
                        {
                            return Json(10); // Check DI Active                             
                        }
                    }

                    if (adm.roleid == 6)
                    {
                        if (adm.is_active == 1)
                        {
                            var CheckADGActive = dc.Users.Where(a => a.UserRoleID == 6 && a.Status == 1 && a.UserID != adm.user_id).FirstOrDefault();

                            if (CheckADGActive != null)
                            {
                                return Json(11); // Check ADG Active
                            }
                        }

                        //var CheckADGExistence = dc.Users.Where(a => a.UserRoleID == 6 && a.UserID != adm.user_id).FirstOrDefault();

                        //if (CheckADGExistence != null)
                        //{
                        //    return Json(12); // Check ADG Existence
                        //}
                    }

                    if (adm.user_id == 0)
                    {
                        string add = "ADD";
                        string password_ = comm.Passwordgenrator();
                        if (adm.roleid == 3)
                        {
                            var daai_result = dc.Users.Where(x => x.UserRoleID == adm.roleid && x.Status == 1).Select(a => a.UserID).ToList();
                            director_count = daai_result.Count();
                        }
                        if (director_count == 0)
                        {
                            var search_exst_email = dc.Users.Where(x => x.EmailAddress == adm.email).Select(a => a.FirstName).ToList();
                            var search_exst_staff_id = dc.Users.Where(x => x.StaffId == adm.Staff_id).Select(a => a.FirstName).ToList();
                            if (search_exst_email.Count() == 0 && search_exst_staff_id.Count() == 0)
                            {
                                LoginPassword lpwd = new LoginPassword();
                                string pwd_salt = lpwd.createsalt(8);
                                string hash_pwd = lpwd.Generatehash(password_, pwd_salt);
                                System.Data.Entity.Core.Objects.ObjectParameter user_id = new System.Data.Entity.Core.Objects.ObjectParameter("userid_", typeof(Int32));

                                dc.USP_INST_USER_ADMIN_PREVILAGE(adm.roleid, adm.Staff_id, adm.first_name, adm.middle_name, adm.last_name,
                                    adm.email, adm.dob, pwd_salt, hash_pwd, adm.phone, add, adm.DesingationID, adm.No_of_certificate, adm.No_of_Skill, adm.is_active,
                                   Convert.ToInt32(Session["UserId"]), Convert.ToInt32(Session["UserId"]), user_id, adm.ActAsDirector);

                                if (adm.No_of_certificate > 0)
                                {
                                    for (int i = 0; i < adm.No_of_certificate; i++)
                                    {
                                        String diff = (adm.Renewal_dt[i] - adm.Certified_dt[i]).TotalDays.ToString();
                                        string validity = string.Empty;
                                        if (Convert.ToInt32(diff) > 0)
                                        {
                                            validity = diff + " " + "Days";
                                        }
                                        else
                                        {
                                            validity = "Expired";
                                        }
                                        dc.USP_INST_USER_CERTIFICATE(Convert.ToInt32(user_id.Value), adm.CerticateName[i], adm.Certified_dt[i], adm.Renewal_dt[i], validity,
                                            Convert.ToInt32(Session["UserId"]), Convert.ToInt32(Session["UserId"]));
                                    }
                                }
                                if (adm.No_of_Skill > 0)
                                {
                                    for (int j = 0; j < adm.No_of_Skill; j++)
                                    {
                                        var bresult = dc.USP_INST_USER_SKILL(Convert.ToInt32(user_id.Value), adm.Skillname[j], adm.work_experience[j], adm.Rating[j],
                                              Convert.ToInt32(Session["UserId"]), Convert.ToInt32(Session["UserId"]));
                                    }
                                }
                                List<string> DListUsers = new List<string>();
                                List<string> DListUsersEmail = new List<string>();
                                DListUsersEmail.Add(adm.email);
                                DListUsers.Add(adm.first_name);
                                var rolename = dc.LKUserRoles.Where(a => a.LKUserRoleID == adm.roleid).Select(a => a.LKUserRoleName).FirstOrDefault();
                                string siteURL = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                                bool Isent = mail.SendMailNotification(13, Convert.ToString(Session["FirstName"]), Convert.ToInt32(Session["roleid"]), siteURL, DListUsers, DListUsersEmail, 0, null, adm.email, password_, Convert.ToString(rolename), null);
                                return Json(4);
                            }
                            else
                            {
                                if (search_exst_email.Count > 0)
                                {
                                    return Json(0);//Email_id already exist
                                }
                                else if (search_exst_staff_id.Count() > 0)
                                {
                                    return Json(1);// Staff Id already Exist
                                }
                            }
                        }
                        else
                        {
                            return Json(2);// Director already exist
                        }
                    }
                    else
                    {
                        string add = "ADD";
                        if (adm.roleid == 1 && adm.is_active == 0)
                        {
                            var admin_result = dc.Users.Where(x => x.UserRoleID == adm.roleid && x.Status == 1 && x.UserID != adm.user_id).Select(a => a.UserID).ToList();
                            admin_count = admin_result.Count();
                        }
                        else
                        {
                            admin_count = 1;
                        }
                        if (adm.roleid == 3 && adm.is_active == 1)
                        {
                            var daai_result = dc.Users.Where(x => x.UserRoleID == adm.roleid && x.Status == 1 && x.UserID != adm.user_id).Select(a => a.UserID).ToList();
                            director_count = daai_result.Count();
                        }
                        if (director_count == 0)
                        {
                            if (admin_count > 0)
                            {
                                var search_exst_email = dc.Users.Where(x => x.EmailAddress == adm.email && x.UserID != adm.user_id).Select(a => a.FirstName).ToList();
                                var search_exst_staff_id = dc.Users.Where(x => x.StaffId == adm.Staff_id && x.UserID != adm.user_id).Select(a => a.FirstName).ToList();
                                if (search_exst_email.Count() == 0 && search_exst_staff_id.Count() == 0)
                                {

                                    dc.USP_UPDT_USERS(adm.user_id, adm.roleid, adm.Staff_id, adm.first_name, adm.middle_name, adm.last_name, adm.email, adm.dob, adm.phone, add, adm.DesingationID, adm.No_of_certificate,
                                       adm.No_of_Skill, adm.is_active, Convert.ToInt32(Session["UserId"]), adm.ActAsDirector);

                                    if (adm.No_of_certificate > 0)
                                    {
                                        for (int i = 0; i < adm.No_of_certificate; i++)
                                        {
                                            String diff = (adm.Renewal_dt[i] - adm.Certified_dt[i]).TotalDays.ToString();
                                            string validity = string.Empty;
                                            if (Convert.ToInt32(diff) > 0)
                                            {
                                                validity = diff + " " + "Days";
                                            }
                                            else
                                            {
                                                validity = "Expired";
                                            }
                                            dc.USP_INST_USER_CERTIFICATE(adm.user_id, adm.CerticateName[i], adm.Certified_dt[i], adm.Renewal_dt[i], validity,
                                                Convert.ToInt32(Session["UserId"]), Convert.ToInt32(Session["UserId"]));
                                        }
                                    }
                                    if (adm.No_of_Skill > 0)
                                    {
                                        for (int j = 0; j < adm.No_of_Skill; j++)
                                        {
                                            dc.USP_INST_USER_SKILL(adm.user_id, adm.Skillname[j], adm.work_experience[j], adm.Rating[j],
                                                Convert.ToInt32(Session["UserId"]), Convert.ToInt32(Session["UserId"]));
                                        }
                                    }
                                    return Json(3);
                                }
                                else
                                {
                                    if (search_exst_email.Count > 0)
                                    {
                                        return Json(0);//Email_id already exist
                                    }
                                    else if (search_exst_staff_id.Count() > 0)
                                    {
                                        return Json(1);// Staff Id already Exist
                                    }
                                }
                            }
                            else
                            {
                                return Json(6);
                            }
                        }
                        else
                        {
                            return Json(5);
                        }
                    }
                    return Json(0);// return to login page
                }
                return Json(6);
            }
        }
        /// <summary>
        /// View the user details corresponding user Id
        /// </summary> 
        /// 
        /// <param name="id">Used to indicate User Id.</param>
        public ActionResult AddUser_Admin(int id = 0)
        {
            if (Convert.ToInt32(Session["RoleId"]) == 1)
            {

                var invalidurlcheck = dc.Users.AsEnumerable().Where(a => a.CreatedBy == Convert.ToInt32(Session["Userid"]) &&

                                           a.UserID == id).FirstOrDefault();
                if (invalidurlcheck != null || id == 0)
                {


                    TempData["active_dashboard"] = "create_users";
                    gcaa_ismEntities gc = new gcaa_ismEntities();
                    var skills = gc.LKSkillTypes.ToList().OrderBy(x => x.LKSkillTypeName);
                    SelectList skillist = new SelectList(skills, "LKSkillTypeID", "LKSkillTypeName");
                    ViewBag.skills = skillist;

                    // var userrole = gc.LKUserRoles.ToList().Where(c => c.LKUserRoleID != 3).OrderBy(x => x.LKUserRoleName);
                    var userrole = gc.LKUserRoles.ToList().OrderBy(x => x.LKUserRoleName);
                    SelectList user_role = new SelectList(userrole, "LKUserRoleID", "LKUserRoleName");
                    ViewBag.userrolename = user_role;

                    var UserDesig = gc.LKUserDesignations.ToList().OrderBy(x => x.LKDesignationID);
                    SelectList SelUserDesig = new SelectList(UserDesig, "LKDesignationID", "LKDesignationName");
                    ViewBag.UserDesigList = SelUserDesig;
                    var Ratinglist = new SelectList(new[] { new { ID = "1", Value = "1" },
                new { ID = "2", Value = "2" },
                new { ID = "3", Value = "3" },
                new { ID = "4", Value = "4" },
                new { ID = "5", Value = "5" },
                new { ID = "6", Value = "6" },
                new { ID = "7", Value = "7" },
                new { ID = "8", Value = "8" },
                new { ID = "9", Value = "9" },
                new { ID = "10", Value = "10" },

            }, "ID", "Value", 1);
                    ViewBag.Ratings = Ratinglist;

                    if (id != 0)
                    {
                        ViewBag.title = "User Details";
                        AdminModel admin = new AdminModel();

                        var user_ = gc.USP_VIEW_USERS_ADMIN(id).FirstOrDefault();

                        if (user_ != null)
                        {
                            var user_certificate = gc.USP_VIEW_CERTIFICATION_ADMIN(id).ToList();
                            SelectList usercertificate = new SelectList(user_certificate);
                            ViewBag.Certificateinfo = user_certificate;
                            ViewBag.CertificateinfoCount = usercertificate.Count();
                            var user_skill = gc.USP_VIEW_SKILL_ADMIN(id).ToList();
                            SelectList userskill = new SelectList(user_certificate);
                            ViewBag.skillinfo = user_skill;
                            ViewBag.skillinfoCount = user_skill.Count();
                            admin.first_name = user_.FirstName;
                            admin.middle_name = user_.MiddleName;
                            admin.last_name = user_.LastName;
                            admin.Staff_id = user_.StaffId;
                            DateTime dt = Convert.ToDateTime(user_.DateOfBirth);
                            string date_of_birth = dt.ToString("yyyy-MM-dd");
                            string bithdate = Convert.ToDateTime(user_.DateOfBirth).ToString("yyyy-MM-dd");
                            admin.dob = Convert.ToDateTime(bithdate);
                            ViewBag.birth_date = comm.convertDateTime(Convert.ToDateTime(user_.DateOfBirth));
                            admin.email = user_.EmailAddress;
                            admin.phone = user_.ContactNo;
                            admin.roleid = user_.UserRoleID;
                            admin.is_active = user_.Status;
                            admin.user_id = id;
                            admin.ActAsDirector = Convert.ToBoolean(user_.ActAsDirector);
                            admin.roleName = gc.LKUserRoles.Where(ro => ro.LKUserRoleID == user_.UserRoleID).Select(ro => ro.LKUserRoleName).FirstOrDefault();
                            ViewBag.DesingationID = gc.LKUserDesignations.Where(des => des.LKDesignationID == user_.UserDesignationID).Select(des => des.LKDesignationID).FirstOrDefault();
                        }
                        return View(admin);
                    }
                    else
                    {
                        ViewBag.title = "Create User";
                        AdminModel newadmin = new AdminModel();
                        newadmin.user_id = id;
                        return View(newadmin);
                    }
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            else
            {
                return RedirectToAction("Dashboard", "Home");
            }
        }
        /// <summary>
        ///View all the common Global distribution list details for admin user
        /// </summary>         
        public ActionResult Global_DistributionList()
        {
            if (Convert.ToInt32(Session["RoleId"]) == 1)
            {
                TempData["active_dashboard"] = "global_DL";
                Session["act_dashboard"] = "global_DL";
                return View(dc.USP_VIEW_INVESTIGATION_DISTRIBUTIONLIST(0));
            }
            else
            {
                return RedirectToAction("Dashboard", "Home");
            }
        }
        /// <summary>
        /// View new global distribution list only for admin user
        /// </summary>       
        /// <param name="Id">Used to indicate Global Distribution List Id.</param>
        [HttpGet]
        public ActionResult CreateGlobal_DistributionList(int Id = 0)
        {
            if (Convert.ToInt32(Session["RoleId"]) == 1)
            {
                InvDistributionList DistList = new InvDistributionList();

                try
                {
                    if (Id > 0)
                    {
                        var _dislistInfo = dc.EmailDistributionLists.AsEnumerable().Where(a => a.EmailDistributionListID == Id).FirstOrDefault();

                        var UserInfo = (from didlist in dc.EmailDistrubutionListRecipients where didlist.EmailDistrubutionListID == Id select new { didlist.RecipientName });

                        DistList.EmailDistributionListName = _dislistInfo.EmailDistributionListName;

                        DistList.EmailDistributionListDescription = _dislistInfo.EmailDistributionListDescription;

                        DistList.InvestigationFileID = 0; DistList.LKDistributionListTypeID = 1; DistList.EmailDistributionListID = Id;

                        SelectList User_Info = new SelectList(UserInfo, "RecipientName", "RecipientName");

                        ViewBag.User_Info = User_Info;
                    }
                    else
                    {
                        DistList.Recipients_Info = null;

                        DistList.EmailDistributionListID = 0;

                        DistList.InvestigationFileID = 0;

                        ViewBag.User_Info = null;
                    }
                }
                catch (System.Exception ex)
                {
                    comm.Exception_Log("Home", "SaveGlobal_DistrList", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                    return RedirectToAction("NotFound", "Error");
                }

                return View(DistList);
            }
            else
            {
                return RedirectToAction("Dashboard", "Home");
            }
        }
        /// <summary>
        /// Save new global distribution list only for admin user
        /// </summary> 
        /// <param name="InvDist">Used to indicate Global Distribution List Object .</param>
        [HttpPost]
        public ActionResult CreateGlobal_DistributionList(InvDistributionList InvDist)
        {
            try
            {
                var result = 0;

                if (Convert.ToInt32(Session["UserId"]) > 0)
                {
                    if (InvDist.EmailDistributionListID > 0)
                    {
                        result = dc.USP_INST_INVESTIGATION_EMAILDISTRIBUTIONLIST(InvDist.EmailDistributionListID, InvDist.InvestigationFileID, 1, InvDist.EmailDistributionListName,

                                 InvDist.EmailDistributionListDescription, Convert.ToInt32(Session["UserId"]));

                        if (result >= 1)
                        {
                            var OldRecipientsList = dc.EmailDistrubutionListRecipients.Where(a => a.EmailDistrubutionListID == InvDist.EmailDistributionListID).ToList();

                            if (OldRecipientsList != null)
                            {
                                foreach (var i in OldRecipientsList)
                                {
                                    dc.EmailDistrubutionListRecipients.Remove(i);
                                }

                                dc.SaveChanges();
                            }

                            if (InvDist.Recipients_Info != null)
                            {
                                string[] Recip_Info = InvDist.Recipients_Info;

                                foreach (string recipName in Recip_Info)
                                {
                                    var user_Info = dc.Users.AsQueryable().Where(a => a.FirstName == recipName).FirstOrDefault();

                                    string EmailAddress = string.Empty; int userId = 0;

                                    if (user_Info != null)
                                    {
                                        EmailAddress = user_Info.EmailAddress;

                                        userId = user_Info.UserID;
                                    }
                                    else
                                    {
                                        EmailAddress = recipName;
                                    }

                                    if (recipName != null)
                                    {
                                        if (recipName != string.Empty)
                                        {
                                            result = dc.USP_INST_INVESTIGATION_EMAILDISTRIBUTIONLIST_RECEPIENT(0, InvDist.EmailDistributionListID,

                                               userId, EmailAddress, Convert.ToInt32(Session["UserId"]), recipName);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        result = dc.USP_INST_INVESTIGATION_EMAILDISTRIBUTIONLIST(0, InvDist.InvestigationFileID, 1, InvDist.EmailDistributionListName,

                                  InvDist.EmailDistributionListDescription, Convert.ToInt32(Session["UserId"]));

                        InvDist.EmailDistributionListID = dc.EmailDistributionLists.DefaultIfEmpty().Max(a => a.EmailDistributionListID);

                        if (result >= 1)
                        {
                            if (InvDist.Recipients_Info != null)
                            {
                                string[] Recip_Info = null;

                                if (InvDist.Recipients_Info[0].ToString().Contains(','))
                                {
                                    Recip_Info = InvDist.Recipients_Info[0].ToString().Split(',');
                                }
                                else
                                {
                                    Recip_Info = InvDist.Recipients_Info;
                                }

                                foreach (string recipName in Recip_Info)
                                {
                                    var user_Info = dc.Users.AsQueryable().Where(a => a.FirstName == recipName).FirstOrDefault();

                                    string EmailAddress = string.Empty; int userId = 0;

                                    if (user_Info != null)
                                    {
                                        EmailAddress = user_Info.EmailAddress;

                                        userId = user_Info.UserID;
                                    }
                                    else
                                    {
                                        EmailAddress = recipName;
                                    }

                                    if (recipName != null)
                                    {
                                        if (recipName != string.Empty)
                                        {
                                            result = dc.USP_INST_INVESTIGATION_EMAILDISTRIBUTIONLIST_RECEPIENT(0, InvDist.EmailDistributionListID,

                                        userId, EmailAddress, Convert.ToInt32(Session["UserId"]), recipName);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("Home", "SaveGlobal_DistrList", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("NotFound", "Error");
            }

            return RedirectToAction("Global_DistributionList", "Admin");
        }
        /// <summary>
        /// Delete global distribution list only for admin user
        /// </summary> 
        /// 
        /// <param name="Id">Used to indicate Global Distribution List id .</param>
        public ActionResult DeleteDistributionlist(Int32 Id)
        {
            string results = string.Empty;

            int? InvestigationId = 0;

            try
            {
                if (Convert.ToInt32(Session["UserId"]) > 0)
                {
                    if (Convert.ToInt32(Id) > 0)
                    {
                        var OldRecipientsList = dc.EmailDistrubutionListRecipients.Where(a => a.EmailDistrubutionListID == Id).ToList();

                        var OldDistList = dc.EmailDistributionLists.Where(a => a.EmailDistributionListID == Id).ToList();

                        InvestigationId = OldDistList.SingleOrDefault().InvestigationFileID;

                        if (OldRecipientsList != null)
                        {
                            foreach (var i in OldRecipientsList)
                            {
                                dc.EmailDistrubutionListRecipients.Remove(i);
                            }

                            foreach (var j in OldDistList)
                            {
                                dc.EmailDistributionLists.Remove(j);
                            }

                            dc.SaveChanges();
                        }

                        results = "success";

                    }
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("", "", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("NotFound", "Error");
            }

            return RedirectToAction("Global_DistributionList", "Admin");

        }
        /// <summary>
        /// Validate Global Recipience Email in global distribution list 
        /// </summary> 
        /// <param name="RecEmail">Global Recipience Email as string array.</param>
        public int ValidateGlobalRecipienceEmail(string[] RecEmail)
        {
            int result = 1;

            try
            {
                string[] RecEmailAddress = null;

                bool IsSingleValue = RecEmail[0].ToString().Contains(',');

                if (IsSingleValue)
                {
                    RecEmailAddress = RecEmail[0].ToString().Split(',');
                }
                else
                {
                    RecEmailAddress = RecEmail;
                }

                foreach (string RecName in RecEmailAddress)
                {
                    if (RecName != string.Empty)
                    {
                        var user_Info = dc.Users.AsQueryable().Where(a => a.FirstName == RecName).FirstOrDefault();

                        if (user_Info == null)
                        {
                            bool retval = IsValidEmail(RecName);

                            if (!retval)
                            {
                                return result = 0;
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("TaskController", "ValidateRecipienceEmail", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }

            return result;
        }
        /// <summary>
        /// Validate user Email address valid or invalid
        /// </summary> 
        /// 
        /// <param name="strIn">Receive the Global Recipience Email and User email address.</param>
        public bool IsValidEmail(string strIn)
        {
            // Return true if strIn is in valid email format.
            try
            {
                return Regex.IsMatch(strIn,
                      @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                      @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-0-9a-z]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                      RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }
        /// <summary>
        /// View All Inventories details as list view
        /// </summary> 
        /// <param name="page">Receive the page no as interger .</param>
        public ActionResult ViewAllInventories(int? page)
        {
            var Invs = new InventoryModel();
            try
            {
                if (page == null)
                {
                    ViewBag.Inv_serial_nbr = 0;
                }
                else
                {
                    ViewBag.Inv_serial_nbr = 10 * (page - 1);
                }
                Invs = new InventoryModel()
                {
                    All_Inv = dc.USP_VIEW_ALL_INVENTORIES().ToList(),
                };
                ViewBag.Inv_page_no = page;
                ViewBag.count_Invcontent = Invs.All_Inv.Count();
                Session["Inventory"] = Invs.All_Inv;
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("AdminController", "ViewAllInventories", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserID"]));

                return RedirectToAction("PageNotFound", "Error");
            }
            return View(Invs);
        }
        /// <summary>
        /// Search a item in all investigation inventories list
        /// </summary> 
        /// <param name="searchInv">Receive the search key value .</param>
        /// <param name="page">Receive the page no as interger .</param>
        [HttpPost]
        public ActionResult ViewAllInventories(InventoryModel searchInv, int? page, string btnname)
        {
            var InvInfo = new InventoryModel();
            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 1 || Convert.ToInt32(Session["RoleId"]) == 3)
                {
                    if (page == null)
                    {
                        ViewBag.Inv_serial_nbr = 0;
                    }
                    else
                    {
                        ViewBag.Inv_serial_nbr = 10 * (page - 1);
                    }
                    string searchText = searchInv.Item;
                    InvInfo.All_Inv = (List<USP_VIEW_ALL_INVENTORIES_Result>)Session["Inventory"];
                    if (!string.IsNullOrEmpty(searchText))
                    {
                        InvInfo.All_Inv = InvInfo.All_Inv.Where(x => string.Equals(x.Item, searchText, StringComparison.CurrentCultureIgnoreCase) || x.Item.ToLower().Contains(searchText.ToLower()) || string.Equals(Convert.ToString(x.ItemNo), searchText, StringComparison.CurrentCultureIgnoreCase) || Convert.ToString(x.ItemNo).ToLower().Contains(searchText.ToLower()) || string.Equals(x.CategoryName, searchText, StringComparison.CurrentCultureIgnoreCase) || x.CategoryName.ToLower().Contains(searchText.ToLower()) || Convert.ToString(x.ItemNo).ToLower().Contains(searchText.ToLower()) || string.Equals(x.Description, searchText, StringComparison.CurrentCultureIgnoreCase) || x.Description.ToLower().Contains(searchText.ToLower())).ToList();
                    }
                    ViewBag.Inv_page_no = page;
                    ViewBag.count_Invcontent = InvInfo.All_Inv.Count();
                    TempData["SearchKey"] = searchText;
                    if (btnname == "Export")
                    {
                        DataTable dt = new DataTable("Inventories");
                        dt.Columns.AddRange(new DataColumn[4] {
                                            new DataColumn("Item"),
                                            new DataColumn("Category"),
                                            new DataColumn("Availability"),
                                            new DataColumn("Quantity"),
                                            });
                        foreach (var dr in InvInfo.All_Inv)
                        {
                            dt.Rows.Add(dr.Item, dr.CategoryName, dr.Availability, dr.Quantity);
                        }
                        using (XLWorkbook wb = new XLWorkbook())
                        {
                            wb.Worksheets.Add(dt);

                            using (MemoryStream stream = new MemoryStream())
                            {
                                wb.SaveAs(stream);
                                return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Inventories" + DateTime.Now + ".xlsx");
                            }
                        }
                    }
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("AdminController", "ViewAllInventories", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserID"]));

                return RedirectToAction("PageNotFound", "Error");
            }
            return View(InvInfo);
        }
        /// <summary>
        /// Add new item in to investigation inventory list
        /// </summary> 
        /// <param name="id">Receive the inventory id as integer .</param>
        public ActionResult AddInventory(int id)
        {
            var invent = new InventoryModel();
            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 1)
                {
                    if (id > 0)
                    {
                        invent = new InventoryModel()
                        {
                            View_Inv = dc.USP_VIEW_INVENTORY(id).ToList(),
                            Inv_Attach = dc.USP_INVENTORY_ATTACHMENTS("view", id, null, null, null, null).ToList(),
                            Inve_Updt = dc.USP_VIEW_INVENTORY_QUANTITY_HISTORY(id).ToList(),
                            Inve_Use = dc.USP_INVENTORY_INVESTIGATION("View", id, null, null, null, null, null, null, null).ToList()
                        };
                        foreach (var inv in invent.View_Inv)
                        {
                            invent.InventoryID = inv.InventoryID;
                            invent.Availability = inv.Availability;
                            invent.Category = inv.Category;
                            invent.CategoryName = inv.CategoryName;
                            invent.Comments = inv.Comments;
                            invent.Description = inv.Description;
                            invent.ExpiredItems = inv.ExpiredItems;
                            invent.RequiredItems = inv.RequiredItems;
                            if (inv.ExpiryDate != null)
                                ViewBag.ExpiryDate = comm.convertDateTime(Convert.ToDateTime(inv.ExpiryDate));
                            invent.Item = inv.Item;
                            invent.ItemNo = inv.ItemNo;
                            invent.PartNo = inv.PartNo;
                            invent.Quantity = inv.Quantity;
                            if (inv.Quantity.Contains(' '))
                            {
                                ViewBag.Quantity = inv.Quantity.Split(' ')[0];
                                ViewBag.QuantityType = inv.Quantity.Split(' ')[1];
                                string quantype = inv.Quantity.Split(' ')[1];
                                invent.QuantityID = dc.LKInventoryQuantities.Where(a => a.QuantityType == quantype).Select(a => a.LKQuantityID).FirstOrDefault();
                            }
                            else
                            {
                                ViewBag.Quantity = null;
                                ViewBag.QuantityType = null;
                                invent.QuantityID = 0;
                            }
                            invent.SNo = inv.SNo;
                        }
                        ViewBag.InventoryID = id;
                        TempData["BackPage"] = "Inventory";
                        ViewBag.InveUpdtHistory = invent.Inve_Updt.Count();
                        ViewBag.InveUseHIst = invent.Inve_Use.Count();
                    }
                    else { ViewBag.InventoryID = 0; }
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("AdminController", "AddInventory", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserID"]));

                return RedirectToAction("PageNotFound", "Error");
            }
            return View(invent);
        }
        /// <summary>
        /// Save new item in to investigation inventory list
        /// </summary> 
        /// <param name="InvMod">Receive the investigation inventory model values .</param>
        [HttpPost]
        public ActionResult AddInventory(InventoryModel InvMod)
        {
            try
            {
                string siteURL = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                if (Convert.ToInt32(Session["RoleId"]) == 1)
                {
                    if (Convert.ToInt32(Session["UserID"]) > 0)
                    {
                        if (InvMod.Category == 0)
                        {
                            dc.USP_INST_INV_CATEGORY(InvMod.CategoryName, Convert.ToInt32(Session["UserID"]));
                            var categoryID = dc.LKInventoryCategories.Where(a => a.CategoryName == InvMod.CategoryName).Select(a => a.LKCategoryID).FirstOrDefault();
                            InvMod.Category = categoryID;
                        }
                        if (InvMod.QuantityID == 0)
                        {
                            dc.USP_INST_INV_QUAN_TYPE(InvMod.QuantityType, Convert.ToInt32(Session["UserID"]));
                            var Quantity = dc.LKInventoryQuantities.Where(a => a.QuantityType == InvMod.QuantityType).Select(a => a.QuantityType).FirstOrDefault();
                            InvMod.QuantityType = Quantity;
                        }
                        var result = dc.USP_INST_INVENTORY(InvMod.InventoryID, InvMod.Category, InvMod.Item, InvMod.Description, InvMod.PartNo, InvMod.SNo, InvMod.ItemNo, (InvMod.Quantity + " " + InvMod.QuantityType), InvMod.Availability, InvMod.ExpiryDate, InvMod.RequiredItems, InvMod.ExpiredItems, InvMod.Comments, Convert.ToInt32(Session["UserID"]), Convert.ToInt32(Session["UserID"])).FirstOrDefault();
                        saveFilestoSharePoint(Request.Files, Convert.ToString(result.InventoryID));
                        dc.SaveChanges();
                    }
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("AdminController", "AddInventory::POST", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserID"]));

                return RedirectToAction("PageNotFound", "Error");
            }
            return RedirectToAction("ViewAllInventories", "Admin");

            //return View();
        }
        /// <summary>
        /// Upload the Inventory File
        /// </summary> 
        /// <param name="invID">Receive the inventory id .</param>
        [HttpPost]
        public JsonResult UploadInvnFile(int invID)
        {
            try
            {
                comm.CreateIfMissing(Server.MapPath("~/") + "App_Data/Invn_Files/" + invID);

                foreach (string file in Request.Files)
                {
                    var fileContent = Request.Files[file];

                    if (fileContent != null && fileContent.ContentLength > 0)
                    {
                        var stream = fileContent.InputStream;

                        var fileName = Path.GetFileName(file);

                        var path = Path.Combine(Server.MapPath("~/App_Data/Invn_Files/" + invID), fileContent.FileName);

                        using (var fileStream = System.IO.File.Create(path))
                        {
                            stream.CopyTo(fileStream);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("Admin", "UploadInvnFile", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
                return Json("Upload failed");
            }

            return Json("File uploaded successfully");
        }
        /// <summary>
        /// Remove the investigation Inventory file by Inventory item id
        /// </summary> 
        /// <param name="filename">Receive the investigation inventory image file name .</param>
        /// <param name="Invn_ID">Receive the investigation inventory id .</param>
        [HttpPost]
        public JsonResult RemoveInventoryImages(string filename, int Invn_ID)
        {
            string result = "failed";

            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 1)
                {
                    if (Convert.ToInt32(Session["UserId"]) > 0)
                    {
                        string[] fName = filename.Split('\\');

                        if (fName.Count() >= 2)
                        {
                            var fullpathoffile = Server.MapPath("~/App_Data/Invn_Files/" + Invn_ID + "/" + fName[2]);

                            if (System.IO.File.Exists(fullpathoffile))
                            {
                                System.IO.File.Delete(fullpathoffile);
                            }

                            result = "success";
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("Admin", "RemoveInventoryImages", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }

            return Json(result);
        }
        /// <summary>
        /// Delete the image in investigation inventory details
        /// </summary> 
        /// <param name="id">Receive the investigation inventory id .</param>
        /// <param name="path">Receive the investigation inventory image file path .</param>
        [HttpPost]
        public JsonResult DeleteImages(Int32 id, string path)
        {
            string results = string.Empty;

            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 1)
                {
                    if (Convert.ToInt32(id) > 0)
                    {
                        if (Convert.ToInt32(Session["UserId"]) > 0)
                        {
                            bool IsFileDeleted = comm.deleteFiles(path);

                            var result = dc.USP_INVENTORY_ATTACHMENTS("Delete", Convert.ToInt32(id), 0, null, null, 0).FirstOrDefault();

                            results = "success";
                        }
                    }
                }
                else
                {
                    results = "Failed";

                    return Json(results);
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("DIController", "DeleteAttachment", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }

            return Json(results);
        }
        /// <summary>
        /// Get all the investigation inventory Category
        /// </summary> 
        /// 
        public JsonResult getInvCategory()
        {
            string category = string.Empty;
            try
            {
                var Inv_Cate = dc.USP_VIEW_INV_CATEGORY(0).ToList();
                JavaScriptSerializer JSS = new JavaScriptSerializer();
                category = JSS.Serialize(Inv_Cate);
            }
            catch (System.Exception ex)
            {
                category = "";
                comm.Exception_Log("Admin", "getInvCategory", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }
            return Json(category, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Get all  the investigation inventory quantity type
        /// </summary> 
        /// 
        public JsonResult getInvQuantype()
        {
            string quan_type = string.Empty;
            try
            {
                var Inv_Quan_Type = dc.USP_VIEW_INV_QUAN_TYPE(0).ToList();
                JavaScriptSerializer JSS = new JavaScriptSerializer();
                quan_type = JSS.Serialize(Inv_Quan_Type);
            }
            catch (System.Exception ex)
            {
                quan_type = "";
                comm.Exception_Log("Admin", "getInvQuantype", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }
            return Json(quan_type, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Get all  investigation inventory  
        /// </summary> 
        /// 
        public JsonResult getInvestigations()
        {
            string Investigations = string.Empty;
            try
            {
                var Inv_Use = dc.USP_VIEW_ALL_INVESTIGATIONS_INVENTORY_USE().ToList();
                JavaScriptSerializer JSS = new JavaScriptSerializer();
                Investigations = JSS.Serialize(Inv_Use);
            }
            catch (System.Exception ex)
            {
                Investigations = "";
                comm.Exception_Log("Admin", "getInvestigations", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }
            return Json(Investigations, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Show notification if use inventory is used
        /// </summary> 
        /// 
        public JsonResult getNotifications()
        {
            string Notifications = string.Empty;
            try
            {
                var Inv_Use = dc.USP_VIEW_ALL_NOTIFICATIONS_INVENTORY_USE().ToList();
                JavaScriptSerializer JSS = new JavaScriptSerializer();
                Notifications = JSS.Serialize(Inv_Use);
            }
            catch (System.Exception ex)
            {
                Notifications = "";
                comm.Exception_Log("Admin", "getInvestigations", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }
            return Json(Notifications, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// View the designation in User page
        /// </summary> 
        /// 
        /// <param name="designationID">Receive the user designation id .</param>
        public ActionResult AddDesignation(int designationID = 0)
        {
            AdminModel _adddesg = new AdminModel();

            try
            {
                //var usrDesignation = dc.LKUserDesignations.ToList();

                //SelectList usr_Designation = new SelectList(usrDesignation.ToList(), "LKDesignationID", "LKDesignationName");

                //ViewBag.usr_Designation = usr_Designation;              

                var Design_info = dc.LKUserDesignations.Where(a => a.LKDesignationID == designationID).FirstOrDefault();

                if (Design_info != null)
                {
                    _adddesg.LKDesignationName = Design_info.LKDesignationName;

                    _adddesg.LKDesignationID = Design_info.LKDesignationID;
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("Admin", "AddDesignation", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }

            return View(_adddesg);
        }
        /// <summary>
        /// Save or update new designation in User page
        /// </summary> 
        /// <param name="usrdesig">Receive the user designation id .</param>
        [HttpPost]
        public JsonResult AddDesignations(AdminModel usrdesig)
        {
            AdminModel usr_desig = new AdminModel();

            //var DbTransaction = dc.Database.BeginTransaction();

            try
            {
                var CheckUserDesignation = dc.LKUserDesignations.AsEnumerable().Where(a => a.LKDesignationID == Convert.ToInt32(usrdesig.LKDesignationID) &&

                                         a.LKDesignationName == Convert.ToString(usrdesig.LKDesignationName)

                                          && a.LKDesignationID != usrdesig.LKDesignationID).FirstOrDefault(); // Check designation name both save and update 

                if (CheckUserDesignation == null)
                {
                    if (Convert.ToInt32(usrdesig.LKDesignationID) == 0 && usrdesig.LKDesignationName != string.Empty)
                    {
                        var DesgInfo = dc.LKUserDesignations.Where(a => a.LKDesignationName == usrdesig.LKDesignationName).FirstOrDefault();

                        if (DesgInfo == null)
                        {
                            var result = dc.USP_INST_USER_DESIGNATION(usrdesig.LKDesignationName, 0);
                        }
                        else
                        {
                            var result = dc.USP_INST_USER_DESIGNATION(usrdesig.LKDesignationName, DesgInfo.LKDesignationID);
                        }
                    }
                    else
                    {
                        //update

                        var result = dc.USP_INST_USER_DESIGNATION(Convert.ToString(usrdesig.LKDesignationName), Convert.ToInt32(usrdesig.LKDesignationID));
                    }

                    //DbTransaction.Commit();

                    return Json(1);
                }
                else
                {
                    return Json(0);
                }
            }
            catch (System.Exception ex)
            {
                //DbTransaction.Rollback();

                comm.Exception_Log("Admin", "AddDesignation(AdminModel usrdesig)", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }
            return Json(0);

        }
        /// <summary>
        /// Save attached inventory Files to SharePoint
        /// </summary> 
        /// <param name="files">Receive the attachment file details .</param>
        /// <param name="CONTROL_NUMER">Receive the control number .</param>
        private void saveFilestoSharePoint(HttpFileCollectionBase files, string CONTROL_NUMER)
        {
            try
            {
                if (files.Count > 0)
                {
                    if (string.IsNullOrEmpty(CONTROL_NUMER))
                        CONTROL_NUMER = "0";
                    int filecount = 0;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];

                        if (file.ContentLength > 0 && !string.IsNullOrEmpty(file.FileName))
                        {
                            int fileSize = file.ContentLength;

                            //string fileName = file.FileName;
                            string fileName = file.FileName.Substring(0, file.FileName.LastIndexOf('.')) + "_" + DateTime.Now.ToFileTime() + file.FileName.Substring(file.FileName.LastIndexOf('.'), ((file.FileName.Length) - (file.FileName.LastIndexOf('.'))));

                            string mimeType = file.ContentType.Split('/')[1].ToString();

                            DocumentTypeId[i] = Convert.ToInt32(dc.LKDocumentTypes.AsQueryable().Where(d => d.LKDocumentTypeName == mimeType).Select(d => d.LKDocumentTypeID).FirstOrDefault());

                            Stream fileContent = file.InputStream;

                            string[] attachURLs = comm.savetoSharePointOtherDocsOnPrem(fileContent, fileName, CONTROL_NUMER, "Inventory_Documents");

                            SharepointfilePath[filecount] = attachURLs[0];

                            SharepointAnonymousPath[filecount] = attachURLs[1];
                            if (!string.IsNullOrEmpty(SharepointfilePath[filecount]))
                            {
                                var result = dc.USP_INVENTORY_ATTACHMENTS("Insert", Convert.ToInt32(CONTROL_NUMER), DocumentTypeId[i], SharepointfilePath[filecount], SharepointAnonymousPath[filecount], Convert.ToInt32(Session["UserID"]));

                                dc.SaveChanges();
                            }
                            filecount++;
                        }
                    }
                    //   comm.DeleteDirectory(Server.MapPath("~/") + "CorrespondenceFiles");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("AdminController", "saveFilestoSharePoint", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }
        }
        /// <summary>
        /// Update Quantity History into DB Table
        /// </summary> 
        /// <param name="Available">Receive the available qty from stock .</param>
        /// <param name="Refilled">Receive the refilled qty .</param>
        /// <param name="InvQuanType">Receive the type of quantiy .</param>
        /// <param name="inventoryID">Receive the inventory id .</param>
        [HttpPost]
        public JsonResult UpdateQuantityHistory(string Available, string Refilled, string InvQuanType, int inventoryID)
        {
            string result = string.Empty;
            try
            {
                dc.USP_INVENTORY_QUANTITY_HISTORY("Insert", inventoryID, Available + " " + InvQuanType, (Convert.ToUInt32(Available) + Convert.ToInt32(Refilled)) + " " + InvQuanType, Refilled + " " + InvQuanType, Convert.ToInt32(Session["UserID"]));
                result = "success";
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("AdminController", "UpdateQuantityHistory", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserID"]));
                result = "failure";
            }
            return Json(result);
        }
        /// <summary>
        /// Update Inventory Usage to DB Table
        /// </summary> 
        /// <param name="Used">Receive the Used inventory quantity.</param>
        /// <param name="quantity">Receive the total inventory quantity .</param>
        /// <param name="InvQuanType">Receive the type of quantiy .</param>
        /// <param name="InvestigationID">Receive the investication id .</param>
        /// <param name="inventoryID">Receive the inventory id .</param>
        /// <param name="UsedDate">Receive the inventory used date id .</param>
        /// <param name="Remarks">Receive the remarks of the inventory .</param>
        [HttpPost]
        public JsonResult UpdateInventoryUsage(int Used, int quantity, string InvQuanType, int InvestigationID, int inventoryID, string UsedDate, string Remarks)
        {
            string result = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(UsedDate))
                    dc.USP_INVENTORY_INVESTIGATION("Insert", inventoryID, InvestigationID, null, (quantity - Convert.ToInt32(Used)) + " " + InvQuanType, Used + " " + InvQuanType, Convert.ToDateTime(UsedDate), Remarks, Convert.ToInt32(Session["UserID"]));
                else
                    dc.USP_INVENTORY_INVESTIGATION("Insert", inventoryID, InvestigationID, null, (quantity - Convert.ToInt32(Used)) + " " + InvQuanType, Used + " " + InvQuanType, null, Remarks, Convert.ToInt32(Session["UserID"]));
                result = "success";
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("AdminController", "UpdateInventoryUsage", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserID"]));
                result = "failure";
            }
            return Json(result);
        }
        /// <summary>
        /// View all investigation Inventory Usages
        /// </summary> 
        /// 
        /// <param name="page">Receive the page no of the investigation inventory items .</param>
        /// <param name="id">Receive the investigation inventory id .</param>
        /// 
        public ActionResult ViewInventoryUsage(int? page, int id = 0)
        {
            var Invs = new InventoryModel();
            try
            {
                if (page == null)
                {
                    ViewBag.InvUse_serial_nbr = 0;
                }
                else
                {
                    ViewBag.InvUse_serial_nbr = 10 * (page - 1);
                }
                Invs = new InventoryModel()
                {
                    Inve_Use = dc.USP_INVENTORY_INVESTIGATION("VIEW", id, null, null, null, null, null, null, null).ToList(),
                };
                ViewBag.Inve_page_no = page;
                ViewBag.count_Invecontent = Invs.Inve_Use.Count();
                Session["Inventory_Use"] = Invs.Inve_Use;
                TempData["InventoryId"] = id;
                if (id > 0)
                    TempData["BackPage"] = "Inventory_hist";
                else
                    TempData["BackPage"] = "Inventory";
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("AdminController", "ViewInventoryUsage", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }
            return View(Invs);
        }

        [HttpPost]
        public ActionResult ViewInventoryUsage(int? page, int id, string pageExport)
        {
            var Invs = new InventoryModel();
            try
            {
                if (page == null)
                {
                    ViewBag.InvUse_serial_nbr = 0;
                }
                else
                {
                    ViewBag.InvUse_serial_nbr = 10 * (page - 1);
                }
                Invs = new InventoryModel()
                {
                    Inve_Use = dc.USP_INVENTORY_INVESTIGATION("VIEW", id, null, null, null, null, null, null, null).ToList(),
                };
                List<USP_INVENTORY_INVESTIGATION_Result> itemUsages = dc.USP_INVENTORY_INVESTIGATION("VIEW", id, null, null, null, null, null, null, null).ToList();
                if (pageExport == "Export")
                {
                    DataTable dt = new DataTable("View Item Usage");
                    dt.Columns.AddRange(new DataColumn[7] {
                                            new DataColumn("Item"),
                                            new DataColumn("Investigation Number"),
                                            new DataColumn("Quantity Used"),
                                            new DataColumn("Used Date"),
                                            new DataColumn("Remarks"),
                                            new DataColumn("Updated By"),
                                            new DataColumn("Updated On"),
                                            });
                    foreach (var dr in itemUsages)
                    {
                        dt.Rows.Add(dr.Item, dr.InvestigationNumber, dr.QuantityUsed, dr.UsageDate, dr.Remarks, dr.UpdatedBy, dr.UpdatedOn);
                    }
                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        wb.Worksheets.Add(dt);

                        using (MemoryStream stream = new MemoryStream())
                        {
                            wb.SaveAs(stream);
                            return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Item usage" + DateTime.Now + ".xlsx");
                        }
                    }

                }
                ViewBag.Inve_page_no = page;
                ViewBag.count_Invecontent = Invs.Inve_Use.Count();
                Session["Inventory_Use"] = Invs.Inve_Use;
                TempData["InventoryId"] = id;
                if (id > 0)
                    TempData["BackPage"] = "Inventory_hist";
                else
                    TempData["BackPage"] = "Inventory";
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("AdminController", "ViewInventoryUsage", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }
            return View(Invs);
        }
        /// <summary>
        /// View the  investigation Inventory Quantity History by Inventory Id.
        /// </summary> 
        /// 
        /// <param name="page">Receive the page no of the inventory items .</param>
        /// <param name="id">Receive the inventory id .</param>
        public ActionResult ViewInventoryQuantityHistory(int? page, int id = 0)
        {
            var Invs = new InventoryModel();
            try
            {
                if (page == null)
                {
                    ViewBag.InvQuanHist_serial_nbr = 0;
                }
                else
                {
                    ViewBag.InvQuanHist_serial_nbr = 10 * (page - 1);
                }
                Invs = new InventoryModel()
                {
                    Inve_Updt = dc.USP_VIEW_INVENTORY_QUANTITY_HISTORY(id).ToList(),
                };
                ViewBag.InvQuanHist_page_no = page;
                ViewBag.count_InvQuanHistContent = Invs.Inve_Updt.Count();
                Session["Inventory_Use"] = Invs.Inve_Updt;
                TempData["InventoryId"] = id;
                if (id > 0)
                    TempData["BackPage"] = "Inventory_hist";
                else
                    TempData["BackPage"] = "Inventory";
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("AdminController", "ViewInventoryQuantityHistory", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }
            return View(Invs);
        }

        [HttpPost]
        public ActionResult ViewInventoryQuantityHistory(int? page, int id, string pageExport)
        {
            var Invs = new InventoryModel();
            try
            {
                if (page == null)
                {
                    ViewBag.InvQuanHist_serial_nbr = 0;
                }
                else
                {
                    ViewBag.InvQuanHist_serial_nbr = 10 * (page - 1);
                }
                Invs = new InventoryModel()
                {
                    Inve_Updt = dc.USP_VIEW_INVENTORY_QUANTITY_HISTORY(id).ToList(),
                };
                List<USP_VIEW_INVENTORY_QUANTITY_HISTORY_Result> inventoryHistory = dc.USP_VIEW_INVENTORY_QUANTITY_HISTORY(id).ToList();
                if (pageExport == "Export")
                {
                    DataTable dt = new DataTable("Item History");
                    dt.Columns.AddRange(new DataColumn[5] {
                                            new DataColumn("Item"),
                                            new DataColumn("Available Quantity"),
                                            new DataColumn("Refilled Quality"),
                                            new DataColumn("Updated By"),
                                            new DataColumn("Updated On"),
                                            });
                    foreach (var history in inventoryHistory)
                    {
                        dt.Rows.Add(history.Item, history.Available, history.Refilled, history.UpdatedBy, history.UpdatedOn);

                    }
                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        wb.Worksheets.Add(dt);

                        using (MemoryStream stream = new MemoryStream())
                        {
                            wb.SaveAs(stream);
                            return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "History" + DateTime.Now + ".xlsx");
                        }
                    }
                }

                ViewBag.InvQuanHist_page_no = page;
                ViewBag.count_InvQuanHistContent = Invs.Inve_Updt.Count();
                Session["Inventory_Use"] = Invs.Inve_Updt;
                TempData["InventoryId"] = id;
                if (id > 0)
                    TempData["BackPage"] = "Inventory_hist";
                else
                    TempData["BackPage"] = "Inventory";
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("AdminController", "ViewInventoryQuantityHistory", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }
            return View(Invs);
        }
    }
}