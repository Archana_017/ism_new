//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ISM_Project.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class UserForgotPassword
    {
        public int UserForgotPasswordID { get; set; }
        public int UserID { get; set; }
        public string PasswordToken { get; set; }
        public short TokenAlreadyClaimed { get; set; }
        public System.DateTime TokenExpiryDate { get; set; }
        public System.DateTime DateRequested { get; set; }
        public System.DateTime DateCreated { get; set; }
    
        public virtual User User { get; set; }
    }
}
