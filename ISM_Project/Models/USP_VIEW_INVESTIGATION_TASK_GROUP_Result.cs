//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ISM_Project.Models
{
    using System;
    
    public partial class USP_VIEW_INVESTIGATION_TASK_GROUP_Result
    {
        public int InvestigationTaskGroupID { get; set; }
        public string InvestigationTaskGroupName { get; set; }
        public string InvestigationTaskGroupDescription { get; set; }
        public int InvestigationTaskGroupSortOrder { get; set; }
    }
}
