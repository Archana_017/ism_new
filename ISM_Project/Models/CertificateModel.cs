﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ISM_Project.Models
{
    public class CertificateModel
    {
        public int CertificateId { get; set; }
        public string CertificateName { get; set; }
        public DateTime? CertifiedDate { get; set; }
        public DateTime? DateofRenewal { get; set; }
        public int ValidityDays { get; set; }
    }
}