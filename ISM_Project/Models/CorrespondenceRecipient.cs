//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ISM_Project.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class CorrespondenceRecipient
    {
        public long CorrespondenceRecipientID { get; set; }
        public Nullable<int> CorrespondenceID { get; set; }
        public string EncodedURL { get; set; }
        public Nullable<int> RecipientResponseStatus { get; set; }
        public string RecipientEmailID { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
    
        public virtual Correspondence Correspondence { get; set; }
    }
}
