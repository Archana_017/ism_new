﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ISM_Project.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ISM_Project.Controllers
{
    /// <summary>
    /// Anonymous Controller for send and receive SR related task to external user and SR committee
    /// </summary> 
    /// 
    public class AnonymousController : Controller
    {
        #region DB object,some common class object and sharepoint path declaration. 
        gcaa_ismEntities gcaa = new gcaa_ismEntities();
        static string[] SharepointfilePath = new string[100];
        static string[] SharepointAnonymousPath = new string[100];
        static int[] DocumentTypeId = new int[100];
        Common comm = new Common();
        MailConfiguration mail = new MailConfiguration();
        #endregion
        #region Correspondence to External Users
        /// <summary>
        /// Correspondence Response view page return based on correspondence id 
        /// </summary>          
        /// <param name="Id">Receive the unique id of correspondence .</param>
        public ActionResult CorrespondenceResponse(string Id)
        {
            var Corr = new IIC();
            var responseDetails = gcaa.USP_VIEW_CORRES_RESPONSE_DETAILS(Id).FirstOrDefault();
            Corr.CorrespondenceMessageResponseFrom = responseDetails.RecipientEmailID;
            Corr.CorrespondenceID = Convert.ToInt32(responseDetails.CorrespondenceID);            
            var respstatus = gcaa.CorrespondenceRecipients.Where(a => a.EncodedURL == Id).Select(a => a.RecipientResponseStatus).FirstOrDefault();
            var corresIni = gcaa.Correspondences.Where(a => a.CorrespondenceID == responseDetails.CorrespondenceID).FirstOrDefault();
            var corresMsg = gcaa.CorrespondenceMessages.Where(m => m.CorrespondenceID == responseDetails.CorrespondenceID && m.CORR_MSG_TYPE == "Initial").FirstOrDefault();
            if(corresMsg.LKCorrespondenceMessageTypeID==6)
            Corr.responseStatus = Convert.ToInt32(respstatus);
            Corr.CorrespondenceControlNumber = corresIni.CorrespondenceControlNumber;
            Corr.LKInitialCorrespondenceMessageTypeID = Convert.ToInt32(corresMsg.LKCorrespondenceMessageTypeID);
            Corr.CorrespondenceMessageFrom = corresMsg.CorrespondenceMessageFrom;
            Corr.Subject = corresIni.Subject;
            var SRResponseID = gcaa.InvestigationSafetyRecommendationStakeholders.Where(a => a.EncodedURL == Id).FirstOrDefault();
            if (Corr.LKInitialCorrespondenceMessageTypeID == 6)
            {
                if (SRResponseID != null)
                {
                    Corr.SR_Response_ID = SRResponseID.SRResponseID;
                    Corr.SR_ID = Convert.ToInt32(SRResponseID.SRID);
                }
            }
            
            if (SRResponseID != null && SRResponseID.SRID  != null)
            {

                IEnumerable<InvestigationSafetyRecommendationCommunicationHIstory> history = (from communicationHistory in gcaa.InvestigationSafetyRecommendationCommunicationHIstories
                                                                                              where communicationHistory.SRID == SRResponseID.SRID
                                                                                              select communicationHistory).AsEnumerable();

                Corr.ISRCHistory = history;
            }
            return View(Corr);
        }
        /// <summary>
        /// To Save the Correspondence Response data
        /// </summary> 
        /// 
        /// <param name="Corr_response">Receive the investigator responce messages .</param>
        [HttpPost]        
        public ActionResult CorrespondenceResponse(IIC Corr_response)
        {
            string msg; string type;
            msg = type = string.Empty;
            string randomsiteURL = Request.Url.AbsolutePath.Split('/')[3];
            string siteURL = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
            try
            {
                saveFilestoSharePoint(Request.Files, Corr_response.CorrespondenceControlNumber);
                if (Convert.ToString(Corr_response.DAAIApprovalRequired).ToLower() == "false")
                    Corr_response.DAAIApproved = null;
                else
                    Corr_response.DAAIApproved = "Pending";
                var msg_ID = gcaa.USP_ADD_CORRES_RESPONSES(Corr_response.CorrespondenceID, Corr_response.CorrespondenceInitiatedDate, Corr_response.CorrespondenceMessageResponseFrom, Corr_response.LKCorrespondenceMessageTypeID, Corr_response.CorrespondenceContent, Corr_response.DAAIApprovalRequired, Corr_response.DAAIApproved, 0, 0, randomsiteURL).FirstOrDefault();
                if (Corr_response.LKInitialCorrespondenceMessageTypeID == 6)
                    gcaa.USP_INST_SR_History(null, Corr_response.SR_Response_ID, Corr_response.SR_ID, Corr_response.CorrespondenceContent, null, null, null, null, null, 3, 0, null, "Insert",null);
                for (int i = 0; i < SharepointfilePath.Length; i++)
                {
                    if (SharepointfilePath[i] != null)
                    {
                        gcaa.USP_CORRES_ATTACHMENTS("insert", Convert.ToInt32(msg_ID.CorrespondenceMessageID), DocumentTypeId[i], SharepointfilePath[i], SharepointAnonymousPath[i], Convert.ToInt32(Session["UserId"]));
                        if (Corr_response.LKInitialCorrespondenceMessageTypeID == 6)
                            gcaa.USP_SR_ATTACHMENTS("insert", Corr_response.SR_ID, Corr_response.SR_Response_ID, DocumentTypeId[i], SharepointfilePath[i], SharepointfilePath[i], 0);
                    }
                    else
                        break;
                }
                bool mailSent = false;
                List<string> DListUsers = new List<string>();
                List<string> DListUsersEmail = new List<string>();
                DListUsersEmail.Add(Corr_response.CorrespondenceMessageFrom);
                if (Corr_response.LKInitialCorrespondenceMessageTypeID == 6)
                {
                    string[] filesAttached = new string[50];
                    var Attachments = gcaa.USP_CORRES_ATTACHMENTS("view", Convert.ToInt32(msg_ID.CorrespondenceMessageID), null, null, null, null).ToList();
                    int filesCount = 0;
                    foreach (var attachment in Attachments)
                    {
                        if (!string.IsNullOrEmpty(attachment.AttachmentPublicUrl))
                        {
                            filesAttached[filesCount] = comm.getEventFilesOrOtherFilesOnPrem(Convert.ToString(attachment.AttachmentPublicUrl));
                            filesCount++;
                        }
                    }
                    //mail to notify IIC regarding the response.
                    mailSent = mail.SendMailNotification(18, null, 0, siteURL, null, DListUsersEmail, 0, null, Corr_response.CorrespondenceControlNumber, Corr_response.CorrespondenceMessageResponseFrom, null, null);
                    DListUsersEmail = new List<string>();
                    DListUsersEmail.Add(Corr_response.CorrespondenceMessageResponseFrom);
                    //mail to confirm the response to the external user about the response.
                    mailSent = mail.SendMailNotificationwithAttach(7, null, 0, null, DListUsersEmail, null, null, 0, filesAttached, null, null, Corr_response.Subject, null, Corr_response.CorrespondenceContent, Corr_response.CorrespondenceMessageFrom );
                }
                else if (Corr_response.LKInitialCorrespondenceMessageTypeID != 6)
                {
                    string[] filesAttached = new string[50];
                    var Attachments = gcaa.USP_CORRES_ATTACHMENTS("view", Convert.ToInt32(msg_ID.CorrespondenceMessageID), null, null, null, null).ToList();
                    int filesCount = 0;
                    foreach (var attachment in Attachments)
                    {
                        if (!string.IsNullOrEmpty(attachment.AttachmentPublicUrl))
                        {
                            filesAttached[filesCount] = comm.getEventFilesOrOtherFilesOnPrem(Convert.ToString(attachment.AttachmentPublicUrl));
                            filesCount++;
                        }
                    }
                    mailSent = mail.SendMailNotification(18, null, 0, siteURL, null, DListUsersEmail, 0, null, Corr_response.CorrespondenceControlNumber, Corr_response.CorrespondenceMessageResponseFrom, Corr_response.CorrespondenceMessageFrom, null);
                    DListUsersEmail = new List<string>();
                    DListUsersEmail.Add(Corr_response.CorrespondenceMessageResponseFrom);
                    mailSent = mail.SendMailNotificationwithAttach(7, null, 0, null, DListUsersEmail, null, null, 0, filesAttached, null, null, Corr_response.Subject, null, Corr_response.CorrespondenceContent,Corr_response.CorrespondenceMessageFrom );
                }
                msg = "Your Response saved Successfully.";
                type = "success";
                Array.Clear(SharepointfilePath, 0, SharepointfilePath.Length);
                Array.Clear(DocumentTypeId, 0, DocumentTypeId.Length);
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("IICController", "CorrespondenceResponse::POST", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
                msg = "There has been an issue in saving your response.Please contact admin.";
            }
            return RedirectToAction("SuccessMessage", "Anonymous", new { message = msg, type = type });
        }
        #endregion
        /// <summary>
        ///  return the Success Message of send correspondence 
        /// </summary> 
        /// 
        /// <param name="message">Receive the investigator responce messages .</param>
        /// <param name="type">Receive the investigator type of responce .</param>
        public ActionResult SuccessMessage(string message, string type)
        {
            ViewBag.Message = message;
            ViewBag.Type = type;
            return View();
        }
        #region SR Committee
        /// <summary>
        ///  return the SR Committee page view with all recommendation
        /// </summary> 
        /// 
        /// <param name="Id">Receive the safety recommendation id .</param>
        public ActionResult SRCommittee(string Id)
        {
            var SRResponse = new SR();
            try
            {
                var responseDetails = gcaa.USP_VIEW_BY_SRCOMMITTEE(Id).FirstOrDefault();
                SRResponse.SRID = Convert.ToInt32(responseDetails.SRID);
                SRResponse.SRHistoryID = Convert.ToInt32(responseDetails.SRHistoryID);
                SRResponse.SRCommitteeActionStatus = responseDetails.SRCommitteeActionStatus;
                SRResponse.SRCommitteeEmailID = responseDetails.SRCommitteeEmailID;
                SRResponse.StakeholderResponse = responseDetails.StakeholderResponse;
                SRResponse.IICCommentsforSHResponse = responseDetails.IICCommentsforSHResponse;
                SRResponse.SRIMPLEMENTATIONSTATUS = responseDetails.SRIMPLEMENTATIONSTATUS;
                SRResponse.SafetyRecommendation = responseDetails.SafetyRecommendation;
                string Json = responseDetails.InvestigationTaskDataInJSON;
                if (!string.IsNullOrEmpty(Json))
                {
                    JObject taskformDetails = JObject.Parse(Json);
                    var values = JsonConvert.DeserializeObject<Dictionary<string, List<string>>>(Json);
                    Dictionary<string, List<string>> keyValues = new Dictionary<string, List<string>>();
                    keyValues = values;
                    if (keyValues.ContainsKey("reportflighthistory.text"))
                    {
                        if (!string.IsNullOrEmpty(keyValues["reportflighthistory.text"][0]))
                        {
                            SRResponse.HistoryOfFlight = keyValues["reportflighthistory.text"][0];
                        }
                    }
                }
                var SRClosureApproval = new List<SelectListItem>();
                SRClosureApproval.Add(new SelectListItem() { Text = "Select", Value = "N/A" });
                SRClosureApproval.Add(new SelectListItem() { Text = "Yes", Value = "Yes" });
                SRClosureApproval.Add(new SelectListItem() { Text = "No", Value = "No" });
                SelectList SRClosureApproval_List = new SelectList(SRClosureApproval, "VALUE", "Text");
                ViewBag.SRClosureApprovalList = SRClosureApproval_List;
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("IICController", "SRCommittee", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }
            return View(SRResponse);
        }
        /// <summary>
        /// To save the SR Committee information and the validations
        /// </summary> 
        /// 
        /// <param name="SRHistory">Receive the safety recommendation full details as object for save to DB.</param>
        [HttpPost]
        public ActionResult SRCommittee(SR SRHistory)
        {
            string msg; string type;
            msg = type = string.Empty;
            string randomsiteURL = Request.Url.AbsolutePath.Split('/')[3];
            string siteURL = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
            try
            {
                Session.Clear();
                if (SRHistory.SRCommitteeClosureStatus.ToLower() == "yes")
                    SRHistory.SRStatus = 6;
                else
                    SRHistory.SRStatus = 7;
                gcaa.USP_INST_SR_History(SRHistory.SRHistoryID, SRHistory.SRResponseID, SRHistory.SRID, null, null, null, SRHistory.SRCommitteeComments, SRHistory.SRCommitteeClosureStatus, null, SRHistory.SRStatus, 0, 0, "UPDATEBYSRC",null);
                var SRCreatedBy = gcaa.InvestigationSafetyRecommendations.AsQueryable().Where(d => d.InvestigationSafetyRecommendationID == SRHistory.SRID).FirstOrDefault();
                var DList = gcaa.Users.AsQueryable().Where(d => d.UserID == SRCreatedBy.CreatedBy).Select(x => new { x.EmailAddress, x.FirstName }).FirstOrDefault();
                List<string> DListUsers = new List<string>();
                List<string> DListUsersEmail = new List<string>();
                DListUsersEmail.Add(DList.EmailAddress);
                DListUsers.Add(DList.FirstName);
                SRHistory.SafetyRecommendationNumber = SRCreatedBy.SafetyRecommendationNumber;
                bool mailSent = mail.SendMailNotification(10, null, 0, siteURL, DListUsers, DListUsersEmail, 0, null, SRHistory.SafetyRecommendationNumber, null, null, null);
                
                // if SR committee reject the SR again send to IIC for update more info.

                if (SRHistory.SRStatus == 7)
                {                    
                    try
                    {                       
                        var corrMsgInfo = gcaa.CorrespondenceMessages.Where(a => a.SR_Number == SRHistory.SafetyRecommendationNumber
                        
                                                                      && a.InvestigationSafetyRecommendationID == SRHistory.SRID).FirstOrDefault();

                        int CorrespondenceID = Convert.ToInt32(corrMsgInfo.CorrespondenceID);

                        var corrInfo = gcaa.Correspondences.Where(a => a.CorrespondenceID == CorrespondenceID).FirstOrDefault();

                        var corrRecInfo = gcaa.CorrespondenceRecipients.Where(a => a.CorrespondenceRecipientID == SRHistory.CorresRecipientID).FirstOrDefault();

                        saveFilestoSharePoint(Request.Files, corrInfo.CorrespondenceControlNumber);

                        if (Convert.ToString(corrMsgInfo.DAAIApprovalRequired).ToLower() == "false")
                            corrMsgInfo.DAAIApproved = null;
                        else
                            corrMsgInfo.DAAIApproved = "Pending";
                        var msg_ID = gcaa.USP_ADD_CORRES_RESPONSES(corrInfo.CorrespondenceID, corrInfo.CorrespondenceInitiatedDate,
                            corrMsgInfo.CorrespondenceMessageFrom, corrMsgInfo.LKCorrespondenceMessageTypeID, corrMsgInfo.CorrespondenceContent,
                            corrMsgInfo.DAAIApprovalRequired, corrMsgInfo.DAAIApproved, 0, 0, randomsiteURL).FirstOrDefault();
                        //if (Corr_response.LKInitialCorrespondenceMessageTypeID == 6)

                        // Get info from previous Stakeholders 
                        var SRResponseInfo = gcaa.InvestigationSafetyRecommendationStakeholders.Where(a => a.SRID == SRHistory.SRID).FirstOrDefault();


                        gcaa.USP_INST_SR_History(null, SRResponseInfo.SRResponseID, SRHistory.SRID, corrMsgInfo.CorrespondenceContent, null, null, null, null, null,
                            3, 0, null, "INSERTCOMMITTEEREJECT", null);
                        for (int i = 0; i < SharepointfilePath.Length; i++)
                        {
                            if (SharepointfilePath[i] != null)
                            {
                                gcaa.USP_CORRES_ATTACHMENTS("insert", Convert.ToInt32(msg_ID.CorrespondenceMessageID), DocumentTypeId[i], SharepointfilePath[i],
                                    SharepointAnonymousPath[i], Convert.ToInt32(Session["UserId"]));
                                //if (Corr_response.LKInitialCorrespondenceMessageTypeID == 6)
                                    gcaa.USP_SR_ATTACHMENTS("insert", SRHistory.SRID, SRResponseInfo.SRResponseID, DocumentTypeId[i], SharepointfilePath[i],
                                        SharepointfilePath[i], 0);
                            }
                            else
                                break;
                        }
                         mailSent = false;
                         DListUsers = new List<string>();
                         DListUsersEmail = new List<string>();
                        DListUsersEmail.Add(corrMsgInfo.CorrespondenceMessageFrom);
                        //if (Corr_response.LKInitialCorrespondenceMessageTypeID == 6)
                        //{
                            string[] filesAttached = new string[50];
                            var Attachments = gcaa.USP_CORRES_ATTACHMENTS("view", Convert.ToInt32(msg_ID.CorrespondenceMessageID), null, null, null, null).ToList();
                            int filesCount = 0;
                            foreach (var attachment in Attachments)
                            {
                                if (!string.IsNullOrEmpty(attachment.AttachmentPublicUrl))
                                {
                                    filesAttached[filesCount] = comm.getEventFilesOrOtherFilesOnPrem(Convert.ToString(attachment.AttachmentPublicUrl));
                                    filesCount++;
                                }
                            }
                            //mail to notify IIC regarding the response.
                            mailSent = mail.SendMailNotification(18, null, 0, siteURL, null, DListUsersEmail, 0, null, corrInfo.CorrespondenceControlNumber,
                                corrMsgInfo.CorrespondenceMessageFrom, null, null);
                            DListUsersEmail = new List<string>();
                            DListUsersEmail.Add(corrMsgInfo.CorrespondenceMessageFrom);
                            //mail to confirm the response to the external user about the response.
                            mailSent = mail.SendMailNotificationwithAttach(7, null, 0, null, DListUsersEmail, null, null, 0, filesAttached, null, null,
                                corrInfo.Subject, null, corrMsgInfo.CorrespondenceContent, null);
                       // }
                    }
                    catch (System.Exception ex)
                    {
                        comm.Exception_Log("IICController", "SRCommittee::POST", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
                        msg = "There has been an issue in saving your response.Please contact admin.";
                    }
                }

                msg = "Your Response saved Successfully.";
                type = "success";
                Array.Clear(SharepointfilePath, 0, SharepointfilePath.Length);
                Array.Clear(DocumentTypeId, 0, DocumentTypeId.Length);
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("IICController", "SRCommittee::POST", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
                msg = "There has been an issue in saving your response.Please contact admin.";
            }
            return RedirectToAction("SuccessMessage", "Anonymous", new { message = msg, type = type });
        }
        #endregion
        /// <summary>
        /// save correspondence file attachment to SharePoint 
        /// </summary> 
        /// 
        /// <param name="files">Receive the safety recommendation attachment files.</param>
        /// <param name="CONTROL_NUMER">Receive the safety recommendation id .</param>
        private void saveFilestoSharePoint(HttpFileCollectionBase files, string CONTROL_NUMER)
        {
            try
            {
                if (files.Count > 0)
                {
                    if (string.IsNullOrEmpty(CONTROL_NUMER))
                        CONTROL_NUMER = "0";
                    int filecount = 0;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];
                        if (file.ContentLength > 0 && !string.IsNullOrEmpty(file.FileName))
                        {
                            int fileSize = file.ContentLength;

                            string fileName = file.FileName.Substring(0, file.FileName.LastIndexOf('.')) + "_" + DateTime.Now.ToFileTime() + file.FileName.Substring(file.FileName.LastIndexOf('.'), ((file.FileName.Length) - (file.FileName.LastIndexOf('.'))));

                            string mimeType = file.ContentType.Split('/')[1].ToString();

                            DocumentTypeId[i] = Convert.ToInt32(gcaa.LKDocumentTypes.AsQueryable().Where(d => d.LKDocumentTypeName == mimeType).Select(d => d.LKDocumentTypeID).FirstOrDefault());

                            Stream fileContent = file.InputStream;

                            string[] attachURLs = comm.savetoSharePointOtherDocsOnPrem(fileContent, fileName, CONTROL_NUMER, "Corres_Documents");

                            SharepointfilePath[filecount] = attachURLs[0];

                            SharepointAnonymousPath[filecount] = attachURLs[1];

                            filecount++;
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("IICController", "saveFilestoSharePoint", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }
        }
    }
}