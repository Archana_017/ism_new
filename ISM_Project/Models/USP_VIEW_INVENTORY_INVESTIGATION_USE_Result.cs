//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ISM_Project.Models
{
    using System;
    
    public partial class USP_VIEW_INVENTORY_INVESTIGATION_USE_Result
    {
        public Nullable<int> InventoryID { get; set; }
        public string Item { get; set; }
        public Nullable<int> InvestigationID { get; set; }
        public string QuantityUsed { get; set; }
        public string InvestigationFileNumber { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
    }
}
