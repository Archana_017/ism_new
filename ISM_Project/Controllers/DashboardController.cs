﻿using ISM_Project.Models;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using PagedList.Mvc;

namespace ISM_Project.Controllers
{
    /// <summary>
    /// Dashboard Controller - contains all application user dashboard based on user role and user id.
    /// </summary> 
    /// 
    public class DashboardController : Controller
    {
        Common comm = new Common();
        gcaa_ismEntities dc = new gcaa_ismEntities();

        /// <summary>
        /// 
        /// </summary> 
        /// 
        public ActionResult Index()
        {
            return View("Dashboard");
        }
        /// <summary>
        /// 
        /// </summary> 
        /// 
        public ActionResult Dashboard()
        {
            return View();
        }
        /// <summary>
        /// Admin action method view all information about application user 
        /// </summary> 
        /// 
        /// <param name="page">Receive the admin dashboard page no .</param>
        public ActionResult AdminDashboard(int? page)
        {
            AdminModel adm = new AdminModel();
            try
            {
                if (page == null)
                {
                    ViewBag.serial_nbr = 0;
                }
                else
                {
                    ViewBag.serial_nbr = 10 * (page - 1);
                }
                TempData["active_dashboard"] = "admin";
                Session["act_dashboard"] = "admin";

                if (Convert.ToInt32(Session["UserId"]) > 0)
                {
                    if (Convert.ToInt32(Session["roleid"]) == 1)
                    {
                        adm = new AdminModel()
                        {
                            All_Users = dc.USP_VIEW_ALL_USERS().ToList(),
                        };
                        ViewBag.page_no = page;
                        ViewBag.count_pagecontent = adm.All_Users.Count();
                        return View(adm);
                    }
                    else
                    {
                        return RedirectToAction("Dashboard", "Home");
                    }
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("AdminController", "AdminDashboard", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }
            return View(adm);
        }
        /// <summary>
        /// Duty investigator Dashboard view all notification based on user Id
        /// </summary> 
        /// 
        /// <param name="page">Receive the DI dashboard page no .</param>
        /// <param name="occyear"> Receive occurrence year for dashboard filtration</param>
        /// <param name="occstatusid"> Receive occurrence status for dashboard filtration</param>
        /// <param name="frmdi">Receive the dashboard data as object</param>
        ///
        public ActionResult DIDashboard(int? page, int? occyear, int? occstatusid, Form39 frmdi)
        {
            if (page == null || page == 0)
            {
                ViewBag.serial_nbr = 0;
            }
            else
            {
                ViewBag.serial_nbr = 10 * (page - 1);
            }

            if (frmdi.Occurrenceyear == null || frmdi.Occurrenceyear == 0)
            {
                if (Convert.ToInt32(occyear) > 0)
                {
                    ViewBag.Occurrence_year = occyear;

                    frmdi.Occurrenceyear = Convert.ToInt32(occyear);
                }
                else
                {
                    ViewBag.Occurrence_year = null;
                }
            }
            else
            {
                ViewBag.Occurrence_year = frmdi.Occurrenceyear.ToString();
            }

            if (frmdi.LKOccurrenceStatusTypeId == null || frmdi.LKOccurrenceStatusTypeId == 0)
            {
                if (Convert.ToInt32(occstatusid) > 0)
                {
                    frmdi.LKOccurrenceStatusTypeId = Convert.ToInt32(occstatusid);

                    ViewBag.LKOccurrenceStatusTypeId = Convert.ToInt32(occstatusid);
                }
            }
            else
            {
                ViewBag.LKOccurrenceStatusTypeId = frmdi.LKOccurrenceStatusTypeId;
            }

            TempData["active_dashboard"] = "di";

            Session["act_dashboard"] = "di";

            var di_board = new Form39();

            int TempUserId = 0;

            if (Convert.ToInt32(Session["RoleId"]) == 1)
            {
                ViewBag.Delete_Notification = true;

                TempUserId = 0;

                var LKOccurrence_Status = dc.LKOccurrenceStatusTypes.Where(a => a.LKOccurrenceStatusTypeId == 1 || a.LKOccurrenceStatusTypeId == 2 || a.LKOccurrenceStatusTypeId == 5).ToList();
                SelectList LKOccurrenceStatus = new SelectList(LKOccurrence_Status, "LKOccurrenceStatusTypeId", "LKOccurrenceStatusTypeName");
                ViewBag.Occurrence_Status = LKOccurrenceStatus;

                di_board = new Form39()
                {
                    diDashBoard = dc.USP_VIEW_OCCURRENCE_FOR_DI(TempUserId, frmdi.Occurrenceyear, frmdi.LKOccurrenceStatusTypeId,1).ToList(),
                    Occurrenceyear = Convert.ToInt32(frmdi.Occurrenceyear),
                    LKOccurrenceStatusTypeId = Convert.ToInt32(frmdi.LKOccurrenceStatusTypeId)
                };
            }
            else if (Convert.ToInt32(Session["RoleId"]) == 2)
            {
                ViewBag.Delete_Notification = false;

                TempUserId = Convert.ToInt32(Session["UserId"]);

                var LKOccurrence_Status = dc.LKOccurrenceStatusTypes.ToList().OrderBy(x => x.LKOccurrenceStatusTypeId);
                SelectList LKOccurrenceStatus = new SelectList(LKOccurrence_Status, "LKOccurrenceStatusTypeId", "LKOccurrenceStatusTypeName");
                ViewBag.Occurrence_Status = LKOccurrenceStatus;

                di_board = new Form39()
                {
                    diDashBoard = dc.USP_VIEW_OCCURRENCE_FOR_DI(TempUserId, frmdi.Occurrenceyear, frmdi.LKOccurrenceStatusTypeId,0).ToList(),
                    Occurrenceyear = Convert.ToInt32(frmdi.Occurrenceyear),
                    LKOccurrenceStatusTypeId = Convert.ToInt32(frmdi.LKOccurrenceStatusTypeId)
                };
            }

            ViewBag.count_page = di_board.diDashBoard.Count();

            ViewBag.page_count = page;

            return View(di_board);
        }
        /// <summary>
        ///  Director action method view all notification and investigation status to the Dashboard
        /// </summary> 
        /// 
        /// <param name="page1">Receive the duty investigator dashboard page no .</param>
        /// <param name="page2">Receive the investigator dashboard page no .</param>
        ///  <param name="page3">Receive the correspondence dashboard page no .</param>
        ///  <param name="page4">Receive the safty recommendation dashboard page no .</param>
        ///  <param name="page5">Receive the closure investigation dashboard page no .</param>
        /// <param name="occyear">Receive occurrence year for dashboard filtration</param>
        /// <param name="occstatusid">Receive occurrence status for dashboard filtration</param>
        ///  <param name="frmdaai">Receive the dashboard details as object</param>
        ///  
        public ActionResult DirectorDashboard(int? page1, int? page2, int? page3, int? page4, int? page5, int? occyear, int? occstatusid, Form39 frmdaai)
        {
            Response.AddHeader("Refresh", "600");
            if (page1 == null)//occurrences
            {
                ViewBag.serial_nbr = 0;
            }
            else
            {
                ViewBag.serial_nbr = 10 * (page1 - 1);
            }
            if (page2 == null)//Investigations
            {
                ViewBag.serial_nbr1 = 0;
            }
            else
            {
                ViewBag.serial_nbr1 = 10 * (page2 - 1);
            }
            if (page3 == null)//Correspondences
            {
                ViewBag.serial_nbr2 = 0;
            }
            else
            {
                ViewBag.serial_nbr2 = 10 * (page3 - 1);
            }
            if (page4 == null) // SR
            {
                ViewBag.serial_nbr3 = 0;
            }
            else
            {
                ViewBag.serial_nbr3 = 10 * (page4 - 1);
            }
            if (page5 == null) // Closure Details
            {
                ViewBag.serial_nbr4 = 0;
            }
            else
            {
                ViewBag.serial_nbr4 = 10 * (page5 - 1);
            }
            TempData["active_dashboard"] = "director";
            Session["act_dashboard"] = "director";

            if (frmdaai.Occurrenceyear == null || frmdaai.Occurrenceyear == 0)
            {
                if (Convert.ToInt32(occyear) > 0)
                {
                    ViewBag.InvOccurrence_year = occyear;

                    frmdaai.Occurrenceyear = Convert.ToInt32(occyear);
                }
                else
                {
                    ViewBag.InvOccurrence_year = null;
                }
            }
            else
            {
                ViewBag.Occurrence_year = frmdaai.Occurrenceyear.ToString();
            }

            var LKOccurrence_Status = dc.LKOccurrenceStatusTypes.ToList().OrderBy(x => x.LKOccurrenceStatusTypeId);
            SelectList LKOccurrenceStatus = new SelectList(LKOccurrence_Status, "LKOccurrenceStatusTypeId", "LKOccurrenceStatusTypeName");
            ViewBag.Occurrence_Status = LKOccurrenceStatus;

            //if (frmdaai.InvOccurrenceyear == null || frmdaai.InvOccurrenceyear == 0)
            //{
            //    ViewBag.InvOccurrence_year = null;
            //}
            //else
            //{
            //    ViewBag.InvOccurrence_year = frmdaai.InvOccurrenceyear.ToString();
            //}

            if (frmdaai.InvOccurrenceyear == null || frmdaai.InvOccurrenceyear == 0)
            {
                if (Convert.ToInt32(occyear) > 0 )
                {
                    ViewBag.InvOccurrence_year = occyear;

                    frmdaai.InvOccurrenceyear = Convert.ToInt32(occyear);
                }
                else
                {
                    ViewBag.InvOccurrence_year = null;
                }
            }
            else
            {
                ViewBag.InvOccurrence_year = frmdaai.InvOccurrenceyear.ToString();
            }

            if (frmdaai.InvLKOccurrenceStatusTypeId == null || frmdaai.InvLKOccurrenceStatusTypeId == 0)
            {
                if (Convert.ToInt32(occstatusid) > 0 && page2 > 0)
                {
                    ViewBag.LKOccurrenceStatusTypeId = occstatusid;

                    frmdaai.InvLKOccurrenceStatusTypeId = Convert.ToInt32(occstatusid);
                }
            }
            else
            {
                ViewBag.LKOccurrenceStatusTypeId = frmdaai.InvLKOccurrenceStatusTypeId;
            }

            var invLKOccurrence_Status = dc.LKOccurrenceStatusTypes.Where(a => a.LKOccurrenceStatusTypeId == 3 ||  a.LKOccurrenceStatusTypeId == 6);
            SelectList invLKOccurrenceStatus = new SelectList(invLKOccurrence_Status, "LKOccurrenceStatusTypeId", "LKOccurrenceStatusTypeName");
            ViewBag.InvOccurrence_Status = invLKOccurrenceStatus;

            var director = new Form39();
            director = new Form39()
            {
                OccList = dc.USP_VIEW_ALL_OCCURRENCE(0, 0, frmdaai.LKOccurrenceStatusTypeId, frmdaai.Occurrenceyear).ToList(),
                InvList = dc.USP_VIEW_INVESTIGATION(frmdaai.InvLKOccurrenceStatusTypeId, frmdaai.InvOccurrenceyear).ToList(),
                Corres = dc.USP_VIEW_PENDING_CORRESPONDENCE(0).ToList(),
                PENDING_SR = dc.USP_VIEW_PENDNIG_SR().ToList(),
                ClouserDet = dc.USP_VIEW_INVESTIGATION_CLOUSERDETAILS(0).ToList()
            };
            ViewBag.page_no = page1;
            ViewBag.page_no1 = page2;
            ViewBag.page_no2 = page3;
            ViewBag.page_no3 = page4;
            ViewBag.page_no4 = page5;
            Session["mode"] = "view";
            ViewBag.Dir_count_pagecontent = director.OccList.Count();
            ViewBag.Dir_count_pagecontent1 = director.InvList.Count();
            ViewBag.Dir_count_pagecontent2 = director.Corres.Count();
            ViewBag.Dir_count_pagecontent3 = director.PENDING_SR.Count();
            ViewBag.Dir_count_pagecontent4 = director.ClouserDet.Count();

            #region on 06-Aug-2018

            var Year = dc.USP_VIEW_OCCURRENCE_YEAR(DateTime.Now.Year).ToList();

            //Year.Add(new SelectListItem() { Text = "2018", Value = "2018" });
            //Year.Add(new SelectListItem() { Text = "2017", Value = "2017" });
            //Year.Add(new SelectListItem() { Text = "2016", Value = "2016" });
            //Year.Add(new SelectListItem() { Text = "2015", Value = "2015" });
            //Year.Add(new SelectListItem() { Text = "2014", Value = "2014" });

            SelectList year_list = new SelectList(Year);

            ViewBag.Year_id = year_list;

            ViewBag.CurrentYear = DateTime.Now.Year;

            #endregion

            return View(director);
        }
        /// <summary>
        /// Investigator action method view all investigation status to the Dashboard based on user 
        /// </summary> 
        /// 
        /// <param name="page">Receive the duty investigator dashboard page no .</param>
        /// <param name="page1">Receive the team member dashboard page no .</param>       
        /// <param name="occyear">Receive occurrence year for dashboard filtration</param>
        /// <param name="occstatusid">Receive occurrence status for dashboard filtration</param>
        /// <param name="occyeartask">Receive occurrence year for taskdashboard filtration</param>
        /// <param name="occinvestgtnid">Receive investigation no for taskdashboard filtration</param>
        /// <param name="frmiic">Receive the dashboard details as object</param>
        public ActionResult InvestigatorDashboard(int? page, int? page1,int? occyear, int? occyeartask, int? occstatusid, int? occinvestgtnid, Form39 frmiic)
        {
            Response.AddHeader("Refresh", "600");
            var investDashboard = new Form39();

            try
            {
                if (page == null)
                {
                    ViewBag.serial_nbr = 0;
                }
                else
                {
                    ViewBag.serial_nbr = 10 * (page - 1);
                }
                if (page1 == null)
                {
                    ViewBag.serial_nbr1 = 0;
                }
                else
                {
                    ViewBag.serial_nbr1 = 10 * (page1 - 1);
                }
                TempData["active_dashboard"] = "IIC";
                Session["act_dashboard"] = "IIC";
                TempData["investigationdetail_url"] = "iic";
                TempData["active_url_manuals_iic"] = "iic";          

                if (frmiic.InvOccurrenceyear == null || frmiic.InvOccurrenceyear == 0)
                {
                    if (Convert.ToInt32(occyear) > 0)
                    {
                        ViewBag.InvOccurrence_year = occyear;

                        frmiic.InvOccurrenceyear = Convert.ToInt32(occyear);
                    }
                    else
                    {
                        ViewBag.InvOccurrence_year = null;
                    }
                }
                else
                {
                    ViewBag.InvOccurrence_year = frmiic.InvOccurrenceyear.ToString();
                }

                if (frmiic.InvLKOccurrenceStatusTypeId == null || frmiic.InvLKOccurrenceStatusTypeId == 0)
                {
                    if (Convert.ToInt32(occstatusid) > 0)
                    {
                        ViewBag.LKOccurrenceStatusTypeId = occstatusid;

                        frmiic.InvLKOccurrenceStatusTypeId = Convert.ToInt32(occstatusid);
                    }
                }
                else
                {
                    ViewBag.LKOccurrenceStatusTypeId = frmiic.InvLKOccurrenceStatusTypeId;
                }

                int TempUserId = 0;

                if (Convert.ToInt32(Session["RoleId"]) == 1)
                {
                    ViewBag.Delete_Investigation = true;

                    TempUserId = 0;                    
                }
                else
                {
                    ViewBag.Delete_Investigation = false;

                    TempUserId = Convert.ToInt32(Session["UserId"]);
                }

                var InvLKOccurrence_Status = dc.LKOccurrenceStatusTypes.Where(a => a.LKOccurrenceStatusTypeId == 3 || a.LKOccurrenceStatusTypeId == 6 ||a.LKOccurrenceStatusTypeId == 7);
                SelectList InvLKOccurrenceStatus = new SelectList(InvLKOccurrence_Status, "LKOccurrenceStatusTypeId", "LKOccurrenceStatusTypeName");
                ViewBag.InvOccurrence_Status = InvLKOccurrenceStatus;

                investDashboard = new Form39()
                {
                    InvDashBoard = dc.USP_VIEW_INVESTIGATIONDETAILS_BY_OCCURRENCESTATUSTYPE_ID("3,4", TempUserId , frmiic.InvLKOccurrenceStatusTypeId, frmiic.InvOccurrenceyear).ToList(),

                    InvTeamMemDashBoard = dc.USP_VIEW_INVESTIGATIONDETAILS_FOR_MULTIPLE_TEAMMEMBER_DASHBOARD(Convert.ToInt32(Session["UserId"])).ToList(),

                    InvOccurrenceyear = frmiic.InvOccurrenceyear,

                    InvLKOccurrenceStatusTypeId = frmiic.InvLKOccurrenceStatusTypeId
                };



                if (frmiic.TaskInvOccurrenceyear == null || frmiic.TaskInvOccurrenceyear == 0)
                {
                    if (Convert.ToInt32(occyeartask) > 0)
                    {
                        ViewBag.TaskInvOccurrence_year = occyeartask;

                        frmiic.TaskInvOccurrenceyear = Convert.ToInt32(occyeartask);
                    }
                    else
                    {
                        ViewBag.TaskInvOccurrence_year = null;
                    }
                }
                else
                {
                    ViewBag.TaskInvOccurrence_year = frmiic.TaskInvOccurrenceyear.ToString();
                }

                if (frmiic.InvInvestigationId == null || frmiic.InvInvestigationId == 0)
                {
                    if (Convert.ToInt32(occinvestgtnid) > 0)
                    {
                        ViewBag.LKOInvestigationId = occinvestgtnid;

                        frmiic.InvInvestigationId = Convert.ToInt32(occinvestgtnid);
                    }
                }
                else
                {
                    ViewBag.LKOInvestigationId = frmiic.InvInvestigationId;
                }

                int TempUserId1 = 0;

                if (Convert.ToInt32(Session["RoleId"]) == 1)
                {
                    ViewBag.Delete_Investigation = true;

                    TempUserId1 = 0;
                }
                else
                {
                    ViewBag.Delete_Investigation = false;

                    TempUserId1 = Convert.ToInt32(Session["UserId"]);
                }

                var InvLKOInvestigation_Id = dc.InvestigationFile.Where(a => (a.LKInvestigationStatusTypeID == 3 || a.LKInvestigationStatusTypeID == 1)  && !string.IsNullOrEmpty(a.InvestigationFileNumber));
            

                investDashboard = new Form39()
                {
                    InvDashBoard = dc.USP_VIEW_INVESTIGATIONDETAILS_BY_OCCURRENCESTATUSTYPE_ID("3,4", TempUserId, frmiic.InvLKOccurrenceStatusTypeId, frmiic.InvOccurrenceyear).ToList(),

                    InvTeamMemDashBoard = dc.USP_VIEW_INVESTIGATIONDETAILS_FOR_MULTIPLE_TEAMMEMBER_DASHBOARD(Convert.ToInt32(Session["UserId"])).ToList(),

                    InvOccurrenceyear = frmiic.InvOccurrenceyear,

                    InvLKOccurrenceStatusTypeId = frmiic.InvLKOccurrenceStatusTypeId
                };

                if(frmiic.TaskInvOccurrenceyear != null && frmiic.TaskInvOccurrenceyear > 0)
                {
                    investDashboard.InvTeamMemDashBoard = investDashboard.InvTeamMemDashBoard.Where(a => a.NotificationDate.Value.Year.Equals(frmiic.TaskInvOccurrenceyear));
                    InvLKOInvestigation_Id = dc.InvestigationFile.Where(a => a.InvestigationFileNumber.Contains(frmiic.TaskInvOccurrenceyear.ToString())); 
                }
                if(frmiic.InvInvestigationId != null && frmiic.InvInvestigationId > 0)
                {
                    investDashboard.InvTeamMemDashBoard = investDashboard.InvTeamMemDashBoard.Where(a => a.InvestigationFileID.Equals(frmiic.InvInvestigationId));
                }
                SelectList InvLKOInvestigationId = new SelectList(InvLKOInvestigation_Id, "InvestigationFileID", "InvestigationFileNumber");
                ViewBag.Inv_LKOInvestigationId = InvLKOInvestigationId;

                string searchtext ="" ;

                if (frmiic.InvSearchtext != null )
                {

                    searchtext = frmiic.InvSearchtext;
                  
                }
               


                if (searchtext != null && searchtext != "")
                {

                    investDashboard.InvDashBoard = investDashboard.InvDashBoard.
                     Where(x =>

                             string.Equals(x.InvestigationNumber.ToString(), searchtext, StringComparison.CurrentCultureIgnoreCase)
                             || x.InvestigationNumber.ToLower().Contains(searchtext.ToLower())

                             || string.Equals(x.PlaceOfIncident.ToString(), searchtext, StringComparison.CurrentCultureIgnoreCase)
                             || x.PlaceOfIncident.ToLower().Contains(searchtext.ToLower())

                             || string.Equals(x.LKOccurrenceStatusTypeName.ToString(), searchtext, StringComparison.CurrentCultureIgnoreCase)
                             || x.LKOccurrenceStatusTypeName.ToLower().Contains(searchtext.ToLower())

                             || string.Equals(x.NotificationID.ToString(), searchtext, StringComparison.CurrentCultureIgnoreCase)
                             || x.NotificationID.ToLower().Contains(searchtext.ToLower())

                             || string.Equals(x.CountryName.ToString(), searchtext, StringComparison.CurrentCultureIgnoreCase)
                             || x.CountryName.ToLower().Contains(searchtext.ToLower())

                             || string.Equals(x.LKIncidentTypeName.ToString(), searchtext, StringComparison.CurrentCultureIgnoreCase)
                             || x.LKIncidentTypeName.ToLower().Contains(searchtext.ToLower())

                             || string.Equals(x.EventDescription.ToString(), searchtext, StringComparison.CurrentCultureIgnoreCase)
                             || x.EventDescription.ToLower().Contains(searchtext.ToLower())

                             || string.Equals(x.OccurrenceDescription.ToString(), searchtext, StringComparison.CurrentCultureIgnoreCase)
                             || x.OccurrenceDescription.ToLower().Contains(searchtext.ToLower())

                             || string.Equals(x.OccurrenceCategorization.ToString(), searchtext, StringComparison.CurrentCultureIgnoreCase)
                             || x.OccurrenceCategorization.ToLower().Contains(searchtext.ToLower())

                              ).ToList();
                    };
               
                ViewBag.LKOInvestigationId = InvLKOInvestigationId;
                ViewBag.page_no = page;
                ViewBag.page_no1 = page1;
                ViewBag.count_pagecontent = investDashboard.InvDashBoard.Count();
                ViewBag.count_pagecontent1 = investDashboard.InvTeamMemDashBoard.Count();
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("Dashboard", "InvestigatorDashboard", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }
            return View(investDashboard);
        }
        /// <summary>
        /// Team Member action method view all assigned event to the Dashboard based on user 
        /// </summary> 
        /// 
        /// <param name="page">Receive the duty team member dashboard page no .</param>
        public ActionResult TeamMemberDashboard(int? page)
        {
            Response.AddHeader("Refresh", "600");
            if (page == null)
            {
                ViewBag.serial_nbr = 0;
            }
            else
            {
                ViewBag.serial_nbr = 10 * (page - 1);
            }
            TempData["active_dashboard"] = "teammember";
            Session["act_dashboard"] = "teammember";

            ViewBag.page_count = page;
            //  ViewBag.count_page = di.USP_VIEW_INVESTIGATIONDETAILS_FOR_TEAMMEMBER_DASHBOARD(Convert.ToInt32(Session["UserId"])).ToList().Count();
            ViewBag.count_page = dc.USP_VIEW_INVESTIGATIONDETAILS_FOR_MULTIPLE_TEAMMEMBER_DASHBOARD(Convert.ToInt32(Session["UserId"])).ToList().Count();

            if (ViewBag.count_page > 10)
            {
                //   return View(di.USP_VIEW_INVESTIGATIONDETAILS_FOR_TEAMMEMBER_DASHBOARD(Convert.ToInt32(Session["UserId"])).ToList().ToPagedList(page ?? 1, 10));

                return View(dc.USP_VIEW_INVESTIGATIONDETAILS_FOR_MULTIPLE_TEAMMEMBER_DASHBOARD(Convert.ToInt32(Session["UserId"])).ToList().ToPagedList(page ?? 1, 10));
            }
            int count_page = ViewBag.count_page;
            if (count_page > 0)
                // return View(di.USP_VIEW_INVESTIGATIONDETAILS_FOR_TEAMMEMBER_DASHBOARD(Convert.ToInt32(Session["UserId"])).ToList().ToPagedList(page ?? 1, count_page));
                return View(dc.USP_VIEW_INVESTIGATIONDETAILS_FOR_MULTIPLE_TEAMMEMBER_DASHBOARD(Convert.ToInt32(Session["UserId"])).ToList().ToPagedList(page ?? 1, count_page));
            else
                // return View(di.USP_VIEW_INVESTIGATIONDETAILS_FOR_TEAMMEMBER_DASHBOARD(Convert.ToInt32(Session["UserId"])).ToList().ToPagedList(page ?? 1, 1));
                return View(dc.USP_VIEW_INVESTIGATIONDETAILS_FOR_MULTIPLE_TEAMMEMBER_DASHBOARD(Convert.ToInt32(Session["UserId"])).ToList().ToPagedList(page ?? 1, 1));
        }

        public ActionResult SRManagementDashboard(int? page, int? page1, int? occyear, int? occstatusid, Form39 frmiic)
        {
            Response.AddHeader("Refresh", "600");
            var investDashboard = new Form39();

            try
            {
                if (page == null)
                {
                    ViewBag.serial_nbr = 0;
                }
                else
                {
                    ViewBag.serial_nbr = 10 * (page - 1);
                }
                if (page1 == null)
                {
                    ViewBag.serial_nbr1 = 0;
                }
                else
                {
                    ViewBag.serial_nbr1 = 10 * (page1 - 1);
                }
                TempData["active_dashboard"] = "SRM";
                Session["act_dashboard"] = "SRM";
                TempData["active_url_manuals_iic"] = "iic";

                if (frmiic.InvOccurrenceyear == null || frmiic.InvOccurrenceyear == 0)
                {
                    if (Convert.ToInt32(occyear) > 0)
                    {
                        ViewBag.InvOccurrence_year = occyear;

                        frmiic.InvOccurrenceyear = Convert.ToInt32(occyear);
                    }
                    else
                    {
                        ViewBag.InvOccurrence_year = null;
                    }
                }
                else
                {
                    ViewBag.InvOccurrence_year = frmiic.InvOccurrenceyear.ToString();
                }

                if (frmiic.InvLKOccurrenceStatusTypeId == null || frmiic.InvLKOccurrenceStatusTypeId == 0)
                {
                    if (Convert.ToInt32(occstatusid) > 0)
                    {
                        ViewBag.LKOccurrenceStatusTypeId = occstatusid;

                        frmiic.InvLKOccurrenceStatusTypeId = Convert.ToInt32(occstatusid);
                    }
                }
                else
                {
                    ViewBag.LKOccurrenceStatusTypeId = frmiic.InvLKOccurrenceStatusTypeId;
                }

                int TempUserId = 0;

                if (Convert.ToInt32(Session["RoleId"]) == 1)
                {
                    ViewBag.Delete_Investigation = true;

                    TempUserId = 0;
                }
                else
                {
                    ViewBag.Delete_Investigation = false;

                    TempUserId = Convert.ToInt32(Session["UserId"]);
                }

                var InvLKOccurrence_Status = dc.LKOccurrenceStatusTypes.Where(a => a.LKOccurrenceStatusTypeId == 3 || a.LKOccurrenceStatusTypeId == 6 );
                SelectList InvLKOccurrenceStatus = new SelectList(InvLKOccurrence_Status, "LKOccurrenceStatusTypeId", "LKOccurrenceStatusTypeName");
                ViewBag.InvOccurrence_Status = InvLKOccurrenceStatus;

                investDashboard = new Form39()
                {
                    srInvestigationDashBoard = dc.USP_VIEW_INVESTIGATIONDETAILS_BY_OCCURRENCESTATUSTYPE_IN_SR("3,4", 0 , frmiic.InvLKOccurrenceStatusTypeId, frmiic.InvOccurrenceyear).ToList(),

                    InvTeamMemDashBoard = dc.USP_VIEW_INVESTIGATIONDETAILS_FOR_MULTIPLE_TEAMMEMBER_DASHBOARD(Convert.ToInt32(Session["UserId"])).ToList(),

                    InvOccurrenceyear = frmiic.InvOccurrenceyear,

                    InvLKOccurrenceStatusTypeId = frmiic.InvLKOccurrenceStatusTypeId
                };

                ViewBag.page_no = page;
                ViewBag.page_no1 = page1;
                ViewBag.count_pagecontent = investDashboard.srInvestigationDashBoard.Count();
                ViewBag.count_pagecontent1 = investDashboard.InvTeamMemDashBoard.Count();
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("Dashboard", "SRManagementDashboard", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));
            }
            return View(investDashboard);
        }
    }
}