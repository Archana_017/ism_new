//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ISM_Project.Models
{
    using System;
    
    public partial class USP_VIEW_EVENT_ANALYSIS_LATENT_VALUES_Result
    {
        public Nullable<int> LKOrganizationalInfluencesId { get; set; }
        public Nullable<int> LKUnsafeSupervisionId { get; set; }
        public Nullable<int> LKPreConditionsId { get; set; }
        public Nullable<int> LKUnsafeActId { get; set; }
        public Nullable<int> LKLatentActiveCheck { get; set; }
    }
}
