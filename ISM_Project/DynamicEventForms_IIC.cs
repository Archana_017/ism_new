﻿using ISM_Project.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ISM_Project
{
    /// <summary>
    ///  Dynamic Event forms contains the some common function for generating dynamic form based on JSON input 
    ///  and also include some functionality of investigation incharge. 
    /// </summary> 
    /// 
    public class DynamicEventForms_IIC
    {
        /// <summary>
        /// Event Generation iic
        /// </summary> 
        /// 
        ///<param name="json">Receive investigation file JSON</param>
        ///<param name="invfileid">Receive investigation file id</param>
        ///<param name="taskassignid">Receive investigation task  id</param>
        ///<param name="teammember_id">Receive teammember id</param>
        ///<param name="id">Receive investigation  id</param>
        ///<param name="ProgressInPercentage">Receive investigation Progress In Percentage</param>
        ///<param name="pvttaskassignmentid">Receive investigation task assignment</param>
        ///<param name="invstatus">Receive investigation ststus</param>
        ///<param name="eventuniquenumber">Receive investigation event no</param>
        /// <param name="taskgroupid">Receive investigation task group id</param>
        /// <param name="inv_status_type">Receive investigation ststus</param>
        /// <param name="eventsubstatus">Receive investigation sub task status</param>
        public string Event_Generation_iic(string json, int invfileid, int taskassignid, int teammember_id, int id, int ProgressInPercentage, int pvttaskassignmentid, int invstatus, int eventuniquenumber, int taskgroupid, int inv_status_type, int eventsubstatus)
        {

            var formContent = "";
            dynamic formDetails = JObject.Parse(json);
            var event_name = formDetails.form_title;
            event_name = Convert.ToString(event_name);
            var form_name = Convert.ToString(formDetails.form_name);
            var form_action = Convert.ToString(formDetails.form_action);
            var field = formDetails.fields;
            var button_div = false;
            formContent += "<form name='" + form_name + "' id='" + form_name + "' action='/Task/SaveFiles_iic/" + id + "?tskgrp_id=" + taskgroupid + "&invfile_id=" + invfileid + "' method='post' class='subtask-form' enctype='multipart/form-data'>";
            for (var i = 0; i < field.Count; i++)
            {
                if (field[i].field_type == "text")
                {
                    string val = Convert.ToString(field[i].value[0]);

                    formContent += textControll(field[i]);
                }
                else if (field[i].field_type == "h2")
                {
                    formContent += h2Control(field[i]);
                }
                else if (field[i].field_type == "radio")
                {
                    formContent += radioControl(field[i]);
                }
                else if (field[i].field_type == "dropdown")
                {
                    formContent += dropdownControl(field[i]);
                }
                else if (field[i].field_type == "date")
                {
                    formContent += dateControl(field[i]);
                }
                else if (field[i].field_type == "textarea")
                {
                    string val = Convert.ToString(field[i].value[0]);

                    formContent += textareaControl(field[i]);
                }
                else if (field[i].field_type == "file")
                {
                    var field_value = field[i].value[0];
                    formContent += fileInputControll(field[i], pvttaskassignmentid, id);
                }
                else if (field[i].field_type == "checkbox")
                {
                    formContent += checkboxControl(field[i]);
                }
                else if (field[i].field_type == "label")
                {
                    formContent += labelControl(field[i]);
                }
                else if (field[i].field_type == "button")
                {
                    if (button_div == false)
                    {
                        formContent += "<div class='sub-sav-btn'>";
                    }
                    formContent += buttonControll(field[i], teammember_id, ProgressInPercentage, taskassignid, id, pvttaskassignmentid, invstatus, eventuniquenumber, inv_status_type, taskgroupid, eventsubstatus);
                    if (button_div == true)
                    {
                        formContent += "</div>";
                    }
                    button_div = true;

                }
                else if (field[i].field_type == "hidden")
                {
                    formContent += hiddenControll(field[i], invfileid, taskassignid);
                }
                else if (field[i].field_type == "datepicker")
                {
                    formContent += datepickercontrol(field[i]);
                }
                else if (field[i].field_type == "table_start")
                {
                    {
                        formContent += tablecontrol(field[i]);
                    }

                }
                else if (field[i].field_type == "text-tbl")
                {
                    {
                        formContent += tablecontrol(field[i]);
                    }
                }
                else if (field[i].field_type == "table_end")
                {
                    formContent += tablecontrol(field[i]);

                }
            }
            formContent += "</form>";
            return formContent;


            //return Content(formContent, "text/html");
            //formContent += btn;
            //  return Json(new { nform_title = event_name, nform_name = form_name, action_name = form_action, fields = formContent });
        }
        /// <summary>
        /// table controll for dynamic form   
        /// </summary> 
        /// 
        ///<param name="field">Receive table control field value</param>
        public string tablecontrol(dynamic field)
        {
            var tc = "";
            if (field.field_type == "table_start")
            {
                tc = "<div class='form-group row' style='margin-left: 0px;'>" +
                       "<table class='table table-bordered'>" +
                            "<thead>" +

                                    "<th colspan='2'>" + field.label + "</th>" +

                                 "</thead>" +
                             "<tbody>";
                return tc;

            }
            if (field.field_type == "text-tbl")
            {
                if (Convert.ToString(field.name).Contains("_"))
                {
                    tc = "<tr>" +
                            "<td>" + field.label + "</td>" +
                            //"<div type='text' class='input-group noPastDate_change'>" +
                            "<td><input type = 'date' id = " + field.name + " class='form-control' name=" + field.name + " value='" + field.value[0] + "'/></td>  " +
                            //"</div>" +
                            " </tr>";

                }
                else
                {
                    tc = "<tr>" +
                             "<td>" + field.label + "</td>" +
                             "<td><input type = 'text' id = " + field.name + " class='form-control' name=" + field.name + " value='" + field.value[0] + "'/></td>" +
                             " </tr>";
                }

                return tc;
            }
            if (field.field_type == "table_end")
            {
                tc = "</tbody>" +
                      "</table>" +
                  "</div>";
            }

            return tc;
        }
        /// <summary>
        ///  datepicker controll for dynamic form   
        /// </summary> 
        /// 
        ///<param name="field">Receive table control field value</param>
        public string datepickercontrol(dynamic field)
        {
            var tc = "<div class='form-group row' style='margin-left: 0px;'>" +
                 "<label class='col-sm-4 col-form-label' for='exampleInputEmail1'>" +
                field.label +
                "</label>" +
                "<div type='date' class='input-group eventPicker'>" +
            "<input type = 'text' id = '" + field.name + "' class='form-control' name='" + field.name + "' value='" + field.value[0] + "'/>" +
               "<span class='input-group-addon'>" +
                                                        "<span class='fa fa-calendar'></span>" +
                                                    "</span>" +
                                                "</div> </div>";
            return tc;
        }
        /// <summary>
        ///  text controll for dynamic form   
        /// </summary> 
        /// 
        ///<param name="field">Receive table control field value</param>
        public string textControll(dynamic field)
        {
            var tc = "";

            if (Convert.ToString(field.name).ToLower().Contains("report"))
                tc = "<div class='form-group row' style='margin-left: 0px;'>" +
               "<label class='col-sm-4 col-form-label' for='exampleInputEmail1'>" +
               field.label +
               "<span class='text-danger'></span>" +
               "</label>" +
               "<div class='col-sm-8 col-form-box'>" +
               "<input type='text' class='form-control reports' name='" + field.name + "' value='" + field.value[0] + "'  placeholder=''>" +
               "</div>" +
               "</div>";
            else
                tc = "<div class='form-group row' style='margin-left: 0px;'>" +
                "<label class='col-sm-4 col-form-label' for='exampleInputEmail1'>" +
                field.label +
                "<span class='text-danger'></span>" +
                "</label>" +
                "<div class='col-sm-8 col-form-box'>" +
                "<input type='text' class='form-control' name='" + field.name + "' value='" + field.value[0] + "'  placeholder=''>" +
                "</div>" +
                "</div>";      
            return tc;
        }
        /// <summary>
        ///  file controll for dynamic form   
        /// </summary> 
        /// 
        ///<param name="field">Receive table control field value</param>
        public string fileControll(dynamic field)
        {
            if (field.value != null && field.value != "")
            {
                return anchorControl(field);
            }
            var fc = "<div class='form-group row' style='margin-left: 0px;'>" +
                "<label class='col-sm-4 col-form-label' for='exampleInputPassword1' >" + field.label + "<span class='text-danger' ></span ></label >" +
                "<div class='col-sm-8 col-form-box' > " +
                "<div class='input-group inver-file-upload inver-file-upload-dynamic' > " +
                "<input id= 'uploadFile' class='form-control' value='" + field.value + "' placeholder= 'Choose File' disabled= 'disabled' > " +
                "<div class='input-group-btn' > " +
                "<div class='fileUpload btn btn-success' > " +
                "<span > <i class='fa fa-upload'></i> Choose File</span > " +
                "<input id= 'uploadBtn' type= 'file' class='upload' > " +
                "</div > " +
                "</div > " +
                "</div > " +
                "</div > " +
                "</div>";
            return fc;
        }
        /// <summary>
        ///  radio controll for dynamic form   
        /// </summary> 
        /// 
        ///<param name="field">Receive table control field value</param>
        public string radioControl(dynamic field)
        {
            var rc = "<div class='form-group row' style='margin-left: 0px;'>" +
                "<label class='col-sm-4 col-form-label' for='exampleInputEmail1'>" +
                field.label + "<span class='text-danger'></span>" +
                "</label>" +
                "<div class='col-sm-8 radio-btn-div'>";
            for (var i = 0; i < field.field_options.options.Count; i++)
            {
                var ch = field.field_options.options[i].isChecked == "true" ? "checked" : "";
                rc += "<label class='radio-inline' > " +
                    "<input type='radio' name='optradio' " + ch + ">" + field.field_options.options[i].label +
                    "</label>";

            }
            rc += "</div>" +
                "</div>";
            return rc;
        }
        /// <summary>
        ///  dropdown controll for dynamic form   
        /// </summary> 
        /// 
        ///<param name="field">Receive table control field value</param>
        public string dropdownControl(dynamic field)
        {
            var dc = "<div class='form-group row' style='margin-left: 0px;'>" +
                "<label class='col-sm-4 col-form-label' for='exampleInputEmail1' >" + field.label + "<span class='text-danger' ></span ></label >" +
                "<div class='col-sm-8 col-form-box'>" +
                "<select class='form-control' value='' id='exampleFormControlSelect1'>";
            for (var i = 0; i < field.field_options.options.Count; i++)
            {
                dc += "<option>" + field.field_options.options[i].label + "</option>";
            }
            dc += "</select>" +
                "</div>" +
                "</div>";
            return dc;
        }
        /// <summary>
        ///   textarea controll for dynamic form   
        /// </summary> 
        /// 
        ///<param name="field">Receive table control field value</param>
        public string textareaControl(dynamic field)
        {
            var tc = "<div class='form-group row' style='margin-left: 0px;'>" +
                 "<label class='col-sm-4 col-form-label' for='exampleInputPassword1' >" + field.label;
            if (field.mandatory == 1)
            {
                tc += "<span class='text-danger' ></span>";
            }
            tc += "</label><div class='col-sm-8 col-form-box'>";

            if (field.mandatory == 1)
            {
                if (Convert.ToString(field.name).ToLower().Contains("report"))
                    tc += "<textarea class='form-control mandatory reports' id='" + field.name + "' rows='3' name='" + field.name + "'>" + field.value[0] + "</textarea>";
                else
                    tc += "<textarea class='form-control mandatory ' id='" + field.name + "' rows='3' name='" + field.name + "'>" + field.value[0] + "</textarea>";
            }
            else
            {
                if (Convert.ToString(field.name).Contains("report"))
                    tc += "<textarea class='form-control reports' id='form-69-textarea' rows='3'>" + field.value[0] + "</textarea>";
                else
                    tc += "<textarea class='form-control' id='form-69-textarea' rows='3'>" + field.value[0] + "</textarea>";
            }

            tc += "</div> " +
                "</div>";
            return tc;
        }
        /// <summary>
        ///  date controll for dynamic form   
        /// </summary> 
        /// 
        ///<param name="field">Receive table control field value</param>
        public string dateControl(dynamic field)
        {
            var dc = "<div class='form-group row' style='margin-left: 0px;'>" +
                "<label class='col-sm-4 col-form-label' for='exampleInputEmail1'>" +
                field.label +
                "<span class='text-danger'></span>" +
                "</label>" +
                "<div class='col-sm-8 col-form-box'>" +
                "<input type='date' class='form-control' value='" + field.value + "' id='exampleInputEmail1' placeholder=''>" +
                "</div>" +
                "</div>";
            return dc;
        }
        /// <summary>
        ///  checkbox controll for dynamic form   
        /// </summary> 
        /// 
        ///<param name="field">Receive table control field value</param>
        public string checkboxControl(dynamic field)
        {

            if (field.value[0] == 1)
            {
                var tc = "<div class='form-group row' style='margin-left: 0px;'>" +
                   "<label class='col-sm-4 col-form-label' for='exampleInputEmail1'>" +
                   field.label +
                   "</label>" +
                   "<div class='col-sm-8 col-form-box'>" +
                   "<input type='checkbox' checked class='checkbox-dynamic' name='" + field.name + "'id='exampleInputEmail1' placeholder=''>" +
                   "</div>" +
                   "</div>";
                return tc;
            }
            else
            {
                var tc = "<div class='form-group row' style='margin-left: 0px;'>" +
                    "<label class='col-sm-4 col-form-label' for='exampleInputEmail1'>" +
                    field.label +
                    "</label>" +
                    "<div class='col-sm-8 col-form-box'>" +
                    "<input type='checkbox' class='checkbox-dynamic' name='" + field.name + "'id='exampleInputEmail1' placeholder=''>" +
                    "</div>" +
                    "</div>";
                return tc;
            }
        }
        /// <summary>
        ///  anchor controll for dynamic form   
        /// </summary> 
        /// 
        ///<param name="field">Receive table control field value</param>
        public string anchorControl(dynamic field)
        {
            var tc = "<div class='form-group row' style='margin-left: 0px;'>" +
                "<label class='col-sm-4 col-form-label' for='exampleInputEmail1'>" +
                field.label +
                "<span class='text-danger'></span>" +
                "</label>" +
                "<div class='col-sm-8 col-form-box'>" +
                "<a href='" + field.value + "' target='_blank'>View </a>" +
                "</div>" +
                "</div>";
            return tc;
        }
        /// <summary>
        /// button Controll used for take some functioanality of event JSON 
        /// </summary>
        /// <param name="field">Receive the name of the field name</param>
        /// <param name="teammemberid">Receive the Team member id</param>
        /// <param name="ProgressInPercentage">Receive the task progress precentage</param>
        /// <param name="taskassignid">Receive the task assignment Id</param>
        /// <param name="id">Receive the investigation id</param>
        /// <param name="pvttaskassignmentid">Receive the previous event task assignment Id</param>
        /// <param name="invstatus">Receive the investigation ststus</param>
        /// <param name="eventuniquenumber">Receive the event unique number</param>
        /// <param name="inv_status_type">Receive the investigation status type</param>
        /// <param name="taskgroupid">Receive the investigation task group id</param>
        /// <param name="eventsubmitstatus">Receive the investigation event submit status</param>
        /// <returns></returns>
        public string buttonControll(dynamic field, int teammemberid, int ProgressInPercentage, int taskassignid, int id, int pvttaskassignmentid, int invstatus, int eventuniquenumber, int inv_status_type, int taskgroupid, int eventsubmitstatus)
        {
            var tc = "";

            if (Convert.ToInt32(HttpContext.Current.Session["roleid"]) == 4 /* && /*inv_status_type!=2*/)
            {
                if (field.field_submit == "1")
                {
                    tc += "<button type = 'submit' name='command' value='save' class='btn btn-save' onclick='return chkreportText();'>" + field.label + " & Update</button>";

                    //tc += "<button type = 'submit' name='command' value='complete' class='btn btn-save' onclick=$('.btn-save').hide();>Confirm & Task Complete</button>";

                    if (eventsubmitstatus == 1)
                    {
                        if (invstatus != 1)
                        {
                            tc += "<button type = 'button'  style='float:left; background-color:#03a9f4' class='take_action_iic btn btn-save' id='take_action' data-taskgroupid='" + taskgroupid + "' data-taskassignment='" + taskassignid + "' data-evuniquenumber='" + eventuniquenumber + "'>Take Action</button>";
                        }
                    }
                }
            }
            //else
            //{
            //    if (pvttaskassignmentid == id)
            //    {

            //        if (invstatus == 0)
            //        {

            //            if (field.field_submit == "1")
            //            {
            //                tc += "<button type = 'submit' class='btn btn-save' onclick=$(this).hide();>" + field.label + "</button>";
            //            }
            //            else
            //            {
            //                if (ProgressInPercentage == 100)
            //                {
            //                    tc += "<button type = 'button' class='btn btn-save task_complete' id='tskid_" + taskassignid + "_" + eventuniquenumber + "'>" + field.label + "</button>";
            //                }
            //                else
            //                {
            //                    tc += "<button type = 'button' disabled = 'true' class='btn btn-save' id='" + field.field_id + "'>" + field.label + "</button>";
            //                }
            //            }
            //        }
            //    }
            //}

            //else
            ////{
            ////    if (field.field_submit == "1")
            ////    {
            ////        tc += "<button type = 'submit' disabled = 'true' class='btn btn-save'>" + field.label + "</button>";
            ////    }
            ////    else
            ////    {
            ////        tc += "<button type = 'button' disabled = 'true' class='btn btn-save' id='" + field.field_id + "'>" + field.label + "</button>";
            ////    }

            ////}
            return tc;

        }
        /// <summary>
        ///  label controll for dynamic form   
        /// </summary> 
        /// 
        ///<param name="field">Receive table control field value</param>
        public string labelControl(dynamic field)
        {
            var tc = "<div class='form-group row' style='margin-left: 0px;'>" +
                            "<label class='" + field.class_name + "'>" +
                            field.label +
                            "</label>" +
                            "</div>";
            return tc;

        }
        /// <summary>
        ///  h2 controll for dynamic form   
        /// </summary> 
        /// 
        ///<param name="field">Receive table control field value</param>
        public string h2Control(dynamic field)
        {
            if (field.parent_div_class != null)
            {
                var tc = "<div class='" + field.parent_div_class + "form-group row' style='margin-left: 0px;'><h2 class='" + field.class_name + "'>" + field.label + "</h2></div>";
                return tc;
            }
            else
            {
                var tc = "<div class='form-group row' style='margin-left: 0px;'><h2 class='" + field.class_name + "'>" + field.label + "</h2></div>";
                return tc;
            }


        }
        /// <summary>
        ///  file controll for dynamic form   
        /// </summary> 
        /// 
        ///<param name="field">Receive table control field value</param>
        ///<param name="pvttaskassignmentid">Receive task assignment id</param>
        ///<param name="id">Receive investigation file id</param>
        public string fileInputControll(dynamic field, int pvttaskassignmentid, int id)
        {
            gcaa_ismEntities dc = new gcaa_ismEntities();

            var tc = "<div class='form-group row' style='margin-left: 0px;'>" +

                    "<label class='col-sm-4 col-form-label'>" + field.label;
            if (field.mandatory == 1)
            {
                tc += "<span class='text-danger' ></span>";
            }

            tc += "</label>";
            if (Convert.ToInt32(HttpContext.Current.Session["roleid"]) == 4 || Convert.ToInt32(HttpContext.Current.Session["roleid"]) == 3)
            {
                tc += "<div class='col-sm-8 col-form-box'>" +
                 "<input type='hidden'  id='" + field.name + "_count' value='1'/>" +
                 "<label class='values-tick'<i class='fa fa-times' aria-hidden='true'></i></label>" +
                  "<div class='input-group inver-file-upload' style='margin - left:15px; margin - bottom: 15px; margin - top: 15px; width: 643px;'>" +
                "<input id = '" + field.name + "_uploadFile_1' class='form-control' placeholder='Maximum file size limited to 100MB' disabled='disabled'>" +
                "<div class='input-group-btn'>" +
                    "<div class='fileUpload btn btn-success'>" +
                       "<span><i class='fa fa-upload'></i> Choose File</span>";
                if (field.mandatory == 1)
                {
                    tc += "<input id ='" + field.name + "_uploadBtn_1' type ='file' class='event_upload mandatory' name='" + field.name + "'/>";
                }
                else
                {
                    tc += "<input id ='" + field.name + "_uploadBtn_1' type ='file' class='event_upload' name='" + field.name + "'/>";
                }



                tc += "</div>" +
             "</div>" +
         "</div>" +
     "</div>";
            }
            tc += "</div>";

            if (field.allow_multiple == 1)
            {
                tc += "<div class='repeat_uploads'><div class='col-md-12 " + field.div_name + " no-padding'></div>";
                if (Convert.ToInt32(HttpContext.Current.Session["roleid"]) == 4)
                {
                    tc += "<div class='col-sm-12 col-form-label'>" +
                        "<label class='cur " + field.multiple_class + "' data-control_name=" + field.name + " data-divname='" + field.div_name + "'><i class='fa fa-plus' aria-hidden='true'></i> Add New</label>" +
                       "</div>";
                }
                tc += "</div>";
            }

            if (field.allow_multiple == 1 && field.value != null && field.value[0] != "")
            {
                JArray items2 = (JArray)field.value;
                int n = items2.Count;


                for (int j = 0; j < n; j++)
                {
                    if (field.value[j] != null)
                    {
                        int primary_key = field.value[j];
                        string url = dc.InvestigationDocumentAsAttachments.Where(a => a.InvestigationDocumentAsAttachmentID == primary_key).Select(a => a.AttachmentPublicUrl).FirstOrDefault();
                        //      tc += "<div class=''><div class='col-sm-6 col-form-label'>" +
                        //   //"<label class='cur " + field.multiple_class + "' data-control_name=" + field.name + " data-divname='" + field.div_name + "'> Add New <i class='fa fa-plus-square' aria-hidden='true'></i></label>" +
                        //  "<div><ul style='padding-left: 0;list - style: none;margin: 5px 0;'><li><a href='"+url+"'> View"+
                        //  "</a></li></ul></div>" +
                        //  "</div>" +
                        //  "<div class='col-sm-6 " + field.div_name + " no-padding'>" +
                        //"</div></div>";

                        tc += "<div class='col-sm-9 col-form-box' style='padding-right: 18px;' id='filedatadiv-" + primary_key + "'>" +
                               "<label class='values-tick'><i class='fa fa-check' aria-hidden='true'></i></label>" +

                                "<a href = '" + url + "' target='_blank' class='view-tag'>View</a>";

                        if (Convert.ToInt32(HttpContext.Current.Session["roleid"]) == 4)
                        {
                            tc += "<a href = '' data-attachid='" + primary_key + "' data-filename='" + field.name + "'  class='view-tag-delete'>Delete</a>";

                        }
                        tc += "</div>";
                    }
                }

                tc += "<input type='hidden' id='deleted-" + field.name + "' name='deleted-" + field.name + "'>";

            }
            return tc;

        }
        /// <summary>
        ///  hidden controll for dynamic form   
        /// </summary> 
        ///
        ///<param name="field">Receive table control field value</param>
        /// <param name="invfileid">Receive investigation file id</param>
        ///  <param name="taskassignid">Receive investigation task assignment id</param>
        public string hiddenControll(dynamic field, int invfileid, int taskassignid)
        {
            var tc = "";
            if (field.name == "InvFileID")
            {
                tc += "<input type='hidden' name='" + field.name + "' id='" + field.name + "' value='" + invfileid + "'>";
            }
            else if (field.name == "TeamAssignID")
            {
                tc += "<input type='hidden' name='" + field.name + "' id='" + field.name + "' value='" + taskassignid + "'>";

            }
            return tc;
        }
        /// <summary>
        /// Modify JsonA Prop Value With JsonB Prop Value
        /// </summary> 
        /// 
        ///<param name="oldjsonFieldsProp">Receive old json field properties</param>
        ///<param name="oldjsonValueProp">Receive old json value properties</param>
        ///<param name="oldjsonNameProp">Receive old json name properties</param>
        ///<param name="namePropValue">Receive  old json value</param>
        ///<param name="parsedJsonold">Receive JObject value</param>
        ///<param name="parsedJsonnew">Receive JObject value</param>
        ///<param name="currentIndex">Receive current object index</param>
        public string ModifyJsonAPropValueWithJsonBPropValue(string oldjsonFieldsProp, string oldjsonValueProp, string oldjsonNameProp, string namePropValue, JObject parsedJsonold, JObject parsedJsonnew, int currentIndex)
        {
            if (parsedJsonnew[namePropValue] != null)
            {
                var propArray = parsedJsonnew[namePropValue];
                if (parsedJsonold[oldjsonFieldsProp][currentIndex][oldjsonValueProp] != null)
                {
                    JArray valuePropArray = (JArray)parsedJsonold[oldjsonFieldsProp][currentIndex][oldjsonValueProp];
                    valuePropArray.Clear(); // Clearing the empty value
                    foreach (var item in propArray)
                    {
                        valuePropArray.Add(item);
                    }
                }
            }
            return parsedJsonold.ToString();
        }
    }
}
