﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ISM_Project.Models
{
    public class InvestigationAttachmentModel
    {
        public int InvestigationDocumentAsAttachmentID { get; set; }
        public int InvestigationFileID { get; set; }
        public int LKInvestigationAttachmentTypeID { get; set; }
        public int LKDocumentTypeID { get; set; }
        public Uri AttachmentPublicUrl { get; set; }
        public System.DateTime DateCreated { get; set; }
        public Nullable<System.DateTime> DateUpdated { get; set; }
        public int CreatedBy { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
    }
}