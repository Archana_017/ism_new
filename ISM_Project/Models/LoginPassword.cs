﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ISM_Project.Models
{
    public class LoginPassword

    {
        public string createsalt(int size)
        {
            var rg = new System.Security.Cryptography.RNGCryptoServiceProvider();
            var buff = new byte[size];
            rg.GetBytes(buff);
            return Convert.ToBase64String(buff);

        }
        public string Generatehash(string input,string salt)
        {
            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(input + salt);
            System.Security.Cryptography.SHA256Managed sha256string = new System.Security.Cryptography.SHA256Managed();
            byte[] hash = sha256string.ComputeHash(bytes);
            return Convert.ToBase64String(hash);
        }
    }
}