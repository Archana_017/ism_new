﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ISM_Project.Models
{
    public class InventoryModel
    {
        public int InventoryID { get; set; }
        public Nullable<int> Category { get; set; }
        public string CategoryName { get; set; }
        public string Item { get; set; }
        public Nullable<int> ItemNo { get; set; }
        public string Description { get; set; }
        public string PartNo { get; set; }
        public string SNo { get; set; }
        public string Quantity { get; set; }
        public string QuantityType { get; set; }
        public int QuantityID { get; set; }
        public string Availability { get; set; }
        public Nullable<System.DateTime> ExpiryDate { get; set; }
        public string RequiredItems { get; set; }
        public string ExpiredItems { get; set; }
        public string Comments { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public Nullable<System.DateTime> UsageDate { get; set; }
        public string Remarks { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public HttpPostedFileBase[] InvenFiles { get; set; }

        public int INVENTORYQUANTITYID { get; set; }
        public string AVAILABLE { get; set; }
        public string REFILLED { get; set; }

        public Nullable<int> QunatityUsed { get; set; }
        public Nullable<int> InvestigationID { get; set; }
        public Nullable<int> NotificationID { get; set; }

        public IEnumerable<USP_VIEW_ALL_INVENTORIES_Result> All_Inv { get; set; }
        public IEnumerable<USP_VIEW_INV_CATEGORY_Result> Inv_Cat { get; set; }
        public IEnumerable<USP_VIEW_INVENTORY_Result> View_Inv { get; set; }
        public IEnumerable<USP_INVENTORY_ATTACHMENTS_Result> Inv_Attach { get; set; }
        public IEnumerable<USP_VIEW_INVENTORY_QUANTITY_HISTORY_Result> Inve_Updt { get; set; }
        public IEnumerable<USP_INVENTORY_INVESTIGATION_Result> Inve_Use { get; set; }

    }
}