﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using Microsoft.SharePoint.Client;
using Microsoft.SharePoint.Client.Sharing;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Security;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using ISM_Project.Models;
using A1 = DocumentFormat.OpenXml.Drawing;
using DW = DocumentFormat.OpenXml.Drawing.Wordprocessing;
using PIC = DocumentFormat.OpenXml.Drawing.Pictures;
using System.Xml.Linq;

namespace ISM_Project.Controllers
{
    /// <summary>
    ///  Director - Controller contains the functionality of  new investigation open and close i.e IIC Assignment , Re-Assignament 
    /// </summary> 
    /// 

    public class DirectorController : Controller
    {
        Common comm = new Common();
        MailConfiguration mail = new MailConfiguration();
        PdfGeneration pdf = new PdfGeneration();
        /// <summary>
        /// Redirect to DI_Filled_Form35 view page 
        /// </summary> 
        /// 
        /// <param name="id">Receive the investigation file id.</param>
        public ActionResult DI_Filled_Form35(int id = 0)
        {
            var model = new Form39();
            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 3 || Convert.ToInt32(Session["RoleId"]) == 6)
                {
                    gcaa_ismEntities gcaa = new gcaa_ismEntities();
                    var Inv = gcaa.InvestigationFileDetails.ToList();
                    model = new Form39
                    {
                        Inv = gcaa.InvestigationFileDetails.ToList(),
                        MasterValues = gcaa.LKCOMMONs.ToList(),
                        Ass = gcaa.InvestigationFileAssessments.ToList(),
                        Form35_DI = gcaa.USP_VIEW_INVESTIGATION_FORM35(id).ToList(),
                        Attachment = gcaa.USP_VIEW_INVESTIGATION_ATTACHMENTFILE(id).ToList(),
                        Inv_AircraftDet = gcaa.USP_VIEW_INVESTIGATION_AIRCRAFT_DETAIL_FORM35(id).ToList(),

                    };
                    
                    var Notify_ID = gcaa.USP_VIEW_INVESTIGATION_FORM35(id).FirstOrDefault();
                    OccurrenceDetail occurrenceDetail = (from o in gcaa.OccurrenceDetails where o.OccurrenceDetailID == Notify_ID.OccurrenceDetailID select o).SingleOrDefault();
                    occurrenceDetail.IsViewedOrActionTaken = true;
                    gcaa.SaveChanges();
                    TempData["PageHead"] = Notify_ID.NotificationID;
                    Session["InvFileID"] = Notify_ID.InvestigationFileID;
                    var acc_class = gcaa.USP_GET_MASTERVALUES(1).ToList();
                    ViewBag.AccID_ClassList = acc_class;
                    var Air_Class = gcaa.USP_GET_MASTERVALUES(2);
                    ViewBag.Air_ClassList = Air_Class.ToList();
                    var USE = gcaa.USP_GET_MASTERVALUES(3).ToList();
                    ViewBag.USEList = USE;
                    var INVOLVE = gcaa.USP_GET_MASTERVALUES(4).ToList();
                    ViewBag.INVOLVEList = INVOLVE;
                    var YesNo = gcaa.USP_GET_MASTERVALUES(17).ToList();
                    ViewBag.YesNoList = YesNo;
                    var Close_Reason = gcaa.USP_GET_MASTERVALUES(18).ToList();
                    SelectList Reasonlist = new SelectList(Close_Reason, "ID", "LKVALUE");
                    ViewBag.CloseReasonList = Reasonlist;
                    var DI_Sugg = gcaa.USP_GET_MASTERVALUES(5);
                    ViewBag.DI_SuggList = DI_Sugg.ToList();
                    var DI_Fur_Sugg = gcaa.USP_GET_MASTERVALUES(6);
                    ViewBag.DI_Fur_SuggList = DI_Fur_Sugg.ToList();
                    var Incident_Type = gcaa.USP_GET_INCIDENTTYPE().ToList();
                    ViewBag.Incident_TypeList = Incident_Type;

                    var Investigation_Type = gcaa.USP_GET_MASTERVALUES(7);
                    SelectList InvestigationType = new SelectList(Investigation_Type, "ID", "LKVALUE");
                    ViewBag.Investigation_Type = Investigation_Type;
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("DirectorController", "DI_Filled_Form35", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }
            return View(model);
        }
        /// <summary>
        /// DI_Filled_Form35 view page to open the investigation of the selected investigation file.
        /// </summary> 
        /// 
        /// <param name="formValues">Receive the investigation form 035 details as object.</param>
        [HttpPost]
        public ActionResult DI_Filled_Form35(Form39 formValues)
        {
            //  var model = new Form39();
            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 3)
                {
                    gcaa_ismEntities gcaa = new gcaa_ismEntities();
                    if (formValues.InvestigationFileID == 0)
                    {
                        gcaa_ismEntities dc = new gcaa_ismEntities();
                        formValues.InvestigationFileID = Convert.ToInt32(Session["InvFileID"]);
                        formValues.InvestigationClosedBy = Convert.ToInt32(Session["UserId"]);
                        var result = dc.USP_CLOSE_OCCURENCE(formValues.InvestigationFileID, formValues.InvestigationClosureReason, formValues.InvestigationClosedBy, formValues.InvestigationClosedBy, formValues.InvestigationClosedBy);
                        dc.SaveChanges();
                    }
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("DirectorController", "DI_Filled_Form35::POST", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }
            //if (id != 0)
            //{
            //    var occurence = gcaa.USP_VIEW_OCCURRENCE(id).FirstOrDefault();
            //    return View(occurence);
            //}
            return RedirectToAction("Dashboard", "Home");
        }
        /// <summary>
        /// Open the Form_39 view page for open new investigation file and assigning investigator and teammember.
        /// </summary> 
        /// 
        /// <param name="id">Receive the investigation file id.</param>
        public ActionResult Form_39(int id = 0)
        {
            var model = new Form39();
            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 3 || Convert.ToInt32(Session["RoleId"]) == 3)
                {
                    gcaa_ismEntities gcaa = new gcaa_ismEntities();

                    int OccYear = gcaa.InvestigationFileDetails.Where(a => a.InvestigationFileID == id).Select(a => a.LocalDateAndTimeOfOccurrence).FirstOrDefault().Value.Year;

                    model = new Form39
                    {
                        Inv = gcaa.InvestigationFileDetails,
                        MasterValues = gcaa.LKCOMMONs,
                        Ass = gcaa.InvestigationFileAssessments,
                        Form35_DI = gcaa.USP_VIEW_INVESTIGATION_FORM35(id).ToList(),
                        assign_iic = gcaa.USP_view_Assign_IIC().ToList(),
                        Inv_AircraftDet = gcaa.USP_VIEW_INVESTIGATION_AIRCRAFT_DETAIL_FORM35(id).ToList(),
                        list_iic_incharge = gcaa.USP_LIST_IIC_INCHARGE_WITH_CASE().ToList(),
                        InvestigationNumber = gcaa.USP_GET_MAX_INVESTIGATION_FILE_NUM(OccYear).FirstOrDefault()
                    };

                    var Notify_ID = gcaa.USP_VIEW_INVESTIGATION_FORM35(id).FirstOrDefault();
                    TempData["PageHead"] = Notify_ID.NotificationID;
                    //TempData["BackPage"] = "yes";
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("DirectorController", "Form_39", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }
            return View(model);
        }
        /// <summary>
        /// View profile of paticluar investigation incharge
        /// </summary> 
        /// 
        /// <param name="userid">Receive the user id.</param>
        public ActionResult IIC_View_Profile(int userid = 0)
        {
            var model = new Form39();
            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 3 || Convert.ToInt32(Session["RoleId"]) == 4 || Convert.ToInt32(Session["RoleId"]) == 6 || Convert.ToInt32(Session["RoleId"]) == 7)
                {
                    gcaa_ismEntities gcaa = new gcaa_ismEntities();

                    model = new Form39
                    {

                        iic_view_profile = gcaa.USP_IIC_VIEW_PROFILE(userid).ToList(),
                        iic_view_profile_2 = gcaa.USP_IIC_VIEW_PROFILE_2(userid).ToList(),
                        IIC_Cases = gcaa.USP_VIEW_IIC_CASES(userid).ToList()
                    };
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("DirectorController", "IIC_View_Profile", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }
            return View(model);
        }
        /// <summary>
        ///  Assign investigation to selected investigation incharge and selected team members.
        /// </summary> 
        /// 
        /// <param name="id">Receive the investigation id.</param>
        public ActionResult IIC_Assigned(int id = 0)
        {
            var model = new Form39();
            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 3 || Convert.ToInt32(Session["RoleId"]) == 6)
                {
                    gcaa_ismEntities gcaa = new gcaa_ismEntities();
                    int Inv_Id = Convert.ToInt32(Session["InvFileID"]);
                    if (id > 0)
                    {
                        model = new Form39
                        {
                            Form35_DI = gcaa.USP_VIEW_INVESTIGATION_FORM35(id).ToList(),
                        };
                        var Notify_ID = gcaa.USP_VIEW_INVESTIGATION_FORM35(Inv_Id).FirstOrDefault();
                        TempData["PageHead"] = Notify_ID.NotificationID;
                        //Session["AAICase"] = Notify_ID.InvestigationNumber;
                        //Session["IICAssigned"] = Notify_ID.IICAssigned;
                        //TempData["PageHead"] = Notify_ID.InvestigationNumber;
                        //TempData["BackPage"] = "yes";
                    }
                }
                else
                {
                    return RedirectToAction("Dashboard", "Home");
                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("DirectorController", "IIC_Assigned", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                return RedirectToAction("PageNotFound", "Error");
            }
            return View(model);
        }
        /// <summary>
        /// Save the assigned investigation incharge and the team member of selected investigation
        /// </summary> 
        /// 
        /// <param name="formVal">Receive the investigation details as object.</param>
        [HttpPost]
        public JsonResult Assign_IIC(Form39 formVal)
        {
            bool mailSent = false; string message = string.Empty;
            try
            {
                if (Convert.ToInt32(Session["RoleId"]) == 3)
                {
                    gcaa_ismEntities dc = new gcaa_ismEntities();

                    string[] InvNoSplit = (formVal.InvestigationNumber.Split('/'));

                    if (InvNoSplit[0] != "AIFN")
                    {
                        return Json("3");
                    }

                    if (InvNoSplit[1].Length != 4)
                    {
                        return Json("3");
                    }

                    if (InvNoSplit[2].Length != 4)
                    {
                        return Json("3");
                    }

                    var AlredyExist = dc.InvestigationFiles.Where(a => a.InvestigationFileNumber == formVal.InvestigationNumber).FirstOrDefault();

                    if (AlredyExist != null)
                    {
                        return Json("2");
                    }

                    //formVal.InvestigationFileID = Convert.ToInt32(Session["InvFileID"]);
                    int user_ID = Convert.ToInt32(Session["UserId"]);
                    var result = dc.USP_ASSIGN_IIC_INVESTIGATION(Convert.ToInt32(Session["InvFileID"]), formVal.IICAssigned, 0, 0, user_ID, null, null, user_ID, user_ID, "ASSIGN",formVal.InvestigationNumber);
                    dc.SaveChanges();

                    #region Change On 23-jul-2018

                    formVal.InvestigationFileID = Convert.ToInt32(Session["InvFileID"]);

                    var team_member = dc.USP_VIEW_TEAMMEMBER(formVal.InvestigationFileID).ToList();

                    if (team_member.Count() == 0)
                    {
                        if (formVal.TeammemberList == null)
                        {
                            //return RedirectToAction("Teammanagement", "task", new { id = tm.invfile });
                        }
                        else
                        {
                            for (int i = 0; i < formVal.TeammemberList.Length; i++)
                            {
                                dc.USP_INST_TEAMMEMBERS(formVal.InvestigationFileID, Convert.ToInt32(formVal.TeammemberList[i]), Convert.ToInt32(Session["UserId"]));
                            }
                            dc.USP_INST_SRM_AS_TEAM_MEMBER(formVal.InvestigationFileID, Convert.ToInt32(Session["UserId"]));
                        }
                    }
                    else
                    {
                        if (formVal.TeammemberList == null)
                        {
                            var tm_member = dc.USP_VIEW_TEAMMEMBER(formVal.InvestigationFileID).ToList();
                            foreach (var item_teammember in tm_member)
                            {
                                dc.USP_INST_TEAMMEMBERHISTORY_WITHOUT_REASON(item_teammember.InvestigationFileID, item_teammember.TeamMemberID, Convert.ToInt32(Session["UserId"]));
                                dc.USP_DELETE_TEAMMEMBER(item_teammember.InvestigationFileID, item_teammember.TeamMemberID);
                            }
                        }
                        else
                        {
                            for (int i = 0; i < formVal.TeammemberList.Length; i++)
                            {

                                dc.USP_Teammember_Removal_Process(formVal.InvestigationFileID, Convert.ToInt32(formVal.TeammemberList[i]), Convert.ToInt32(Session["UserId"]));

                            }
                            var temp_invfile = dc.USP_TeamTemp_Retrieval(formVal.InvestigationFileID).ToList();

                            foreach (var item_temp in temp_invfile)
                            {
                                dc.USP_DELETE_TEAMMEMBER(item_temp.InvestigationFileID, item_temp.TeamMemberID);
                                dc.USP_INST_TEAMMEMBERHISTORY_WITHOUT_REASON(item_temp.InvestigationFileID, item_temp.TeamMemberID, Convert.ToInt32(Session["UserId"]));

                            }
                            dc.USP_CLEAR_TEAMTEMP(formVal.InvestigationFileID);
                        }
                    }

                    #endregion

                    // check if the investigation type is foreign - (LKCommon lookup table id as 21)

                    int mailtemplateid = 26;

                    var IsForeignInv = dc.USP_VIEW_INVESTIGATION_TYPEOFINV(formVal.InvestigationFileID, 21).FirstOrDefault();

                    if (Convert.ToInt32(IsForeignInv.InvestigationTypeId) == 20) // UAE investigation
                    {
                        mailtemplateid = 3;

                        #region Change On 31-jul-2018

                        int TaskGroupCount = dc.InvestigationTaskGroups.Count();

                        var TaskGroupinfo = dc.InvestigationTaskGroups.ToList();

                        for (int i = 0; i < TaskGroupCount; i++)      // Save all Scope 
                        {
                            result = dc.USP_INST_INVESTIGATION_SCOPE_DEFINITION(formVal.InvestigationFileID, Convert.ToInt32(TaskGroupinfo[i].InvestigationTaskGroupID), Convert.ToInt16(0),

                               Convert.ToInt16(0), 0, 0, 0, 0, 0, Convert.ToInt32(Session["UserId"]));
                        }

                        // Update Scope status for Event E1 & E66

                        int InvestigationScopeDefinitionID = dc.InvestigationScopeDefinitions.Where(a => a.InvestigationFileID == formVal.InvestigationFileID &&

                                                              a.InvestigationTaskGroupID == 16).FirstOrDefault().InvestigationScopeDefinitionID;

                        result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION(InvestigationScopeDefinitionID, 1, Convert.ToInt32(Session["UserId"]));

                        int InvestigationTaskGroupEventID = 0; DateTime DateTimeofCompletion = DateTime.Now; string InvestigationTaskGroupEventUniqueNumber = string.Empty;

                        int InvestigationTaskGroupID = 16;

                        System.Data.Entity.Core.Objects.ObjectParameter inv_task_assignment_id = new System.Data.Entity.Core.Objects.ObjectParameter("inv_task_assignment_id", typeof(Int32));

                        #region for E1

                        InvestigationTaskGroupEventUniqueNumber = "E1"; InvestigationTaskGroupEventID = 115;

                        result = dc.USP_INST_INVESTIGATION_TASK_ASSIGNMENT_WITH_MULTIPLETEAMMEMBER(formVal.InvestigationFileID, InvestigationTaskGroupEventID,

                                                        /*Convert.ToInt32(scope.TeamMemberId)*/null, DateTimeofCompletion.AddDays(15), Convert.ToInt32(Session["userid"]),

                                                        InvestigationTaskGroupEventUniqueNumber, InvestigationTaskGroupID, inv_task_assignment_id);

                        result = dc.USP_INST_INVESTIGATION_TASKDETAILS(formVal.InvestigationFileID, InvestigationTaskGroupEventID, 0, null, 0,

                            Convert.ToInt32(Session["UserId"]), InvestigationTaskGroupID, InvestigationTaskGroupEventUniqueNumber, Convert.ToInt32(inv_task_assignment_id.Value));

                        result = dc.USP_INST_MULTIPLE_TEAMMEMBER_EVENT(Convert.ToInt32(inv_task_assignment_id.Value), formVal.IICAssigned, Convert.ToInt32(Session["UserId"]),

                            Convert.ToInt32(Session["UserId"]));

                        var InvEventInfo = dc.InvestigationTaskGroupEvents.Where(a => a.InvestigationTaskGroupEventUniqueNumber == InvestigationTaskGroupEventUniqueNumber).ToList();

                        foreach (var TaskId in InvEventInfo)
                        {
                            result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_TASKGROUPIDandINVESTIGATIONID(TaskId.InvestigationTaskGroupID, formVal.InvestigationFileID, Convert.ToInt32(Session["userid"]));
                        }



                        #endregion

                        #region for E66

                        InvestigationTaskGroupEventUniqueNumber = "E66"; InvestigationTaskGroupEventID = 116;

                        result = dc.USP_INST_INVESTIGATION_TASK_ASSIGNMENT_WITH_MULTIPLETEAMMEMBER(formVal.InvestigationFileID, InvestigationTaskGroupEventID,

                                                        /*Convert.ToInt32(scope.TeamMemberId)*/null, DateTimeofCompletion.AddMonths(6), Convert.ToInt32(Session["userid"]),

                                                        InvestigationTaskGroupEventUniqueNumber, InvestigationTaskGroupID, inv_task_assignment_id);

                        result = dc.USP_INST_INVESTIGATION_TASKDETAILS(formVal.InvestigationFileID, InvestigationTaskGroupEventID, 0, null, 0,

                            Convert.ToInt32(Session["UserId"]), InvestigationTaskGroupID, InvestigationTaskGroupEventUniqueNumber, Convert.ToInt32(inv_task_assignment_id.Value));

                        result = dc.USP_INST_MULTIPLE_TEAMMEMBER_EVENT(Convert.ToInt32(inv_task_assignment_id.Value), formVal.IICAssigned, Convert.ToInt32(Session["UserId"]),

                            Convert.ToInt32(Session["UserId"]));

                        InvEventInfo = dc.InvestigationTaskGroupEvents.Where(a => a.InvestigationTaskGroupEventUniqueNumber == InvestigationTaskGroupEventUniqueNumber).ToList();

                        foreach (var TaskId in InvEventInfo) // Event info 
                        {
                            result = dc.USP_UPDT_INVESTIGATION_SCOPE_DEFINITION_TASKGROUPIDandINVESTIGATIONID(TaskId.InvestigationTaskGroupID, formVal.InvestigationFileID, Convert.ToInt32(Session["userid"]));
                        }

                        #endregion

                        //#region Email Notification

                        //List<string> ListUsers = new List<string>();

                        //List<string> ListUsersEmail = new List<string>();

                        //int UserId = Convert.ToInt32(Session["UserId"]);

                        //int TeamMemberId = Convert.ToInt32(formVal.IICAssigned);

                        //var TeammberInfo = dc.Users.AsQueryable().Where(a => a.UserID == TeamMemberId).FirstOrDefault();

                        //ListUsersEmail.Add(TeammberInfo.EmailAddress.ToString());

                        //ListUsers.Add(TeammberInfo.FirstName.ToString());

                        //var InvInfo = dc.InvestigationFiles.AsQueryable().Where(a => a.InvestigationFileID == formVal.InvestigationFileID).FirstOrDefault();

                        //var LoginUserInfo = dc.Users.AsQueryable().Where(a => a.UserID == UserId).FirstOrDefault();

                        //string siteURL = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));

                        //bool IsSend = mail.SendMailNotification(4, LoginUserInfo.EmailAddress, LoginUserInfo.UserRoleID, siteURL, ListUsers, ListUsersEmail, InvInfo.InvestigationFileID,

                        //null, InvInfo.InvestigationFileNumber, null, "", "");

                        //#endregion

                        #endregion
                    }

                    //  var DList = dc.USP_GET_DL(3).ToList();
                    //Assigned IICs
                    var Assigned_IIC = dc.USP_VIEW_ASSIGNED_IIC(Convert.ToInt32(Session["InvFileID"])).ToList();
                    List<string> DListUsers = new List<string>();
                    List<string> DListUsersEmail = new List<string>();
                    foreach (var users in Assigned_IIC)
                    {
                        DListUsersEmail.Add(users.EmailAddress);
                        DListUsers.Add(users.FirstName);
                    }
                    string siteURL = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                    dynamic Inv = dc.USP_VIEW_INVESTIGATION_FORM35_PDF(Convert.ToInt32(Session["InvFileID"])).FirstOrDefault();
                    dynamic FILES = dc.USP_VIEW_INVESTIGATION_ATTACHMENTFILE(Convert.ToInt32(Session["InvFileID"])).ToList();
                    dynamic invAircraft = dc.USP_VIEW_INVESTIGATION_AIRCRAFT_DETAIL_FORM35_PDF(Convert.ToInt32(Session["InvFileID"])).ToList();
                    
                    mailSent = mail.SendMailNotification(mailtemplateid, Convert.ToString(Session["FirstName"]), Convert.ToInt32(Session["roleid"]), siteURL, DListUsers, DListUsersEmail,
                        
                        Convert.ToInt32(Session["InvFileID"]),null, Inv.NotificationID, null, null, null);

                    //DListUsers = new List<string>();
                    //DListUsersEmail = new List<string>();
                    //for (int i = 0; i < formVal.TeammemberList.Length; i++)
                    //{
                    //    int userid = Convert.ToInt32(formVal.TeammemberList[i]);

                    //    var userinfo = dc.Users.Where(a => a.UserID == userid).FirstOrDefault();

                    //    DListUsersEmail.Add(userinfo.EmailAddress);
                    //    DListUsers.Add(userinfo.FirstName);
                    //}
                    //mailSent = mail.SendMailNotification(4, Convert.ToString(Session["FirstName"]), Convert.ToInt32(Session["roleid"]), siteURL,

                    //        DListUsers, DListUsersEmail, Convert.ToInt32(Session["InvFileID"]), null, Inv.NotificationID, null, null, null);
                }
                else
                {

                }
            }
            catch (System.Exception ex)
            {
                comm.Exception_Log("DirectorController", "IIC_Assigned::POST", "Message : " + ex.Message + "InnerException : " + ex.InnerException + " Line No : " + ex.StackTrace, Convert.ToInt32(Session["UserId"]));

                mailSent = false;

                RedirectToAction("PageNotFound", "Error");
            }
            return Json(mailSent);
        }

    }
}