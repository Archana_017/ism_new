//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ISM_Project.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class InvestigationEventAnalysisDetailed
    {
        public int InvestigationEventAnalysisDetailedId { get; set; }
        public int InvestigationEventAnalysisId { get; set; }
        public int InvestigationFileId { get; set; }
        public Nullable<int> LKOrganizationalInfluencesId { get; set; }
        public Nullable<int> LKUnsafeSupervisionId { get; set; }
        public Nullable<int> LKPreConditionsId { get; set; }
        public Nullable<int> LKUnsafeActId { get; set; }
        public Nullable<int> LKLatentActiveCheck { get; set; }
        public Nullable<System.DateTime> DateCreated { get; set; }
        public Nullable<System.DateTime> DateUpdated { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
    }
}
